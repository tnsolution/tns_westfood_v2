﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
namespace TNS.FIN
{
    public class RiceTeamCompany_Data
    {
        public static DataTable List()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT *,  AS [Key] FROM FNC_RiceTeamCompany WHERE RecordStatus != 99 ORDER BY [RANK]";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }

        //
        public static DataTable TIENCOMCONGTY(DateTime FromDate, DateTime ToDate,int BranchKey,int DepartmentKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string Fillter = "";

            if (BranchKey > 0)
            {
                Fillter += " AND BranchKey = @BranchKey";
            }
            if (DepartmentKey > 0)
                Fillter += " AND DepartmentKey =@DepartmentKey ";

            string zSQL = @"
--declare @FromDate datetime ='2020-01-01 00:00:00'
--declare @ToDate datetime ='2020-12-31 23:59:59'
--declare @TeamKey int = 79

CREATE TABLE Temp
(
	[BranchKey] INT,
	[DepartmentKey] INT ,
	[TeamKey] INT,
	[TeamName] nvarchar(500) ,
	[TeamID] nvarchar(500) ,
	[Month] INT,
	[YEAR] INT ,
	[COMTRUA] FLOAT,
	[COMCONGTY] FLOAT ,
    [DONVINAU] FLOAT ,
	[COMNGOAI] FLOAT 
)
INSERT INTO Temp
SELECT BranchKey,DepartmentKey,TeamKey,TeamName,TeamID,MONTH(DateWrite),
YEAR(DateWrite),SUM(Money1),SUM(Money2),SUM(Money3),SUM(Money4)
FROM [dbo].[FNC_RiceTeamCompany] WHERE RecordStatus <> 99 
AND DateWrite BETWEEN @FromDate AND @ToDate 
@Parameter
GROUP BY DateWrite, BranchKey,DepartmentKey,TeamKey,TeamName,TeamID


SELECT 
P.DepartmentName + '|' + P.TeamName + '|' + P.TeamID AS LeftColumn,
P.DateWrite AS HeaderColumn,
P.COMTRUA, P.COMCONGTY, P.DONVINAU, P.COMNGOAI
FROM
(
	SELECT A.BranchKey,A.DepartmentKey,C.DepartmentID,C.DepartmentName,
	A.TeamKey,A.TeamID,A.TeamName,CONCAT( N'THÁNG ',[Month],'/',YEAR) AS DateWrite ,
	A.COMTRUA,A.COMCONGTY,A.DONVINAU, A.COMNGOAI,
	D.RANK AS BranchRank,C.RANK AS DepartmentRank,B.RANK as TeamRank FROM Temp A 
	LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
	LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
	LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
   
) P

DROP TABLE Temp";
            zSQL = zSQL.Replace("@Parameter", Fillter);
            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BranchKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable PHANCOMCONGTY(DateTime FromDate, DateTime ToDate, int BranchKey, int DepartmentKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string Fillter = "";

            if (BranchKey > 0)
            {
                Fillter += " AND BranchKey = @BranchKey";
            }
            if (DepartmentKey > 0)
                Fillter += " AND DepartmentKey =@DepartmentKey ";

            string zSQL = @"
--declare @FromDate datetime ='2020-01-01 00:00:00'
--declare @ToDate datetime ='2020-12-31 23:59:59'
--declare @TeamKey int = 79

CREATE TABLE Temp
(
	[BranchKey] INT,
	[DepartmentKey] INT ,
	[TeamKey] INT,
	[TeamName] nvarchar(500) ,
	[TeamID] nvarchar(500) ,
	[Month] INT,
	[YEAR] INT ,
	[COMTRUA] FLOAT,
	[COMCONGTY] FLOAT 
)
INSERT INTO Temp
SELECT BranchKey,DepartmentKey,TeamKey,TeamName,TeamID,MONTH(DateWrite),
YEAR(DateWrite),SUM(Rice1),SUM(Rice2)
FROM [dbo].[FNC_RiceTeamCompany] WHERE RecordStatus <> 99 
AND DateWrite BETWEEN @FromDate AND @ToDate 
@Parameter
GROUP BY DateWrite, BranchKey,DepartmentKey,TeamKey,TeamName,TeamID


SELECT 
P.DepartmentName + '|' + P.TeamName + '|' + P.TeamID AS LeftColumn,
P.DateWrite AS HeaderColumn,
P.COMTRUA, P.COMCONGTY
FROM
(
	SELECT A.BranchKey,A.DepartmentKey,C.DepartmentID,C.DepartmentName,
	A.TeamKey,A.TeamID,A.TeamName,CONCAT( N'THÁNG ',[Month],'/',YEAR) AS DateWrite ,
	A.COMTRUA,A.COMCONGTY,
	D.RANK AS BranchRank,C.RANK AS DepartmentRank,B.RANK as TeamRank FROM Temp A 
	LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
	LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
	LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
   
) P

DROP TABLE Temp";
            zSQL = zSQL.Replace("@Parameter", Fillter);
            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BranchKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static float DonGiaComCty(DateTime Date)
        {
            float zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT [dbo].[LayDonGiaComCty](@Date)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Date", SqlDbType.DateTime).Value = Date;
                if(float.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                //Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
    }
}
