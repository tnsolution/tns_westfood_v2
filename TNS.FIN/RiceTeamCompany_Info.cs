﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
namespace TNS.FIN
{
    public class RiceTeamCompany_Info
    {

        #region [ Field Name ]
        private int _AutoKey = 0;
        private DateTime _DateWrite;
        private int _TeamKey = 0;
        private string _TeamID = "";
        private string _TeamName = "";
        private int _BranchKey = 0;
        private int _DepartmentKey = 0;
        private int _Rice1 = 0;
        private float _Price1;
        private float _Money1;
        private int _Rice2 = 0;
        private float _Price2;
        private float _Money2;
        private float _Money3;
        private float _Money4;
        private string _Description = "";
        private int _RecordStatus = 0;
        private DateTime _CreatedOn;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _RoleID = "";
        private string _Message = "";
        #endregion
        #region [ Properties ]
        public bool IsCommandOK
        {
            get
            {
                int zNumber = 0;
                int.TryParse(_Message.Substring(0, 1), out zNumber);
                if (zNumber <= 3) return true; else return false;
            }
        }

        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        public int Key
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public DateTime DateWrite
        {
            get { return _DateWrite; }
            set { _DateWrite = value; }
        }
        public int TeamKey
        {
            get { return _TeamKey; }
            set { _TeamKey = value; }
        }
        public string TeamID
        {
            get { return _TeamID; }
            set { _TeamID = value; }
        }
        public string TeamName
        {
            get { return _TeamName; }
            set { _TeamName = value; }
        }
        public int BranchKey
        {
            get { return _BranchKey; }
            set { _BranchKey = value; }
        }
        public int DepartmentKey
        {
            get { return _DepartmentKey; }
            set { _DepartmentKey = value; }
        }
        public int Rice1
        {
            get { return _Rice1; }
            set { _Rice1 = value; }
        }
        public float Price1
        {
            get { return _Price1; }
            set { _Price1 = value; }
        }
        public float Money1
        {
            get { return _Money1; }
            set { _Money1 = value; }
        }
        public int Rice2
        {
            get { return _Rice2; }
            set { _Rice2 = value; }
        }
        public float Price2
        {
            get { return _Price2; }
            set { _Price2 = value; }
        }
        public float Money2
        {
            get { return _Money2; }
            set { _Money2 = value; }
        }
        public float Money3
        {
            get { return _Money3; }
            set { _Money3 = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public float Money4
        {
            get
            {
                return _Money4;
            }

            set
            {
                _Money4 = value;
            }
        }
        #endregion
        #region [ Constructor Get Information ]
        public RiceTeamCompany_Info()
        {
        }
        public RiceTeamCompany_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM FNC_RiceTeamCompany WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    if (zReader["DateWrite"] != DBNull.Value)
                        _DateWrite = (DateTime)zReader["DateWrite"];
                    if (zReader["TeamKey"] != DBNull.Value)
                        _TeamKey = int.Parse(zReader["TeamKey"].ToString());
                    _TeamID = zReader["TeamID"].ToString().Trim();
                    _TeamName = zReader["TeamName"].ToString().Trim();
                    if (zReader["BranchKey"] != DBNull.Value)
                        _BranchKey = int.Parse(zReader["BranchKey"].ToString());
                    if (zReader["DepartmentKey"] != DBNull.Value)
                        _DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    if (zReader["Rice1"] != DBNull.Value)
                        _Rice1 = int.Parse(zReader["Rice1"].ToString());
                    if (zReader["Price1"] != DBNull.Value)
                        _Price1 = float.Parse(zReader["Price1"].ToString());
                    if (zReader["Money1"] != DBNull.Value)
                        _Money1 = float.Parse(zReader["Money1"].ToString());
                    if (zReader["Rice2"] != DBNull.Value)
                        _Rice2 = int.Parse(zReader["Rice2"].ToString());
                    if (zReader["Price2"] != DBNull.Value)
                        _Price2 = float.Parse(zReader["Price2"].ToString());
                    if (zReader["Money2"] != DBNull.Value)
                        _Money2 = float.Parse(zReader["Money2"].ToString());
                    if (zReader["Money3"] != DBNull.Value)
                        _Money3 = float.Parse(zReader["Money3"].ToString());
                    if (zReader["Money4"] != DBNull.Value)
                        _Money4 = float.Parse(zReader["Money4"].ToString());
                    _Description = zReader["Description"].ToString().Trim();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        public RiceTeamCompany_Info(string TeamID, DateTime DateTime)
        {
            DateTime zFromDate = new DateTime(DateTime.Year, DateTime.Month, DateTime.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(DateTime.Year, DateTime.Month, DateTime.Day, 23, 59, 59);
            string zSQL = "SELECT * FROM FNC_RiceTeamCompany WHERE TeamID = @TeamID AND DateWrite BETWEEN @FromDate AND @ToDate AND RecordStatus <> 99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@TeamID", SqlDbType.NVarChar).Value = TeamID.Trim();
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    if (zReader["DateWrite"] != DBNull.Value)
                        _DateWrite = (DateTime)zReader["DateWrite"];
                    if (zReader["TeamKey"] != DBNull.Value)
                        _TeamKey = int.Parse(zReader["TeamKey"].ToString());
                    _TeamID = zReader["TeamID"].ToString().Trim();
                    _TeamName = zReader["TeamName"].ToString().Trim();
                    if (zReader["BranchKey"] != DBNull.Value)
                        _BranchKey = int.Parse(zReader["BranchKey"].ToString());
                    if (zReader["DepartmentKey"] != DBNull.Value)
                        _DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    if (zReader["Rice1"] != DBNull.Value)
                        _Rice1 = int.Parse(zReader["Rice1"].ToString());
                    if (zReader["Price1"] != DBNull.Value)
                        _Price1 = float.Parse(zReader["Price1"].ToString());
                    if (zReader["Money1"] != DBNull.Value)
                        _Money1 = float.Parse(zReader["Money1"].ToString());
                    if (zReader["Rice2"] != DBNull.Value)
                        _Rice2 = int.Parse(zReader["Rice2"].ToString());
                    if (zReader["Price2"] != DBNull.Value)
                        _Price2 = float.Parse(zReader["Price2"].ToString());
                    if (zReader["Money2"] != DBNull.Value)
                        _Money2 = float.Parse(zReader["Money2"].ToString());
                    if (zReader["Money3"] != DBNull.Value)
                        _Money3 = float.Parse(zReader["Money3"].ToString());
                    if (zReader["Money4"] != DBNull.Value)
                        _Money4 = float.Parse(zReader["Money4"].ToString());
                    _Description = zReader["Description"].ToString().Trim();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO FNC_RiceTeamCompany ("
        + " DateWrite ,TeamKey ,TeamID ,TeamName ,BranchKey ,DepartmentKey ,Rice1 ,Price1 ,Money1 ,Rice2 ,Price2 ,Money2 ,Money3,Money4 ,Description ,RecordStatus ,CreatedOn ,CreatedBy ,CreatedName ,ModifiedOn ,ModifiedBy ,ModifiedName ) "
         + " VALUES ( "
         + "@DateWrite ,@TeamKey ,@TeamID ,@TeamName ,@BranchKey ,@DepartmentKey ,@Rice1 ,@Price1 ,@Money1 ,@Rice2 ,@Price2 ,@Money2 ,@Money3 ,@Money4 ,@Description ,0 ,GETDATE(),@CreatedBy ,@CreatedName ,GETDATE(),@ModifiedBy ,@ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (_DateWrite == DateTime.MinValue)
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = _DateWrite;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = _TeamKey;
                zCommand.Parameters.Add("@TeamID", SqlDbType.NVarChar).Value = _TeamID.Trim();
                zCommand.Parameters.Add("@TeamName", SqlDbType.NVarChar).Value = _TeamName.Trim();
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = _BranchKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@Rice1", SqlDbType.Int).Value = _Rice1;
                zCommand.Parameters.Add("@Price1", SqlDbType.Float).Value = _Price1;
                zCommand.Parameters.Add("@Money1", SqlDbType.Float).Value = _Money1;
                zCommand.Parameters.Add("@Rice2", SqlDbType.Int).Value = _Rice2;
                zCommand.Parameters.Add("@Price2", SqlDbType.Float).Value = _Price2;
                zCommand.Parameters.Add("@Money2", SqlDbType.Float).Value = _Money2;
                zCommand.Parameters.Add("@Money3", SqlDbType.Float).Value = _Money3;
                zCommand.Parameters.Add("@Money4", SqlDbType.Float).Value = _Money4;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE FNC_RiceTeamCompany SET "
                        + " DateWrite = @DateWrite,"
                        + " TeamKey = @TeamKey,"
                        + " TeamID = @TeamID,"
                        + " TeamName = @TeamName,"
                        + " BranchKey = @BranchKey,"
                        + " DepartmentKey = @DepartmentKey,"
                        + " Rice1 = @Rice1,"
                        + " Price1 = @Price1,"
                        + " Money1 = @Money1,"
                        + " Rice2 = @Rice2,"
                        + " Price2 = @Price2,"
                        + " Money2 = @Money2,"
                        + " Money3 = @Money3,"
                        + " Money4 = @Money4,"
                        + " Description = @Description,"
                        + " RecordStatus = 1,"
                        + " ModifiedOn = GETDATE(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                       + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                if (_DateWrite == DateTime.MinValue)
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = _DateWrite;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = _TeamKey;
                zCommand.Parameters.Add("@TeamID", SqlDbType.NVarChar).Value = _TeamID.Trim();
                zCommand.Parameters.Add("@TeamName", SqlDbType.NVarChar).Value = _TeamName.Trim();
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = _BranchKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@Rice1", SqlDbType.Int).Value = _Rice1;
                zCommand.Parameters.Add("@Price1", SqlDbType.Float).Value = _Price1;
                zCommand.Parameters.Add("@Money1", SqlDbType.Float).Value = _Money1;
                zCommand.Parameters.Add("@Rice2", SqlDbType.Int).Value = _Rice2;
                zCommand.Parameters.Add("@Price2", SqlDbType.Float).Value = _Price2;
                zCommand.Parameters.Add("@Money2", SqlDbType.Float).Value = _Money2;
                zCommand.Parameters.Add("@Money3", SqlDbType.Float).Value = _Money3;
                zCommand.Parameters.Add("@Money4", SqlDbType.Float).Value = _Money4;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE FNC_RiceTeamCompany SET [RecordStatus] = 99, [ModifiedOn] = GETDATE(), ModifiedBy = @ModifiedBy, ModifiedName = @ModifiedName WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_AutoKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        #endregion
    }
}
