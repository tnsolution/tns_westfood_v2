﻿using System;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;

namespace TNS.FIN
{
    public class Payment_Info
    {

        #region [ Field Name ]
        private string _PaymentKey = "";
        private string _PaymentID = "";
        private string _PaymentNumber = "";
        private DateTime _PaymentDate;
        private int _IsInternal = 0;
        private string _PaymentDescription = "";
        private double _AmountOrderMain = 0;
        private double _AmountOrderForeign = 0;
        private string _CurrencyID = "";
        private float _CurrencyRate;
        private double _AmountFeeBank = 0;
        private int _PayMethod = 0;
        private string _ParentTable = "";
        private string _ParentKey = "";
        private string _CustomerID = "";
        private string _CustomerName = "";
        private int _CategoryKey = 0;
        private string _CreditAccount = "";
        private string _DebitAccount = "";
        private string _DirectPayer = "";
        private int _Slug = 0;
        private string _Organization = "";
        private int _RecordStatus = 0;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _CreatedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private DateTime _ModifiedOn;
        private string _RoleID = "";
        private string _Message = "";
        private string _Note = "";
        #endregion
        #region [ Properties ]
        public bool IsCommandOK
        {
            get
            {
                if (_Message.Length > 0)
                {
                    int zNumber = 0;
                    int.TryParse(_Message.Substring(0, 1), out zNumber);
                    if (zNumber <= 3) return true; else return false;
                }
                else
                {
                    return false;
                }
            }
        }
        public bool HasInfo
        {
            get
            {
                if (_PaymentKey.Trim().Length > 0) return true; else return false;
            }
        }
        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        public string Key
        {
            get { return _PaymentKey; }
            set { _PaymentKey = value; }
        }
        public string PaymentID
        {
            get { return _PaymentID; }
            set { _PaymentID = value; }
        }
        public string PaymentNumber
        {
            get { return _PaymentNumber; }
            set { _PaymentNumber = value; }
        }
        public DateTime PaymentDate
        {
            get { return _PaymentDate; }
            set { _PaymentDate = value; }
        }
        public int IsInternal
        {
            get { return _IsInternal; }
            set { _IsInternal = value; }
        }
        public string PaymentDescription
        {
            get { return _PaymentDescription; }
            set { _PaymentDescription = value; }
        }
        public double AmountOrderMain
        {
            get { return _AmountOrderMain; }
            set { _AmountOrderMain = value; }
        }
        public double AmountOrderForeign
        {
            get { return _AmountOrderForeign; }
            set { _AmountOrderForeign = value; }
        }
        public string CurrencyID
        {
            get { return _CurrencyID; }
            set { _CurrencyID = value; }
        }
        public float CurrencyRate
        {
            get { return _CurrencyRate; }
            set { _CurrencyRate = value; }
        }
        public double AmountFeeBank
        {
            get { return _AmountFeeBank; }
            set { _AmountFeeBank = value; }
        }
        public int PayMethod
        {
            get { return _PayMethod; }
            set { _PayMethod = value; }
        }
        public string ParentTable
        {
            get { return _ParentTable; }
            set { _ParentTable = value; }
        }
        public string ParentKey
        {
            get { return _ParentKey; }
            set { _ParentKey = value; }
        }
        public string CustomerID
        {
            get { return _CustomerID; }
            set { _CustomerID = value; }
        }
        public string CustomerName
        {
            get { return _CustomerName; }
            set { _CustomerName = value; }
        }
        public int CategoryKey
        {
            get { return _CategoryKey; }
            set { _CategoryKey = value; }
        }
        public string CreditAccount
        {
            get { return _CreditAccount; }
            set { _CreditAccount = value; }
        }
        public string DebitAccount
        {
            get { return _DebitAccount; }
            set { _DebitAccount = value; }
        }
        public string DirectPayer
        {
            get { return _DirectPayer; }
            set { _DirectPayer = value; }
        }
        public int Slug
        {
            get { return _Slug; }
            set { _Slug = value; }
        }
        public string Organization
        {
            get { return _Organization; }
            set { _Organization = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public string Note
        {
            get
            {
                return _Note;
            }

            set
            {
                _Note = value;
            }
        }
        #endregion
        #region [ Constructor Get Information ]
        public Payment_Info()
        {
        }

        public Payment_Info(string PaymentKey)
        {
            string zSQL = @"SELECT *, dbo.Fn_GetCustomerID(A.ParentKey) as CustomerID, CASE 
	WHEN B.Slug = 1 THEN dbo.Fn_GetFullName(A.ParentKey)
	WHEN B.Slug = 2 THEN dbo.Fn_GetCompanyName(A.ParentKey)
END AS CustomerName 
FROM FNC_Payment A LEFT JOIN CRM_Customer B ON A.ParentKey = B.CustomerKey WHERE A.PaymentKey = @PaymentKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@PaymentKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(PaymentKey);
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _PaymentKey = zReader["PaymentKey"].ToString();
                    _PaymentID = zReader["PaymentID"].ToString().Trim();
                    _PaymentNumber = zReader["PaymentNumber"].ToString().Trim();
                    _CustomerID = zReader["CustomerID"].ToString().Trim();
                    _CustomerName = zReader["CustomerName"].ToString().Trim();
                    if (zReader["PaymentDate"] != DBNull.Value)
                        _PaymentDate = (DateTime)zReader["PaymentDate"];
                    if (zReader["IsInternal"] != DBNull.Value)
                        _IsInternal = int.Parse(zReader["IsInternal"].ToString());
                    _PaymentDescription = zReader["PaymentDescription"].ToString().Trim();
                    if (zReader["AmountOrderMain"] != DBNull.Value)
                        _AmountOrderMain = double.Parse(zReader["AmountOrderMain"].ToString());
                    if (zReader["AmountOrderForeign"] != DBNull.Value)
                        _AmountOrderForeign = double.Parse(zReader["AmountOrderForeign"].ToString());
                    _CurrencyID = zReader["CurrencyID"].ToString().Trim();
                    if (zReader["CurrencyRate"] != DBNull.Value)
                        _CurrencyRate = float.Parse(zReader["CurrencyRate"].ToString());
                    if (zReader["AmountFeeBank"] != DBNull.Value)
                        _AmountFeeBank = double.Parse(zReader["AmountFeeBank"].ToString());
                    if (zReader["PayMethod"] != DBNull.Value)
                        _PayMethod = int.Parse(zReader["PayMethod"].ToString());
                    _ParentTable = zReader["ParentTable"].ToString().Trim();
                    _ParentKey = zReader["ParentKey"].ToString().Trim();
                    if (zReader["CategoryKey"] != DBNull.Value)
                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    _CreditAccount = zReader["CreditAccount"].ToString().Trim();
                    _DebitAccount = zReader["DebitAccount"].ToString().Trim();
                    _DirectPayer = zReader["DirectPayer"].ToString().Trim();
                    if (zReader["Slug"] != DBNull.Value)
                        _Slug = int.Parse(zReader["Slug"].ToString());
                    _Organization = zReader["Organization"].ToString().Trim();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _Note = zReader["Note"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }

        public void Payment_Employee(string PaymentKey)
        {
            string zSQL = @" SELECT A.*
FROM FNC_Payment A
 LEFT JOIN  dbo.HRM_Employee B ON A.ParentKey = B.EmployeeKey
WHERE A.RecordStatus <> 99 AND  A.ParentKey !='' AND A.PaymentKey = @PaymentKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@PaymentKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(PaymentKey);
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _PaymentKey = zReader["PaymentKey"].ToString();
                    _PaymentID = zReader["PaymentID"].ToString().Trim();
                    _PaymentNumber = zReader["PaymentNumber"].ToString().Trim();
                    if (zReader["PaymentDate"] != DBNull.Value)
                        _PaymentDate = (DateTime)zReader["PaymentDate"];
                    if (zReader["IsInternal"] != DBNull.Value)
                        _IsInternal = int.Parse(zReader["IsInternal"].ToString());
                    _PaymentDescription = zReader["PaymentDescription"].ToString().Trim();
                    if (zReader["AmountOrderMain"] != DBNull.Value)
                        _AmountOrderMain = double.Parse(zReader["AmountOrderMain"].ToString());
                    if (zReader["AmountOrderForeign"] != DBNull.Value)
                        _AmountOrderForeign = double.Parse(zReader["AmountOrderForeign"].ToString());
                    _CurrencyID = zReader["CurrencyID"].ToString().Trim();
                    if (zReader["CurrencyRate"] != DBNull.Value)
                        _CurrencyRate = float.Parse(zReader["CurrencyRate"].ToString());
                    if (zReader["AmountFeeBank"] != DBNull.Value)
                        _AmountFeeBank = double.Parse(zReader["AmountFeeBank"].ToString());
                    if (zReader["PayMethod"] != DBNull.Value)
                        _PayMethod = int.Parse(zReader["PayMethod"].ToString());
                    _ParentTable = zReader["ParentTable"].ToString().Trim();
                    _ParentKey = zReader["ParentKey"].ToString().Trim();
                    if (zReader["CategoryKey"] != DBNull.Value)
                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    _CreditAccount = zReader["CreditAccount"].ToString().Trim();
                    _DebitAccount = zReader["DebitAccount"].ToString().Trim();
                    _DirectPayer = zReader["DirectPayer"].ToString().Trim();
                    if (zReader["Slug"] != DBNull.Value)
                        _Slug = int.Parse(zReader["Slug"].ToString());
                    _Organization = zReader["Organization"].ToString().Trim();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _Note = zReader["Note"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "FNC_Payment_INSERT";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@PaymentID", SqlDbType.NVarChar).Value = _PaymentID.Trim();
                zCommand.Parameters.Add("@PaymentNumber", SqlDbType.NChar).Value = _PaymentNumber.Trim();
                if (_PaymentDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@PaymentDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@PaymentDate", SqlDbType.DateTime).Value = _PaymentDate;
                zCommand.Parameters.Add("@IsInternal", SqlDbType.Int).Value = _IsInternal;
                zCommand.Parameters.Add("@PaymentDescription", SqlDbType.NVarChar).Value = _PaymentDescription.Trim();
                zCommand.Parameters.Add("@AmountOrderMain", SqlDbType.Money).Value = _AmountOrderMain;
                zCommand.Parameters.Add("@AmountOrderForeign", SqlDbType.Money).Value = _AmountOrderForeign;
                zCommand.Parameters.Add("@CurrencyID", SqlDbType.NVarChar).Value = _CurrencyID.Trim();
                zCommand.Parameters.Add("@CurrencyRate", SqlDbType.Float).Value = _CurrencyRate;
                zCommand.Parameters.Add("@AmountFeeBank", SqlDbType.Money).Value = _AmountFeeBank;
                zCommand.Parameters.Add("@PayMethod", SqlDbType.Int).Value = _PayMethod;
                zCommand.Parameters.Add("@ParentTable", SqlDbType.NVarChar).Value = _ParentTable.Trim();
                zCommand.Parameters.Add("@ParentKey", SqlDbType.NVarChar).Value = _ParentKey.Trim();
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@CreditAccount", SqlDbType.NVarChar).Value = _CreditAccount.Trim();
                zCommand.Parameters.Add("@DebitAccount", SqlDbType.NVarChar).Value = _DebitAccount.Trim();
                zCommand.Parameters.Add("@DirectPayer", SqlDbType.NVarChar).Value = _DirectPayer.Trim();
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = _Slug;
                zCommand.Parameters.Add("@Organization", SqlDbType.NVarChar).Value = _Organization.Trim();

                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zCommand.Parameters.Add("@Note", SqlDbType.NVarChar).Value = _Note.Trim();
                _Message = zCommand.ExecuteScalar().ToString();
                if (_Message.Substring(0, 2) == "11")
                    _PaymentKey = _Message.Substring(2, 36);
                else
                    _PaymentKey = "";
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "FNC_Payment_UPDATE";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                if (_PaymentKey.Length == 36)
                    zCommand.Parameters.Add("@PaymentKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_PaymentKey);
                else
                    zCommand.Parameters.Add("@PaymentKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@PaymentID", SqlDbType.NVarChar).Value = _PaymentID.Trim();
                zCommand.Parameters.Add("@PaymentNumber", SqlDbType.NChar).Value = _PaymentNumber.Trim();
                if (_PaymentDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@PaymentDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@PaymentDate", SqlDbType.DateTime).Value = _PaymentDate;
                zCommand.Parameters.Add("@IsInternal", SqlDbType.Int).Value = _IsInternal;
                zCommand.Parameters.Add("@PaymentDescription", SqlDbType.NVarChar).Value = _PaymentDescription.Trim();
                zCommand.Parameters.Add("@AmountOrderMain", SqlDbType.Money).Value = _AmountOrderMain;
                zCommand.Parameters.Add("@AmountOrderForeign", SqlDbType.Money).Value = _AmountOrderForeign;
                zCommand.Parameters.Add("@CurrencyID", SqlDbType.NVarChar).Value = _CurrencyID.Trim();
                zCommand.Parameters.Add("@CurrencyRate", SqlDbType.Float).Value = _CurrencyRate;
                zCommand.Parameters.Add("@AmountFeeBank", SqlDbType.Money).Value = _AmountFeeBank;
                zCommand.Parameters.Add("@PayMethod", SqlDbType.Int).Value = _PayMethod;
                zCommand.Parameters.Add("@ParentTable", SqlDbType.NVarChar).Value = _ParentTable.Trim();
                zCommand.Parameters.Add("@ParentKey", SqlDbType.NVarChar).Value = _ParentKey.Trim();
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@CreditAccount", SqlDbType.NVarChar).Value = _CreditAccount.Trim();
                zCommand.Parameters.Add("@DebitAccount", SqlDbType.NVarChar).Value = _DebitAccount.Trim();
                zCommand.Parameters.Add("@DirectPayer", SqlDbType.NVarChar).Value = _DirectPayer.Trim();
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = _Slug;
                zCommand.Parameters.Add("@Organization", SqlDbType.NVarChar).Value = _Organization.Trim();

                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zCommand.Parameters.Add("@Note", SqlDbType.NVarChar).Value = _Note.Trim();
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "FNC_Payment_DELETE";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@PaymentKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_PaymentKey);
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_PaymentKey.Trim().Length == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        #endregion
    }
}
