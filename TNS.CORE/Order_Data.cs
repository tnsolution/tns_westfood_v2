﻿using System;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
using TNS.Misc;

namespace TNS.CORE
{
    public class Order_Data
    {
        public static int CountOrderIDFollow(string OrderID_Follow)
        {
            int zResult = 0;
            string zSQL = @"DECLARE  @count INT =0
SELECT @count +=COUNT(*) FROM dbo.IVT_Stock_Input WHERE OrderID= @OrderID_Follow AND  RecordStatus!=99
SELECT @count +=COUNT(*) FROM dbo.IVT_Stock_Output WHERE OrderID= @OrderID_Follow AND RecordStatus!=99
SELECT @count";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderID_Follow", SqlDbType.NVarChar).Value = OrderID_Follow;
                zResult = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
        public static DataTable List_Search(DateTime FromDate, DateTime ToDate, string ProductID, string StageID, string CompareID, string OrderID, int TeamKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.*, 
dbo.FTR_Order_SumRate(A.OrderKey) AS Rate,
E.TeamID, E.TeamName, C.StageKey, C.StagesID, C.StageName, C.Price AS StagePrice,
ISNULL(dbo.LayLoaiHang(A.OrderIDFollow,A.ProductKey),N'5.KHÁC') AS ProductType,
[dbo].[FTR_Order_TatCaTenHeSoCuaDonHang](A.OrderKey)AS RateID
FROM FTR_Order A
LEFT JOIN [dbo].[IVT_Product_Stages] C ON A.Category_Stage = C.StageKey
LEFT JOIN [dbo].[SYS_Team] E ON E.TeamKey=A.TeamKey
WHERE A.OrderDate BETWEEN @FromDate AND @ToDate AND A.RecordStatus != 99";
            if (TeamKey > 0)
            {
                zSQL += " AND A.TeamKey = @TeamKey";
            }
            if (ProductID.Length > 0)
            {
                zSQL += " AND A.ProductID = @ProductID";
            }
            if (StageID.Length > 0)
            {
                zSQL += " AND C.StagesID = @StagesID";
            }
            if (CompareID != "")
            {
                zSQL += " AND A.OrderID_Compare LIKE @OrderID_Compare";
            }
            if (OrderID != "")
            {
                zSQL += " AND A.OrderID LIKE  @OrderID";
            }
            zSQL += " ORDER BY A.OrderDate ASC , A.OrderID ASC ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@ProductID", SqlDbType.NVarChar).Value = ProductID;
                zCommand.Parameters.Add("@StagesID", SqlDbType.NVarChar).Value = StageID;
                zCommand.Parameters.Add("@OrderID_Compare", SqlDbType.NVarChar).Value = "%"+CompareID+"%";
                zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = "%"+ OrderID+"%";
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        
        public static DataTable List(int TeamKey, DateTime FromDate, DateTime ToDate, string ProductKey, int StageKey, int CategoryKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.OrderKey,A.OrderID,A.WorkStatus,A.OrderDate,
A.ProductName,C.StageName,A.QuantityReality,SUM(D.Rate) AS Rate ,A.Note, C.Price
FROM FTR_Order A
LEFT JOIN [dbo].[FTR_Order_Rate] B ON A.OrderKey = B.OrderKey
LEFT JOIN [dbo].[IVT_Product_Stages] C ON A.Category_Stage = C.StageKey
LEFT JOIN [dbo].[FTR_Category] D ON B.CategoryKey = D.AutoKey  
WHERE A.OrderDate BETWEEN @FromDate AND @ToDate AND A.RecordStatus != 99 AND B.RecordStatus != 99";
            if (TeamKey > 0)
            {
                zSQL += " AND A.TeamKey = @TeamKey";
            }
            if (ProductKey.Length > 0)
            {
                zSQL += " AND ProductKey = @ProductKey";
            }
            if (StageKey > 0)
            {
                zSQL += " AND Category_Stage = @StageKey";
            }
            if (CategoryKey > 0)
            {
                zSQL += " AND B.CategoryKey = @CategoryKey";
            }
            zSQL += @" GROUP BY A.OrderKey,A.OrderID,A.WorkStatus,A.OrderDate, 
A.ProductName,C.StageName,A.QuantityReality,A.Note,C.Price  ";
            zSQL += " ORDER BY A.OrderDate ASC , A.OrderID ASC ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@ProductKey", SqlDbType.NVarChar).Value = ProductKey;
                zCommand.Parameters.Add("@StageKey", SqlDbType.Int).Value = StageKey;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable List_Stages(int TeamKey, DateTime FromDate)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 23, 0, 0);
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT StageKey,StageName FROM FTR_Order A
LEFT JOIN IVT_Product_StageS B ON B.StageKey = A.Category_Stage
WHERE A.TeamKey = @TeamKey AND A.OrderDate BETWEEN @FromDate AND @ToDate
AND A.RecordStatus < 99 AND A.WorkStatus = 1
GROUP BY StageKey,StageName
ORDER BY StageName ASC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List_Money(int TeamKey, int Category, DateTime FromDate)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 23, 0, 0);
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT OrderKey,OrderID,WorkStatus,B.Price,ROUND(QuantityReality,1) AS QuantityReality,
[dbo].[FTR_Order_Employee_Basket](OrderKey) AS Basket,
[dbo].[FTR_Order_Employee_Kilogram](OrderKey) AS Kilogram,
[dbo].[FTR_Order_Employee_Time](OrderKey) AS Time,
[dbo].[FTR_Order_SumRate](OrderKey) AS Rate
 FROM [dbo].[FTR_Order] A
 LEFT JOIN [dbo].[IVT_Product_Stages] B ON A.Category_Stage = B.StageKey
WHERE A.RecordStatus < 99 
AND A.TeamKey = @TeamKey 
AND OrderDate BETWEEN @FromDate AND @ToDate ";
            if (Category > 0)
            {
                zSQL += "AND Category_Stage = @StageKey ";
            }
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@StageKey", SqlDbType.Int).Value = Category;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static int FindOrderID(string OrderID)
        {
            int zResult = 0;
            string zSQL = @"SELECT COUNT(*) FROM FTR_Order WHERE OrderID = @OrderID AND RecordStatus < 99";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = OrderID;
                zResult = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
        public static int FindOrderID_Compare(string OrderID_Compare)
        {
            int zResult = 0;
            string zSQL = @"SELECT COUNT(*) FROM FTR_Order WHERE OrderID_Compare = @OrderID_Compare AND RecordStatus < 99";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderID_Compare", SqlDbType.NVarChar).Value = OrderID_Compare;
                zResult = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
        public static string FindOrder(string UserKey)
        {
            string zResult = "";
            string zSQL = @"SELECT TOP 1 (OrderID) FROM [dbo].[FTR_Order] WHERE RecordStatus < 99 AND CreatedBy = @CreatedBy
ORDER BY CreatedOn DESC";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = UserKey;
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
        public static DataTable List_General(int TeamKey, DateTime FromDate)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 23, 59, 59);
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.OrderKey,B.OrderID,Status,A.StageKey,StageName,Price FROM [dbo].[FTR_Order_Money] A
LEFT JOIN [dbo].[FTR_Order] B ON A.OrderKey = B.OrderKey
LEFT JOIN [dbo].[IVT_Product_Stages] C ON B.Category_Stage = C.StageKey
LEFT JOIN [dbo].[FTR_Order_Log] D ON A.OrderKey = D.OrderKey
WHERE A.TeamKey = @TeamKey AND A.RecordStatus < 99 AND B.RecordStatus < 99
AND B.OrderDate BETWEEN @FromDate AND @ToDate
AND Money_Eat > 0
GROUP BY A.OrderKey,B.OrderID,Status,A.StageKey,StageName,Price";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List_Employee(string OrderKey, string EmployeeKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT B.EmployeeKey,B.EmployeeID,C.FullName,B.Basket,
B.Kilogram,[dbo].[FTR_SoLuongThanhPham](B.EmployeeKey,@OrderKey) AS KgProduct,B.Time,B.Money 
FROM [dbo].[FTR_Order] A 
LEFT JOIN [dbo].[FTR_Order_Employee] B ON A.OrderKey = B.OrderKey
LEFT JOIN [dbo].[SYS_Personal] C ON B.EmployeeKey = C.ParentKey
LEFT JOIN [dbo].[FTR_Order_Money] D ON A.OrderKey = D.OrderKey
WHERE A.OrderKey = @OrderKey AND A.RecordStatus < 99 AND B.RecordStatus < 99 ";
            if (EmployeeKey.Length > 2)
            {
                zSQL += " AND B.EmployeeKey = @EmployeeKey ";
            }
            zSQL += @" GROUP BY B.EmployeeKey,B.EmployeeID,C.FullName,B.Basket,B.Kilogram,B.Time,B.Money";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = OrderKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable List_Compare(DateTime FromDate, DateTime ToDate)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT OrderID_Compare, OrderID_Compare 
FROM dbo.FTR_Order 
WHERE RecordStatus < 99  
AND OrderDate BETWEEN @FromDate AND @ToDate  
GROUP BY OrderID_Compare";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable List_Compare_Productivity(DateTime FromDate, DateTime ToDate, string CompareID)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.OrderKey,A.OrderID,A.WorkStatus,A.OrderDate,
A.ProductName,C.StageName,A.QuantityReality,SUM(D.Rate) AS Rate ,A.Note, C.Price
FROM FTR_Order A
LEFT JOIN [dbo].[FTR_Order_Rate] B ON A.OrderKey = B.OrderKey
LEFT JOIN [dbo].[IVT_Product_Stages] C ON A.Category_Stage = C.StageKey
LEFT JOIN [dbo].[FTR_Category] D ON B.CategoryKey = D.AutoKey  
WHERE A.OrderDate BETWEEN @FromDate AND @ToDate 
AND A.RecordStatus != 99 
AND B.RecordStatus != 99 
AND A.OrderID_Compare = @OrderID_Compare";
            zSQL += @" GROUP BY A.OrderKey,A.OrderID,A.WorkStatus,A.OrderDate, 
A.ProductName,C.StageName,A.QuantityReality,A.Note,C.Price  ";
            zSQL += " ORDER BY A.OrderDate ASC , A.OrderID ASC ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@OrderID_Compare", SqlDbType.NVarChar).Value = CompareID.Trim();
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        //Phần xem phân bổ thời gian
        public static DataTable List_OrderCompare(DateTime FromDate, DateTime ToDate, string OrderIDCompare, int TeamKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT  OrderDate,OrderID_Compare,WorkName,OrderTime,
SUM(OrderKg) AS OrderKg,SUM(WorkTime) AS WorkTime  FROM [dbo].[FTR_Order_Extend]
WHERE RecordStatus <>99 AND  OrderID_Compare IS NOT NULL 
AND OrderDate BETWEEN @FromDate AND @ToDate";
            if(OrderIDCompare.Trim().Length >0)
            {
                zSQL += " AND OrderID_Compare =@OrderIDCompare";
            }
            if(TeamKey>0)
            {
                zSQL += " AND TeamKey =@TeamKey ";
            }
            zSQL+= @" 
GROUP BY OrderDate,OrderID_Compare,OrderTime,WorkName
ORDER BY OrderDate,OrderID_Compare";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@OrderIDCompare", SqlDbType.NVarChar).Value = OrderIDCompare.Trim();
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable List_Order(string OrderIDCompare)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"      
SELECT OrderKey,OrderDate,OrderID,OrderKg,OrderTime,WorkTime,ProductID,ProductName FROM  [dbo].[FTR_Order_Extend]
WHERE OrderID_Compare = @OrderIDCompare AND RecordStatus <> 99
ORDER BY OrderID";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderIDCompare", SqlDbType.NVarChar).Value = OrderIDCompare.Trim();
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable List_ExtentEmployee(string OrderKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
            SELECT * FROM FTR_Order_Extend_Employee WHERE OrderKey = @OrderKey AND RecordStatus <> 99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = OrderKey.Trim();
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static string  FindOrderKey(string OrderID)
        {
            string zResult = "";
            string zSQL = @"SELECT OrderKey FROM FTR_Order WHERE OrderID = @OrderID AND RecordStatus < 99";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = OrderID.Trim();
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
    }
}
