﻿using System;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;

namespace TNS.CORE
{
    public class Order_Money_Info
    {

        #region [ Field Name ]
        private int _AutoKey = 0;
        private int _TeamKey = 0;
        private string _OrderKey = "";
        private string _EmployeeKey = "";
        private string _EmployeeName = "";
        private string _EmployeeID = "";
        private DateTime _OrderDate;
        private int _StageKey = 0;
        private float _Basket;
        private float _Kg;
        private float _Time;
        private float _Money;
        private int _Category = 0;
        private float _Basket_Borrow;
        private float _Kg_Borrow;
        private float _Time_Borrow;
        private float _Money_Borrow;
        private int _Category_Borrow = 0;
        private float _Basket_Eat;
        private float _Kg_Eat;
        private float _KgEat;
        private float _Time_Eat;
        private float _Money_Eat;
        private float _TimeEat;
        private float _MoneyEat;
        private int _Category_Eat = 0;
        private float _TimePrivate;
        private float _MoneyPrivate;
        private float _KgPrivate;
        private float _BasketPrivate;
        private int _CategoryPrivate = 0;
        private double _Money_Dif = 0;
        private double _Money_Dif_Sunday = 0;
        private double _Money_Dif_Holiday = 0;
        private double _Money_Dif_General = 0;
        private double _Money_Dif_Sunday_General = 0;
        private double _Money_Dif_Holiday_General = 0;
        private int _RecordStatus = 0;
        private DateTime _CreatedOn;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _RoleID = "";
        private string _Message = "";
        #endregion

        #region [ Properties ]
        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        public int Key
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public int TeamKey
        {
            get { return _TeamKey; }
            set { _TeamKey = value; }
        }
        public string EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public string EmployeeName
        {
            get { return _EmployeeName; }
            set { _EmployeeName = value; }
        }
        public string EmployeeID
        {
            get { return _EmployeeID; }
            set { _EmployeeID = value; }
        }
        public DateTime OrderDate
        {
            get { return _OrderDate; }
            set { _OrderDate = value; }
        }
        public int StageKey
        {
            get { return _StageKey; }
            set { _StageKey = value; }
        }
        public float Basket
        {
            get { return _Basket; }
            set { _Basket = value; }
        }
        public float Kg
        {
            get { return _Kg; }
            set { _Kg = value; }
        }
        public float Time
        {
            get { return _Time; }
            set { _Time = value; }
        }
        public float Money
        {
            get { return _Money; }
            set { _Money = value; }
        }
        public int Category
        {
            get { return _Category; }
            set { _Category = value; }
        }
        public float Basket_Borrow
        {
            get { return _Basket_Borrow; }
            set { _Basket_Borrow = value; }
        }
        public float Kg_Borrow
        {
            get { return _Kg_Borrow; }
            set { _Kg_Borrow = value; }
        }
        public float Time_Borrow
        {
            get { return _Time_Borrow; }
            set { _Time_Borrow = value; }
        }
        public float Money_Borrow
        {
            get { return _Money_Borrow; }
            set { _Money_Borrow = value; }
        }
        public int Category_Borrow
        {
            get { return _Category_Borrow; }
            set { _Category_Borrow = value; }
        }
        public float Basket_Eat
        {
            get { return _Basket_Eat; }
            set { _Basket_Eat = value; }
        }
        public float Kg_Eat
        {
            get { return _Kg_Eat; }
            set { _Kg_Eat = value; }
        }
        public float Time_Eat
        {
            get { return _Time_Eat; }
            set { _Time_Eat = value; }
        }
        public float Money_Eat
        {
            get { return _Money_Eat; }
            set { _Money_Eat = value; }
        }
        public int Category_Eat
        {
            get { return _Category_Eat; }
            set { _Category_Eat = value; }
        }
        public float TimePrivate
        {
            get { return _TimePrivate; }
            set { _TimePrivate = value; }
        }
        public float MoneyPrivate
        {
            get { return _MoneyPrivate; }
            set { _MoneyPrivate = value; }
        }
        public float KgPrivate
        {
            get { return _KgPrivate; }
            set { _KgPrivate = value; }
        }
        public float BasketPrivate
        {
            get { return _BasketPrivate; }
            set { _BasketPrivate = value; }
        }
        public int CategoryPrivate
        {
            get { return _CategoryPrivate; }
            set { _CategoryPrivate = value; }
        }
        public double Money_Dif
        {
            get { return _Money_Dif; }
            set { _Money_Dif = value; }
        }
        public double Money_Dif_Sunday
        {
            get { return _Money_Dif_Sunday; }
            set { _Money_Dif_Sunday = value; }
        }
        public double Money_Dif_Holiday
        {
            get { return _Money_Dif_Holiday; }
            set { _Money_Dif_Holiday = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public string OrderKey
        {
            get
            {
                return _OrderKey;
            }

            set
            {
                _OrderKey = value;
            }
        }

        public int RecordStatus
        {
            get
            {
                return _RecordStatus;
            }

            set
            {
                _RecordStatus = value;
            }
        }

        public double Money_Dif_General
        {
            get
            {
                return _Money_Dif_General;
            }

            set
            {
                _Money_Dif_General = value;
            }
        }

        public double Money_Dif_Sunday_General
        {
            get
            {
                return _Money_Dif_Sunday_General;
            }

            set
            {
                _Money_Dif_Sunday_General = value;
            }
        }

        public double Money_Dif_Holiday_General
        {
            get
            {
                return _Money_Dif_Holiday_General;
            }

            set
            {
                _Money_Dif_Holiday_General = value;
            }
        }

        public float KgEat
        {
            get
            {
                return _KgEat;
            }

            set
            {
                _KgEat = value;
            }
        }

        public float MoneyEat
        {
            get
            {
                return _MoneyEat;
            }

            set
            {
                _MoneyEat = value;
            }
        }

        public float TimeEat
        {
            get
            {
                return _TimeEat;
            }

            set
            {
                _TimeEat = value;
            }
        }
        #endregion

        #region [ Constructor Get Information ]
        public Order_Money_Info()
        {
        }
        public Order_Money_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM FTR_Order_Money WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    if (zReader["TeamKey"] != DBNull.Value)
                        _TeamKey = int.Parse(zReader["TeamKey"].ToString());
                    _EmployeeKey = zReader["EmployeeKey"].ToString().Trim();
                    _EmployeeName = zReader["EmployeeName"].ToString().Trim();
                    _EmployeeID = zReader["EmployeeID"].ToString().Trim();
                    if (zReader["OrderDate"] != DBNull.Value)
                        _OrderDate = (DateTime)zReader["OrderDate"];
                    if (zReader["StageKey"] != DBNull.Value)
                        _StageKey = int.Parse(zReader["StageKey"].ToString());
                    if (zReader["Basket"] != DBNull.Value)
                        _Basket = float.Parse(zReader["Basket"].ToString());
                    if (zReader["Kg"] != DBNull.Value)
                        _Kg = float.Parse(zReader["Kg"].ToString());
                    if (zReader["Time"] != DBNull.Value)
                        _Time = float.Parse(zReader["Time"].ToString());
                    if (zReader["Money"] != DBNull.Value)
                        _Money = float.Parse(zReader["Money"].ToString());
                    if (zReader["Category"] != DBNull.Value)
                        _Category = int.Parse(zReader["Category"].ToString());
                    if (zReader["Basket_Borrow"] != DBNull.Value)
                        _Basket_Borrow = float.Parse(zReader["Basket_Borrow"].ToString());
                    if (zReader["Kg_Borrow"] != DBNull.Value)
                        _Kg_Borrow = float.Parse(zReader["Kg_Borrow"].ToString());
                    if (zReader["Time_Borrow"] != DBNull.Value)
                        _Time_Borrow = float.Parse(zReader["Time_Borrow"].ToString());
                    if (zReader["Money_Borrow"] != DBNull.Value)
                        _Money_Borrow = float.Parse(zReader["Money_Borrow"].ToString());
                    if (zReader["Category_Borrow"] != DBNull.Value)
                        _Category_Borrow = int.Parse(zReader["Category_Borrow"].ToString());
                    if (zReader["Basket_Eat"] != DBNull.Value)
                        _Basket_Eat = float.Parse(zReader["Basket_Eat"].ToString());
                    if (zReader["Kg_Eat"] != DBNull.Value)
                        _Kg_Eat = float.Parse(zReader["Kg_Eat"].ToString());
                    if (zReader["Time_Eat"] != DBNull.Value)
                        _Time_Eat = float.Parse(zReader["Time_Eat"].ToString());
                    if (zReader["Money_Eat"] != DBNull.Value)
                        _Money_Eat = float.Parse(zReader["Money_Eat"].ToString());
                    if (zReader["Category_Eat"] != DBNull.Value)
                        _Category_Eat = int.Parse(zReader["Category_Eat"].ToString());
                    if (zReader["TimePrivate"] != DBNull.Value)
                        _TimePrivate = float.Parse(zReader["TimePrivate"].ToString());
                    if (zReader["MoneyPrivate"] != DBNull.Value)
                        _MoneyPrivate = float.Parse(zReader["MoneyPrivate"].ToString());
                    if (zReader["KgPrivate"] != DBNull.Value)
                        _KgPrivate = float.Parse(zReader["KgPrivate"].ToString());
                    if (zReader["BasketPrivate"] != DBNull.Value)
                        _BasketPrivate = float.Parse(zReader["BasketPrivate"].ToString());
                    if (zReader["CategoryPrivate"] != DBNull.Value)
                        _CategoryPrivate = int.Parse(zReader["CategoryPrivate"].ToString());
                    if (zReader["Money_Dif"] != DBNull.Value)
                        _Money_Dif = double.Parse(zReader["Money_Dif"].ToString());
                    if (zReader["Money_Dif_Sunday"] != DBNull.Value)
                        _Money_Dif_Sunday = double.Parse(zReader["Money_Dif_Sunday"].ToString());
                    if (zReader["Money_Dif_Holiday"] != DBNull.Value)
                        _Money_Dif_Holiday = double.Parse(zReader["Money_Dif_Holiday"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "FTR_Order_Money_INSERT";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = _TeamKey;
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = _OrderKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = _EmployeeKey.Trim();
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName.Trim();
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID.Trim();
                if (_OrderDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = _OrderDate;
                zCommand.Parameters.Add("@StageKey", SqlDbType.Int).Value = _StageKey;
                zCommand.Parameters.Add("@Basket", SqlDbType.Float).Value = _Basket;
                zCommand.Parameters.Add("@Kg", SqlDbType.Float).Value = _Kg;
                zCommand.Parameters.Add("@Time", SqlDbType.Float).Value = _Time;
                zCommand.Parameters.Add("@Money", SqlDbType.Float).Value = _Money;
                zCommand.Parameters.Add("@Category", SqlDbType.Int).Value = _Category;
                zCommand.Parameters.Add("@Basket_Borrow", SqlDbType.Float).Value = _Basket_Borrow;
                zCommand.Parameters.Add("@Kg_Borrow", SqlDbType.Float).Value = _Kg_Borrow;
                zCommand.Parameters.Add("@Time_Borrow", SqlDbType.Float).Value = _Time_Borrow;
                zCommand.Parameters.Add("@Money_Borrow", SqlDbType.Float).Value = _Money_Borrow;
                zCommand.Parameters.Add("@Category_Borrow", SqlDbType.Int).Value = _Category_Borrow;
                zCommand.Parameters.Add("@Basket_Eat", SqlDbType.Float).Value = _Basket_Eat;
                zCommand.Parameters.Add("@Kg_Eat", SqlDbType.Float).Value = _Kg_Eat;
                zCommand.Parameters.Add("@Time_Eat", SqlDbType.Float).Value = _Time_Eat;
                zCommand.Parameters.Add("@Money_Eat", SqlDbType.Float).Value = _Money_Eat;
                zCommand.Parameters.Add("@Category_Eat", SqlDbType.Int).Value = _Category_Eat;
                zCommand.Parameters.Add("@TimePrivate", SqlDbType.Float).Value = _TimePrivate;
                zCommand.Parameters.Add("@MoneyPrivate", SqlDbType.Float).Value = _MoneyPrivate;
                zCommand.Parameters.Add("@KgPrivate", SqlDbType.Float).Value = _KgPrivate;
                zCommand.Parameters.Add("@BasketPrivate", SqlDbType.Float).Value = _BasketPrivate;
                zCommand.Parameters.Add("@CategoryPrivate", SqlDbType.Int).Value = _CategoryPrivate;
                zCommand.Parameters.Add("@Money_Dif_Private", SqlDbType.Money).Value = _Money_Dif;
                zCommand.Parameters.Add("@Money_Dif_Sunday_Private", SqlDbType.Money).Value = _Money_Dif_Sunday;
                zCommand.Parameters.Add("@Money_Dif_Holiday_Private", SqlDbType.Money).Value = _Money_Dif_Holiday;
                zCommand.Parameters.Add("@Money_Dif_General", SqlDbType.Money).Value = _Money_Dif_General;
                zCommand.Parameters.Add("@Money_Dif_Sunday_General", SqlDbType.Money).Value = _Money_Dif_Sunday_General;
                zCommand.Parameters.Add("@Money_Dif_Holiday_General", SqlDbType.Money).Value = _Money_Dif_Holiday_General;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "FTR_Order_Money_UPDATE";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = _TeamKey;
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = _OrderKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = _EmployeeKey.Trim();
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName.Trim();
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID.Trim();
                if (_OrderDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = _OrderDate;
                zCommand.Parameters.Add("@StageKey", SqlDbType.Int).Value = _StageKey;
                zCommand.Parameters.Add("@Basket", SqlDbType.Float).Value = _Basket;
                zCommand.Parameters.Add("@Kg", SqlDbType.Float).Value = _Kg;
                zCommand.Parameters.Add("@Time", SqlDbType.Float).Value = _Time;
                zCommand.Parameters.Add("@Money", SqlDbType.Float).Value = _Money;
                zCommand.Parameters.Add("@Category", SqlDbType.Int).Value = _Category;
                zCommand.Parameters.Add("@Basket_Borrow", SqlDbType.Float).Value = _Basket_Borrow;
                zCommand.Parameters.Add("@Kg_Borrow", SqlDbType.Float).Value = _Kg_Borrow;
                zCommand.Parameters.Add("@Time_Borrow", SqlDbType.Float).Value = _Time_Borrow;
                zCommand.Parameters.Add("@Money_Borrow", SqlDbType.Float).Value = _Money_Borrow;
                zCommand.Parameters.Add("@Category_Borrow", SqlDbType.Int).Value = _Category_Borrow;
                zCommand.Parameters.Add("@Basket_Eat", SqlDbType.Float).Value = _Basket_Eat;
                zCommand.Parameters.Add("@Kg_Eat", SqlDbType.Float).Value = _Kg_Eat;
                zCommand.Parameters.Add("@Time_Eat", SqlDbType.Float).Value = _Time_Eat;
                zCommand.Parameters.Add("@Money_Eat", SqlDbType.Float).Value = _Money_Eat;
                zCommand.Parameters.Add("@Category_Eat", SqlDbType.Int).Value = _Category_Eat;
                zCommand.Parameters.Add("@TimePrivate", SqlDbType.Float).Value = _TimePrivate;
                zCommand.Parameters.Add("@MoneyPrivate", SqlDbType.Float).Value = _MoneyPrivate;
                zCommand.Parameters.Add("@KgPrivate", SqlDbType.Float).Value = _KgPrivate;
                zCommand.Parameters.Add("@BasketPrivate", SqlDbType.Float).Value = _BasketPrivate;
                zCommand.Parameters.Add("@CategoryPrivate", SqlDbType.Int).Value = _CategoryPrivate;
                zCommand.Parameters.Add("@Money_Dif_Private", SqlDbType.Money).Value = _Money_Dif;
                zCommand.Parameters.Add("@Money_Dif_Sunday_Private", SqlDbType.Money).Value = _Money_Dif_Sunday;
                zCommand.Parameters.Add("@Money_Dif_Holiday_Private", SqlDbType.Money).Value = _Money_Dif_Holiday;
                zCommand.Parameters.Add("@Money_Dif_General", SqlDbType.Money).Value = _Money_Dif_General;
                zCommand.Parameters.Add("@Money_Dif_Sunday_General", SqlDbType.Money).Value = _Money_Dif_Sunday_General;
                zCommand.Parameters.Add("@Money_Dif_Holiday_General", SqlDbType.Money).Value = _Money_Dif_Holiday_General;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "FTR_Order_Money_DELETE";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete_Order()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE FTR_Order_Money SET RecordStatus= 99  WHERE OrderKey = @OrderKey AND RecordStatus <> 99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = _OrderKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = string.Empty;
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string Save()
        {
            string zResult;
            if (_AutoKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        #endregion

        #region [Constructor Update General]
        public string Create_General()
        {
            string zSQL = "INSERT INTO FTR_Order_Money(" +
                          "TeamKey,OrderKey,EmployeeKey,EmployeeID,EmployeeName,OrderDate," +
                          "StageKey,TimeEat,KgEat,MoneyEat,RecordStatus,CreatedOn,CreatedBy,CreatedName) " +
                          "VALUES(" +
                          "@TeamKey,@OrderKey,@EmployeeKey,@EmployeeID,@EmployeeName,@OrderDate," +
                          "@StageKey,@TimeEat,@KgEat,@MoneyEat,0,GETDATE(),@CreatedBy,@CreatedName)";
            zSQL += "SELECT 11";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = _TeamKey;
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = _OrderKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = _EmployeeKey.Trim();
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName.Trim();
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID.Trim();
                if (_OrderDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = _OrderDate;
                zCommand.Parameters.Add("@StageKey", SqlDbType.Int).Value = _StageKey;
                zCommand.Parameters.Add("@TimeEat", SqlDbType.Float).Value = _TimeEat;
                zCommand.Parameters.Add("@KgEat", SqlDbType.Float).Value = _KgEat;
                zCommand.Parameters.Add("@MoneyEat", SqlDbType.Float).Value = _MoneyEat;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update_General()
        {
            string zSQL = "UPDATE FTR_Order_Money SET " +
                          "TimeEat = @TimeEat ," +
                          "KgEat = @KgEat ," +
                          "MoneyEat = @MoneyEat," +
                          "ModifiedBy = @ModifiedBy," +
                          "ModifiedName = @ModifiedName," +
                          "ModifiedOn = GETDATE()" +
                          "WHERE EmployeeKey = @EmployeeKey AND OrderKey = @OrderKey ";
            zSQL += "SELECT 20";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = _TeamKey;
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = _OrderKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = _EmployeeKey.Trim();
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName.Trim();
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID.Trim();
                if (_OrderDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = _OrderDate;
                zCommand.Parameters.Add("@StageKey", SqlDbType.Int).Value = _StageKey;
                zCommand.Parameters.Add("@TimeEat", SqlDbType.Float).Value = _TimeEat;
                zCommand.Parameters.Add("@KgEat", SqlDbType.Float).Value = _KgEat;
                zCommand.Parameters.Add("@MoneyEat", SqlDbType.Float).Value = _MoneyEat;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete_General()
        {
            string zSQL = "UPDATE FTR_Order_Money SET " +
                          "TimeEat = NULL ," +
                          "KgEat = NULL ," +
                          "MoneyEat = NULL," +
                          "ModifiedBy = @ModifiedBy," +
                          "ModifiedName = @ModifiedName," +
                          "ModifiedOn = GETDATE()" +
                          "WHERE EmployeeKey = @EmployeeKey AND OrderKey = @OrderKey ";
            zSQL += "SELECT 20";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = _TeamKey;
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = _OrderKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = _EmployeeKey.Trim();
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName.Trim();
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID.Trim();
                if (_OrderDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = _OrderDate;
                zCommand.Parameters.Add("@StageKey", SqlDbType.Int).Value = _StageKey;
                zCommand.Parameters.Add("@TimeEat", SqlDbType.Float).Value = _TimeEat;
                zCommand.Parameters.Add("@KgEat", SqlDbType.Float).Value = _KgEat;
                zCommand.Parameters.Add("@MoneyEat", SqlDbType.Float).Value = _MoneyEat;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Create_Tam()
        {
            string zSQL = "INSERT INTO FTR_Order_Money_Tam(" +
                          "TeamKey,OrderKey,EmployeeKey,EmployeeID,EmployeeName,OrderDate," +
                          "StageKey,Time,KgEat,MoneyEat,RecordStatus,CreatedOn,CreatedBy,CreatedName) " +
                          "VALUES(" +
                          "@TeamKey,@OrderKey,@EmployeeKey,@EmployeeID,@EmployeeName,@OrderDate," +
                          "@StageKey,@Time,@KgEat,@MoneyEat,0,GETDATE(),@CreatedBy,@CreatedName)";
            zSQL += "SELECT 11";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = _TeamKey;
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = _OrderKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = _EmployeeKey.Trim();
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName.Trim();
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID.Trim();
                zCommand.Parameters.Add("@Time", SqlDbType.Float).Value = _Time;
                if (_OrderDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = _OrderDate;
                zCommand.Parameters.Add("@StageKey", SqlDbType.Int).Value = _StageKey;
                zCommand.Parameters.Add("@KgEat", SqlDbType.Float).Value = _KgEat;
                zCommand.Parameters.Add("@MoneyEat", SqlDbType.Float).Value = _MoneyEat;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update_Tam()
        {
            string zSQL = "UPDATE FTR_Order_Money_Tam SET " +
                          "KgEat = @KgEat ," +
                          "Time = @Time ," +
                          "MoneyEat = @MoneyEat," +
                          "ModifiedBy = @ModifiedBy," +
                          "ModifiedName = @ModifiedName," +
                          "ModifiedOn = GETDATE()" +
                          "WHERE EmployeeKey = @EmployeeKey AND OrderKey = @OrderKey ";
            zSQL += "SELECT 20";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = _TeamKey;
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = _OrderKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = _EmployeeKey.Trim();
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName.Trim();
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID.Trim();
                zCommand.Parameters.Add("@Time", SqlDbType.Float).Value = _Time;
                if (_OrderDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = _OrderDate;
                zCommand.Parameters.Add("@StageKey", SqlDbType.Int).Value = _StageKey;
                zCommand.Parameters.Add("@KgEat", SqlDbType.Float).Value = _KgEat;
                zCommand.Parameters.Add("@MoneyEat", SqlDbType.Float).Value = _MoneyEat;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete_Tam()
        {
            string zSQL = "DELETE FROM FTR_Order_Money_Tam " +
                          "WHERE OrderKey = @OrderKey ";
            zSQL += "SELECT 20";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = _TeamKey;
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = _OrderKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = _EmployeeKey.Trim();
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName.Trim();
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID.Trim();
                zCommand.Parameters.Add("@Time", SqlDbType.Float).Value = _Time;
                if (_OrderDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = _OrderDate;
                zCommand.Parameters.Add("@StageKey", SqlDbType.Int).Value = _StageKey;
                zCommand.Parameters.Add("@KgEat", SqlDbType.Float).Value = _KgEat;
                zCommand.Parameters.Add("@MoneyEat", SqlDbType.Float).Value = _MoneyEat;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        #endregion

        public string UpdateTeam()
        {
            string zSQL = "UPDATE FTR_Order_Money SET " +
                          "TeamKey = @TeamKey " +
                          "WHERE EmployeeKey = @EmployeeKey AND OrderKey = @OrderKey ";
            zSQL += "SELECT 20";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = _TeamKey;
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = _OrderKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = _EmployeeKey.Trim();
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
    }
}
