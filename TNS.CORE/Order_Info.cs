﻿using System;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;

namespace TNS.CORE
{
    public class Order_Info
    {
        #region [ Field Name ]
        private string _OrderKey = "";
        private string _OrderID_Compare = "";
        private string _OrderID = "";
        private DateTime _OrderDate;
        private DateTime _WorkBegin;
        private DateTime _WorkEnd;
        private int _TeamKey = 0;
        private string _TeamID = "";
        private string _TeamName = "";
        private string _ProductKey = "";
        private string _ProductID = "";
        private string _ProductName = "";
        private float _QuantityDocument;
        private float _QuantityReality;
        private float _QuantityLoss;
        private int _UnitKey = 0;
        private string _UnitName = "";
        private string _OrderIDFollow = "";
        private float _PercentFinish = 0;
        private int _Category_Stage = 0;
        private int _GroupKey = 0;
        private string _GroupID = "";
        private int _StageKey = 0;
        private string _StagesID = "";
        private string _StageName = "";
        private double _StagePrice = 0;
        private int _RecordStatus = 0;
        private int _WorkStatus = 0;
        private string _Note = "";
        private int _Rank = 0;
        private DateTime _CreatedOn;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _RoleID = "";
        private string _Message = "";
        #endregion

        #region [ Properties ]
        public bool IsCommandOK
        {
            get
            {
                int zNumber = 0;
                if (_Message.Length > 0)
                {
                    int.TryParse(_Message.Substring(0, 1), out zNumber);
                }
                if (zNumber <= 3) return true; else return false;
            }
        }
        public bool HasInfo
        {
            get
            {
                if (_OrderKey.Trim().Length > 0) return true; else return false;
            }
        }
        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        public string Key
        {
            get { return _OrderKey; }
            set { _OrderKey = value; }
        }
        public string OrderID
        {
            get { return _OrderID; }
            set { _OrderID = value; }
        }
        public DateTime OrderDate
        {
            get { return _OrderDate; }
            set { _OrderDate = value; }
        }
        public int TeamKey
        {
            get { return _TeamKey; }
            set { _TeamKey = value; }
        }
        public string ProductKey
        {
            get { return _ProductKey; }
            set { _ProductKey = value; }
        }
        public string ProductName
        {
            get { return _ProductName; }
            set { _ProductName = value; }
        }
        public float QuantityDocument
        {
            get { return _QuantityDocument; }
            set { _QuantityDocument = value; }
        }
        public float QuantityReality
        {
            get { return _QuantityReality; }
            set { _QuantityReality = value; }
        }
        public float QuantityLoss
        {
            get { return _QuantityLoss; }
            set { _QuantityLoss = value; }
        }
        public int UnitKey
        {
            get { return _UnitKey; }
            set { _UnitKey = value; }
        }
        public string UnitName
        {
            get { return _UnitName; }
            set { _UnitName = value; }
        }
        public string OrderIDFollow
        {
            get { return _OrderIDFollow; }
            set { _OrderIDFollow = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public string Note
        {
            get { return _Note; }
            set { _Note = value; }
        }
        public int Rank
        {
            get { return _Rank; }
            set { _Rank = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public int Category_Stage
        {
            get
            {
                return _Category_Stage;
            }

            set
            {
                _Category_Stage = value;
            }
        }

        public int WorkStatus
        {
            get
            {
                return _WorkStatus;
            }

            set
            {
                _WorkStatus = value;
            }
        }

        public DateTime WorkBegin
        {
            get
            {
                return _WorkBegin;
            }

            set
            {
                _WorkBegin = value;
            }
        }

        public DateTime WorkEnd
        {
            get
            {
                return _WorkEnd;
            }

            set
            {
                _WorkEnd = value;
            }
        }

        public string ProductID
        {
            get
            {
                return _ProductID;
            }

            set
            {
                _ProductID = value;
            }
        }

        public float PercentFinish
        {
            get
            {
                return _PercentFinish;
            }

            set
            {
                _PercentFinish = value;
            }
        }

        public string OrderID_Compare
        {
            get
            {
                return _OrderID_Compare;
            }

            set
            {
                _OrderID_Compare = value;
            }
        }

        public string TeamID
        {
            get
            {
                return _TeamID;
            }

            set
            {
                _TeamID = value;
            }
        }

        public string TeamName
        {
            get
            {
                return _TeamName;
            }

            set
            {
                _TeamName = value;
            }
        }

        public int StageKey
        {
            get
            {
                return _StageKey;
            }

            set
            {
                _StageKey = value;
            }
        }

        public double StagePrice
        {
            get
            {
                return _StagePrice;
            }

            set
            {
                _StagePrice = value;
            }
        }

        public string StageName
        {
            get
            {
                return _StageName;
            }

            set
            {
                _StageName = value;
            }
        }

        public string StagesID
        {
            get
            {
                return _StagesID;
            }

            set
            {
                _StagesID = value;
            }
        }

        public int GroupKey
        {
            get
            {
                return _GroupKey;
            }

            set
            {
                _GroupKey = value;
            }
        }

        public string GroupID
        {
            get
            {
                return _GroupID;
            }

            set
            {
                _GroupID = value;
            }
        }
        #endregion

        #region [ Constructor Get Information ]
        public Order_Info()
        {
        }
        public Order_Info(string OrderKey)
        {
            string zSQL = @"
SELECT A.*, C.TeamID, C.TeamName, B.StagesID, B.Price AS StagePrice, B.StageKey
FROM FTR_Order A 
LEFT JOIN IVT_Product_Stages B ON A.Category_Stage = B.StageKey
LEFT JOIN SYS_Team C ON A.TeamKey = C.TeamKey
WHERE OrderKey = @OrderKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(OrderKey);
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["StagePrice"] != DBNull.Value)
                        _StagePrice = double.Parse(zReader["StagePrice"].ToString());
                    if (zReader["StageKey"] != DBNull.Value)
                        _StageKey = int.Parse(zReader["StageKey"].ToString());

                    _TeamName = zReader["TeamName"].ToString();
                    _TeamID = zReader["TeamID"].ToString();
                    _StagesID = zReader["StagesID"].ToString();
                    _OrderKey = zReader["OrderKey"].ToString();


                    _OrderKey = zReader["OrderKey"].ToString();
                    _OrderID_Compare = zReader["OrderID_Compare"].ToString().Trim();
                    _OrderID = zReader["OrderID"].ToString().Trim();
                    if (zReader["OrderDate"] != DBNull.Value)
                        _OrderDate = (DateTime)zReader["OrderDate"];
                    if (zReader["WorkBegin"] != DBNull.Value)
                        _WorkBegin = (DateTime)zReader["WorkBegin"];
                    if (zReader["WorkEnd"] != DBNull.Value)
                        _WorkEnd = (DateTime)zReader["WorkEnd"];
                    if (zReader["TeamKey"] != DBNull.Value)
                        _TeamKey = int.Parse(zReader["TeamKey"].ToString());
                    _ProductKey = zReader["ProductKey"].ToString().Trim();
                    _ProductID = zReader["ProductID"].ToString().Trim();
                    _ProductName = zReader["ProductName"].ToString().Trim();
                    if (zReader["QuantityDocument"] != DBNull.Value)
                        _QuantityDocument = float.Parse(zReader["QuantityDocument"].ToString());
                    if (zReader["QuantityReality"] != DBNull.Value)
                        _QuantityReality = float.Parse(zReader["QuantityReality"].ToString());
                    if (zReader["QuantityLoss"] != DBNull.Value)
                        _QuantityLoss = float.Parse(zReader["QuantityLoss"].ToString());
                    if (zReader["UnitKey"] != DBNull.Value)
                        _UnitKey = int.Parse(zReader["UnitKey"].ToString());
                    _UnitName = zReader["UnitName"].ToString().Trim();
                    if (zReader["Category_Stage"] != DBNull.Value)
                        _Category_Stage = int.Parse(zReader["Category_Stage"].ToString());
                    _OrderIDFollow = zReader["OrderIDFollow"].ToString().Trim();
                    if (zReader["PercentFinish"] != DBNull.Value)
                        _PercentFinish = float.Parse(zReader["PercentFinish"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["WorkStatus"] != DBNull.Value)
                        _WorkStatus = int.Parse(zReader["WorkStatus"].ToString());
                    _Note = zReader["Note"].ToString().Trim();
                    if (zReader["Rank"] != DBNull.Value)
                        _Rank = int.Parse(zReader["Rank"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion

        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "FTR_Order_INSERT";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@OrderID_Compare", SqlDbType.NVarChar).Value = _OrderID_Compare.Trim();
                zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = _OrderID;
                if (_OrderDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = _OrderDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = _TeamKey;
                zCommand.Parameters.Add("@ProductKey", SqlDbType.NVarChar).Value = _ProductKey;
                zCommand.Parameters.Add("@ProductID", SqlDbType.NVarChar).Value = _ProductID;
                zCommand.Parameters.Add("@ProductName", SqlDbType.NVarChar).Value = _ProductName.Trim();
                zCommand.Parameters.Add("@QuantityDocument", SqlDbType.Float).Value = _QuantityDocument;
                zCommand.Parameters.Add("@QuantityReality", SqlDbType.Float).Value = _QuantityReality;
                zCommand.Parameters.Add("@QuantityLoss", SqlDbType.Float).Value = _QuantityLoss;
                zCommand.Parameters.Add("@UnitKey", SqlDbType.Int).Value = _UnitKey;
                zCommand.Parameters.Add("@UnitName", SqlDbType.NVarChar).Value = _UnitName.Trim();
                zCommand.Parameters.Add("@Category_Stage", SqlDbType.Int).Value = _Category_Stage;
                zCommand.Parameters.Add("@GroupKey", SqlDbType.Int).Value = _GroupKey;
                zCommand.Parameters.Add("@GroupID", SqlDbType.NVarChar).Value = _GroupID;
                zCommand.Parameters.Add("@OrderIDFollow", SqlDbType.NVarChar).Value = _OrderIDFollow.Trim();
                zCommand.Parameters.Add("@PercentFinish", SqlDbType.Float).Value = _PercentFinish;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@WorkStatus", SqlDbType.Int).Value = _WorkStatus;
                zCommand.Parameters.Add("@Note", SqlDbType.NVarChar).Value = _Note.Trim();
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                if (_Message.Substring(0, 2) == "11")
                    _OrderKey = _Message.Substring(2, 36);
                else
                    _OrderKey = "";
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "FTR_Order_UPDATE";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                if (_OrderKey.Length == 36)
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_OrderKey);
                else
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@OrderID_Compare", SqlDbType.NVarChar).Value = _OrderID_Compare.Trim();
                zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = _OrderID.Trim();
                if (_OrderDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = _OrderDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = _TeamKey;
                zCommand.Parameters.Add("@ProductKey", SqlDbType.NVarChar).Value = _ProductKey;
                zCommand.Parameters.Add("@ProductID", SqlDbType.NVarChar).Value = _ProductID;
                zCommand.Parameters.Add("@ProductName", SqlDbType.NVarChar).Value = _ProductName.Trim();
                zCommand.Parameters.Add("@QuantityDocument", SqlDbType.Float).Value = _QuantityDocument;
                zCommand.Parameters.Add("@QuantityReality", SqlDbType.Float).Value = _QuantityReality;
                zCommand.Parameters.Add("@QuantityLoss", SqlDbType.Float).Value = _QuantityLoss;
                zCommand.Parameters.Add("@UnitKey", SqlDbType.Int).Value = _UnitKey;
                zCommand.Parameters.Add("@UnitName", SqlDbType.NVarChar).Value = _UnitName.Trim();
                zCommand.Parameters.Add("@Category_Stage", SqlDbType.Int).Value = _Category_Stage;
                zCommand.Parameters.Add("@GroupKey", SqlDbType.Int).Value = _GroupKey;
                zCommand.Parameters.Add("@GroupID", SqlDbType.NVarChar).Value = _GroupID;
                zCommand.Parameters.Add("@OrderIDFollow", SqlDbType.NVarChar).Value = _OrderIDFollow.Trim();
                zCommand.Parameters.Add("@PercentFinish", SqlDbType.Float).Value = _PercentFinish;
                zCommand.Parameters.Add("@WorkStatus", SqlDbType.Int).Value = _WorkStatus;
                zCommand.Parameters.Add("@Note", SqlDbType.NVarChar).Value = _Note.Trim();
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "FTR_Order_DELETE";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_OrderKey);
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_OrderKey.Trim().Length == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        #endregion
    }
}
