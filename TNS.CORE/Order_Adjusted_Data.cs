﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Connect;
using TNS.Misc;

namespace TNS.CORE
{
    public class Order_Adjusted_Data
    {
        #region[Thông tin chia lần 1]
        public static DataTable List_TeamShare1(DateTime FromDate, DateTime ToDate)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT
        [dbo].[Fn_TeamKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate) AS TeamKey,
        [dbo].[Fn_GetTeamID]([dbo].[Fn_TeamKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate)) AS TeamID,
        [dbo].[Fn_GetTeamName]([dbo].[Fn_TeamKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate)) AS TeamName,
        ROUND(SUM(A.Time_Eat),4) AS TimeShare,
        ROUND(SUM(A.Basket_Eat),4) AS BasketShare,
        ROUND(SUM(A.Kg_Eat),4) AS KgShare ,
        ROUND(SUM(A.Money_Eat),0) AS MoneyShare
        FROM  FTR_Order_Money A
        WHERE A.RecordStatus <> 99 
        AND A.Money_Eat > 0 
        AND A.OrderDate BETWEEN @FromDate AND @ToDate
        GROUP BY [dbo].[Fn_TeamKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate),
        [dbo].[Fn_GetTeamID]([dbo].[Fn_TeamKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate)),
        [dbo].[Fn_GetTeamName]([dbo].[Fn_TeamKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate))";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        //public static DataTable List_TeamShare1(DateTime FromDate, DateTime ToDate)
        //{
        //    DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
        //    DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
        //    DataTable zTable = new DataTable();
        //    string zSQL = @"SELECT
        //[dbo].Fn_LayTeamKey_TheoThang(A.EmployeeKey,@FromDate) AS TeamKey,
        //[dbo].[Fn_GetTeamID](dbo.Fn_LayTeamKey_TheoThang(A.EmployeeKey,@FromDate)) AS TeamID,
        //[dbo].[Fn_GetTeamName](dbo.Fn_LayTeamKey_TheoThang(A.EmployeeKey,@FromDate)) AS TeamName,
        //ROUND(SUM(A.Time_Eat),4) AS TimeShare,
        //ROUND(SUM(A.Basket_Eat),4) AS BasketShare,
        //ROUND(SUM(A.Kg_Eat),4) AS KgShare ,
        //ROUND(SUM(A.Money_Eat),0) AS MoneyShare
        //FROM  FTR_Order_Money A
        //WHERE A.RecordStatus <> 99 
        //AND A.Money_Eat > 0 
        //AND A.OrderDate BETWEEN @FromDate AND @ToDate
        //GROUP BY [dbo].Fn_LayTeamKey_TheoThang(A.EmployeeKey,@FromDate),
        //[dbo].[Fn_GetTeamID](dbo.Fn_LayTeamKey_TheoThang(A.EmployeeKey,@FromDate)),
        //[dbo].[Fn_GetTeamName](dbo.Fn_LayTeamKey_TheoThang(A.EmployeeKey,@FromDate))";
        //    string zConnectionString = ConnectDataBase.ConnectionString;
        //    try
        //    {
        //        SqlConnection zConnect = new SqlConnection(zConnectionString);
        //        zConnect.Open();
        //        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //        zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
        //        zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
        //        SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
        //        zAdapter.Fill(zTable);
        //        zCommand.Dispose();
        //        zConnect.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        string zstrMessage = "08" + ex.ToString();
        //    }
        //    return zTable;
        //}
        #region[Close-14/08/2020 -List_TeamShare1-Chuyển tổ nhóm]
        //public static DataTable List_TeamShare1(DateTime FromDate, DateTime ToDate)
        //{
        //    DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
        //    DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
        //    DataTable zTable = new DataTable();
        //    string zSQL = @"SELECT
        //[dbo].[LayTeamKey](A.EmployeeKey) AS TeamKey,
        //[dbo].[Fn_GetTeamID]([dbo].[LayTeamKey](A.EmployeeKey)) AS TeamID,
        //[dbo].[Fn_GetTeamName]([dbo].[LayTeamKey](A.EmployeeKey)) AS TeamName,
        //ROUND(SUM(A.Time_Eat),2) AS TimeShare,
        //ROUND(SUM(A.Basket_Eat),2) AS BasketShare,
        //ROUND(SUM(A.Kg_Eat),2) AS KgShare ,
        //ROUND(SUM(A.Money_Eat),2) AS MoneyShare
        //FROM  FTR_Order_Money A
        //WHERE A.RecordStatus <> 99 
        //AND A.Money_Eat > 0 
        //AND A.OrderDate BETWEEN @FromDate AND @ToDate
        //GROUP BY [dbo].[LayTeamKey](A.EmployeeKey),
        //[dbo].[Fn_GetTeamID]([dbo].[LayTeamKey](A.EmployeeKey)),
        //[dbo].[Fn_GetTeamName]([dbo].[LayTeamKey](A.EmployeeKey))";
        //    string zConnectionString = ConnectDataBase.ConnectionString;
        //    try
        //    {
        //        SqlConnection zConnect = new SqlConnection(zConnectionString);
        //        zConnect.Open();
        //        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //        zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
        //        zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
        //        SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
        //        zAdapter.Fill(zTable);
        //        zCommand.Dispose();
        //        zConnect.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        string zstrMessage = "08" + ex.ToString();
        //    }
        //    return zTable;
        //}
        #endregion


        public static DataTable List_OrderShare1(DateTime FromDate, DateTime ToDate, int TeamKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT
        B.OrderKey,B.OrderID_Compare, B.OrderID, B.OrderDate, B.Category_Stage AS StageKey,
        [dbo].[LayMaCongViec](B.Category_Stage) AS StageID,
        dbo.LayGiaCongViec(B.Category_Stage) AS Price,
        dbo.LayTenCongViec(B.Category_Stage) AS StageName,
        ROUND(SUM(A.Time_Eat),4) AS TimeShare ,
        ROUND(SUM(A.Basket_Eat),4) AS BasketShare ,
        ROUND(SUM(A.Kg_Eat),4) AS KgShare ,
        ROUND(SUM(A.Money_Eat),0) AS MoneyShare
        FROM  FTR_Order_Money A
        LEFT JOIN FTR_Order B ON A.OrderKey = B.OrderKey
        LEFT JOIN dbo.HRM_Employee E ON E.EmployeeKey=A.EmployeeKey
        WHERE A.RecordStatus <> 99 
        AND [dbo].[Fn_TeamKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate) = @TeamKey
        AND A.Money_Eat > 0
        AND B.RecordStatus <> 99
        AND B.OrderDate BETWEEN @FromDate AND @ToDate
        AND E.StartingDate <= @FromDate AND ( E.LeavingDate >= @ToDate OR E.LeavingDate IS NULL)
        GROUP BY B.OrderKey,B.OrderID_Compare, B.OrderID,  B.OrderDate, B.Category_Stage
        ORDER BY B.OrderDate, B.OrderID";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        //public static DataTable List_OrderShare1(DateTime FromDate, DateTime ToDate, int TeamKey)
        //{

        //    DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
        //    DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
        //    DataTable zTable = new DataTable();
        //    string zSQL = @"SELECT
        //B.OrderKey,B.OrderID_Compare, B.OrderID, B.OrderDate, B.Category_Stage AS StageKey,
        //[dbo].[LayMaCongViec](B.Category_Stage) AS StageID,
        //dbo.LayGiaCongViec(B.Category_Stage) AS Price,
        //dbo.LayTenCongViec(B.Category_Stage) AS StageName,
        //ROUND(SUM(A.Time_Eat),4) AS TimeShare ,
        //ROUND(SUM(A.Basket_Eat),4) AS BasketShare ,
        //ROUND(SUM(A.Kg_Eat),4) AS KgShare ,
        //ROUND(SUM(A.Money_Eat),0) AS MoneyShare
        //FROM  FTR_Order_Money A
        //LEFT JOIN FTR_Order B ON A.OrderKey = B.OrderKey
        //LEFT JOIN dbo.HRM_Team_Employee_Month E ON E.EmployeeKey=A.EmployeeKey
        //LEFT JOIN dbo.SYS_Team F ON F.TeamKey=E.TeamKey
        //WHERE A.RecordStatus <> 99 
        //AND F.TeamKey = @TeamKey
        //AND A.Money_Eat > 0
        //AND B.RecordStatus <> 99
        //AND B.OrderDate BETWEEN @FromDate AND @ToDate
        //AND (MONTH(E.DateWrite) =MONTH(@FromDate) AND YEAR(E.DateWrite) =YEAR(@FromDate))
        //GROUP BY B.OrderKey,B.OrderID_Compare, B.OrderID,  B.OrderDate, B.Category_Stage
        //ORDER BY B.OrderDate, B.OrderID";
        //    string zConnectionString = ConnectDataBase.ConnectionString;
        //    try
        //    {
        //        SqlConnection zConnect = new SqlConnection(zConnectionString);
        //        zConnect.Open();
        //        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //        zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
        //        zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
        //        zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
        //        SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
        //        zAdapter.Fill(zTable);
        //        zCommand.Dispose();
        //        zConnect.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        string zstrMessage = "08" + ex.ToString();
        //    }
        //    return zTable;
        //}
        #region[close-14/08/2020-List_OrderShare1-Chuyen tổ nhóm]
        //public static DataTable List_OrderShare1(DateTime FromDate, DateTime ToDate, int TeamKey)
        //{
        //    DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
        //    DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
        //    DataTable zTable = new DataTable();
        //    string zSQL = @"SELECT
        //B.OrderKey,B.OrderID_Compare, B.OrderID, B.OrderDate, B.Category_Stage AS StageKey,
        //[dbo].[LayMaCongViec](B.Category_Stage) AS StageID,
        //dbo.LayGiaCongViec(B.Category_Stage) AS Price,
        //dbo.LayTenCongViec(B.Category_Stage) AS StageName,
        //ROUND(SUM(A.Time_Eat),2) AS TimeShare ,
        //ROUND(SUM(A.Basket_Eat),2) AS BasketShare ,
        //ROUND(SUM(A.Kg_Eat),2) AS KgShare ,
        //ROUND(SUM(A.Money_Eat),2) AS MoneyShare
        //FROM  FTR_Order_Money A
        //LEFT JOIN FTR_Order B ON A.OrderKey = B.OrderKey
        //LEFT JOIN dbo.HRM_Employee E ON E.EmployeeKey=A.EmployeeKey
        //LEFT JOIN dbo.SYS_Team F ON F.TeamKey=E.TeamKey
        //WHERE A.RecordStatus <> 99 
        //AND F.TeamKey = @TeamKey
        //AND A.Money_Eat > 0
        //AND B.RecordStatus <> 99
        //AND B.OrderDate BETWEEN @FromDate AND @ToDate
        //GROUP BY B.OrderKey,B.OrderID_Compare, B.OrderID,  B.OrderDate, B.Category_Stage
        //ORDER BY B.OrderDate, B.OrderID";
        //    string zConnectionString = ConnectDataBase.ConnectionString;
        //    try
        //    {
        //        SqlConnection zConnect = new SqlConnection(zConnectionString);
        //        zConnect.Open();
        //        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //        zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
        //        zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
        //        zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
        //        SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
        //        zAdapter.Fill(zTable);
        //        zCommand.Dispose();
        //        zConnect.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        string zstrMessage = "08" + ex.ToString();
        //    }
        //    return zTable;
        //}
        #endregion

        public static DataTable List_EmployeeShare1(string OrderKey, int TeamKey, DateTime FromDate, DateTime ToDate)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            DataTable zTable = new DataTable();
            string zSQL = @"
--declare @FromDate datetime='2020-04-01 00:00:00'
--declare @ToDate datetime='2020-04-30 23:59:59'

--Nhân sự làm việc trong tháng
CREATE TABLE #EmployeeTemp
(
EmployeeKey NVARCHAR(50),
EmployeeID NVARCHAR(50),
EmployeeName NVARCHAR(500),
BranchKey INT,
DepartmentKey INT ,
TeamKey INT ,
PositionKey INT 
)
INSERT INTO #EmployeeTemp
SELECT EmployeeKey,EmployeeID,EmployeeName,
BranchKey,DepartmentKey,TeamKey, PositionKey
 FROM (
		SELECT EmployeeKey,EmployeeID,[dbo].[Fn_GetFullName](EmployeeKey) AS EmployeeName,StartingDate,
			CASE
				WHEN LeavingDate IS NULL THEN GETDATE()
				ELSE LeavingDate
			END LeavingDate,
            [dbo].[Fn_BranchKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate) AS BranchKey,
            [dbo].[Fn_DepartmentKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate) AS DepartmentKey,
            [dbo].[Fn_TeamKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate) AS TeamKey ,
            [dbo].[Fn_PositionKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate) AS PositionKey
			FROM [dbo].[HRM_Employee] 
			WHERE RecordStatus <>99 
	) X
	WHERE (
            ( StartingDate <=@FromDate AND @FromDate <=  LeavingDate  )
	     OR ( StartingDate <=@ToDate   AND @ToDate   <=  LeavingDate )
        )
    @Customer
----------Kết thúc tạo danh sách nhân sự
SELECT EmployeeName, EmployeeKey,EmployeeID ,
dbo.SumTimeTeam(@FromDate, @ToDate, @TeamKey, EmployeeKey) AS Time,
dbo.LaySoGioAnChung(EmployeeKey, @OrderKey) AS SoGio,
dbo.LaySoRoAnChung(EmployeeKey, @OrderKey) AS Basket,
dbo.LaySoLuongAnChung(EmployeeKey, @OrderKey) AS Kg,
dbo.LaySoTienAnChung(EmployeeKey, @OrderKey) AS Money
FROM #EmployeeTemp 
ORDER BY  LEN(EmployeeID),EmployeeID

DROP TABLE #EmployeeTemp
";
            string zFilter = "";

            if (TeamKey != 0)
            {
                zFilter += " AND TeamKey= @TeamKey";
            }
            if (zFilter.Length != 0)
            {
                zSQL = zSQL.Replace("@Customer", zFilter);
            }
            else
            {
                zSQL = zSQL.Replace("@Customer", "");
            }

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = OrderKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        //        public static DataTable List_EmployeeShare1(string OrderKey, int TeamKey, DateTime FromDate, DateTime ToDate)
        //        {
        //            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
        //            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
        //            string zSQL = "";
        //            DataTable zTable = new DataTable();

        //            zSQL = @"
        //SELECT A. EmployeeName,
        //A.EmployeeKey, A.EmployeeID ,
        //dbo.SumTimeTeam(@FromDate, @ToDate, @TeamKey, A.EmployeeKey) AS Time,
        //dbo.LaySoGioAnChung(A.EmployeeKey, @OrderKey) AS SoGio,
        //dbo.LaySoRoAnChung(A.EmployeeKey, @OrderKey) AS Basket,
        //dbo.LaySoLuongAnChung(A.EmployeeKey, @OrderKey) AS Kg,
        //dbo.LaySoTienAnChung(A.EmployeeKey, @OrderKey) AS Money
        //FROM [dbo].[HRM_Team_Employee_Month] A
        //WHERE A.RecordStatus < 99 
        //AND (MONTH(A.DateWrite) = MONTH(@FromDate)) AND (YEAR(A.DateWrite) = YEAR(@FromDate))
        //AND A.TeamKey = @TeamKey 
        //ORDER BY LEN(A.EmployeeID), A.EmployeeID";

        //            string zConnectionString = ConnectDataBase.ConnectionString;
        //            try
        //            {
        //                SqlConnection zConnect = new SqlConnection(zConnectionString);
        //                zConnect.Open();
        //                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = OrderKey;
        //                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
        //                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
        //                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
        //                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
        //                zAdapter.Fill(zTable);
        //                zCommand.Dispose();
        //                zConnect.Close();
        //            }
        //            catch (Exception ex)
        //            {
        //                string zstrMessage = ex.ToString();
        //            }
        //            return zTable;
        //        }
        #region[close-14/08/2020-List_EmployeeShare1-Chuyen tổ nhóm]
        //        public static DataTable List_EmployeeShare1(string OrderKey, int TeamKey, DateTime FromDate, DateTime ToDate)
        //        {
        //            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
        //            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
        //            string zSQL = "";
        //            DataTable zTable = new DataTable();

        //            zSQL = @"
        //SELECT dbo.Fn_GetFullName(A.EmployeeKey) AS EmployeeName,
        //A.EmployeeKey, A.EmployeeID ,
        //dbo.SumTimeTeam(@FromDate, @ToDate, @TeamKey, A.EmployeeKey) AS Time,
        //dbo.LaySoGioAnChung(A.EmployeeKey, @OrderKey) AS SoGio,
        //dbo.LaySoRoAnChung(A.EmployeeKey, @OrderKey) AS Basket,
        //dbo.LaySoLuongAnChung(A.EmployeeKey, @OrderKey) AS Kg,
        //dbo.LaySoTienAnChung(A.EmployeeKey, @OrderKey) AS Money
        //FROM HRM_Employee A
        //WHERE A.RecordStatus < 99
        //AND A.TeamKey = @TeamKey 
        //ORDER BY LEN(A.EmployeeID), A.EmployeeID";

        //            string zConnectionString = ConnectDataBase.ConnectionString;
        //            try
        //            {
        //                SqlConnection zConnect = new SqlConnection(zConnectionString);
        //                zConnect.Open();
        //                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = OrderKey;
        //                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
        //                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
        //                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
        //                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
        //                zAdapter.Fill(zTable);
        //                zCommand.Dispose();
        //                zConnect.Close();
        //            }
        //            catch (Exception ex)
        //            {
        //                string zstrMessage = ex.ToString();
        //            }
        //            return zTable;
        //        }
        #endregion


        public static DataTable List_TeamOfShare1(int TeamKey, string OrderKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT
[dbo].Fn_LayTeamKey_TheoThang(A.EmployeeKey,@FromDate) AS TeamKey,
[dbo].[Fn_GetTeamID]( [dbo].Fn_LayTeamKey_TheoThang(A.EmployeeKey,@FromDate)) AS TeamID,
[dbo].[Fn_GetTeamName]( [dbo].Fn_LayTeamKey_TheoThang(A.EmployeeKey,@FromDate)) AS TeamName
FROM  dbo.FTR_Order_Adjusted A
WHERE A.RecordStatus <> 99 
AND A.CategoryKey  =1  AND A.Borrow=1 AND A.Share=1
--AND A.OrderDate BETWEEN @FromDate AND @ToDate 
AND A.OrderKey= @OrderKey
GROUP BY A.OrderKey,  [dbo].Fn_LayTeamKey_TheoThang(A.EmployeeKey,@FromDate) ,
[dbo].[Fn_GetTeamID]( [dbo].Fn_LayTeamKey_TheoThang(A.EmployeeKey,@FromDate)),
[dbo].[Fn_GetTeamName]( [dbo].Fn_LayTeamKey_TheoThang(A.EmployeeKey,@FromDate)) ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = OrderKey;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        #region[close-14/08/2020-List_TeamOfShare1-Chuyen tổ nhóm]
        //        public static DataTable List_TeamOfShare1(int TeamKey, string OrderKey)
        //        {
        //            DataTable zTable = new DataTable();
        //            string zSQL = @"
        //SELECT
        //[dbo].[LayTeamKey](A.EmployeeKey) AS TeamKey,
        //[dbo].[Fn_GetTeamID]([dbo].[LayTeamKey](A.EmployeeKey)) AS TeamID,
        //[dbo].[Fn_GetTeamName]([dbo].[LayTeamKey](A.EmployeeKey)) AS TeamName
        //FROM  dbo.FTR_Order_Adjusted A
        //WHERE A.RecordStatus <> 99 
        //AND A.CategoryKey  =1  AND A.Borrow=1 AND A.Share=1
        //--AND A.OrderDate BETWEEN @FromDate AND @ToDate 
        //AND A.OrderKey= @OrderKey
        //GROUP BY A.OrderKey, [dbo].[LayTeamKey](A.EmployeeKey) ,
        //[dbo].[Fn_GetTeamID]([dbo].[LayTeamKey](A.EmployeeKey)),
        //[dbo].[Fn_GetTeamName]([dbo].[LayTeamKey](A.EmployeeKey)) ";
        //            string zConnectionString = ConnectDataBase.ConnectionString;
        //            try
        //            {
        //                SqlConnection zConnect = new SqlConnection(zConnectionString);
        //                zConnect.Open();
        //                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = OrderKey;
        //                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
        //                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
        //                zAdapter.Fill(zTable);
        //                zCommand.Dispose();
        //                zConnect.Close();
        //            }
        //            catch (Exception ex)
        //            {
        //                string zstrMessage = "08" + ex.ToString();
        //            }
        //            return zTable;
        //        }
        #endregion

        #endregion

        #region[Thông tin Chia lại lần 2]
        //Chia lại lan 2
        //        public static DataTable List_TeamShare2(DateTime FromDate, DateTime ToDate)
        //        {
        //            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
        //            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
        //            DataTable zTable = new DataTable();
        //            string zSQL = @"SELECT
        //[dbo].Fn_LayTeamKey_TheoThang(A.EmployeeKey,@FromDate) AS TeamKey,
        //[dbo].[Fn_GetTeamID]( [dbo].Fn_LayTeamKey_TheoThang(A.EmployeeKey,@FromDate)) AS TeamID,
        //[dbo].[Fn_GetTeamName]( [dbo].Fn_LayTeamKey_TheoThang(A.EmployeeKey,@FromDate)) AS TeamName,
        //ROUND(SUM(A.TimeProduct),4) AS TimeShare,
        //ROUND(SUM(A.BasketProduct),4) AS BasketShare,
        //ROUND(SUM(A.KgProduct),4) AS KgShare ,
        //ROUND(SUM(A.MoneyShare),0) AS MoneyShare
        //FROM  dbo.FTR_Order_Adjusted A
        //WHERE A.RecordStatus <> 99 
        //AND A.CategoryKey  =1  AND A.Borrow=1 AND A.Share=1
        //AND A.OrderDate BETWEEN @FromDate AND @ToDate
        //GROUP BY  [dbo].Fn_LayTeamKey_TheoThang(A.EmployeeKey,@FromDate),
        //[dbo].[Fn_GetTeamID]( [dbo].Fn_LayTeamKey_TheoThang(A.EmployeeKey,@FromDate)),
        //[dbo].[Fn_GetTeamName]( [dbo].Fn_LayTeamKey_TheoThang(A.EmployeeKey,@FromDate))";
        //            string zConnectionString = ConnectDataBase.ConnectionString;
        //            try
        //            {
        //                SqlConnection zConnect = new SqlConnection(zConnectionString);
        //                zConnect.Open();
        //                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
        //                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
        //                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
        //                zAdapter.Fill(zTable);
        //                zCommand.Dispose();
        //                zConnect.Close();
        //            }
        //            catch (Exception ex)
        //            {
        //                string zstrMessage = "08" + ex.ToString();
        //            }
        //            return zTable;
        //        }
        public static DataTable List_TeamShare2(DateTime FromDate, DateTime ToDate)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT
[dbo].[Fn_TeamKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate) AS TeamKey,
[dbo].[Fn_GetTeamID]( [dbo].[Fn_TeamKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate)) AS TeamID,
[dbo].[Fn_GetTeamName]( [dbo].[Fn_TeamKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate)) AS TeamName,
ROUND(SUM(A.TimeProduct),4) AS TimeShare,
ROUND(SUM(A.BasketProduct),4) AS BasketShare,
ROUND(SUM(A.KgProduct),4) AS KgShare ,
ROUND(SUM(A.MoneyShare),0) AS MoneyShare
FROM  dbo.FTR_Order_Adjusted A
WHERE A.RecordStatus <> 99 
AND A.CategoryKey  =1  AND A.Borrow=1 AND A.Share=1
AND A.OrderDate BETWEEN @FromDate AND @ToDate
GROUP BY  [dbo].[Fn_TeamKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate),
[dbo].[Fn_GetTeamID]( [dbo].[Fn_TeamKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate)),
[dbo].[Fn_GetTeamName]([dbo].[Fn_TeamKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate))";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        #region[close-14/08/2020-List_TeamShare2-Chuyen tổ nhóm]
        //        public static DataTable List_TeamShare2(DateTime FromDate, DateTime ToDate)
        //        {
        //            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
        //            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
        //            DataTable zTable = new DataTable();
        //            string zSQL = @"SELECT
        //[dbo].[LayTeamKey](A.EmployeeKey) AS TeamKey,
        //[dbo].[Fn_GetTeamID]([dbo].[LayTeamKey](A.EmployeeKey)) AS TeamID,
        //[dbo].[Fn_GetTeamName]([dbo].[LayTeamKey](A.EmployeeKey)) AS TeamName,
        //ROUND(SUM(A.TimeProduct),2) AS TimeShare,
        //ROUND(SUM(A.BasketProduct),2) AS BasketShare,
        //ROUND(SUM(A.KgProduct),2) AS KgShare ,
        //ROUND(SUM(A.MoneyShare),2) AS MoneyShare
        //FROM  dbo.FTR_Order_Adjusted A
        //WHERE A.RecordStatus <> 99 
        //AND A.CategoryKey  =1  AND A.Borrow=1 AND A.Share=1
        //AND A.OrderDate BETWEEN @FromDate AND @ToDate
        //GROUP BY [dbo].[LayTeamKey](A.EmployeeKey),
        //[dbo].[Fn_GetTeamID]([dbo].[LayTeamKey](A.EmployeeKey)),
        //[dbo].[Fn_GetTeamName]([dbo].[LayTeamKey](A.EmployeeKey))";
        //            string zConnectionString = ConnectDataBase.ConnectionString;
        //            try
        //            {
        //                SqlConnection zConnect = new SqlConnection(zConnectionString);
        //                zConnect.Open();
        //                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
        //                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
        //                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
        //                zAdapter.Fill(zTable);
        //                zCommand.Dispose();
        //                zConnect.Close();
        //            }
        //            catch (Exception ex)
        //            {
        //                string zstrMessage = "08" + ex.ToString();
        //            }
        //            return zTable;
        //        }
        #endregion

        public static DataTable List_OrderShare2(DateTime FromDate, DateTime ToDate, int TeamKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT
A.OrderKey,A.OrderIDCompare, A.OrderID, A.OrderDate, A.StageKey,
A.StageID,A.Price,A.StageName,
ROUND(SUM(A.TimeProduct),4) AS TimeShare ,
ROUND(SUM(A.BasketProduct),4) AS BasketShare ,
ROUND(SUM(A.KgProduct),4) AS KgShare ,
ROUND(SUM(A.MoneyShare),0) AS MoneyShare
FROM dbo.FTR_Order_Adjusted A
WHERE A.RecordStatus <> 99 
AND  [dbo].[Fn_TeamKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate) =@TeamKey
AND A.CategoryKey = 1 AND A.Borrow=1 AND A.Share=1
AND A.RecordStatus <> 99
AND A.OrderDate BETWEEN @FromDate AND @ToDate
GROUP BY A.OrderKey,A.OrderIDCompare, A.OrderID,  A.OrderDate, A.Stagekey,
A.StageID,A.Price,A.StageName
ORDER BY A.OrderDate, A.OrderID";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        //        public static DataTable List_OrderShare2(DateTime FromDate, DateTime ToDate, int TeamKey)
        //        {
        //            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
        //            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
        //            DataTable zTable = new DataTable();
        //            string zSQL = @"SELECT
        //A.OrderKey,A.OrderIDCompare, A.OrderID, A.OrderDate, A.StageKey,
        //A.StageID,A.Price,A.StageName,
        //ROUND(SUM(A.TimeProduct),4) AS TimeShare ,
        //ROUND(SUM(A.BasketProduct),4) AS BasketShare ,
        //ROUND(SUM(A.KgProduct),4) AS KgShare ,
        //ROUND(SUM(A.MoneyShare),0) AS MoneyShare
        //FROM dbo.FTR_Order_Adjusted A
        //WHERE A.RecordStatus <> 99 
        //AND  [dbo].Fn_LayTeamKey_TheoThang(A.EmployeeKey,@FromDate) =@TeamKey
        //AND A.CategoryKey = 1 AND A.Borrow=1 AND A.Share=1
        //AND A.RecordStatus <> 99
        //AND A.OrderDate BETWEEN @FromDate AND @ToDate
        //GROUP BY A.OrderKey,A.OrderIDCompare, A.OrderID,  A.OrderDate, A.Stagekey,
        //A.StageID,A.Price,A.StageName
        //ORDER BY A.OrderDate, A.OrderID";
        //            string zConnectionString = ConnectDataBase.ConnectionString;
        //            try
        //            {
        //                SqlConnection zConnect = new SqlConnection(zConnectionString);
        //                zConnect.Open();
        //                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
        //                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
        //                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
        //                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
        //                zAdapter.Fill(zTable);
        //                zCommand.Dispose();
        //                zConnect.Close();
        //            }
        //            catch (Exception ex)
        //            {
        //                string zstrMessage = "08" + ex.ToString();
        //            }
        //            return zTable;
        //        }
        #region[close-14/08/2020-List_OrderShare2-Chuyen tổ nhóm]
        //        public static DataTable List_OrderShare2(DateTime FromDate, DateTime ToDate, int TeamKey)
        //        {
        //            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
        //            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
        //            DataTable zTable = new DataTable();
        //            string zSQL = @"SELECT
        //A.OrderKey,A.OrderIDCompare, A.OrderID, A.OrderDate, A.StageKey,
        //A.StageID,A.Price,A.StageName,
        //ROUND(SUM(A.TimeProduct),2) AS TimeShare ,
        //ROUND(SUM(A.BasketProduct),2) AS BasketShare ,
        //ROUND(SUM(A.KgProduct),2) AS KgShare ,
        //ROUND(SUM(A.MoneyShare),2) AS MoneyShare
        //FROM dbo.FTR_Order_Adjusted A
        //WHERE A.RecordStatus <> 99 
        //AND [dbo].[LayTeamKey](A.EmployeeKey) =@TeamKey
        //AND A.CategoryKey = 1 AND A.Borrow=1 AND A.Share=1
        //AND A.RecordStatus <> 99
        //AND A.OrderDate BETWEEN @FromDate AND @ToDate
        //GROUP BY A.OrderKey,A.OrderIDCompare, A.OrderID,  A.OrderDate, A.Stagekey,
        //A.StageID,A.Price,A.StageName
        //ORDER BY A.OrderDate, A.OrderID";
        //            string zConnectionString = ConnectDataBase.ConnectionString;
        //            try
        //            {
        //                SqlConnection zConnect = new SqlConnection(zConnectionString);
        //                zConnect.Open();
        //                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
        //                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
        //                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
        //                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
        //                zAdapter.Fill(zTable);
        //                zCommand.Dispose();
        //                zConnect.Close();
        //            }
        //            catch (Exception ex)
        //            {
        //                string zstrMessage = "08" + ex.ToString();
        //            }
        //            return zTable;
        //        }
        #endregion

        public static DataTable List_EmployeeShare2(string OrderKey, int TeamKey, DateTime FromDate, DateTime ToDate)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            string zSQL = "";
            DataTable zTable = new DataTable();
            zSQL = @"
--declare @FromDate datetime='2020-04-01 00:00:00'
--declare @ToDate datetime='2020-04-30 23:59:59'

--Nhân sự làm việc trong tháng
CREATE TABLE #EmployeeTemp
(
EmployeeKey NVARCHAR(50),
EmployeeID NVARCHAR(50),
EmployeeName NVARCHAR(500),
BranchKey INT,
DepartmentKey INT ,
TeamKey INT ,
PositionKey INT 
)
INSERT INTO #EmployeeTemp
SELECT EmployeeKey,EmployeeID,EmployeeName,
BranchKey,DepartmentKey,TeamKey, PositionKey
 FROM (
		SELECT EmployeeKey,EmployeeID,[dbo].[Fn_GetFullName](EmployeeKey) AS EmployeeName,StartingDate,
			CASE
				WHEN LeavingDate IS NULL THEN GETDATE()
				ELSE LeavingDate
			END LeavingDate,
            [dbo].[Fn_BranchKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate) AS BranchKey,
            [dbo].[Fn_DepartmentKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate) AS DepartmentKey,
            [dbo].[Fn_TeamKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate) AS TeamKey ,
            [dbo].[Fn_PositionKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate) AS PositionKey
			FROM [dbo].[HRM_Employee] 
			WHERE RecordStatus <>99 
	) X
	WHERE (
            ( StartingDate <=@FromDate AND @FromDate <=  LeavingDate  )
	     OR ( StartingDate <=@ToDate   AND @ToDate   <=  LeavingDate )
        )
    @Customer
----------Kết thúc tạo danh sách nhân sự

--DECLARE @KgShare FLOAT
--DECLARE @MoneyShare FLOAT 

--SELECT @KgShare = KgProduct FROM dbo.FTR_Order_Adjusted WHERE CategoryKey=1 AND Borrow=1 AND Share=1 AND RecordStatus<>99 AND OrderKey=@OrderKey
--SELECT @MoneyShare = MoneyShare FROM dbo.FTR_Order_Adjusted WHERE CategoryKey=1 AND Borrow=1 AND Share=1 AND RecordStatus<>99 AND OrderKey=@OrderKey

SELECT EmployeeKey,EmployeeID ,EmployeeName,
dbo.SumTimeTeam(@FromDate, @ToDate, @TeamKey, EmployeeKey) AS Time,
[dbo].[LaySoGioAnChungDeChiaLaiLan2](EmployeeKey,@OrderKey) AS SoGio,
[dbo].[LaySoRoAnChungDeChiaLaiLan2](EmployeeKey,@OrderKey) AS Basket,
[dbo].[LaySoKiAnChungDeChiaLaiLan2](EmployeeKey,@OrderKey) AS Kg,
[dbo].[LaySoTienAnChungDeChiaLaiLan2](EmployeeKey,@OrderKey) AS [Money]
FROM #EmployeeTemp 
ORDER BY  LEN(EmployeeID),EmployeeID

DROP TABLE #EmployeeTemp
";
            string zFilter = "";

            if (TeamKey != 0)
            {
                zFilter += " AND TeamKey= @TeamKey";
            }
            if (zFilter.Length != 0)
            {
                zSQL = zSQL.Replace("@Customer", zFilter);
            }
            else
            {
                zSQL = zSQL.Replace("@Customer", "");
            }

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = OrderKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        //        public static DataTable List_EmployeeShare2(string OrderKey, int TeamKey, DateTime FromDate, DateTime ToDate)
        //        {
        //            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
        //            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
        //            string zSQL = "";
        //            DataTable zTable = new DataTable();

        //            zSQL = @"
        //DECLARE @KgShare FLOAT
        //DECLARE @MoneyShare FLOAT 

        //SELECT @KgShare = KgProduct FROM dbo.FTR_Order_Adjusted WHERE CategoryKey=1 AND Borrow=1 AND Share=1 AND RecordStatus<>99 AND OrderKey=@OrderKey
        //SELECT @MoneyShare = MoneyShare FROM dbo.FTR_Order_Adjusted WHERE CategoryKey=1 AND Borrow=1 AND Share=1 AND RecordStatus<>99 AND OrderKey=@OrderKey
        //SELECT 
        //A.EmployeeKey, A.EmployeeID ,dbo.Fn_GetFullName(A.EmployeeKey) AS EmployeeName,
        //dbo.SumTimeTeam(@FromDate, @ToDate, @TeamKey, A.EmployeeKey) AS Time,
        //[dbo].[LaySoGioAnChungDeChiaLaiLan2](A.EmployeeKey,@OrderKey) AS SoGio,
        //[dbo].[LaySoRoAnChungDeChiaLaiLan2](A.EmployeeKey,@OrderKey) AS Basket,
        //[dbo].[LaySoKiAnChungDeChiaLaiLan2](A.EmployeeKey,@OrderKey) AS Kg,
        //[dbo].[LaySoTienAnChungDeChiaLaiLan2](A.EmployeeKey,@OrderKey) AS [Money]
        //FROM dbo.HRM_Team_Employee_Month A
        //WHERE A.RecordStatus < 99
        //AND A.TeamKey = @TeamKey 
        //AND (MONTH(A.DateWrite) =MONTH(@FromDate) AND YEAR(A.DateWrite) =YEAR(@FromDate))
        //ORDER BY LEN(A.EmployeeID), A.EmployeeID";

        //            string zConnectionString = ConnectDataBase.ConnectionString;
        //            try
        //            {
        //                SqlConnection zConnect = new SqlConnection(zConnectionString);
        //                zConnect.Open();
        //                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = OrderKey;
        //                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
        //                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
        //                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
        //                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
        //                zAdapter.Fill(zTable);
        //                zCommand.Dispose();
        //                zConnect.Close();
        //            }
        //            catch (Exception ex)
        //            {
        //                string zstrMessage = ex.ToString();
        //            }
        //            return zTable;
        //        }
        #region[close-14/08/2020-List_EmployeeShare2-Chuyen tổ nhóm]
        //        public static DataTable List_EmployeeShare2(string OrderKey, int TeamKey, DateTime FromDate, DateTime ToDate)
        //        {
        //            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
        //            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
        //            string zSQL = "";
        //            DataTable zTable = new DataTable();

        //            zSQL = @"
        //DECLARE @KgShare FLOAT
        //DECLARE @MoneyShare FLOAT 

        //SELECT @KgShare = KgProduct FROM dbo.FTR_Order_Adjusted WHERE CategoryKey=1 AND Borrow=1 AND Share=1 AND RecordStatus<>99 AND OrderKey=@OrderKey
        //SELECT @MoneyShare = MoneyShare FROM dbo.FTR_Order_Adjusted WHERE CategoryKey=1 AND Borrow=1 AND Share=1 AND RecordStatus<>99 AND OrderKey=@OrderKey
        //SELECT 
        //A.EmployeeKey, A.EmployeeID ,dbo.Fn_GetFullName(A.EmployeeKey) AS EmployeeName,
        //dbo.SumTimeTeam(@FromDate, @ToDate, @TeamKey, A.EmployeeKey) AS Time,
        //[dbo].[LaySoGioAnChungDeChiaLaiLan2](A.EmployeeKey,@OrderKey) AS SoGio,
        //[dbo].[LaySoRoAnChungDeChiaLaiLan2](A.EmployeeKey,@OrderKey) AS Basket,
        //[dbo].[LaySoKiAnChungDeChiaLaiLan2](A.EmployeeKey,@OrderKey) AS Kg,
        //[dbo].[LaySoTienAnChungDeChiaLaiLan2](A.EmployeeKey,@OrderKey) AS [Money]
        //FROM dbo.HRM_Employee A
        //WHERE A.RecordStatus < 99
        //AND A.TeamKey = @TeamKey  
        //ORDER BY LEN(A.EmployeeID), A.EmployeeID";

        //            string zConnectionString = ConnectDataBase.ConnectionString;
        //            try
        //            {
        //                SqlConnection zConnect = new SqlConnection(zConnectionString);
        //                zConnect.Open();
        //                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = OrderKey;
        //                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
        //                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
        //                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
        //                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
        //                zAdapter.Fill(zTable);
        //                zCommand.Dispose();
        //                zConnect.Close();
        //            }
        //            catch (Exception ex)
        //            {
        //                string zstrMessage = ex.ToString();
        //            }
        //            return zTable;
        //        }
        #endregion

        #endregion

        //Lay so ki an chung 1 cá nhân đem về lần 1 của 1 đơn hàng
        public static float LaySoKiAnChungChiaLaiLan1(string OrderKey, string EmployeeKey)
        {
            float result = 0;
            string zSQL = @"SELECT ISNULL(dbo.LaySoLuongAnChung(@EmployeeKey, @OrderKey) ,0) ";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = OrderKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                if (float.TryParse(zCommand.ExecuteScalar().ToString(), out result))
                {

                }
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return result;
        }
        //Lay so tiền an chung 1 cá nhân đem về lần 1 của 1 đơn hàng
        public static float LaySoTienAnChungChiaLaiLan1(string OrderKey, string EmployeeKey)
        {
            float result = 0;
            string zSQL = @"SELECT ISNULL(dbo.LaySoTienAnChung(@EmployeeKey, @OrderKey) ,0) ";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = OrderKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                if (float.TryParse(zCommand.ExecuteScalar().ToString(), out result))
                {

                }
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return result;
        }
        //Lay so giờ an chung 1 cá nhân đem về lần 1 của 1 đơn hàng
        public static float LaySoGioAnChungChiaLaiLan1(string OrderKey, string EmployeeKey)
        {
            float result = 0;
            string zSQL = @"SELECT ISNULL(dbo.LaySoGioAnChung(@EmployeeKey, @OrderKey) ,0) ";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = OrderKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                if (float.TryParse(zCommand.ExecuteScalar().ToString(), out result))
                {

                }
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return result;
        }
        //Lay so rổ an chung 1 cá nhân đem về lần 1 của 1 đơn hàng
        public static float LaySoRoAnChungChiaLaiLan1(string OrderKey, string EmployeeKey)
        {
            float result = 0;
            string zSQL = @"SELECT ISNULL(dbo.LaySoRoAnChung(@EmployeeKey, @OrderKey) ,0) ";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = OrderKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                if (float.TryParse(zCommand.ExecuteScalar().ToString(), out result))
                {

                }
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return result;
        }



        //Lay so ki an chung 1 cá nhân đem về sau khi chia xong lần 1 của 1 đơn hàng
        public static float LaySoKiAnChungChiaLaiLan2(string OrderKey, string EmployeeKey)
        {
            float result = 0;
            string zSQL = @"SELECT ISNULL(dbo.LaySoKiAnChungDeChiaLaiLan2(@EmployeeKey, @OrderKey) ,0) ";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = OrderKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                if (float.TryParse(zCommand.ExecuteScalar().ToString(), out result))
                {

                }
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return result;
        }
        //Lay so tiền an chung 1 cá nhân đem về sau khi chia xong lần 1 của 1 đơn hàng
        public static float LaySoTienAnChungChiaLaiLan2(string OrderKey, string EmployeeKey)
        {
            float result = 0;
            string zSQL = @"SELECT ISNULL(dbo.LaySoTienAnChungDeChiaLaiLan2(@EmployeeKey, @OrderKey) ,0) ";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = OrderKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                if (float.TryParse(zCommand.ExecuteScalar().ToString(), out result))
                {

                }
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return result;
        }
        //Lay so giờ an chung 1 cá nhân đem về sau khi chia xong lần 1 của 1 đơn hàng
        public static float LaySoGioAnChungChiaLaiLan2(string OrderKey, string EmployeeKey)
        {
            float result = 0;
            string zSQL = @"SELECT ISNULL(dbo.LaySoGioAnChungDeChiaLaiLan2(@EmployeeKey, @OrderKey) ,0) ";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = OrderKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                if (float.TryParse(zCommand.ExecuteScalar().ToString(), out result))
                {

                }
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return result;
        }
        //Lay so Rổ an chung 1 cá nhân đem về sau khi chia xong lần 1 của 1 đơn hàng
        public static float LaySoRoAnChungChiaLaiLan2(string OrderKey, string EmployeeKey)
        {
            float result = 0;
            string zSQL = @"SELECT ISNULL(dbo.LaySoRoAnChungDeChiaLaiLan2(@EmployeeKey, @OrderKey) ,0) ";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = OrderKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                if (float.TryParse(zCommand.ExecuteScalar().ToString(), out result))
                {

                }
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return result;
        }


        //Lấy danh sách nhân viên của đơn hàng đã chia xong lần 1
        public static DataTable List_Employee1(string OrderKey, int TeamKey)
        {
            string zSQL = "";
            DataTable zTable = new DataTable();

            zSQL = @"SELECT * FROM dbo.FTR_Order_Adjusted 
                    WHERE TeamKey=@TeamKey AND OrderKey=@OrderKey AND CategoryKey=1 AND RecordStatus<> 99
                    ORDER BY Borrow ,LEN(EmployeeID), EmployeeID";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = OrderKey;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        //Lấy danh sách nhân viên của  đơn hàng đã chia xong lần 2
        public static DataTable List_Employee2(string OrderKey, int TeamKey)
        {
            string zSQL = "";
            DataTable zTable = new DataTable();

            zSQL = @"SELECT * FROM dbo.FTR_Order_Adjusted 
                    WHERE TeamKey=@TeamKey AND OrderKey=@OrderKey AND CategoryKey=2 AND RecordStatus<> 99
                    ORDER BY Borrow ,LEN(EmployeeID), EmployeeID";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = OrderKey;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }



        //Form  OrderView
        //Lấy danh sách nhân viên của đơn hàng đã chia xong lần 1
        public static DataTable List_NhanSuChiaLaiLan1_Cua1DonHang(string OrderKey,DateTime Datetime)
        {
            DateTime zFromDate = new DateTime(Datetime.Year, Datetime.Month, Datetime.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(Datetime.Year, Datetime.Month, Datetime.Day, 23, 59, 59);
            string zSQL = "";
            DataTable zTable = new DataTable();

            zSQL = @"SELECT EmployeeKey,EmployeeID,EmployeeName,
[dbo].[Fn_GetTeamID]([dbo].[Fn_TeamKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate)) AS TeamID,
Time,Borrow,Private,Share,TimeEat,TimeProduct,Basket,BasketProduct,
Kg,KgProduct,Money,
CASE 
WHEN Share =0 THEN MoneyPersonal
WHEN Share=1 THEN MoneyShare
END AS MoneyProduct FROM dbo.FTR_Order_Adjusted 
                    WHERE  OrderKey= @OrderKey AND CategoryKey=1 AND RecordStatus<> 99
                    ORDER BY Borrow,TeamID ,LEN(EmployeeID), EmployeeID";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = OrderKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        //Lấy danh sách nhân viên của đơn hàng đã chia xong lần 2
        public static DataTable List_NhanSuChiaLaiLan2_Cua1DonHang(string OrderKey, DateTime Datetime)
        {
            DateTime zFromDate = new DateTime(Datetime.Year, Datetime.Month, Datetime.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(Datetime.Year, Datetime.Month, Datetime.Day, 23, 59, 59);
            string zSQL = "";
            DataTable zTable = new DataTable();

            zSQL = @"SELECT EmployeeKey,EmployeeID,EmployeeName,
[dbo].[Fn_GetTeamID]([dbo].[Fn_TeamKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate)) AS TeamID,
Time,Borrow,Private,Share,TimeEat,TimeProduct,Basket,BasketProduct,
Kg,KgProduct,Money,
CASE 
WHEN Share =0 THEN MoneyPersonal
WHEN Share=1 THEN MoneyShare
END AS MoneyProduct  FROM dbo.FTR_Order_Adjusted 
                    WHERE  OrderKey=@OrderKey AND CategoryKey=2 AND RecordStatus<> 99
                    ORDER BY Borrow,TeamID ,LEN(EmployeeID), EmployeeID";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = OrderKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }


        //Danh sách đơn hàng chia lại còn thiếu lần 1
        public static DataTable SoLuongDongHangChiaNangSuat(DateTime FromDate, DateTime ToDate)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            DataTable zTable = new DataTable();
            string zSQL = @"
--DECLARE @FromDate DATETIME='2021-01-01 00:00:00'
--DECLARE @ToDate DATETIME='2021-01-31 23:59:59'

CREATE TABLE #DSCANCHIA
(
OrderDate DATETIME ,
OrderKey NVARCHAR(50),
OrderID NVARCHAR(50),
TeamKey INT,--Mã nhóm của người ăn chung
TeamID NVARCHAR(50),
TeamName NVARCHAR(500),
)
---Nhóm và đơn hàng cần chia lại lần 1
INSERT INTO #DSCANCHIA
		SELECT
		A.OrderDate,A.OrderKey, A.OrderID,
        A.TeamKey,
        [dbo].[Fn_GetTeamID](A.TeamKey) AS TeamID,
        [dbo].[Fn_GetTeamName](A.TeamKey) AS TeamName
        FROM  FTR_Order A
        WHERE A.RecordStatus <> 99 
        AND A.OrderDate BETWEEN @FromDate AND @ToDate
        GROUP BY A.OrderDate,A.OrderKey,A.OrderID,
		A.TeamKey,
       [dbo].[Fn_GetTeamID](A.TeamKey),
       [dbo].[Fn_GetTeamName](A.TeamKey)

SELECT CAST(A.TeamKey AS NVARCHAR(50))+'|'+A.TeamID+'|'+TeamName  AS LeftColumn ,DAY(OrderDate)  AS HeaderColumn ,COUNT(*) AS Amount FROM #DSCANCHIA A
WHERE  A.OrderKey NOT IN (
		--Đối chiếu với bảng đơn hàng đã chia lại rồi
		SELECT H.OrderKey FROM 
		[dbo].[FTR_Order_Money] H
		WHERE H.RecordStatus <> 99  
		AND H.OrderKey =A.OrderKey AND  H.TeamKey =A.TeamKey
		GROUP BY H.OrderKey, H.TeamKey
						)
GROUP BY  DAY(OrderDate),TeamKey,TeamID,TeamName
HAVING COUNT(*) >0
ORDER BY HeaderColumn

DROP TABLE  #DSCANCHIA";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable SoLuongDongHangChiaLaiConThieu_Lan1 (DateTime FromDate, DateTime ToDate)
        {
            
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            DataTable zTable = new DataTable();
            string zSQL = @"
--DECLARE @FromDate DATETIME='2021-01-01 00:00:00'
--DECLARE @ToDate DATETIME='2021-01-10 23:59:59'

CREATE TABLE #DSCANCHIA
(
OrderDate DATETIME ,
OrderKey NVARCHAR(50),
OrderID NVARCHAR(50),
TeamKey INT,--Mã nhóm của người ăn chung
TeamID NVARCHAR(50),
TeamName NVARCHAR(500),
)
---Nhóm và đơn hàng cần chia lại lần 1
INSERT INTO #DSCANCHIA
		SELECT
		A.OrderDate,B.OrderKey, B.OrderID,
        [dbo].[Fn_TeamKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate) AS TeamKey,
        [dbo].[Fn_GetTeamID]([dbo].[Fn_TeamKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate)) AS TeamID,
        [dbo].[Fn_GetTeamName]([dbo].[Fn_TeamKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate)) AS TeamName
        FROM  FTR_Order_Money A
		LEFT JOIN FTR_Order B ON A.OrderKey = B.OrderKey
        WHERE A.RecordStatus <> 99  AND B.RecordStatus <> 99
        AND A.Money_Eat > 0 
        AND A.OrderDate BETWEEN @FromDate AND @ToDate
        GROUP BY A.OrderDate,B.OrderKey,B.OrderID,
		[dbo].[Fn_TeamKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate),
        [dbo].[Fn_GetTeamID]([dbo].[Fn_TeamKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate)),
        [dbo].[Fn_GetTeamName]([dbo].[Fn_TeamKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate))

SELECT CAST(A.TeamKey AS NVARCHAR(50))+'|'+A.TeamID+'|'+TeamName  AS LeftColumn ,DAY(OrderDate)  AS HeaderColumn ,COUNT(*) AS Amount FROM #DSCANCHIA A
WHERE  A.OrderKey NOT IN (
		--Đối chiếu với bảng đơn hàng đã chia lại rồi
		SELECT H.OrderKey FROM [dbo].[FTR_Order_Adjusted] H
		WHERE RecordStatus <> 99  
		AND H.OrderKey =A.OrderKey AND  H.TeamKey =A.TeamKey AND H.CategoryKey=1
		GROUP BY H.OrderKey, H.TeamKey
						)
GROUP BY  DAY(OrderDate),TeamKey,TeamID,TeamName
HAVING COUNT(*) >0
ORDER BY HeaderColumn
DROP TABLE  #DSCANCHIA";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable SoLuongDongHangChiaLaiConThieu_Lan2(DateTime FromDate, DateTime ToDate)
        {

            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            DataTable zTable = new DataTable();
            string zSQL = @"
--DECLARE @FromDate DATETIME='2021-01-01 00:00:00'
--DECLARE @ToDate DATETIME='2021-01-10 23:59:59'

CREATE TABLE #DSCANCHIA
(
OrderDate DATETIME ,
OrderKey NVARCHAR(50),
OrderID NVARCHAR(50),
TeamKey INT,--Mã nhóm của người ăn chung
TeamID NVARCHAR(50),
TeamName NVARCHAR(500),
)
---Nhóm và đơn hàng cần chia lại lần 2
INSERT INTO #DSCANCHIA
		SELECT
		A.OrderDate,A.OrderKey, A.OrderID,
		[dbo].[Fn_TeamKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate) AS TeamKey,
		[dbo].[Fn_GetTeamID]( [dbo].[Fn_TeamKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate)) AS TeamID,
		[dbo].[Fn_GetTeamName]( [dbo].[Fn_TeamKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate)) AS TeamName
		FROM  dbo.FTR_Order_Adjusted A
		WHERE A.RecordStatus <> 99 
		AND A.CategoryKey  =1  AND A.Borrow=1 AND A.Share=1
		AND A.OrderDate BETWEEN @FromDate AND @ToDate
		GROUP BY  A.OrderDate,A.OrderKey, A.OrderID,
		[dbo].[Fn_TeamKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate),
		[dbo].[Fn_GetTeamID]( [dbo].[Fn_TeamKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate)),
		[dbo].[Fn_GetTeamName]([dbo].[Fn_TeamKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate))

SELECT CAST(A.TeamKey AS NVARCHAR(50))+'|'+A.TeamID+'|'+TeamName  AS LeftColumn ,DAY(OrderDate)  AS HeaderColumn ,COUNT(*) AS Amount FROM #DSCANCHIA A
WHERE  A.OrderKey NOT IN (
		--Đối chiếu với bảng đơn hàng đã chia lại rồi
		SELECT H.OrderKey FROM [dbo].[FTR_Order_Adjusted] H
		WHERE RecordStatus <> 99  
		AND H.OrderKey =A.OrderKey AND  H.TeamKey =A.TeamKey AND H.CategoryKey=2
		GROUP BY H.OrderKey, H.TeamKey
						)
GROUP BY  DAY(OrderDate),TeamKey,TeamID,TeamName
HAVING COUNT(*) >0
ORDER BY HeaderColumn

DROP TABLE #DSCANCHIA";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
    }
}
