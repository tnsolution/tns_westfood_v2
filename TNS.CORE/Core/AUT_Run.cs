﻿using System;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
using TNS.Misc;

namespace TNS.CORE
{
    public class AUT_Run
    {
        public int AutoKey { get; set; } = 0;
        public string Month { get; set; } = string.Empty;
        public string Title { get; set; } = string.Empty;
        public string Error { get; set; } = string.Empty;
        public string CreatedBy { get; set; } = string.Empty;
        public int RecordStatus { get; set; } = 0;
        public void Insert()
        {
            string zSQL = @" 
INSERT INTO 
AUT_Run (CreateDate, Month, Error, Title, RecordStatus, TimeBegin, TimeEnd, CreatedBy) 
VALUES (GETDATE(), @Month, @Error, @Title, @RecordStatus, GETDATE(), NULL, @CreatedBy)
SELECT AutoKey FROM AUT_Run WHERE AutoKey = SCOPE_IDENTITY()
";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;

                zCommand.Parameters.Add("@Month", SqlDbType.NVarChar).Value = Month;
                zCommand.Parameters.Add("@Error", SqlDbType.NVarChar).Value = Error;
                zCommand.Parameters.Add("@Title", SqlDbType.NVarChar).Value = Title;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = CreatedBy;
                AutoKey = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        public void Update()
        {
            string zSQL = @" 
UPDATE AUT_Run SET 
TimeEnd = GETDATE(), 
Error = @Error,
RecordStatus = @RecordStatus
WHERE AutoKey = @AutoKey";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                zCommand.Parameters.Add("@Error", SqlDbType.NVarChar).Value = Error;              
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.NVarChar).Value = RecordStatus;

                zCommand.ExecuteNonQuery();
                zCommand.Dispose();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        public static void Delete(string CreatedBy)
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE AUT_Run WHERE CreatedBy = @CreatedBy";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = CreatedBy;
                zCommand.ExecuteNonQuery();
                zCommand.Dispose();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
    }
}