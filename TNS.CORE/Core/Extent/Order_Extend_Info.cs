﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
namespace TNS.CORE
{
    public class Order_Extend_Info
    {

        #region [ Field Name ]
        private string _ExtendKey = "";
        private string _OrderKey = "";
        private string _OrderID = "";
        private string _OrderID_Compare = "";
        private DateTime _OrderDate;
        private float _OrderTime;
        private float _OrderKg;
        private float _WorkTime;
        private string _ProductKey = "";
        private string _ProductID = "";
        private string _ProductName = "";
        private int _TeamKey = 0;
        private string _TeamID = "";
        private string _TeamName = "";
        private int _WorkKey = 0;
        private string _WorkID = "";
        private string _WorkName = "";
        private int _GroupKey = 0;
        private string _GroupID = "";
        private int _RecordStatus = 0;
        private DateTime _CreatedOn;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _RoleID = "";
        private string _Message = "";
        #endregion
        #region [ Properties ]
        public bool IsCommandOK
        {
            get
            {
                int zNumber = 0;
                int.TryParse(_Message.Substring(0, 1), out zNumber);
                if (zNumber <= 3) return true; else return false;
            }
        }
        public bool HasInfo
        {
            get
            {
                if (_ExtendKey.Trim().Length > 0) return true; else return false;
            }
        }
        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        public string Key
        {
            get { return _ExtendKey; }
            set { _ExtendKey = value; }
        }
        public string OrderKey
        {
            get { return _OrderKey; }
            set { _OrderKey = value; }
        }
        public string OrderID
        {
            get { return _OrderID; }
            set { _OrderID = value; }
        }
        public string OrderID_Compare
        {
            get { return _OrderID_Compare; }
            set { _OrderID_Compare = value; }
        }
        public DateTime OrderDate
        {
            get { return _OrderDate; }
            set { _OrderDate = value; }
        }
        public float OrderTime
        {
            get { return _OrderTime; }
            set { _OrderTime = value; }
        }
        public float OrderKg
        {
            get { return _OrderKg; }
            set { _OrderKg = value; }
        }
        public float WorkTime
        {
            get { return _WorkTime; }
            set { _WorkTime = value; }
        }
        public string ProductKey
        {
            get { return _ProductKey; }
            set { _ProductKey = value; }
        }
        public string ProductID
        {
            get { return _ProductID; }
            set { _ProductID = value; }
        }
        public string ProductName
        {
            get { return _ProductName; }
            set { _ProductName = value; }
        }
        public int TeamKey
        {
            get { return _TeamKey; }
            set { _TeamKey = value; }
        }
        public string TeamID
        {
            get { return _TeamID; }
            set { _TeamID = value; }
        }
        public string TeamName
        {
            get { return _TeamName; }
            set { _TeamName = value; }
        }
        public int WorkKey
        {
            get { return _WorkKey; }
            set { _WorkKey = value; }
        }
        public string WorkID
        {
            get { return _WorkID; }
            set { _WorkID = value; }
        }
        public string WorkName
        {
            get { return _WorkName; }
            set { _WorkName = value; }
        }
        public int GroupKey
        {
            get { return _GroupKey; }
            set { _GroupKey = value; }
        }
        public string GroupID
        {
            get { return _GroupID; }
            set { _GroupID = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion
        #region [ Constructor Get Information ]
        public Order_Extend_Info()
        {
        }
        public Order_Extend_Info(string ExtendKey)
        {
            string zSQL = "SELECT * FROM FTR_Order_Extend WHERE ExtendKey = @ExtendKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ExtendKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(ExtendKey);
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _ExtendKey = zReader["ExtendKey"].ToString();
                    _OrderKey = zReader["OrderKey"].ToString();
                    _OrderID = zReader["OrderID"].ToString().Trim();
                    _OrderID_Compare = zReader["OrderID_Compare"].ToString().Trim();
                    if (zReader["OrderDate"] != DBNull.Value)
                        _OrderDate = (DateTime)zReader["OrderDate"];
                    if (zReader["OrderTime"] != DBNull.Value)
                        _OrderTime = float.Parse(zReader["OrderTime"].ToString());
                    if (zReader["OrderKg"] != DBNull.Value)
                        _OrderKg = float.Parse(zReader["OrderKg"].ToString());
                    if (zReader["WorkTime"] != DBNull.Value)
                        _WorkTime = float.Parse(zReader["WorkTime"].ToString());
                    _ProductKey = zReader["ProductKey"].ToString().Trim();
                    _ProductID = zReader["ProductID"].ToString().Trim();
                    _ProductName = zReader["ProductName"].ToString().Trim();
                    if (zReader["TeamKey"] != DBNull.Value)
                        _TeamKey = int.Parse(zReader["TeamKey"].ToString());
                    _TeamID = zReader["TeamID"].ToString().Trim();
                    _TeamName = zReader["TeamName"].ToString().Trim();
                    if (zReader["WorkKey"] != DBNull.Value)
                        _WorkKey = int.Parse(zReader["WorkKey"].ToString());
                    _WorkID = zReader["WorkID"].ToString().Trim();
                    _WorkName = zReader["WorkName"].ToString().Trim();
                    if (zReader["GroupKey"] != DBNull.Value)
                        _GroupKey = int.Parse(zReader["GroupKey"].ToString());
                    _GroupID = zReader["GroupID"].ToString().Trim();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO FTR_Order_Extend ("
        + " OrderKey ,OrderID ,OrderID_Compare ,OrderDate ,OrderTime ,OrderKg ,WorkTime ,ProductKey ,ProductID ,ProductName ,TeamKey ,TeamID ,TeamName ,WorkKey ,WorkID ,WorkName ,GroupKey ,GroupID ,RecordStatus ,CreatedOn ,CreatedBy ,CreatedName ,ModifiedOn ,ModifiedBy ,ModifiedName ) "
         + " VALUES ( "
         + "@OrderKey ,@OrderID ,@OrderID_Compare ,@OrderDate ,@OrderTime ,@OrderKg ,@WorkTime ,@ProductKey ,@ProductID ,@ProductName ,@TeamKey ,@TeamID ,@TeamName ,@WorkKey ,@WorkID ,@WorkName ,@GroupKey ,@GroupID ,@RecordStatus ,GETDATE(),@CreatedBy ,@CreatedName ,GETDATE(),@ModifiedBy ,@ModifiedName ) ";

            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (_OrderKey.Length > 0)
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_OrderKey);
                else
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = _OrderID.Trim();
                zCommand.Parameters.Add("@OrderID_Compare", SqlDbType.NVarChar).Value = _OrderID_Compare.Trim();
                if (_OrderDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = _OrderDate;
                zCommand.Parameters.Add("@OrderTime", SqlDbType.Float).Value = _OrderTime;
                zCommand.Parameters.Add("@OrderKg", SqlDbType.Float).Value = _OrderKg;
                zCommand.Parameters.Add("@WorkTime", SqlDbType.Float).Value = _WorkTime;
                zCommand.Parameters.Add("@ProductKey", SqlDbType.NVarChar).Value = _ProductKey.Trim();
                zCommand.Parameters.Add("@ProductID", SqlDbType.NVarChar).Value = _ProductID.Trim();
                zCommand.Parameters.Add("@ProductName", SqlDbType.NVarChar).Value = _ProductName.Trim();
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = _TeamKey;
                zCommand.Parameters.Add("@TeamID", SqlDbType.NVarChar).Value = _TeamID.Trim();
                zCommand.Parameters.Add("@TeamName", SqlDbType.NVarChar).Value = _TeamName.Trim();
                zCommand.Parameters.Add("@WorkKey", SqlDbType.Int).Value = _WorkKey;
                zCommand.Parameters.Add("@WorkID", SqlDbType.NVarChar).Value = _WorkID.Trim();
                zCommand.Parameters.Add("@WorkName", SqlDbType.NVarChar).Value = _WorkName.Trim();
                zCommand.Parameters.Add("@GroupKey", SqlDbType.Int).Value = _GroupKey;
                zCommand.Parameters.Add("@GroupID", SqlDbType.NVarChar).Value = _GroupID.Trim();
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                if (_Message.Substring(0, 2) == "11")
                    _ExtendKey = _Message.Substring(2, 36);
                else
                    _ExtendKey = "";
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE FTR_Order_Extend SET "
                        + " OrderKey = @OrderKey,"
                        + " OrderID = @OrderID,"
                        + " OrderID_Compare = @OrderID_Compare,"
                        + " OrderDate = @OrderDate,"
                        + " OrderTime = @OrderTime,"
                        + " OrderKg = @OrderKg,"
                        + " WorkTime = @WorkTime,"
                        + " ProductKey = @ProductKey,"
                        + " ProductID = @ProductID,"
                        + " ProductName = @ProductName,"
                        + " TeamKey = @TeamKey,"
                        + " TeamID = @TeamID,"
                        + " TeamName = @TeamName,"
                        + " WorkKey = @WorkKey,"
                        + " WorkID = @WorkID,"
                        + " WorkName = @WorkName,"
                        + " GroupKey = @GroupKey,"
                        + " GroupID = @GroupID,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = @ModifiedOn,"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                       + " WHERE ExtendKey = @ExtendKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (_ExtendKey.Length == 36)
                    zCommand.Parameters.Add("@ExtendKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_ExtendKey);
                else
                    zCommand.Parameters.Add("@ExtendKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                if (_OrderKey.Length == 36)
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_OrderKey);
                else
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = _OrderID.Trim();
                zCommand.Parameters.Add("@OrderID_Compare", SqlDbType.NVarChar).Value = _OrderID_Compare.Trim();
                if (_OrderDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = _OrderDate;
                zCommand.Parameters.Add("@OrderTime", SqlDbType.Float).Value = _OrderTime;
                zCommand.Parameters.Add("@OrderKg", SqlDbType.Float).Value = _OrderKg;
                zCommand.Parameters.Add("@WorkTime", SqlDbType.Float).Value = _WorkTime;
                zCommand.Parameters.Add("@ProductKey", SqlDbType.NVarChar).Value = _ProductKey.Trim();
                zCommand.Parameters.Add("@ProductID", SqlDbType.NVarChar).Value = _ProductID.Trim();
                zCommand.Parameters.Add("@ProductName", SqlDbType.NVarChar).Value = _ProductName.Trim();
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = _TeamKey;
                zCommand.Parameters.Add("@TeamID", SqlDbType.NVarChar).Value = _TeamID.Trim();
                zCommand.Parameters.Add("@TeamName", SqlDbType.NVarChar).Value = _TeamName.Trim();
                zCommand.Parameters.Add("@WorkKey", SqlDbType.Int).Value = _WorkKey;
                zCommand.Parameters.Add("@WorkID", SqlDbType.NVarChar).Value = _WorkID.Trim();
                zCommand.Parameters.Add("@WorkName", SqlDbType.NVarChar).Value = _WorkName.Trim();
                zCommand.Parameters.Add("@GroupKey", SqlDbType.Int).Value = _GroupKey;
                zCommand.Parameters.Add("@GroupID", SqlDbType.NVarChar).Value = _GroupID.Trim();
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE FTR_Order_Extend SET [RecordStatus] = 99, [ModifiedOn] = GETDATE(), ModifiedBy = @ModifiedBy, ModifiedName = @ModifiedName WHERE ExtendKey = @ExtendKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ExtendKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_ExtendKey);
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_ExtendKey.Trim().Length == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        #endregion
    }
}
