﻿using System;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
using TNS.Misc;

namespace TNS.CORE
{
    public class Report
    {
        public static DataTable No1(DateTime FromDate, DateTime ToDate, int TeamKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string PivotDate = "";
            for (int i = FromDate.Day; i <= ToDate.Day; i++)
            {
                PivotDate += "[" + i + "],";
            }
            if (PivotDate.Contains(","))
            {
                PivotDate = PivotDate.Remove(PivotDate.LastIndexOf(","));
            }

            DataTable zTable = new DataTable();
            string zSQL = @"
            SELECT * FROM (
			    SELECT EmployeeName, EmployeeID, DAY(Date) AS [Time], SUM(Total) + dbo.ThanhTienNgoaiGio(EmployeeKey, Date) AS Total 
                FROM FTR_Money_Date 
			    WHERE 
                TeamKey = @TeamKey AND (Date BETWEEN @FromDate AND @ToDate)
			    GROUP BY EmployeeKey,Date, EmployeeName ,EmployeeID, DAY(Date)) 
            X PIVOT (
                 SUM(Total)
				 FOR [Time] IN (" + PivotDate + ")) P ORDER BY LEN(EmployeeID), EmployeeID";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable No1_V2(DateTime FromDate, DateTime ToDate, int TeamKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string PivotDate = "";
            for (int i = FromDate.Day; i <= ToDate.Day; i++)
            {
                PivotDate += "[" + i + "],";
            }
            if (PivotDate.Contains(","))
            {
                PivotDate = PivotDate.Remove(PivotDate.LastIndexOf(","));
            }

            DataTable zTable = new DataTable();
            string zSQL = @"
        ;WITH #temp AS (
            SELECT * FROM (
	                    (SELECT TeamKey,[dbo].[Fn_GetTeamID](TeamKey) AS TeamID,[dbo].[Fn_GetTeamName](TeamKey) AS TeamName,EmployeeName, EmployeeID, DAY(Date) AS [Time], Total AS Total 
                        FROM FTR_Money_Date 
	                    WHERE RecordStatus <> 99 
                        AND (Date BETWEEN @FromDate AND @ToDate)
                        @CustomParamater
	                    ) 
	                    UNION
	                    (SELECT [dbo].[Fn_GetOptionChangeTeamKey](EmployeeKey,@FromDate),[dbo].[Fn_GetTeamID]([dbo].[Fn_GetOptionChangeTeamKey](EmployeeKey,@FromDate)) AS TeamID,[dbo].[Fn_GetTeamName]([dbo].[Fn_GetOptionChangeTeamKey](EmployeeKey,@FromDate)) AS TeamName, EmployeeName,EmployeeID,Day(DateImport),[Money] AS Total 
	                    FROM [dbo].[Temp_Import_Detail]
	                    WHERE RecordStatus <> 99 
                        AND (DateImport BETWEEN @FromDate AND @ToDate)
                        @Extent  )
	) 
            X PIVOT (
                 SUM(Total)
				 FOR [Time] IN (" + PivotDate + ")) P ";
            zSQL += @")
SELECT A.* FROM #temp A
LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
ORDER BY D.RANK,C.RANK,B.RANK,LEN(EmployeeID), EmployeeID";
            if (TeamKey > 0)
            {
                zSQL = zSQL.Replace("@CustomParamater", " AND TeamKey = @TeamKey ");
                zSQL = zSQL.Replace("@Extent", " AND dbo.Fn_LayTeamKey_TheoThang(EmployeeKey,@FromDate) = @TeamKey  ");
            }
            else
            {
                zSQL = zSQL.Replace("@CustomParamater", "");
                zSQL = zSQL.Replace("@Extent", "");
            }
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }

        //        public static DataTable No1_V3(DateTime FromDate, DateTime ToDate, int TeamKey, string Search)
        //        {
        //            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
        //            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

        //            string PivotDate = "";
        //            for (int i = FromDate.Day; i <= ToDate.Day; i++)
        //            {
        //                PivotDate += "[" + i + "],";
        //            }
        //            if (PivotDate.Contains(","))
        //            {
        //                PivotDate = PivotDate.Remove(PivotDate.LastIndexOf(","));
        //            }

        //            DataTable zTable = new DataTable();
        //            string zSQL = @"
        //CREATE TABLE #temp(
        //DateWork datetime,
        //TeamKey int,
        //EmployeeKey nvarchar(50),
        //EmployeeID nvarchar(50),
        //EmployeeName nvarchar(250),
        //[MoneyDate] money
        //)
        //--Nhóm chính
        //INSERT INTO #temp (DateWork,TeamKey,EmployeeKey,EmployeeID,EmployeeName,[MoneyDate])
        //SELECT OrderDate,[dbo].[Fn_GetOptionChangeTeamKey](EmployeeKey,@FromDate),EmployeeKey,EmployeeID,EmployeeName,SUM([Money] + MoneyPrivate +Money_Borrow) 
        //FROM [dbo].[FTR_Order_Money]  
        //WHERE  OrderDate BETWEEN @FROMDATE AND @TODATE 
        //AND RecordStatus <>99  @CustomParamater  --AND EmployeeID in ('108')
        //GROUP BY OrderDate,TeamKey,EmployeeKey,EmployeeID,EmployeeName

        //--Chia lại
        //INSERT INTO #temp (DateWork,TeamKey,EmployeeKey,EmployeeID,EmployeeName,[MoneyDate])
        //SELECT OrderDate,[dbo].[Fn_GetOptionChangeTeamKey](EmployeeKey,@FromDate),EmployeeKey,EmployeeID,EmployeeName,Sum(MoneyPersonal) 
        //FROM [dbo].[FTR_Order_Adjusted]
        //WHERE  OrderDate BETWEEN  @FromDate AND  @ToDate 
        //AND Share=0 AND RecordStatus <>99  @CustomParamater --AND EmployeeID in ('108')
        //GROUP BY OrderDate,TeamKey,EmployeeKey,EmployeeID,EmployeeName

        //--Giờ dư
        //INSERT INTO #temp (DateWork,TeamKey,EmployeeKey,EmployeeID,EmployeeName,[MoneyDate])
        //SELECT DateImport,[dbo].[Fn_GetOptionChangeTeamKey](EmployeeKey,@FromDate),EmployeeKey,EmployeeID,EmployeeName,[Money] from [dbo].[Temp_Import_Detail]
        //WHERE  DateImport BETWEEN @FROMDATE AND @TODATE  
        //AND RecordStatus <>99  @CustomParamater  --AND EmployeeID in ('108')

        //CREATE TABLE #temp2(
        //DateWork datetime,
        //TeamKey int,
        //EmployeeKey nvarchar(50),
        //EmployeeID nvarchar(50),
        //EmployeeName nvarchar(250),
        //Total money
        //)
        //--Sum tổng lương 1 ngày
        //INSERT INTO #temp2 (DateWork,TeamKey,EmployeeKey,EmployeeID,EmployeeName,Total)
        //SELECT DateWork,TeamKey,EmployeeKey,EmployeeID,EmployeeName, SUM([MoneyDate]) FROM #temp 
        //GROUP BY DateWork,TeamKey,EmployeeKey,EmployeeID,EmployeeName

        //--Pivot theo ngày

        //;WITH #BangTam AS (
        //	SELECT * FROM(
        //	SELECT TeamKey,[dbo].[Fn_GetTeamID](TeamKey) AS TeamID,[dbo].[Fn_GetTeamName](TeamKey) AS TeamName,EmployeeID,EmployeeName,Day(DateWork) as [Time],Total FROM #temp2 WHERE Total >0)
        //	X PIVOT (
        //	SUM(Total)
        //	FOR [Time] IN @DetaiPivot ) P 
        //)
        //--Sắp xếp
        //SELECT A.* FROM #BangTam A
        //LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
        //LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
        //LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
        //ORDER BY D.RANK,C.RANK,B.RANK,LEN(EmployeeID), EmployeeID
        //DROP TABLE #temp
        //DROP TABLE #temp2
        //";

        //            zSQL = zSQL.Replace("@DetaiPivot","("+PivotDate+")");

        //            string Fillter = "";
        //            if (TeamKey > 0)
        //            {
        //                Fillter += " AND [dbo].[Fn_GetOptionChangeTeamKey](EmployeeKey,@FromDate) =@TeamKey";
        //            }
        //            if(Search.Trim().Length>0)
        //            {
        //                Fillter += " AND EmployeeID IN ('"+ Search.Trim() + "') ";
        //            }
        //            if(Fillter=="")
        //            {
        //                zSQL = zSQL.Replace("@CustomParamater", "");
        //            }
        //            else
        //            {
        //                zSQL = zSQL.Replace("@CustomParamater", Fillter);
        //            }
        //            string zConnectionString = ConnectDataBase.ConnectionString;
        //            try
        //            {
        //                SqlConnection zConnect = new SqlConnection(zConnectionString);
        //                zConnect.Open();
        //                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
        //                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
        //                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
        //                //zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search.Trim() + "%";
        //                zCommand.CommandTimeout = 350;
        //                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
        //                zAdapter.Fill(zTable);
        //                zCommand.Dispose();
        //                zConnect.Close();
        //            }
        //            catch (Exception ex)
        //            {
        //                string zstrMessage = "08" + ex.ToString();
        //            }
        //            return zTable;
        //        }
        public static DataTable No1_V4(DateTime FromDate, DateTime ToDate, int TeamKey, string Search)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string PivotDate = "";
            for (int i = FromDate.Day; i <= ToDate.Day; i++)
            {
                PivotDate += "[" + i + "],";
            }
            if (PivotDate.Contains(","))
            {
                PivotDate = PivotDate.Remove(PivotDate.LastIndexOf(","));
            }

            DataTable zTable = new DataTable();
            string zSQL = @"
CREATE TABLE #temp(
DateWork datetime,
TeamKey int,
EmployeeKey nvarchar(50),
EmployeeID nvarchar(50),
EmployeeName nvarchar(250),
[MoneyDate] money
)
--Nhóm chính
INSERT INTO #temp (DateWork,TeamKey,EmployeeKey,EmployeeID,EmployeeName,[MoneyDate])
SELECT OrderDate,[dbo].[Fn_TeamKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate),EmployeeKey,EmployeeID,EmployeeName,SUM([Money] + MoneyPrivate +Money_Borrow) 
FROM [dbo].[FTR_Order_Money]  
WHERE  OrderDate BETWEEN @FROMDATE AND @TODATE 
AND RecordStatus <>99  @CustomParamater  --AND EmployeeID in ('108')
GROUP BY OrderDate,TeamKey,EmployeeKey,EmployeeID,EmployeeName

--Chia lại
INSERT INTO #temp (DateWork,TeamKey,EmployeeKey,EmployeeID,EmployeeName,[MoneyDate])
SELECT OrderDate,[dbo].[Fn_TeamKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate),EmployeeKey,EmployeeID,EmployeeName,Sum(MoneyPersonal) 
FROM [dbo].[FTR_Order_Adjusted]
WHERE  OrderDate BETWEEN  @FromDate AND  @ToDate 
AND Share=0 AND RecordStatus <>99  @CustomParamater --AND EmployeeID in ('108')
GROUP BY OrderDate,TeamKey,EmployeeKey,EmployeeID,EmployeeName

--Giờ dư
INSERT INTO #temp (DateWork,TeamKey,EmployeeKey,EmployeeID,EmployeeName,[MoneyDate])
SELECT DateImport,[dbo].[Fn_TeamKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate),EmployeeKey,EmployeeID,EmployeeName,[Money] from [dbo].[Temp_Import_Detail]
WHERE  DateImport BETWEEN @FROMDATE AND @TODATE  
AND RecordStatus <>99  @CustomParamater  --AND EmployeeID in ('108')

CREATE TABLE #temp2(
DateWork datetime,
TeamKey int,
EmployeeKey nvarchar(50),
EmployeeID nvarchar(50),
EmployeeName nvarchar(250),
Total money
)
--Sum tổng lương 1 ngày
INSERT INTO #temp2 (DateWork,TeamKey,EmployeeKey,EmployeeID,EmployeeName,Total)
SELECT DateWork,TeamKey,EmployeeKey,EmployeeID,EmployeeName, SUM([MoneyDate]) FROM #temp 
GROUP BY DateWork,TeamKey,EmployeeKey,EmployeeID,EmployeeName

--Pivot theo ngày

;WITH BangTam AS (
	SELECT * FROM(
	SELECT TeamKey,[dbo].[Fn_GetTeamID](TeamKey) AS TeamID,[dbo].[Fn_GetTeamName](TeamKey) AS TeamName,EmployeeID,EmployeeName,Day(DateWork) as [Time],Total FROM #temp2 WHERE Total >0)
	X PIVOT (
	SUM(Total)
	FOR [Time] IN @DetaiPivot ) P 
)
--Sắp xếp
SELECT A.*,D.RANK BranchRank,C.RANK AS DepartmentRank, B.RANK AS TeamRank FROM BangTam A
LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
ORDER BY D.RANK,C.RANK,B.RANK,LEN(EmployeeID), EmployeeID
DROP TABLE #temp
DROP TABLE #temp2
";

            zSQL = zSQL.Replace("@DetaiPivot", "(" + PivotDate + ")");

            string Fillter = "";
            if (TeamKey > 0)
            {
                Fillter += " AND [dbo].[Fn_TeamKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate) =@TeamKey";
            }
            if (Search.Trim().Length > 0)
            {
                Fillter += " AND EmployeeID IN ('" + Search.Trim() + "') ";
            }
            if (Fillter == "")
            {
                zSQL = zSQL.Replace("@CustomParamater", "");
            }
            else
            {
                zSQL = zSQL.Replace("@CustomParamater", Fillter);
            }
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                //zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search.Trim() + "%";
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }


        #region[close-18/08/2020  chuyển nhóm]
        //       public static DataTable No1_V2(DateTime FromDate, DateTime ToDate, int TeamKey)
        //       {
        //           DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
        //           DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

        //           string PivotDate = "";
        //           for (int i = FromDate.Day; i <= ToDate.Day; i++)
        //           {
        //               PivotDate += "[" + i + "],";
        //           }
        //           if (PivotDate.Contains(","))
        //           {
        //               PivotDate = PivotDate.Remove(PivotDate.LastIndexOf(","));
        //           }

        //           DataTable zTable = new DataTable();
        //           string zSQL = @"
        //           SELECT * FROM (
        //                    (SELECT EmployeeName, EmployeeID, DAY(Date) AS [Time], Total AS Total 
        //                       FROM FTR_Money_Date 
        //                    WHERE TeamKey = @TeamKey 
        //                       AND (Date BETWEEN @FromDate AND @ToDate)
        //                       AND RecordStatus <> 99
        //                    ) 
        //                    UNION
        //                    (SELECT EmployeeName,EmployeeID,Day(DateImport),[Money] AS Total 
        //                    FROM [dbo].[Temp_Import_Detail]
        //                    WHERE TeamKey = @TeamKey 
        //                       AND (DateImport BETWEEN @FromDate AND @ToDate)
        //                       AND RecordStatus <> 99 )
        //) 
        //           X PIVOT (
        //                SUM(Total)
        //			 FOR [Time] IN (" + PivotDate + ")) P ORDER BY LEN(EmployeeID), EmployeeID";
        //           string zConnectionString = ConnectDataBase.ConnectionString;
        //           try
        //           {
        //               SqlConnection zConnect = new SqlConnection(zConnectionString);
        //               zConnect.Open();
        //               SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //               zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
        //               zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
        //               zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
        //               zCommand.CommandTimeout = 350;
        //               SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
        //               zAdapter.Fill(zTable);
        //               zCommand.Dispose();
        //               zConnect.Close();
        //           }
        //           catch (Exception ex)
        //           {
        //               string zstrMessage = "08" + ex.ToString();
        //           }
        //           return zTable;
        //       }
        #endregion

        #region[Báo cáo lương theo công đoạn tại tổ]
        public static DataTable No2(DateTime FromDate, DateTime ToDate, int TeamKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 0, 0);
            DataTable zTable = new DataTable();

            DataTable zTablStage = StageName(FromDate, ToDate, TeamKey); //Danh sách tên công đoạn
            string BienKieuDL = "EmployeeName NVARCHAR(255),EmployeeID NVARCHAR(255),"; //Khai báo column bảng tạm
            string BienCot = "EmployeeName,EmployeeID,"; //Column để truyền giá trị insert vào
            string NameSoLuong = "EmployeeName,EmployeeID,"; // Đặt tên cho column số lượng sản phẩm khi union vào wish as
            string NameLuongKhoan = "EmployeeName,EmployeeID,"; // Đặt tên cho column lương khoán khi union vào wish as
            string SumColumnName = "EmployeeName,EmployeeID,"; //Sum để group by bảng #TEAM trong Wish as lại

            for (int i = 0; i < zTablStage.Rows.Count; i++)
            {
                string zStageName = zTablStage.Rows[i]["StageName"].ToString();
                BienKieuDL += "[" + zStageName + "] FLOAT,";
                BienCot += "[" + zStageName + "],";
                NameSoLuong += "[" + zStageName + "] AS [" + zStageName + "_SL] ,'' [" + zStageName + "_LK],";
                NameLuongKhoan += "'' " + "[" + zStageName + "_LK],[" + zStageName + "] AS [" + zStageName + "_SL],";
                SumColumnName += "SUM([" + zStageName + "_SL]) AS [" + zStageName + "_SL],";
                SumColumnName += "SUM([" + zStageName + "_LK]) AS [" + zStageName + "_LK],";
            }
            //Cắt bỏ dấu , cuối cùng
            if (BienKieuDL.Contains(","))
            {
                BienKieuDL = BienKieuDL.Remove(BienKieuDL.LastIndexOf(","));
            }
            if (BienCot.Contains(","))
            {
                BienCot = BienCot.Remove(BienCot.LastIndexOf(","));
            }
            if (NameSoLuong.Contains(","))
            {
                NameSoLuong = NameSoLuong.Remove(NameSoLuong.LastIndexOf(","));
            }
            if (NameLuongKhoan.Contains(","))
            {
                NameLuongKhoan = NameLuongKhoan.Remove(NameLuongKhoan.LastIndexOf(","));
            }
            if (SumColumnName.Contains(","))
            {
                SumColumnName = SumColumnName.Remove(SumColumnName.LastIndexOf(","));
            }
            //Tạo bảng tạm 1 cho store PivotSoLuongThanhPham
            string zSQL = @"
CREATE TABLE #temp1 (
" + BienKieuDL + ") INSERT INTO #temp1 (" + BienCot + ") " +
"EXECUTE [PivotSoLuongThanhPham] @FromDate,@ToDate,@TeamKey ";
            //Tạo bảng tạm 2 cho store PivotSoTien
            zSQL += @"
CREATE TABLE #temp2 (
" + BienKieuDL + ") INSERT INTO #temp2 (" + BienCot + ") " +
"EXECUTE [PivotSoTien]@FromDate,@ToDate,@TeamKey; ";
            // Union 2 bảng tạm 1 và tạm 2 thành 1 bảng
            zSQL += @"WITH  #TEMP AS (
SELECT " + NameSoLuong + " FROM #temp1 UNION SELECT " + NameLuongKhoan + " FROM #temp2)";
            //Group by  và hiển thị bảng tạm lại
            zSQL += @" SELECT " + SumColumnName + " FROM  #TEMP  GROUP BY LEN(EmployeeID), EmployeeID, EmployeeName";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable StageName(DateTime FromDate, DateTime ToDate, int TeamKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 0, 0);
            DataTable zTable = new DataTable();

            string zSQL = @"	
SELECT DISTINCT B.StageName
FROM FTR_Order_Money A 
LEFT JOIN IVT_Product_Stages B ON A.StageKey = B.StageKey	
WHERE A.TeamKey = @TeamKey 
AND B.RecordStatus != 99 
AND A.RecordStatus != 99 
AND A.OrderDate BETWEEN @FromDate AND @ToDate	
ORDER BY B.StageName";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        #endregion
        #region[Báo cáo lương theo công đoạn tại tổ  và chia lại V2_2]
        public static DataTable No2_V2_2(DateTime FromDate, DateTime ToDate, int TeamKey, string Search)
        {
            // báo cáo công đoạn TẠI TỔ
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            DataTable zTable = new DataTable();

            #region [SQL]
            string zSubSQL1 = @"
                SELECT 
				A.TeamKey AS MaNhom, 
				[dbo].[Fn_GetTeamID](A.TeamKey) AS MaNhom,				
				A.EmployeeID, 
				A.EmployeeName, 
				A.StageKey, 				
				dbo.LayNhomCongViec(A.StageKey) AS NhomCongViec,
				dbo.LayTenCongViec(A.StageKey) AS CongViec,
				dbo.LayGiaCongViec(A.StageKey) AS GiaCongViec,
				SUM(Kg+Kg_Borrow+Kg_Eat+KgPrivate) AS [SL],
				SUM(Money+Money_Borrow+Money_Eat+MoneyPrivate+Money_Dif_Private+Money_Dif_General) AS [LK]
				FROM FTR_Order_Money A								
				WHERE A.OrderDate BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120) 
                AND A.RecordStatus <> 99 ";
            if (TeamKey != 0)
            {
                zSubSQL1 += " AND A.TeamKey = @TeamKey";
            }
            if (Search.Trim().Length > 0)
            {
                zSubSQL1 += " AND A.EmployeeID = @Search";
            }

            zSubSQL1 += " GROUP BY A.TeamKey, A.EmployeeName, A.EmployeeID, A.StageKey";
            //        string zSubSQL2 = @"
            //            SELECT 
            //[dbo].Fn_LayTeamKey_TheoThang(A.EmployeeKey,@FromDate) AS MaNhom, 
            //dbo.Fn_GetTeamName([dbo].Fn_LayTeamKey_TheoThang(A.EmployeeKey,@FromDate)) AS TenNhom,				
            //A.EmployeeID, 
            //A.EmployeeName, 
            //A.StageKey, 				
            //dbo.LayNhomCongViec(A.StageKey) AS NhomCongViec,
            //dbo.LayTenCongViec(A.StageKey) AS CongViec,
            //dbo.LayGiaCongViec(A.StageKey) AS GiaCongViec,
            //SUM(A.KgProduct) AS SL,
            //SUM(A.MoneyPersonal) AS [LK]
            //FROM FTR_Order_Adjusted A 				
            //WHERE A.OrderDate BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)";
            //        if (TeamKey != 0)
            //        {
            //            zSubSQL2 += " AND [dbo].Fn_LayTeamKey_TheoThang(A.EmployeeKey,@FromDate) = @TeamKey";
            //        }
            //        zSubSQL2 += " GROUP BY [dbo].Fn_LayTeamKey_TheoThang(A.EmployeeKey,@FromDate), A.EmployeeName, A.EmployeeID, A.StageKey";

            string zSQL = @"
CREATE TABLE #RPT
(	
	KeyNhom int,
	MaNhom NVARCHAR(MAX),
	MaCongNhan NVARCHAR(100),
	TenCongNhan NVARCHAR(500),
	MaCongViec int,
	NhomCongViec NVARCHAR(MAX),
	CongViec NVARCHAR(MAX),
	GiaCongViec NVARCHAR(MAX),	
	SL FLOAT,
	LK FLOAT
);

INSERT INTO #RPT " + zSubSQL1 + @"

-- sua subsql2
SELECT CONVERT(varchar(10), LEN(MaCongNhan))+'|'+MaCongNhan +'|'+ TenCongNhan +'|'+ MaNhom AS TenCongNhan, [dbo].[Fn_LayTen_NhomCongViec](NhomCongViec)+'|'+CongViec+'|'+GiaCongViec AS CongViec, SL, LK
FROM #RPT 
ORDER BY [dbo].[Fn_LaySoSapXep_NhomCongViec](NhomCongViec)
DROP TABLE #RPT
";
            #endregion
            //INSERT INTO #RPT " + zSubSQL2 + @"
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = Search.Trim();
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        #region[Close - 18/08/2020-thay doi to nhom]
        //        public static DataTable No2_V2_2(DateTime FromDate, DateTime ToDate, int TeamKey)
        //        {
        //            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
        //            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
        //            DataTable zTable = new DataTable();

        //            #region [SQL]
        //            string zSubSQL1 = @"
        //                SELECT 
        //				A.TeamKey AS MaNhom, 
        //				dbo.Fn_GetTeamName(A.TeamKey) AS TenNhom,				
        //				A.EmployeeID, 
        //				A.EmployeeName, 
        //				A.StageKey, 				
        //				dbo.LayNhomCongViec(A.StageKey) AS NhomCongViec,
        //				dbo.LayTenCongViec(A.StageKey) AS CongViec,
        //				dbo.LayGiaCongViec(A.StageKey) AS GiaCongViec,
        //				SUM(Kg+Kg_Borrow+Kg_Eat+KgPrivate) AS [SL],
        //				SUM(Money+Money_Borrow+Money_Eat+MoneyPrivate+Money_Dif_Private+Money_Dif_General) AS [LK]
        //				FROM FTR_Order_Money A								
        //				WHERE A.OrderDate BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)";
        //            if (TeamKey != 0)
        //            {
        //                zSubSQL1 += " AND A.TeamKey = @TeamKey";
        //            }

        //            zSubSQL1 += " GROUP BY A.TeamKey, A.EmployeeName, A.EmployeeID, A.StageKey";
        //            string zSubSQL2 = @"
        //                SELECT 
        //				A.TeamKey AS MaNhom, 
        //				dbo.Fn_GetTeamName(A.TeamKey) AS TenNhom,				
        //				A.EmployeeID, 
        //				A.EmployeeName, 
        //				A.StageKey, 				
        //				dbo.LayNhomCongViec(A.StageKey) AS NhomCongViec,
        //				dbo.LayTenCongViec(A.StageKey) AS CongViec,
        //				dbo.LayGiaCongViec(A.StageKey) AS GiaCongViec,
        //				SUM(A.KgProduct) AS SL,
        //				SUM(A.MoneyPersonal) AS [LK]
        //				FROM FTR_Order_Adjusted A 				
        //				WHERE A.OrderDate BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)";
        //            if (TeamKey != 0)
        //            {
        //                zSubSQL2 += " AND A.TeamKey = @TeamKey";
        //            }
        //            zSubSQL2 += " GROUP BY A.TeamKey, A.EmployeeName, A.EmployeeID, A.StageKey";

        //            string zSQL = @"
        //CREATE TABLE #RPT
        //(	
        //	MaNhom int,
        //	TenNhom NVARCHAR(MAX),
        //	MaCongNhan NVARCHAR(100),
        //	TenCongNhan NVARCHAR(500),
        //	MaCongViec int,
        //	NhomCongViec NVARCHAR(MAX),
        //	CongViec NVARCHAR(MAX),
        //	GiaCongViec NVARCHAR(MAX),	
        //	SL FLOAT,
        //	LK FLOAT
        //);

        //INSERT INTO #RPT " + zSubSQL1 + @"
        //INSERT INTO #RPT " + zSubSQL2 + @"

        //SELECT CONVERT(varchar(10), LEN(MaCongNhan))+'|'+MaCongNhan +'|'+ TenCongNhan AS TenCongNhan, NhomCongViec+'|'+CongViec+'|'+GiaCongViec AS CongViec, SL, LK
        //FROM #RPT 
        //ORDER BY NhomCongViec, CongViec, GiaCongViec
        //DROP TABLE #RPT
        //";
        //            #endregion

        //            string zConnectionString = ConnectDataBase.ConnectionString;
        //            try
        //            {
        //                SqlConnection zConnect = new SqlConnection(zConnectionString);
        //                zConnect.Open();
        //                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
        //                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
        //                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
        //                zCommand.CommandTimeout = 350;
        //                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
        //                zAdapter.Fill(zTable);
        //                zCommand.Dispose();
        //                zConnect.Close();
        //            }
        //            catch (Exception ex)
        //            {
        //                string zstrMessage = ex.ToString();
        //            }
        //            return zTable;
        //        }
        #endregion

        #endregion

        #region[Báo cáo lương theo công đoạn tại tổ  và chia lại]
        public static DataTable No2_V2(DateTime FromDate, DateTime ToDate, int TeamKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 0, 0);
            DataTable zTable = new DataTable();

            DataTable zTablStage = StageName_V2(FromDate, ToDate, TeamKey); //Danh sách tên công đoạn
            string BienKieuDL = "EmployeeName NVARCHAR(255),EmployeeID NVARCHAR(255),"; //Khai báo column bảng tạm
            string BienCot = "EmployeeName,EmployeeID,"; //Column để truyền giá trị insert vào
            string NameSoLuong = "EmployeeName,EmployeeID,"; // Đặt tên cho column số lượng sản phẩm khi union vào wish as
            string NameLuongKhoan = "EmployeeName,EmployeeID,"; // Đặt tên cho column lương khoán khi union vào wish as
            string SumColumnName = "EmployeeName,EmployeeID,"; //Sum để group by bảng #TEAM trong Wish as lại

            for (int i = 0; i < zTablStage.Rows.Count; i++)
            {
                string zStageName = zTablStage.Rows[i]["StageName"].ToString();
                BienKieuDL += "[" + zStageName + "] FLOAT,";
                BienCot += "[" + zStageName + "],";
                NameSoLuong += "[" + zStageName + "] AS [" + zStageName + "_SL] ,'' [" + zStageName + "_LK],";
                NameLuongKhoan += "'' " + "[" + zStageName + "_LK],[" + zStageName + "] AS [" + zStageName + "_SL],";
                SumColumnName += "SUM([" + zStageName + "_SL]) AS [" + zStageName + "_SL],";
                SumColumnName += "SUM([" + zStageName + "_LK]) AS [" + zStageName + "_LK],";
            }
            //Cắt bỏ dấu , cuối cùng
            if (BienKieuDL.Contains(","))
            {
                BienKieuDL = BienKieuDL.Remove(BienKieuDL.LastIndexOf(","));
            }
            if (BienCot.Contains(","))
            {
                BienCot = BienCot.Remove(BienCot.LastIndexOf(","));
            }
            if (NameSoLuong.Contains(","))
            {
                NameSoLuong = NameSoLuong.Remove(NameSoLuong.LastIndexOf(","));
            }
            if (NameLuongKhoan.Contains(","))
            {
                NameLuongKhoan = NameLuongKhoan.Remove(NameLuongKhoan.LastIndexOf(","));
            }
            if (SumColumnName.Contains(","))
            {
                SumColumnName = SumColumnName.Remove(SumColumnName.LastIndexOf(","));
            }
            //Tạo bảng tạm 1 cho store PivotSoLuongThanhPham
            string zSQL = @"
CREATE TABLE #temp1 (
" + BienKieuDL + ") INSERT INTO #temp1 (" + BienCot + ") " +
"EXECUTE [PivotSoLuongThanhPham_V2] @FromDate,@ToDate,@TeamKey ";
            //Tạo bảng tạm 2 cho store PivotSoTien
            zSQL += @"
CREATE TABLE #temp2 (
" + BienKieuDL + ") INSERT INTO #temp2 (" + BienCot + ") " +
"EXECUTE [PivotSoTien_V2]@FromDate,@ToDate,@TeamKey; ";
            // Union 2 bảng tạm 1 và tạm 2 thành 1 bảng
            zSQL += @"WITH  #TEMP AS (
SELECT " + NameSoLuong + " FROM #temp1 UNION SELECT " + NameLuongKhoan + " FROM #temp2)";
            //Group by  và hiển thị bảng tạm lại
            zSQL += @" SELECT " + SumColumnName + " FROM  #TEMP  GROUP BY LEN(EmployeeID), EmployeeID, EmployeeName";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable StageName_V2(DateTime FromDate, DateTime ToDate, int TeamKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 0, 0);
            DataTable zTable = new DataTable();

            string zSQL = @"	
With #TAM as
(
SELECT DISTINCT B.StageName AS StageName,C.Rank
FROM FTR_Order_Money A 
LEFT JOIN IVT_Product_Stages B ON A.StageKey = B.StageKey	
LEFT JOIN [dbo].[IVT_Product_Stages_Group] C ON C.GroupKey = B.GroupKey	
WHERE A.TeamKey = @TeamKey 
AND B.RecordStatus != 99 
AND A.RecordStatus != 99 
AND A.OrderDate BETWEEN @FromDate AND @ToDate 
UNION
SELECT DISTINCT A.StageName AS StageName,C.Rank
FROM  [dbo].[FTR_Order_Adjusted]A 
LEFT JOIN IVT_Product_Stages B ON A.StageKey = B.StageKey	
LEFT JOIN [dbo].[IVT_Product_Stages_Group] C ON C.GroupKey = B.GroupKey	
WHERE A.TeamKey = @TeamKey 
AND A.RecordStatus != 99 
AND A.OrderDate BETWEEN @FromDate AND @ToDate 
)
SELECT * FROM #TAM GROUP BY StageName,Rank ORDER BY Rank
";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        #endregion



        /// <summary>
        /// Phân bổ theo mã sản phẩm bước 1
        /// Phân bổ các công việc làm qua từng nhóm
        /// </summary>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="ListTeam"></param>
        /// <returns></returns>
        public static DataTable No3(DateTime FromDate, DateTime ToDate, DataTable ListTeam)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            string PivotTeam = "";
            for (int i = 0; i < ListTeam.Rows.Count; i++)
            {
                PivotTeam += "[" + ListTeam.Rows[i]["TeamName"] + "],";
            }
            if (PivotTeam.Contains(","))
            {
                PivotTeam = PivotTeam.Remove(PivotTeam.LastIndexOf(","));
            }
            DataTable zTable = new DataTable();
            string zSQL = @" 
 WITH #TEMP AS 
(
     SELECT X.ProductID,X.ProductName, X.TeamName,
     SUM
	    ( 
		    ISNULL(Money1, 0) 		+ 
		    ISNULL(Money2 ,0) 		+ 
		    ISNULL(Money3, 0) 		+ 
		    ISNULL(Money4, 0) 		+ 
		    ISNULL(Money5, 0) 		+ 
		    ISNULL(Money6, 0) 		+ 
		    ISNULL(Money7, 0) 		+ 
		    ISNULL(Money8, 0) 		+ 
		    ISNULL(Money9, 0) 		+ 
		    ISNULL(Money10, 0)  
	    ) AS [Money]
     FROM
     (
	    SELECT 
	    dbo.LayMaSanPham(A.OrderKey, @FromDate, @ToDate) AS ProductID,	
	    dbo.LayTenSanPham(A.OrderKey, @FromDate, @ToDate) AS ProductName,
	    dbo.Fn_GetTeamName(A.TeamKey) AS TeamName,
	    ISNULL([MONEY], 0) AS Money1 		, 
	    ISNULL([Money_Borrow] ,0) AS Money2 		, 
	    ISNULL([MONEYEAT], 0) 	AS Money3	, 
	    ISNULL([Money_Dif_Private], 0) AS Money4		, 
	    ISNULL([Money_Dif_Private], 0) AS Money5		, 
	    ISNULL(Money_Dif_General, 0) AS Money6		, 
	    ISNULL(Money_Dif_Sunday_Private, 0) AS Money7		, 
	    ISNULL(Money_Dif_SunDay_General, 0) AS Money8		, 
	    ISNULL(Money_Dif_Holiday_Private, 0) AS Money9		, 
	    ISNULL(Money_Dif_Holiday_General, 0) AS Money10 
	    FROM FTR_Order_Money A
	    WHERE OrderDate BETWEEN @FromDate AND @ToDate
    ) X
    GROUP BY X.ProductID,X.ProductName ,X.TeamName
) 
SELECT * FROM 
(
    SELECT ProductID ,ProductName, TeamName, SUM([Money]) AS Total 
    FROM #TEMP A
    GROUP BY ProductID, ProductName, TeamName
)
X PIVOT (SUM(Total) FOR TeamName IN (" + PivotTeam + ")) P ORDER BY ProductID";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }

        public static DataTable No3_V2(DateTime FromDate, DateTime ToDate)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            DataTable zTable = new DataTable();
            string zSQL = @" 

     SELECT X.ProductID + '|' + X.ProductName AS PRODUCT, X.TeamName,
     SUM(
            ISNULL(Money1, 0) 		+ 
		    ISNULL(Money2 ,0) 		+ 
		    ISNULL(Money3, 0) 		+ 
		    ISNULL(Money4, 0) 		+ 
		    ISNULL(Money5, 0) 		+ 
		    ISNULL(Money6, 0) 		
        ) AS 'NGAYTHUONG',
     SUM(
            ISNULL(Money7, 0) 		+ 
		    ISNULL(Money8, 0) 		
        ) AS 'CHUNHAT',
     SUM(
            ISNULL(Money9, 0) 		+ 
		    ISNULL(Money10, 0) 
        ) AS 'NGAYLE',
     SUM
	    ( 
		    ISNULL(Money1, 0) 		+ 
		    ISNULL(Money2 ,0) 		+ 
		    ISNULL(Money3, 0) 		+ 
		    ISNULL(Money4, 0) 		+ 
		    ISNULL(Money5, 0) 		+ 
		    ISNULL(Money6, 0) 		+
		    ISNULL(Money7, 0) 		+ 
		    ISNULL(Money8, 0) 		+
		    ISNULL(Money9, 0) 		+ 
		    ISNULL(Money10, 0)       
	    ) AS TONG
     FROM
     (
	    SELECT 
	    dbo.LayMaSanPham(A.OrderKey, @FromDate, @ToDate) AS ProductID,	
	    dbo.LayTenSanPham(A.OrderKey, @FromDate, @ToDate) AS ProductName,
	    dbo.Fn_GetTeamName(A.TeamKey) AS TeamName,
	    ISNULL([MONEY], 0) AS Money1 		, 
	    ISNULL([Money_Borrow] ,0) AS Money2 		, 
	    ISNULL([MONEYEAT], 0) 	AS Money3	, 
	    ISNULL([Money_Dif_Private], 0) AS Money4		, 
	    ISNULL([Money_Dif_Private], 0) AS Money5		, 
	    ISNULL(Money_Dif_General, 0) AS Money6		, 
	    ISNULL(Money_Dif_Sunday_Private, 0) AS Money7		, 
	    ISNULL(Money_Dif_SunDay_General, 0) AS Money8		, 
	    ISNULL(Money_Dif_Holiday_Private, 0) AS Money9		, 
	    ISNULL(Money_Dif_Holiday_General, 0) AS Money10 
	    FROM FTR_Order_Money A
	    WHERE OrderDate BETWEEN @FromDate AND @ToDate
    ) X
    GROUP BY X.ProductID,X.ProductName ,X.TeamName";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }

        public static DataTable No3_V3(DateTime FromDate, DateTime ToDate)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            DataTable zTable = new DataTable();
            string zSQL = @"
DECLARE @REPORT3 AS dbo.REPORT3

INSERT INTO @REPORT3
(OrderDate,ProductID,ProductName,TeamKey,TeamName,TONG,CN,LE,THUONG)
SELECT 
A.OrderDate,
A.ProductID,	
A.ProductName,
A.TeamKey,
dbo.Fn_GetTeamName(A.TeamKey) AS TeamName,
dbo.FTR_TongTienThanhPhamTrongNgay(A.ProductKey,A.OrderDate,A.TeamKey,A.Category_Stage),
0,0,0
FROM FTR_Order A
WHERE A.OrderDate BETWEEN @FromDate AND @ToDate
AND  A.RecordStatus <> 99  
GROUP BY 
A.OrderDate,
A.ProductID,	
A.ProductName,
A.TeamKey,
dbo.FTR_TongTienThanhPhamTrongNgay(A.ProductKey,A.OrderDate,A.TeamKey,A.Category_Stage),
dbo.Fn_GetTeamName(A.TeamKey),A.Category_Stage
ORDER BY A.OrderDate
--select * from @REPORT3

SELECT X.ProductID + '|' + X.ProductName AS PRODUCT, X.TeamName ,
SUM(X.THUONG) AS NGAYTHUONG,SUM(X.CN) AS CHUNHAT,SUM(X.LE) AS NGAYLE,SUM(X.TONG) AS TONG
FROM (
SELECT 
Z.OrderDate,
Z.ProductID,	
Z.ProductName,
Z.TeamName,
Z.TONG,
CASE 
	WHEN DATENAME(weekday,Z.OrderDate) ='SUNDAY' THEN Z.TONG / [dbo].[LayHeSoChuNhat](Z.OrderDate)
	WHEN DATENAME(weekday,Z.OrderDate) !='SUNDAY' THEN 0
END AS CN,
CASE 
	WHEN [dbo].[LayHeSoNgayLeDonHang](Z.OrderDate)  >0 THEN Z.TONG - (Z.TONG / [dbo].[LayHeSoNgayLeDonHang](Z.OrderDate))
	WHEN [dbo].[LayHeSoNgayLeDonHang](Z.OrderDate)  <=0 THEN 0
END  AS LE,
CASE
	WHEN DATENAME(weekday,Z.OrderDate) ='SUNDAY' THEN Z.TONG / [dbo].[LayHeSoChuNhat](Z.OrderDate)
	WHEN [dbo].[LayHeSoNgayLeDonHang](Z.OrderDate)  >0 THEN Z.TONG/[dbo].[LayHeSoNgayLeDonHang](Z.OrderDate)
	WHEN Z.TONG > 0 THEN Z.TONG
	WHEN Z.TONG <=0 THEN 0
END AS THUONG
FROM @REPORT3 Z

    ) X
    GROUP BY X.ProductID,X.ProductName ,X.TeamName
	";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.CommandTimeout = 450;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }

        /// <summary>
        /// dong
        /// </summary>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="TeamKey"></param>
        /// <returns></returns>


        public static DataTable ProductName(DateTime FromDate, DateTime ToDate, string ProductID)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 0, 0);
            DataTable zTable = new DataTable();

            string zSQL = @"	
SELECT DISTINCT ProductID, ProductName 
FROM FTR_Order A
WHERE A.ProductID = @ProductID 
AND A.RecordStatus != 99 
AND A.OrderDate BETWEEN @FromDate AND @ToDate	
ORDER BY A.ProductID";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ProductID", SqlDbType.Int).Value = ProductID;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable Adjusted(DateTime FromDate, DateTime ToDate, int TeamKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string PivotDate = "";
            for (int i = FromDate.Day; i <= ToDate.Day; i++)
            {
                PivotDate += "[" + i + "],";
            }
            if (PivotDate.Contains(","))
            {
                PivotDate = PivotDate.Remove(PivotDate.LastIndexOf(","));
            }

            DataTable zTable = new DataTable();
            string zSQL = @"
                SELECT * FROM (
			    SELECT A.EmployeeKey,A.EmployeeName,A.EmployeeID, DAY(A.OrderDate) AS [Time],
                [dbo].[LayTeamKey](A.EmployeeKey) AS TeamKey ,
                dbo.Fn_GetTeamName([dbo].[LayTeamKey](A.EmployeeKey)) AS TeamName ,
                SUM(A.MoneyPersonal) AS MoneyPersonal 
                FROM [dbo].[FTR_Order_Adjusted] A WHERE A.OrderDate BETWEEN @FromDate AND @ToDate 
                 AND A.Share=0 AND A.RecordStatus<>99";
            if (TeamKey > 0)
                zSQL += " AND [dbo].[LayTeamKey](A.EmployeeKey) =@TeamKey ";
            zSQL += @" GROUP BY A.EmployeeKey,A.EmployeeName,A.EmployeeID,DAY(A.OrderDate),[dbo].[LayTeamKey](A.EmployeeKey) )
               X PIVOT (
               SUM(MoneyPersonal )
			   FOR [Time] IN (" + PivotDate + ") )P ORDER BY [dbo].[LayTeamKey](EmployeeKey),LEN(EmployeeID), EmployeeID";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }

        public static DataTable Adjusted_V2(DateTime FromDate, DateTime ToDate, int TeamKey, string Search)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string PivotDate = "";
            for (int i = FromDate.Day; i <= ToDate.Day; i++)
            {
                PivotDate += "[" + i + "],";
            }
            if (PivotDate.Contains(","))
            {
                PivotDate = PivotDate.Remove(PivotDate.LastIndexOf(","));
            }

            DataTable zTable = new DataTable();
            string zSQL = @"
                CREATE TABLE #temp(
DateWork datetime,
TeamKey int,
EmployeeKey nvarchar(50),
EmployeeID nvarchar(50),
EmployeeName nvarchar(250),
[Total] money
)
--Chia lại
INSERT INTO #temp 
SELECT OrderDate,[dbo].[Fn_TeamKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate),EmployeeKey,EmployeeID,EmployeeName,Sum(MoneyPersonal) 
FROM [dbo].[FTR_Order_Adjusted]
WHERE  OrderDate BETWEEN  @FromDate AND  @ToDate 
AND Share=0 AND RecordStatus <>99  @CustomParamater --AND EmployeeID in ('108')
GROUP BY OrderDate,TeamKey,EmployeeKey,EmployeeID,EmployeeName

;WITH BangTam AS (
	SELECT * FROM(
	SELECT TeamKey,[dbo].[Fn_GetTeamID](TeamKey) AS TeamID,[dbo].[Fn_GetTeamName](TeamKey) AS TeamName,EmployeeID,EmployeeName,Day(DateWork) as [Time],Total FROM #temp WHERE Total >0)
	X PIVOT (
	SUM(Total)
	FOR [Time] IN @DetaiPivot ) P 
)
--Sắp xếp
SELECT A.*,D.RANK BranchRank,C.RANK AS DepartmentRank, B.RANK AS TeamRank FROM BangTam A
LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
ORDER BY D.RANK,C.RANK,B.RANK,LEN(EmployeeID), EmployeeID
DROP TABLE #temp
";
            zSQL = zSQL.Replace("@DetaiPivot", "(" + PivotDate + ")");
            string Fillter = "";
            if (TeamKey > 0)
            {
                Fillter += " AND [dbo].[Fn_TeamKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate) =@TeamKey";
            }
            if (Search.Trim().Length > 0)
            {
                Fillter += " AND EmployeeID IN ('" + Search.Trim() + "') ";
            }
            if (Fillter == "")
            {
                zSQL = zSQL.Replace("@CustomParamater", "");
            }
            else
            {
                zSQL = zSQL.Replace("@CustomParamater", Fillter);
            }
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }


        public static DataTable No5(DateTime FromDate, DateTime ToDate, int TeamKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            DataTable zTable = new DataTable();

            DataTable zTablStage = StageName_SanPhamVaCongViec(FromDate, ToDate, TeamKey); //Danh sách tên công đoạn
            string BienKieuDL = "ProductName NVARCHAR(255),ProductID NVARCHAR(255),"; //Khai báo column bảng tạm
            string BienCot = "ProductName,ProductID,"; //Column để truyền giá trị insert vào
            string NameSoLuong = "ProductName,ProductID,"; // Đặt tên cho column số lượng sản phẩm khi union vào wish as
            string NameLuongKhoan = "ProductName,ProductID,"; // Đặt tên cho column lương khoán khi union vào wish as
            string SumColumnName = "ProductName,ProductID,"; //Sum để group by bảng #TEAM trong Wish as lại

            for (int i = 0; i < zTablStage.Rows.Count; i++)
            {
                //string zStage = zTablStage.Rows[i]["StagesID"].ToString() + "-" + zTablStage.Rows[i]["StageName"].ToString();
                string zStage = zTablStage.Rows[i]["StageName"].ToString();
                BienKieuDL += "[" + zStage + "] FLOAT,";
                BienCot += "[" + zStage + "],";
                NameSoLuong += "[" + zStage + "] AS [" + zStage + "_SL] ,'' [" + zStage + "_LK],";
                NameLuongKhoan += "'' " + "[" + zStage + "_LK],[" + zStage + "] AS [" + zStage + "_SL],";
                SumColumnName += "SUM([" + zStage + "_SL]) AS [" + zStage + "_SL],";
                SumColumnName += "SUM([" + zStage + "_LK]) AS [" + zStage + "_LK],";
            }
            //Cắt bỏ dấu , cuối cùng
            if (BienKieuDL.Contains(","))
            {
                BienKieuDL = BienKieuDL.Remove(BienKieuDL.LastIndexOf(","));
            }
            if (BienCot.Contains(","))
            {
                BienCot = BienCot.Remove(BienCot.LastIndexOf(","));
            }
            if (NameSoLuong.Contains(","))
            {
                NameSoLuong = NameSoLuong.Remove(NameSoLuong.LastIndexOf(","));
            }
            if (NameLuongKhoan.Contains(","))
            {
                NameLuongKhoan = NameLuongKhoan.Remove(NameLuongKhoan.LastIndexOf(","));
            }
            if (SumColumnName.Contains(","))
            {
                SumColumnName = SumColumnName.Remove(SumColumnName.LastIndexOf(","));
            }
            //Tạo bảng tạm 1 cho store PivotSoLuongThanhPham
            string zSQL = @"
CREATE TABLE #temp1 (
" + BienKieuDL + ") INSERT INTO #temp1 (" + BienCot + ") " +
"EXECUTE [PivotSoLuong_SanPhamVaCongViec] @FromDate,@ToDate,@TeamKey; ";
            //Tạo bảng tạm 2 cho store PivotSoTien
            zSQL += @"
CREATE TABLE #temp2 (
" + BienKieuDL + ") INSERT INTO #temp2 (" + BienCot + ") " +
"EXECUTE[dbo].[PivotLuongKhoan_SanPhamVaCongViec] @FromDate,@ToDate,@TeamKey; ";
            // Union 2 bảng tạm 1 và tạm 2 thành 1 bảng
            zSQL += @"WITH  #TEMP AS (
SELECT " + NameSoLuong + " FROM #temp1 UNION SELECT " + NameLuongKhoan + " FROM #temp2)";
            //Group by  và hiển thị bảng tạm lại
            zSQL += @" SELECT " + SumColumnName + " FROM  #TEMP  GROUP BY  ProductID, ProductName";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable StageName_SanPhamVaCongViec(DateTime FromDate, DateTime ToDate, int TeamKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 0, 0);
            DataTable zTable = new DataTable();
            string zSQL = @"	
    SELECT DISTINCT B.StagesID,B.StageName
	FROM FTR_Order A 
	LEFT JOIN IVT_Product_Stages B ON A.Category_Stage = B.StageKey	
	WHERE A.TeamKey = @TeamKey AND A.RecordStatus != 99 AND B.RecordStatus != 99 
	AND A.OrderDate BETWEEN @FromDate AND @ToDate";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }






        #region[Report 6]
        public static DataTable No6(DateTime FromDate, int TeamKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            DataTable zTableDefine = ListCodeReport();
            string zDefine = "";

            string zPivotDefine = "";
            for (int i = 0; i < zTableDefine.Rows.Count; i++)
            {
                DataRow r = zTableDefine.Rows[i];
                zDefine += "[" + r["ID"].ToString().Trim() + "],";
                zPivotDefine += "SUM([" + r["ID"].ToString().Trim() + "])" + " AS '" + CodeName(r["ID"].ToString().Trim()).ToString().Trim() + "', ";
            }
            if (zDefine.Contains(","))
            {
                zDefine = zDefine.Remove(zDefine.LastIndexOf(","));
            }

            if (zPivotDefine.Contains(","))
            {
                zPivotDefine = zPivotDefine.Remove(zPivotDefine.LastIndexOf(","));
            }

            DataTable zTable = new DataTable();
            string zSQL = @"SELECT   EmployeeKey,EmployeeName, EmployeeID,TeamKey,TeamName,";
            zSQL += zPivotDefine;
            zSQL += @"FROM 
( SELECT  EmployeeKey,EmployeeName, EmployeeID,TeamKey,TeamName, CodeID as CodeID1, 
Number,Amount
From  [dbo].[HRM_TimeKeeping_Month] WHERE DateWrite BETWEEN @FromDate AND @ToDate ";
            if (TeamKey > 0)
            {
                zSQL += " AND TeamKey = @TeamKey";
            }
            zSQL += @") X
PIVOT 
( sum(Number)
FOR CodeID1 in (";

            zSQL += zDefine;
            zSQL += @")
) as P1 
 group by  TeamKey,TeamName,EmployeeKey,EmployeeName, EmployeeID
 ORDER BY TeamKey,LEN(EmployeeID),EmployeeID ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable ListCodeReport()
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.ID FROM [dbo].[HRM_CodeReport] A 
WHERE A.RecordStatus <>99
ORDER BY A.Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static string CodeName(string ID)
        {
            string zResult = "";
            string zSQL = @"SELECT Name FROM HRM_CodeReport WHERE ID = @ID AND RecordStatus < 99";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ID", SqlDbType.NVarChar).Value = ID;
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
        #endregion

        #region[Report 7]
        public static DataTable ListCodeReportWorker()
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.ID,A.Name FROM [dbo].[SLR_CodeReportWorker] A 
WHERE A.RecordStatus <>99
ORDER BY A.Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable No7(DateTime FromDate, int TeamKey, string Search)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            DataTable zTableDefine = ListCodeReportWorker();
            string zDefine = "";

            string zPivotDefine = "";
            for (int i = 0; i < zTableDefine.Rows.Count; i++)
            {
                DataRow r = zTableDefine.Rows[i];
                zDefine += "[" + r["ID"].ToString().Trim() + "],";
                zPivotDefine += "SUM([" + r["ID"].ToString().Trim() + "])" + " AS '" + CodeNameWorker(r["ID"].ToString().Trim()).ToString().Trim() + "', ";
            }
            if (zDefine.Contains(","))
            {
                zDefine = zDefine.Remove(zDefine.LastIndexOf(","));
            }

            if (zPivotDefine.Contains(","))
            {
                zPivotDefine = zPivotDefine.Remove(zPivotDefine.LastIndexOf(","));
            }

            DataTable zTable = new DataTable();
            string zSQL = @"SELECT EmployeeKey,  EmployeeName, EmployeeID,TeamKey,TeamID,TeamName,ATM,  ";
            zSQL += zPivotDefine;
            zSQL += @"FROM 
(SELECT A.EmployeeKey, A.EmployeeName, A.EmployeeID,A.TeamKey,A.TeamID,A.TeamName,[dbo].[Fn_GetATM](A.EmployeeKey) AS ATM,
D.Rank AS BranchRank,C.Rank AS DepartmentRank,B.Rank AS TeamRank,CodeID as CodeID1, 
A.Amount
From  [dbo].[SLR_ReportWorker] A
LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
WHERE A.DateWrite BETWEEN @FromDate AND @ToDate
AND A.TeamKey <> 98 AND A.RecordStatus <>99";
            if (TeamKey > 0)
            {
                zSQL += " AND A.TeamKey = @TeamKey";
            }
            if (Search.Trim().Length > 0)
            {
                zSQL += " AND ( A.EmployeeID LIKE @Search OR A.EmployeeName LIKE @Search )";
            }
            else
            {
                zSQL += " ";
            }
            zSQL += @") X
PIVOT 
( sum(Amount)
FOR CodeID1 in (";

            zSQL += zDefine;
            zSQL += @")
) as P1 
 group by EmployeeKey, EmployeeName, EmployeeID,TeamKey,TeamID,TeamName,ATM , BranchRank, DepartmentRank,TeamRank 
 ORDER BY BranchRank, DepartmentRank,TeamRank, TeamKey, LEN(EmployeeID), EmployeeID ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search.Trim() + "%";
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static string CodeNameWorker(string ID)
        {
            string zResult = "";
            string zSQL = @"SELECT Name FROM SLR_CodeReportWorker WHERE ID = @ID AND RecordStatus < 99";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ID", SqlDbType.NVarChar).Value = ID;
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
        // chuyển sang bảng lương động
        public static DataTable No7_V3(DateTime FromDate, int DepartmentKey, int TeamKey, string Search, string Salary)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            string Fillter = "";
            if (Salary.Length > 0)
            {
                Fillter += " AND " + Salary;
            }
            if (DepartmentKey > 0)
            {
                Fillter += " AND A.DepartmentKey = @DepartmentKey";
            }
            if (TeamKey > 0)
            {
                Fillter += " AND A.TeamKey = @TeamKey";
            }
            if (Search.Trim().Length > 0)
            {
                Fillter += " AND ( A.EmployeeID LIKE @Search OR A.EmployeeName LIKE @Search )";
            }
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT (CAST(A.EmployeeKey AS NVARCHAR(50)) +'|'+A.EmployeeName+'|'+A.EmployeeID+'|'+A.PositionName+'|'+CAST(A.TeamKey AS NVARCHAR(50))+'|'+A.TeamName+'|'+A.ATM+'|'+CAST(D.Rank AS NVARCHAR(10))+'|'+CAST(C.Rank AS NVARCHAR(10))+'|'+CAST(B.Rank AS NVARCHAR(10))) AS LeftColumn,
(A.TypeName+'|'+A.CodeName) AS HeaderColumn, CAST(A.Amount  AS MONEY) AS Amount
FROM [dbo].[SLR_ReportWorker] A
LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
WHERE A.DateWrite BETWEEN @FromDate and @ToDate 
AND A.RecordStatus <> 99 AND A.Publish =1  @Parameter
ORDER BY D.Rank,C.Rank,B.Rank, LEN(A.EmployeeID),A.EmployeeID, A.Rank
";
            zSQL = zSQL.Replace("@Parameter", Fillter);
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search.Trim() + "%";
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }

        #endregion


        //-------------
        #region [Báo cáo form 8-10]
        public static DataTable No8(DateTime FromDate, DateTime ToDate, int TeamKey)
        {
            string zSQL = @"
CREATE TABLE #DULIEU 
(		
	NgaySanXuat NVARCHAR(100),
	MaCongViec int,
	NhomCongViec NVARCHAR(MAX),
	CongViec NVARCHAR(MAX),
	GiaCongViec NVARCHAR(MAX),	
	SL FLOAT,
	LK FLOAT
);
INSERT INTO #DULIEU
SELECT
CONVERT(VARCHAR, A.OrderDate, 103) AS NgaySanXuat,
A.Category_Stage AS MaCongViec,
dbo.LayNhomCongViec(A.Category_Stage) AS NhomCongViec,
dbo.LayTenCongViec(A.Category_Stage) AS CongViec,
dbo.LayGiaCongViec(A.Category_Stage) AS GiaCongViec,
SUM(ISNULL(KG,0) + ISNULL(Kg_Borrow,0)) AS SL,
SUM(ISNULL([MONEY], 0) + 
								ISNULL([Money_Borrow] ,0) +
								ISNULL([MONEYEAT], 0) +
								ISNULL([Money_Dif_Private], 0) +
								ISNULL([Money_Dif_Private], 0) +
								ISNULL(Money_Dif_General, 0) +
								ISNULL(Money_Dif_Sunday_Private, 0) +
								ISNULL(Money_Dif_SunDay_General, 0) +
								ISNULL(Money_Dif_Holiday_Private, 0) +
								ISNULL(Money_Dif_Holiday_General, 0)) AS LK
FROM 
FTR_Order A
LEFT JOIN FTR_Order_Money B ON A.OrderKey = B.OrderKey
WHERE 
A.RecordStatus != 99 
AND B.RecordStatus <> 99
AND A.OrderDate BETWEEN @FromDate AND @ToDate ";
            if (TeamKey > 0)
            {
                zSQL += " AND A.TeamKey =@TeamKey ";
            }
            zSQL += @" GROUP BY A.OrderDate, A.Category_Stage;

SELECT NgaySanXuat,  [dbo].[Fn_LayTen_NhomCongViec](NhomCongViec)+'|'+CongViec+'|'+GiaCongViec AS CongViec, SL, LK
FROM #DULIEU 
WHERE NhomCongViec <> 'CVTG'
ORDER BY NgaySanXuat, [dbo].[Fn_LaySoSapXep_NhomCongViec](NhomCongViec)

DROP TABLE #DULIEU";

            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable No10(DateTime FromDate, DateTime ToDate, int TeamKey)
        {
            string zSQL = @"
CREATE TABLE #DULIEU 
(		
	NgaySanXuat NVARCHAR(100),
	MaCongViec int,
	NhomCongViec NVARCHAR(MAX),
	CongViec NVARCHAR(MAX),
	GiaCongViec NVARCHAR(MAX),	
	SL FLOAT,
	LK FLOAT
);
INSERT INTO #DULIEU
SELECT
CONVERT(VARCHAR, A.OrderDate, 103) AS NgaySanXuat,
A.Category_Stage AS MaCongViec,
dbo.LayNhomCongViec(A.Category_Stage) AS NhomCongViec,
dbo.LayTenCongViec(A.Category_Stage) AS CongViec,
dbo.LayGiaCongViec(A.Category_Stage) AS GiaCongViec,
SUM(ISNULL(KG,0) + ISNULL(Kg_Borrow,0)) AS SL,
SUM(ISNULL([MONEY], 0) + 
								ISNULL([Money_Borrow] ,0) +
								ISNULL([MONEYEAT], 0) +
								ISNULL([Money_Dif_Private], 0) +
								ISNULL([Money_Dif_Private], 0) +
								ISNULL(Money_Dif_General, 0) +
								ISNULL(Money_Dif_Sunday_Private, 0) +
								ISNULL(Money_Dif_SunDay_General, 0) +
								ISNULL(Money_Dif_Holiday_Private, 0) +
								ISNULL(Money_Dif_Holiday_General, 0)) AS LK
FROM 
FTR_Order A
LEFT JOIN FTR_Order_Money B ON A.OrderKey = B.OrderKey
WHERE 
A.RecordStatus != 99 
AND B.RecordStatus <> 99
AND A.OrderDate BETWEEN @FromDate AND @ToDate";
            if (TeamKey > 0)
            {
                zSQL += " AND A.TeamKey = @TeamKey ";
            }
            zSQL += @" GROUP BY A.OrderDate, A.Category_Stage;

SELECT NgaySanXuat, [dbo].[Fn_LayTen_NhomCongViec](NhomCongViec)+'|'+CongViec+'|'+GiaCongViec AS CongViec, SL, LK
FROM #DULIEU 
WHERE NhomCongViec = 'CVTG'
ORDER BY NgaySanXuat,[dbo].[Fn_LaySoSapXep_NhomCongViec](NhomCongViec)

DROP TABLE #DULIEU";

            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        #endregion

        #region[Báo cáo form 9-11]
        public static DataTable No9(DateTime FromDate, DateTime ToDate)
        {
            string zSQL = @"
CREATE TABLE #DULIEU 
(	
	MaNhom int,
	TenNhom NVARCHAR(MAX),
	MaCongViec int,
	NhomCongViec NVARCHAR(MAX),
	CongViec NVARCHAR(MAX),
	GiaCongViec NVARCHAR(MAX),	
	SL FLOAT,
	LK FLOAT
);
INSERT INTO #DULIEU
SELECT

O.TeamKey AS MaNhom, 
O.TeamName AS TenNhom, 
A.Category_Stage AS MaCongViec,
dbo.LayNhomCongViec(A.Category_Stage) AS NhomCongViec,
dbo.LayTenCongViec(A.Category_Stage) AS CongViec,
dbo.LayGiaCongViec(A.Category_Stage) AS GiaCongViec,
SUM(ISNULL(KG,0) + ISNULL(Kg_Borrow,0)) AS SL,
SUM(ISNULL([MONEY], 0) + 
								ISNULL([Money_Borrow] ,0) +
								ISNULL([MONEYEAT], 0) +
								ISNULL([Money_Dif_Private], 0) +
								ISNULL([Money_Dif_Private], 0) +
								ISNULL(Money_Dif_General, 0) +
								ISNULL(Money_Dif_Sunday_Private, 0) +
								ISNULL(Money_Dif_SunDay_General, 0) +
								ISNULL(Money_Dif_Holiday_Private, 0) +
								ISNULL(Money_Dif_Holiday_General, 0)) AS LK
FROM 
SYS_Team O  
LEFT JOIN FTR_Order A ON A.TeamKey = O.TeamKey
LEFT JOIN FTR_Order_Money B ON A.OrderKey = B.OrderKey
WHERE 
A.RecordStatus != 99 
AND B.RecordStatus <> 99
AND A.OrderDate BETWEEN @FromDate AND @ToDate 
GROUP BY O.TeamName, O.TeamKey, A.Category_Stage;

SELECT TenNhom, [dbo].[Fn_LayTen_NhomCongViec](NhomCongViec)+'|'+CongViec+'|'+GiaCongViec AS CongViec, SL, LK
FROM #DULIEU 
WHERE NhomCongViec = 'CVTG'
ORDER BY [dbo].[Fn_LaySoSapXep_NhomCongViec](NhomCongViec)

DROP TABLE #DULIEU";

            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable No11(DateTime FromDate, DateTime ToDate)
        {
            string zSQL = @"
CREATE TABLE #DULIEU 
(	
	MaNhom int,
	TenNhom NVARCHAR(MAX),
	MaCongViec int,
	NhomCongViec NVARCHAR(MAX),
	CongViec NVARCHAR(MAX),
	GiaCongViec NVARCHAR(MAX),	
	SL FLOAT,
	LK FLOAT
);
INSERT INTO #DULIEU
SELECT

O.TeamKey AS MaNhom, 
O.TeamName AS TenNhom, 
A.Category_Stage AS MaCongViec,
dbo.LayNhomCongViec(A.Category_Stage) AS NhomCongViec,
dbo.LayTenCongViec(A.Category_Stage) AS CongViec,
dbo.LayGiaCongViec(A.Category_Stage) AS GiaCongViec,
SUM(ISNULL(KG,0) + ISNULL(Kg_Borrow,0)) AS SL,
SUM(ISNULL([MONEY], 0) + 
								ISNULL([Money_Borrow] ,0) +
								ISNULL([MONEYEAT], 0) +
								ISNULL([Money_Dif_Private], 0) +
								ISNULL([Money_Dif_Private], 0) +
								ISNULL(Money_Dif_General, 0) +
								ISNULL(Money_Dif_Sunday_Private, 0) +
								ISNULL(Money_Dif_SunDay_General, 0) +
								ISNULL(Money_Dif_Holiday_Private, 0) +
								ISNULL(Money_Dif_Holiday_General, 0)) AS LK
FROM 
SYS_Team O  
LEFT JOIN FTR_Order A ON A.TeamKey = O.TeamKey
LEFT JOIN FTR_Order_Money B ON A.OrderKey = B.OrderKey
WHERE 
A.RecordStatus != 99 
AND B.RecordStatus <> 99
AND A.OrderDate BETWEEN @FromDate AND @ToDate
GROUP BY O.TeamName, O.TeamKey, A.Category_Stage;

SELECT TenNhom, [dbo].[Fn_LayTen_NhomCongViec](NhomCongViec)+'|'+CongViec+'|'+GiaCongViec AS CongViec, SL, LK
FROM #DULIEU 
WHERE NhomCongViec <> 'CVTG'
ORDER BY [dbo].[Fn_LaySoSapXep_NhomCongViec](NhomCongViec)

DROP TABLE #DULIEU";

            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        #endregion

        //--------
        #region[Report 12]
        public static DataTable No12(DateTime FromDate, int TeamKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            DataTable zTableDefine = ListCodeReport12();
            string zDefine = "";

            string zPivotDefine = "";
            for (int i = 0; i < zTableDefine.Rows.Count; i++)
            {
                DataRow r = zTableDefine.Rows[i];
                zDefine += "[" + r["ID"].ToString().Trim() + "],";
                zPivotDefine += "SUM([" + r["ID"].ToString().Trim() + "])" + " AS '" + CodeName12(r["ID"].ToString().Trim()).ToString().Trim() + "', ";
            }
            if (zDefine.Contains(","))
            {
                zDefine = zDefine.Remove(zDefine.LastIndexOf(","));
            }

            if (zPivotDefine.Contains(","))
            {
                zPivotDefine = zPivotDefine.Remove(zPivotDefine.LastIndexOf(","));
            }

            DataTable zTable = new DataTable();
            string zSQL = @"SELECT  EmployeeKey,EmployeeName, EmployeeID,TeamKey,TeamName,";
            zSQL += zPivotDefine;
            zSQL += @"FROM 
( SELECT EmployeeKey,EmployeeName, EmployeeID,TeamKey,TeamName,  CodeID as CodeID1, 
Number,Amount
From  [dbo].[HRM_TimeKeeping_Month_Close] WHERE DateWrite BETWEEN @FromDate AND @ToDate AND Parent !='0' ";
            if (TeamKey > 0)
            {
                zSQL += " AND TeamKey = @TeamKey";
            }
            zSQL += @") X
PIVOT 
( sum(Number)
FOR CodeID1 in (";

            zSQL += zDefine;
            zSQL += @")
) as P1 
 group by  TeamKey,TeamName,EmployeeKey,EmployeeName, EmployeeID 
 ORDER BY TeamKey,LEN(EmployeeID),EmployeeID ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable ListCodeReport12()
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.ID FROM [dbo].[HRM_CodeReport] A 
WHERE A.RecordStatus <>99
ORDER BY A.Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static string CodeName12(string ID)
        {
            string zResult = "";
            string zSQL = @"SELECT Name FROM HRM_CodeReport WHERE ID = @ID AND RecordStatus < 99";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ID", SqlDbType.NVarChar).Value = ID;
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
        #endregion

        #region[Report 13]
        public static DataTable ListCodeReportWorker13()
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.ID,A.Name FROM [dbo].[SLR_CodeReportWorker] A 
WHERE A.RecordStatus <>99
ORDER BY A.Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable No13(DateTime FromDate, int TeamKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            DataTable zTableDefine = ListCodeReportWorker13();
            string zDefine = "";

            string zPivotDefine = "";
            for (int i = 0; i < zTableDefine.Rows.Count; i++)
            {
                DataRow r = zTableDefine.Rows[i];
                zDefine += "[" + r["ID"].ToString().Trim() + "],";
                zPivotDefine += "SUM([" + r["ID"].ToString().Trim() + "])" + " AS '" + CodeNameWorker13(r["ID"].ToString().Trim()).ToString().Trim() + "', ";
            }
            if (zDefine.Contains(","))
            {
                zDefine = zDefine.Remove(zDefine.LastIndexOf(","));
            }

            if (zPivotDefine.Contains(","))
            {
                zPivotDefine = zPivotDefine.Remove(zPivotDefine.LastIndexOf(","));
            }

            DataTable zTable = new DataTable();
            string zSQL = @"SELECT EmployeeKey,  EmployeeName, EmployeeID,TeamKey,TeamID,TeamName,ATM,  ";
            zSQL += zPivotDefine;
            zSQL += @"FROM 
(SELECT EmployeeKey, EmployeeName, EmployeeID,TeamKey,TeamID,TeamName,[dbo].[Fn_GetATM](EmployeeKey) AS ATM, CodeID as CodeID1, 
Amount
From  [dbo].[SLR_ReportWorker_Close] WHERE DateWrite BETWEEN @FromDate AND @ToDate AND Parent IS  NOT NULL ";
            if (TeamKey > 0)
            {
                zSQL += " AND TeamKey = @TeamKey";
            }
            zSQL += @") X
PIVOT 
( sum(Amount)
FOR CodeID1 in (";

            zSQL += zDefine;
            zSQL += @")
) as P1 
 group by EmployeeKey, EmployeeName, EmployeeID,TeamKey,TeamID,TeamName,ATM  
 ORDER BY TeamKey, LEN(EmployeeID), EmployeeID ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static string CodeNameWorker13(string ID)
        {
            string zResult = "";
            string zSQL = @"SELECT Name FROM SLR_CodeReportWorker WHERE ID = @ID AND RecordStatus < 99";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ID", SqlDbType.NVarChar).Value = ID;
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
        //Báo cáo 13 động
        public static DataTable ListCodeWorker(DateTime DateWrite)
        {
            var zTime = DateWrite.FirstEndMonth();
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT * FROM(
	SELECT CodeID,(TypeName+' '+CodeName) AS CodeName,Rank  FROM [dbo].[SLR_ReportWorker_Close]
	WHERE RecordStatus <> 99 AND Publish_Close = 1  
	AND DateWrite BETWEEN @FromDate AND @ToDate  
	GROUP BY CodeID,(TypeName+' '+CodeName),Rank
	)X
ORDER BY X.[Rank]";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zTime.Item1;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zTime.Item2;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        //Xem theo nhóm
        public static DataTable No13_V3_NHANSU(DateTime FromDate, int DepartmentKey, int TeamKey, string Search, string Salary, int Pay)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            string Fillter = "";
            if (Salary.Length > 0)
            {
                Fillter += " AND " + Salary;
            }
            if (DepartmentKey > 0)
            {
                Fillter += " AND A.DepartmentKey = @DepartmentKey";
            }
            if (TeamKey > 0)
            {
                Fillter += " AND A.TeamKey = @TeamKey";
            }
            if (Search.Trim().Length > 0)
            {
                Fillter += " AND ( A.EmployeeID LIKE @Search OR A.EmployeeName LIKE @Search )";
            }
            if (Pay > 0)
            {
                if (Pay == 1)
                    Fillter += " AND A.ATM !=''";
                if (Pay == 2)
                    Fillter += " AND A.ATM =''";
            }
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT (CAST(A.EmployeeKey AS NVARCHAR(50)) +'|'+A.EmployeeName+'|'+A.EmployeeID+'|'+A.PositionName+'|'+CAST(A.TeamKey AS NVARCHAR(50))+'|'+A.TeamName+'|'+A.ATM+'|'+CAST(D.Rank AS NVARCHAR(10))+'|'+CAST(C.Rank AS NVARCHAR(10))+'|'+CAST(B.Rank AS NVARCHAR(10))) AS LeftColumn,
(A.TypeName+'|'+A.CodeName) AS HeaderColumn, CAST(A.Amount  AS MONEY) AS Amount
FROM [dbo].[SLR_ReportWorker_Close] A
LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
WHERE A.DateWrite BETWEEN @FromDate and @ToDate AND A.Parent IS  NOT NULL  
AND A.RecordStatus <> 99 AND A.Publish_Close =1  @Parameter
ORDER BY D.Rank,C.Rank,B.Rank, LEN(A.EmployeeID),A.EmployeeID, A.Rank
";
            zSQL = zSQL.Replace("@Parameter", Fillter);
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search.Trim() + "%";
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        //Xem theo Bộ phận
        public static DataTable No13_V3_TONHOM(DateTime FromDate, int DepartmentKey, int TeamKey, string Salary, int Pay)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            string Fillter = "";
            if (Salary.Length > 0)
            {
                Fillter += " AND " + Salary;
            }
            if (DepartmentKey > 0)
            {
                Fillter += " AND A.DepartmentKey = @DepartmentKey";
            }
            if (TeamKey > 0)
            {
                Fillter += " AND A.TeamKey = @TeamKey";
            }
            if (Pay > 0)
            {
                if (Pay == 1)
                    Fillter += " AND A.ATM !=''";
                if (Pay == 2)
                    Fillter += " AND A.ATM =''";
            }
            DataTable zTable = new DataTable();
            string zSQL = @"
--declare @FromDate datetime ='2020-06-01 00:00:00'
--declare @ToDate datetime ='2020-06-30 23:59:59'

CREATE TABLE #Temp(
DepartmentKey nvarchar(10),
DepartmentID nvarchar(50),
DepartmentName nvarchar(500),
TeamKey nvarchar(10),
TeamID nvarchar(50),
TeamName nvarchar(500),
Amount money,
CodeID nvarchar(250),
CodeName nvarchar(500),
TypeName  nvarchar(500),
Publish_Close nvarchar(10),
[Rank] INT,
ATM nvarchar(50)
)
INSERT INTO #Temp
	SELECT DepartmentKey,DepartmentID,DepartmentName,TeamKey,TeamID,TeamName,ROUND(SUM(Amount),0) AS Amount,
	CodeID,CodeName,TypeName,Publish_Close,[Rank],ATM
	FROM [dbo].[SLR_ReportWorker_Close]
	WHERE DateWrite BETWEEN @FromDate and @ToDate AND Parent IS  NOT NULL  
	AND RecordStatus <> 99 AND Publish_Close =1 
	GROUP BY DepartmentKey,DepartmentID,DepartmentName,TeamKey,TeamID,TeamName,CodeID,CodeName,TypeName,Publish_Close,[Rank],ATM


SELECT (CAST(A.TeamKey AS NVARCHAR(50)) +'|'+A.TeamName+'|'+A.TeamID+'|'+CAST(A.DepartmentKey AS NVARCHAR(50))+'|'+A.DepartmentName+'|'+CAST(D.Rank AS NVARCHAR(10))+'|'+CAST(C.Rank AS NVARCHAR(10))) AS LeftColumn,
(A.TypeName+'|'+A.CodeName) AS HeaderColumn, Amount
FROM #Temp A
LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=A.DepartmentKey
LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey 
WHERE 1=1 @Parameter
ORDER BY A.Rank

DROP TABLE #Temp
";
            zSQL = zSQL.Replace("@Parameter", Fillter);
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        #endregion

        #region[Report 14]
        public static DataTable ListCodeReportSupport()
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.ID,A.Name FROM [dbo].[SLR_CodeReportSupport] A 
WHERE A.RecordStatus <>99
ORDER BY A.Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable No14(DateTime FromDate, int TeamKey, string Search)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            DataTable zTableDefine = ListCodeReportSupport();
            string zDefine = "";

            string zPivotDefine = "";
            for (int i = 0; i < zTableDefine.Rows.Count; i++)
            {
                DataRow r = zTableDefine.Rows[i];
                zDefine += "[" + r["ID"].ToString().Trim() + "],";
                zPivotDefine += "SUM([" + r["ID"].ToString().Trim() + "])" + " AS '" + CodeNameSupport(r["ID"].ToString().Trim()).ToString().Trim() + "', ";
            }
            if (zDefine.Contains(","))
            {
                zDefine = zDefine.Remove(zDefine.LastIndexOf(","));
            }

            if (zPivotDefine.Contains(","))
            {
                zPivotDefine = zPivotDefine.Remove(zPivotDefine.LastIndexOf(","));
            }

            DataTable zTable = new DataTable();
            string zSQL = @"SELECT EmployeeKey,  EmployeeName, EmployeeID,TeamKey,TeamID,TeamName,ATM,  ";
            zSQL += zPivotDefine;
            zSQL += @"FROM 
(SELECT EmployeeKey,EmployeeName, EmployeeID,TeamKey,TeamID,TeamName,
[dbo].[Fn_GetATM](EmployeeKey) AS ATM,
CodeID as CodeID1, 
Amount
From  [dbo].[SLR_ReportSupport] WHERE DateWrite BETWEEN @FromDate AND @ToDate ";
            if (TeamKey > 0)
            {
                zSQL += " AND TeamKey = @TeamKey";
            }
            if (Search.Trim().Length > 0)
            {
                zSQL += " AND ( EmployeeID LIKE @Search OR EmployeeName LIKE @Search )";
            }
            zSQL += @") X
PIVOT 
( SUM(Amount)
FOR CodeID1 in (";

            zSQL += zDefine;
            zSQL += @")
) AS P1 
 GROUP BY EmployeeKey, EmployeeName, EmployeeID,TeamKey,TeamID,TeamName,ATM  
 ORDER BY TeamKey, LEN(EmployeeID), EmployeeID ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search.Trim() + "%";
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static string CodeNameSupport(string ID)
        {
            string zResult = "";
            string zSQL = @"SELECT Name FROM SLR_CodeReportSupport WHERE ID = @ID AND RecordStatus < 99";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ID", SqlDbType.NVarChar).Value = ID;
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
        //Danh sách kiểu động
        public static DataTable No14_V3(DateTime FromDate, int DepartmentKey, int TeamKey, string Search, string Salary)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            string Fillter = "";
            if (Salary.Length > 0)
            {
                Fillter += " AND " + Salary;
            }
            if (DepartmentKey > 0)
            {
                Fillter += " AND A.DepartmentKey = @DepartmentKey";
            }
            if (TeamKey > 0)
            {
                Fillter += " AND A.TeamKey = @TeamKey";
            }
            if (Search.Trim().Length > 0)
            {
                Fillter += " AND ( A.EmployeeID LIKE @Search OR A.EmployeeName LIKE @Search )";
            }
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT (CAST(A.EmployeeKey AS NVARCHAR(50)) +'|'+A.EmployeeName+'|'+A.EmployeeID+'|'+A.PositionName+'|'+CAST(A.TeamKey AS NVARCHAR(50))+'|'+A.TeamName+'|'+A.ATM+'|'+CAST(D.Rank AS NVARCHAR(10))+'|'+CAST(C.Rank AS NVARCHAR(10))+'|'+CAST(B.Rank AS NVARCHAR(10))) AS LeftColumn,
(A.TypeName+'|'+A.CodeName) AS HeaderColumn, CAST(A.Amount  AS MONEY) AS Amount
FROM [dbo].[SLR_ReportSupport] A
LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
WHERE A.DateWrite BETWEEN @FromDate and @ToDate 
AND A.RecordStatus <> 99 AND A.Publish =1  @Parameter
ORDER BY D.Rank,C.Rank,B.Rank, LEN(A.EmployeeID),A.EmployeeID, A.Rank
";
            zSQL = zSQL.Replace("@Parameter", Fillter);
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search.Trim() + "%";
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }


        #endregion

        #region[Report 15]
        public static DataTable ListCodeReportOffice()
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.ID,A.Name FROM [dbo].[SLR_CodeReportOffice] A 
WHERE A.RecordStatus <>99
ORDER BY A.Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable No15(DateTime FromDate, int TeamKey, string Search)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            DataTable zTableDefine = ListCodeReportOffice();
            string zDefine = "";

            string zPivotDefine = "";
            for (int i = 0; i < zTableDefine.Rows.Count; i++)
            {
                DataRow r = zTableDefine.Rows[i];
                zDefine += "[" + r["ID"].ToString().Trim() + "],";
                zPivotDefine += "SUM([" + r["ID"].ToString().Trim() + "])" + " AS '" + CodeNameOffice(r["ID"].ToString().Trim()).ToString().Trim() + "', ";
            }
            if (zDefine.Contains(","))
            {
                zDefine = zDefine.Remove(zDefine.LastIndexOf(","));
            }

            if (zPivotDefine.Contains(","))
            {
                zPivotDefine = zPivotDefine.Remove(zPivotDefine.LastIndexOf(","));
            }

            DataTable zTable = new DataTable();
            string zSQL = @"SELECT EmployeeKey,  EmployeeName, EmployeeID,TeamKey,TeamID,TeamName,ATM,PositionName,  ";
            zSQL += zPivotDefine;
            zSQL += @"FROM 
(SELECT A.EmployeeKey, A.EmployeeName, A.EmployeeID,A.TeamKey,A.TeamID,A.TeamName,
[dbo].[Fn_GetPositionName_FromEmployeeKey](A.EmployeeKey) AS PositionName,
[dbo].[Fn_GetATM](A.EmployeeKey) AS ATM,
D.Rank AS BranchRank,C.Rank AS DepartmentRank,B.Rank AS TeamRank, CodeID as CodeID1, 
Amount
From  [dbo].[SLR_ReportOffice] A
LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
WHERE A.DateWrite BETWEEN @FromDate AND @ToDate
AND A.TeamKey <> 98 AND A.RecordStatus <>99";
            if (TeamKey > 0)
            {
                zSQL += " AND A.TeamKey = @TeamKey ";
            }
            else
            {
                zSQL += " ";
            }
            if (Search.Trim().Length > 0)
            {
                zSQL += " AND ( A.EmployeeID LIKE @Search OR A.EmployeeName LIKE @Search )";
            }
            else
            {
                zSQL += " ";
            }

            zSQL += @") X
PIVOT 
( SUM(Amount)
FOR CodeID1 IN (";

            zSQL += zDefine;
            zSQL += @")
) AS P1 
 GROUP BY EmployeeKey, EmployeeName, EmployeeID,TeamKey,TeamID,TeamName,ATM,PositionName, BranchRank, DepartmentRank,TeamRank 
 ORDER BY BranchRank, DepartmentRank,TeamRank, TeamKey, LEN(EmployeeID), EmployeeID ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search.Trim() + "%";
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static string CodeNameOffice(string ID)
        {
            string zResult = "";
            string zSQL = @"SELECT Name FROM SLR_CodeReportOffice WHERE ID = @ID AND RecordStatus < 99";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ID", SqlDbType.NVarChar).Value = ID;
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
        public static DataTable No15_Tab2(DateTime FromDate, int TeamKey, string Search)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            DataTable zTableDefine = ListCodeReportOffice();
            string zDefine = "";

            string zPivotDefine = "";
            for (int i = 0; i < zTableDefine.Rows.Count; i++)
            {
                DataRow r = zTableDefine.Rows[i];
                zDefine += "[" + r["ID"].ToString().Trim() + "],";
                zPivotDefine += "SUM([" + r["ID"].ToString().Trim() + "])" + " AS '" + CodeNameOffice(r["ID"].ToString().Trim()).ToString().Trim() + "', ";
            }
            if (zDefine.Contains(","))
            {
                zDefine = zDefine.Remove(zDefine.LastIndexOf(","));
            }

            if (zPivotDefine.Contains(","))
            {
                zPivotDefine = zPivotDefine.Remove(zPivotDefine.LastIndexOf(","));
            }

            DataTable zTable = new DataTable();
            string zSQL = @"
CREATE TABLE #Temp
(
EmployeeKey nvarchar(50),
EmployeeName nvarchar(250),
EmployeeID nvarchar(50),
TeamKey int,
TeamID nvarchar(50),
TeamName nvarchar(250),
PositionName nvarchar(250),
ATM nvarchar(50),
CodeID nvarchar(50),
Amount money
)
INSERT INTO #Temp
(EmployeeKey,EmployeeName,EmployeeID,TeamKey,TeamID,TeamName,PositionName,ATM,CodeID,Amount)
SELECT A.EmployeeKey, A.EmployeeName, A.EmployeeID,[dbo].[Fn_GetTeamKeyViewOffice](A.EmployeeKey,@ToDate) AS TeamKey,
[dbo].[Fn_GetTeamID]([dbo].[Fn_GetTeamKeyViewOffice](A.EmployeeKey,@ToDate)) AS TeamID,
[dbo].[Fn_GetTeamName]([dbo].[Fn_GetTeamKeyViewOffice](A.EmployeeKey,@ToDate)) AS TeamName,
[dbo].[Fn_GetPositionName_FromEmployeeKey](A.EmployeeKey) AS PositionName,
[dbo].[Fn_GetATM](A.EmployeeKey) AS ATM,
A.CodeID, 
A.Amount
From  [dbo].[SLR_ReportOffice] A
WHERE A.DateWrite BETWEEN @FromDate AND @ToDate
AND A.TeamKey <> 98 AND A.RecordStatus <>99";
            if (TeamKey > 0)
            {
                zSQL += " AND[dbo].[Fn_GetTeamKeyViewOffice](A.EmployeeKey,@ToDate) = @TeamKey ";
            }
            else
            {
                zSQL += " ";
            }
            if (Search.Trim().Length > 0)
            {
                zSQL += " AND ( A.EmployeeID LIKE @Search OR A.EmployeeName LIKE @Search )";
            }
            else
            {
                zSQL += " ";
            }



            zSQL += @" SELECT EmployeeKey, EmployeeName, EmployeeID, TeamKey, TeamID, TeamName, ATM, PositionName,";
            zSQL += zPivotDefine;
            zSQL += @"FROM 
(SELECT A.EmployeeKey, A.EmployeeName, A.EmployeeID, A.TeamKey, A.TeamID, A.TeamName, A.PositionName, A.ATM, A.CodeID as CodeID1, A.Amount,
D.Rank AS BranchRank,C.Rank AS DepartmentRank,B.Rank AS TeamRank
FROM #Temp  A
LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey";


            zSQL += @") X
PIVOT 
( SUM(Amount)
FOR CodeID1 IN (";

            zSQL += zDefine;
            zSQL += @")
) AS P1 
 GROUP BY EmployeeKey, EmployeeName, EmployeeID,TeamKey,TeamID,TeamName,ATM,PositionName, BranchRank, DepartmentRank,TeamRank 
 ORDER BY BranchRank, DepartmentRank,TeamRank, TeamKey, LEN(EmployeeID), EmployeeID 

DROP TABLE #Temp;
";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search.Trim() + "%";
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        //Danh sách kiểu động
        public static DataTable No15_NHOMMACDINH(DateTime FromDate, int DepartmentKey, int TeamKey, string Search, string Salary)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            string Fillter = "";
            if (Salary.Length > 0)
            {
                Fillter += " AND " + Salary;
            }
            if (DepartmentKey > 0)
            {
                Fillter += " AND A.DepartmentKey = @DepartmentKey";
            }
            if (TeamKey > 0)
            {
                Fillter += " AND A.TeamKey = @TeamKey";
            }
            if (Search.Trim().Length > 0)
            {
                Fillter += " AND ( A.EmployeeID LIKE @Search OR A.EmployeeName LIKE @Search )";
            }
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT (CAST(A.EmployeeKey AS NVARCHAR(50)) +'|'+A.EmployeeName+'|'+A.EmployeeID+'|'+A.PositionName+'|'+CAST(A.TeamKey AS NVARCHAR(50))+'|'+A.TeamName+'|'+A.ATM+'|'+CAST(D.Rank AS NVARCHAR(10))+'|'+CAST(C.Rank AS NVARCHAR(10))+'|'+CAST(B.Rank AS NVARCHAR(10))) AS LeftColumn,
(A.TypeName+'|'+A.CodeName) AS HeaderColumn, CAST(A.Amount  AS MONEY) AS Amount
FROM [dbo].[SLR_ReportOffice] A
LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
WHERE A.DateWrite BETWEEN @FromDate and @ToDate 
AND A.RecordStatus <> 99 AND A.Publish =1  @Parameter
ORDER BY D.Rank,C.Rank,B.Rank, LEN(A.EmployeeID),A.EmployeeID, A.Rank
";
            zSQL = zSQL.Replace("@Parameter", Fillter);
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search.Trim() + "%";
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable No15_NHOMQUANLY(DateTime FromDate, int DepartmentKey, int TeamKey, string Search, string Salary)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            string Fillter = "";
            if (Salary.Length > 0)
            {
                Fillter += " AND " + Salary;
            }
            if (DepartmentKey > 0)
            {
                Fillter += " AND A.DepartmentKey_M = @DepartmentKey";
            }
            if (TeamKey > 0)
            {
                Fillter += " AND A.TeamKey_M = @TeamKey";
            }
            if (Search.Trim().Length > 0)
            {
                Fillter += " AND ( A.EmployeeID LIKE @Search OR A.EmployeeName LIKE @Search )";
            }
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT (CAST(A.EmployeeKey AS NVARCHAR(50)) +'|'+A.EmployeeName+'|'+A.EmployeeID+'|'+A.PositionName+'|'+CAST(A.TeamKey_M AS NVARCHAR(50))+'|'+A.TeamName_M+'|'+A.ATM+'|'+CAST(D.Rank AS NVARCHAR(10))+'|'+CAST(C.Rank AS NVARCHAR(10))+'|'+CAST(B.Rank AS NVARCHAR(10))) AS LeftColumn,
(A.TypeName+'|'+A.CodeName) AS HeaderColumn, CAST(A.Amount  AS MONEY) AS Amount
FROM [dbo].[SLR_ReportOffice] A
LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey_M
LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
WHERE A.DateWrite BETWEEN @FromDate and @ToDate 
AND A.RecordStatus <> 99 AND A.Publish =1  @Parameter
ORDER BY D.Rank,C.Rank,B.Rank, LEN(A.EmployeeID),A.EmployeeID, A.Rank
";
            zSQL = zSQL.Replace("@Parameter", Fillter);
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search.Trim() + "%";
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }


        #endregion

        #region[Report 16 -Báo cáo lương hỗ trợ sản xuất -đã chốt]
        public static DataTable ListCodeReportSupport16()
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.ID,A.Name FROM [dbo].[SLR_CodeReportSupport] A 
WHERE A.RecordStatus <>99
ORDER BY A.Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable No16(DateTime FromDate, int TeamKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            DataTable zTableDefine = ListCodeReportSupport16();
            string zDefine = "";

            string zPivotDefine = "";
            for (int i = 0; i < zTableDefine.Rows.Count; i++)
            {
                DataRow r = zTableDefine.Rows[i];
                zDefine += "[" + r["ID"].ToString().Trim() + "],";
                zPivotDefine += "SUM([" + r["ID"].ToString().Trim() + "])" + " AS '" + CodeNameSupport16(r["ID"].ToString().Trim()).ToString().Trim() + "', ";
            }
            if (zDefine.Contains(","))
            {
                zDefine = zDefine.Remove(zDefine.LastIndexOf(","));
            }

            if (zPivotDefine.Contains(","))
            {
                zPivotDefine = zPivotDefine.Remove(zPivotDefine.LastIndexOf(","));
            }

            DataTable zTable = new DataTable();
            string zSQL = @"SELECT EmployeeKey,  EmployeeName, EmployeeID,TeamKey,TeamID,TeamName,ATM,  ";
            zSQL += zPivotDefine;
            zSQL += @"FROM 
(SELECT EmployeeKey, EmployeeName, EmployeeID,TeamKey,TeamID,TeamName,[dbo].[Fn_GetATM](EmployeeKey) AS ATM, CodeID as CodeID1, 
Amount
From  [dbo].[SLR_ReportSupport_Close] WHERE DateWrite BETWEEN @FromDate AND @ToDate AND Parent IS  NOT NULL";
            if (TeamKey > 0)
            {
                zSQL += " AND TeamKey = @TeamKey";
            }
            zSQL += @") X
PIVOT 
( sum(Amount)
FOR CodeID1 in (";

            zSQL += zDefine;
            zSQL += @")
) as P1 
 group by EmployeeKey, EmployeeName, EmployeeID,TeamKey,TeamID,TeamName,ATM  
 ORDER BY TeamKey, LEN(EmployeeID), EmployeeID ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static string CodeNameSupport16(string ID)
        {
            string zResult = "";
            string zSQL = @"SELECT Name FROM SLR_CodeReportSupport WHERE ID = @ID AND RecordStatus < 99";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ID", SqlDbType.NVarChar).Value = ID;
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
        public static DataTable No16_V3(DateTime FromDate, int TeamKey, string Search)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            DataTable zTableDefine = ListCodeReportSupport();
            string zDefine = "";

            string zPivotDefine = "";
            for (int i = 0; i < zTableDefine.Rows.Count; i++)
            {
                DataRow r = zTableDefine.Rows[i];
                zDefine += "[" + r["ID"].ToString().Trim() + "],";
                zPivotDefine += "SUM([" + r["ID"].ToString().Trim() + "])" + " AS '" + CodeNameSupport(r["ID"].ToString().Trim()).ToString().Trim() + "', ";
            }
            if (zDefine.Contains(","))
            {
                zDefine = zDefine.Remove(zDefine.LastIndexOf(","));
            }

            if (zPivotDefine.Contains(","))
            {
                zPivotDefine = zPivotDefine.Remove(zPivotDefine.LastIndexOf(","));
            }

            DataTable zTable = new DataTable();
            string zSQL = @"SELECT EmployeeKey,  EmployeeName, EmployeeID,TeamKey,TeamID,TeamName,ATM,  ";
            zSQL += zPivotDefine;
            zSQL += @"FROM 
(SELECT EmployeeKey,EmployeeName, EmployeeID,TeamKey,TeamID,TeamName,
[dbo].[Fn_GetATM](EmployeeKey) AS ATM,
CodeID as CodeID1, 
Amount
From  [dbo].[SLR_ReportSupport_Close] WHERE DateWrite BETWEEN @FromDate AND @ToDate AND Parent IS  NOT NULL ";
            if (TeamKey > 0)
            {
                zSQL += " AND TeamKey = @TeamKey";
            }
            if (Search.Trim().Length > 0)
            {
                zSQL += " AND ( EmployeeID LIKE @Search OR EmployeeName LIKE @Search )";
            }
            zSQL += @") X
PIVOT 
( SUM(Amount)
FOR CodeID1 in (";

            zSQL += zDefine;
            zSQL += @")
) AS P1 
 GROUP BY EmployeeKey, EmployeeName, EmployeeID,TeamKey,TeamID,TeamName,ATM  
 ORDER BY TeamKey, LEN(EmployeeID), EmployeeID ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search.Trim() + "%";
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        //chuyển sang báo cáo động
        public static DataTable ListCodeSupport(DateTime DateWrite)
        {
            var zTime = DateWrite.FirstEndMonth();
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT * FROM(
	SELECT CodeID,(TypeName+' '+CodeName) AS CodeName,Rank  FROM [dbo].[SLR_ReportSupport_Close]
	WHERE RecordStatus <> 99 AND Publish_Close = 1  
	AND DateWrite BETWEEN @FromDate AND @ToDate  
	GROUP BY CodeID,(TypeName+' '+CodeName),Rank
	)X
ORDER BY X.[Rank]";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zTime.Item1;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zTime.Item2;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable No16_V4_NHANSU(DateTime FromDate, int DepartmentKey, int TeamKey, string Search, string Salary, int Pay)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            string Fillter = "";
            if (Salary.Length > 0)
            {
                Fillter += " AND " + Salary;
            }
            if (DepartmentKey > 0)
            {
                Fillter += " AND A.DepartmentKey = @DepartmentKey";
            }
            if (TeamKey > 0)
            {
                Fillter += " AND A.TeamKey = @TeamKey";
            }
            if (Search.Trim().Length > 0)
            {
                Fillter += " AND ( A.EmployeeID LIKE @Search OR A.EmployeeName LIKE @Search )";
            }
            if (Pay > 0)
            {
                if (Pay == 1)
                    Fillter += " AND A.ATM !=''";
                if (Pay == 2)
                    Fillter += " AND A.ATM =''";
            }
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT (CAST(A.EmployeeKey AS NVARCHAR(50)) +'|'+A.EmployeeName+'|'+A.EmployeeID+'|'+A.PositionName+'|'+CAST(A.TeamKey AS NVARCHAR(50))+'|'+A.TeamName+'|'+A.ATM+'|'+CAST(D.Rank AS NVARCHAR(10))+'|'+CAST(C.Rank AS NVARCHAR(10))+'|'+CAST(B.Rank AS NVARCHAR(10))) AS LeftColumn,
(A.TypeName+'|'+A.CodeName) AS HeaderColumn, CAST(A.Amount  AS MONEY) AS Amount
FROM [dbo].[SLR_ReportSupport_Close] A
LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
WHERE A.DateWrite BETWEEN @FromDate and @ToDate AND A.Parent IS  NOT NULL  
AND A.RecordStatus <> 99 AND A.Publish_Close =1  @Parameter
ORDER BY D.Rank,C.Rank,B.Rank, LEN(A.EmployeeID),A.EmployeeID, A.Rank
";
            zSQL = zSQL.Replace("@Parameter", Fillter);
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search.Trim() + "%";
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        //Xem theo Bộ phận
        public static DataTable No16_V4_TONHOM(DateTime FromDate, int DepartmentKey, int TeamKey, string Salary, int Pay)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            string Fillter = "";
            if (Salary.Length > 0)
            {
                Fillter += " AND " + Salary;
            }
            if (DepartmentKey > 0)
            {
                Fillter += " AND A.DepartmentKey = @DepartmentKey";
            }
            if (TeamKey > 0)
            {
                Fillter += " AND A.TeamKey = @TeamKey";
            }
            if (Pay > 0)
            {
                if (Pay == 1)
                    Fillter += " AND ATM !=''";
                if (Pay == 2)
                    Fillter += " AND ATM =''";
            }
            DataTable zTable = new DataTable();
            string zSQL = @"
--declare @FromDate datetime ='2020-06-01 00:00:00'
--declare @ToDate datetime ='2020-06-30 23:59:59'

CREATE TABLE #Temp(
DepartmentKey nvarchar(10),
DepartmentID nvarchar(50),
DepartmentName nvarchar(500),
TeamKey nvarchar(10),
TeamID nvarchar(50),
TeamName nvarchar(500),
Amount money,
CodeID nvarchar(250),
CodeName nvarchar(500),
TypeName  nvarchar(500),
Publish_Close nvarchar(10),
[Rank] INT,
ATM nvarchar(50)
)
INSERT INTO #Temp
	SELECT DepartmentKey,DepartmentID,DepartmentName,TeamKey,TeamID,TeamName,ROUND(SUM(Amount),0) AS Amount,
	CodeID,CodeName,TypeName,Publish_Close,[Rank],ATM
	FROM [dbo].[SLR_ReportSupport_Close]
	WHERE DateWrite BETWEEN @FromDate and @ToDate AND Parent IS  NOT NULL  
	AND RecordStatus <> 99 AND Publish_Close =1 
	GROUP BY DepartmentKey,DepartmentID,DepartmentName,TeamKey,TeamID,TeamName,CodeID,CodeName,TypeName,Publish_Close,[Rank],ATM


SELECT (CAST(A.TeamKey AS NVARCHAR(50)) +'|'+A.TeamName+'|'+A.TeamID+'|'+CAST(A.DepartmentKey AS NVARCHAR(50))+'|'+A.DepartmentName+'|'+CAST(D.Rank AS NVARCHAR(10))+'|'+CAST(C.Rank AS NVARCHAR(10))) AS LeftColumn,
(A.TypeName+'|'+A.CodeName) AS HeaderColumn, Amount
FROM #Temp A
LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=A.DepartmentKey
LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey 
WHERE 1=1 @Parameter
ORDER BY A.Rank

DROP TABLE #Temp
";
            zSQL = zSQL.Replace("@Parameter", Fillter);
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        #endregion

        #region[Report 17 -Báo cáo lương văn phòng - đã chốt]
        public static DataTable ListCodeReportOffice17()
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.ID,A.Name FROM [dbo].[SLR_CodeReportOffice] A 
WHERE A.RecordStatus <>99
ORDER BY A.Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable No17(DateTime FromDate, int TeamKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            DataTable zTableDefine = ListCodeReportOffice17();
            string zDefine = "";

            string zPivotDefine = "";
            for (int i = 0; i < zTableDefine.Rows.Count; i++)
            {
                DataRow r = zTableDefine.Rows[i];
                zDefine += "[" + r["ID"].ToString().Trim() + "],";
                zPivotDefine += "SUM([" + r["ID"].ToString().Trim() + "])" + " AS '" + CodeNameOffice17(r["ID"].ToString().Trim()).ToString().Trim() + "', ";
            }
            if (zDefine.Contains(","))
            {
                zDefine = zDefine.Remove(zDefine.LastIndexOf(","));
            }

            if (zPivotDefine.Contains(","))
            {
                zPivotDefine = zPivotDefine.Remove(zPivotDefine.LastIndexOf(","));
            }

            DataTable zTable = new DataTable();
            string zSQL = @"SELECT EmployeeKey,  EmployeeName, EmployeeID,TeamKey,TeamID,TeamName,ATM,  ";
            zSQL += zPivotDefine;
            zSQL += @"FROM 
(SELECT EmployeeKey, EmployeeName, EmployeeID,TeamKey,TeamID,TeamName,[dbo].[Fn_GetATM](EmployeeKey) AS ATM, CodeID as CodeID1, 
Amount
From  [dbo].[SLR_ReportOffice_Close] WHERE DateWrite BETWEEN @FromDate AND @ToDate AND Parent IS  NOT NULL";
            if (TeamKey > 0)
            {
                zSQL += " AND TeamKey = @TeamKey";
            }
            zSQL += @") X
PIVOT 
( sum(Amount)
FOR CodeID1 in (";

            zSQL += zDefine;
            zSQL += @")
) as P1 
 group by EmployeeKey, EmployeeName, EmployeeID,TeamKey,TeamID,TeamName,ATM  
 ORDER BY TeamKey, LEN(EmployeeID), EmployeeID ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static string CodeNameOffice17(string ID)
        {
            string zResult = "";
            string zSQL = @"SELECT Name FROM SLR_CodeReportOffice WHERE ID = @ID AND RecordStatus < 99";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ID", SqlDbType.NVarChar).Value = ID;
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
        //chuyển sang báo cáo động
        public static DataTable ListCodeOffice(DateTime DateWrite)
        {
            var zTime = DateWrite.FirstEndMonth();
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT * FROM(
	SELECT CodeID,(TypeName+' '+CodeName) AS CodeName,Rank  FROM [dbo].[SLR_ReportOffice_Close]
	WHERE RecordStatus <> 99 AND Publish_Close = 1  
	AND DateWrite BETWEEN @FromDate AND @ToDate  
	GROUP BY CodeID,(TypeName+' '+CodeName),Rank
	)X
ORDER BY X.[Rank]";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zTime.Item1;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zTime.Item2;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        //theo nhân sự
        public static DataTable No17_NHANSUMACDINH(DateTime FromDate, int DepartmentKey, int TeamKey, string Search, string Salary, int Pay)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            string Fillter = "";
            if (Salary.Length > 0)
            {
                Fillter += " AND " + Salary;
            }
            if (DepartmentKey > 0)
            {
                Fillter += " AND A.DepartmentKey = @DepartmentKey";
            }
            if (TeamKey > 0)
            {
                Fillter += " AND A.TeamKey = @TeamKey";
            }
            if (Search.Trim().Length > 0)
            {
                Fillter += " AND ( A.EmployeeID LIKE @Search OR A.EmployeeName LIKE @Search )";
            }
            if (Pay > 0)
            {
                if (Pay == 1)
                    Fillter += " AND A.ATM !=''";
                if (Pay == 2)
                    Fillter += " AND A.ATM =''";
            }
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT (CAST(A.EmployeeKey AS NVARCHAR(50)) +'|'+A.EmployeeName+'|'+A.EmployeeID+'|'+A.PositionName+'|'+CAST(A.TeamKey AS NVARCHAR(50))+'|'+A.TeamName+'|'+A.ATM+'|'+CAST(D.Rank AS NVARCHAR(10))+'|'+CAST(C.Rank AS NVARCHAR(10))+'|'+CAST(B.Rank AS NVARCHAR(10))) AS LeftColumn,
(A.TypeName+'|'+A.CodeName) AS HeaderColumn, CAST(A.Amount  AS MONEY) AS Amount
FROM [dbo].[SLR_ReportOffice_Close] A
LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
WHERE A.DateWrite BETWEEN @FromDate and @ToDate 
AND A.RecordStatus <> 99 AND A.Publish_Close =1  @Parameter
ORDER BY D.Rank,C.Rank,B.Rank, LEN(A.EmployeeID),A.EmployeeID, A.Rank
";
            zSQL = zSQL.Replace("@Parameter", Fillter);
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search.Trim() + "%";
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable No17_NHANSUQUANLY(DateTime FromDate, int DepartmentKey, int TeamKey, string Search, string Salary, int Pay)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            string Fillter = "";
            if (Salary.Length > 0)
            {
                Fillter += " AND " + Salary;
            }
            if (DepartmentKey > 0)
            {
                Fillter += " AND A.DepartmentKey_M = @DepartmentKey";
            }
            if (TeamKey > 0)
            {
                Fillter += " AND A.TeamKey_M = @TeamKey";
            }
            if (Search.Trim().Length > 0)
            {
                Fillter += " AND ( A.EmployeeID LIKE @Search OR A.EmployeeName LIKE @Search )";
            }
            if (Pay > 0)
            {
                if (Pay == 1)
                    Fillter += " AND A.ATM !=''";
                if (Pay == 2)
                    Fillter += " AND A.ATM =''";
            }
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT (CAST(A.EmployeeKey AS NVARCHAR(50)) +'|'+A.EmployeeName+'|'+A.EmployeeID+'|'+A.PositionName+'|'+CAST(A.TeamKey_M AS NVARCHAR(50))+'|'+A.TeamName_M+'|'+A.ATM+'|'+CAST(D.Rank AS NVARCHAR(10))+'|'+CAST(C.Rank AS NVARCHAR(10))+'|'+CAST(B.Rank AS NVARCHAR(10))) AS LeftColumn,
(A.TypeName+'|'+A.CodeName) AS HeaderColumn, CAST(A.Amount  AS MONEY) AS Amount
FROM [dbo].[SLR_ReportOffice_Close] A
LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey_M
LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
WHERE A.DateWrite BETWEEN @FromDate and @ToDate 
AND A.RecordStatus <> 99 AND A.Publish_Close =1  @Parameter
ORDER BY D.Rank,C.Rank,B.Rank, LEN(A.EmployeeID),A.EmployeeID, A.Rank
";
            zSQL = zSQL.Replace("@Parameter", Fillter);
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search.Trim() + "%";
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        //Xem theo Nhóm
        public static DataTable No17_NHOMMACDINH(DateTime FromDate, int DepartmentKey, int TeamKey, string Salary, int Pay)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            string Fillter = "";
            if (Salary.Length > 0)
            {
                Fillter += " AND " + Salary;
            }
            if (DepartmentKey > 0)
            {
                Fillter += " AND A.DepartmentKey = @DepartmentKey";
            }
            if (TeamKey > 0)
            {
                Fillter += " AND A.TeamKey = @TeamKey";
            }
            if (Pay > 0)
            {
                if (Pay == 1)
                    Fillter += " AND A.ATM !=''";
                if (Pay == 2)
                    Fillter += " AND A.ATM =''";
            }
            DataTable zTable = new DataTable();
            string zSQL = @"
--declare @FromDate datetime ='2020-06-01 00:00:00'
--declare @ToDate datetime ='2020-06-30 23:59:59'

CREATE TABLE #Temp(
DepartmentKey nvarchar(10),
DepartmentID nvarchar(50),
DepartmentName nvarchar(500),
TeamKey nvarchar(10),
TeamID nvarchar(50),
TeamName nvarchar(500),
Amount money,
CodeID nvarchar(250),
CodeName nvarchar(500),
TypeName  nvarchar(500),
Publish_Close nvarchar(10),
[Rank] INT,
ATM nvarchar(50)
)
INSERT INTO #Temp
	SELECT DepartmentKey,DepartmentID,DepartmentName,TeamKey,TeamID,TeamName,ROUND(SUM(Amount),0) AS Amount,
	CodeID,CodeName,TypeName,Publish_Close,[Rank],ATM
	FROM [dbo].[SLR_ReportOffice_Close]
	WHERE DateWrite BETWEEN @FromDate and @ToDate AND Parent IS  NOT NULL  
	AND RecordStatus <> 99 AND Publish_Close =1 
	GROUP BY DepartmentKey,DepartmentID,DepartmentName,TeamKey,TeamID,TeamName,CodeID,CodeName,TypeName,Publish_Close,[Rank],ATM


SELECT (CAST(A.TeamKey AS NVARCHAR(50)) +'|'+A.TeamName+'|'+A.TeamID+'|'+CAST(A.DepartmentKey AS NVARCHAR(50))+'|'+A.DepartmentName+'|'+CAST(D.Rank AS NVARCHAR(10))+'|'+CAST(C.Rank AS NVARCHAR(10))) AS LeftColumn,
(A.TypeName+'|'+A.CodeName) AS HeaderColumn, Amount
FROM #Temp A
LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=A.DepartmentKey
LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey 
WHERE 1=1 @Parameter
ORDER BY A.Rank

DROP TABLE #Temp
";
            zSQL = zSQL.Replace("@Parameter", Fillter);
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable No17_NHOMQUANLY(DateTime FromDate, int DepartmentKey, int TeamKey, string Salary, int Pay)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            string Fillter = "";
            if (Salary.Length > 0)
            {
                Fillter += " AND " + Salary;
            }
            if (DepartmentKey > 0)
            {
                Fillter += " AND A.DepartmentKey_M = @DepartmentKey";
            }
            if (TeamKey > 0)
            {
                Fillter += " AND A.TeamKey_M = @TeamKey";
            }
            if (Pay > 0)
            {
                if (Pay == 1)
                    Fillter += " AND A.ATM !=''";
                if (Pay == 2)
                    Fillter += " AND A.ATM =''";
            }
            DataTable zTable = new DataTable();
            string zSQL = @"
--declare @FromDate datetime ='2020-06-01 00:00:00'
--declare @ToDate datetime ='2020-06-30 23:59:59'

CREATE TABLE #Temp(
DepartmentKey_M nvarchar(10),
DepartmentID_M nvarchar(50),
DepartmentName_M nvarchar(500),
TeamKey_M nvarchar(10),
TeamID_M nvarchar(50),
TeamName_M nvarchar(500),
Amount money,
CodeID nvarchar(250),
CodeName nvarchar(500),
TypeName  nvarchar(500),
Publish_Close nvarchar(10),
[Rank] INT,
ATM nvarchar(50),
)
INSERT INTO #Temp
	SELECT DepartmentKey_M,DepartmentID_M,DepartmentName_M,TeamKey_M,TeamID_M,TeamName_M,ROUND(SUM(Amount),0) AS Amount,
	CodeID,CodeName,TypeName,Publish_Close,[Rank],ATM
	FROM [dbo].[SLR_ReportOffice_Close]
	WHERE DateWrite BETWEEN @FromDate and @ToDate AND Parent IS  NOT NULL  
	AND RecordStatus <> 99 AND Publish_Close =1 
	GROUP BY  DepartmentKey_M,DepartmentID_M,DepartmentName_M,TeamKey_M,TeamID_M,TeamName_M,CodeID,CodeName,TypeName,Publish_Close,[Rank],ATM


SELECT (CAST(A.TeamKey_M AS NVARCHAR(50)) +'|'+A.TeamName_M+'|'+A.TeamID_M+'|'+CAST(A.DepartmentKey_M AS NVARCHAR(50))+'|'+A.DepartmentName_M+'|'+CAST(D.Rank AS NVARCHAR(10))+'|'+CAST(C.Rank AS NVARCHAR(10))) AS LeftColumn,
(A.TypeName+'|'+A.CodeName) AS HeaderColumn, Amount
FROM #Temp A
LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=A.DepartmentKey_M
LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey 
WHERE 1=1 @Parameter
ORDER BY A.Rank

DROP TABLE #Temp
";
            zSQL = zSQL.Replace("@Parameter", Fillter);
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        #endregion

        public static DataTable No18(DateTime FromDate, DateTime ToDate, int BranchKey, int DepartmentKey, int TeamKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string PivotDate = "";
            for (int i = zFromDate.Month; i <= zToDate.Month; i++)
            {
                PivotDate += "[" + i + "],";
            }
            if (PivotDate.Contains(","))
            {
                PivotDate = PivotDate.Remove(PivotDate.LastIndexOf(","));
            }
            string Fillter = "";

            if (BranchKey > 0)
            {
                Fillter += " AND BranchKey = @BranchKey";
            }
            if (DepartmentKey > 0)
                Fillter += " AND DepartmentKey =@DepartmentKey ";
            if (TeamKey > 0)
                Fillter += " AND TeamKey=@TeamKey ";
            DataTable zTable = new DataTable();
            string zSQL = @"
--declare @FromDate datetime ='2020-01-01 00:00:00'
--declare @ToDate datetime ='2020-12-31 23:59:59'
CREATE TABLE #RPT
(	
	Months int,
	EmployeeKey NVARCHAR(50),
	EmployeeID NVARCHAR(100),
	EmployeeName NVARCHAR(MAX),
	TeamKey int,
	TeamID NVARCHAR(50),
	TeamName NVARCHAR(MAX),
	BranchKey int ,
	DepartmentKey int,
	Amount FLOAT
);
INSERT INTO #RPT
SELECT MONTH(DateWrite), EmployeeKey,EmployeeID,EmployeeName,
TeamKey,TeamID,TeamName,BranchKey,DepartmentKey,
SUM(Amount) AS Amount
FROM [dbo].[SLR_ReportWorker_Close]
WHERE CodeID ='TCSX' 
AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120) @Parameter
GROUP BY DateWrite,EmployeeKey,EmployeeID,EmployeeName,TeamKey,TeamID,TeamName,BranchKey,DepartmentKey

INSERT INTO #RPT
SELECT MONTH(DateWrite),EmployeeKey,EmployeeID,EmployeeName,
TeamKey,TeamID,TeamName,BranchKey,DepartmentKey,
SUM(Amount) AS Amount
FROM [dbo].[SLR_ReportSupport_Close]
WHERE CodeID ='TCSX' AND  DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120) @Parameter
GROUP BY DateWrite,EmployeeKey,EmployeeID,EmployeeName,TeamKey,TeamID,TeamName,BranchKey,DepartmentKey

INSERT INTO #RPT
SELECT MONTH(DateWrite),EmployeeKey,EmployeeID,EmployeeName,
TeamKey,TeamID,TeamName,BranchKey,DepartmentKey,
SUM(Amount) AS Amount
FROM [dbo].[SLR_ReportOffice_Close]
WHERE CodeID ='TCSX'AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120) @Parameter
GROUP BY DateWrite,EmployeeKey,EmployeeID,EmployeeName,TeamKey,TeamID,TeamName,BranchKey,DepartmentKey

";

            zSQL += @" SELECT P.* FROM (

                SELECT Months,EmployeeKey,EmployeeID,EmployeeName,TeamKey,TeamID,TeamName,Amount FROM #RPT
                    ) 
                X PIVOT(
                     SUM(Amount)
                     FOR [Months] IN (" + PivotDate + ")) P ";

            zSQL += @" LEFT JOIN[dbo].[SYS_Team] B ON B.TeamKey=P.TeamKey 
        LEFT JOIN[dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
        LEFT JOIN[dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
        ORDER BY D.Rank, C.Rank, B.Rank, LEN(EmployeeID), EmployeeID";


            zSQL += " DROP TABLE #RPT";
            zSQL = zSQL.Replace("@Parameter", Fillter);
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BranchKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        //Nâng cấp  thành report 4.6
        public static DataTable No18_V2_ChuaNhanHeSoGio(DateTime FromDate, DateTime ToDate, int BranchKey, int DepartmentKey, int TeamKey, string MathMoney, double Money, string MathTime, double Time, string Name)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            int SoThang = 0;
            for (int i = FromDate.Month; i <= ToDate.Month; i++)
            {
                SoThang++;
            }



            string Fillter = "";

            if (BranchKey > 0)
            {
                Fillter += " AND BranchKey = @BranchKey";
            }
            if (DepartmentKey > 0)
                Fillter += " AND DepartmentKey =@DepartmentKey ";
            if (TeamKey > 0)
                Fillter += " AND TeamKey =@TeamKey ";
            if (Money != 0)
            {
                Fillter += " AND [MONEY] " + MathMoney + " @Money";
            }
            if (Time != 0)
            {
                Fillter += " AND [MEDIUM] " + MathTime + " @Time";
            }
            if (Name.Length > 0)
            {
                Fillter += " AND EmployeeID = @EmployeeID";
            }
            string zSQL = @"

--declare @FromDate datetime ='2020-01-01 00:00:00'
--declare @ToDate datetime ='2020-12-31 23:59:59'
--declare @SoThang int =12
--declare @Money int =800000
--declare @Time int =4

CREATE TABLE #Temp
(
	[BranchKey] INT ,
	[DepartmentKey] INT,
	[TeamKey] INT,
	[TeamID] nvarchar(500) ,
	[TeamName] nvarchar(500) ,
	EmployeeKey nvarchar(500),
	EmployeeID nvarchar(500),
	EmployeeName nvarchar(500),
	[DateWrite] nvarchar(500),
	[Money] FLOAT,
	[TimeKeeping] FLOAT,
	[Time] FLOAT,
)
CREATE TABLE #Temp2
(
	[BranchKey] INT ,
	[DepartmentKey] INT,
	[TeamKey] INT,
	[TeamID] nvarchar(500) ,
	[TeamName] nvarchar(500) ,
	EmployeeKey nvarchar(500),
	EmployeeID nvarchar(500),
	EmployeeName nvarchar(500),
	[DateWrite] nvarchar(500),
	[Money] FLOAT,
	[TimeKeeping] FLOAT,
	[Time] FLOAT,
)
CREATE TABLE #Temp3
(
	[BranchKey] INT ,
	[DepartmentKey] INT,
	[TeamKey] INT,
	[TeamID] nvarchar(500) ,
	[TeamName] nvarchar(500) ,
	EmployeeKey nvarchar(500),
	EmployeeID nvarchar(500),
	EmployeeName nvarchar(500),
	[DateWrite] nvarchar(500),
	[Money] FLOAT,
	[TimeKeeping] FLOAT,
	[Time] FLOAT,
	[Medium] FLOAT,
)
--Tổng tiền sản xuất
		--Trực tiếp
		INSERT INTO #Temp
		SELECT BranchKey,DepartmentKey,TeamKey,TeamID,TeamName, EmployeeKey,EmployeeID,EmployeeName,
		MONTH(DateWrite),SUM(Amount),0,0
		FROM [dbo].[SLR_ReportWorker_Close]
		WHERE CodeID ='TCSX' 
		AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120) 
		GROUP BY DateWrite,EmployeeKey,EmployeeID,EmployeeName,TeamKey,TeamID,TeamName,BranchKey,DepartmentKey
		--Hỗ trợ
		INSERT INTO #Temp
		SELECT BranchKey,DepartmentKey,TeamKey,TeamID,TeamName, EmployeeKey,EmployeeID,EmployeeName,
		MONTH(DateWrite),SUM(Amount) ,0,0
		FROM [dbo].[SLR_ReportSupport_Close]
		WHERE CodeID ='TCSX' AND  DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120) 
		GROUP BY DateWrite,EmployeeKey,EmployeeID,EmployeeName,TeamKey,TeamID,TeamName,BranchKey,DepartmentKey
		--Văn phòng
		INSERT INTO #Temp
		SELECT BranchKey,DepartmentKey,TeamKey,TeamID,TeamName, EmployeeKey,EmployeeID,EmployeeName,
		MONTH(DateWrite),SUM(Amount) ,0,0
		FROM [dbo].[SLR_ReportOffice_Close]
		WHERE CodeID ='TCSX'AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120) 
		GROUP BY DateWrite,EmployeeKey,EmployeeID,EmployeeName,TeamKey,TeamID,TeamName,BranchKey,DepartmentKey

--Tổng công 
		--Trực tiếp
		INSERT INTO #Temp
		SELECT BranchKey,DepartmentKey,TeamKey,TeamID,TeamName, EmployeeKey,EmployeeID,EmployeeName,
		MONTH(DateWrite),0,SUM(Amount),0
		FROM [dbo].[SLR_ReportWorker_Close]
		WHERE CodeID ='SLTC' 
		AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120) 
		GROUP BY DateWrite,EmployeeKey,EmployeeID,EmployeeName,TeamKey,TeamID,TeamName,BranchKey,DepartmentKey
		--Hỗ trợ
		INSERT INTO #Temp
		SELECT  BranchKey,DepartmentKey,TeamKey,TeamID,TeamName, EmployeeKey,EmployeeID,EmployeeName,
		MONTH(DateWrite),0,SUM(Amount) ,0
		FROM [dbo].[SLR_ReportSupport_Close]
		WHERE CodeID ='NCTT' AND  DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120) 
		GROUP BY DateWrite,EmployeeKey,EmployeeID,EmployeeName,TeamKey,TeamID,TeamName,BranchKey,DepartmentKey
		--Văn phòng
		INSERT INTO #Temp
		SELECT BranchKey,DepartmentKey,TeamKey,TeamID,TeamName, EmployeeKey,EmployeeID,EmployeeName,
		MONTH(DateWrite),0,SUM(Amount),0
		FROM [dbo].[SLR_ReportOffice_Close]
		WHERE CodeID ='NCTT'AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120) 
		GROUP BY DateWrite,EmployeeKey,EmployeeID,EmployeeName,TeamKey,TeamID,TeamName,BranchKey,DepartmentKey
--Tổng giờ chưa nhân hệ số
		INSERT INTO #Temp
		--Tổng giờ trực tiếp
		SELECT [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey) AS BranchKey,
		[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey) AS DepartmentKey,
		TeamKey,TeamID,TeamName,EmployeeKey,EmployeeID,EmployeeName,
		MONTH(DateImport) AS DateWrite,
		0,0,SUM(ROUND((CAST (LEFT(TotalTime ,2) AS MONEY) * 60 + CAST (Right(TotalTime ,2) AS MONEY)),0))
		FROM [dbo].[Temp_Import_Detail] 
		WHERE RecordStatus <> 99 
		AND DateImport BETWEEN @FromDate AND @ToDate
		GROUP BY  [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
		[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),TeamKey,TeamName,TeamID,
		EmployeeKey,EmployeeID,EmployeeName,MONTH(DateImport)

		--Tổng giờ  gián tiếp và hỗ trợ
		INSERT INTO #Temp
		SELECT [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey) AS BranchKey,
		[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey) AS DepartmentKey,
		TeamKey,TeamID,TeamName,UPPER(EmployeeKey),EmployeeID,[dbo].[Fn_GetFullName](EmployeeKey) AS EmployeeName,
		MONTH(DateWrite) AS DateWrite,
		0,0,SUM(ROUND((CAST (LEFT(TotalTime ,2) AS MONEY) * 60 + CAST (Right(TotalTime ,2) AS MONEY)),0))
		FROM [dbo].[HRM_Employee_KeepingTime]
		WHERE RecordStatus <> 99 
		AND DateWrite BETWEEN @FromDate AND @ToDate
		AND ( [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey) =2 OR ([dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey) =4 AND [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey) =26))
		GROUP BY  [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
		[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),TeamKey,TeamName,TeamID,
		UPPER(EmployeeKey),EmployeeID,[dbo].[Fn_GetFullName](EmployeeKey),MONTH(DateWrite)
	;WITH #H AS
        (
		SELECT BranchKey,DepartmentKey,TeamKey,TeamID,TeamName,EmployeeKey,EmployeeID,EmployeeName,DateWrite,SUM([Money]) AS [MONEY],
		SUM(TimeKeeping) AS TIMEKEEPING ,SUM([Time]) AS [TIME] FROM #Temp
		GROUP BY BranchKey,DepartmentKey,TeamKey,TeamName,TeamID,EmployeeKey,EmployeeID,EmployeeName,DateWrite
	        UNION ALL --98: tong
	    SELECT BranchKey,DepartmentKey,TeamKey,TeamID,TeamName,EmployeeKey,EmployeeID,EmployeeName,'98'DateWrite,SUM([Money]) AS [MONEY],
		SUM(TimeKeeping) AS TIMEKEEPING ,SUM([Time]) AS [TIME] FROM #Temp
		GROUP BY BranchKey,DepartmentKey,TeamKey,TeamName,TeamID,EmployeeKey,EmployeeID,EmployeeName
			UNION ALL--99: Trung binh
	    SELECT BranchKey,DepartmentKey,TeamKey,TeamID,TeamName,EmployeeKey,EmployeeID,EmployeeName,'99'DateWrite,SUM([Money])/@SoThang AS [MONEY],
		SUM(TimeKeeping)/@SoThang AS TIMEKEEPING ,SUM([Time])/@SoThang AS [TIME] FROM #Temp
		GROUP BY BranchKey,DepartmentKey,TeamKey,TeamName,TeamID,EmployeeKey,EmployeeID,EmployeeName
        ) 

--Tao bang tam
		INSERT INTO #Temp2
		SELECT * FROM #H
--Tính trung bình
		INSERT INTO #Temp3
		SELECT BranchKey,DepartmentKey,TeamKey,TeamID,TeamName,
				EmployeeKey,EmployeeID,EmployeeName,DateWrite,
				[Money],TIMEKEEPING,TIME,
				CASE WHEN TIMEKEEPING >0  THEN  (TIME/60) / TIMEKEEPING
						ELSE 0
				END AS [MEDIUM]
		FROM #Temp2
--Select bảng tạm dữ liệu đã tạo
SELECT 
P.TeamName + '|' + P.EmployeeName + '|' + P.EmployeeID+'|'+CAST(D.Rank AS NVARCHAR(10))+'|'+CAST(C.Rank AS NVARCHAR(10))+'|'+CAST(ISNULL(B.Rank,'0') AS NVARCHAR(10)) AS LeftColumn,
P.DateWrite AS HeaderColumn,
P.[MONEY], P.TIMEKEEPING, P.[TIME],P.[MEDIUM]
FROM #Temp3 P
LEFT JOIN[dbo].[SYS_Team] B ON B.TeamKey=P.TeamKey 
LEFT JOIN[dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
LEFT JOIN[dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
WHERE P.EmployeeKey IN
(--Lấy điều kiện dựa vào số liệu tổng cộng và trung bình
	SELECT EmployeeKey FROM #Temp2 WHERE 1=1 AND (DateWrite = 99 OR DateWrite=98)  @Parameter
    GROUP BY EmployeeKey
)
ORDER BY DateWrite,D.Rank,C.Rank,B.Rank,LEN(EmployeeID), EmployeeID
DROP TABLE #Temp
DROP TABLE #Temp2
DROP TABLE #Temp3";
            zSQL = zSQL.Replace("@Parameter", Fillter);
            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BranchKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@Money", SqlDbType.Money).Value = Money;
                zCommand.Parameters.Add("@Time", SqlDbType.Money).Value = Time;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = Name;
                zCommand.Parameters.Add("@SoThang", SqlDbType.Int).Value = SoThang;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable No18_V2_DaNhanHeSoGio(DateTime FromDate, DateTime ToDate, int BranchKey, int DepartmentKey, int TeamKey, string MathMoney, double Money, string MathTime, double Time, string Name)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            int SoThang = 0;
            for (int i = FromDate.Month; i <= ToDate.Month; i++)
            {
                SoThang++;
            }
            string Fillter = "";

            if (BranchKey > 0)
            {
                Fillter += " AND BranchKey = @BranchKey";
            }
            if (DepartmentKey > 0)
                Fillter += " AND DepartmentKey =@DepartmentKey ";
            if (TeamKey > 0)
                Fillter += " AND TeamKey =@TeamKey ";
            if (Money != 0)
            {
                Fillter += " AND [MONEY] " + MathMoney + " @Money";
            }
            if (Time != 0)
            {
                Fillter += " AND [MEDIUM] " + MathTime + " @Time";
            }
            if (Name.Length > 0)
            {
                Fillter += " AND EmployeeID = @EmployeeID";
            }
            string zSQL = @"
--declare @FromDate datetime ='2020-01-01 00:00:00'
--declare @ToDate datetime ='2020-12-31 23:59:59'
--declare @SoThang int =12
--declare @Money int =800000
--declare @Time int =4

CREATE TABLE #Temp
(
	[BranchKey] INT ,
	[DepartmentKey] INT,
	[TeamKey] INT,
	[TeamID] nvarchar(500) ,
	[TeamName] nvarchar(500) ,
	EmployeeKey nvarchar(500),
	EmployeeID nvarchar(500),
	EmployeeName nvarchar(500),
	[DateWrite] nvarchar(500),
	[Money] FLOAT,
	[TimeKeeping] FLOAT,
	[Time] FLOAT,
)
CREATE TABLE #Temp2
(
	[BranchKey] INT ,
	[DepartmentKey] INT,
	[TeamKey] INT,
	[TeamID] nvarchar(500) ,
	[TeamName] nvarchar(500) ,
	EmployeeKey nvarchar(500),
	EmployeeID nvarchar(500),
	EmployeeName nvarchar(500),
	[DateWrite] nvarchar(500),
	[Money] FLOAT,
	[TimeKeeping] FLOAT,
	[Time] FLOAT,
)
CREATE TABLE #Temp3
(
	[BranchKey] INT ,
	[DepartmentKey] INT,
	[TeamKey] INT,
	[TeamID] nvarchar(500) ,
	[TeamName] nvarchar(500) ,
	EmployeeKey nvarchar(500),
	EmployeeID nvarchar(500),
	EmployeeName nvarchar(500),
	[DateWrite] nvarchar(500),
	[Money] FLOAT,
	[TimeKeeping] FLOAT,
	[Time] FLOAT,
	[Medium] FLOAT,
)
--Tổng tiền sản xuất
		--Trực tiếp
		INSERT INTO #Temp
		SELECT BranchKey,DepartmentKey,TeamKey,TeamID,TeamName, EmployeeKey,EmployeeID,EmployeeName,
		MONTH(DateWrite),SUM(Amount),0,0
		FROM [dbo].[SLR_ReportWorker_Close]
		WHERE CodeID ='TCSX' 
		AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120) 
		GROUP BY DateWrite,EmployeeKey,EmployeeID,EmployeeName,TeamKey,TeamID,TeamName,BranchKey,DepartmentKey
		--Hỗ trợ
		INSERT INTO #Temp
		SELECT BranchKey,DepartmentKey,TeamKey,TeamID,TeamName, EmployeeKey,EmployeeID,EmployeeName,
		MONTH(DateWrite),SUM(Amount) ,0,0
		FROM [dbo].[SLR_ReportSupport_Close]
		WHERE CodeID ='TCSX' AND  DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120) 
		GROUP BY DateWrite,EmployeeKey,EmployeeID,EmployeeName,TeamKey,TeamID,TeamName,BranchKey,DepartmentKey
		--Văn phòng
		INSERT INTO #Temp
		SELECT BranchKey,DepartmentKey,TeamKey,TeamID,TeamName, EmployeeKey,EmployeeID,EmployeeName,
		MONTH(DateWrite),SUM(Amount) ,0,0
		FROM [dbo].[SLR_ReportOffice_Close]
		WHERE CodeID ='TCSX'AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120) 
		GROUP BY DateWrite,EmployeeKey,EmployeeID,EmployeeName,TeamKey,TeamID,TeamName,BranchKey,DepartmentKey

--Tổng công 
		--Trực tiếp
		INSERT INTO #Temp
		SELECT BranchKey,DepartmentKey,TeamKey,TeamID,TeamName, EmployeeKey,EmployeeID,EmployeeName,
		MONTH(DateWrite),0,SUM(Amount),0
		FROM [dbo].[SLR_ReportWorker_Close]
		WHERE CodeID ='SLTC' 
		AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120) 
		GROUP BY DateWrite,EmployeeKey,EmployeeID,EmployeeName,TeamKey,TeamID,TeamName,BranchKey,DepartmentKey
		--Hỗ trợ
		INSERT INTO #Temp
		SELECT  BranchKey,DepartmentKey,TeamKey,TeamID,TeamName, EmployeeKey,EmployeeID,EmployeeName,
		MONTH(DateWrite),0,SUM(Amount) ,0
		FROM [dbo].[SLR_ReportSupport_Close]
		WHERE CodeID ='NCTT' AND  DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120) 
		GROUP BY DateWrite,EmployeeKey,EmployeeID,EmployeeName,TeamKey,TeamID,TeamName,BranchKey,DepartmentKey
		--Văn phòng
		INSERT INTO #Temp
		SELECT BranchKey,DepartmentKey,TeamKey,TeamID,TeamName, EmployeeKey,EmployeeID,EmployeeName,
		MONTH(DateWrite),0,SUM(Amount),0
		FROM [dbo].[SLR_ReportOffice_Close]
		WHERE CodeID ='NCTT'AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120) 
		GROUP BY DateWrite,EmployeeKey,EmployeeID,EmployeeName,TeamKey,TeamID,TeamName,BranchKey,DepartmentKey
--Tổng giờ  nhân hệ số
		INSERT INTO #Temp
		--Tổng giờ trực tiếp
		SELECT [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey) AS BranchKey,
		[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey) AS DepartmentKey,
		TeamKey,TeamID,TeamName,EmployeeKey,EmployeeID,EmployeeName,
		MONTH(DateImport) AS DateWrite,
		0,0,SUM(ROUND(((CAST (LEFT(TotalTime ,2) AS MONEY) * 60 + CAST (Right(TotalTime ,2) AS MONEY))* Rate),0))
		FROM [dbo].[Temp_Import_Detail] 
		WHERE RecordStatus <> 99 
		AND DateImport BETWEEN @FromDate AND @ToDate
		GROUP BY  [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
		[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),TeamKey,TeamName,TeamID,
		EmployeeKey,EmployeeID,EmployeeName,MONTH(DateImport)

		--Tổng giờ  gián tiếp và hỗ trợ
		INSERT INTO #Temp
		SELECT [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey) AS BranchKey,
		[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey) AS DepartmentKey,
		TeamKey,TeamID,TeamName,UPPER(EmployeeKey),EmployeeID, [dbo].[Fn_GetFullName](EmployeeKey) AS EmployeeName,
		MONTH(DateWrite) AS DateWrite,
		0,0,SUM(ROUND((CAST (LEFT(TotalTime ,2) AS MONEY) * 60 + CAST (Right(TotalTime ,2) AS MONEY)),0))
		FROM [dbo].[HRM_Employee_KeepingTime]
		WHERE RecordStatus <> 99 
		AND DateWrite BETWEEN @FromDate AND @ToDate
		AND ( [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey) =2 OR ([dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey) =4 AND [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey) =26))
		GROUP BY  [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
		[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),TeamKey,TeamName,TeamID,
		UPPER(EmployeeKey),EmployeeID,[dbo].[Fn_GetFullName](EmployeeKey),MONTH(DateWrite)

 ;WITH #H AS
        (
		SELECT BranchKey,DepartmentKey,TeamKey,TeamID,TeamName,EmployeeKey,EmployeeID,EmployeeName,DateWrite,SUM([Money]) AS [MONEY],
		SUM(TimeKeeping) AS TIMEKEEPING ,SUM([Time]) AS [TIME] FROM #Temp
		GROUP BY BranchKey,DepartmentKey,TeamKey,TeamName,TeamID,EmployeeKey,EmployeeID,EmployeeName,DateWrite
	        UNION ALL --98: tong
	    SELECT BranchKey,DepartmentKey,TeamKey,TeamID,TeamName,EmployeeKey,EmployeeID,EmployeeName,'98'DateWrite,SUM([Money]) AS [MONEY],
		SUM(TimeKeeping) AS TIMEKEEPING ,SUM([Time]) AS [TIME] FROM #Temp
		GROUP BY BranchKey,DepartmentKey,TeamKey,TeamName,TeamID,EmployeeKey,EmployeeID,EmployeeName
			UNION ALL--99: Trung binh
	    SELECT BranchKey,DepartmentKey,TeamKey,TeamID,TeamName,EmployeeKey,EmployeeID,EmployeeName,'99'DateWrite,SUM([Money])/@SoThang AS [MONEY],
		SUM(TimeKeeping)/@SoThang AS TIMEKEEPING ,SUM([Time])/@SoThang AS [TIME] FROM #Temp
		GROUP BY BranchKey,DepartmentKey,TeamKey,TeamName,TeamID,EmployeeKey,EmployeeID,EmployeeName
        ) 

--Tao bang tam
		INSERT INTO #Temp2
		SELECT * FROM #H
--Tính trung bình
		INSERT INTO #Temp3
		SELECT BranchKey,DepartmentKey,TeamKey,TeamID,TeamName,
				EmployeeKey,EmployeeID,EmployeeName,DateWrite,
				[Money],TIMEKEEPING,TIME,
				CASE WHEN TIMEKEEPING >0  THEN  (TIME/60) / TIMEKEEPING
						ELSE 0
				END AS [MEDIUM]
		FROM #Temp2
--Select bảng tạm dữ liệu đã tạo
SELECT 
P.TeamName + '|' + P.EmployeeName + '|' + P.EmployeeID+'|'+CAST(D.Rank AS NVARCHAR(10))+'|'+CAST(C.Rank AS NVARCHAR(10))+'|'+CAST(ISNULL(B.Rank,'0') AS NVARCHAR(10)) AS LeftColumn,
P.DateWrite AS HeaderColumn,
P.[MONEY], P.TIMEKEEPING, P.[TIME],P.[MEDIUM]
FROM #Temp3 P
LEFT JOIN[dbo].[SYS_Team] B ON B.TeamKey=P.TeamKey 
LEFT JOIN[dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
LEFT JOIN[dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
WHERE P.EmployeeKey IN
(--Lấy điều kiện dựa vào số liệu tổng cộng và trung bình
	SELECT EmployeeKey FROM #Temp2 WHERE 1=1 AND (DateWrite = 99 OR DateWrite=98)  @Parameter
    GROUP BY EmployeeKey
)
ORDER BY DateWrite,D.Rank,C.Rank,B.Rank,LEN(EmployeeID), EmployeeID
DROP TABLE #Temp
DROP TABLE #Temp2
DROP TABLE #Temp3";
            zSQL = zSQL.Replace("@Parameter", Fillter);
            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BranchKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@Money", SqlDbType.Money).Value = Money;
                zCommand.Parameters.Add("@Time", SqlDbType.Money).Value = Time;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = Name;
                zCommand.Parameters.Add("@SoThang", SqlDbType.Int).Value = SoThang;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }



        public static DataTable Report_47(DateTime FromDate, DateTime ToDate, string NhomViec)
        {
            string zSQL = @"SELECT 
ISNULL(dbo.LayLoaiHang(A.OrderIDFollow, A.ProductKey), N'Chưa Phân Loại') +'|'+ A.ProductID AS SANPHAM, 
dbo.LayNhomCongViec(A.Category_Stage) +'|'+ dbo.LayTenCongViec(A.Category_Stage) +'|'+ CONVERT(NVARCHAR(50), dbo.LayGiaCongViec(A.Category_Stage)) AS CongViec,
SUM(ISNULL([MONEY], 0) + 
		ISNULL([Money_Borrow] ,0) +
		ISNULL([MONEYEAT], 0) +
		ISNULL([Money_Dif_Private], 0) +
		ISNULL([Money_Dif_Private], 0) +
		ISNULL(Money_Dif_General, 0) +
		ISNULL(Money_Dif_Sunday_Private, 0) +
		ISNULL(Money_Dif_SunDay_General, 0) +
		ISNULL(Money_Dif_Holiday_Private, 0) +
		ISNULL(Money_Dif_Holiday_General, 0)) AS LK
FROM FTR_Order A
LEFT JOIN FTR_Order_Money B ON A.OrderKey = B.OrderKey
WHERE 
A.RecordStatus <> 99
AND B.RecordStatus <> 99
AND A.OrderDate BETWEEN @FromDate AND @ToDate";
            if (NhomViec != "0")
            {
                zSQL += " AND dbo.LayNhomCongViec(A.Category_Stage) = @NhomViec";
            }

            zSQL += " GROUP BY A.ProductID, A.Category_Stage, A.OrderIDFollow, A.ProductKey ORDER BY CongViec";

            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zCommand.Parameters.Add("@NhomViec", SqlDbType.NVarChar).Value = NhomViec;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable Report_47_V2(DateTime FromDate, DateTime ToDate, string NhomViec)
        {
            string zSQL = @"SELECT 
ISNULL(dbo.LayLoaiHang(A.OrderIDFollow, A.ProductKey), N'Chưa Phân Loại') +'|'+ A.ProductID AS SANPHAM, 
dbo.Fn_GetTeamName(A.TeamKey) +'|'+ dbo.LayTenCongViec(A.Category_Stage) +'|'+ CONVERT(NVARCHAR(50), dbo.LayGiaCongViec(A.Category_Stage)) AS CongViec,
SUM(ISNULL([MONEY], 0) + 
		ISNULL([Money_Borrow] ,0) +
		ISNULL([MONEYEAT], 0) +
		ISNULL([Money_Dif_Private], 0) +
		ISNULL([Money_Dif_Private], 0) +
		ISNULL(Money_Dif_General, 0) +
		ISNULL(Money_Dif_Sunday_Private, 0) +
		ISNULL(Money_Dif_SunDay_General, 0) +
		ISNULL(Money_Dif_Holiday_Private, 0) +
		ISNULL(Money_Dif_Holiday_General, 0)) AS LK
FROM FTR_Order A
LEFT JOIN FTR_Order_Money B ON A.OrderKey = B.OrderKey
WHERE 
A.RecordStatus <> 99
AND B.RecordStatus <> 99
AND A.OrderDate BETWEEN @FromDate AND @ToDate";
            if (NhomViec != "0")
            {
                zSQL += " AND dbo.LayNhomCongViec(A.Category_Stage) = @NhomViec";
            }

            zSQL += " GROUP BY A.ProductID, A.Category_Stage, A.OrderIDFollow, A.ProductKey, A.TeamKey ORDER BY CongViec";

            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zCommand.Parameters.Add("@NhomViec", SqlDbType.NVarChar).Value = NhomViec;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        #region[Report 41]
        public static DataTable Report_41_Master(DateTime FromDate, DateTime ToDate, string NhomViec)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            string zSQL = @"SELECT 
A.Category_Stage,
dbo.LayNhomCongViec(A.Category_Stage) AS NhomCongViec, 
dbo.LayTenCongViec(A.Category_Stage) AS TenCongViec,
COUNT(DISTINCT B.EmployeeKey) AS SoCongNhan,
SUM(B.Time) AS SoThoiGian,
SUM(B.Basket) AS SoRo,
SUM(A.QuantityDocument) AS SLNguyenLieu,
SUM(B.Kilogram) AS SLThanhPham,
dbo.TyLeNangSuat(SUM(A.QuantityDocument),SUM(B.Time)) AS NSNguyenLieu,
dbo.TyLeNangSuat(SUM(B.Kilogram),SUM(B.Time)) AS NSThanhPham
FROM FTR_Order A
LEFT JOIN FTR_Order_Employee B ON A.OrderKey = B.OrderKey
WHERE A.OrderDate BETWEEN @FromDate AND @ToDate
AND B.RecordStatus <> 99
AND A.RecordStatus <> 99
";
            if (NhomViec != "0")
            {
                zSQL += " AND dbo.LayNhomCongViec(A.Category_Stage) = @NhomViec";
            }

            zSQL += " GROUP BY A.Category_Stage ORDER BY NhomCongViec, TenCongViec";

            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@NhomViec", SqlDbType.NVarChar).Value = NhomViec;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable Report_41_Detail(DateTime FromDate, DateTime ToDate, int CongViec)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            string zSQL = @"
SELECT 
dbo.Fn_GetFullName(A.EmployeeKey), 
A.EmployeeID,
SUM(a.Time) AS SoThoiGian,
SUM(a.Basket) AS SoRo,
SUM(B.QuantityDocument) AS SLNguyenLieu,
SUM(A.Kilogram) AS SLThanhPham,
dbo.TyLeNangSuat(SUM(B.QuantityDocument),SUM(A.Time)) AS NSNguyenLieu,
dbo.TyLeNangSuat(SUM(A.Kilogram),SUM(A.Time)) AS NSThanhPham
FROM 
FTR_Order_Employee A 
LEFT JOIN FTR_Order B ON A.OrderKey = B.OrderKey
WHERE 
A.OrderDate BETWEEN @FromDate AND @ToDate
AND B.RecordStatus <> 99
AND A.RecordStatus <> 99
AND B.Category_Stage = @CongViec
GROUP BY A.EmployeeKey, A.EmployeeID
ORDER BY LEN(EmployeeID), EmployeeID
";

            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@CongViec", SqlDbType.Int).Value = CongViec;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        #endregion
        public static DataTable Report_42(DateTime FromDate, DateTime ToDate, string MaCongNhan, string NhomViec)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            string zSQL = @"
SELECT 
A.EmployeeID +'|'+ dbo.Fn_GetFullName(A.EmployeeKey) AS CongNhan, 
dbo.LayNhomCongViec(B.Category_Stage) + '|' + dbo.LayTenCongViec(B.Category_Stage) AS TenCongViec,
SUM(a.Time) AS TG,
SUM(a.Basket) AS RO,
SUM(a.Kilogram) AS KG
FROM 
FTR_Order_Employee A 
LEFT JOIN FTR_Order B ON A.OrderKey = B.OrderKey
WHERE 
A.OrderDate BETWEEN @FromDate AND @ToDate
AND B.RecordStatus <> 99
AND A.RecordStatus <> 99
";
            if (MaCongNhan != string.Empty)
            {
                zSQL += " AND A.EmployeeID = @MaCongNhan";
            }

            if (NhomViec != "0")
            {
                zSQL += " AND dbo.LayNhomCongViec(B.Category_Stage) = @NhomCongViec";
            }

            zSQL += " GROUP BY A.EmployeeKey, A.EmployeeID, B.Category_Stage ORDER BY TenCongViec";

            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@MaCongNhan", SqlDbType.NVarChar).Value = MaCongNhan;
                zCommand.Parameters.Add("@NhomCongViec", SqlDbType.NVarChar).Value = NhomViec;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable Report_43(DateTime FromDate, DateTime ToDate, string NhomCongNhan)
        {
            string zSQL = @"
SELECT 
A.ProductID AS SANPHAM, 
dbo.Fn_GetTeamName(B.TeamKey) +'|'+ dbo.LayTenCongViec(A.Category_Stage) +'|'+ CONVERT(NVARCHAR(50), dbo.LayGiaCongViec(A.Category_Stage)) AS CongViec,
SUM(ISNULL([MONEY], 0) + 
		ISNULL([Money_Borrow] ,0) +
		ISNULL([MONEYEAT], 0) +
		ISNULL([Money_Dif_Private], 0) +
		ISNULL([Money_Dif_Private], 0) +
		ISNULL(Money_Dif_General, 0) +
		ISNULL(Money_Dif_Sunday_Private, 0) +
		ISNULL(Money_Dif_SunDay_General, 0) +
		ISNULL(Money_Dif_Holiday_Private, 0) +
		ISNULL(Money_Dif_Holiday_General, 0)) AS LK
FROM FTR_Order A
LEFT JOIN FTR_Order_Money B ON A.OrderKey = B.OrderKey
WHERE 
A.RecordStatus <> 99
AND B.RecordStatus <> 99
AND A.OrderDate BETWEEN @FromDate AND @ToDate
";
            if (NhomCongNhan != "0")
            {
                zSQL += " AND B.TeamKey = @NhomCongNhan";
            }

            zSQL += " GROUP BY A.ProductID, A.Category_Stage, A.ProductKey, B.TeamKey ORDER BY CongViec";

            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zCommand.Parameters.Add("@NhomCongNhan", SqlDbType.NVarChar).Value = NhomCongNhan;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable Report_44(DateTime FromDate, DateTime ToDate, string MaCongNhan)
        {
            #region [SQL]
            string zSubSQL1 = @"
                SELECT 
				A.EmployeeID, 
				A.EmployeeName, 
				A.OrderDate,								
				dbo.LayTenCongViec(A.StageKey) AS CongViec,				
				SUM(A.Money+Money_Borrow+Money_Eat+MoneyPrivate+Money_Dif_Private+Money_Dif_General) AS [LK],
				SUM(B.THOIGIAN_TINHTOANLAI) AS TG,
				SUM(Kg+Kg_Borrow+Kg_Eat+KgPrivate) AS SLThanhPham,
				dbo.TyLeNangSuat(SUM(Kg+Kg_Borrow+Kg_Eat+KgPrivate), SUM(B.THOIGIAN_TINHTOANLAI) ) AS NS
				FROM FTR_Order_Money A	
                LEFT JOIN FTR_Order_Employee B ON A.OrderKey = B.OrderKey
				WHERE A.OrderDate BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)";
            if (MaCongNhan != string.Empty)
            {
                zSubSQL1 += " AND A.EmployeeID = @EmployeeID";
            }

            zSubSQL1 += " GROUP BY A.OrderDate, A.TeamKey, A.EmployeeName, A.EmployeeID, A.StageKey";
            //        string zSubSQL2 = @"
            //            SELECT 
            //A.EmployeeID, 
            //A.EmployeeName, 
            //A.OrderDate,										
            //dbo.LayTenCongViec(A.StageKey) AS CongViec,					
            //   SUM(A.MoneyPersonal) AS [LK],
            //SUM(A.Time) AS [TG],
            //SUM(A.Kg+A.KgProduct) AS SLThanhPham,
            //dbo.TyLeNangSuat(SUM(A.Kg+A.KgProduct) ,SUM(A.Time)) AS NS
            //FROM FTR_Order_Adjusted A 				
            //WHERE A.OrderDate BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)";
            //        if (MaCongNhan != string.Empty)
            //        {
            //            zSubSQL2 += " AND A.EmployeeID = @EmployeeID";
            //        }
            //        zSubSQL2 += " GROUP BY A.OrderDate, A.TeamKey, A.EmployeeName, A.EmployeeID, A.StageKey";

            string zSQL = @"
CREATE TABLE #RPT
(	
	MaCongNhan NVARCHAR(100),
	TenCongNhan NVARCHAR(500),
	NgayLamViec Datetime,		
	CongViec NVARCHAR(MAX),	
	LK FLOAT,
	TG FLOAT,
	SLTP FLOAT,
	NS FLOAT,
);

INSERT INTO #RPT " + zSubSQL1 + @"

SELECT MaCongNhan, TenCongNhan, CONVERT(VARCHAR,NgayLamViec,103) AS NGAYLAMVIEC, CongViec, LK, TG, SLTP, NS
FROM #RPT 
ORDER BY CongViec
DROP TABLE #RPT
";
            #endregion

            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = MaCongNhan;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable Report_48(DateTime FromDate, DateTime ToDate, string NhomViec)
        {
            #region [SQL]
            string zSubSQL1 = @"
                SELECT 
        dbo.LayLoaiSP(a.ProductID),
        A.ProductID AS SanPham, 
        dbo.LayNhomCongViec(A.Category_Stage) NhomViec, 
        dbo.LayTenCongViec(A.Category_Stage) CongViec,
        SUM(B.[Time] + B.Time_Borrow + B.Time_Eat + B.TimePrivate) AS [TG],
        SUM(ISNULL([MONEY], 0) + 
		ISNULL([Money_Borrow] ,0) +
		ISNULL([MONEYEAT], 0) +
		ISNULL([Money_Dif_Private], 0) +
		ISNULL([Money_Dif_Private], 0) +
		ISNULL(Money_Dif_General, 0) +
		ISNULL(Money_Dif_Sunday_Private, 0) +
		ISNULL(Money_Dif_SunDay_General, 0) +
		ISNULL(Money_Dif_Holiday_Private, 0) +
		ISNULL(Money_Dif_Holiday_General, 0)) AS [LK],		
		SUM(Kg+Kg_Borrow+Kg_Eat+KgPrivate) AS SLThanhPham
        FROM FTR_Order A
        LEFT JOIN FTR_Order_Money B ON A.OrderKey = B.OrderKey
        WHERE 
        A.RecordStatus <> 99
        AND B.RecordStatus <> 99
        AND A.OrderDate BETWEEN @FromDate AND @ToDate
        GROUP BY A.ProductID, A.Category_Stage, A.ProductKey 
        ORDER BY ProductID";

            string zSubSQL2 = @"
                SELECT 
				dbo.LayLoaiSP(b.ProductID),
				B.ProductID AS SanPham, 
				dbo.LayNhomCongViec(A.StageKey) NhomViec, 
				dbo.LayTenCongViec(A.StageKey) AS CongViec,		
                SUM(A.Time) AS [TG],
				SUM(A.MoneyPersonal) AS [LK],				
				SUM(A.Kg+A.KgProduct) AS SLThanhPham		
				FROM FTR_Order_Adjusted A 				
				LEFT JOIN FTR_Order B ON A.OrderKey = B.OrderKey
				WHERE A.OrderDate BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120) 
				GROUP BY B.ProductID, A.StageKey";

            string zSQL = @"
CREATE TABLE #RPT8
(	
	LOAISANPHAM NVARCHAR(500),
	SANPHAM  NVARCHAR(500),
	NHOMSANPHAM  NVARCHAR(500),
	CONGVIEC NVARCHAR(MAX),
	TG FLOAT,
	LK FLOAT,
	KG FLOAT
);

INSERT INTO #RPT8 " + zSubSQL1 + @"
INSERT INTO #RPT8 " + zSubSQL2 + @"

SELECT LOAISANPHAM + '|' + SANPHAM AS SANPHAM, CONGVIEC, TG, LK, KG
FROM #RPT8 ORDER BY CONGVIEC
DROP TABLE #RPT8
";
            #endregion

            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zCommand.Parameters.Add("@NhomViec", SqlDbType.NVarChar).Value = NhomViec;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }


        //Báo cáo mới
        #region[Report 41_V2]
        #region[không sd]
        //        public static DataTable Report_41_Master_V2(DateTime FromDate, DateTime ToDate, string NhomViec)
        //        {
        //            string zSQL = @"
        //SELECT 
        //A.Category_Stage,
        //dbo.LayNhomCongViec(A.Category_Stage) AS NhomCongViec, 
        //dbo.LayMaCongViec(A.Category_Stage) AS MaCongViec,
        //dbo.LayTenCongViec(A.Category_Stage) AS TenCongViec,
        //dbo.FTR_SoLuongCongNhanThamGiaCongDoan(A.Category_Stage,@FromDate,@ToDate) AS SoCongNhan,
        //SUM(B.Time) AS SoThoiGian,
        //SUM(B.Basket) AS SoRo,
        //dbo.FTR_TongSLNguyenLieuKhiXongCongDoan (A.Category_Stage,@FromDate,@ToDate)AS SLNguyenLieu,
        //dbo.FTR_TongSLThanhPhamKhiXongCongDoan(A.Category_Stage,@FromDate,@ToDate) AS SLThanhPham,
        //(dbo.FTR_TongSLNguyenLieuKhiXongCongDoan (A.Category_Stage,@FromDate,@ToDate)/SUM(B.Time)) AS NSNguyenLieu,
        //(dbo.FTR_TongSLThanhPhamKhiXongCongDoan(A.Category_Stage,@FromDate,@ToDate) /SUM(B.Time)) AS NSThanhPham
        //FROM FTR_Order A
        //LEFT JOIN FTR_Order_Employee B ON A.OrderKey = B.OrderKey
        //WHERE A.OrderDate BETWEEN @FromDate AND @ToDate
        //AND B.RecordStatus <> 99
        //AND A.RecordStatus <> 99
        //";
        //            if (NhomViec != "0")
        //            {
        //                zSQL += " AND dbo.LayNhomCongViec(A.Category_Stage) = @NhomViec";
        //            }

        //            zSQL += " GROUP BY A.Category_Stage ORDER BY NhomCongViec, TenCongViec";

        //            DataTable zTable = new DataTable();
        //            string zConnectionString = ConnectDataBase.ConnectionString;
        //            try
        //            {
        //                SqlConnection zConnect = new SqlConnection(zConnectionString);
        //                zConnect.Open();
        //                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
        //                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
        //                zCommand.Parameters.Add("@NhomViec", SqlDbType.NVarChar).Value = NhomViec;
        //                zCommand.CommandTimeout = 350;
        //                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
        //                zAdapter.Fill(zTable);
        //                zCommand.Dispose();
        //                zConnect.Close();
        //            }
        //            catch (Exception ex)
        //            {
        //                string zstrMessage = ex.ToString();
        //            }
        //            return zTable;
        //        }
        //        public static DataTable Report_41_Detail_V2(DateTime FromDate, DateTime ToDate, int CongViec, float SoGio, float SoRo, float NguyenLieu, float ThanhPham)
        //        {
        //            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
        //            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
        //            string zSQL = @"
        //CREATE TABLE #RPT
        //(	
        //	EmployeeName NVARCHAR(50),
        //	EmployeeID NVARCHAR(50),
        //	TeamID NVARCHAR(50),
        //	SoThoiGian FLOAT,
        //	SoRo FLOAT,
        //	SLNguyenLieu FLOAT,
        //	SLThanhPham FLOAT,
        //	NSNguyenLieu FLOAT,
        //	NSThanhPham FLOAT,
        //	Style INT 
        //);
        //INSERT INTO #RPT
        //SELECT 
        //dbo.Fn_GetFullName(A.EmployeeKey) AS EmployeeName, 
        //A.EmployeeID,
        //[dbo].[Fn_GetTeamID](C.TeamKey)AS TeamID,
        //SUM(a.Time) AS SoThoiGian,
        //SUM(a.Basket) AS SoRo,
        //dbo.FTR_TongSLNguyenLieu_CongDoan_CongNhan
        //(SUM(a.Basket),SUM(a.Time),@NguyenLieu, @SoRo,@SoGio) AS SLNguyenLieu,
        //dbo.FTR_TongSLThanhPham_CongDoan_CongNhan(@Congviec,A.EmployeeKey,@FromDate,@ToDate) AS SLThanhPham,
        //(dbo.FTR_TongSLNguyenLieu_CongDoan_CongNhan
        //( SUM(a.Basket),SUM(a.Time),@NguyenLieu, @SoRo,@SoGio)/SUM(A.Time)) AS NSNguyenLieu,
        //(dbo.FTR_TongSLThanhPham_CongDoan_CongNhan(@Congviec,A.EmployeeKey,@FromDate,@ToDate)/SUM(A.Time)) AS NSThanhPham,
        //0 -- nhóm chính
        //FROM 
        //FTR_Order_Employee A 
        //LEFT JOIN FTR_Order B ON A.OrderKey = B.OrderKey
        //LEFT JOIN [dbo].[HRM_Employee] C ON C.EmployeeKey=A.EmployeeKey
        //WHERE 
        //A.OrderDate BETWEEN @FromDate AND @ToDate
        //AND B.RecordStatus <> 99
        //AND A.RecordStatus <> 99
        //AND B.Category_Stage = @CongViec 
        //AND C.Slug !=1 --Trừ các con ma 
        //AND A.Share !=1 --trừ nhũng người ăn chung
        //GROUP BY A.EmployeeKey, A.EmployeeID,[dbo].[Fn_GetTeamID](C.TeamKey)
        //ORDER BY TeamID, LEN(A.EmployeeID), A.EmployeeID

        //INSERT INTO #RPT
        //SELECT 
        //dbo.Fn_GetFullName(A.EmployeeKey) AS EmployeeName, 
        //A.EmployeeID,
        //[dbo].[Fn_GetTeamID](C.TeamKey)AS TeamID,
        //SUM(A.TimeProduct) AS SoThoiGian,
        //SUM(A.BasketProduct) AS SoRo,
        //dbo.FTR_TongSLNguyenLieu_CongDoan_CongNhan
        //(SUM(A.BasketProduct),SUM(A.TimeProduct),@NguyenLieu, @SoRo,@SoGio) AS SLNguyenLieu,
        //dbo.FTR_TongSLThanhPham_CongDoan_CongNhan(@Congviec,A.EmployeeKey,@FromDate,@ToDate) AS SLThanhPham,
        //(dbo.FTR_TongSLNguyenLieu_CongDoan_CongNhan
        //(SUM(A.BasketProduct),SUM(A.TimeProduct),@NguyenLieu, @SoRo,@SoGio)/SUM(A.TimeProduct)) AS NSNguyenLieu,
        //(dbo.FTR_TongSLThanhPham_CongDoan_CongNhan(@Congviec,A.EmployeeKey,@FromDate,@ToDate)/SUM(A.TimeProduct)) AS NSThanhPham,
        //1 -- nhóm chia lại
        //FROM [dbo].[FTR_Order_Adjusted] A 
        //LEFT JOIN FTR_Order B ON A.OrderKey = CAST(B.OrderKey AS NVARCHAR(50))
        //LEFT JOIN [dbo].[HRM_Employee] C ON C.EmployeeKey=A.EmployeeKey
        //WHERE 
        //A.OrderDate BETWEEN @FromDate AND @ToDate
        //AND B.RecordStatus <> 99
        //AND A.RecordStatus <> 99
        //AND B.Category_Stage = @CongViec
        //AND A.Share =0 AND A.KgProduct >0
        //GROUP BY A.EmployeeKey, A.EmployeeID,[dbo].[Fn_GetTeamID](C.TeamKey)
        //ORDER BY TeamID, LEN(A.EmployeeID), A.EmployeeID


        //SELECT *
        //FROM #RPT
        //ORDER BY Style,TeamID, LEN(EmployeeID),EmployeeID

        //DROP TABLE #RPT
        //";

        //            DataTable zTable = new DataTable();
        //            string zConnectionString = ConnectDataBase.ConnectionString;
        //            try
        //            {
        //                SqlConnection zConnect = new SqlConnection(zConnectionString);
        //                zConnect.Open();
        //                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
        //                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
        //                zCommand.Parameters.Add("@CongViec", SqlDbType.Int).Value = CongViec;
        //                zCommand.Parameters.Add("@NguyenLieu", SqlDbType.Float).Value = NguyenLieu;
        //                zCommand.Parameters.Add("@ThanhPham", SqlDbType.Float).Value = ThanhPham;
        //                zCommand.Parameters.Add("@SoGio", SqlDbType.Float).Value = SoGio;
        //                zCommand.Parameters.Add("@SoRo", SqlDbType.Float).Value = SoRo;
        //                zCommand.CommandTimeout = 350;
        //                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
        //                zAdapter.Fill(zTable);
        //                zCommand.Dispose();
        //                zConnect.Close();
        //            }
        //            catch (Exception ex)
        //            {
        //                string zstrMessage = ex.ToString();
        //            }
        //            return zTable;
        //        }

        #endregion
        #region[KT 04/01/2021]
        //        public static DataTable Report_41_Master_V3(DateTime FromDate, DateTime ToDate, string NhomViec)
        //        {
        //            string zSQL = @"
        //DECLARE @REPORT41_MASTER AS dbo.REPORT41_MASTER

        //INSERT INTO @REPORT41_MASTER
        //(OrderKey,Category_Stage,NhomCongViec,TenCongViec,MaCongViec,SoCongNhan,
        //SoThoiGian,SoRo,SLNguyenLieu,SLThanhPham,NSNguyenLieu,NSThanhPham)
        //SELECT 
        //A.OrderKey,
        //A.Category_Stage,
        //dbo.LayNhomCongViec(A.Category_Stage) AS NhomCongViec, 
        //dbo.LayTenCongViec(A.Category_Stage) AS TenCongViec,
        //dbo.LayMaCongViec(A.Category_Stage) AS MaCongViec,
        //0,
        //CASE 
        //    WHEN dbo.LayNhomCongViec(A.Category_Stage) = 'KHAC' THEN SUM(B.Kg+B.KgPrivate +B.Kg_Borrow)
        //	WHEN dbo.LayNhomCongViec(A.Category_Stage) = 'CVTG' THEN SUM(B.Kg+B.KgPrivate +B.Kg_Borrow)
        //	WHEN dbo.[LayTongThoiGianExtend](A.Category_Stage,A.OrderKey)>0 THEN dbo.[LayTongThoiGianExtend](A.Category_Stage,A.OrderKey) 
        //	WHEN dbo.[LayTongThoiGianExtend](A.Category_Stage,A.OrderKey)<=0 THEN SUM(B.Time+B.TimePrivate +B.Time_Borrow) 
        //END AS SoThoiGian,
        //SUM(B.Basket +BasketPrivate +B.Basket_Borrow) AS SoRo,
        //dbo.FTR_TongSLNguyenLieuKhiXongCongDoan (A.Category_Stage,@FromDate,@ToDate)AS SLNguyenLieu,
        //SUM(B.Kg+KgPrivate +B.Kg_Borrow) AS SLThanhPham,
        //0 AS NSNguyenLieu,
        //0 AS NSThanhPham
        //FROM FTR_Order A
        //LEFT JOIN FTR_Order_Money B ON A.OrderKey = B.OrderKey
        //WHERE A.OrderDate BETWEEN @FromDate AND @ToDate
        //AND B.RecordStatus <> 99
        //AND A.RecordStatus <> 99 
        //@CustomParamater 
        //GROUP BY A.OrderKey,A.Category_Stage ORDER BY NhomCongViec, TenCongViec

        //INSERT INTO @REPORT41_MASTER
        //(OrderKey,Category_Stage,NhomCongViec,TenCongViec,MaCongViec,SoCongNhan,
        //SoThoiGian,SoRo,SLNguyenLieu,SLThanhPham,NSNguyenLieu,NSThanhPham)
        //SELECT 
        //A.OrderKey,
        //A.Category_Stage,
        //dbo.LayNhomCongViec(A.Category_Stage) AS NhomCongViec, 
        //dbo.LayTenCongViec(A.Category_Stage) AS TenCongViec,
        //dbo.LayMaCongViec(A.Category_Stage) AS MaCongViec,
        //0,
        //SUM(B.TimeProduct) AS SoThoiGian,
        //SUM(B.BasketProduct) AS SoRo,
        //0,
        //SUM(B.KgProduct) AS SLThanhPham,
        //0 AS NSNguyenLieu,
        //0 AS NSThanhPham
        //FROM FTR_Order A
        //LEFT JOIN FTR_Order_Adjusted B ON CAST(A.OrderKey as nvarchar(50)) = B.OrderKey
        //WHERE A.OrderDate BETWEEN @FromDate AND @ToDate
        //AND B.RecordStatus <> 99
        //AND A.RecordStatus <> 99 
        //@CustomParamater 
        //GROUP BY A.OrderKey,A.Category_Stage ORDER BY NhomCongViec, TenCongViec
        //--select * from @REPORT41_MASTER

        //SELECT Category_Stage,NhomCongViec,TenCongViec,MaCongViec,
        //dbo.FTR_SoLuongCongNhanThamGiaCongDoan(Category_Stage,@FromDate,@ToDate) AS SoCongNhan
        //,SUM(SoThoiGian) AS SoThoiGian,SUM(SoRo) AS SoRo,
        //SUM(DISTINCT SLNguyenLieu) AS SLNguyenLieu,SUM(DISTINCT SLThanhPham) AS SLThanhPham,
        //CASE 
        //WHEN SUM(SoThoiGian)>0 THEN SUM(DISTINCT SLNguyenLieu)/SUM(SoThoiGian) 
        //WHEN SUM(SoThoiGian)<=0 THEN 0
        //END AS NSNguyenLieu,
        //CASE 
        //WHEN SUM(SoThoiGian)>0 THEN SUM(DISTINCT SLThanhPham) /SUM(SoThoiGian)
        //WHEN SUM(SoThoiGian)<=0 THEN 0
        //END  AS NSThanhPham
        //FROM @REPORT41_MASTER
        //GROUP BY Category_Stage,NhomCongViec,TenCongViec,MaCongViec
        //ORDER BY NhomCongViec,Category_Stage";


        //            if (NhomViec != "0")
        //            {
        //                zSQL = zSQL.Replace("@CustomParamater", " AND dbo.LayNhomCongViec(A.Category_Stage) = @NhomViec");
        //            }
        //            else
        //            {
        //                zSQL = zSQL.Replace("@CustomParamater", "");
        //            }
        //            DataTable zTable = new DataTable();
        //            string zConnectionString = ConnectDataBase.ConnectionString;
        //            try
        //            {
        //                SqlConnection zConnect = new SqlConnection(zConnectionString);
        //                zConnect.Open();
        //                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
        //                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
        //                zCommand.Parameters.Add("@NhomViec", SqlDbType.NVarChar).Value = NhomViec;
        //                zCommand.CommandTimeout = 350;
        //                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
        //                zAdapter.Fill(zTable);
        //                zCommand.Dispose();
        //                zConnect.Close();
        //            }
        //            catch (Exception ex)
        //            {
        //                string zstrMessage = ex.ToString();
        //            }
        //            return zTable;
        //        }
        #endregion
        public static DataTable Report_41_Master_V4(DateTime FromDate, DateTime ToDate, string NhomViec)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            string zSQL = @"
--declare @FromDate datetime ='2020-06-01 00:00:00'
--declare @ToDate datetime ='2020-06-10 23:59:59'
--declare @NhomViec nvarchar(50)='XOAI'

--Thống kê tất cả đơn hàng
CREATE TABLE #MASTER
(
OrderKey NVARCHAR(50),
Category_Stage INT ,
NhomCongViec NVARCHAR(50),
SoThoiGian FLOAT,
SoRo FLOAT,
SoLuong FLOAT,
SoGioPhanBo FLOAT
)
INSERT INTO #MASTER
		SELECT 
		A.OrderKey,
		A.Category_Stage,
		dbo.LayNhomCongViec(A.Category_Stage) AS NhomCongViec, 
		SUM(B.Time+B.TimePrivate +B.Time_Borrow),
		SUM(B.Basket +BasketPrivate +B.Basket_Borrow),
		SUM(B.Kg+KgPrivate +B.Kg_Borrow),
		dbo.[LayTongThoiGianExtend](A.Category_Stage,A.OrderKey)
		FROM FTR_Order A
		LEFT JOIN FTR_Order_Money B ON A.OrderKey = B.OrderKey
		WHERE A.OrderDate BETWEEN @FromDate AND @ToDate
		AND B.RecordStatus <> 99
		AND A.RecordStatus <> 99 
		@CustomParamater
		GROUP BY A.OrderKey,A.Category_Stage 

INSERT INTO #MASTER
		SELECT 
		A.OrderKey,
		A.Category_Stage,
		dbo.LayNhomCongViec(A.Category_Stage),
		SUM(B.TimeProduct) ,
		SUM(B.BasketProduct) ,
		SUM(B.KgProduct) ,
		0 
		FROM FTR_Order A
		LEFT JOIN FTR_Order_Adjusted B ON CAST(A.OrderKey as nvarchar(50)) = B.OrderKey
		WHERE A.OrderDate BETWEEN @FromDate AND @ToDate
		AND B.RecordStatus <> 99
		AND A.RecordStatus <> 99 
		 @CustomParamater 
		GROUP BY A.OrderKey,A.Category_Stage 

--Cộng đơn hàng và chia lại
CREATE TABLE #MASTER2
(
Category_Stage INT ,
NhomCongViec NVARCHAR(50),
SoThoiGian FLOAT,
SoRo FLOAT,
SoLuong FLOAT,

)
INSERT INTO #MASTER2
		SELECT Category_Stage,NhomCongViec,
			CASE WHEN NhomCongViec='KHAC' THEN SoLuong
				 WHEN NhomCongViec='CVTG' THEN SoLuong
				 WHEN SoGioPhanBo > 0 THEN SoGioPhanBo
				 ELSE SoThoiGian
			END  AS SoThoiGian,
			SoRo,
			SoLuong
		FROM(
				SELECT Category_Stage,NhomCongViec,
				SUM(SoThoiGian) AS SoThoiGian,
				SUM(SoRo) AS SoRo,
				SUM(SoLuong) AS SoLuong,
				SUM(SoGioPhanBo) AS SoGioPhanBo
				FROM #MASTER
				GROUP BY Category_Stage,NhomCongViec
			)X
-- Chia năng suất  trung bình
CREATE TABLE #MASTER3
(
Category_Stage INT ,
NhomCongViec NVARCHAR(50),
SoCongNhan int,
SoThoiGian FLOAT,
SoRo FLOAT,
SoLuong FLOAT,
SoNguyenLieu FLOAT
)
INSERT INTO #MASTER3
		SELECT 
		Category_Stage,
		NhomCongViec,
		dbo.FTR_SoLuongCongNhanThamGiaCongDoan(Category_Stage,@FromDate,@ToDate),
		SoThoiGian,
		SoRo,
		SoLuong,--Tổng số thành phẩm
		dbo.FTR_TongSLNguyenLieuKhiXongCongDoan (Category_Stage,@FromDate,@ToDate)--Tổng số nguyên liệu
		FROM #MASTER2
--Trả kết quả
SELECT 
Category_Stage,
NhomCongViec,
dbo.LayTenCongViec(Category_Stage) AS TenCongViec,
dbo.LayMaCongViec(Category_Stage) AS MaCongViec,
SoCongNhan,
SoThoiGian,
SoRo,
SoNguyenLieu,	--Nguyên liệu
SoLuong,		--Thành phẩm
CASE WHEN SoThoiGian>0 THEN SoNguyenLieu/SoThoiGian
	 ELSE 0
END AS NSNguyenLieu,
CASE WHEN SoThoiGian>0 THEN SoLuong/SoThoiGian
	 ELSE 0
END AS NSThanhPham
FROM #MASTER3
ORDER BY [dbo].[Fn_LaySoSapXep_NhomCongViec](Category_Stage)

DROP TABLE #MASTER
DROP TABLE #MASTER2
DROP TABLE #MASTER3
";


            if (NhomViec != "0")
            {
                zSQL = zSQL.Replace("@CustomParamater", " AND dbo.LayNhomCongViec(A.Category_Stage) = @NhomViec");
            }
            else
            {
                zSQL = zSQL.Replace("@CustomParamater", "");
            }
            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@NhomViec", SqlDbType.NVarChar).Value = NhomViec;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable Report_41_Detail_V3(DateTime FromDate, DateTime ToDate, int CongViec, float SoGio, float SoRo, float NguyenLieu, float ThanhPham)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            string zSQL = @"
DECLARE @REPORT41_DETAIL AS dbo.REPORT41_DETAIL
INSERT INTO @REPORT41_DETAIL
(OrderKey,EmployeeKey,EmployeeName,EmployeeID,TeamID,SoThoiGian,SoRo,SoThanhPham,Style,TongNguyenLieu,TongThanhPham,TongRo,TongGio)
SELECT 
A.OrderKey,A.EmployeeKey,dbo.Fn_GetFullName(A.EmployeeKey) AS EmployeeName, A.EmployeeID,
[dbo].[Fn_GetTeamID](C.TeamKey)AS TeamID,
CASE
	WHEN dbo.LayNhomCongViec(B.Category_Stage) = 'KHAC' THEN (A.Kg+A.KgPrivate +A.Kg_Borrow)
	WHEN dbo.LayNhomCongViec(B.Category_Stage) = 'CVTG' THEN (A.Kg+A.KgPrivate +A.Kg_Borrow)
	WHEN dbo.[LayThoiGianExtend_41](A.EmployeeKey,A.StageKey,A.OrderKey) >0 THEN dbo.[LayThoiGianExtend_41](A.EmployeeKey,A.StageKey,A.OrderKey)
	ELSE (A.Time + A.TimePrivate + A.Time_Borrow)
END AS SoThoiGian, 
(A.Basket+A.BasketPrivate +A.Basket_Borrow) AS SoRo,
(A.Kg+A.KgPrivate + A.Kg_Borrow) AS SoThanhPham,
0, -- nhóm chính
@TongNguyenLieu,
@TongThanhPham,
@TongSoRo,
@TongSoGio
FROM 
FTR_Order_Money A 
LEFT JOIN FTR_Order B ON A.OrderKey = B.OrderKey
LEFT JOIN [dbo].[HRM_Employee] C ON C.EmployeeKey=A.EmployeeKey
WHERE 
A.OrderDate BETWEEN @FromDate AND @ToDate
AND B.RecordStatus <> 99
AND A.RecordStatus <> 99
AND B.Category_Stage = @CongViec 
AND C.Slug !=1 --Trừ các con ma 
AND A.Category_Eat !=1 --trừ nhũng người ăn chung
ORDER BY TeamID, LEN(A.EmployeeID), A.EmployeeID

INSERT INTO @REPORT41_DETAIL
(OrderKey,EmployeeKey,EmployeeName,EmployeeID,TeamID,SoThoiGian,SoRo,SoThanhPham,Style,TongNguyenLieu,TongThanhPham,TongRo,TongGio)
SELECT 
A.OrderKey,A.EmployeeKey,dbo.Fn_GetFullName(A.EmployeeKey) AS EmployeeName, 
A.EmployeeID,
[dbo].[Fn_GetTeamID](C.TeamKey)AS TeamID,
A.TimeProduct AS SoThoiGian,
A.BasketProduct AS SoRo,
A.KgProduct AS SoThanhPham,
1 ,-- nhóm chia lại
@TongNguyenLieu,
@TongThanhPham,
@TongSoRo,
@TongSoGio
FROM [dbo].[FTR_Order_Adjusted] A 
LEFT JOIN FTR_Order B ON A.OrderKey = CAST(B.OrderKey AS NVARCHAR(50))
LEFT JOIN [dbo].[HRM_Employee] C ON C.EmployeeKey=A.EmployeeKey
WHERE 
A.OrderDate BETWEEN @FromDate AND @ToDate
AND B.RecordStatus <> 99
AND A.RecordStatus <> 99
AND B.Category_Stage = @CongViec
AND A.Share =0 AND A.KgProduct >0
ORDER BY TeamID, LEN(A.EmployeeID), A.EmployeeID
--select * from @REPORT41_DETAIL 

SELECT EmployeeName,EmployeeID,TeamID,SUM(SoThoiGian)AS SoThoiGian,SUM(SoRo) AS SoRo,
Sum(dbo.FTR_TongSLNguyenLieu_CongDoan_CongNhan(SoRo,SoThoiGian,TongNguyenLieu, TongRo,TongGio)) AS SLNguyenLieu,
Sum(SoThanhPham) AS SLThanhPham,
CASE
WHEN SUM(SoThoiGian)  >0 THEN Sum([dbo].[FTR_TongSLNguyenLieu_CongDoan_CongNhan](SoRo,SoThoiGian,TongNguyenLieu, TongRo,TongGio)) /SUM(SoThoiGian) 
ELSE 0
END AS NSNguyenLieu,
CASE 
WHEN SUM(SoThoiGian)  >0 THEN SUM(SoThanhPham)/SUM(SoThoiGian)
ELSE 0
 END AS NSThanhPham,
Style
FROM @REPORT41_DETAIL 
GROUP BY EmployeeName,EmployeeID,TeamID,Style
ORDER BY Style,TeamID, LEN(EmployeeID),EmployeeID
";

            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@CongViec", SqlDbType.Int).Value = CongViec;
                zCommand.Parameters.Add("@TongNguyenLieu", SqlDbType.Float).Value = NguyenLieu;
                zCommand.Parameters.Add("@TongThanhPham", SqlDbType.Float).Value = ThanhPham;
                zCommand.Parameters.Add("@TongSoGio", SqlDbType.Float).Value = SoGio;
                zCommand.Parameters.Add("@TongSoRo", SqlDbType.Float).Value = SoRo;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        #endregion

        #region[42_V2]
        #region[Khong sd]
        //        public static DataTable Report_42_V3(DateTime FromDate, DateTime ToDate, string MaCongNhan)
        //        {
        //            #region [SQL]
        //            string zSQL = @"
        //DECLARE @REPORT42 AS dbo.REPORT42 
        //DECLARE @REPORT42BUA AS dbo.REPORT42BUA

        //-----SELECT THÔNG TIN BÁO CÁO THEO SỐ LIỆU BAN ĐẦU
        //;WITH X1 AS 
        //(
        //	SELECT
        //	A.OrderDate, A.TeamKey, A.EmployeeName, A.EmployeeID, A.StageKey,
        //	C.ProductKey,C.ProductName, 
        //    dbo.[LayNhomCongViec](A.StageKey) NhomCongViec,
        //	dbo.LayTenCongViec(A.StageKey) TenCongViec,
        //	dbo.LayGiaCongViec(A.StageKey) GiaCongViec,
        //	SUM(A.[Money]) + SUM(A.MoneyPrivate) AS 'LK', 0 'TG', 0 'SR', SUM(A.Kg) + SUM(A.KgPrivate) AS 'TP'
        //	FROM FTR_Order_Money A 
        //	LEFT JOIN FTR_Order C ON A.OrderKey = C.OrderKey
        //	WHERE 
        //	A.RecordStatus <> 99
        //	AND C.RecordStatus <> 99
        //	AND A.OrderDate BETWEEN @FromDate AND @ToDate
        //    @CustomParamater
        //	GROUP BY A.OrderDate, A.TeamKey, A.EmployeeName, A.EmployeeID,C.ProductKey, C.ProductName, A.StageKey
        //UNION ALL
        //	SELECT
        //	A.OrderDate, A.TeamKey, A.EmployeeName, A.EmployeeID, A.StageKey,
        //	C.ProductKey,C.ProductName,
        //    dbo.[LayNhomCongViec](A.StageKey) NhomCongViec,
        //	dbo.LayTenCongViec(A.StageKey) TenCongViec,
        //	dbo.LayGiaCongViec(A.StageKey) GiaCongViec,
        //	SUM(A.MoneyPersonal) AS 'LK', 0 'TG', 0 'SR', SUM(A.KgProduct) AS 'TP'
        //	FROM FTR_Order_Adjusted A 
        //	LEFT JOIN FTR_Order C ON A.OrderKey = C.OrderKey
        //	WHERE
        //	A.RecordStatus <> 99
        //	AND C.RecordStatus <> 99
        //	AND A.OrderDate BETWEEN @FromDate AND @ToDate AND A.Share !=1
        //    @CustomParamater
        //	GROUP BY A.OrderDate, A.TeamKey, A.EmployeeName, A.EmployeeID,C.ProductKey, C.ProductName, A.StageKey
        //)
        //INSERT INTO @REPORT42 (OrderDate, TeamKey, TeamName, EmployeeName, EmployeeID,ProductKey, ProductName,WorkGroup, WorkKey, WorkName, WorkPrice, LK, TG, SR, TP, NS)
        //SELECT OrderDate, TeamKey, dbo.Fn_GetTeamName(TeamKey) AS TeamName, EmployeeName, EmployeeID,ProductKey, ProductName,NhomCongViec, StageKey, TenCongViec, GiaCongViec, LK, TG, SR, TP, 0
        //FROM X1
        //;WITH X2 AS 
        //(
        //	SELECT
        //	A.OrderDate, B.TeamKey, dbo.Fn_GetFullName(B.EmployeeKey) AS EmployeeName, A.EmployeeID, C.Category_Stage AS StageKey,
        //	C.ProductKey,C.ProductName,
        //    dbo.[LayNhomCongViec](C.Category_Stage) NhomCongViec,
        //	dbo.LayTenCongViec(C.Category_Stage) TenCongViec,
        //	dbo.LayGiaCongViec(C.Category_Stage) GiaCongViec,
        //	0 'LK', SUM(A.[Time]) AS 'TG',	 SUM(A.Basket) AS 'SR', 0 'TP'
        //	FROM FTR_Order_Employee A
        //	LEFT JOIN HRM_Employee B ON A.EmployeeKey = B.EmployeeKey
        //	LEFT JOIN FTR_Order C ON A.OrderKey = C.OrderKey
        //	WHERE
        //	C.RecordStatus <> 99
        //	AND B.RecordStatus <> 99
        //	AND A.RecordStatus <> 99
        //	AND A.OrderDate BETWEEN @FromDate AND @ToDate
        //    @CustomParamater
        //	GROUP BY A.OrderDate, B.TeamKey, B.EmployeeKey, A.EmployeeID,C.ProductKey, C.ProductName, C.Category_Stage, A.[Time]
        //UNION ALL
        //	SELECT
        //	A.OrderDate, A.TeamKey, A.EmployeeName, A.EmployeeID, A.StageKey,
        //	C.ProductKey,C.ProductName,
        //    dbo.[LayNhomCongViec](A.StageKey) NhomCongViec,
        //	dbo.LayTenCongViec(A.StageKey) TenCongViec,
        //	dbo.LayGiaCongViec(A.StageKey) GiaCongViec,
        //	0 'LK', SUM(A.TimeProduct) AS 'TG', SUM(A.Basketproduct) AS 'SR', 0 'TP'
        //	FROM FTR_Order_Adjusted A 
        //	LEFT JOIN FTR_Order C ON A.OrderKey = C.OrderKey
        //	WHERE 
        //	A.RecordStatus <> 99
        //	AND C.RecordStatus <> 99
        //	AND A.OrderDate BETWEEN @FromDate AND @ToDate AND A.Share !=1
        //    @CustomParamater
        //	GROUP BY A.OrderDate, A.TeamKey, A.EmployeeName, A.EmployeeID,C.ProductKey, C.ProductName, A.StageKey	
        //)
        //INSERT INTO @REPORT42 (OrderDate, TeamKey, TeamName, EmployeeName, EmployeeID,ProductKey, ProductName,WorkGroup, WorkKey, WorkName, WorkPrice, LK, TG, SR, TP, NS)
        //SELECT OrderDate, TeamKey, dbo.Fn_GetTeamName(TeamKey), EmployeeName, EmployeeID,ProductKey, ProductName, NhomCongViec, StageKey, TenCongViec, GiaCongViec, LK, TG, SR, TP, 0
        //FROM X2

        //-----TẠO BẢNG BÁO CÁO CẦN LÀM PHÉP BÙA LẠI THỜI GIAN LÀM VIỆC
        //INSERT INTO @REPORT42BUA 
        //(OrderDate, TeamKey, TeamName, EmployeeName, EmployeeID, ProductKey, ProductName, WorkGroup, WorkKey, WorkName, WorkPrice, LK, TG, SR, TP, NS)
        //SELECT 
        //Z.OrderDate, Z.TeamKey, Z.TeamName, Z.EmployeeName, Z.EmployeeID,Z.ProductKey, Z.ProductName, Z.WorkGroup, Z.WorkKey, Z.WorkName, Z.WorkPrice, 
        //SUM(LK) AS LK, 
        //SUM(TG) AS TG,
        //SUM(SR) AS SR, 
        //SUM(TP) AS TP,
        //SUM(NS) AS NS
        //FROM @REPORT42 Z
        //GROUP BY Z.OrderDate, Z.TeamKey, Z.TeamName, Z.EmployeeName, Z.EmployeeID,Z.ProductKey, Z.ProductName,Z.WorkGroup, Z.WorkKey, Z.WorkName, Z.WorkPrice

        //-----SELECT LẠI KẾT QUẢ SAU KHI BÙA LẠI THỜI GIAN.
        //SELECT 
        //CONVERT(NVARCHAR(50), B.OrderDate) + '|' + B.EmployeeName + '|' + B.EmployeeID AS 'ORDER', 
        //B.ProductName + '|' + B.WorkName + '|' + CONVERT(NVARCHAR(50), B.WorkPrice) AS 'WORK', 
        //dbo.TuyetChieu_TinhThoiGian(@REPORT42BUA, EmployeeID, WorkKey, TG, TP, B.OrderDate) AS 'TGBua', LK, SR, TP, TG,
        //dbo.TyLeNangSuat(TP, dbo.TuyetChieu_TinhThoiGian(@REPORT42BUA, EmployeeID, WorkKey, TG, TP,B.OrderDate)) AS NS
        //FROM @REPORT42BUA B
        //";
        //            #endregion

        //            if (MaCongNhan != string.Empty)
        //            {
        //                zSQL = zSQL.Replace("@CustomParamater", " AND A.EmployeeID IN (" + MaCongNhan + ")");
        //            }

        //            DataTable zTable = new DataTable();
        //            string zConnectionString = ConnectDataBase.ConnectionString;
        //            try
        //            {
        //                SqlConnection zConnect = new SqlConnection(zConnectionString);
        //                zConnect.Open();
        //                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
        //                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
        //                zCommand.CommandTimeout = 350;
        //                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
        //                zAdapter.Fill(zTable);
        //                zCommand.Dispose();
        //                zConnect.Close();
        //            }
        //            catch (Exception ex)
        //            {
        //                string zstrMessage = ex.ToString();
        //            }
        //            return zTable;
        //        }
        //        public static DataTable Report_42_V2(DateTime FromDate, DateTime ToDate, string MaCongNhan)
        //        {
        //            #region [SQL]
        //            string zSQL = @"
        //DECLARE @REPORT42 AS dbo.REPORT42 
        //DECLARE @REPORT42BUA AS dbo.REPORT42BUA

        //-----SELECT THÔNG TIN BÁO CÁO THEO SỐ LIỆU BAN ĐẦU
        //;WITH X1 AS 
        //(
        //	SELECT
        //	A.OrderDate, A.TeamKey, A.EmployeeName, A.EmployeeID, A.StageKey,
        //	C.ProductKey,C.ProductName, 
        //    dbo.[LayNhomCongViec](A.StageKey) NhomCongViec,
        //	dbo.LayTenCongViec(A.StageKey) TenCongViec,
        //	dbo.LayGiaCongViec(A.StageKey) GiaCongViec,
        //	SUM(A.[Money]) + SUM(A.MoneyPrivate) AS 'LK', 0 'TG', 0 'SR', SUM(A.Kg) + SUM(A.KgPrivate) AS 'TP'
        //	FROM FTR_Order_Money A 
        //	LEFT JOIN FTR_Order C ON A.OrderKey = C.OrderKey
        //	WHERE 
        //	A.RecordStatus <> 99
        //	AND C.RecordStatus <> 99
        //	AND A.OrderDate BETWEEN @FromDate AND @ToDate
        //    @CustomParamater
        //	GROUP BY A.OrderDate, A.TeamKey, A.EmployeeName, A.EmployeeID,C.ProductKey, C.ProductName, A.StageKey
        //UNION ALL
        //	SELECT
        //	A.OrderDate, A.TeamKey, A.EmployeeName, A.EmployeeID, A.StageKey,
        //	C.ProductKey,C.ProductName,
        //    dbo.[LayNhomCongViec](A.StageKey) NhomCongViec,
        //	dbo.LayTenCongViec(A.StageKey) TenCongViec,
        //	dbo.LayGiaCongViec(A.StageKey) GiaCongViec,
        //	SUM(A.MoneyPersonal) AS 'LK', 0 'TG', 0 'SR', SUM(A.KgProduct) AS 'TP'
        //	FROM FTR_Order_Adjusted A 
        //	LEFT JOIN FTR_Order C ON A.OrderKey = C.OrderKey
        //	WHERE
        //	A.RecordStatus <> 99
        //	AND C.RecordStatus <> 99
        //	AND A.OrderDate BETWEEN @FromDate AND @ToDate AND A.Share !=1
        //    @CustomParamater
        //	GROUP BY A.OrderDate, A.TeamKey, A.EmployeeName, A.EmployeeID,C.ProductKey, C.ProductName, A.StageKey
        //)
        //INSERT INTO @REPORT42 (OrderDate, TeamKey, TeamName, EmployeeName, EmployeeID,ProductKey, ProductName,WorkGroup, WorkKey, WorkName, WorkPrice, LK, TG, SR, TP, NS)
        //SELECT OrderDate, TeamKey, dbo.Fn_GetTeamName(TeamKey) AS TeamName, EmployeeName, EmployeeID,ProductKey, ProductName,NhomCongViec, StageKey, TenCongViec, GiaCongViec, LK, TG, SR, TP, 0
        //FROM X1
        //;WITH X2 AS 
        //(
        //	SELECT
        //	A.OrderDate, B.TeamKey, dbo.Fn_GetFullName(B.EmployeeKey) AS EmployeeName, A.EmployeeID, C.Category_Stage AS StageKey,
        //	C.ProductKey,C.ProductName,
        //    dbo.[LayNhomCongViec](C.Category_Stage) NhomCongViec,
        //	dbo.LayTenCongViec(C.Category_Stage) TenCongViec,
        //	dbo.LayGiaCongViec(C.Category_Stage) GiaCongViec,
        //	0 'LK', SUM(A.[Time]) AS 'TG',	 SUM(A.Basket) AS 'SR', 0 'TP'
        //	FROM FTR_Order_Employee A
        //	LEFT JOIN HRM_Employee B ON A.EmployeeKey = B.EmployeeKey
        //	LEFT JOIN FTR_Order C ON A.OrderKey = C.OrderKey
        //	WHERE
        //	C.RecordStatus <> 99
        //	AND B.RecordStatus <> 99
        //	AND A.RecordStatus <> 99
        //	AND A.OrderDate BETWEEN @FromDate AND @ToDate
        //    @CustomParamater
        //	GROUP BY A.OrderDate, B.TeamKey, B.EmployeeKey, A.EmployeeID,C.ProductKey, C.ProductName, C.Category_Stage, A.[Time]
        //UNION ALL
        //	SELECT
        //	A.OrderDate, A.TeamKey, A.EmployeeName, A.EmployeeID, A.StageKey,
        //	C.ProductKey,C.ProductName,
        //    dbo.[LayNhomCongViec](A.StageKey) NhomCongViec,
        //	dbo.LayTenCongViec(A.StageKey) TenCongViec,
        //	dbo.LayGiaCongViec(A.StageKey) GiaCongViec,
        //	0 'LK', SUM(A.TimeProduct) AS 'TG', SUM(A.Basketproduct) AS 'SR', 0 'TP'
        //	FROM FTR_Order_Adjusted A 
        //	LEFT JOIN FTR_Order C ON A.OrderKey = C.OrderKey
        //	WHERE 
        //	A.RecordStatus <> 99
        //	AND C.RecordStatus <> 99
        //	AND A.OrderDate BETWEEN @FromDate AND @ToDate AND A.Share !=1
        //    @CustomParamater
        //	GROUP BY A.OrderDate, A.TeamKey, A.EmployeeName, A.EmployeeID,C.ProductKey, C.ProductName, A.StageKey	
        //)
        //INSERT INTO @REPORT42 (OrderDate, TeamKey, TeamName, EmployeeName, EmployeeID,ProductKey, ProductName,WorkGroup, WorkKey, WorkName, WorkPrice, LK, TG, SR, TP, NS)
        //SELECT OrderDate, TeamKey, dbo.Fn_GetTeamName(TeamKey), EmployeeName, EmployeeID,ProductKey, ProductName, NhomCongViec, StageKey, TenCongViec, GiaCongViec, LK, TG, SR, TP, 0
        //FROM X2

        //-----TẠO BẢNG BÁO CÁO CẦN LÀM PHÉP BÙA LẠI THỜI GIAN LÀM VIỆC
        //INSERT INTO @REPORT42BUA 
        //(OrderDate, TeamKey, TeamName, EmployeeName, EmployeeID, ProductKey, ProductName, WorkGroup, WorkKey, WorkName, WorkPrice,CheckType, LK, TG, SR, TP, NS)
        //SELECT 
        //Z.OrderDate, Z.TeamKey, Z.TeamName, Z.EmployeeName, Z.EmployeeID,Z.ProductKey, Z.ProductName, Z.WorkGroup, Z.WorkKey, Z.WorkName, Z.WorkPrice, 
        //dbo.[LayGiaTriBuaThoiGianLaiKhong](Z.WorkKey),  
        //SUM(LK) AS LK, 
        //SUM(TG) AS TG,
        //SUM(SR) AS SR, 
        //SUM(TP) AS TP,
        //SUM(NS) AS NS
        //FROM @REPORT42 Z
        //GROUP BY Z.OrderDate, Z.TeamKey, Z.TeamName, Z.EmployeeName, Z.EmployeeID,Z.ProductKey, Z.ProductName,Z.WorkGroup, Z.WorkKey, Z.WorkName, Z.WorkPrice

        //-----SELECT LẠI KẾT QUẢ SAU KHI BÙA LẠI THỜI GIAN.
        //SELECT 
        //CONVERT(NVARCHAR(50), B.OrderDate) + '|' + B.EmployeeName + '|' + B.EmployeeID AS 'ORDER', 
        //B.ProductName + '|' + B.WorkName + '|' + CONVERT(NVARCHAR(50), B.WorkPrice) AS 'WORK', 
        //dbo.TuyetChieu_TinhThoiGian(@REPORT42BUA, EmployeeID,ProductKey, WorkKey, TG, TP, B.OrderDate, B.CheckType) AS 'TGBua', LK, SR, TP, TG,
        //dbo.TyLeNangSuat(TP, dbo.TuyetChieu_TinhThoiGian(@REPORT42BUA, EmployeeID,ProductKey, WorkKey, TG, TP,B.OrderDate, B.CheckType)) AS NS
        //FROM @REPORT42BUA B
        //";
        //            #endregion

        //            if (MaCongNhan != string.Empty)
        //            {
        //                zSQL = zSQL.Replace("@CustomParamater", " AND A.EmployeeID IN (" + MaCongNhan + ")");
        //            }

        //            DataTable zTable = new DataTable();
        //            string zConnectionString = ConnectDataBase.ConnectionString;
        //            try
        //            {
        //                SqlConnection zConnect = new SqlConnection(zConnectionString);
        //                zConnect.Open();
        //                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
        //                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
        //                zCommand.CommandTimeout = 350;
        //                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
        //                zAdapter.Fill(zTable);
        //                zCommand.Dispose();
        //                zConnect.Close();
        //            }
        //            catch (Exception ex)
        //            {
        //                string zstrMessage = ex.ToString();
        //            }
        //            return zTable;
        //        }
        #endregion
        public static DataTable Report_42_V4(DateTime FromDate, DateTime ToDate, string MaCongNhan)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            #region [SQL]
            string zSQL = @"
DECLARE @REPORT42BUA AS dbo.REPORT42BUA
DECLARE @REPORT42 AS dbo.REPORT42
-----SELECT THÔNG TIN BÁO CÁO THEO SỐ LIỆU BAN ĐẦU
;WITH X1 AS 
(
	SELECT
	A.OrderDate, A.TeamKey, dbo.Fn_GetTeamName(A.TeamKey) AS TeamName,A.EmployeeName,A.EmployeeID,
	C.ProductKey,C.ProductName, 
    dbo.LayNhomCongViec(A.StageKey) NhomCongViec,
	A.StageKey,	D.StageName, D.Price,
	SUM(A.[Money]) + SUM(A.MoneyPrivate) + SUM(A.Money_Borrow) AS 'LK',
	SUM(A.[Time]) + SUM(A.TimePrivate) + SUM(A.Time_Borrow) AS 'TG',
	SUM(A.Basket) + SUM(A.BasketPrivate) +SUM(A.Basket_Borrow) AS 'SR', 
	SUM(A.Kg) + SUM(A.KgPrivate) + SUM(A.Kg_Borrow) AS 'TP',
	0 'Table'
	FROM FTR_Order_Money A 
	LEFT JOIN FTR_Order C ON A.OrderKey = C.OrderKey
	LEFT JOIN IVT_Product_Stages D ON D.StageKey = A.StageKey
	WHERE 
	A.RecordStatus <> 99
	AND C.RecordStatus <> 99
	AND A.OrderDate BETWEEN @FromDate AND @ToDate
	@CustomParamater
    --AND A.EmployeeID IN ('A11')
	GROUP BY A.OrderDate, A.TeamKey, A.EmployeeName, A.EmployeeID,
	C.ProductKey, C.ProductName, A.StageKey, D.StageName, D.Price

UNION ALL

	SELECT
	A.OrderDate, A.TeamKey, dbo.Fn_GetTeamName(A.TeamKey) AS TeamName, A.EmployeeName, A.EmployeeID,
	C.ProductKey,C.ProductName,
    dbo.[LayNhomCongViec](A.StageKey) NhomCongViec,	
	A.StageKey,D.StageName, D.Price,
	SUM(A.MoneyPersonal) AS 'LK',
	SUM(A.TimeProduct) AS 'TG',
	SUM(A.BasketProduct) AS 'SR',
	SUM(A.KgProduct) AS 'TP',
	1 'Table'
	FROM FTR_Order_Adjusted A 
	LEFT JOIN FTR_Order C ON A.OrderKey = C.OrderKey
	LEFT JOIN IVT_Product_Stages D ON D.StageKey = A.StageKey
	WHERE
	A.RecordStatus <> 99
	AND C.RecordStatus <> 99
	AND A.OrderDate BETWEEN @FromDate AND @ToDate AND A.Share !=1
	@CustomParamater
    --AND A.EmployeeID IN ('A11')
	GROUP BY A.OrderDate, A.TeamKey, A.EmployeeName, A.EmployeeID,
	C.ProductKey, C.ProductName, A.StageKey, D.StageName, D.Price
)
INSERT INTO  @REPORT42
(OrderDate, TeamKey, TeamName, EmployeeName, EmployeeID, ProductKey, ProductName,
WorkGroup, WorkKey, WorkName, WorkPrice, LK, TG, SR, TP, [Table],TGCHIA,TGShow)
SELECT OrderDate, TeamKey, TeamName, EmployeeName, EmployeeID, ProductKey, ProductName,
NhomCongViec, StageKey, StageName, Price, LK, TG, SR, TP, [Table],
dbo.LayThoiGianShareExtend(EmployeeID, @FromDate, @ToDate, StageKey) TGCHIA,
CASE 
	WHEN NhomCongViec = 'KHAC' THEN TP
	WHEN NhomCongViec = 'CVTG' THEN TP
	WHEN dbo.LayThoiGianExtend_42(EmployeeID, OrderDate, OrderDate, StageKey) > 0 THEN dbo.LayThoiGianExtend_42(EmployeeID, OrderDate, OrderDate, StageKey)
	WHEN dbo.LayThoiGianExtend_42(EmployeeID, OrderDate, OrderDate, StageKey) <= 0 THEN TG
END + dbo.LayThoiGianShareExtend(EmployeeID, OrderDate, OrderDate, StageKey)  AS TGShow
FROM X1 
ORDER BY EmployeeID

INSERT INTO  @REPORT42BUA 
(OrderDate, TeamKey, TeamName, EmployeeName, EmployeeID, ProductKey, ProductName,
WorkGroup, WorkKey, WorkName, WorkPrice, LK, TG, SR, TP,TGCHIA,TGShow,NS)
SELECT OrderDate, TeamKey, TeamName, EmployeeName, EmployeeID, ProductKey, ProductName,
WorkGroup, WorkKey, WorkName, WorkPrice, SUM(LK), SUM(TG), SUM(SR), SUM(TP),SUM(TGCHIA),SUM(TGShow),
CASE
 WHEN SUM(TGShow) >0 THEN SUM(TP)/SUM(TGShow)
 WHEN SUM(TGShow) <=0 THEN 0
END 

FROM @REPORT42 X2
GROUP BY X2.OrderDate, X2.TeamKey, X2.TeamName, X2.EmployeeName, X2.EmployeeID,
X2.ProductKey, X2.ProductName,X2.WorkGroup, X2.WorkKey, X2.WorkName, X2.WorkPrice
ORDER BY EmployeeID
--SELECT * FROM @REPORT42BUA
-----SELECT LẠI KẾT QUẢ SAU KHI BÙA LẠI THỜI GIAN.
SELECT 
CONVERT(NVARCHAR(50), B.OrderDate) + '|' + B.EmployeeName + '|' + B.EmployeeID AS 'ORDER', 
B.ProductName + '|' + B.WorkName + '|' + CONVERT(NVARCHAR(50), B.WorkPrice) AS 'WORK', 
ISNULL (TGShow,0) AS 'TGBua', ISNULL (LK,0) AS LK, ISNULL (SR,0) AS SR, ISNULL (TP,0) AS TP, ISNULL (TG,0) AS TG, ISNULL (NS,0) AS NS
FROM @REPORT42BUA B
";
            #endregion

            if (MaCongNhan != string.Empty)
            {
                zSQL = zSQL.Replace("@CustomParamater", " AND A.EmployeeID IN (" + MaCongNhan + ")");
            }
            else
            {
                zSQL = zSQL.Replace("@CustomParamater", "");
            }
            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        #endregion
        #region[Report45]
        public static DataTable ListStageOfGroup(int GroupKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.StageKey,B.GroupName,A.StageName,A.StagesID,A.Price FROM [dbo].[IVT_Product_Stages] A 
LEFT JOIN [dbo].[IVT_Product_Stages_Group] B ON B.GroupKey=A.GroupKey
WHERE A.RecordStatus <>99
 @CustomParamater
ORDER BY B.Rank, A.StagesID";
            if (GroupKey != 0)
            {
                zSQL = zSQL.Replace("@CustomParamater", " AND A.GroupKey =@GroupKey");
            }
            else
            {
                zSQL = zSQL.Replace("@CustomParamater", "");
            }
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@GroupKey", SqlDbType.Int).Value = GroupKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable No45(DateTime FromDate, DateTime ToDate, int StageKey, double SoTienChuan, double GioHanhChanh)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT 
    [dbo].[LayTenCongViec](@StageKey) AS TENCV,
    [dbo].[LayMaCongViec](@StageKey) AS  MACV,
    OrderDate,
	[dbo].[FTR_SoLuongCongNhanThamGiaCongDoan_Rpt45](@StageKey,OrderDate) AS SLCN,
	SOGIO,
	SLTP,
	CASE 
		WHEN SOGIO >0 THEN SLTP / SOGIO 
		WHEN SOGIO <=0 THEN 0
	END AS NANGSUAT,
	CASE 
		WHEN (SLTP >0 AND @GioHanhChanh >0 )THEN @SoTienChuan / @GioHanhChanh * SOGIO / SLTP
		WHEN (SLTP <=0 AND @GioHanhChanh <=0 ) THEN 0
	END AS CHIPHI
FROM
(
SELECT OrderDate,
	SUM(
	CASE 
		WHEN [dbo].[LayTongThoiGianExtend_Rpt45](Category_Stage,OrderKey) >0 THEN [dbo].[LayTongThoiGianExtend_Rpt45](Category_Stage,OrderKey)
		wHEN [dbo].[LayTongThoiGianExtend_Rpt45](Category_Stage,OrderKey) <= 0 THEN [dbo].[FTR_TongGioDonHang_Rpt45](Category_Stage,OrderKey)
	END )AS SOGIO,
	SUM(QuantityReality) as SLTP
	FROM [dbo].[FTR_Order]
	WHERE RecordStatus <>99
	AND OrderDate BETWEEN @FromDate AND @ToDate
	AND Category_Stage= @StageKey
	GROUP BY OrderDate
)X ORDER BY OrderDate
	";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@SoTienChuan", SqlDbType.Decimal).Value = SoTienChuan;
                zCommand.Parameters.Add("@GioHanhChanh", SqlDbType.Decimal).Value = GioHanhChanh;
                zCommand.Parameters.Add("@StageKey", SqlDbType.Int).Value = StageKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.CommandTimeout = 450;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        #endregion

        #region[Report 19]
        public static DataTable ListEmployeeOfTeam(int TeamKey, DateTime FromDate, DateTime ToDate)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT EmployeeID FROM [dbo].[HRM_Employee] 
WHERE [dbo].[Fn_TeamKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate)= @TeamKey 
AND RecordStatus <> 99 
--AND StartingDate <= @FromDate   
AND (LeavingDate IS NULL OR LeavingDate >= @ToDate)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable Report_19(DateTime FromDate, DateTime ToDate, string MaCongNhan)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            #region [SQL]
            string zSQL = @"
-----SELECT THÔNG TIN BÁO CÁO THEO SỐ LIỆU BAN ĐẦU
;WITH X1 AS 
(
	SELECT
	A.OrderDate,A.EmployeeName,A.EmployeeID,A.EmployeeKey,
	C.ProductKey,C.ProductName, 
    dbo.LayNhomCongViec(A.StageKey) NhomCongViec,
	A.StageKey,	D.StageName, D.Price,
	SUM(A.[Money]) + SUM(A.MoneyPrivate) + SUM(A.Money_Borrow) AS 'LK',
	SUM(
	CASE
	WHEN dbo.[LayThoiGianExtend_Rpt19](A.EmployeeKey,A.OrderKey) >0 THEN  dbo.[LayThoiGianExtend_Rpt19](A.EmployeeKey,A.OrderKey)
	WHEN dbo.[LayThoiGianExtend_Rpt19](A.EmployeeKey,A.OrderKey) <=0 THEN A.[Time] +A.TimePrivate + A.Time_Borrow 
	END) AS 'TG',
	SUM(A.Basket) + SUM(A.BasketPrivate) +SUM(A.Basket_Borrow) AS 'SR', 
	SUM(A.Kg) + SUM(A.KgPrivate) + SUM(A.Kg_Borrow) AS 'TP',
	0 'Table'
	FROM FTR_Order_Money A 
	LEFT JOIN FTR_Order C ON A.OrderKey = C.OrderKey
	LEFT JOIN IVT_Product_Stages D ON D.StageKey = A.StageKey
	WHERE 
	A.RecordStatus <> 99
	AND C.RecordStatus <> 99
	AND A.OrderDate BETWEEN @FromDate AND @ToDate
     @CustomParamater
	--AND A.EmployeeID IN ('A3','A2')
	GROUP BY A.OrderDate,A.EmployeeName, A.EmployeeID,EmployeeKey,
	A.StageKey, D.StageName, D.Price,C.ProductKey, C.ProductName

UNION ALL

	SELECT
	 A.OrderDate,A.EmployeeName, A.EmployeeID,EmployeeKey,
	C.ProductKey,C.ProductName,
    dbo.[LayNhomCongViec](A.StageKey) NhomCongViec,	
	A.StageKey,D.StageName, D.Price,
	SUM(A.MoneyPersonal) AS 'LK',
	SUM(A.TimeProduct) AS 'TG',
	SUM(A.BasketProduct) AS 'SR',
	SUM(A.KgProduct) AS 'TP',
	1 'Table'
	FROM FTR_Order_Adjusted A 
	LEFT JOIN FTR_Order C ON A.OrderKey = C.OrderKey
	LEFT JOIN IVT_Product_Stages D ON D.StageKey = A.StageKey
	WHERE
	A.RecordStatus <> 99
	AND C.RecordStatus <> 99
	AND A.OrderDate BETWEEN @FromDate AND @ToDate AND A.Share !=1
    @CustomParamater
	--AND A.EmployeeID IN ('A3','A2')
	GROUP BY A.OrderDate, A.EmployeeName, A.EmployeeID,EmployeeKey,
	A.StageKey, D.StageName, D.Price ,C.ProductKey, C.ProductName

	UNION ALL --Nếu ngày đó có giờ tăng ca mà không có giờ công đoạn, vì công đoạn lẽ tẻ được gôm vào 1  ngày khác

	SELECT
	 ''OrderDate,A.EmployeeName, A.EmployeeID,A.EmployeeKey,
	''ProductKey,''ProductName,
    ''NhomCongViec,	
	''StageKey,''StageName, ''Price,
	0 AS 'LK',
	0 AS 'TG',
	0 AS 'SR',
	0 AS 'TP',
	1 'Table'
	FROM [dbo].[Temp_Import_Detail] A
	WHERE
	A.RecordStatus <> 99
	AND A.DateImport BETWEEN @FromDate AND @ToDate 
    @CustomParamater
	--AND A.EmployeeID IN ('A1','A533')
    GROUP BY A.EmployeeName, A.EmployeeID,A.EmployeeKey
)
--select * from X1 order by EmployeeID

-------SELECT LẠI KẾT QUẢ SAU KHI BÙA LẠI THỜI GIAN.
SELECT 
EmployeeName + '|' + EmployeeID+'|'+ CONVERT(NVARCHAR(50),[dbo].[TongThanhTienNgoaiGio](EmployeeKey,@FromDate,@ToDate)) AS 'ORDER', 
StageName + '|' + CONVERT(NVARCHAR(50), Price) +'|'+ ProductName AS 'WORK', 
ISNULL(LK,0) AS LK, ISNULL(SR,0) AS SR, ISNULL (TP,0) AS TP, ISNULL (TG,0) AS TG
FROM X1 Order by LEN(EmployeeID), EmployeeID
";
            #endregion

            if (MaCongNhan != string.Empty)
            {
                zSQL = zSQL.Replace("@CustomParamater", " AND A.EmployeeID IN (" + MaCongNhan + ")");
            }
            else
            {
                zSQL = zSQL.Replace("@CustomParamater", "");
            }
            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.CommandTimeout = 1000;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable Report_19_V2(DateTime FromDate, DateTime ToDate, string MaCongNhan, string TeamKey) // nhóm thể hiện đã làm ỏ tổ gốc hoặc đã làm ở tổ khác
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            #region [SQL]
            string zSQL = @"
DECLARE @REPORT19 AS dbo.REPORT19

-- 1 GHI NĂNG XUẤT
INSERT INTO @REPORT19 (OrderDate, EmployeeName, EmployeeID, EmployeeKey, TeamKey, TeamName, ProductKey, ProductName, NhomCongViec, StageKey, StageName, Price, TIENDU, LK, TGBUA, TGDATA, SR, TP)
SELECT 
	A.OrderDate,																	-- NGÀY ĐƠN HÀNG
	A.EmployeeName,																    -- TÊN CÔNG NHÂN
	A.EmployeeID,																	-- MÃ CÔNG NHÂN
	A.EmployeeKey,																	-- KEY CÔNG NHÂN
	D.TeamKey,																		-- KEY NHÓM CÔNG NHÂN
	dbo.Fn_GetTeamName(D.TeamKey) AS TeamName,				                        -- TÊN NHÓM CÔNG NHÂN
	C.ProductKey,																	-- KEY SẢN PHẨM
	C.ProductName,																	-- TÊN SẢN PHẨM
    dbo.LayNhomCongViec(A.StageKey) AS NhomCongViec,			                    -- NHÓM CÔNG VIỆC
	A.StageKey,																		-- KEY CÔNG VIỆC
	D.StageName,																	-- TÊN CÔNG VIỆC
	D.Price,																		-- GIÁ CÔNG VIỆC
	0 AS TIENDU,																	-- TIEN GIO DU
	A.[Money] + A.MoneyPrivate + A.Money_Borrow AS LK,								-- LƯƠNG KHOÁN
	dbo.[LayThoiGianExtend_Rpt19](A.EmployeeKey,A.OrderKey) AS TGBUA,		        -- THỜI GIAN NHẬP = TAY
	A.[Time] + A.TimePrivate + A.Time_Borrow AS TGDATA,							    -- THỜI GIAN THEO DỮ LIỆU
	A.Basket  + A.BasketPrivate + A.Basket_Borrow AS SR,							-- SỐ RỔ
	A.Kg + A.KgPrivate + A.Kg_Borrow AS TP											-- THÀNH PHẨM

	FROM FTR_Order_Money A 
	LEFT JOIN FTR_Order C ON A.OrderKey = C.OrderKey
	LEFT JOIN IVT_Product_Stages D ON D.StageKey = A.StageKey
	WHERE 
	A.RecordStatus <> 99
	AND C.RecordStatus <> 99
	AND A.OrderDate BETWEEN @FromDate AND @ToDate
    @TeamKey1 
	@CustomParamater
-- 2 CHI LẠI
INSERT INTO @REPORT19 (OrderDate, EmployeeName, EmployeeID, EmployeeKey, TeamKey, TeamName, ProductKey, ProductName, NhomCongViec, StageKey, StageName, Price, TIENDU, LK, TGBUA, TGDATA, SR, TP)
SELECT
	A.OrderDate,																	-- NGÀY ĐƠN HÀNG
	A.EmployeeName,															        -- TÊN CÔNG NHÂN
	A.EmployeeID,																    -- MÃ CÔNG NHÂN
	A.EmployeeKey,																    -- KEY CÔNG NHÂN
	D.TeamKey,																	    -- KEY NHÓM CÔNG NHÂN
	dbo.Fn_GetTeamName(D.TeamKey) AS TeamName,			                            -- TÊN NHÓM CÔNG NHÂN
	C.ProductKey,																	-- KEY SẢN PHẨM
	C.ProductName,																    -- TÊN SẢN PHẨM
    dbo.[LayNhomCongViec](A.StageKey) AS NhomCongViec,	                            -- NHÓM CÔNG VIỆC
	A.StageKey,																	    -- KEY CÔNG VIỆC
	D.StageName,																    -- TÊN CÔNG VIỆC
	D.Price,																		-- GIÁ CÔNG VIỆC
	0 AS TIENDU,																	-- TIEN GIO DU
	(A.MoneyPersonal) AS LK,														-- LƯƠNG KHOÁN
	0 AS TGBUA,																		-- THỜI GIAN NHẬP = TAY
	(A.TimeProduct) AS TG,															-- THỜI GIAN THEO DỮ LIỆU
	(A.BasketProduct) AS SR,														-- SỐ RỔ
	(A.KgProduct) AS TP																-- THÀNH PHẨM
	FROM FTR_Order_Adjusted A 
	LEFT JOIN FTR_Order C ON A.OrderKey = C.OrderKey
	LEFT JOIN IVT_Product_Stages D ON D.StageKey = A.StageKey
	WHERE
	A.RecordStatus <> 99
	AND C.RecordStatus <> 99
	AND A.OrderDate BETWEEN @FromDate AND @ToDate AND A.Share !=1
    @TeamKey1 
	@CustomParamater
-- 3 GIỜ DƯ
INSERT INTO @REPORT19 (OrderDate, EmployeeName, EmployeeID, EmployeeKey, TeamKey, TeamName, ProductKey, ProductName, NhomCongViec, StageKey, StageName, Price, TIENDU, LK, TGBUA, TGDATA, SR, TP)
SELECT
	NULL AS OrderDate,												                -- NGÀY NHẬP
	A.EmployeeName,															        -- TÊN CÔNG NHÂN
	A.EmployeeID,																    -- MÃ CÔNG NHÂN
	A.EmployeeKey,																    -- KEY CÔNG NHÂN
	A.TeamKey,																	    -- KEY NHÓM CÔNG NHÂN
	dbo.Fn_GetTeamName(A.TeamKey) AS TeamName,			-- TÊN NHÓM CÔNG NHÂN
	'' AS ProductKey,															    -- KEY SẢN PHẨM
	'' AS ProductName,															    -- TÊN SẢN PHẨM
    '' AS NhomCongViec,														        -- NHÓM CÔNG VIỆC
	'' AS StageKey,																    -- KEY CÔNG VIỆC
	'' AS StageName,															    -- TÊN CÔNG VIỆC
	'' AS Price,																	-- GIÁ CÔNG VIỆC
	SUM(A.[Money]) AS TIENDU,														-- TIEN GIO DU
	0 AS LK,																		-- LƯƠNG KHOÁN
	0 AS TGBUA,																		-- THỜI GIAN NHẬP = TAY
	0 AS TG,																		-- THỜI GIAN THEO DỮ LIỆU
	0 AS SR,																		-- SỐ RỔ
	0 AS TP																			-- THÀNH PHẨM
	FROM [dbo].[Temp_Import_Detail] A
	WHERE
	A.RecordStatus <> 99
	@CustomParamater
	AND A.DateImport BETWEEN @FromDate AND @ToDate 
	GROUP BY A.EmployeeName, A.EmployeeID, A.EmployeeKey, A.TeamKey

SELECT
EmployeeName + '|' + EmployeeID AS [ORDER],
CONVERT(NVARCHAR(50),TeamKey) + '|' + TeamName + '|' + ProductName + '|' + NhomCongViec + '|' + StageName + '|' + CONVERT(NVARCHAR(50),Price) AS WORK,
SUM(TIENDU) TIENDU,
SUM(LK) LK, 
SUM(
	CASE
        WHEN NhomCongViec = 'CVTG' OR NhomCongViec = 'KHAC' THEN TP
		WHEN TGBUA > 0 THEN TGBUA
		WHEN TGBUA <= 0 THEN TGDATA  
	END
) AS TG,
SUM(SR) SR, 
SUM(TP) TP
FROM @REPORT19
GROUP BY EmployeeName, EmployeeID, EmployeeKey, TeamKey, TeamName, ProductKey, ProductName, NhomCongViec, StageKey, StageName, Price
ORDER BY TeamKey
";
            #endregion

            zSQL = zSQL.Replace("@CustomParamater", " AND A.EmployeeID IN (" + MaCongNhan + ")");
            if (TeamKey != "")
            {
                zSQL = zSQL.Replace("@TeamKey1", " AND D.TeamKey IN (" + TeamKey + ")");
                //zSQL = zSQL.Replace("@TeamKey2", " AND A.TeamKey =" + TeamKey + "");
            }
            else
            {
                zSQL = zSQL.Replace("@TeamKey1", " ");
                //zSQL = zSQL.Replace("@TeamKey2", "");
            }
            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.CommandTimeout = 1000;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        //        public static DataTable Report_19_V2(DateTime FromDate, DateTime ToDate, string MaCongNhan)
        //        {
        //            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
        //            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

        //            #region [SQL]
        //            string zSQL = @"
        //DECLARE @REPORT19 AS dbo.REPORT19

        //-- 1 GHI NĂNG XUẤT
        //INSERT INTO @REPORT19 (OrderDate, EmployeeName, EmployeeID, EmployeeKey, TeamKey, TeamName, ProductKey, ProductName, NhomCongViec, StageKey, StageName, Price, TIENDU, LK, TGBUA, TGDATA, SR, TP)
        //SELECT 
        //	A.OrderDate,																		-- NGÀY ĐƠN HÀNG
        //	A.EmployeeName,																-- TÊN CÔNG NHÂN
        //	A.EmployeeID,																	-- MÃ CÔNG NHÂN
        //	A.EmployeeKey,																	-- KEY CÔNG NHÂN
        //	D.TeamKey,																		-- KEY NHÓM CÔNG NHÂN
        //	dbo.Fn_GetTeamName(D.TeamKey) AS TeamName,				-- TÊN NHÓM CÔNG NHÂN
        //	C.ProductKey,																		-- KEY SẢN PHẨM
        //	C.ProductName,																	-- TÊN SẢN PHẨM
        //    dbo.LayNhomCongViec(A.StageKey) AS NhomCongViec,			-- NHÓM CÔNG VIỆC
        //	A.StageKey,																		-- KEY CÔNG VIỆC
        //	D.StageName,																	-- TÊN CÔNG VIỆC
        //	D.Price,																				-- GIÁ CÔNG VIỆC
        //	0 AS TIENDU,																							-- TIEN GIO DU
        //	A.[Money] + A.MoneyPrivate + A.Money_Borrow AS LK,								-- LƯƠNG KHOÁN
        //	dbo.[LayThoiGianExtend_Rpt19](A.EmployeeKey,A.OrderKey) AS TGBUA,		-- THỜI GIAN NHẬP = TAY
        //	A.[Time] + A.TimePrivate + A.Time_Borrow AS TGDATA,							-- THỜI GIAN THEO DỮ LIỆU
        //	A.Basket  + A.BasketPrivate + A.Basket_Borrow AS SR,								-- SỐ RỔ
        //	A.Kg + A.KgPrivate + A.Kg_Borrow AS TP													-- THÀNH PHẨM

        //	FROM FTR_Order_Money A 
        //	LEFT JOIN FTR_Order C ON A.OrderKey = C.OrderKey
        //	LEFT JOIN IVT_Product_Stages D ON D.StageKey = A.StageKey
        //	WHERE 
        //	A.RecordStatus <> 99
        //	AND C.RecordStatus <> 99
        //	AND A.OrderDate BETWEEN @FromDate AND @ToDate
        //	@CustomParamater
        //-- 2 CHI LẠI
        //INSERT INTO @REPORT19 (OrderDate, EmployeeName, EmployeeID, EmployeeKey, TeamKey, TeamName, ProductKey, ProductName, NhomCongViec, StageKey, StageName, Price, TIENDU, LK, TGBUA, TGDATA, SR, TP)
        //SELECT
        //	A.OrderDate,																	-- NGÀY ĐƠN HÀNG
        //	A.EmployeeName,															-- TÊN CÔNG NHÂN
        //	A.EmployeeID,																-- MÃ CÔNG NHÂN
        //	A.EmployeeKey,																-- KEY CÔNG NHÂN
        //	D.TeamKey,																	-- KEY NHÓM CÔNG NHÂN
        //	dbo.Fn_GetTeamName(D.TeamKey) AS TeamName,			-- TÊN NHÓM CÔNG NHÂN
        //	C.ProductKey,																	-- KEY SẢN PHẨM
        //	C.ProductName,																-- TÊN SẢN PHẨM
        //    dbo.[LayNhomCongViec](A.StageKey) AS NhomCongViec,	-- NHÓM CÔNG VIỆC
        //	A.StageKey,																	-- KEY CÔNG VIỆC
        //	D.StageName,																-- TÊN CÔNG VIỆC
        //	D.Price,																			-- GIÁ CÔNG VIỆC
        //	0 AS TIENDU,																							-- TIEN GIO DU
        //	(A.MoneyPersonal) AS LK,																		-- LƯƠNG KHOÁN
        //	0 AS TGBUA,																							-- THỜI GIAN NHẬP = TAY
        //	(A.TimeProduct) AS TG,																			-- THỜI GIAN THEO DỮ LIỆU
        //	(A.BasketProduct) AS SR,																		-- SỐ RỔ
        //	(A.KgProduct) AS TP																				-- THÀNH PHẨM
        //	FROM FTR_Order_Adjusted A 
        //	LEFT JOIN FTR_Order C ON A.OrderKey = C.OrderKey
        //	LEFT JOIN IVT_Product_Stages D ON D.StageKey = A.StageKey
        //	WHERE
        //	A.RecordStatus <> 99
        //	AND C.RecordStatus <> 99
        //	AND A.OrderDate BETWEEN @FromDate AND @ToDate AND A.Share !=1
        //	@CustomParamater
        //-- 3 GIỜ DƯ
        //INSERT INTO @REPORT19 (OrderDate, EmployeeName, EmployeeID, EmployeeKey, TeamKey, TeamName, ProductKey, ProductName, NhomCongViec, StageKey, StageName, Price, TIENDU, LK, TGBUA, TGDATA, SR, TP)
        //SELECT
        //	NULL AS OrderDate,												-- NGÀY NHẬP
        //	A.EmployeeName,															-- TÊN CÔNG NHÂN
        //	A.EmployeeID,																-- MÃ CÔNG NHÂN
        //	A.EmployeeKey,																-- KEY CÔNG NHÂN
        //	A.TeamKey,																	-- KEY NHÓM CÔNG NHÂN
        //	dbo.Fn_GetTeamName(A.TeamKey) AS TeamName,			-- TÊN NHÓM CÔNG NHÂN
        //	'' AS ProductKey,															-- KEY SẢN PHẨM
        //	'' AS ProductName,															-- TÊN SẢN PHẨM
        //    '' AS NhomCongViec,														-- NHÓM CÔNG VIỆC
        //	'' AS StageKey,																-- KEY CÔNG VIỆC
        //	'' AS StageName,															-- TÊN CÔNG VIỆC
        //	'' AS Price,																		-- GIÁ CÔNG VIỆC
        //	SUM(A.[Money]) AS TIENDU,																	-- TIEN GIO DU
        //	0 AS LK,																								-- LƯƠNG KHOÁN
        //	0 AS TGBUA,																							-- THỜI GIAN NHẬP = TAY
        //	0 AS TG,																								-- THỜI GIAN THEO DỮ LIỆU
        //	0 AS SR,																								-- SỐ RỔ
        //	0 AS TP																								    -- THÀNH PHẨM
        //	FROM [dbo].[Temp_Import_Detail] A
        //	WHERE
        //	A.RecordStatus <> 99
        //	@CustomParamater
        //	AND A.DateImport BETWEEN @FromDate AND @ToDate 
        //	GROUP BY A.EmployeeName, A.EmployeeID, A.EmployeeKey, A.TeamKey

        //SELECT
        //EmployeeName + '|' + EmployeeID AS [ORDER],
        //CONVERT(NVARCHAR(50),TeamKey) + '|' + TeamName + '|' + ProductName + '|' + NhomCongViec + '|' + StageName + '|' + CONVERT(NVARCHAR(50),Price) AS WORK,
        //SUM(TIENDU) TIENDU,
        //SUM(LK) LK, 
        //SUM(
        //	CASE
        //        WHEN NhomCongViec = 'CVTG' OR NhomCongViec = 'KHAC' THEN TP
        //		WHEN TGBUA > 0 THEN TGBUA
        //		WHEN TGBUA <= 0 THEN TGDATA  
        //	END
        //) AS TG,
        //SUM(SR) SR, 
        //SUM(TP) TP
        //FROM @REPORT19
        //GROUP BY EmployeeName, EmployeeID, EmployeeKey, TeamKey, TeamName, ProductKey, ProductName, NhomCongViec, StageKey, StageName, Price
        //ORDER BY TeamKey
        //";
        //            #endregion
        //            zSQL = zSQL.Replace("@CustomParamater", " AND A.EmployeeID IN (" + MaCongNhan + ")");
        //            DataTable zTable = new DataTable();
        //            string zConnectionString = ConnectDataBase.ConnectionString;
        //            try
        //            {
        //                SqlConnection zConnect = new SqlConnection(zConnectionString);
        //                zConnect.Open();
        //                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
        //                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
        //                zCommand.CommandTimeout = 1000;
        //                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
        //                zAdapter.Fill(zTable);
        //                zCommand.Dispose();
        //                zConnect.Close();
        //            }
        //            catch (Exception ex)
        //            {
        //                string zstrMessage = ex.ToString();
        //            }
        //            return zTable;
        //        }

        #endregion

        //Frm_Report_2020_01    Bảng thông tin công nhân theo công đoạn số lượng/lương khoán tại tổ chính (TRÂN Y/C NGÀY 18-08-2020)
        //        public static DataTable TRANPHAM1(DateTime FromDate, DateTime ToDate, string TeamKey)
        //        {
        //            string zSQL = @"


        //--declare @FromDate datetime ='2020-05-02 00:00:00'
        //--declare @ToDate datetime ='2020-05-02 23:59:59'
        //--declare @TeamKey int = 79
        //CREATE TABLE #Temp
        //(	[OrderKey] nvarchar(500),
        //	[TeamKeyEmployee] INT,
        //	[TeamKey] INT ,
        //	[EmployeeKey] nvarchar(500),
        //	[EmployeeName] nvarchar(500) ,
        //	[EmployeeID] nvarchar(500) ,
        //	[StageGroup] NVARCHAR(500),
        //	[StageKey] INT ,
        //	[StageName] nvarchar(500) ,
        //	[StagePrice] FLOAT ,
        //	[TG] FLOAT,
        //	[SL] FLOAT ,
        //	[LK] FLOAT ,
        //	[TYPE] INT
        //)


        //INSERT INTO #Temp 
        //(OrderKey,TeamKeyEmployee,TeamKey,EmployeeKey,EmployeeName,EmployeeID,
        //StageGroup,StageKey,StageName,StagePrice,TG,SL,LK,[TYPE])
        //	SELECT 
        //	A.OrderKey,
        //    dbo.Fn_LayTeamKey_TheoThang(A.EmployeeKey,@FromDate),     
        //	A.TeamKey, 
        //	A.EmployeeKey,
        //    A.EmployeeName, 
        //    A.EmployeeID,
        //	dbo.LayNhomCongViec(A.StageKey),
        //	A.StageKey,
        //	dbo.LayTenCongViec(A.StageKey) ,
        //	dbo.LayGiaCongViec(A.StageKey) ,
        //	ISNULL((A.[Time] +A.TimePrivate +A.TimeEat+Time_Eat),0), 
        //	ISNULL((A.Kg + A.Kg_Borrow+A.KgPrivate+A.Kg_Eat),0),
        //	ISNULL((A.[Money] + A.Money_Borrow+A.MoneyPrivate+A.Money_Eat),0) ,
        //	CASE 
        //			WHEN  (A.Category=1  OR A.CategoryPrivate=1) THEN 0
        //			WHEN  (A.Category !=1 AND A.CategoryPrivate!=1) THEN 1
        //	END AS [TYPE]
        //	FROM FTR_Order_Money A
        //	LEFT JOIN FTR_Order B ON B.OrderKey=A.OrderKey
        //	WHERE A.RecordStatus <> 99
        //	AND A.OrderDate BETWEEN @FromDate AND @ToDate
        //	AND A.TeamKey = @TeamKey
        //SELECT 
        //CONVERT(NVARCHAR(50), [Type]) + '|' + EmployeeName + '|' + EmployeeID + '|' + dbo.Fn_GetTeamID(TeamKeyEmployee) AS CONGNHAN,
        //StageName + '|' + CONVERT(NVARCHAR(50), StagePrice) AS NOIDUNG, 
        //CASE 
        //	WHEN TGDIEUCHINH > 0 THEN TGDIEUCHINH	-- THỜI GIAN NHẬP TAY TỪ FORM EXCEL THỜI GIAN
        //	WHEN StageGroup = 'KHAC' THEN SL
        //	WHEN StageGroup = 'CVTG' THEN SL
        //	WHEN TGDIEUCHINH = 0 THEN TG	-- THỜI GIAN NHẬP TỪ FORM CÔNG ĐOẠN ĐƠN HÀNG	
        //END AS TG,

        //SL, LK
        //FROM
        //(
        //	SELECT A.*, [dbo].[LayThoiGianLamSanPham_Rpt20](A.EmployeeKey,A.StageKey,A.OrderKey) AS TGDIEUCHINH,
        //    D.RANK AS BranchRank,C.RANK AS DepartmentRank,B.RANK as TeamRank FROM #Temp A 
        //   	LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKeyEmployee
        //	LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
        //	LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey

        //) X
        //	ORDER BY [TYPE], BranchRank,DepartmentRank,TeamRank,LEN(EmployeeID), EmployeeID

        //DROP TABLE #Temp;
        //";

        //            DataTable zTable = new DataTable();
        //            string zConnectionString = ConnectDataBase.ConnectionString;
        //            try
        //            {
        //                SqlConnection zConnect = new SqlConnection(zConnectionString);
        //                zConnect.Open();
        //                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
        //                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
        //                zCommand.Parameters.Add("@TeamKey", SqlDbType.NVarChar).Value = TeamKey;
        //                zCommand.CommandTimeout = 350;
        //                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
        //                zAdapter.Fill(zTable);
        //                zCommand.Dispose();
        //                zConnect.Close();
        //            }
        //            catch (Exception ex)
        //            {
        //                string zstrMessage = ex.ToString();
        //            }
        //            return zTable;
        //        }

        //.Phân bổ chi tiết theo hàng tươi-tái chế-vô bao bì- khác(theo nhóm, )
        //        public static DataTable TRANPHAM1V2(DateTime FromDate, DateTime ToDate, string TeamKey)
        //        {
        //            string zSQL = @"


        //--declare @FromDate datetime ='2020-05-02 00:00:00'
        //--declare @ToDate datetime ='2020-05-02 23:59:59'
        //--declare @TeamKey int = 79
        //CREATE TABLE #Temp
        //(	[OrderKey] nvarchar(500),
        //	[TeamKeyEmployee] INT,
        //	[TeamKey] INT ,
        //	[EmployeeKey] nvarchar(500),
        //	[EmployeeName] nvarchar(500) ,
        //	[EmployeeID] nvarchar(500) ,
        //	[StageGroup] NVARCHAR(500),
        //	[StageKey] INT ,
        //	[StageName] nvarchar(500) ,
        //	[StagePrice] FLOAT ,
        //	[TG] FLOAT,
        //	[SL] FLOAT ,
        //	[LK] FLOAT ,
        //	[TYPE] INT
        //)


        //INSERT INTO #Temp 
        //(OrderKey,TeamKeyEmployee,TeamKey,EmployeeKey,EmployeeName,EmployeeID,
        //StageGroup,StageKey,StageName,StagePrice,TG,SL,LK,[TYPE])
        //	SELECT 
        //	A.OrderKey,
        //    [dbo].[Fn_TeamKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate),     
        //	A.TeamKey, 
        //	A.EmployeeKey,
        //    A.EmployeeName, 
        //    A.EmployeeID,
        //	dbo.LayNhomCongViec(A.StageKey),
        //	A.StageKey,
        //	dbo.LayTenCongViec(A.StageKey) ,
        //	dbo.LayGiaCongViec(A.StageKey) ,
        //	ISNULL((A.[Time] +A.TimePrivate +A.TimeEat+Time_Eat),0), 
        //	ISNULL((A.Kg + A.Kg_Borrow+A.KgPrivate+A.Kg_Eat),0),
        //	ISNULL((A.[Money] + A.Money_Borrow+A.MoneyPrivate+A.Money_Eat),0) ,
        //	CASE 
        //			WHEN  (A.Category=1  OR A.CategoryPrivate=1) THEN 0
        //			WHEN  (A.Category !=1 AND A.CategoryPrivate!=1) THEN 1
        //	END AS [TYPE]
        //	FROM FTR_Order_Money A
        //	LEFT JOIN FTR_Order B ON B.OrderKey=A.OrderKey
        //	WHERE A.RecordStatus <> 99
        //	AND A.OrderDate BETWEEN @FromDate AND @ToDate
        //	AND A.TeamKey = @TeamKey
        //SELECT 
        //CONVERT(NVARCHAR(50), [Type]) + '|' + EmployeeName + '|' + EmployeeID + '|' + dbo.Fn_GetTeamID(TeamKeyEmployee) AS CONGNHAN,
        //StageName + '|' + CONVERT(NVARCHAR(50), StagePrice) AS NOIDUNG, 
        //CASE 
        //	WHEN TGDIEUCHINH > 0 THEN TGDIEUCHINH	-- THỜI GIAN NHẬP TAY TỪ FORM EXCEL THỜI GIAN
        //	WHEN StageGroup = 'KHAC' THEN SL
        //	WHEN StageGroup = 'CVTG' THEN SL
        //	WHEN TGDIEUCHINH = 0 THEN TG	-- THỜI GIAN NHẬP TỪ FORM CÔNG ĐOẠN ĐƠN HÀNG	
        //END AS TG,

        //SL, LK
        //FROM
        //(
        //	SELECT A.*, [dbo].[LayThoiGianLamSanPham_Rpt20](A.EmployeeKey,A.StageKey,A.OrderKey) AS TGDIEUCHINH,
        //    D.RANK AS BranchRank,C.RANK AS DepartmentRank,B.RANK as TeamRank FROM #Temp A 
        //   	LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKeyEmployee
        //	LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
        //	LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey

        //) X
        //	ORDER BY [TYPE], BranchRank,DepartmentRank,TeamRank,LEN(EmployeeID), EmployeeID

        //DROP TABLE #Temp;
        //";

        //            DataTable zTable = new DataTable();
        //            string zConnectionString = ConnectDataBase.ConnectionString;
        //            try
        //            {
        //                SqlConnection zConnect = new SqlConnection(zConnectionString);
        //                zConnect.Open();
        //                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
        //                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
        //                zCommand.Parameters.Add("@TeamKey", SqlDbType.NVarChar).Value = TeamKey;
        //                zCommand.CommandTimeout = 350;
        //                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
        //                zAdapter.Fill(zTable);
        //                zCommand.Dispose();
        //                zConnect.Close();
        //            }
        //            catch (Exception ex)
        //            {
        //                string zstrMessage = ex.ToString();
        //            }
        //            return zTable;
        //        }
        public static DataTable TRANPHAM1V3(DateTime FromDate, DateTime ToDate, string TeamKey)
        {
            string zSQL = @"
--declare @FromDate datetime ='2020-05-02 00:00:00'
--declare @ToDate datetime ='2020-05-02 23:59:59'
--declare @TeamKey int = 79
CREATE TABLE #Temp
(	[OrderKey] nvarchar(500),
	[TeamKeyEmployee] INT,
	[TeamKey] INT ,
	[EmployeeKey] nvarchar(500),
	[EmployeeName] nvarchar(500) ,
	[EmployeeID] nvarchar(500) ,
	[StageGroup] NVARCHAR(500),
	[StageKey] INT ,
	[StageName] nvarchar(500) ,
	[StagePrice] FLOAT ,
	[TG] FLOAT,
	[SL] FLOAT ,
	[LK] FLOAT ,
	[TYPE] INT
)


        INSERT INTO #Temp 
	    SELECT 
	    A.OrderKey,
        [dbo].[Fn_TeamKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate),     
	    A.TeamKey, 
	    A.EmployeeKey,
        A.EmployeeName, 
        A.EmployeeID,
	    dbo.LayNhomCongViec(A.StageKey),
	    A.StageKey,
	    dbo.LayTenCongViec(A.StageKey) ,
	    dbo.LayGiaCongViec(A.StageKey) ,
	    ISNULL((A.[Time] +A.TimePrivate +A.TimeEat+Time_Eat),0), 
	    ISNULL((A.Kg + A.Kg_Borrow+A.KgPrivate+A.Kg_Eat),0),
	    ISNULL((A.[Money] + A.Money_Borrow+A.MoneyPrivate+A.Money_Eat),0) ,
	    CASE 
			    WHEN  (A.Category=1  OR A.CategoryPrivate=1) THEN 0
			    WHEN  (A.Category !=1 AND A.CategoryPrivate!=1) THEN 1
	    END AS [TYPE]
	    FROM FTR_Order_Money A
	    LEFT JOIN FTR_Order B ON B.OrderKey=A.OrderKey
	    WHERE A.RecordStatus <> 99
	    AND A.OrderDate BETWEEN @FromDate AND @ToDate
	    AND A.TeamKey = @TeamKey
SELECT 
CONVERT(NVARCHAR(50), [Type]) + '|' + EmployeeName + '|' + EmployeeID + '|' + dbo.Fn_GetTeamID(TeamKeyEmployee) AS CONGNHAN,
StageName + '|' + CONVERT(NVARCHAR(50), StagePrice) AS NOIDUNG, 
CASE 
	WHEN TGDIEUCHINH > 0 THEN TGDIEUCHINH	-- THỜI GIAN NHẬP TAY TỪ FORM EXCEL THỜI GIAN
	WHEN StageGroup = 'KHAC' THEN SL
	WHEN StageGroup = 'CVTG' THEN SL
	WHEN TGDIEUCHINH = 0 THEN TG	-- THỜI GIAN NHẬP TỪ FORM CÔNG ĐOẠN ĐƠN HÀNG	
END AS TG,
SL, LK
FROM
(
	SELECT A.*, [dbo].[LayThoiGianLamSanPham_Rpt20](A.EmployeeKey,A.StageKey,A.OrderKey) AS TGDIEUCHINH,
    D.RANK AS BranchRank,C.RANK AS DepartmentRank,B.RANK as TeamRank FROM #Temp A 
   	LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKeyEmployee
	LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
	LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
   
) X
	ORDER BY [TYPE], BranchRank,DepartmentRank,TeamRank,LEN(EmployeeID), EmployeeID

DROP TABLE #Temp;
";

            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.NVarChar).Value = TeamKey;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable TRANPHAM1V4(DateTime FromDate, DateTime ToDate, int TeamKey, string Search)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string zSQL = @"
--declare @FromDate datetime ='2020-06-04 00:00:00'
--declare @ToDate datetime ='2020-06-04 23:59:59'
--declare @TeamKey int = 88
--Lấy dữ liệu từng  đơn hàng
CREATE TABLE #Temp
(	[OrderKey] nvarchar(500),
	[TeamKeyEmployee] INT,
	[TeamKey] INT ,
	[EmployeeKey] nvarchar(500),
	[EmployeeName] nvarchar(500) ,
	[EmployeeID] nvarchar(500) ,
	[StageGroup] NVARCHAR(500),
	[StageKey] INT ,
	[StageName] nvarchar(500) ,
	[StagePrice] FLOAT ,
	[TG] FLOAT,
	[SL] FLOAT ,
	[LK] FLOAT ,
	[TYPE] INT--Có phải người mượn không
)
        INSERT INTO #Temp 
	    SELECT 
	    A.OrderKey,
        [dbo].[Fn_TeamKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate),     
	    A.TeamKey, 
	    A.EmployeeKey,
        A.EmployeeName, 
        A.EmployeeID,
	    dbo.LayNhomCongViec(A.StageKey),
	    A.StageKey,
	    dbo.LayTenCongViec(A.StageKey) ,
	    dbo.LayGiaCongViec(A.StageKey) ,
		CASE 
			WHEN [dbo].[LayThoiGianLamSanPham_Rpt20](A.EmployeeKey,A.StageKey,A.OrderKey) >0 THEN [dbo].[LayThoiGianLamSanPham_Rpt20](A.EmployeeKey,A.StageKey,A.OrderKey)
			WHEN dbo.LayNhomCongViec(A.StageKey) = 'KHAC' THEN ISNULL((A.Kg + A.Kg_Borrow+A.KgPrivate+A.Kg_Eat),0)
			WHEN dbo.LayNhomCongViec(A.StageKey) = 'CVTG' THEN ISNULL((A.Kg + A.Kg_Borrow+A.KgPrivate+A.Kg_Eat),0)
			--ELSE  ISNULL((A.[Time] +A.TimePrivate +A.TimeEat+Time_Eat),0)	
	        --ELSE ISNULL(C.Time,0)
           ELSE ISNULL([dbo].[LayThoiGianCaNhan_NhapLieu](A.OrderKey,A.EmployeeKey),0)	--Số giờ bên nhập liệu. nếu lưu giờ order_money vào sẽ bị sai khi xác định trường nếu chạy tính lương lại
		END AS [TG],
	    ISNULL((A.Kg + A.Kg_Borrow+A.KgPrivate+A.Kg_Eat),0),
	    ISNULL((A.[Money] + A.Money_Borrow+A.MoneyPrivate+A.Money_Eat),0) ,
	    CASE 
			    WHEN  (A.Category=1  OR A.CategoryPrivate=1) THEN 0
			    WHEN  (A.Category !=1 AND A.CategoryPrivate!=1) THEN 1
	    END AS [TYPE]
	    FROM FTR_Order_Money A
	    LEFT JOIN FTR_Order B ON B.OrderKey=A.OrderKey
		--LEFT JOIN FTR_Order_Employee  C ON C.OrderKey=B.OrderKey
	    WHERE A.RecordStatus <> 99  AND B.RecordStatus <> 99 --AND C.RecordStatus <> 99
	    AND A.OrderDate BETWEEN @FromDate AND @ToDate
	    AND A.TeamKey = @TeamKey AND (A.EmployeeID LIKE @Search OR A.EmployeeName LIKE @Search)
--Tính tổng từ ngày đến ngày
CREATE TABLE #Temp2
(	[OrderKey] nvarchar(500),
	[TeamKeyEmployee] INT,
	[TeamKey] INT ,
	[EmployeeKey] nvarchar(500),
	[EmployeeName] nvarchar(500) ,
	[EmployeeID] nvarchar(500) ,
	[StageGroup] NVARCHAR(500),
	[StageKey] INT ,
	[StageName] nvarchar(500) ,
	[StagePrice] FLOAT ,
	[TG] FLOAT,
	[SL] FLOAT ,
	[LK] FLOAT ,
	[TYPE] INT
)
		INSERT INTO #Temp2
		SELECT [OrderKey],[TeamKeyEmployee],[TeamKey],[EmployeeKey],[EmployeeName],[EmployeeID],
		[StageGroup],[StageKey],[StageName],[StagePrice],SUM([TG]),SUM([SL]),SUM([LK]),[TYPE]
		FROM #Temp
		GROUP BY [OrderKey],[TeamKeyEmployee],[TeamKey],[EmployeeKey],[EmployeeName],[EmployeeID],
		[StageGroup],[StageKey],[StageName],[StagePrice],[TYPE]
--SELECT * FROM #Temp2

SELECT 
CONVERT(NVARCHAR(50), [Type]) + '|' + EmployeeName + '|' + EmployeeID + '|' + dbo.Fn_GetTeamID(TeamKeyEmployee)
+'|'+BranchRank+'|'+DepartmentRank+'|'+TeamRank AS CONGNHAN,
[dbo].[Fn_LayTen_NhomCongViec]([StageGroup])+'|'+  StageName + '|' + CONVERT(NVARCHAR(50), StagePrice) AS NOIDUNG, 
TG,
SL, 
LK
FROM
(
	SELECT A.*,
    CAST(D.RANK AS nvarchar(50)) AS BranchRank,CAST(C.RANK AS nvarchar(50)) AS DepartmentRank, CAST(B.RANK AS nvarchar(50)) as TeamRank FROM #Temp A 
   	LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKeyEmployee
	LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
	LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
   
) X
	ORDER BY [dbo].[Fn_LaySoSapXep_NhomCongViec](StageGroup)

DROP TABLE #Temp
DROP TABLE #Temp2
";

            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search + "%";
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }


        public static DataTable ReportMau1(DateTime FromDate, DateTime ToDate,int DepartmentKey, int TeamKey,string Parent, string Search)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            string zFilter = "";
            if (DepartmentKey != 0)
            {
                zFilter += " AND [dbo].[Fn_GetDepartmentKey_FromTeamKey](A.TeamKey) = @DepartmentKey";
            }
            if (TeamKey != 0)
            {
                zFilter += " AND A.TeamKey = @TeamKey";
            }
            if (Parent != "0")
            {
                zFilter += " AND C.Parent = @Parent";
            }
            if (Search.Length != 0)
            {
                zFilter += " AND A.ProductID LIKE @Search";
            }
            string zSQL = @"
--DECLARE @FromDate DATETIME = '2020-05-01 00:00:00'
--DECLARE @ToDate DATETIME = '2020-05-01 23:59:59'
CREATE TABLE #Temp
(	
	[LOAISP] [nvarchar](100) ,
    [KEYSP] [nvarchar](100) ,
	[MASP] [nvarchar](500) ,
	[TENSP] [nvarchar](500) ,
	[KEYNHOMSP][nvarchar](500) ,
	[MANHOM] [nvarchar](500) ,
	[TENNHOM] [nvarchar](500) ,
	[NHOMCONGVIEC] [nvarchar](500),
	[MACONGVIEC] [nvarchar](500) ,
	[CONGVIEC] [nvarchar](100) ,
	[DONGIA] [nvarchar](100) ,
	[LK] [float] ,
	[TG] [float] ,
	[TP] [float] 	
)

INSERT INTO #Temp 
	SELECT 
		ISNULL(dbo.LayLoaiHang(A.OrderIDFollow, A.ProductKey), '5.KHÁC') AS LOAISP,
        A.ProductKey AS KEYSP,
		A.ProductID AS MASP, 
		A.ProductName AS TENSP, 
	    C.Parent AS KEYNHOMSP,
		A.TeamKey AS MaNhom,
		dbo.Fn_GetTeamName(A.TeamKey) AS TenNhom,
		dbo.LayNhomCongViec(A.Category_Stage) AS NhomCongViec,	
		A.Category_Stage AS MACONGVIEC,
		dbo.LayTenCongViec(A.Category_Stage) AS CongViec,    
		dbo.LayGiaCongViec(A.Category_Stage) AS DonGia,	
			ISNULL(SUM(B.[Money]) + SUM(B.MoneyPrivate) + SUM(B.Money_Borrow) + SUM(B.Money_Eat),0) AS 'LK',
			ISNULL(SUM(B.[Time]) + SUM(B.TimePrivate) + SUM(B.Time_Borrow) + SUM(B.Time_Eat),0) AS 'TG',		
			ISNULL(SUM(B.Kg) + SUM(B.KgPrivate) + SUM(B.Kg_Borrow) + SUM(B.Kg_Eat),0) AS 'TP'
		FROM FTR_Order A 
		LEFT JOIN FTR_Order_Money B ON A.OrderKey = B.OrderKey
	    LEFT JOIN [dbo].[IVT_Product] C ON C.ProductKey=A.ProductKey
		WHERE A.OrderDate BETWEEN @FromDate AND @ToDate  
        AND A.RecordStatus <> 99 AND B.RecordStatus <> 99 @Paramater
		GROUP BY A.ProductKey, A.ProductID, A.ProductName, A.Category_Stage, A.TeamKey, A.OrderIDFollow, C.Parent

SELECT 
	LOAISP + '|' + TENSP + '|' + MASP +'|'+ [dbo].[Fn_LayDonViTinhTuSanPham](KEYSP)+'|'+[dbo].[Fn_LayTenSanPhamTuBangProduct](KEYNHOMSP) + '|'+ [dbo].[LayLoaiSP](MASP) AS LeftColumn , 
	MaNhom +  '|' +TenNhom + '|' + CongViec + '|' + DonGia AS HeaderColumn, 
	LK, 
	CASE 
		WHEN TGDIEUCHINH > 0 THEN TGDIEUCHINH	-- THỜI GIAN NHẬP TAY TỪ FORM EXCEL THỜI GIAN
		WHEN TGDIEUCHINH = 0 THEN TGCONGDOAN	-- THỜI GIAN NHẬP TỪ FORM CÔNG ĐOẠN ĐƠN HÀNG
		WHEN NHOMCONGVIEC = 'KHAC' THEN TP
		WHEN NHOMCONGVIEC = 'CVTG' THEN TP	
	END AS TG,
	TP
	FROM
	(
		SELECT 
		LOAISP, KEYSP, MASP, TENSP,KEYNHOMSP, MANHOM, TENNHOM, NHOMCONGVIEC, 
		MACONGVIEC, CONGVIEC, DONGIA, LK, TP, TG AS TGCONGDOAN, 
		ISNULL(dbo.LayThoiGianLamSanPham(@FROMDATE, @TODATE, MACONGVIEC),0) AS TGDIEUCHINH
		FROM #Temp
	) X
        LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=X.MANHOM
	    LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
	    LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
        ORDER BY D.Rank,C.Rank,B.Rank
DROP TABLE #Temp;

";
            zSQL = zSQL.Replace("@Paramater",zFilter);
            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search.Trim() + "%";
                zCommand.Parameters.Add("@Parent", SqlDbType.NVarChar).Value = Parent;
                zCommand.CommandTimeout = 1000;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        //.Phân bổ tổng hợp sản phẩm theo công đoạn, nhóm, (THUONG,CN,LE,TONG)
        public static DataTable ReportMau2(DateTime FromDate, DateTime ToDate, int DepartmentKey, int TeamKey,string Parent, string Search)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            string zSQL = @"
--DECLARE @FromDate DATETIME = '2020-05-01 00:00:00'
--DECLARE @ToDate DATETIME = '2020-05-01 23:59:59'

CREATE TABLE #Temp
(
	[NGAY] [datetime] ,
	[LOAISP] [nvarchar](100) ,
    [KEYSP] [nvarchar](100) ,
	[MASP] [nvarchar](500) ,
	[TENSP] [nvarchar](500) ,
	[KEYNHOMSP][nvarchar](500) ,
	[MANHOM] [nvarchar](50) ,
	[TENNHOM] [nvarchar](500) ,
	[TONG] [float] ,
	[CN] [float] ,
	[LE] [float] ,
	[THUONG] [float] 
)

INSERT INTO #Temp
SELECT 
	A.OrderDate AS NGAY,
	ISNULL(dbo.LayLoaiHang(A.OrderIDFollow, A.ProductKey), '5.KHÁC') AS LOAISP,
    A.ProductKey AS KEYSP,
	A.ProductID AS MASP,	
	A.ProductName AS TENSP,	
	C.Parent AS KEYNHOMSP,
	CONVERT(NVARCHAR(50), A.TeamKey) AS MANHOM,
	dbo.Fn_GetTeamName(A.TeamKey) AS TENNHOM,
	SUM(
	    ISNULL([MONEY], 0)						+
	    ISNULL([Money_Borrow] ,0) 			    +
	    ISNULL([MoneyPrivate], 0)               +
        ISNULL([Money_Eat],0)) 
	AS TONG,
	0 AS CN,
	0 AS LE,
	0 AS THUONG
	FROM FTR_Order A
	LEFT JOIN FTR_Order_Money B ON B.OrderKey=A.OrderKey
	LEFT JOIN [dbo].[IVT_Product] C ON C.ProductKey=A.ProductKey
	WHERE A.OrderDate BETWEEN @FromDate AND @ToDate
	AND A.RecordStatus <> 99 AND B.RecordStatus <> 99";
            if (DepartmentKey != 0)
            {
                zSQL += " AND [dbo].[Fn_GetDepartmentKey_FromTeamKey](A.TeamKey) = @DepartmentKey";
            }
            if (TeamKey != 0)
            {
                zSQL += " AND A.TeamKey = @TeamKey";
            }
            if (Parent != "0")
            {
                zSQL += " AND C.Parent = @Parent";
            }
            if (Search.Length != 0)
            {
                zSQL += " AND A.ProductID LIKE @Search";
            }

            zSQL += @"
GROUP BY A.OrderDate, A.OrderIDFollow, A.ProductKey, A.ProductID, A.ProductName, A.TeamKey ,C.Parent
SELECT 
P.LOAISP + '|' + P.TENSP + '|' + P.MASP +'|'+ [dbo].[Fn_LayDonViTinhTuSanPham](P.KEYSP)+'|'+[dbo].[Fn_LayTenSanPhamTuBangProduct](P.KEYNHOMSP) + '|'+ [dbo].[LayLoaiSP](P.MASP) AS LeftColumn,
P.MANHOM + '|' + P.TENNHOM AS HeaderColumn,
P.TONG, P.CN, P.LE, P.THUONG
FROM (
	SELECT X.NGAY, X.LOAISP,X.KEYSP, X.MASP, X.TENSP,X.KEYNHOMSP, X.MANHOM, X.TENNHOM, X.TONG,
	CASE 
		WHEN DATENAME(weekday,X.NGAY) ='SUNDAY' THEN X.TONG / [dbo].[LayHeSoChuNhat](X.NGAY)
		WHEN DATENAME(weekday,X.NGAY) !='SUNDAY' THEN 0
	END AS CN,
	CASE 
		WHEN [dbo].[LayHeSoNgayLeDonHang](X.NGAY)  >0 THEN X.TONG - (X.TONG / [dbo].[LayHeSoNgayLeDonHang](X.NGAY))
		WHEN [dbo].[LayHeSoNgayLeDonHang](X.NGAY)  <=0 THEN 0
	END  AS LE,
	CASE
		WHEN DATENAME(weekday,X.NGAY) ='SUNDAY' THEN X.TONG / [dbo].[LayHeSoChuNhat](X.NGAY)
		WHEN [dbo].[LayHeSoNgayLeDonHang](X.NGAY)  >0 THEN X.TONG/[dbo].[LayHeSoNgayLeDonHang](X.NGAY)
		WHEN X.TONG > 0 THEN X.TONG
		WHEN X.TONG <=0 THEN 0
	END AS THUONG
	FROM #Temp X
) P
        LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=P.MANHOM
	    LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
	    LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
        ORDER BY D.Rank,C.Rank,B.Rank
DROP TABLE #Temp";

            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@Parent", SqlDbType.NVarChar).Value = Parent;
                zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search.Trim() + "%";
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        //CẦN KIỂM TRA LẠI MÃ PHÒNG BAN*********************************************
        // Báo cáo chấm công chưa chốt
        public static DataTable BaoCaoCong(DateTime FromDate, DateTime ToDate, int BranchKey, int DepartmentKey, int TeamKey, string Search)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            string zSQL = @"
--DECLARE @FROMDATE DATETIME = '2020-05-01 00:00:00'
--DECLARE @TODATE DATETIME = '2020-05-31 23:59:59'
DECLARE @cols NVARCHAR(MAX)
DECLARE @query NVARCHAR(MAX)

CREATE TABLE #CHAMCONG (
	MAID NVARCHAR(500), NGAYBAOCAO NVARCHAR(500), MANHANVIEN NVARCHAR(500), HOTEN NVARCHAR(500),KEYPHONGBAN INT, MAPHONGBAN NVARCHAR(500), TENPHONGBAN NVARCHAR(500), CHUCVU NVARCHAR(500), TENCOT NVARCHAR(500), GIATRI NVARCHAR(500), SAPXEP INT
)

INSERT INTO #CHAMCONG 
(
	MAID, NGAYBAOCAO, MANHANVIEN, HOTEN,KEYPHONGBAN, MAPHONGBAN, TENPHONGBAN, CHUCVU, TENCOT, GIATRI,SAPXEP
)

SELECT 
UPPER(A.EmployeeKey), 
CONVERT(NVARCHAR(100), GETDATE(),103), 
A.EmployeeID, 
UPPER(A.EmployeeName), 
A.TeamKey,
A.TeamID,
A.TeamName,
dbo.Fn_GetPositionName_FromEmployeeKey(A.EmployeeKey),
DAY(A.DateWrite) AS TENCOT, 
TimeID AS GIATRI,
0
FROM HRM_Employee_KeepingTime A
WHERE A.DateWrite BETWEEN @FROMDATE AND @TODATE @CustomTeam


INSERT INTO #CHAMCONG 
(
	MAID, NGAYBAOCAO, MANHANVIEN, HOTEN,KEYPHONGBAN, MAPHONGBAN, TENPHONGBAN, CHUCVU, TENCOT, GIATRI, SAPXEP
)
SELECT
UPPER(A.EmployeeKey), 
CONVERT(NVARCHAR(100), GETDATE(),103), 
A.EmployeeID, 
UPPER(A.EmployeeName), 
A.TeamKey,
A.TeamID,
A.TeamName,
dbo.Fn_GetPositionName_FromEmployeeKey(A.EmployeeKey),
A.CodeName AS TENCOT,
A.NUMBER AS GIATRI,
B.Rank
FROM HRM_TimeKeeping_Month A
LEFT JOIN [dbo].[HRM_CodeReport] B ON B.ID=A.CodeID
WHERE A.DateWrite BETWEEN @FROMDATE AND @TODATE @CustomTeam

SET @cols = STUFF((SELECT ',' + QUOTENAME(TENCOT) FROM #CHAMCONG GROUP BY TENCOT,SAPXEP ORDER BY SAPXEP,LEN(TENCOT), TENCOT
					FOR XML PATH(''), TYPE
					).value(N'.[1]', N'NVARCHAR(MAX)') 
				,1,1,'');
--Xóa cột sắp xếp để pivot không bị loạn
ALTER TABLE #CHAMCONG
   DROP COLUMN SAPXEP;

SET @query = '
			SELECT KEYPHONGBAN,MAPHONGBAN, TENPHONGBAN, MANHANVIEN, HOTEN, CHUCVU,' + @cols + ' 
			FROM 
            (
                SELECT * FROM #CHAMCONG  --@MAPHONGBAN
			) X
            PIVOT 
            (
                 MAX(GIATRI)
				 FOR TENCOT in (' + @cols + ')
            ) P 
			LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=P.KEYPHONGBAN
            LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
            LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
            ORDER BY D.Rank, C.Rank,B.Rank, LEN(MANHANVIEN), MANHANVIEN '

EXECUTE(@query)
DROP TABLE #CHAMCONG
";

            string zFilter = "";

            if (BranchKey != 0)
            {
                zFilter += " AND [dbo].[Fn_BranchKeyWorkingHistory](EmployeeKey,@FROMDATE,@TODATE) = @BranchKey";
            }
            if (DepartmentKey != 0)
            {
                zFilter += " AND [dbo].[Fn_DepartmentKeyWorkingHistory](A.EmployeeKey,@FROMDATE,@TODATE) =@DepartmentKey";
            }
            if (TeamKey != 0)
            {
                zFilter += " AND A.TeamKey= @TeamKey";
            }
            if (Search.Trim().Length > 0)
            {

                zFilter += " AND ( A.EmployeeID LIKE @Search OR A.EmployeeName LIKE @Search )";
            }



            if (zFilter.Length != 0)
            {
                //zSQL = zSQL.Replace("@MAPHONGBAN", " WHERE KEYPHONGBAN = " + TeamKey + " ");
                zSQL = zSQL.Replace("@CustomTeam", zFilter);
            }
            else
            {
                // zSQL = zSQL.Replace("@MAPHONGBAN", "");
                zSQL = zSQL.Replace("@CustomTeam", "");
            }

            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FROMDATE", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@TODATE", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BranchKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search + "%";
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        // Báo cáo chấm công chưa chốt 18/10/2021
        public static DataTable BaoCaoCong_V02(DateTime FromDate, DateTime ToDate, int BranchKey, int DepartmentKey, int TeamKey, string Search)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            string zSQL = @"
--DECLARE @FROMDATE DATETIME = '2020-05-01 00:00:00'
--DECLARE @TODATE DATETIME = '2020-05-31 23:59:59'
DECLARE @cols NVARCHAR(MAX)
DECLARE @query NVARCHAR(MAX)

CREATE TABLE #CHAMCONG (
	MAID NVARCHAR(500), NGAYBAOCAO NVARCHAR(500), MANHANVIEN NVARCHAR(500), HOTEN NVARCHAR(500),KEYPHONGBAN INT, MAPHONGBAN NVARCHAR(500), TENPHONGBAN NVARCHAR(500), CHUCVU NVARCHAR(500), TENCOT NVARCHAR(500), GIATRI NVARCHAR(500), SAPXEP INT
)

INSERT INTO #CHAMCONG 
(
	MAID, NGAYBAOCAO, MANHANVIEN, HOTEN,KEYPHONGBAN, MAPHONGBAN, TENPHONGBAN, CHUCVU, TENCOT, GIATRI,SAPXEP
)

SELECT 
UPPER(A.EmployeeKey), 
CONVERT(NVARCHAR(100), GETDATE(),103), 
A.EmployeeID, 
UPPER(A.EmployeeName), 
A.TeamKey,
A.TeamID,
A.TeamName,
dbo.Fn_GetPositionName_FromEmployeeKey(A.EmployeeKey),
('9|'+CAST(DAY(A.DateWrite) AS NVARCHAR(50))) AS TENCOT, --9: danh sach ma cham cong 30 ngay
TimeID AS GIATRI,
0  
FROM HRM_Employee_KeepingTime A
WHERE A.DateWrite BETWEEN @FROMDATE AND @TODATE @CustomTeam


INSERT INTO #CHAMCONG 
(
	MAID, NGAYBAOCAO, MANHANVIEN, HOTEN,KEYPHONGBAN, MAPHONGBAN, TENPHONGBAN, CHUCVU, TENCOT, GIATRI, SAPXEP
)
SELECT
UPPER(A.EmployeeKey), 
CONVERT(NVARCHAR(100), GETDATE(),103), 
A.EmployeeID, 
UPPER(A.EmployeeName), 
A.TeamKey,
A.TeamID,
A.TeamName,
dbo.Fn_GetPositionName_FromEmployeeKey(A.EmployeeKey),
(CAST(A.CategoryKey AS NVARCHAR(10))+'|' +A.CodeName) AS TENCOT,
A.NUMBER AS GIATRI,
B.Rank
FROM HRM_TimeKeeping_Month A
LEFT JOIN [dbo].[HRM_CodeReport] B ON B.ID=A.CodeID
WHERE A.RecordStatus <> 99 AND  A.DateWrite BETWEEN @FROMDATE AND @TODATE 
AND B.DateWrite BETWEEN @FROMDATE AND @TODATE AND B.RecordStatus<> 99
 @CustomTeam

SET @cols = STUFF((SELECT ',' + QUOTENAME(TENCOT) FROM #CHAMCONG GROUP BY TENCOT,SAPXEP  ORDER BY SAPXEP,LEN(TENCOT), TENCOT
					FOR XML PATH(''), TYPE
					).value(N'.[1]', N'NVARCHAR(MAX)') 
				,1,1,'');
--Xóa cột sắp xếp để pivot không bị loạn
ALTER TABLE #CHAMCONG
   DROP COLUMN SAPXEP;

SET @query = '
			SELECT KEYPHONGBAN,MAPHONGBAN, TENPHONGBAN, MANHANVIEN, HOTEN, CHUCVU,' + @cols + ' 
			FROM 
            (
                SELECT * FROM #CHAMCONG  --@MAPHONGBAN
			) X
            PIVOT 
            (
                 MAX(GIATRI)
				 FOR TENCOT in (' + @cols + ')
            ) P 
			LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=P.KEYPHONGBAN
            LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
            LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
            ORDER BY D.Rank, C.Rank,B.Rank, LEN(MANHANVIEN), MANHANVIEN '

EXECUTE(@query)
DROP TABLE #CHAMCONG
";

            string zFilter = "";

            if (BranchKey != 0)
            {
                zFilter += " AND [dbo].[Fn_BranchKeyWorkingHistory](EmployeeKey,@FROMDATE,@TODATE) = @BranchKey";
            }
            if (DepartmentKey != 0)
            {
                zFilter += " AND [dbo].[Fn_DepartmentKeyWorkingHistory](A.EmployeeKey,@FROMDATE,@TODATE) =@DepartmentKey";
            }
            if (TeamKey != 0)
            {
                zFilter += " AND A.TeamKey= @TeamKey";
            }
            if (Search.Trim().Length > 0)
            {

                zFilter += " AND ( A.EmployeeID LIKE @Search OR A.EmployeeName LIKE @Search )";
            }



            if (zFilter.Length != 0)
            {
                //zSQL = zSQL.Replace("@MAPHONGBAN", " WHERE KEYPHONGBAN = " + TeamKey + " ");
                zSQL = zSQL.Replace("@CustomTeam", zFilter);
            }
            else
            {
                // zSQL = zSQL.Replace("@MAPHONGBAN", "");
                zSQL = zSQL.Replace("@CustomTeam", "");
            }

            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FROMDATE", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@TODATE", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BranchKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search + "%";
                zCommand.CommandTimeout = 750;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        // Báo cáo chấm công đã chốt
        public static DataTable BaoCaoCong_DaChot(DateTime FromDate, DateTime ToDate, int BranchKey, int DepartmentKey, int TeamKey, string Search)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            string zSQL = @"
--DECLARE @FROMDATE DATETIME = '2020-05-01 00:00:00'
--DECLARE @TODATE DATETIME = '2020-05-31 23:59:59'
DECLARE @cols NVARCHAR(MAX)
DECLARE @query NVARCHAR(MAX)

CREATE TABLE #CHAMCONG (
	MAID NVARCHAR(500), NGAYBAOCAO NVARCHAR(500), MANHANVIEN NVARCHAR(500), HOTEN NVARCHAR(500),KEYPHONGBAN INT, MAPHONGBAN NVARCHAR(500), TENPHONGBAN NVARCHAR(500), CHUCVU NVARCHAR(500), TENCOT NVARCHAR(500), GIATRI NVARCHAR(500), SAPXEP INT
)

INSERT INTO #CHAMCONG 
(
	MAID, NGAYBAOCAO, MANHANVIEN, HOTEN,KEYPHONGBAN, MAPHONGBAN, TENPHONGBAN, CHUCVU, TENCOT, GIATRI,SAPXEP
) 

SELECT 
UPPER(A.EmployeeKey), 
CONVERT(NVARCHAR(100), GETDATE(),103), 
A.EmployeeID, 
UPPER(A.EmployeeName), 
A.TeamKey,
A.TeamID,
A.TeamName,
dbo.Fn_GetPositionName_FromEmployeeKey(A.EmployeeKey),
DAY(A.DateWrite) AS TENCOT, 
TimeID AS GIATRI,
0
FROM HRM_Employee_KeepingTime A
WHERE A.DateWrite BETWEEN @FROMDATE AND @TODATE @CustomTeam 


INSERT INTO #CHAMCONG 
(
	MAID, NGAYBAOCAO, MANHANVIEN, HOTEN,KEYPHONGBAN, MAPHONGBAN, TENPHONGBAN, CHUCVU, TENCOT, GIATRI, SAPXEP
)
SELECT
UPPER(A.EmployeeKey), 
CONVERT(NVARCHAR(100), GETDATE(),103), 
A.EmployeeID, 
UPPER(A.EmployeeName), 
A.TeamKey,
A.TeamID,
A.TeamName,
dbo.Fn_GetPositionName_FromEmployeeKey(A.EmployeeKey),
A.CodeName AS TENCOT,
A.NUMBER AS GIATRI,
B.Rank
FROM HRM_TimeKeeping_Month_Close A
LEFT JOIN [dbo].[HRM_CodeReport] B ON B.ID=A.CodeID
WHERE A.DateWrite BETWEEN @FROMDATE AND @TODATE AND A.Parent!=0  @CustomTeam 

SET @cols = STUFF((SELECT ',' + QUOTENAME(TENCOT) FROM #CHAMCONG GROUP BY TENCOT,SAPXEP ORDER BY SAPXEP,LEN(TENCOT), TENCOT
					FOR XML PATH(''), TYPE
					).value(N'.[1]', N'NVARCHAR(MAX)') 
				,1,1,'');
--Xóa cột sắp xếp để pivot không bị loạn
ALTER TABLE #CHAMCONG
   DROP COLUMN SAPXEP;

SET @query = '
			SELECT KEYPHONGBAN,MAPHONGBAN, TENPHONGBAN, MANHANVIEN, HOTEN, CHUCVU,' + @cols + ' 
			FROM 
            (
                SELECT * FROM #CHAMCONG  --@MAPHONGBAN
			) X
            PIVOT 
            (
                 MAX(GIATRI)
				 FOR TENCOT in (' + @cols + ')
            ) P 
			LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=P.KEYPHONGBAN
            LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
            LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
            ORDER BY D.Rank, C.Rank,B.Rank, LEN(MANHANVIEN), MANHANVIEN '

EXECUTE(@query)
DROP TABLE #CHAMCONG
";

            string zFilter = "";

            if (BranchKey != 0)
            {
                zFilter += " AND [dbo].[Fn_BranchKeyWorkingHistory](EmployeeKey,@FROMDATE,@TODATE) = @BranchKey";
            }
            if (DepartmentKey != 0)
            {
                zFilter += " AND [dbo].[Fn_DepartmentKeyWorkingHistory](A.EmployeeKey,@FROMDATE,@TODATE) =@DepartmentKey";
            }
            if (TeamKey != 0)
            {
                zFilter += " AND A.TeamKey= @TeamKey";
            }
            if (Search.Trim().Length > 0)
            {

                zFilter += " AND ( A.EmployeeID LIKE @Search OR A.EmployeeName LIKE @Search )";
            }



            if (zFilter.Length != 0)
            {
                //zSQL = zSQL.Replace("@MAPHONGBAN", " WHERE KEYPHONGBAN = " + TeamKey + " ");
                zSQL = zSQL.Replace("@CustomTeam", zFilter);
            }
            else
            {
                // zSQL = zSQL.Replace("@MAPHONGBAN", "");
                zSQL = zSQL.Replace("@CustomTeam", " ");
            }

            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FROMDATE", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@TODATE", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BranchKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search + "%";
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        // Báo cáo chấm công đã chốt 18/10/2021
        public static DataTable BaoCaoCong_DaChot_V02(DateTime FromDate, DateTime ToDate, int BranchKey, int DepartmentKey, int TeamKey, string Search)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            string zSQL = @"
--DECLARE @FROMDATE DATETIME = '2020-05-01 00:00:00'
--DECLARE @TODATE DATETIME = '2020-05-31 23:59:59'
DECLARE @cols NVARCHAR(MAX)
DECLARE @query NVARCHAR(MAX)

CREATE TABLE #CHAMCONG (
	MAID NVARCHAR(500), NGAYBAOCAO NVARCHAR(500), MANHANVIEN NVARCHAR(500), HOTEN NVARCHAR(500),KEYPHONGBAN INT, MAPHONGBAN NVARCHAR(500), TENPHONGBAN NVARCHAR(500), CHUCVU NVARCHAR(500), TENCOT NVARCHAR(500), GIATRI NVARCHAR(500), SAPXEP INT
)

INSERT INTO #CHAMCONG 
(
	MAID, NGAYBAOCAO, MANHANVIEN, HOTEN,KEYPHONGBAN, MAPHONGBAN, TENPHONGBAN, CHUCVU, TENCOT, GIATRI,SAPXEP
)

SELECT 
UPPER(A.EmployeeKey), 
CONVERT(NVARCHAR(100), GETDATE(),103), 
A.EmployeeID, 
UPPER(A.EmployeeName), 
A.TeamKey,
A.TeamID,
A.TeamName,
dbo.Fn_GetPositionName_FromEmployeeKey(A.EmployeeKey),
('9|'+CAST(DAY(A.DateWrite) AS NVARCHAR(50))) AS TENCOT, --9: danh sach ma cham cong 30 ngay
TimeID AS GIATRI,
0  
FROM HRM_Employee_KeepingTime A
WHERE A.DateWrite BETWEEN @FROMDATE AND @TODATE @CustomTeam


INSERT INTO #CHAMCONG 
(
	MAID, NGAYBAOCAO, MANHANVIEN, HOTEN,KEYPHONGBAN, MAPHONGBAN, TENPHONGBAN, CHUCVU, TENCOT, GIATRI, SAPXEP
)
SELECT
UPPER(A.EmployeeKey), 
CONVERT(NVARCHAR(100), GETDATE(),103), 
A.EmployeeID, 
UPPER(A.EmployeeName), 
A.TeamKey,
A.TeamID,
A.TeamName,
dbo.Fn_GetPositionName_FromEmployeeKey(A.EmployeeKey),
(CAST(A.CategoryKey AS NVARCHAR(10))+'|' +A.CodeName) AS TENCOT,
A.NUMBER AS GIATRI,
B.Rank
FROM HRM_TimeKeeping_Month_Close A
LEFT JOIN [dbo].[HRM_CodeReport] B ON B.ID=A.CodeID
WHERE A.RecordStatus <> 99 AND  A.DateWrite BETWEEN @FROMDATE AND @TODATE 
AND B.DateWrite BETWEEN @FROMDATE AND @TODATE AND B.RecordStatus<> 99
 @CustomTeam

SET @cols = STUFF((SELECT ',' + QUOTENAME(TENCOT) FROM #CHAMCONG GROUP BY TENCOT,SAPXEP  ORDER BY SAPXEP,LEN(TENCOT), TENCOT
					FOR XML PATH(''), TYPE
					).value(N'.[1]', N'NVARCHAR(MAX)') 
				,1,1,'');
--Xóa cột sắp xếp để pivot không bị loạn
ALTER TABLE #CHAMCONG
   DROP COLUMN SAPXEP;

SET @query = '
			SELECT KEYPHONGBAN,MAPHONGBAN, TENPHONGBAN, MANHANVIEN, HOTEN, CHUCVU,' + @cols + ' 
			FROM 
            (
                SELECT * FROM #CHAMCONG  --@MAPHONGBAN
			) X
            PIVOT 
            (
                 MAX(GIATRI)
				 FOR TENCOT in (' + @cols + ')
            ) P 
			LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=P.KEYPHONGBAN
            LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
            LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
            ORDER BY D.Rank, C.Rank,B.Rank, LEN(MANHANVIEN), MANHANVIEN '

EXECUTE(@query)
DROP TABLE #CHAMCONG
";

            string zFilter = "";

            if (BranchKey != 0)
            {
                zFilter += " AND [dbo].[Fn_BranchKeyWorkingHistory](EmployeeKey,@FROMDATE,@TODATE) = @BranchKey";
            }
            if (DepartmentKey != 0)
            {
                zFilter += " AND [dbo].[Fn_DepartmentKeyWorkingHistory](A.EmployeeKey,@FROMDATE,@TODATE) =@DepartmentKey";
            }
            if (TeamKey != 0)
            {
                zFilter += " AND A.TeamKey= @TeamKey";
            }
            if (Search.Trim().Length > 0)
            {

                zFilter += " AND ( A.EmployeeID LIKE @Search OR A.EmployeeName LIKE @Search )";
            }



            if (zFilter.Length != 0)
            {
                //zSQL = zSQL.Replace("@MAPHONGBAN", " WHERE KEYPHONGBAN = " + TeamKey + " ");
                zSQL = zSQL.Replace("@CustomTeam", zFilter);
            }
            else
            {
                // zSQL = zSQL.Replace("@MAPHONGBAN", "");
                zSQL = zSQL.Replace("@CustomTeam", " ");
            }

            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FROMDATE", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@TODATE", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BranchKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search + "%";
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        #region[báo cáo ngày]
        public static DataTable BaoCaoThoiGian(DateTime FromDate, DateTime ToDate, int BranchKey, int DepartmentKey, int TeamKey, string Search)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string PivotDate = "";
            for (int i = FromDate.Day; i <= ToDate.Day; i++)
            {
                PivotDate += "[" + i + "],";
            }
            if (PivotDate.Contains(","))
            {
                PivotDate = PivotDate.Remove(PivotDate.LastIndexOf(","));
            }


            string zSQL = @"
--DECLARE @FromDate Datetime = '2020-05-01 00:00:00'
--DECLARE @ToDate Datetime = '2020-05-31 23:59:59'

			SELECT MANHANVIEN, HOTEN, MAPHONGBAN, TENPHONGBAN, @cols  FROM 
            (
                SELECT 
				DAY(DateWrite) AS NGAY, 
				EmployeeID AS MANHANVIEN,
                EmployeeName AS HOTEN,
                TeamID AS MAPHONGBAN,
                dbo.Fn_GetTeamName(TeamKey) AS TENPHONGBAN,
				REPLACE(TotalTime, '00:00', '') AS THOIGIAN
				FROM HRM_Employee_KeepingTime 
				WHERE RecordStatus <> 99 AND DateWrite BETWEEN  @FromDate AND @ToDate 	@CustomTeam 		
            ) X
            PIVOT 
            (
                 MAX(THOIGIAN)
				 FOR NGAY in (@cols)
            ) P ORDER BY TENPHONGBAN, LEN(MANHANVIEN), MANHANVIEN";
            zSQL = zSQL.Replace("@cols", PivotDate);

            string zFilter = "";

            if (BranchKey != 0)
            {
                zFilter += " AND [dbo].[Fn_BranchKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate) = @BranchKey";
            }
            if (DepartmentKey != 0)
            {
                zFilter += " AND [dbo].[Fn_DepartmentKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate) =@DepartmentKey";
            }
            if (TeamKey != 0)
            {
                zFilter += " AND [dbo].[Fn_TeamKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate)= @TeamKey";
            }
            if (Search.Trim().Length > 0)
            {

                zFilter += " AND (  EmployeeID LIKE @Search OR EmployeeName LIKE @Search )";
            }
            if (zFilter.Length != 0)
            {
                zSQL = zSQL.Replace("@CustomTeam", zFilter);
            }
            else
            {
                zSQL = zSQL.Replace("@CustomTeam", "");
            }
            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BranchKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search + "%";
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable BaoCaoLuongKhoan(DateTime FromDate, DateTime ToDate, int TeamKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            string zSQL = @"
--DECLARE @FromDate NVARCHAR(100) = '2020-05-01 00:00:00'
--DECLARE @ToDate NVARCHAR(100) = '2020-05-31 23:59:59'
DECLARE @cols NVARCHAR(MAX)
DECLARE @query NVARCHAR(MAX)

CREATE TABLE #BCNGAY (
	MAID NVARCHAR(500), 
	NGAYBAOCAO NVARCHAR(500), 
	MANHANVIEN NVARCHAR(500), 
	HOTEN NVARCHAR(500), 
	MAPHONGBAN NVARCHAR(500), 
	TENPHONGBAN NVARCHAR(500), 
	CHUCVU NVARCHAR(500), 
	TENCOT NVARCHAR(500), 
	GIATRI FLOAT
)

INSERT INTO #BCNGAY 
(
	MAID, NGAYBAOCAO, MANHANVIEN, HOTEN, MAPHONGBAN, TENPHONGBAN, TENCOT, GIATRI
)
SELECT 
NEWID(), CONVERT(NVARCHAR(100), GETDATE(),103), 
X.EmployeeID, X.EmployeeName, 
DBO.Fn_GetTeamID(X.TeamKey) AS TeamID, 
DBO.Fn_GetTeamName(X.TeamKey) AS TeamName, 
X.NGAY, X.[Money]
FROM
(
	SELECT
	dbo.Fn_GetOptionChangeTeamKey(EmployeeKey,@FromDate) AS TeamKey,
	EmployeeName,
	EmployeeID,
	Day(DateImport) NGAY,
	[Money]
	FROM Temp_Import_Detail
	WHERE RecordStatus <> 99 
	AND (DateImport BETWEEN @FromDate AND @ToDate)
) X
WHERE 1=1 
@ParamTeam
INSERT INTO #BCNGAY 
(
	MAID, NGAYBAOCAO, MANHANVIEN, HOTEN, MAPHONGBAN, TENPHONGBAN, TENCOT, GIATRI
)
SELECT 
NEWID(), CONVERT(NVARCHAR(100), GETDATE(),103), 
EmployeeID, 
EmployeeName, 
[dbo].[Fn_GetTeamID](TeamKey) AS TeamID,
[dbo].[Fn_GetTeamName](TeamKey) AS TeamName,
DAY(Date) AS NGAY, 
Total AS [MONEY] 
FROM FTR_Money_Date 
WHERE RecordStatus <> 99 
AND ([Date] BETWEEN @FromDate AND @ToDate)
@ParamTeam

SET @cols = STUFF((SELECT ',' + QUOTENAME(TENCOT) FROM #BCNGAY GROUP BY TENCOT ORDER BY LEN(TENCOT), TENCOT
					FOR XML PATH(''), TYPE
					).value(N'.[1]', N'NVARCHAR(MAX)') 
				,1,1,'');

SET @query = '
			SELECT MANHANVIEN, HOTEN, MAPHONGBAN, TENPHONGBAN,' + @cols + ' 
			FROM 
            (
                SELECT NGAYBAOCAO, MANHANVIEN, HOTEN, MAPHONGBAN, TENPHONGBAN, TENCOT, SUM(GIATRI) AS GIATRI
				FROM #BCNGAY
				GROUP BY NGAYBAOCAO, MANHANVIEN, HOTEN, MAPHONGBAN, TENPHONGBAN, TENCOT
			) X
            PIVOT 
            (
                 SUM(GIATRI)
				 FOR TENCOT in (' + @cols + ')
            ) P ORDER BY TENPHONGBAN, LEN(MANHANVIEN), MANHANVIEN'

EXECUTE(@query)
DROP TABLE #BCNGAY
";
            if (TeamKey != 0)
            {
                zSQL = zSQL.Replace("@ParamTeam", " AND TeamKey = @TeamKey");
            }
            else
            {
                zSQL = zSQL.Replace("@ParamTeam", "");
            }

            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }

        #endregion
        //Cáo cáo lương khoán ngày tại tổ có người mượn
        public static DataTable No21(DateTime FromDate, DateTime ToDate, int TeamKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string PivotDate = "";
            for (int i = FromDate.Day; i <= ToDate.Day; i++)
            {
                PivotDate += "[" + i + "],";
            }
            if (PivotDate.Contains(","))
            {
                PivotDate = PivotDate.Remove(PivotDate.LastIndexOf(","));
            }

            DataTable zTable = new DataTable();
            string zSQL = @"
;WITH Temp AS(
SELECT * FROM (
	SELECT 
		DAY(B.OrderDate) AS [Time],
		[dbo].[Fn_TeamKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate) AS TeamKeyEmployee,     
		dbo.Fn_GetTeamID([dbo].[Fn_TeamKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate)) AS TeamID,
		dbo.Fn_GetTeamName([dbo].[Fn_TeamKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate)) AS TeamName,
		A.TeamKey, 
		A.EmployeeKey,
		A.EmployeeName, 
		A.EmployeeID,
		ISNULL(SUM((A.[Money] + A.Money_Borrow+A.MoneyPrivate+A.Money_Eat)),0) AS [LK],
		CASE 
			WHEN  (A.Category=1  OR A.CategoryPrivate=1) THEN 0
			WHEN  (A.Category !=1 AND A.CategoryPrivate!=1) THEN 1
		END AS [TYPE]
		FROM FTR_Order_Money A
		LEFT JOIN FTR_Order B ON B.OrderKey=A.OrderKey
		WHERE A.RecordStatus <> 99
        AND B.RecordStatus <> 99 
		AND A.OrderDate BETWEEN @FromDate AND @ToDate
		AND A.TeamKey = @TeamKey
	   GROUP BY DAY(B.OrderDate),
	   [dbo].[Fn_TeamKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate),
	   dbo.Fn_GetTeamID([dbo].[Fn_TeamKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate)) ,
	   dbo.Fn_GetTeamName([dbo].[Fn_TeamKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate)) ,
	   A.TeamKey,
	   A.EmployeeKey,
	   A.EmployeeName, 
	   A.EmployeeID,
	   A.Category,A.CategoryPrivate
) X
 PIVOT (
    SUM(LK)
	FOR [Time] IN (@CustomParamater)) P
)

SELECT A.*, D.RANK BranchRank,C.RANK AS DepartmentRank, B.RANK AS TeamRank
FROM Temp  A
LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKeyEmployee
LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
ORDER BY A.[TYPE], D.RANK,C.RANK,B.RANK,LEN(A.EmployeeID), A.EmployeeID";

            zSQL = zSQL.Replace("@CustomParamater", PivotDate);
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }

        #region[Báo cáo thu nhập]
        //Thu Hiển thị theo khối--nhóm mặc định
        public static DataTable TONGTHUNHAP_KHOI(DateTime FromDate, DateTime ToDate, int BranchKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);

            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, 1, 0, 0, 0);
            zToDate = zToDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            string Fillter = "";

            if (BranchKey > 0)
            {
                Fillter += " AND BranchKey = @BranchKey";
            }
            DataTable zTable = new DataTable();
            string zSQL = @"
--declare @FromDate datetime ='2020-01-01 00:00:00'
--declare @ToDate datetime ='2020-12-31 23:59:59'
--declare @TeamKey int = 79

CREATE TABLE Temp
(	
	ID NVARCHAR(50),
	[NOIDUNG] NVARCHAR(500) ,
	[DateView] DATETIME,
	[BranchKey] INT,
	[Amount] MONEY
)
--Dòng 1 tổng lương ------------------------------------------
        --//Tổng cộng công nhân
        INSERT INTO Temp
        SELECT 'TLSX', N'01.Tổng Lương',
        CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_ReportWorker_Close]
        WHERE CodeID ='TCSX' 
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey)
        --Tổng lương hỗ trợ
        INSERT INTO Temp
        SELECT'TLSX', N'01.Tổng Lương',
        CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_ReportSupport_Close]
        WHERE CodeID ='TCSX' 
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey)
        --Tổng lương văn phòng
        INSERT INTO Temp
        SELECT 'TLSX',N'01.Tổng Lương',
        CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_ReportOffice_Close]
        WHERE CodeID ='TCSX' 
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey)
--Dòng 2 Lương tháng 13------------------------------------------------
        INSERT INTO Temp
        SELECT 'TL13',N'02.Lương tháng 13',
        CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_Salary13_Detail]
        WHERE (Class ='TU' OR Class='CT' )
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey)
--Dòng 3 Thưởng theo Kết quả kinh doanh------------------------------------------------
        INSERT INTO Temp
        SELECT 'TLKD',N'03.Thưởng theo Kết quả kinh doanh',
        CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_SalaryBusiness_Detail]
        WHERE Class ='TU' 
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey)
--Dòng 4 Các khoản trích theo lương (chi phí công ty)------------------------------------------------
         --Sản xuất
        INSERT INTO Temp
        SELECT'TLTL', N'04.Các khoản trích theo lương (chi phí công ty)',
        CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        BranchKey,
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_ReportWorker_Close]
        WHERE  RecordStatus <> 99 AND Parent IS NOT NULL
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND (CodeID='XHCT' OR CodeID='YTCT' OR CodeID='NVCT' OR CodeID='CDCT')
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),BranchKey
        --Hỗ trợ
        INSERT INTO Temp
         SELECT'TLTL', N'04.Các khoản trích theo lương (chi phí công ty)',
        CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        BranchKey,
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_ReportSupport_Close]
        WHERE  RecordStatus <> 99 AND Parent IS NOT NULL
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND (CodeID='XHCT' OR CodeID='YTCT' OR CodeID='NVCT' OR CodeID='CDCT')
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),BranchKey
        --Văp phòng
        INSERT INTO Temp
        SELECT'TLTL', N'04.Các khoản trích theo lương (chi phí công ty)',
        CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        BranchKey,
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_ReportOffice_Close]
        WHERE  RecordStatus <> 99 AND Parent IS NOT NULL
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND (CodeID='XHCT' OR CodeID='YTCT' OR CodeID='NVCT' OR CodeID='CDCT')
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),BranchKey
       
--Dòng 5 Hỗ trợ thấp điểm
        --Công nhân
        INSERT INTO Temp
        SELECT 'HTTD',N'05.Hỗ trợ thấp điểm',
        CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_ReportWorker_Close]
        WHERE CodeID ='HTTD' 
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey)
        --khối hỗ trợ
        INSERT INTO Temp
        SELECT'HTTD', N'05.Hỗ trợ thấp điểm',
        CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_ReportSupport_Close]
        WHERE CodeID ='HTTD' 
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey)
        --khối văn phòng
        INSERT INTO Temp
        SELECT 'HTTD',N'05.Hỗ trợ thấp điểm',
        CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_ReportOffice_Close]
        WHERE CodeID ='HTTD' 
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey)

--Dòng 6 Tiền cơm bảng lương
        --Công nhân
        INSERT INTO Temp
        SELECT 'HTCO',N'06.Phụ cấp Cơm trưa + Cơm tăng ca',
        CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_ReportWorker_Close]
        WHERE CategoryKey = 6 --(CodeID ='STCC' OR CodeID='STCU')
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey)
        --khối hỗ trợ
        INSERT INTO Temp
        SELECT  'HTCO',N'06.Phụ cấp Cơm trưa + Cơm tăng ca',
        CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_ReportSupport_Close]
        WHERE CategoryKey = 6 --(CodeID ='STCC' OR CodeID='STCU')
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey)
        --khối văn phòng
        INSERT INTO Temp
        SELECT  'HTCO',N'06.Phụ cấp Cơm trưa + Cơm tăng ca',
        CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_ReportOffice_Close]
        WHERE CategoryKey = 6 --(CodeID ='STCC' OR CodeID='STCU')
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey)
-- Dòng 7 Phụ cấp Cơm trưa (trả cho đơn vị nấu)
        INSERT INTO Temp
        SELECT 'CCTY',N'07.Phụ cấp Cơm trưa (trả cho đơn vị nấu)',
        CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        Round(SUM(Money1+Money2+Money3),0) AS Amount
        FROM [dbo].[FNC_RiceTeamCompany]
        WHERE RecordStatus <> 99
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey)
-- Dòng 8 Tổng thu nhập (Lương, thưởng, phụ cấp)
        INSERT INTO Temp
        SELECT  'TLPC',N'08.Tổng thu nhập (Lương, thưởng, phụ cấp)',
        CONVERT(datetime,CONCAT(YEAR([DateView]),'/',MONTH([DateView]),'/1'),120),
        BranchKey,
        Round(SUM(Amount),0) AS Amount
        FROM Temp
        WHERE  [DateView] BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        AND (ID='TLSX'OR ID='TL13' OR ID='TLKD' OR ID='HTTD'OR ID='HTCO' OR ID='TLTL')
        GROUP BY MONTH([DateView]),YEAR([DateView]),BranchKey

--Dòng 9 Số lượng lao động
        --Công nhân
        INSERT INTO Temp
        SELECT 'SLLD', N'09.Số lượng lao động',CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        COUNT(EmployeeKey) AS Amount
        FROM [dbo].[SLR_ReportWorker_Close]
        WHERE CodeID ='STTL' AND RecordStatus <> 99
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey)
        --Hô tro
        INSERT INTO Temp
        SELECT 'SLLD', N'09.Số lượng lao động',CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        COUNT(EmployeeKey) AS Amount
        FROM [dbo].[SLR_ReportSupport_Close]
        WHERE CodeID ='STTL' AND RecordStatus <> 99
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey)
        --van phòng
        INSERT INTO Temp
        SELECT 'SLLD', N'09.Số lượng lao động',CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        COUNT(EmployeeKey) AS Amount
        FROM [dbo].[SLR_ReportOffice_Close]
        WHERE CodeID ='STTL' AND RecordStatus <> 99
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey)
--Dòng 10 Số giờ làm việc
        --Số giờ làm việc công nhân
        INSERT INTO Temp
        SELECT 'SGLV',N'10.Số giờ làm việc', 
        CONVERT(datetime,CONCAT(YEAR(DateImport),'/',MONTH(DateImport),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        ROUND(((SUM(CAST (LEFT(TotalTime ,2) AS MONEY)) *60 + SUM(CAST (Right(TotalTime ,2) AS MONEY))) /60),0) AS Amount
        FROM [dbo].[Temp_Import_Detail]
        WHERE RecordStatus <> 99 
        AND DateImport BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY MONTH(DateImport),YEAR(DateImport),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey)

        --Số giờ làm nhóm hỗ trợ
        INSERT INTO Temp
        SELECT  'SGLV',N'10.Số giờ làm việc',
        CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        ROUND(((SUM(CAST (LEFT(TotalTime ,2) AS MONEY)) *60 + SUM(CAST (Right(TotalTime ,2) AS MONEY))) /60),0) AS Amount
        FROM[dbo].[HRM_Employee_KeepingTime]
        WHERE RecordStatus <> 99 
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        AND [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey)=26
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey)
        --Số giờ làm việc khối văn phòng
        INSERT INTO Temp
        SELECT  'SGLV',N'10.Số giờ làm việc',
        CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        ROUND(((SUM(CAST (LEFT(TotalTime ,2) AS MONEY)) *60 + SUM(CAST (Right(TotalTime ,2) AS MONEY))) /60),0) AS Amount
        FROM[dbo].[HRM_Employee_KeepingTime]
        WHERE RecordStatus <> 99 
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        AND [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey)= 2
        AND [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey) <>26
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey)

--Dong 11 số ngày công
        INSERT INTO Temp
        SELECT 'SCTT', N'11.Ngày công làm việc',CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        SUM(Number) AS Amount
        FROM [dbo].[HRM_TimeKeeping_Month_Close]
        WHERE RecordStatus <> 99 AND CodeID='SCTT'
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey)

--Dòng 12 số tiền trung bình
        ;WITH G AS (
	        SELECT DateView AS [DATE],
	        BranchKey,
	        Amount AS TLPC,0 AS SLLD
	        FROM Temp
	        WHERE  [DateView] BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
	        AND ID='TLPC'
        UNION 
	        SELECT DateView AS [DATE],
	        BranchKey,
	        0 AS TLPC  ,Amount AS SLLD
	        FROM Temp
	        WHERE  [DateView] BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
	        AND ID='SLLD'
        )
        INSERT INTO Temp
        SELECT  'STTB',N'12.Thu nhập trung bình (VNĐ/Người)',[DATE],BranchKey,
	        CASE WHEN SUM(SLLD)>0 THEN ROUND(SUM(TLPC)/SUM(SLLD),0) 
	        ELSE 0
	        END Amount
        FROM G
        GROUP BY [DATE],BranchKey
--//Kết thúc dòng 12
--Tổng cộng tất cả các bộ phận
        ;WITH K AS 
        (
        SELECT NOIDUNG,CONCAT(MONTH([DateView]),'/',YEAR([DateView])) AS [DATE],
        CAST(BranchKey AS NVARCHAR(10)) AS BranchKey,
        CAST(Amount AS money)AS Amount
        FROM Temp WHERE 1=1 @Parameter
	        UNION ALL
	        SELECT
	        NOIDUNG,
	        '99/9999'[DATE],
	        BranchKey,
	        SUM(Amount) AS Amount
	        FROM Temp WHERE 1=1 @Parameter
	        GROUP BY NOIDUNG,BranchKey
        ) 
        SELECT LeftColumn,HeaderColumn,Amount FROM(
	        SELECT B.NOIDUNG AS LeftColumn,[DATE]+'|'+CAST(B.BranchKey AS NVARCHAR(10))+'|'+[dbo].[Fn_GetBranchName](B.BranchKey) AS HeaderColumn,Amount,[DATE],
	        D.Rank AS BranchRank
	        FROM K B
	        LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=B.BranchKey
            
         ) X
         ORDER BY  LEN([DATE]),[DATE],BranchRank

DROP TABLE Temp";
            zSQL = zSQL.Replace("@Parameter", Fillter);

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BranchKey;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        //Thu Hiển thị theo bộ phận-- nhóm mặc đinh
        public static DataTable TONGTHUNHAP_BOPHAN_MACDINH(DateTime FromDate, DateTime ToDate, int BranchKey, int DepartmentKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);

            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, 1, 0, 0, 0);
            zToDate = zToDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            string Fillter = "";

            if (BranchKey > 0)
            {
                Fillter += " AND BranchKey = @BranchKey";
            }
            if (DepartmentKey > 0)
                Fillter += " AND DepartmentKey =@DepartmentKey ";

            DataTable zTable = new DataTable();
            string zSQL = @"
--declare @FromDate datetime ='2020-01-01 00:00:00'
--declare @ToDate datetime ='2020-12-31 23:59:59'
--declare @TeamKey int = 79

CREATE TABLE Temp
(	
	ID NVARCHAR(50),
	[NOIDUNG] NVARCHAR(500) ,
	[DateView] DATETIME,
	[BranchKey] INT,
	[DepartmentKey] INT,
	[Amount] MONEY
)
--Dòng 1 tổng lương ------------------------------------------
        --//Tổng cộng công nhân
        INSERT INTO Temp
        SELECT 'TLSX', N'01.Tổng Lương',
        CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_ReportWorker_Close]
        WHERE CodeID ='TCSX' 
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey)
        --Tổng lương hỗ trợ
        INSERT INTO Temp
        SELECT'TLSX', N'01.Tổng Lương',
        CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_ReportSupport_Close]
        WHERE CodeID ='TCSX' 
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey)
        --Tổng lương văn phòng
        INSERT INTO Temp
        SELECT 'TLSX',N'01.Tổng Lương',
        CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_ReportOffice_Close]
        WHERE CodeID ='TCSX' 
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey)
--Dòng 2 Lương tháng 13------------------------------------------------
        INSERT INTO Temp
        SELECT 'TL13',N'02.Lương tháng 13',
        CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_Salary13_Detail]
        WHERE (Class ='TU' OR Class='CT' )
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey)
--Dòng 3 Thưởng theo Kết quả kinh doanh------------------------------------------------
        INSERT INTO Temp
        SELECT 'TLKD',N'03.Thưởng theo Kết quả kinh doanh',
        CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_SalaryBusiness_Detail]
        WHERE Class ='TU' 
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey)
--Dòng 4 Các khoản trích theo lương (chi phí công ty)------------------------------------------------
          --Sản xuất
        INSERT INTO Temp
        SELECT'TLTL', N'04.Các khoản trích theo lương (chi phí công ty)',
        CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        BranchKey,DepartmentKey,
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_ReportWorker_Close]
        WHERE  RecordStatus <> 99 AND Parent IS NOT NULL
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND (CodeID='XHCT' OR CodeID='YTCT' OR CodeID='NVCT' OR CodeID='CDCT')
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),BranchKey,DepartmentKey
        --Hỗ trợ
        INSERT INTO Temp
         SELECT'TLTL', N'04.Các khoản trích theo lương (chi phí công ty)',
        CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        BranchKey,DepartmentKey,
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_ReportSupport_Close]
        WHERE  RecordStatus <> 99 AND Parent IS NOT NULL
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND (CodeID='XHCT' OR CodeID='YTCT' OR CodeID='NVCT' OR CodeID='CDCT')
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),BranchKey,DepartmentKey
        --Văp phòng
        INSERT INTO Temp
         SELECT'TLTL', N'04.Các khoản trích theo lương (chi phí công ty)',
        CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        BranchKey,DepartmentKey,
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_ReportOffice_Close]
        WHERE  RecordStatus <> 99 AND Parent IS NOT NULL
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND (CodeID='XHCT' OR CodeID='YTCT' OR CodeID='NVCT' OR CodeID='CDCT')
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),BranchKey,DepartmentKey

--Dòng 5 Hỗ trợ thấp điểm
        --Công nhân
        INSERT INTO Temp
        SELECT 'HTTD',N'05.Hỗ trợ thấp điểm',
        CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_ReportWorker_Close]
        WHERE CodeID ='HTTD' 
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey)
        --khối hỗ trợ
        INSERT INTO Temp
        SELECT'HTTD', N'05.Hỗ trợ thấp điểm',
        CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_ReportSupport_Close]
        WHERE CodeID ='HTTD' 
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey)
        --khối văn phòng
        INSERT INTO Temp
        SELECT 'HTTD',N'05.Hỗ trợ thấp điểm',
        CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_ReportOffice_Close]
        WHERE CodeID ='HTTD' 
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey)

--Dòng 6 Tiền cơm bảng lương
        --Công nhân
        INSERT INTO Temp
        SELECT 'HTCO',N'06.Phụ cấp Cơm trưa + Cơm tăng ca',
        CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_ReportWorker_Close]
        WHERE CategoryKey = 6 --(CodeID ='STCC' OR CodeID='STCU')
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey)
        --khối hỗ trợ
        INSERT INTO Temp
        SELECT  'HTCO',N'06.Phụ cấp Cơm trưa + Cơm tăng ca',
        CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_ReportSupport_Close]
        WHERE CategoryKey =6 --(CodeID ='STCC' OR CodeID='STCU')
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey)
        --khối văn phòng
        INSERT INTO Temp
        SELECT  'HTCO',N'06.Phụ cấp Cơm trưa + Cơm tăng ca',
        CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_ReportOffice_Close]
        WHERE CategoryKey = 6 --(CodeID ='STCC' OR CodeID='STCU')
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey)
-- Dòng 7 Phụ cấp Cơm trưa (trả cho đơn vị nấu)
        INSERT INTO Temp
        SELECT 'CCTY',N'07.Phụ cấp Cơm trưa (trả cho đơn vị nấu)',
        CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
        Round(SUM(Money1+Money2+Money3),0) AS Amount
        FROM [dbo].[FNC_RiceTeamCompany]
        WHERE RecordStatus <> 99
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey)
-- Dòng 8 Tổng thu nhập (Lương, thưởng, phụ cấp)
        INSERT INTO Temp
        SELECT  'TLPC',N'08.Tổng thu nhập (Lương, thưởng, phụ cấp)',
        CONVERT(datetime,CONCAT(YEAR([DateView]),'/',MONTH([DateView]),'/1'),120),
        BranchKey,
        DepartmentKey,
        Round(SUM(Amount),0) AS Amount
        FROM Temp
        WHERE  [DateView] BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        AND (ID='TLSX'OR ID='TL13' OR ID='TLKD' OR ID='HTTD'OR ID='HTCO' OR ID='TLTL')
        GROUP BY MONTH([DateView]),YEAR([DateView]),BranchKey,DepartmentKey

--Dòng 9 Số lượng lao động
        --Công nhân
        INSERT INTO Temp
        SELECT 'SLLD', N'09.Số lượng lao động',CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
        COUNT(EmployeeKey) AS Amount
        FROM [dbo].[SLR_ReportWorker_Close]
        WHERE CodeID ='STTL' AND RecordStatus <> 99
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey)
        --Hô tro
        INSERT INTO Temp
        SELECT 'SLLD', N'09.Số lượng lao động',CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
        COUNT(EmployeeKey) AS Amount
        FROM [dbo].[SLR_ReportSupport_Close]
        WHERE CodeID ='STTL' AND RecordStatus <> 99
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey)
        --van phòng
        INSERT INTO Temp
        SELECT 'SLLD', N'09.Số lượng lao động',CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
        COUNT(EmployeeKey) AS Amount
        FROM [dbo].[SLR_ReportOffice_Close]
        WHERE CodeID ='STTL' AND RecordStatus <> 99
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey)
--Dòng 10 Số giờ làm việc
        --Số giờ làm việc công nhân
        INSERT INTO Temp
        SELECT 'SGLV',N'10.Số giờ làm việc', 
        CONVERT(datetime,CONCAT(YEAR(DateImport),'/',MONTH(DateImport),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
        ROUND(((SUM(CAST (LEFT(TotalTime ,2) AS MONEY)) *60 + SUM(CAST (Right(TotalTime ,2) AS MONEY))) /60),0) AS Amount
        FROM [dbo].[Temp_Import_Detail]
        WHERE RecordStatus <> 99 
        AND DateImport BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY MONTH(DateImport),YEAR(DateImport),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey)

        --Số giờ làm nhóm hỗ trợ
        INSERT INTO Temp
        SELECT  'SGLV',N'10.Số giờ làm việc',
        CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
        ROUND(((SUM(CAST (LEFT(TotalTime ,2) AS MONEY)) *60 + SUM(CAST (Right(TotalTime ,2) AS MONEY))) /60),0) AS Amount
        FROM[dbo].[HRM_Employee_KeepingTime]
        WHERE RecordStatus <> 99 
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        AND [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey)=26
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey)
        --Số giờ làm việc khối văn phòng
        INSERT INTO Temp
        SELECT  'SGLV',N'10.Số giờ làm việc',
        CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
        ROUND(((SUM(CAST (LEFT(TotalTime ,2) AS MONEY)) *60 + SUM(CAST (Right(TotalTime ,2) AS MONEY))) /60),0) AS Amount
        FROM[dbo].[HRM_Employee_KeepingTime]
        WHERE RecordStatus <> 99 
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        AND [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey)= 2
        AND [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey) <>26
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey)

--Dong 11 số ngày công
        INSERT INTO Temp
        SELECT 'SCTT', N'11.Ngày công làm việc',CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
        SUM(Number) AS Amount
        FROM [dbo].[HRM_TimeKeeping_Month_Close]
        WHERE RecordStatus <> 99 AND CodeID='SCTT'
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey)

--Dòng 12 số tiền trung bình
        ;WITH G AS (
	        SELECT DateView AS [DATE],
	        BranchKey,
	        DepartmentKey,
	        Amount AS TLPC,0 AS SLLD
	        FROM Temp
	        WHERE  [DateView] BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
	        AND ID='TLPC'
        UNION 
	        SELECT DateView AS [DATE],
	        BranchKey,
	        DepartmentKey,
	        0 AS TLPC  ,Amount AS SLLD
	        FROM Temp
	        WHERE  [DateView] BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
	        AND ID='SLLD'
        )
        INSERT INTO Temp
        SELECT  'STTB',N'12.Thu nhập trung bình (VNĐ/Người)',[DATE],BranchKey,DepartmentKey,
	        CASE WHEN SUM(SLLD)>0 THEN ROUND(SUM(TLPC)/SUM(SLLD),0) 
	        ELSE 0
	        END Amount
        FROM G
        GROUP BY [DATE],BranchKey,DepartmentKey
--//Kết thúc dòng 12
--Tổng cộng tất cả các bộ phận
        ;WITH K AS 
        (
        SELECT NOIDUNG,CONCAT(MONTH([DateView]),'/',YEAR([DateView])) AS [DATE],
        CAST(DepartmentKey AS NVARCHAR(10)) AS DepartmentKey,
        CAST(Amount AS money)AS Amount
        FROM Temp WHERE 1=1 @Parameter
	        UNION ALL
	        SELECT
	        NOIDUNG,
	        '99/9999'[DATE],
	        DepartmentKey,
	        SUM(Amount) AS Amount
	        FROM Temp WHERE 1=1 @Parameter
	        GROUP BY NOIDUNG,DepartmentKey
        ) 
        SELECT LeftColumn,HeaderColumn,Amount FROM(
	        SELECT B.NOIDUNG AS LeftColumn,[DATE]+'|'+CAST(B.DepartmentKey AS NVARCHAR(10))+'|'+[dbo].[Fn_GetDepartmentName](B.DepartmentKey) AS HeaderColumn,Amount,[DATE],
	        D.Rank AS BranchRank,C.Rank AS DepartmentRank
	        FROM K B
	        LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
	        LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
            
         ) X
         ORDER BY  LEN([DATE]),[DATE],BranchRank,DepartmentRank

DROP TABLE Temp";
            zSQL = zSQL.Replace("@Parameter", Fillter);

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BranchKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        //Thu Hiển thị theo tổ nhóm-- nhóm mặc định
        public static DataTable TONGTHUNHAP_TONHOM(DateTime FromDate, DateTime ToDate, int BranchKey, int DepartmentKey, int TeamKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);

            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, 1, 0, 0, 0);
            zToDate = zToDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            string Fillter = "";

            if (BranchKey > 0)
            {
                Fillter += " AND BranchKey = @BranchKey";
            }
            if (DepartmentKey > 0)
                Fillter += " AND DepartmentKey =@DepartmentKey ";
            if (TeamKey > 0)
                Fillter += " AND TeamKey =@TeamKey ";

            DataTable zTable = new DataTable();
            string zSQL = @"
--declare @FromDate datetime ='2020-01-01 00:00:00'
--declare @ToDate datetime ='2020-12-31 23:59:59'
--declare @TeamKey int = 79

CREATE TABLE Temp
(	
	ID NVARCHAR(50),
	[NOIDUNG] NVARCHAR(500) ,
	[DateView] DATETIME,
	[BranchKey] INT,
	[DepartmentKey] INT,
	[TeamKey] INT,
	[Amount] MONEY
)
--Dòng 1 tổng lương ------------------------------------------
        --//Tổng cộng công nhân
        INSERT INTO Temp
        SELECT 'TLSX', N'01.Tổng Lương',
        CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
		TeamKey,
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_ReportWorker_Close]
        WHERE CodeID ='TCSX' 
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),TeamKey
        --Tổng lương hỗ trợ
        INSERT INTO Temp
        SELECT'TLSX', N'01.Tổng Lương',
        CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
		TeamKey,
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_ReportSupport_Close]
        WHERE CodeID ='TCSX' 
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),TeamKey
        --Tổng lương văn phòng
        INSERT INTO Temp
        SELECT 'TLSX',N'01.Tổng Lương',
        CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
		TeamKey,
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_ReportOffice_Close]
        WHERE CodeID ='TCSX' 
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),TeamKey
--Dòng 2 Lương tháng 13------------------------------------------------
        INSERT INTO Temp
        SELECT 'TL13',N'02.Lương tháng 13',
        CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
		TeamKey,
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_Salary13_Detail]
        WHERE (Class ='TU' OR Class='CT' )
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),TeamKey
--Dòng 3 Thưởng theo Kết quả kinh doanh------------------------------------------------
        INSERT INTO Temp
        SELECT 'TLKD',N'03.Thưởng theo Kết quả kinh doanh',
        CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
		TeamKey,
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_SalaryBusiness_Detail]
        WHERE Class ='TU' 
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),TeamKey
--Dòng 4 Các khoản trích theo lương (chi phí công ty)------------------------------------------------
          --Sản xuất
        INSERT INTO Temp
        SELECT'TLTL', N'04.Các khoản trích theo lương (chi phí công ty)',
        CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        BranchKey,DepartmentKey,TeamKey,
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_ReportWorker_Close]
        WHERE  RecordStatus <> 99 AND Parent IS NOT NULL
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND (CodeID='XHCT' OR CodeID='YTCT' OR CodeID='NVCT' OR CodeID='CDCT')
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),BranchKey,DepartmentKey,TeamKey
        --Hỗ trợ
        INSERT INTO Temp
         SELECT'TLTL', N'04.Các khoản trích theo lương (chi phí công ty)',
        CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        BranchKey,DepartmentKey,TeamKey,
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_ReportSupport_Close]
        WHERE  RecordStatus <> 99 AND Parent IS NOT NULL
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND (CodeID='XHCT' OR CodeID='YTCT' OR CodeID='NVCT' OR CodeID='CDCT')
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),BranchKey,DepartmentKey,TeamKey
        --Văp phòng
        INSERT INTO Temp
         SELECT'TLTL', N'04.Các khoản trích theo lương (chi phí công ty)',
        CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        BranchKey,DepartmentKey,TeamKey,
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_ReportOffice_Close]
        WHERE  RecordStatus <> 99 AND Parent IS NOT NULL
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND (CodeID='XHCT' OR CodeID='YTCT' OR CodeID='NVCT' OR CodeID='CDCT')
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),BranchKey,DepartmentKey,TeamKey
--Dòng 5 Hỗ trợ thấp điểm
        --Công nhân
        INSERT INTO Temp
        SELECT 'HTTD',N'05.Hỗ trợ thấp điểm',
        CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
		TeamKey,
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_ReportWorker_Close]
        WHERE CodeID ='HTTD' 
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),TeamKey
        --khối hỗ trợ
        INSERT INTO Temp
        SELECT'HTTD', N'05.Hỗ trợ thấp điểm',
        CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
		TeamKey,
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_ReportSupport_Close]
        WHERE CodeID ='HTTD' 
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),TeamKey
        --khối văn phòng
        INSERT INTO Temp
        SELECT 'HTTD',N'05.Hỗ trợ thấp điểm',
        CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
		TeamKey,
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_ReportOffice_Close]
        WHERE CodeID ='HTTD' 
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),TeamKey

--Dòng 6 Tiền cơm bảng lương
        --Công nhân
        INSERT INTO Temp
        SELECT 'HTCO',N'06.Phụ cấp Cơm trưa + Cơm tăng ca',
        CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
		TeamKey,
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_ReportWorker_Close]
        WHERE CategoryKey = 6 --(CodeID ='STCC' OR CodeID='STCU')
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),TeamKey
        --khối hỗ trợ
        INSERT INTO Temp
        SELECT  'HTCO',N'06.Phụ cấp Cơm trưa + Cơm tăng ca',
        CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
		TeamKey,
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_ReportSupport_Close]
        WHERE CategoryKey = 6 --(CodeID ='STCC' OR CodeID='STCU')
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),TeamKey
        --khối văn phòng
        INSERT INTO Temp
        SELECT  'HTCO',N'06.Phụ cấp Cơm trưa + Cơm tăng ca',
        CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
		TeamKey,
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_ReportOffice_Close]
        WHERE CategoryKey = 6 --(CodeID ='STCC' OR CodeID='STCU')
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),TeamKey
-- Dòng 7 Phụ cấp Cơm trưa (trả cho đơn vị nấu)
        INSERT INTO Temp
        SELECT 'CCTY',N'07.Phụ cấp Cơm trưa (trả cho đơn vị nấu)',
        CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
		TeamKey,
        Round(SUM(Money1+Money2+Money3),0) AS Amount
        FROM [dbo].[FNC_RiceTeamCompany]
        WHERE RecordStatus <> 99
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),TeamKey
-- Dòng 8 Tổng thu nhập (Lương, thưởng, phụ cấp)
        INSERT INTO Temp
        SELECT  'TLPC',N'08.Tổng thu nhập (Lương, thưởng, phụ cấp)',
        CONVERT(datetime,CONCAT(YEAR([DateView]),'/',MONTH([DateView]),'/1'),120),
        BranchKey,
        DepartmentKey,
		TeamKey,
        Round(SUM(Amount),0) AS Amount
        FROM Temp
        WHERE  [DateView] BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        AND (ID='TLSX'OR ID='TL13' OR ID='TLKD' OR ID='HTTD'OR ID='HTCO' OR ID='TLTL')
        GROUP BY MONTH([DateView]),YEAR([DateView]),BranchKey,DepartmentKey,TeamKey

--Dòng 9 Số lượng lao động
        --Công nhân
        INSERT INTO Temp
        SELECT 'SLLD', N'09.Số lượng lao động',CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
		TeamKey,
        COUNT(EmployeeKey) AS Amount
        FROM [dbo].[SLR_ReportWorker_Close]
        WHERE CodeID ='STTL' AND RecordStatus <> 99
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),TeamKey
        --Hô tro
        INSERT INTO Temp
        SELECT 'SLLD', N'09.Số lượng lao động',CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
		TeamKey,
        COUNT(EmployeeKey) AS Amount
        FROM [dbo].[SLR_ReportSupport_Close]
        WHERE CodeID ='STTL' AND RecordStatus <> 99
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),TeamKey
        --van phòng
        INSERT INTO Temp
        SELECT 'SLLD', N'09.Số lượng lao động',CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
		TeamKey,
        COUNT(EmployeeKey) AS Amount
        FROM [dbo].[SLR_ReportOffice_Close]
        WHERE CodeID ='STTL' AND RecordStatus <> 99
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),TeamKey
--Dòng 10 Số giờ làm việc
        --Số giờ làm việc công nhân
        INSERT INTO Temp
        SELECT 'SGLV',N'10.Số giờ làm việc', 
        CONVERT(datetime,CONCAT(YEAR(DateImport),'/',MONTH(DateImport),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
		TeamKey,
        ROUND(((SUM(CAST (LEFT(TotalTime ,2) AS MONEY)) *60 + SUM(CAST (Right(TotalTime ,2) AS MONEY))) /60),0) AS Amount
        FROM [dbo].[Temp_Import_Detail]
        WHERE RecordStatus <> 99 
        AND DateImport BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY MONTH(DateImport),YEAR(DateImport),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),TeamKey

        --Số giờ làm nhóm hỗ trợ
        INSERT INTO Temp
        SELECT  'SGLV',N'10.Số giờ làm việc',
        CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
		TeamKey,
        ROUND(((SUM(CAST (LEFT(TotalTime ,2) AS MONEY)) *60 + SUM(CAST (Right(TotalTime ,2) AS MONEY))) /60),0) AS Amount
        FROM[dbo].[HRM_Employee_KeepingTime]
        WHERE RecordStatus <> 99 
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        AND [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey)=26
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),TeamKey
        --Số giờ làm việc khối văn phòng
        INSERT INTO Temp
        SELECT  'SGLV',N'10.Số giờ làm việc',
        CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
		TeamKey,
        ROUND(((SUM(CAST (LEFT(TotalTime ,2) AS MONEY)) *60 + SUM(CAST (Right(TotalTime ,2) AS MONEY))) /60),0) AS Amount
        FROM[dbo].[HRM_Employee_KeepingTime]
        WHERE RecordStatus <> 99 
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        AND [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey)= 2
        AND [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey) <>26
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),TeamKey

--Dong 11 số ngày công
        INSERT INTO Temp
        SELECT 'SCTT', N'11.Ngày công làm việc',CONVERT(datetime,CONCAT(YEAR(DateWrite),'/',MONTH(DateWrite),'/1'),120),
        [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
        [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
		TeamKey,
        SUM(Number) AS Amount
        FROM [dbo].[HRM_TimeKeeping_Month_Close]
        WHERE RecordStatus <> 99 AND CodeID='SCTT'
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY MONTH(DateWrite),YEAR(DateWrite),[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),TeamKey

--Dòng 12 số tiền trung bình
        ;WITH G AS (
	        SELECT DateView AS [DATE],
	        BranchKey,
	        DepartmentKey,
			TeamKey,
	        Amount AS TLPC,0 AS SLLD
	        FROM Temp
	        WHERE  [DateView] BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
	        AND ID='TLPC'
        UNION 
	        SELECT DateView AS [DATE],
	        BranchKey,
	        DepartmentKey,
			TeamKey,
	        0 AS TLPC  ,Amount AS SLLD
	        FROM Temp
	        WHERE  [DateView] BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
	        AND ID='SLLD'
        )
        INSERT INTO Temp
        SELECT  'STTB',N'12.Thu nhập trung bình (VNĐ/Người)',[DATE],BranchKey,DepartmentKey,TeamKey,
	        CASE WHEN SUM(SLLD)>0 THEN ROUND(SUM(TLPC)/SUM(SLLD),0) 
	        ELSE 0
	        END Amount
        FROM G
        GROUP BY [DATE],BranchKey,DepartmentKey,TeamKey
--//Kết thúc dòng 12
--Tổng cộng tất cả các bộ phận
        ;WITH K AS 
        (
        SELECT NOIDUNG,CONCAT(MONTH([DateView]),'/',YEAR([DateView])) AS [DATE],
        CAST(TeamKey AS NVARCHAR(10)) AS TeamKey,
        CAST(Amount AS money)AS Amount
        FROM Temp WHERE 1=1 @Parameter
	        UNION ALL
	        SELECT
	        NOIDUNG,
	        '99/9999'[DATE],
	        TeamKey,
	        SUM(Amount) AS Amount
	        FROM Temp WHERE 1=1 @Parameter
	        GROUP BY NOIDUNG,TeamKey
        ) 
        SELECT LeftColumn,HeaderColumn,Amount FROM(
	        SELECT A.NOIDUNG AS LeftColumn,[DATE]+'|'+CAST(A.TeamKey AS NVARCHAR(10))+'|'+[dbo].[Fn_GetTeamName](A.TeamKey) AS HeaderColumn,Amount,[DATE],
	        D.Rank AS BranchRank,C.Rank AS DepartmentRank,B.Rank AS TeamRank
	        FROM K A
			LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
	        LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
	        LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
            
         ) X
         ORDER BY  LEN([DATE]),[DATE],BranchRank,DepartmentRank,TeamRank

DROP TABLE Temp";
            zSQL = zSQL.Replace("@Parameter", Fillter);

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BranchKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }

        #endregion

        //Báo cáo tiền lương ngày- số giờ làm việc- trung bình lương ngày
        public static DataTable TRUNGBINHNGAY(DateTime FromDate, DateTime ToDate, int TeamKey, string Search)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            string Fillter = "";
            if (TeamKey > 0)
            {
                Fillter += " AND [dbo].[Fn_TeamKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate) =@TeamKey";
            }
            if (Search.Trim().Length > 0)
            {
                Fillter += " AND EmployeeID IN ('" + Search.Trim() + "') ";
            }

            string zSQL = @"
--declare @FromDate datetime ='2021-01-01 00:00:00'
--declare @ToDate datetime ='2021-01-30 23:59:59'

CREATE TABLE temp(
Ngay nvarchar(4),
Thang nvarchar(4),
Nam nvarchar(4),
TeamKey int,
EmployeeKey nvarchar(50),
EmployeeID nvarchar(50),
EmployeeName nvarchar(250),
[MoneyDate] money,
[HourDate] money,
[Medium] money
)
        --Tiền Nhóm chính
        INSERT INTO Temp 
        SELECT DAY(OrderDate),MONTH(OrderDate),YEAR(OrderDate),
        [dbo].[Fn_TeamKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate),EmployeeKey,EmployeeID,EmployeeName,
        SUM([Money] + MoneyPrivate +Money_Borrow) ,0,0
        FROM [dbo].[FTR_Order_Money]  
        WHERE  OrderDate BETWEEN @FROMDATE AND @TODATE  AND ([Money]>0 OR MoneyPrivate > 0 OR Money_Borrow  >0)
        AND RecordStatus <>99  @CustomParamater --AND EmployeeID in ('108')
        GROUP BY DAY(OrderDate),MONTH(OrderDate),YEAR(OrderDate),TeamKey,EmployeeKey,EmployeeID,EmployeeName

        --Tiền Chia lại
        INSERT INTO Temp 
        SELECT DAY(OrderDate),MONTH(OrderDate),YEAR(OrderDate),
        [dbo].[Fn_TeamKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate),EmployeeKey,EmployeeID,EmployeeName,
        Sum(MoneyPersonal) ,0,0
        FROM [dbo].[FTR_Order_Adjusted]
        WHERE  OrderDate BETWEEN  @FromDate AND  @ToDate AND MoneyPersonal >0
        AND Share=0 AND RecordStatus <>99 @CustomParamater --AND EmployeeID in ('108')
        GROUP BY DAY(OrderDate),MONTH(OrderDate),YEAR(OrderDate),TeamKey,EmployeeKey,EmployeeID,EmployeeName

        --Tiền Giờ dư
        INSERT INTO Temp 
        SELECT DAY(DateImport),MONTH(DateImport),YEAR(DateImport),
        [dbo].[Fn_TeamKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate),
        EmployeeKey,EmployeeID,EmployeeName,
        [Money] ,0,0
        FROM [dbo].[Temp_Import_Detail]
        WHERE  DateImport BETWEEN @FROMDATE AND @TODATE AND [Money] > 0
        AND RecordStatus <>99 @CustomParamater --AND EmployeeID in ('108')

--Lấy giờ dư
--Lưu ý: nếu là cn thì số giờ * hệ sô cn, nếu là lễ thì số giờ * hệ số lễ
        INSERT INTO Temp 
        SELECT DAY(DateImport),MONTH(DateImport),YEAR(DateImport),
        [dbo].[Fn_TeamKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate),
        EmployeeKey,EmployeeID,EmployeeName,
        0,
        CASE 
	        WHEN DATENAME(weekday,DateImport) ='SUNDAY'
		        THEN ROUND(((CAST (LEFT(TotalTime ,2) AS MONEY) *60 + CAST (Right(TotalTime ,2) AS MONEY)) /60),1)*[dbo].[LayHeSoChuNhat](DateImport)
	        WHEN[dbo].[LayHeSoNgayLeDonHang](DateImport) >0 
	        THEN ROUND(((CAST (LEFT(TotalTime ,2) AS MONEY) *60 + CAST (Right(TotalTime ,2) AS MONEY)) /60),1) * [dbo].[LayHeSoNgayLeDonHang](DateImport)
	        ELSE ROUND(((CAST (LEFT(TotalTime ,2) AS MONEY) *60 + CAST (Right(TotalTime ,2) AS MONEY)) /60),1)
        END HourDate,
        0
        FROM [dbo].[Temp_Import_Detail]
        WHERE RecordStatus <> 99 AND ROUND(((CAST (LEFT(TotalTime ,2) AS MONEY) *60 + CAST (Right(TotalTime ,2) AS MONEY)) /60),1) >0
        AND DateImport BETWEEN @FROMDATE AND @TODATE  
        @CustomParamater --AND EmployeeID in ('108')

 SELECT TeamName+'|'+EmployeeName+'|'+EmployeeID+'|'+CAST(DepartmentRank AS NVARCHAR(50))+'|'+CAST(TeamRank AS NVARCHAR(50)) AS LeftColumn,
  DateWrite AS HeaderColumn,ROUND(MoneyDate,0) AS MoneyDate ,ROUND(HourDate,2) AS HourDate,ROUND(MediumDate,0) AS MediumDate 
  FROM(
	        
			SELECT CONCAT(Ngay,'/',Thang,'/',Nam) AS DateWrite,
			A.TeamKey AS TeamKey,
			B.TeamID,B.TeamName,
			EmployeeKey,EmployeeID,EmployeeName,
			 CAST(SUM(MoneyDate) AS money) AS MoneyDate,
			 CAST(SUM(HourDate) AS money) AS HourDate,
			CASE WHEN SUM(HourDate) >0 THEN (SUM(MoneyDate)/SUM(HourDate) )
			ELSE 0
			END  MediumDate,
			D.Rank AS BranchRank,C.Rank AS DepartmentRank,B.Rank AS TeamRank
			FROM Temp A
			LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
	        LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
	        LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
            GROUP BY  CONCAT(Ngay,'/',Thang,'/',Nam),A.TeamKey,EmployeeKey,EmployeeID,EmployeeName ,
			B.TeamID,B.TeamName,D.Rank,C.Rank,B.Rank
         ) X
          ORDER BY  LEN(DateWrite),DateWrite,BranchRank,DepartmentRank,TeamRank,LEN(EmployeeID),EmployeeID

DROP TABLE Temp
";
            zSQL = zSQL.Replace("@CustomParamater", Fillter);
            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.NVarChar).Value = TeamKey;
                zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search.Trim() + "%";
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable List_PayATM(DateTime DateWrite, int BranchKey, int DepartmentKey, int TeamKey, string Search)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            string Fillter = "";
            if (BranchKey > 0)
            {
                Fillter += " AND BranchKey = @BranchKey";
            }
            if (DepartmentKey > 0)
            {
                Fillter += " AND DepartmentKey = @DepartmentKey";
            }
            if (TeamKey > 0)
            {
                Fillter += " AND TeamKey = @TeamKey";
            }
            if (Search.Trim().Length > 0)
            {
                Fillter += " AND ( EmployeeID LIKE @Search OR EmployeeName LIKE @Search )";
            }
            DataTable zTable = new DataTable();
            string zSQL = @"
--declare @FromDate datetime ='2020-06-01 00:00:00'
--declare @ToDate datetime ='2020-06-30 23:59:59'

SELECT X.EmployeeName,X.TeamID,X.EmployeeID,X.ATM,X.Amount,X.Description FROM(
		SELECT EmployeeName,TeamID,EmployeeID,ATM,Amount,''Description FROM [dbo].[SLR_ReportOffice_Close]
		WHERE DateWrite between @FromDate AND @ToDate AND CodeID='STTL' AND ATM !='' @Parameter
UNION 
		SELECT EmployeeName,TeamID,EmployeeID,ATM,Amount,''Description FROM [dbo].[SLR_ReportSupport_Close]
		WHERE DateWrite between @FromDate AND @ToDate AND CodeID='STTL' AND ATM !='' @Parameter
UNION 
		SELECT EmployeeName,TeamID,EmployeeID,ATM ,Amount,''Description FROM [dbo].[SLR_ReportWorker_Close]
		WHERE DateWrite between @FromDate AND @ToDate AND CodeID='STTL' AND ATM !='' @Parameter
)X
LEFT JOIN [dbo].[SYS_Team] B ON B.TeamID=X.TeamID
LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
ORDER BY D.Rank, C.Rank,B.Rank, LEN(EmployeeID),EmployeeID 
";
            zSQL = zSQL.Replace("@Parameter", Fillter);
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BranchKey;
                zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search.Trim() + "%";
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }

        //Báo cáo tiền lương ngày- số giờ làm việc- trung bình lương ngày
        public static DataTable TRUNGBINHNGAY_TAITO(DateTime FromDate, DateTime ToDate, int TeamKey, string Search)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            string Fillter = "";
            if (TeamKey > 0)
            {
                Fillter += " AND [dbo].[Fn_TeamKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate) =@TeamKey";
            }
            if (Search.Trim().Length > 0)
            {
                Fillter += " AND EmployeeID IN ('" + Search.Trim() + "') ";
            }

            string zSQL = @"
--DECLARE @FromDate DATETIME = '2021-01-01 00:00:00'
--DECLARE @ToDate DATETIME = '2021-01-10 23:59:59'
--DECLARE @TeamKey INT =88 

CREATE TABLE #temp(
Ngay nvarchar(4),
Thang nvarchar(4),
Nam nvarchar(4),
TeamKey int,
EmployeeKey nvarchar(50),
EmployeeID nvarchar(50),
EmployeeName nvarchar(250),
[MoneyDate] money,
[HourDate] money,
[Medium] money
)
INSERT INTO #temp
--Lương khoán tại tổ của người tại tổ (không có người tổ khác)

		SELECT 
		 DAY(OrderDate),MONTH(OrderDate),YEAR(OrderDate),   
		TeamKey, 
		EmployeeKey,
		EmployeeID,
		EmployeeName, 
		ISNULL(SUM(([Money] + Money_Borrow+MoneyPrivate+Money_Eat)),0) AS [LK],0,0
		FROM FTR_Order_Money A
		WHERE RecordStatus <> 99
		AND OrderDate BETWEEN @FromDate AND @ToDate
        AND [dbo].[Fn_TeamKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate) = TeamKey 
        @CustomParamater
	   GROUP BY DAY(OrderDate),MONTH(OrderDate),YEAR(OrderDate),
	   [dbo].[Fn_TeamKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate),
	   TeamKey,
	   EmployeeKey,
	   EmployeeName, 
	   EmployeeID

--Lấy giờ tại tổ
--Lưu ý: nếu là cn thì số giờ * hệ sô cn, nếu là lễ thì số giờ * hệ số lễ
        INSERT INTO #Temp 
        SELECT DAY(DateImport),MONTH(DateImport),YEAR(DateImport),
        [dbo].[Fn_TeamKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate),
        EmployeeKey,EmployeeID,EmployeeName,
        0, 
        CASE 
	        WHEN DATENAME(weekday,DateImport) ='SUNDAY'
		        THEN Time_TeamMain * [dbo].[LayHeSoChuNhat](DateImport)
	        WHEN[dbo].[LayHeSoNgayLeDonHang](DateImport) >0 
	        THEN Time_TeamMain * [dbo].[LayHeSoNgayLeDonHang](DateImport)
	        ELSE Time_TeamMain
        END HourDate,
        0
        FROM [dbo].[Temp_Import_Detail]
        WHERE RecordStatus <> 99 AND Time_TeamMain > 0
        AND DateImport BETWEEN @FROMDATE AND @TODATE  
        @CustomParamater --AND EmployeeID in ('108')

 SELECT TeamName+'|'+EmployeeName+'|'+EmployeeID+'|'+CAST(DepartmentRank AS NVARCHAR(50))+'|'+CAST(TeamRank AS NVARCHAR(50)) AS LeftColumn,
  DateWrite AS HeaderColumn,ROUND(MoneyDate,0) AS MoneyDate ,ROUND(HourDate,2) AS HourDate,ROUND(MediumDate,0) AS MediumDate 
  FROM(
	        
			SELECT CONCAT(Ngay,'/',Thang,'/',Nam) AS DateWrite,
			A.TeamKey AS TeamKey,
			B.TeamID,B.TeamName,
			EmployeeKey,EmployeeID,EmployeeName,
			 CAST(SUM(MoneyDate) AS money) AS MoneyDate,
			 CAST(SUM(HourDate) AS money) AS HourDate,
			CASE WHEN SUM(HourDate) >0 THEN (SUM(MoneyDate)/SUM(HourDate) )
			ELSE 0
			END  MediumDate,
			D.Rank AS BranchRank,C.Rank AS DepartmentRank,B.Rank AS TeamRank
			FROM #Temp A
			LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
	        LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
	        LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
            GROUP BY  CONCAT(Ngay,'/',Thang,'/',Nam),A.TeamKey,EmployeeKey,EmployeeID,EmployeeName ,
			B.TeamID,B.TeamName,D.Rank,C.Rank,B.Rank
         ) X
          ORDER BY  LEN(DateWrite),DateWrite,BranchRank,DepartmentRank,TeamRank,LEN(EmployeeID),EmployeeID

DROP TABLE #Temp
";
            zSQL = zSQL.Replace("@CustomParamater", Fillter);
            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.NVarChar).Value = TeamKey;
                zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search.Trim() + "%";
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        //Report 22 kế thừa TRANPHAM1V4
        public static DataTable Report22(DateTime FromDate, DateTime ToDate, int TeamKey, string Search)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string zSQL = @"
--declare @FromDate datetime ='2020-06-04 00:00:00'
--declare @ToDate datetime ='2020-06-04 23:59:59'
--declare @TeamKey int = 88
--Lấy dữ liệu từng  đơn hàng
CREATE TABLE #Temp
(	[OrderKey] nvarchar(500),
	[TeamKeyEmployee] INT,
	[TeamKey] INT ,
	[EmployeeKey] nvarchar(500),
	[EmployeeName] nvarchar(500) ,
	[EmployeeID] nvarchar(500) ,
	[StageGroup] NVARCHAR(500),
	[StageKey] INT ,
	[StageName] nvarchar(500) ,
	[StagePrice] FLOAT ,
	[SL] FLOAT ,
	[LK] FLOAT ,
	[TYPE] INT--Có phải người mượn không
)
        INSERT INTO #Temp 
	    SELECT 
	    A.OrderKey,
        [dbo].[Fn_TeamKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate),     
	    A.TeamKey, 
	    A.EmployeeKey,
        A.EmployeeName, 
        A.EmployeeID,
	    dbo.LayNhomCongViec(A.StageKey),
	    A.StageKey,
	    dbo.LayTenCongViec(A.StageKey) ,
	    dbo.LayGiaCongViec(A.StageKey) ,
	    ISNULL((A.Kg + A.Kg_Borrow+A.KgPrivate+A.Kg_Eat),0),
	    ISNULL((A.[Money] + A.Money_Borrow+A.MoneyPrivate+A.Money_Eat),0) ,
	    CASE 
			    WHEN  (A.Category=1  OR A.CategoryPrivate=1) THEN 0
			    WHEN  (A.Category !=1 AND A.CategoryPrivate!=1) THEN 1
	    END AS [TYPE]
	    FROM FTR_Order_Money A
	    LEFT JOIN FTR_Order B ON B.OrderKey=A.OrderKey
	    WHERE A.RecordStatus <> 99  AND B.RecordStatus <> 99 --AND C.RecordStatus <> 99
	    AND A.OrderDate BETWEEN @FromDate AND @ToDate
	    AND A.TeamKey = @TeamKey AND (A.EmployeeID LIKE @Search OR A.EmployeeName LIKE @Search)
--Tính tổng từ ngày đến ngày
CREATE TABLE #Temp2
(	[OrderKey] nvarchar(500),
	[TeamKeyEmployee] INT,
	[TeamKey] INT ,
	[EmployeeKey] nvarchar(500),
	[EmployeeName] nvarchar(500) ,
	[EmployeeID] nvarchar(500) ,
	[StageGroup] NVARCHAR(500),
	[StageKey] INT ,
	[StageName] nvarchar(500) ,
	[StagePrice] FLOAT ,
	[SL] FLOAT ,
	[LK] FLOAT ,
	[TYPE] INT
)
		INSERT INTO #Temp2
		SELECT [OrderKey],[TeamKeyEmployee],[TeamKey],[EmployeeKey],[EmployeeName],[EmployeeID],
		[StageGroup],[StageKey],[StageName],[StagePrice],SUM([SL]),SUM([LK]),[TYPE]
		FROM #Temp
		GROUP BY [OrderKey],[TeamKeyEmployee],[TeamKey],[EmployeeKey],[EmployeeName],[EmployeeID],
		[StageGroup],[StageKey],[StageName],[StagePrice],[TYPE]
--SELECT * FROM #Temp2

SELECT 
CONVERT(NVARCHAR(50), [Type]) + '|' + EmployeeName + '|' + EmployeeID + '|' + dbo.Fn_GetTeamID(TeamKeyEmployee)
+'|'+BranchRank+'|'+DepartmentRank+'|'+TeamRank AS CONGNHAN,
[dbo].[Fn_LayTen_NhomCongViec]([StageGroup])+'|'+  StageName + '|' + CONVERT(NVARCHAR(50), StagePrice) AS NOIDUNG, 
SL, 
LK
FROM
(
	SELECT A.*,
    CAST(D.RANK AS nvarchar(50)) AS BranchRank,CAST(C.RANK AS nvarchar(50)) AS DepartmentRank, CAST(B.RANK AS nvarchar(50)) as TeamRank FROM #Temp A 
   	LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKeyEmployee
	LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
	LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
   
) X
	ORDER BY [dbo].[Fn_LaySoSapXep_NhomCongViec](StageGroup)

DROP TABLE #Temp
DROP TABLE #Temp2
";

            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search + "%";
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        //Report 23 thu gọn từ report 19_V2
        public static DataTable Report_23(DateTime FromDate, DateTime ToDate, string MaCongNhan, string TeamKey) // nhóm thể hiện đã làm ỏ tổ gốc hoặc đã làm ở tổ khác
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            #region [SQL]
            string zSQL = @"
DECLARE @REPORT19 AS dbo.REPORT19

-- 1 GHI NĂNG XUẤT
INSERT INTO @REPORT19 (OrderDate, EmployeeName, EmployeeID, EmployeeKey, TeamKey, TeamName, ProductKey, ProductName, NhomCongViec, StageKey, StageName, Price, TIENDU, LK, TGBUA, TGDATA, SR, TP)
SELECT 
	A.OrderDate,																		-- NGÀY ĐƠN HÀNG
	A.EmployeeName,																-- TÊN CÔNG NHÂN
	A.EmployeeID,																	-- MÃ CÔNG NHÂN
	A.EmployeeKey,																	-- KEY CÔNG NHÂN
	D.TeamKey,																		-- KEY NHÓM CÔNG NHÂN
	dbo.Fn_GetTeamName(D.TeamKey) AS TeamName,				-- TÊN NHÓM CÔNG NHÂN
	C.ProductKey,																		-- KEY SẢN PHẨM
	C.ProductName,																	-- TÊN SẢN PHẨM
    dbo.LayNhomCongViec(A.StageKey) AS NhomCongViec,			-- NHÓM CÔNG VIỆC
	A.StageKey,																		-- KEY CÔNG VIỆC
	D.StageName,																	-- TÊN CÔNG VIỆC
	D.Price,																				-- GIÁ CÔNG VIỆC
	0 AS TIENDU,																							-- TIEN GIO DU
	A.[Money] + A.MoneyPrivate + A.Money_Borrow AS LK,								-- LƯƠNG KHOÁN
	dbo.[LayThoiGianExtend_Rpt19](A.EmployeeKey,A.OrderKey) AS TGBUA,		-- THỜI GIAN NHẬP = TAY
	A.[Time] + A.TimePrivate + A.Time_Borrow AS TGDATA,							-- THỜI GIAN THEO DỮ LIỆU
	A.Basket  + A.BasketPrivate + A.Basket_Borrow AS SR,								-- SỐ RỔ
	A.Kg + A.KgPrivate + A.Kg_Borrow AS TP													-- THÀNH PHẨM

	FROM FTR_Order_Money A 
	LEFT JOIN FTR_Order C ON A.OrderKey = C.OrderKey
	LEFT JOIN IVT_Product_Stages D ON D.StageKey = A.StageKey
	WHERE 
	A.RecordStatus <> 99
	AND C.RecordStatus <> 99
	AND A.OrderDate BETWEEN @FromDate AND @ToDate
    @TeamKey1 
	@CustomParamater
-- 2 CHI LẠI
INSERT INTO @REPORT19 (OrderDate, EmployeeName, EmployeeID, EmployeeKey, TeamKey, TeamName, ProductKey, ProductName, NhomCongViec, StageKey, StageName, Price, TIENDU, LK, TGBUA, TGDATA, SR, TP)
SELECT
	A.OrderDate,																	-- NGÀY ĐƠN HÀNG
	A.EmployeeName,															-- TÊN CÔNG NHÂN
	A.EmployeeID,																-- MÃ CÔNG NHÂN
	A.EmployeeKey,																-- KEY CÔNG NHÂN
	D.TeamKey,																	-- KEY NHÓM CÔNG NHÂN
	dbo.Fn_GetTeamName(D.TeamKey) AS TeamName,			-- TÊN NHÓM CÔNG NHÂN
	C.ProductKey,																	-- KEY SẢN PHẨM
	C.ProductName,																-- TÊN SẢN PHẨM
    dbo.[LayNhomCongViec](A.StageKey) AS NhomCongViec,	-- NHÓM CÔNG VIỆC
	A.StageKey,																	-- KEY CÔNG VIỆC
	D.StageName,																-- TÊN CÔNG VIỆC
	D.Price,																			-- GIÁ CÔNG VIỆC
	0 AS TIENDU,																							-- TIEN GIO DU
	(A.MoneyPersonal) AS LK,																		-- LƯƠNG KHOÁN
	0 AS TGBUA,																							-- THỜI GIAN NHẬP = TAY
	(A.TimeProduct) AS TG,																			-- THỜI GIAN THEO DỮ LIỆU
	(A.BasketProduct) AS SR,																		-- SỐ RỔ
	(A.KgProduct) AS TP																				-- THÀNH PHẨM
	FROM FTR_Order_Adjusted A 
	LEFT JOIN FTR_Order C ON A.OrderKey = C.OrderKey
	LEFT JOIN IVT_Product_Stages D ON D.StageKey = A.StageKey
	WHERE
	A.RecordStatus <> 99
	AND C.RecordStatus <> 99
	AND A.OrderDate BETWEEN @FromDate AND @ToDate AND A.Share !=1
    @TeamKey1 
	@CustomParamater
-- 3 GIỜ DƯ
INSERT INTO @REPORT19 (OrderDate, EmployeeName, EmployeeID, EmployeeKey, TeamKey, TeamName, ProductKey, ProductName, NhomCongViec, StageKey, StageName, Price, TIENDU, LK, TGBUA, TGDATA, SR, TP)
SELECT
	NULL AS OrderDate,												-- NGÀY NHẬP
	A.EmployeeName,															-- TÊN CÔNG NHÂN
	A.EmployeeID,																-- MÃ CÔNG NHÂN
	A.EmployeeKey,																-- KEY CÔNG NHÂN
	A.TeamKey,																	-- KEY NHÓM CÔNG NHÂN
	dbo.Fn_GetTeamName(A.TeamKey) AS TeamName,			-- TÊN NHÓM CÔNG NHÂN
	'' AS ProductKey,															-- KEY SẢN PHẨM
	'' AS ProductName,															-- TÊN SẢN PHẨM
    '' AS NhomCongViec,														-- NHÓM CÔNG VIỆC
	'' AS StageKey,																-- KEY CÔNG VIỆC
	'' AS StageName,															-- TÊN CÔNG VIỆC
	'' AS Price,																		-- GIÁ CÔNG VIỆC
	SUM(A.[Money]) AS TIENDU,																	-- TIEN GIO DU
	0 AS LK,																								-- LƯƠNG KHOÁN
	0 AS TGBUA,																							-- THỜI GIAN NHẬP = TAY
	0 AS TG,																								-- THỜI GIAN THEO DỮ LIỆU
	0 AS SR,																								-- SỐ RỔ
	0 AS TP																								    -- THÀNH PHẨM
	FROM [dbo].[Temp_Import_Detail] A
	WHERE
	A.RecordStatus <> 99
	@CustomParamater
	AND A.DateImport BETWEEN @FromDate AND @ToDate 
	GROUP BY A.EmployeeName, A.EmployeeID, A.EmployeeKey, A.TeamKey

SELECT
EmployeeName + '|' + EmployeeID AS [ORDER],
CONVERT(NVARCHAR(50),TeamKey) + '|' + TeamName + '|' + ProductName + '|' + NhomCongViec + '|' + StageName + '|' + CONVERT(NVARCHAR(50),Price) AS WORK,
SUM(TIENDU) TIENDU,
SUM(LK) LK, 
SUM(TP) TP
FROM @REPORT19
GROUP BY EmployeeName, EmployeeID, EmployeeKey, TeamKey, TeamName, ProductKey, ProductName, NhomCongViec, StageKey, StageName, Price
ORDER BY TeamKey
";
            #endregion

            zSQL = zSQL.Replace("@CustomParamater", " AND A.EmployeeID IN (" + MaCongNhan + ")");
            if (TeamKey != "")
            {
                zSQL = zSQL.Replace("@TeamKey1", " AND D.TeamKey IN (" + TeamKey + ")");
                //zSQL = zSQL.Replace("@TeamKey2", " AND A.TeamKey =" + TeamKey + "");
            }
            else
            {
                zSQL = zSQL.Replace("@TeamKey1", " ");
                //zSQL = zSQL.Replace("@TeamKey2", "");
            }
DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.CommandTimeout = 1000;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        //Báo cáo xem khoán tính chuyên cần công đoạn  
        public static DataTable KhoanTinhChuyenCan_ChiTiet(DateTime FromDate, DateTime ToDate, string MaCongNhan, string TeamKey) // nhóm thể hiện đã làm ỏ tổ gốc hoặc đã làm ở tổ khác
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            #region [SQL]
            string zSQL = @"
--Declare @FromDate datetime ='2021-01-01 00:00:00'
--Declare @ToDate datetime ='2021-01-31 23:59:59'

DECLARE @REPORT19 AS dbo.REPORT19

-- 1 GHI NĂNG XUẤT
INSERT INTO @REPORT19 (OrderDate, EmployeeName, EmployeeID, EmployeeKey, TeamKey, TeamName, ProductKey, ProductName, NhomCongViec, StageKey, StageName, Price, TIENDU, LK)
SELECT 
	A.OrderDate,																		-- NGÀY ĐƠN HÀNG
	A.EmployeeName,																-- TÊN CÔNG NHÂN
	A.EmployeeID,																	-- MÃ CÔNG NHÂN
	A.EmployeeKey,																	-- KEY CÔNG NHÂN
	D.TeamKey,																		-- KEY NHÓM CÔNG NHÂN
	dbo.Fn_GetTeamName(D.TeamKey) AS TeamName,				-- TÊN NHÓM CÔNG NHÂN
	C.ProductKey,																		-- KEY SẢN PHẨM
	C.ProductName,																	-- TÊN SẢN PHẨM
    dbo.LayNhomCongViec(A.StageKey) AS NhomCongViec,			-- NHÓM CÔNG VIỆC
	A.StageKey,																		-- KEY CÔNG VIỆC
	D.StageName,																	-- TÊN CÔNG VIỆC
	D.Price,																				-- GIÁ CÔNG VIỆC
	0 AS TIENDU,																							-- TIEN GIO DU
	A.[Money] + A.MoneyPrivate + A.Money_Borrow AS LK								-- LƯƠNG KHOÁN
	FROM FTR_Order_Money A 
	LEFT JOIN FTR_Order C ON A.OrderKey = C.OrderKey
	LEFT JOIN IVT_Product_Stages D ON D.StageKey = A.StageKey
	WHERE 
	A.RecordStatus <> 99
	AND C.RecordStatus <> 99
	AND D.RecordStatus <> 99
	AND A.OrderDate BETWEEN @FromDate AND @ToDate
	AND D.IsWorkTime=1 --Công đoạn có tính chuyên cần
    @TeamKey1 
	@CustomParamater
-- 2 CHI LẠI
INSERT INTO @REPORT19 (OrderDate, EmployeeName, EmployeeID, EmployeeKey, TeamKey, TeamName, ProductKey, ProductName, NhomCongViec, StageKey, StageName, Price, TIENDU, LK)
SELECT
	A.OrderDate,																	-- NGÀY ĐƠN HÀNG
	A.EmployeeName,															-- TÊN CÔNG NHÂN
	A.EmployeeID,																-- MÃ CÔNG NHÂN
	A.EmployeeKey,																-- KEY CÔNG NHÂN
	D.TeamKey,																	-- KEY NHÓM CÔNG NHÂN
	dbo.Fn_GetTeamName(D.TeamKey) AS TeamName,			-- TÊN NHÓM CÔNG NHÂN
	C.ProductKey,																	-- KEY SẢN PHẨM
	C.ProductName,																-- TÊN SẢN PHẨM
    dbo.[LayNhomCongViec](A.StageKey) AS NhomCongViec,	-- NHÓM CÔNG VIỆC
	A.StageKey,																	-- KEY CÔNG VIỆC
	D.StageName,																-- TÊN CÔNG VIỆC
	D.Price,																			-- GIÁ CÔNG VIỆC
	0 AS TIENDU,																							-- TIEN GIO DU
	(A.MoneyPersonal) AS LK																		-- LƯƠNG KHOÁN																			-- THÀNH PHẨM
	FROM FTR_Order_Adjusted A 
	LEFT JOIN FTR_Order C ON A.OrderKey = C.OrderKey
	LEFT JOIN IVT_Product_Stages D ON D.StageKey = A.StageKey
	WHERE
	A.RecordStatus <> 99
	AND C.RecordStatus <> 99
	AND D.RecordStatus <> 99
	AND A.OrderDate BETWEEN @FromDate AND @ToDate AND A.Share !=1
	AND D.IsWorkTime =1
    @TeamKey1 
	@CustomParamater
-- 3 GIỜ DƯ
INSERT INTO @REPORT19 (OrderDate, EmployeeName, EmployeeID, EmployeeKey, TeamKey, TeamName, ProductKey, ProductName, NhomCongViec, StageKey, StageName, Price, TIENDU, LK)
SELECT
	NULL AS OrderDate,												-- NGÀY NHẬP
	A.EmployeeName,															-- TÊN CÔNG NHÂN
	A.EmployeeID,																-- MÃ CÔNG NHÂN
	A.EmployeeKey,																-- KEY CÔNG NHÂN
	A.TeamKey,																	-- KEY NHÓM CÔNG NHÂN
	dbo.Fn_GetTeamName(A.TeamKey) AS TeamName,			-- TÊN NHÓM CÔNG NHÂN
	'' AS ProductKey,															-- KEY SẢN PHẨM
	'' AS ProductName,															-- TÊN SẢN PHẨM
    '' AS NhomCongViec,														-- NHÓM CÔNG VIỆC
	'' AS StageKey,																-- KEY CÔNG VIỆC
	'' AS StageName,															-- TÊN CÔNG VIỆC
	'' AS Price,																		-- GIÁ CÔNG VIỆC
	SUM(A.[Money]) AS TIENDU,																	-- TIEN GIO DU
	0 AS LK																								-- LƯƠNG KHOÁN																							    -- THÀNH PHẨM
	FROM [dbo].[Temp_Import_Detail] A
	WHERE
	A.RecordStatus <> 99
	@CustomParamater
	AND A.DateImport BETWEEN @FromDate AND @ToDate 
	GROUP BY A.EmployeeName, A.EmployeeID, A.EmployeeKey, A.TeamKey

SELECT
EmployeeName + '|' + EmployeeID AS [ORDER],
CONVERT(NVARCHAR(50),TeamKey) + '|' + TeamName + '|' + ProductName + '|' + NhomCongViec + '|' + StageName + '|' + CONVERT(NVARCHAR(50),Price) AS WORK,
SUM(TIENDU) TIENDU,
SUM(LK) LK
FROM @REPORT19
GROUP BY EmployeeName, EmployeeID, EmployeeKey, TeamKey, TeamName, ProductKey, ProductName, NhomCongViec, StageKey, StageName, Price
ORDER BY TeamKey
";
            #endregion

            zSQL = zSQL.Replace("@CustomParamater", " AND A.EmployeeID IN (" + MaCongNhan + ")");
            if (TeamKey != "")
            {
                zSQL = zSQL.Replace("@TeamKey1", " AND D.TeamKey IN (" + TeamKey + ")");
                //zSQL = zSQL.Replace("@TeamKey2", " AND A.TeamKey =" + TeamKey + "");
            }
            else
            {
                zSQL = zSQL.Replace("@TeamKey1", " ");
                //zSQL = zSQL.Replace("@TeamKey2", "");
            }
            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.CommandTimeout = 1000;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
    }
}