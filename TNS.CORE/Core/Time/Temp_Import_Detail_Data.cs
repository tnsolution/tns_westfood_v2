﻿using System;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
namespace TNS.HRM
{
    public class Temp_Import_Detail_Data
    {
        public static DataTable List()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT *, ExcelKey AS [Key] FROM Temp_Import_Detail WHERE RecordStatus != 99 ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable ListDetail(int SheetKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT * 
FROM Temp_Import_Detail 
WHERE  SheetKey = @SheetKey  AND RecordStatus <> 99 
ORDER BY TeamID, LEN(EmployeeID), EmployeeID";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@SheetKey", SqlDbType.Int).Value = SheetKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable LoadExcelData(int ExcelKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @" SELECT * FROM [dbo].[Temp_Import_Detail] 
 WHERE ExcelKey=@ExcelKey AND RecordStatus <> 99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ExcelKey", SqlDbType.Int).Value = ExcelKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable ListDetail(int TeamKey, DateTime Datetime)
        {
            DateTime zFromDate = new DateTime(Datetime.Year, Datetime.Month, Datetime.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(Datetime.Year, Datetime.Month, Datetime.Day, 23, 59, 59);
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT * 
FROM Temp_Import_Detail 
WHERE TeamKey = @TeamKey 
AND DateImport BETWEEN @FromDate AND @ToDate 
AND RecordStatus <> 99
ORDER BY  LEN(EmployeeID), EmployeeID";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
    }
}
