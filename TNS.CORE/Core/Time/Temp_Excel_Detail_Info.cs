﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
namespace TNS.HRM
{
    public class Temp_Excel_Detail_Info
    {

        #region [ Field Name ]
        private int _AutoKey = 0;
        private int _ExcelKey = 0;
        private int _SheetKey = 0;
        private DateTime _DateImport;
        private string _EmployeeKey = "";
        private string _EmployeeID = "";
        private string _EmployeeName = "";
        private int _TeamKey = 0;
        private string _TeamID = "";
        private string _TeamName = "";
        private string _BeginTime = "";
        private string _EndTime = "";
        private string _OffTime = "";
        private string _DifferentTime = "";
        private string _TotalTime = "";
        private string _OverTime = "";
        private DateTime _OverTimeBegin;
        private DateTime _OverTimeEnd;
        private int _CheckOverNight = 0;
        private string _Description = "";
        private int _CodeKey = 0;
        private string _CodeName = "";
        private int _RiceKey = 0;
        private string _RiceName = "";
        private string _Code = "";
        private float _Value;
        private float _Money;
        private float _Money_Rice;
        private int _Hand = 0;
        private int _RecordStatus = 0;
        private DateTime _CreatedOn;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _RoleID = "";
        private string _Message = "";
        #endregion
        #region [ Properties ]
        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        public int Key
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public int ExcelKey
        {
            get { return _ExcelKey; }
            set { _ExcelKey = value; }
        }
        public int SheetKey
        {
            get { return _SheetKey; }
            set { _SheetKey = value; }
        }
        public DateTime DateImport
        {
            get { return _DateImport; }
            set { _DateImport = value; }
        }
        public string EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public string EmployeeID
        {
            get { return _EmployeeID; }
            set { _EmployeeID = value; }
        }
        public string EmployeeName
        {
            get { return _EmployeeName; }
            set { _EmployeeName = value; }
        }
        public int TeamKey
        {
            get { return _TeamKey; }
            set { _TeamKey = value; }
        }
        public string TeamID
        {
            get { return _TeamID; }
            set { _TeamID = value; }
        }
        public string TeamName
        {
            get { return _TeamName; }
            set { _TeamName = value; }
        }
        public string BeginTime
        {
            get { return _BeginTime; }
            set { _BeginTime = value; }
        }
        public string EndTime
        {
            get { return _EndTime; }
            set { _EndTime = value; }
        }
        public string OffTime
        {
            get { return _OffTime; }
            set { _OffTime = value; }
        }
        public string DifferentTime
        {
            get { return _DifferentTime; }
            set { _DifferentTime = value; }
        }
        public string TotalTime
        {
            get { return _TotalTime; }
            set { _TotalTime = value; }
        }
        public string OverTime
        {
            get { return _OverTime; }
            set { _OverTime = value; }
        }
        public DateTime OverTimeBegin
        {
            get { return _OverTimeBegin; }
            set { _OverTimeBegin = value; }
        }
        public DateTime OverTimeEnd
        {
            get { return _OverTimeEnd; }
            set { _OverTimeEnd = value; }
        }
        public int CheckOverNight
        {
            get { return _CheckOverNight; }
            set { _CheckOverNight = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public int CodeKey
        {
            get { return _CodeKey; }
            set { _CodeKey = value; }
        }
        public string CodeName
        {
            get { return _CodeName; }
            set { _CodeName = value; }
        }
        public int RiceKey
        {
            get { return _RiceKey; }
            set { _RiceKey = value; }
        }
        public string RiceName
        {
            get { return _RiceName; }
            set { _RiceName = value; }
        }
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }
        public float Value
        {
            get { return _Value; }
            set { _Value = value; }
        }
        public float Money
        {
            get { return _Money; }
            set { _Money = value; }
        }
        public float Money_Rice
        {
            get { return _Money_Rice; }
            set { _Money_Rice = value; }
        }
        public int Hand
        {
            get { return _Hand; }
            set { _Hand = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion
        #region [ Constructor Get Information ]
        public Temp_Excel_Detail_Info()
        {
        }
        public Temp_Excel_Detail_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM Temp_Excel_Detail WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    if (zReader["ExcelKey"] != DBNull.Value)
                        _ExcelKey = int.Parse(zReader["ExcelKey"].ToString());
                    if (zReader["SheetKey"] != DBNull.Value)
                        _SheetKey = int.Parse(zReader["SheetKey"].ToString());
                    if (zReader["DateImport"] != DBNull.Value)
                        _DateImport = (DateTime)zReader["DateImport"];
                    _EmployeeKey = zReader["EmployeeKey"].ToString();
                    _EmployeeID = zReader["EmployeeID"].ToString().Trim();
                    _EmployeeName = zReader["EmployeeName"].ToString().Trim();
                    if (zReader["TeamKey"] != DBNull.Value)
                        _TeamKey = int.Parse(zReader["TeamKey"].ToString());
                    _TeamID = zReader["TeamID"].ToString().Trim();
                    _TeamName = zReader["TeamName"].ToString().Trim();
                    _BeginTime = zReader["BeginTime"].ToString().Trim();
                    _EndTime = zReader["EndTime"].ToString().Trim();
                    _OffTime = zReader["OffTime"].ToString().Trim();
                    _DifferentTime = zReader["DifferentTime"].ToString().Trim();
                    _TotalTime = zReader["TotalTime"].ToString().Trim();
                    _OverTime = zReader["OverTime"].ToString().Trim();
                    if (zReader["OverTimeBegin"] != DBNull.Value)
                        _OverTimeBegin = (DateTime)zReader["OverTimeBegin"];
                    if (zReader["OverTimeEnd"] != DBNull.Value)
                        _OverTimeEnd = (DateTime)zReader["OverTimeEnd"];
                    if (zReader["CheckOverNight"] != DBNull.Value)
                        _CheckOverNight = int.Parse(zReader["CheckOverNight"].ToString());
                    _Description = zReader["Description"].ToString().Trim();
                    if (zReader["CodeKey"] != DBNull.Value)
                        _CodeKey = int.Parse(zReader["CodeKey"].ToString());
                    _CodeName = zReader["CodeName"].ToString().Trim();
                    if (zReader["RiceKey"] != DBNull.Value)
                        _RiceKey = int.Parse(zReader["RiceKey"].ToString());
                    _RiceName = zReader["RiceName"].ToString().Trim();
                    _Code = zReader["Code"].ToString().Trim();
                    if (zReader["Value"] != DBNull.Value)
                        _Value = float.Parse(zReader["Value"].ToString());
                    if (zReader["Money"] != DBNull.Value)
                        _Money = float.Parse(zReader["Money"].ToString());
                    if (zReader["Money_Rice"] != DBNull.Value)
                        _Money_Rice = float.Parse(zReader["Money_Rice"].ToString());
                    if (zReader["Hand"] != DBNull.Value)
                        _Hand = int.Parse(zReader["Hand"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }

        public Temp_Excel_Detail_Info(string EmployeeID, DateTime FromDate, DateTime ToDate)
        {
            string zSQL = "SELECT * FROM Temp_Excel_Detail WHERE  EmployeeID = @EmployeeID AND BeginTime BETWEEN @FromDate AND @ToDate AND RecordStatus <>99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = EmployeeID;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    if (zReader["ExcelKey"] != DBNull.Value)
                        _ExcelKey = int.Parse(zReader["ExcelKey"].ToString());
                    if (zReader["SheetKey"] != DBNull.Value)
                        _SheetKey = int.Parse(zReader["SheetKey"].ToString());
                    if (zReader["DateImport"] != DBNull.Value)
                        _DateImport = (DateTime)zReader["DateImport"];
                    _EmployeeKey = zReader["EmployeeKey"].ToString();
                    _EmployeeID = zReader["EmployeeID"].ToString().Trim();
                    _EmployeeName = zReader["EmployeeName"].ToString().Trim();
                    if (zReader["TeamKey"] != DBNull.Value)
                        _TeamKey = int.Parse(zReader["TeamKey"].ToString());
                    _TeamID = zReader["TeamID"].ToString().Trim();
                    _TeamName = zReader["TeamName"].ToString().Trim();
                    _BeginTime = zReader["BeginTime"].ToString().Trim();
                    _EndTime = zReader["EndTime"].ToString().Trim();
                    _OffTime = zReader["OffTime"].ToString().Trim();
                    _DifferentTime = zReader["DifferentTime"].ToString().Trim();
                    _TotalTime = zReader["TotalTime"].ToString().Trim();
                    _OverTime = zReader["OverTime"].ToString().Trim();
                    if (zReader["OverTimeBegin"] != DBNull.Value)
                        _OverTimeBegin = (DateTime)zReader["OverTimeBegin"];
                    if (zReader["OverTimeEnd"] != DBNull.Value)
                        _OverTimeEnd = (DateTime)zReader["OverTimeEnd"];
                    if (zReader["CheckOverNight"] != DBNull.Value)
                        _CheckOverNight = int.Parse(zReader["CheckOverNight"].ToString());
                    _Description = zReader["Description"].ToString().Trim();
                    if (zReader["CodeKey"] != DBNull.Value)
                        _CodeKey = int.Parse(zReader["CodeKey"].ToString());
                    _CodeName = zReader["CodeName"].ToString().Trim();
                    if (zReader["RiceKey"] != DBNull.Value)
                        _RiceKey = int.Parse(zReader["RiceKey"].ToString());
                    _RiceName = zReader["RiceName"].ToString().Trim();
                    _Code = zReader["Code"].ToString().Trim();
                    if (zReader["Value"] != DBNull.Value)
                        _Value = float.Parse(zReader["Value"].ToString());
                    if (zReader["Money"] != DBNull.Value)
                        _Money = float.Parse(zReader["Money"].ToString());
                    if (zReader["Money_Rice"] != DBNull.Value)
                        _Money_Rice = float.Parse(zReader["Money_Rice"].ToString());
                    if (zReader["Hand"] != DBNull.Value)
                        _Hand = int.Parse(zReader["Hand"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }

        public Temp_Excel_Detail_Info(DataRow InRow)
        {
            try
            {
                if (InRow["AutoKey"] != DBNull.Value)
                    _AutoKey = int.Parse(InRow["AutoKey"].ToString());
                if (InRow["ExcelKey"] != DBNull.Value)
                    _ExcelKey = int.Parse(InRow["ExcelKey"].ToString());
                if (InRow["SheetKey"] != DBNull.Value)
                    _SheetKey = int.Parse(InRow["SheetKey"].ToString());
                if (InRow["DateImport"] != DBNull.Value)
                    _DateImport = (DateTime)InRow["DateImport"];
                _EmployeeKey = InRow["EmployeeKey"].ToString();
                _EmployeeID = InRow["EmployeeID"].ToString().Trim();
                _EmployeeName = InRow["EmployeeName"].ToString().Trim();
                if (InRow["TeamKey"] != DBNull.Value)
                    _TeamKey = int.Parse(InRow["TeamKey"].ToString());
                _TeamID = InRow["TeamID"].ToString().Trim();
                _TeamName = InRow["TeamName"].ToString().Trim();
                _BeginTime = InRow["BeginTime"].ToString().Trim();
                _EndTime = InRow["EndTime"].ToString().Trim();
                _OffTime = InRow["OffTime"].ToString().Trim();
                _DifferentTime = InRow["DifferentTime"].ToString().Trim();
                _TotalTime = InRow["TotalTime"].ToString().Trim();
                _OverTime = InRow["OverTime"].ToString().Trim();
                if (InRow["OverTimeBegin"] != DBNull.Value)
                    _OverTimeBegin = (DateTime)InRow["OverTimeBegin"];
                if (InRow["OverTimeEnd"] != DBNull.Value)
                    _OverTimeEnd = (DateTime)InRow["OverTimeEnd"];
                if (InRow["CheckOverNight"] != DBNull.Value)
                    _CheckOverNight = int.Parse(InRow["CheckOverNight"].ToString());
                _Description = InRow["Description"].ToString().Trim();
                if (InRow["CodeKey"] != DBNull.Value)
                    _CodeKey = int.Parse(InRow["CodeKey"].ToString());
                _CodeName = InRow["CodeName"].ToString().Trim();
                if (InRow["RiceKey"] != DBNull.Value)
                    _RiceKey = int.Parse(InRow["RiceKey"].ToString());
                _RiceName = InRow["RiceName"].ToString().Trim();
                _Code = InRow["Code"].ToString().Trim();
                if (InRow["Value"] != DBNull.Value)
                    _Value = float.Parse(InRow["Value"].ToString());
                if (InRow["Money"] != DBNull.Value)
                    _Money = float.Parse(InRow["Money"].ToString());
                if (InRow["Money_Rice"] != DBNull.Value)
                    _Money_Rice = float.Parse(InRow["Money_Rice"].ToString());
                if (InRow["Hand"] != DBNull.Value)
                    _Hand = int.Parse(InRow["Hand"].ToString());
                if (InRow["RecordStatus"] != DBNull.Value)
                    _RecordStatus = int.Parse(InRow["RecordStatus"].ToString());
                if (InRow["CreatedOn"] != DBNull.Value)
                    _CreatedOn = (DateTime)InRow["CreatedOn"];
                _CreatedBy = InRow["CreatedBy"].ToString().Trim();
                _CreatedName = InRow["CreatedName"].ToString().Trim();
                if (InRow["ModifiedOn"] != DBNull.Value)
                    _ModifiedOn = (DateTime)InRow["ModifiedOn"];
                _ModifiedBy = InRow["ModifiedBy"].ToString().Trim();
                _ModifiedName = InRow["ModifiedName"].ToString().Trim();

            }
            catch (Exception Err) { _Message = Err.ToString(); }
        }

        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO Temp_Excel_Detail ("
 + " ExcelKey ,SheetKey ,DateImport ,EmployeeKey ,EmployeeID ,EmployeeName ,TeamKey ,TeamID ,TeamName ,BeginTime ,EndTime ,OffTime ,DifferentTime ,TotalTime ,OverTime ,OverTimeBegin ,OverTimeEnd ,CheckOverNight ,Description ,CodeKey ,CodeName ,RiceKey ,RiceName ,Code ,Value ,Money ,Money_Rice ,Hand ,RecordStatus ,CreatedOn ,CreatedBy ,CreatedName ,ModifiedOn ,ModifiedBy ,ModifiedName ) "
  + " VALUES ( "
  + "@ExcelKey ,@SheetKey ,@DateImport ,@EmployeeKey ,@EmployeeID ,@EmployeeName ,@TeamKey ,@TeamID ,@TeamName ,@BeginTime ,@EndTime ,@OffTime ,@DifferentTime ,@TotalTime ,@OverTime ,@OverTimeBegin ,@OverTimeEnd ,@CheckOverNight ,@Description ,@CodeKey ,@CodeName ,@RiceKey ,@RiceName ,@Code ,@Value ,@Money ,@Money_Rice ,@Hand ,0 ,GETDATE(),@CreatedBy ,@CreatedName ,GETDATE(),@ModifiedBy ,@ModifiedName ) ";
            zSQL += " SELECT 11 ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ExcelKey", SqlDbType.Int).Value = _ExcelKey;
                zCommand.Parameters.Add("@SheetKey", SqlDbType.Int).Value = _SheetKey;
                if (_DateImport == DateTime.MinValue)
                    zCommand.Parameters.Add("@DateImport", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateImport", SqlDbType.DateTime).Value = _DateImport;
                if (_EmployeeKey.Length > 0)
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_EmployeeKey);
                else
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID.Trim();
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName.Trim();
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = _TeamKey;
                zCommand.Parameters.Add("@TeamID", SqlDbType.NVarChar).Value = _TeamID.Trim();
                zCommand.Parameters.Add("@TeamName", SqlDbType.NVarChar).Value = _TeamName.Trim();
                zCommand.Parameters.Add("@BeginTime", SqlDbType.NVarChar).Value = _BeginTime.Trim();
                zCommand.Parameters.Add("@EndTime", SqlDbType.NVarChar).Value = _EndTime.Trim();
                zCommand.Parameters.Add("@OffTime", SqlDbType.NVarChar).Value = _OffTime.Trim();
                zCommand.Parameters.Add("@DifferentTime", SqlDbType.NVarChar).Value = _DifferentTime.Trim();
                zCommand.Parameters.Add("@TotalTime", SqlDbType.NVarChar).Value = _TotalTime.Trim();
                zCommand.Parameters.Add("@OverTime", SqlDbType.NVarChar).Value = _OverTime.Trim();
                if (_OverTimeBegin == DateTime.MinValue)
                    zCommand.Parameters.Add("@OverTimeBegin", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@OverTimeBegin", SqlDbType.DateTime).Value = _OverTimeBegin;
                if (_OverTimeEnd == DateTime.MinValue)
                    zCommand.Parameters.Add("@OverTimeEnd", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@OverTimeEnd", SqlDbType.DateTime).Value = _OverTimeEnd;
                zCommand.Parameters.Add("@CheckOverNight", SqlDbType.Int).Value = _CheckOverNight;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                zCommand.Parameters.Add("@CodeKey", SqlDbType.Int).Value = _CodeKey;
                zCommand.Parameters.Add("@CodeName", SqlDbType.NVarChar).Value = _CodeName.Trim();
                zCommand.Parameters.Add("@RiceKey", SqlDbType.Int).Value = _RiceKey;
                zCommand.Parameters.Add("@RiceName", SqlDbType.NVarChar).Value = _RiceName.Trim();
                zCommand.Parameters.Add("@Code", SqlDbType.NVarChar).Value = _Code.Trim();
                zCommand.Parameters.Add("@Value", SqlDbType.Float).Value = _Value;
                zCommand.Parameters.Add("@Money", SqlDbType.Float).Value = _Money;
                zCommand.Parameters.Add("@Money_Rice", SqlDbType.Float).Value = _Money_Rice;
                zCommand.Parameters.Add("@Hand", SqlDbType.Int).Value = _Hand;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                Message = zCommand.ExecuteScalar().ToString();

                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE Temp_Excel_Detail SET "
            + " ExcelKey = @ExcelKey,"
            + " SheetKey = @SheetKey,"
            + " DateImport = @DateImport,"
            + " EmployeeKey = @EmployeeKey,"
            + " EmployeeID = @EmployeeID,"
            + " EmployeeName = @EmployeeName,"
            + " TeamKey = @TeamKey,"
            + " TeamID = @TeamID,"
            + " TeamName = @TeamName,"
            + " BeginTime = @BeginTime,"
            + " EndTime = @EndTime,"
            + " OffTime = @OffTime,"
            + " DifferentTime = @DifferentTime,"
            + " TotalTime = @TotalTime,"
            + " OverTime = @OverTime,"
            + " OverTimeBegin = @OverTimeBegin,"
            + " OverTimeEnd = @OverTimeEnd,"
            + " CheckOverNight = @CheckOverNight,"
            + " Description = @Description,"
            + " CodeKey = @CodeKey,"
            + " CodeName = @CodeName,"
            + " RiceKey = @RiceKey,"
            + " RiceName = @RiceName,"
            + " Code = @Code,"
            + " Value = @Value,"
            + " Money = @Money,"
            + " Money_Rice = @Money_Rice,"
            + " Hand = @Hand,"
            + " RecordStatus = 1,"
            + " ModifiedOn = GETDATE(),"
            + " ModifiedBy = @ModifiedBy,"
            + " ModifiedName = @ModifiedName"
           + " WHERE AutoKey = @AutoKey SELECT 20";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@ExcelKey", SqlDbType.Int).Value = _ExcelKey;
                zCommand.Parameters.Add("@SheetKey", SqlDbType.Int).Value = _SheetKey;
                if (_DateImport == DateTime.MinValue)
                    zCommand.Parameters.Add("@DateImport", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateImport", SqlDbType.DateTime).Value = _DateImport;
                if (_EmployeeKey.Length == 36)
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_EmployeeKey);
                else
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID.Trim();
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName.Trim();
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = _TeamKey;
                zCommand.Parameters.Add("@TeamID", SqlDbType.NVarChar).Value = _TeamID.Trim();
                zCommand.Parameters.Add("@TeamName", SqlDbType.NVarChar).Value = _TeamName.Trim();
                zCommand.Parameters.Add("@BeginTime", SqlDbType.NVarChar).Value = _BeginTime.Trim();
                zCommand.Parameters.Add("@EndTime", SqlDbType.NVarChar).Value = _EndTime.Trim();
                zCommand.Parameters.Add("@OffTime", SqlDbType.NVarChar).Value = _OffTime.Trim();
                zCommand.Parameters.Add("@DifferentTime", SqlDbType.NVarChar).Value = _DifferentTime.Trim();
                zCommand.Parameters.Add("@TotalTime", SqlDbType.NVarChar).Value = _TotalTime.Trim();
                zCommand.Parameters.Add("@OverTime", SqlDbType.NVarChar).Value = _OverTime.Trim();
                if (_OverTimeBegin == DateTime.MinValue)
                    zCommand.Parameters.Add("@OverTimeBegin", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@OverTimeBegin", SqlDbType.DateTime).Value = _OverTimeBegin;
                if (_OverTimeEnd == DateTime.MinValue)
                    zCommand.Parameters.Add("@OverTimeEnd", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@OverTimeEnd", SqlDbType.DateTime).Value = _OverTimeEnd;
                zCommand.Parameters.Add("@CheckOverNight", SqlDbType.Int).Value = _CheckOverNight;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                zCommand.Parameters.Add("@CodeKey", SqlDbType.Int).Value = _CodeKey;
                zCommand.Parameters.Add("@CodeName", SqlDbType.NVarChar).Value = _CodeName.Trim();
                zCommand.Parameters.Add("@RiceKey", SqlDbType.Int).Value = _RiceKey;
                zCommand.Parameters.Add("@RiceName", SqlDbType.NVarChar).Value = _RiceName.Trim();
                zCommand.Parameters.Add("@Code", SqlDbType.NVarChar).Value = _Code.Trim();
                zCommand.Parameters.Add("@Value", SqlDbType.Float).Value = _Value;
                zCommand.Parameters.Add("@Money", SqlDbType.Float).Value = _Money;
                zCommand.Parameters.Add("@Money_Rice", SqlDbType.Float).Value = _Money_Rice;
                zCommand.Parameters.Add("@Hand", SqlDbType.Int).Value = _Hand;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @" UPDATE dbo.Temp_Excel_Detail SET
      [RecordStatus] = 99,
      [ModifiedOn] = GETDATE(),
      [ModifiedBy] = @ModifiedBy,
      [ModifiedName] = @ModifiedName
      WHERE[AutoKey] = @AutoKey SELECT 30";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string Delete_Working_Time_Rice(int ExcelKey)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"UPDATE dbo.HRM_Employee_Time SET RecordStatus = 99 WHERE ExcelKey = @ExcelKey
 UPDATE dbo.HRM_Time_Working SET RecordStatus = 99 WHERE WorkingID = @ExcelKey
 UPDATE dbo.HRM_Employee_Rice SET RecordStatus = 99 WHERE ExcelKey = @ExcelKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ExcelKey", SqlDbType.Int).Value = ExcelKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_AutoKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        #endregion
    }
}
