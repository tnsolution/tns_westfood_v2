﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;

namespace TNS.HRM
{
    public class Temp_Excel_Info
    {
        #region [ Field Name ]

        private int _ExcelKey = 0;
        private DateTime _DateImport;
        private string _File = "";
        private int _RecordStatus = 0;
        private DateTime _CreatedOn;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _RoleID = "";
        private string _Message = "";
        #endregion

        #region [ Properties ]
        public int Key
        {
            get
            {
                return _ExcelKey;
            }

            set
            {
                _ExcelKey = value;
            }
        }

        public DateTime DateImport
        {
            get
            {
                return _DateImport;
            }

            set
            {
                _DateImport = value;
            }
        }

        public string File
        {
            get
            {
                return _File;
            }

            set
            {
                _File = value;
            }
        }

        public int RecordStatus
        {
            get
            {
                return _RecordStatus;
            }

            set
            {
                _RecordStatus = value;
            }
        }

        public DateTime CreatedOn
        {
            get
            {
                return _CreatedOn;
            }

            set
            {
                _CreatedOn = value;
            }
        }

        public string CreatedBy
        {
            get
            {
                return _CreatedBy;
            }

            set
            {
                _CreatedBy = value;
            }
        }

        public string CreatedName
        {
            get
            {
                return _CreatedName;
            }

            set
            {
                _CreatedName = value;
            }
        }

        public DateTime ModifiedOn
        {
            get
            {
                return _ModifiedOn;
            }

            set
            {
                _ModifiedOn = value;
            }
        }

        public string ModifiedBy
        {
            get
            {
                return _ModifiedBy;
            }

            set
            {
                _ModifiedBy = value;
            }
        }

        public string ModifiedName
        {
            get
            {
                return _ModifiedName;
            }

            set
            {
                _ModifiedName = value;
            }
        }

        public string RoleID
        {
            get
            {
                return _RoleID;
            }

            set
            {
                _RoleID = value;
            }
        }

        public string Message
        {
            get
            {
                return _Message;
            }

            set
            {
                _Message = value;
            }
        }
        #endregion

        #region [ Constructor Get Information ]
        public Temp_Excel_Info()
        {

        }
        public Temp_Excel_Info(int ExcelKey)
        {
            string zSQL = "SELECT * FROM Temp_Excel WHERE ExcelKey = @ExcelKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (ExcelKey > 0)
                    zCommand.Parameters.Add("@ExcelKey", SqlDbType.Int).Value = ExcelKey;
                else
                    zCommand.Parameters.Add("@ExcelKey", SqlDbType.Int).Value = DBNull.Value;

                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _ExcelKey = int.Parse(zReader["ExcelKey"].ToString());
                    if (zReader["DateImport"] != DBNull.Value)
                        _DateImport = (DateTime)zReader["DateImport"];
                    _File = zReader["File"].ToString().Trim();                  
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion

        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = @" INSERT INTO Temp_Excel
          (
DateImport, [File],RecordStatus,
CreatedOn, CreatedBy, CreatedName, ModifiedOn, ModifiedBy, ModifiedName
          ) VALUES (
 @DateImport, @File,0,
 GETDATE(), @CreatedBy, @CreatedName,  GETDATE(),@ModifiedBy,  @ModifiedName )
 SELECT ExcelKey FROM Temp_Excel WHERE ExcelKey = SCOPE_IDENTITY()
";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (_DateImport == DateTime.MinValue)
                    zCommand.Parameters.Add("@DateImport", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateImport", SqlDbType.DateTime).Value = _DateImport;
                zCommand.Parameters.Add("@File", SqlDbType.NVarChar).Value = File;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _ExcelKey = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = @"UPDATE dbo.Temp_Excel SET 
			DateImport = @DateImport,
			File = @File,
            RecordStatus = 1,
			ModifiedOn = GETDATE(),
			ModifiedBy = @ModifiedBy,
			ModifiedName = @ModifiedName
          WHERE RecordStatus != 99 AND ExcelKey = @ExcelKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ExcelKey", SqlDbType.Int).Value = _ExcelKey;
                if (_DateImport == DateTime.MinValue)
                    zCommand.Parameters.Add("@DateImport", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateImport", SqlDbType.DateTime).Value = _DateImport;
                zCommand.Parameters.Add("@File", SqlDbType.NVarChar).Value = File.Trim();
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"UPDATE dbo.Temp_Excel SET
                  [RecordStatus] = 99,
                  [ModifiedOn] = GETDATE(),
                  [ModifiedBy] = @ModifiedBy,
                  [ModifiedName] = @ModifiedName
                  WHERE[ExcelKey] = @ExcelKey 
        ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ExcelKey", SqlDbType.Int).Value = _ExcelKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string DeleteAll()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"
UPDATE Temp_Excel SET [RecordStatus] = 99, [ModifiedOn] = GETDATE(), ModifiedBy = @ModifiedBy, ModifiedName = @ModifiedName WHERE ExcelKey = @ExcelKey
UPDATE Temp_Excel_Sheet SET [RecordStatus] = 99, [ModifiedOn] = GETDATE(), ModifiedBy = @ModifiedBy, ModifiedName = @ModifiedName WHERE ExcelKey = @ExcelKey
UPDATE Temp_Excel_Detail SET [RecordStatus] = 99, [ModifiedOn] = GETDATE(), ModifiedBy = @ModifiedBy, ModifiedName = @ModifiedName WHERE ExcelKey = @ExcelKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ExcelKey", SqlDbType.Int).Value = _ExcelKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "";
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_ExcelKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        #endregion
    }
}
