﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
public class Temp_Excel_Detail_Data
{     
    public static DataTable LoadExcelData(int ExcelKey)
    {
        DataTable zTable = new DataTable();
        string zSQL = @" 
SELECT * FROM 
Temp_Excel_Detail
WHERE ExcelKey=@ExcelKey AND RecordStatus <> 99";
        string zConnectionString = ConnectDataBase.ConnectionString;
        try
        {
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
            zCommand.Parameters.Add("@ExcelKey", SqlDbType.Int).Value = ExcelKey;
            SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
            zAdapter.Fill(zTable);
            zCommand.Dispose();
            zConnect.Close();
        }
        catch (Exception ex)
        {
            string zstrMessage = ex.ToString();
        }
        return zTable;
    }

    public static string Delete_WK_Time_Permit(int ExcelKey)
    {
        string zResult = "";
        string zSQL = @"
UPDATE dbo.HRM_Time_Working SET RecordStatus = 99 WHERE WorkingID = @ExcelKey  
UPDATE dbo.HRM_Employee_Time SET RecordStatus = 99 WHERE ExcelKey = @ExcelKey 
UPDATE dbo.HRM_Employee_Rice SET RecordStatus= 99 WHERE ExcelKey = @ExcelKey 
";
        string zConnectionString = ConnectDataBase.ConnectionString;
        try
        {
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
            zCommand.Parameters.Add("@ExcelKey", SqlDbType.Int).Value = ExcelKey;
            SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
            zCommand.Dispose();
            zConnect.Close();
        }
        catch (Exception ex)
        {
            string Mesages = ex.ToString();
        }
        return zResult;
    }
    public static int Count_Key_Rice(string RiceName)
    {
        int zResult = 0;
        string zSQL = @"SELECT COUNT(RiceKey) FROM [dbo].[HRM_Time_Rice] WHERE RiceName = @RiceName AND RecordStatus <> 99";
        string zConnectionString = ConnectDataBase.ConnectionString;
        try
        {
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
            zCommand.Parameters.Add("@RiceName", SqlDbType.NVarChar).Value = RiceName.Trim();
            SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
            zResult = int.Parse(zCommand.ExecuteScalar().ToString());
            zCommand.Dispose();
            zConnect.Close();
        }
        catch (Exception ex)
        {
            string zMessage = ex.ToString();
        }
        return zResult;
    }
    public static float Get_Money_Rice(string RiceName)
    {
        float zResult = 0;
        string zSQL = @"SELECT ISNULL([Money],0) FROM [dbo].[HRM_Time_Rice] WHERE RiceName = @RiceName AND RecordStatus <> 99";
        string zConnectionString = ConnectDataBase.ConnectionString;
        try
        {
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
            zCommand.Parameters.Add("@RiceName", SqlDbType.NVarChar).Value = RiceName.Trim();
            SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
            if (float.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
            {

            }
            zCommand.Dispose();
            zConnect.Close();
        }
        catch (Exception ex)
        {
            string zMessage = ex.ToString();
        }
        return zResult;
    }
    public static DataTable ListDetail(int SheetKey)
    {
        DataTable zTable = new DataTable();
        string zSQL = @"
SELECT * 
FROM Temp_Excel_Detail 
WHERE  SheetKey = @SheetKey  AND RecordStatus <> 99 
ORDER BY TeamID, LEN(EmployeeID), EmployeeID";
        string zConnectionString = ConnectDataBase.ConnectionString;
        try
        {
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
            zCommand.Parameters.Add("@SheetKey", SqlDbType.Int).Value = SheetKey;
            SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
            zAdapter.Fill(zTable);
            zCommand.Dispose();
            zConnect.Close();
        }
        catch (Exception ex)
        {
            string zstrMessage = ex.ToString();
        }
        return zTable;
    }

}
