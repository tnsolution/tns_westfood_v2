﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
using TNS.SYS;

namespace TNS.SLR
{
    public class CodeReportOffice_Data
    {
        public static DataTable List(DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT AutoKey,ID,Name,TypeName,Rank,Publish,Publish_Close,Description  FROM SLR_CodeReportOffice WHERE RecordStatus != 99 
                            AND DateWrite BETWEEN @FromDate AND @ToDate  
                            ORDER BY [RANK]";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static string SaveLog_CodeReportOffice()
        {
            string zResult = "";
            string zSQL = @"
DECLARE @Parent int =0
INSERT INTO [SLR_CodeReportOffice_Log]
([DateTime],[Parent],[ID],[Name],[Description],[Type],[TypeName],CategoryKey,CategoryName,Publish,Publish_Close,
[Rank],[RecordStatus],[CreatedOn],[CreatedBy],[CreatedName],
 [ModifiedOn],[ModifiedBy],[ModifiedName])
 VALUES
(GETDATE(),0,'','','',0,'',0,'',0,0,
0,0,getdate(),@CreatedBy,@CreatedName,
GETDATE(),'','')

SELECT  @Parent =AutoKey FROM [SLR_CodeReportOffice_Log] WHERE AutoKey = SCOPE_IDENTITY()
--Tạo key cha xong --> update list theo 
INSERT INTO [SLR_CodeReportOffice_Log]
([DateTime],[Parent],[ID],[Name],[Description],[Type],[TypeName],CategoryKey,CategoryName,Publish,Publish_Close,
 [Rank],[RecordStatus],[CreatedOn],[CreatedBy],[CreatedName],
 [ModifiedOn],[ModifiedBy],[ModifiedName])
SELECT 
GETDATE(),@Parent,[ID],[Name],[Description],[Type],TypeName,CategoryKey,CategoryName,Publish,Publish_Close,
 [Rank],[RecordStatus],[CreatedOn],[CreatedBy],[CreatedName],
 [ModifiedOn],[ModifiedBy],[ModifiedName]
FROM SLR_CodeReportOffice WHERE RecordStatus != 99 ORDER BY [RANK]
                ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = SessionUser.UserLogin.Key;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = SessionUser.UserLogin.EmployeeName;
                zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                zResult = "08" + ex.ToString();
            }
            return zResult;
        }
    }
}

