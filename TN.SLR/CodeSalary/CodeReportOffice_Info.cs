﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
using TNS.Misc;

namespace TNS.SLR
{
    public class CodeReportOffice_Info
    {

        #region [ Field Name ]
        private int _AutoKey = 0;
        private DateTime _DateWrite;
        private string _ID = "";
        private string _Name = "";
        private string _Description = "";
        private int _Type = 0;
        private string _TypeName = "";
        private int _CategoryKey = 0;
        private string _CategoryName = "";
        private int _Rank = 0;
        private int _Publish = 0;
        private int _Publish_Close = 0;
        private int _RecordStatus = 0;
        private DateTime _CreatedOn;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _RoleID = "";
        private string _Message = "";
        #endregion
        #region [ Properties ]
        public bool IsCommandOK
        {
            get
            {
                int zNumber = 0;
                int.TryParse(_Message.Substring(0, 1), out zNumber);
                if (zNumber <= 3) return true; else return false;
            }
        }
       
        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        public int Key
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public string ID
        {
            get { return _ID; }
            set { _ID = value; }
        }
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public int Type
        {
            get { return _Type; }
            set { _Type = value; }
        }
        public string TypeName
        {
            get { return _TypeName; }
            set { _TypeName = value; }
        }
        public int CategoryKey
        {
            get { return _CategoryKey; }
            set { _CategoryKey = value; }
        }
        public string CategoryName
        {
            get { return _CategoryName; }
            set { _CategoryName = value; }
        }
        public int Rank
        {
            get { return _Rank; }
            set { _Rank = value; }
        }
        public int Publish
        {
            get { return _Publish; }
            set { _Publish = value; }
        }
        public int Publish_Close
        {
            get { return _Publish_Close; }
            set { _Publish_Close = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public DateTime DateWrite
        {
            get
            {
                return _DateWrite;
            }

            set
            {
                _DateWrite = value;
            }
        }
        #endregion
        #region [ Constructor Get Information ]
        public CodeReportOffice_Info()
        {
        }
        public CodeReportOffice_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM SLR_CodeReportOffice WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    if (zReader["DateWrite"] != DBNull.Value)
                        _DateWrite = (DateTime)zReader["DateWrite"];
                    _ID = zReader["ID"].ToString().Trim();
                    _Name = zReader["Name"].ToString().Trim();
                    _Description = zReader["Description"].ToString().Trim();
                    if (zReader["Type"] != DBNull.Value)
                        _Type = int.Parse(zReader["Type"].ToString());
                    _TypeName = zReader["TypeName"].ToString().Trim();
                    if (zReader["CategoryKey"] != DBNull.Value)
                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    _CategoryName = zReader["CategoryName"].ToString().Trim();
                    if (zReader["Rank"] != DBNull.Value)
                        _Rank = int.Parse(zReader["Rank"].ToString());
                    if (zReader["Publish"] != DBNull.Value)
                        _Publish = int.Parse(zReader["Publish"].ToString());
                    if (zReader["Publish_Close"] != DBNull.Value)
                        _Publish_Close = int.Parse(zReader["Publish_Close"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = @"UPDATE [dbo].[SLR_CodeReportOffice] SET [Rank]=[Rank] +1 WHERE [Rank] >=@Rank AND RecordStatus <>99 AND DateWrite BETWEEN @FromDate AND @ToDate
         INSERT INTO SLR_CodeReportOffice (
         DateWrite,ID ,Name ,Description ,Type ,TypeName ,CategoryKey ,CategoryName ,Rank ,Publish ,Publish_Close ,RecordStatus ,CreatedOn ,CreatedBy ,CreatedName ,ModifiedOn ,ModifiedBy ,ModifiedName ) 
         VALUES ( 
         @DateWrite,@ID ,@Name ,@Description ,@Type ,@TypeName ,@CategoryKey ,@CategoryName ,@Rank ,@Publish ,@Publish_Close ,@RecordStatus ,GETDATE(),@CreatedBy ,@CreatedName ,GETDATE(),@ModifiedBy ,@ModifiedName ) ";
            zSQL += " SELECT AutoKey FROM SLR_CodeReportOffice WHERE AutoKey = SCOPE_IDENTITY() ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                var zTime = _DateWrite.FirstEndMonth();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = _DateWrite;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zTime.Item1;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zTime.Item2;

                zCommand.Parameters.Add("@ID", SqlDbType.NVarChar).Value = _ID.Trim();
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = _Name.Trim();
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                zCommand.Parameters.Add("@Type", SqlDbType.Int).Value = _Type;
                zCommand.Parameters.Add("@TypeName", SqlDbType.NVarChar).Value = _TypeName.Trim();
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName.Trim();
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zCommand.Parameters.Add("@Publish", SqlDbType.Int).Value = _Publish;
                zCommand.Parameters.Add("@Publish_Close", SqlDbType.Int).Value = _Publish_Close;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE SLR_CodeReportOffice SET "
                        + " DateWrite = @DateWrite,"
                        + " ID = @ID,"
                        + " Name = @Name,"
                        + " Description = @Description,"
                        + " Type = @Type,"
                        + " TypeName = @TypeName,"
                        + " CategoryKey = @CategoryKey,"
                        + " CategoryName = @CategoryName,"
                        + " Rank = @Rank,"
                        + " Publish = @Publish,"
                        + " Publish_Close = @Publish_Close,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = GETDATE(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                       + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = _DateWrite;
                zCommand.Parameters.Add("@ID", SqlDbType.NVarChar).Value = _ID.Trim();
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = _Name.Trim();
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                zCommand.Parameters.Add("@Type", SqlDbType.Int).Value = _Type;
                zCommand.Parameters.Add("@TypeName", SqlDbType.NVarChar).Value = _TypeName.Trim();
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName.Trim();
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zCommand.Parameters.Add("@Publish", SqlDbType.Int).Value = _Publish;
                zCommand.Parameters.Add("@Publish_Close", SqlDbType.Int).Value = _Publish_Close;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"UPDATE [dbo].[SLR_CodeReportOffice] SET [Rank]=[Rank] -1 WHERE [Rank] >@Rank AND RecordStatus <>99 AND DateWrite BETWEEN @FromDate AND @ToDate

                UPDATE [dbo].[SLR_CodeReportOffice] SET RecordStatus =99 WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                var zTime = _DateWrite.FirstEndMonth();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zTime.Item1;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zTime.Item2;

                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_AutoKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        #endregion
    }
}
