﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
namespace TNS.SLR
{
    public class ReportSupport_Close_Info
    {

        #region [ Field Name ]
        private string _AutoKey = "";
        private string _Parent = "";
        private DateTime _DateWrite;
        private string _EmployeeKey = "";
        private string _EmployeeID = "";
        private string _EmployeeName = "";
        private int _TeamKey = 0;
        private string _TeamID = "";
        private string _TeamName = "";
        private int _CodeKey = 0;
        private string _CodeID = "";
        private string _CodeName = "";
        private float _Number;
        private float _Amount;
        private string _RoleID = "";
        private string _Message = "";
        private int _Type = 0;
        private string _TypeName = "";
        private int _CategoryKey = 0;
        private string _CategoryName = "";
        private int _RecordStatus = 0;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _CreatedOn;
        private DateTime _ModifiedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        #endregion
        #region [ Properties ]

        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        public string Key
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public DateTime DateWrite
        {
            get { return _DateWrite; }
            set { _DateWrite = value; }
        }
        public string EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public string EmployeeID
        {
            get { return _EmployeeID; }
            set { _EmployeeID = value; }
        }
        public string EmployeeName
        {
            get { return _EmployeeName; }
            set { _EmployeeName = value; }
        }
        public int TeamKey
        {
            get { return _TeamKey; }
            set { _TeamKey = value; }
        }
        public string TeamID
        {
            get { return _TeamID; }
            set { _TeamID = value; }
        }
        public string TeamName
        {
            get { return _TeamName; }
            set { _TeamName = value; }
        }
        public int CodeKey
        {
            get { return _CodeKey; }
            set { _CodeKey = value; }
        }
        public string CodeID
        {
            get { return _CodeID; }
            set { _CodeID = value; }
        }
        public string CodeName
        {
            get { return _CodeName; }
            set { _CodeName = value; }
        }
        public float Number
        {
            get { return _Number; }
            set { _Number = value; }
        }
        public float Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public int RecordStatus
        {
            get
            {
                return _RecordStatus;
            }

            set
            {
                _RecordStatus = value;
            }
        }

        public string CreatedBy
        {
            get
            {
                return _CreatedBy;
            }

            set
            {
                _CreatedBy = value;
            }
        }

        public string CreatedName
        {
            get
            {
                return _CreatedName;
            }

            set
            {
                _CreatedName = value;
            }
        }

        public DateTime CreatedOn
        {
            get
            {
                return _CreatedOn;
            }

            set
            {
                _CreatedOn = value;
            }
        }

        public DateTime ModifiedOn
        {
            get
            {
                return _ModifiedOn;
            }

            set
            {
                _ModifiedOn = value;
            }
        }

        public string ModifiedBy
        {
            get
            {
                return _ModifiedBy;
            }

            set
            {
                _ModifiedBy = value;
            }
        }

        public string ModifiedName
        {
            get
            {
                return _ModifiedName;
            }

            set
            {
                _ModifiedName = value;
            }
        }

        public int Type
        {
            get
            {
                return _Type;
            }

            set
            {
                _Type = value;
            }
        }

        public string Parent
        {
            get
            {
                return _Parent;
            }

            set
            {
                _Parent = value;
            }
        }

        public string TypeName
        {
            get
            {
                return _TypeName;
            }

            set
            {
                _TypeName = value;
            }
        }

        public int CategoryKey
        {
            get
            {
                return _CategoryKey;
            }

            set
            {
                _CategoryKey = value;
            }
        }

        public string CategoryName
        {
            get
            {
                return _CategoryName;
            }

            set
            {
                _CategoryName = value;
            }
        }
        #endregion
        #region [ Constructor Get Information ]
        public ReportSupport_Close_Info()
        {
        }
        public ReportSupport_Close_Info(string AutoKey)
        {
            string zSQL = "SELECT * FROM SLR_ReportSupport_Close WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if(AutoKey.Trim().Length !=36)
                {
                    zCommand.Parameters.Add("@AutoKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@AutoKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(AutoKey);
                }
             
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                        _AutoKey = zReader["AutoKey"].ToString();
                    if (zReader["DateWrite"] != DBNull.Value)
                        _DateWrite = (DateTime)zReader["DateWrite"];
                    _EmployeeKey = zReader["EmployeeKey"].ToString();
                    _EmployeeID = zReader["EmployeeID"].ToString().Trim();
                    _EmployeeName = zReader["EmployeeName"].ToString().Trim();
                    if (zReader["TeamKey"] != DBNull.Value)
                        _TeamKey = int.Parse(zReader["TeamKey"].ToString());
                    _TeamID = zReader["TeamID"].ToString().Trim();
                    _TeamName = zReader["TeamName"].ToString().Trim();
                    if (zReader["CodeKey"] != DBNull.Value)
                        _CodeKey = int.Parse(zReader["CodeKey"].ToString());
                    if (zReader["Type"] != DBNull.Value)
                        _Type = int.Parse(zReader["Type"].ToString());
                    _TypeName = zReader["TypeName"].ToString().Trim();
                    if (zReader["CategoryKey"] != DBNull.Value)
                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    _CategoryName = zReader["CategoryName"].ToString().Trim();
                    _CodeID = zReader["CodeID"].ToString().Trim();
                    _CodeName = zReader["CodeName"].ToString().Trim();
                    if (zReader["Number"] != DBNull.Value)
                        _Number = float.Parse(zReader["Number"].ToString());
                    if (zReader["Amount"] != DBNull.Value)
                        _Amount = float.Parse(zReader["Amount"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        public void GetParent_Info(DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            string zSQL = "SELECT * FROM SLR_ReportSupport_Close WHERE DateWrite BETWEEN @FromDate AND @ToDate AND RecordStatus <> 99 AND Parent is null ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _AutoKey = zReader["AutoKey"].ToString();
                    if (zReader["DateWrite"] != DBNull.Value)
                        _DateWrite = (DateTime)zReader["DateWrite"];
                    _EmployeeKey = zReader["EmployeeKey"].ToString();
                    _EmployeeID = zReader["EmployeeID"].ToString().Trim();
                    _EmployeeName = zReader["EmployeeName"].ToString().Trim();
                    if (zReader["TeamKey"] != DBNull.Value)
                        _TeamKey = int.Parse(zReader["TeamKey"].ToString());
                    _TeamID = zReader["TeamID"].ToString().Trim();
                    _TeamName = zReader["TeamName"].ToString().Trim();
                    if (zReader["CodeKey"] != DBNull.Value)
                        _CodeKey = int.Parse(zReader["CodeKey"].ToString());
                    if (zReader["Type"] != DBNull.Value)
                        _Type = int.Parse(zReader["Type"].ToString());
                    _TypeName = zReader["TypeName"].ToString().Trim();
                    if (zReader["CategoryKey"] != DBNull.Value)
                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    _CategoryName = zReader["CategoryName"].ToString().Trim();
                    _CodeID = zReader["CodeID"].ToString().Trim();
                    _CodeName = zReader["CodeName"].ToString().Trim();
                    if (zReader["Number"] != DBNull.Value)
                        _Number = float.Parse(zReader["Number"].ToString());
                    if (zReader["Amount"] != DBNull.Value)
                        _Amount = float.Parse(zReader["Amount"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO SLR_ReportSupport_Close ("
        + " AutoKey, Parent, DateWrite ,EmployeeKey ,EmployeeID ,EmployeeName ,TeamKey ,TeamID ,TeamName ,CodeKey ,CodeID ,CodeName ,Number ,Amount,RecordStatus,Type,TypeName ,CategoryKey,CategoryName, CreatedOn,CreatedBy ,CreatedName ,ModifiedOn ,ModifiedBy ,ModifiedName ) "
         + " VALUES ( "
         + " @AutoKey ,@Parent, @DateWrite ,@EmployeeKey ,@EmployeeID ,@EmployeeName ,@TeamKey ,@TeamID ,@TeamName ,@CodeKey ,@CodeID ,@CodeName ,@Number ,@Amount,0,@Type,@TypeName ,@CategoryKey,@CategoryName,GETDATE(),@CreatedBy ,@CreatedName ,GETDATE(),@ModifiedBy ,@ModifiedName ) ";
            zSQL += " SELECT 11";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                Guid zNewID = Guid.NewGuid();
                _AutoKey = zNewID.ToString();
                if (_AutoKey.Trim().Length != 36)
                {
                    zCommand.Parameters.Add("@AutoKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@AutoKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_AutoKey);
                }
                if (_Parent.Length > 0)
                    zCommand.Parameters.Add("@Parent", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_Parent);
                else
                    zCommand.Parameters.Add("@Parent", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                if (_DateWrite == DateTime.MinValue)
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = _DateWrite;
                if (_EmployeeKey.Length > 0)
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_EmployeeKey);
                else
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID.Trim();
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName.Trim();
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = _TeamKey;
                zCommand.Parameters.Add("@TeamID", SqlDbType.NVarChar).Value = _TeamID.Trim();
                zCommand.Parameters.Add("@TeamName", SqlDbType.NVarChar).Value = _TeamName.Trim();
                zCommand.Parameters.Add("@CodeKey", SqlDbType.Int).Value = _CodeKey;
                zCommand.Parameters.Add("@CodeID", SqlDbType.NVarChar).Value = _CodeID.Trim();
                zCommand.Parameters.Add("@CodeName", SqlDbType.NVarChar).Value = _CodeName.Trim();
                zCommand.Parameters.Add("@Number", SqlDbType.Float).Value = _Number;
                zCommand.Parameters.Add("@Amount", SqlDbType.Float).Value = _Amount;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zCommand.Parameters.Add("RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("Type", SqlDbType.Int).Value = _Type;
                zCommand.Parameters.Add("@TypeName", SqlDbType.NVarChar).Value = _TypeName.Trim();
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName.Trim();
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                _Message = zCommand.ExecuteScalar().ToString();
                if (_Message.Substring(0, 2) != "11")
                    _AutoKey = "";
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE SLR_ReportSupport_Close SET "
                        + " DateWrite = @DateWrite,"
                        + " Parent = @Parent,"
                        + " EmployeeKey = @EmployeeKey,"
                        + " EmployeeID = @EmployeeID,"
                        + " EmployeeName = @EmployeeName,"
                        + " TeamKey = @TeamKey,"
                        + " TeamID = @TeamID,"
                        + " TeamName = @TeamName,"
                        + " CodeKey = @CodeKey,"
                        + " CodeID = @CodeID,"
                        + " CodeName = @CodeName,"
                        + " Number = @Number,"
                        + " Amount = @Amount,"
                        + " Type = @Type,"
                        + " TypeName = @TypeName,"
                        + " CategoryKey = @CategoryKey,"
                        + " CategoryName = @CategoryName,"
                        + " ModifiedOn = GETDATE(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                       + " WHERE AutoKey = @AutoKey SELECT 20";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (_AutoKey.Trim().Length != 36)
                {
                    zCommand.Parameters.Add("@AutoKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@AutoKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_AutoKey);
                }
                if (_Parent.Length > 0)
                    zCommand.Parameters.Add("@Parent", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_Parent);
                else
                    zCommand.Parameters.Add("@Parent", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                if (_DateWrite == DateTime.MinValue)
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = _DateWrite;
                if (_EmployeeKey.Length == 36)
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_EmployeeKey);
                else
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID.Trim();
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName.Trim();
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = _TeamKey;
                zCommand.Parameters.Add("@TeamID", SqlDbType.NVarChar).Value = _TeamID.Trim();
                zCommand.Parameters.Add("@TeamName", SqlDbType.NVarChar).Value = _TeamName.Trim();
                zCommand.Parameters.Add("@CodeKey", SqlDbType.Int).Value = _CodeKey;
                zCommand.Parameters.Add("@CodeID", SqlDbType.NVarChar).Value = _CodeID.Trim();
                zCommand.Parameters.Add("@CodeName", SqlDbType.NVarChar).Value = _CodeName.Trim();
                zCommand.Parameters.Add("@Number", SqlDbType.Float).Value = _Number;
                zCommand.Parameters.Add("@Amount", SqlDbType.Float).Value = _Amount;
                zCommand.Parameters.Add("Type", SqlDbType.Int).Value = _Type;
                zCommand.Parameters.Add("@TypeName", SqlDbType.NVarChar).Value = _TypeName.Trim();
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE SLR_ReportSupport_Close SET [RecordStatus] = 99, [ModifiedOn] = GETDATE(), ModifiedBy = @ModifiedBy, ModifiedName = @ModifiedName WHERE AutoKey = @AutoKey SELECT 30";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (_AutoKey.Trim().Length != 36)
                {
                    zCommand.Parameters.Add("@AutoKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@AutoKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_AutoKey);
                }
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete_Team(int TeamKey,DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month,1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"DELETE FROM SLR_ReportSupport_Close 
WHERE TeamKey = @TeamKey AND DateWrite between @FromDate and @ToDate SELECT 30";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_AutoKey == "")
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        //Funtion xóa và tạo mới
        public string CreateClose(DateTime DateWrite, string CreatedBy, string CreatedName, string Parent)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);

            //---------- String SQL Access Database ---------------
            string zSQL = @"
            DELETE FROM [dbo].[SLR_ReportSupport_Close] WHERE DateWrite BETWEEN  @FromDate AND @ToDate AND Parent = @Parent    ";

            zSQL += @"INSERT INTO SLR_ReportSupport_Close (
            AutoKey, Parent ,DateWrite ,EmployeeKey ,EmployeeID ,EmployeeName ,
            BranchKey,BranchID,BranchName,DepartmentKey,DepartmentID,DepartmentName,
            TeamKey ,TeamID ,TeamName ,PositionKey,PositionID,PositionName,ATM,
            CodeKey ,CodeID ,CodeName ,Number ,Amount,RecordStatus,
            Type,TypeName,CategoryKey,CategoryName,Publish,Publish_Close,[Rank],
            CreatedOn,CreatedBy ,CreatedName ,ModifiedOn ,ModifiedBy ,ModifiedName ) 
            SELECT 
            newid(), @Parent, DateWrite ,EmployeeKey ,EmployeeID ,EmployeeName ,
            BranchKey,BranchID,BranchName,DepartmentKey,DepartmentID,DepartmentName,
            TeamKey ,TeamID ,TeamName ,PositionKey,PositionID,PositionName,ATM,
            CodeKey ,CodeID ,CodeName ,Number ,Amount,0,
            Type,TypeName,CategoryKey,CategoryName,Publish,Publish_Close,[Rank],
            GETDATE(),@CreatedBy ,@CreatedName ,GETDATE(),@CreatedBy ,@CreatedName 
            FROM SLR_ReportSupport WHERE DateWrite BETWEEN  @FromDate AND @ToDate AND RecordStatus <> 99  ";
            zSQL += " SELECT 11";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
               // zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                if (Parent.Length > 0)
                    zCommand.Parameters.Add("@Parent", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Parent);
                else
                    zCommand.Parameters.Add("@Parent", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = CreatedName.Trim();
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        ////Funtion xóa và tạo mới
        //public string CreateClose(DateTime DateWrite, int TeamKey, string CreatedBy, string CreatedName, string Parent)
        //{
        //    DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
        //    DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
        //    zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);

        //    //---------- String SQL Access Database ---------------
        //    string zSQL = @"
        //    DELETE FROM [dbo].[SLR_ReportSupport_Close] WHERE DateWrite BETWEEN  @FromDate AND @ToDate AND Parent is not null ";
        //    if (TeamKey > 0)
        //        zSQL += " AND TeamKey= @TeamKey ";
        //    zSQL += @"INSERT INTO SLR_ReportSupport_Close (
        //    AutoKey, Parent ,DateWrite ,EmployeeKey ,EmployeeID ,EmployeeName ,TeamKey ,TeamID ,TeamName ,
        //    CodeKey ,CodeID ,CodeName ,Number ,Amount,RecordStatus,Type,TypeName,Rank,
        //    CreatedOn,CreatedBy ,CreatedName ,ModifiedOn ,ModifiedBy ,ModifiedName ) 
        //    SELECT 
        //    newid(), @Parent, DateWrite ,EmployeeKey ,EmployeeID ,EmployeeName ,TeamKey ,TeamID ,TeamName ,
        //    CodeKey ,CodeID ,CodeName ,Number ,Amount,0,Type,TypeName,Rank,
        //    GETDATE(),@CreatedBy ,@CreatedName ,GETDATE(),@CreatedBy ,@CreatedName 
        //    FROM SLR_ReportSupport WHERE DateWrite BETWEEN  @FromDate AND @ToDate AND RecordStatus <> 99  ";
        //    if (TeamKey > 0)
        //        zSQL += " AND TeamKey= @TeamKey ";
        //    zSQL += " SELECT 11";
        //    string zResult = "";
        //    string zConnectionString = ConnectDataBase.ConnectionString;
        //    SqlConnection zConnect = new SqlConnection(zConnectionString);
        //    zConnect.Open();
        //    try
        //    {
        //        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //        zCommand.CommandType = CommandType.Text;
        //        zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
        //        if (Parent.Length > 0)
        //            zCommand.Parameters.Add("@Parent", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Parent);
        //        else
        //            zCommand.Parameters.Add("@Parent", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
        //        zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
        //        zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
        //        zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = CreatedBy.Trim();
        //        zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = CreatedName.Trim();
        //        _Message = zCommand.ExecuteScalar().ToString();
        //        zCommand.Dispose();
        //    }
        //    catch (Exception Err)
        //    {
        //        _Message = "08" + Err.ToString();
        //    }
        //    finally
        //    {
        //        zConnect.Close();
        //    }
        //    return zResult;
        //}
        #endregion
    }
}
