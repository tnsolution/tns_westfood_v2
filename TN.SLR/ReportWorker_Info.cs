﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
namespace TNS.SLR
{
    public class ReportWorker_Info
    {

        #region [ Field Name ]
        private string _AutoKey = "";
        private DateTime _DateWrite;
        private string _EmployeeKey = "";
        private string _EmployeeID = "";
        private string _EmployeeName = "";
        private int _BranchKey = 0;
        private string _BranchID = "";
        private string _BranchName = "";
        private int _DepartmentKey = 0;
        private string _DepartmentID = "";
        private string _DepartmentName = "";
        private int _TeamKey = 0;
        private string _TeamID = "";
        private string _TeamName = "";
        private int _PositionKey = 0;
        private string _PositionID = "";
        private string _PositionName = "";
        private string _ATM = "";
        private int _CodeKey = 0;
        private string _CodeID = "";
        private string _CodeName = "";
        private float _Number;
        private double _Amount;
        private string _RoleID = "";
        private string _Message = "";
        private int _Type = 0;
        private string _TypeName = "";
        private int _CategoryKey = 0;
        private string _CategoryName = "";
        private int _Publish = 0;
        private int _Publish_Close = 0;
        private int _Rank = 0;
        private int _RecordStatus = 0;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _CreatedOn;
        private DateTime _ModifiedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        #endregion
        #region [ Properties ]

        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        public string Key
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public DateTime DateWrite
        {
            get { return _DateWrite; }
            set { _DateWrite = value; }
        }
        public string EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public string EmployeeID
        {
            get { return _EmployeeID; }
            set { _EmployeeID = value; }
        }
        public string EmployeeName
        {
            get { return _EmployeeName; }
            set { _EmployeeName = value; }
        }
        public int TeamKey
        {
            get { return _TeamKey; }
            set { _TeamKey = value; }
        }
        public string TeamID
        {
            get { return _TeamID; }
            set { _TeamID = value; }
        }
        public string TeamName
        {
            get { return _TeamName; }
            set { _TeamName = value; }
        }
        public int CodeKey
        {
            get { return _CodeKey; }
            set { _CodeKey = value; }
        }
        public string CodeID
        {
            get { return _CodeID; }
            set { _CodeID = value; }
        }
        public string CodeName
        {
            get { return _CodeName; }
            set { _CodeName = value; }
        }
        public float Number
        {
            get { return _Number; }
            set { _Number = value; }
        }
        public double Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public int RecordStatus
        {
            get
            {
                return _RecordStatus;
            }

            set
            {
                _RecordStatus = value;
            }
        }

        public string CreatedBy
        {
            get
            {
                return _CreatedBy;
            }

            set
            {
                _CreatedBy = value;
            }
        }

        public string CreatedName
        {
            get
            {
                return _CreatedName;
            }

            set
            {
                _CreatedName = value;
            }
        }

        public DateTime CreatedOn
        {
            get
            {
                return _CreatedOn;
            }

            set
            {
                _CreatedOn = value;
            }
        }

        public DateTime ModifiedOn
        {
            get
            {
                return _ModifiedOn;
            }

            set
            {
                _ModifiedOn = value;
            }
        }

        public string ModifiedBy
        {
            get
            {
                return _ModifiedBy;
            }

            set
            {
                _ModifiedBy = value;
            }
        }

        public string ModifiedName
        {
            get
            {
                return _ModifiedName;
            }

            set
            {
                _ModifiedName = value;
            }
        }

        public int Type
        {
            get
            {
                return _Type;
            }

            set
            {
                _Type = value;
            }
        }

        public int BranchKey
        {
            get
            {
                return _BranchKey;
            }

            set
            {
                _BranchKey = value;
            }
        }

        public string BranchID
        {
            get
            {
                return _BranchID;
            }

            set
            {
                _BranchID = value;
            }
        }

        public string BranchName
        {
            get
            {
                return _BranchName;
            }

            set
            {
                _BranchName = value;
            }
        }

        public int DepartmentKey
        {
            get
            {
                return _DepartmentKey;
            }

            set
            {
                _DepartmentKey = value;
            }
        }

        public string DepartmentID
        {
            get
            {
                return _DepartmentID;
            }

            set
            {
                _DepartmentID = value;
            }
        }

        public string DepartmentName
        {
            get
            {
                return _DepartmentName;
            }

            set
            {
                _DepartmentName = value;
            }
        }

        public int PositionKey
        {
            get
            {
                return _PositionKey;
            }

            set
            {
                _PositionKey = value;
            }
        }

        public string PositionID
        {
            get
            {
                return _PositionID;
            }

            set
            {
                _PositionID = value;
            }
        }

        public string PositionName
        {
            get
            {
                return _PositionName;
            }

            set
            {
                _PositionName = value;
            }
        }

        public string ATM
        {
            get
            {
                return _ATM;
            }

            set
            {
                _ATM = value;
            }
        }

        public string TypeName
        {
            get
            {
                return _TypeName;
            }

            set
            {
                _TypeName = value;
            }
        }

        public int Publish
        {
            get
            {
                return _Publish;
            }

            set
            {
                _Publish = value;
            }
        }

        public int Publish_Close
        {
            get
            {
                return _Publish_Close;
            }

            set
            {
                _Publish_Close = value;
            }
        }

        public int Rank
        {
            get
            {
                return _Rank;
            }

            set
            {
                _Rank = value;
            }
        }

        public int CategoryKey
        {
            get
            {
                return _CategoryKey;
            }

            set
            {
                _CategoryKey = value;
            }
        }

        public string CategoryName
        {
            get
            {
                return _CategoryName;
            }

            set
            {
                _CategoryName = value;
            }
        }
        #endregion
        #region [ Constructor Get Information ]
        public ReportWorker_Info()
        {
        }
        public ReportWorker_Info(string AutoKey)
        {
            string zSQL = "SELECT * FROM SLR_ReportWorker WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if(AutoKey.Trim().Length !=36)
                {
                    zCommand.Parameters.Add("@AutoKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@AutoKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(AutoKey);
                }
             
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                        _AutoKey = zReader["AutoKey"].ToString();
                    if (zReader["DateWrite"] != DBNull.Value)
                        _DateWrite = (DateTime)zReader["DateWrite"];
                    _EmployeeKey = zReader["EmployeeKey"].ToString();
                    _EmployeeID = zReader["EmployeeID"].ToString().Trim();
                    _EmployeeName = zReader["EmployeeName"].ToString().Trim();
                    if (zReader["BranchKey"] != DBNull.Value)
                        _BranchKey = int.Parse(zReader["BranchKey"].ToString());
                    _BranchID = zReader["BranchID"].ToString().Trim();
                    _BranchName = zReader["BranchName"].ToString().Trim();
                    if (zReader["DepartmentKey"] != DBNull.Value)
                        _DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    _DepartmentID = zReader["DepartmentID"].ToString().Trim();
                    _DepartmentName = zReader["DepartmentName"].ToString().Trim();
                    if (zReader["TeamKey"] != DBNull.Value)
                        _TeamKey = int.Parse(zReader["TeamKey"].ToString());
                    _TeamID = zReader["TeamID"].ToString().Trim();
                    _TeamName = zReader["TeamName"].ToString().Trim();
                    if (zReader["PositionKey"] != DBNull.Value)
                        _PositionKey = int.Parse(zReader["PositionKey"].ToString());
                    _PositionID = zReader["PositionID"].ToString().Trim();
                    _PositionName = zReader["PositionName"].ToString().Trim();
                    _ATM = zReader["ATM"].ToString().Trim();
                    if (zReader["CodeKey"] != DBNull.Value)
                        _CodeKey = int.Parse(zReader["CodeKey"].ToString());
                    if (zReader["Type"] != DBNull.Value)
                        _Type = int.Parse(zReader["Type"].ToString()); 
                    _TypeName = zReader["TypeName"].ToString().Trim();
                    if (zReader["CategoryKey"] != DBNull.Value)
                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    _CategoryName = zReader["CategoryName"].ToString().Trim();

                    _CodeID = zReader["CodeID"].ToString().Trim();
                    _CodeName = zReader["CodeName"].ToString().Trim();
                    if (zReader["Number"] != DBNull.Value)
                        _Number = float.Parse(zReader["Number"].ToString());
                    if (zReader["Amount"] != DBNull.Value)
                        _Amount = double.Parse(zReader["Amount"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = @"INSERT INTO SLR_ReportWorker (
AutoKey, DateWrite ,EmployeeKey ,EmployeeID ,EmployeeName,BranchKey ,BranchID ,BranchName ,DepartmentKey ,DepartmentID ,DepartmentName ,TeamKey ,TeamID ,TeamName,PositionKey ,PositionID ,PositionName,ATM ,CodeKey ,CodeID ,CodeName ,Number ,Amount,RecordStatus,Type ,TypeName , CategoryKey,CategoryName,Publish ,Publish_Close ,Rank, CreatedOn,CreatedBy ,CreatedName ,ModifiedOn ,ModifiedBy ,ModifiedName ) 
VALUES ( 
@AutoKey,@DateWrite ,@EmployeeKey ,@EmployeeID ,@EmployeeName,@BranchKey ,@BranchID ,@BranchName ,@DepartmentKey ,@DepartmentID ,@DepartmentName  ,@TeamKey ,@TeamID ,@TeamName ,@PositionKey ,@PositionID ,@PositionName,@ATM, @CodeKey ,@CodeID ,@CodeName ,@Number ,@Amount,0,@Type,@TypeName ,@CategoryKey,@CategoryName,@Publish ,@Publish_Close ,@Rank ,GETDATE(),@CreatedBy ,@CreatedName ,GETDATE(),@ModifiedBy ,@ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                Guid zNewID = Guid.NewGuid();
                _AutoKey = zNewID.ToString();
                if (_AutoKey.Trim().Length != 36)
                {
                    zCommand.Parameters.Add("@AutoKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@AutoKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_AutoKey);
                }
                if (_DateWrite == DateTime.MinValue)
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = _DateWrite;
                if (_EmployeeKey.Length > 0)
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_EmployeeKey);
                else
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID.Trim();
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName.Trim();
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = _BranchKey;
                zCommand.Parameters.Add("@BranchID", SqlDbType.NVarChar).Value = _BranchID.Trim();
                zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = _BranchName.Trim();
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@DepartmentID", SqlDbType.NVarChar).Value = _DepartmentID.Trim();
                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = _DepartmentName.Trim();
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = _TeamKey;
                zCommand.Parameters.Add("@TeamID", SqlDbType.NVarChar).Value = _TeamID.Trim();
                zCommand.Parameters.Add("@TeamName", SqlDbType.NVarChar).Value = _TeamName.Trim();
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = _PositionKey;
                zCommand.Parameters.Add("@PositionID", SqlDbType.NVarChar).Value = _PositionID.Trim();
                zCommand.Parameters.Add("@PositionName", SqlDbType.NVarChar).Value = _PositionName.Trim();
                zCommand.Parameters.Add("@ATM", SqlDbType.NVarChar).Value = _ATM.Trim();
                zCommand.Parameters.Add("@CodeKey", SqlDbType.Int).Value = _CodeKey;
                zCommand.Parameters.Add("@CodeID", SqlDbType.NVarChar).Value = _CodeID.Trim();
                zCommand.Parameters.Add("@CodeName", SqlDbType.NVarChar).Value = _CodeName.Trim();
                zCommand.Parameters.Add("@Number", SqlDbType.Float).Value = _Number;
                zCommand.Parameters.Add("@Amount", SqlDbType.Money).Value = _Amount;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zCommand.Parameters.Add("RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("Type", SqlDbType.Int).Value = _Type;
                zCommand.Parameters.Add("@TypeName", SqlDbType.NVarChar).Value = _TypeName.Trim();
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName.Trim();
                zCommand.Parameters.Add("@Publish", SqlDbType.Int).Value = _Publish;
                zCommand.Parameters.Add("@Publish_Close", SqlDbType.Int).Value = _Publish_Close;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                //zCommand.Parameters.Add("@ActionName", SqlDbType.NVarChar).Value = "Tạo mới thông tin lương > Mã thẻ:"+ _EmployeeID+">Họ tên:"+ _EmployeeName+">Mã lương:"+ _CodeID+">Tên lương:"+_CodeName+">Số lượng:"+_Amount;
                zResult = zCommand.ExecuteNonQuery().ToString();
               
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE SLR_ReportWorker SET "
                        + " DateWrite = @DateWrite,"
                        + " EmployeeKey = @EmployeeKey,"
                        + " EmployeeID = @EmployeeID,"
                        + " EmployeeName = @EmployeeName,"
                        + " BranchKey = @BranchKey,"
                        + " BranchID = @BranchID,"
                        + " BranchName = @BranchName,"
                        + " DepartmentKey = @DepartmentKey,"
                        + " DepartmentID = @DepartmentID,"
                        + " DepartmentName = @DepartmentName,"
                        + " TeamKey = @TeamKey,"
                        + " TeamID = @TeamID,"
                        + " TeamName = @TeamName,"
                        + " PositionKey = @PositionKey,"
                        + " PositionID = @PositionID,"
                        + " PositionName = @PositionName,"
                        + " ATM = @ATM,"
                        + " CodeKey = @CodeKey,"
                        + " CodeID = @CodeID,"
                        + " CodeName = @CodeName,"
                        + " Number = @Number,"
                        + " Amount = @Amount,"
                        + " Type = @Type,"
                        + " TypeName = @TypeName,"
                        + " CategoryKey = @CategoryKey,"
                        + " CategoryName = @CategoryName,"
                        + " Publish = @Publish,"
                        + " Publish_Close = @Publish_Close,"
                        + " Rank = @Rank,"
                        + " ModifiedOn = GETDATE(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                       + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (_AutoKey.Trim().Length != 36)
                {
                    zCommand.Parameters.Add("@AutoKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@AutoKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_AutoKey);
                }
                if (_DateWrite == DateTime.MinValue)
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = _DateWrite;
                if (_EmployeeKey.Length == 36)
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_EmployeeKey);
                else
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID.Trim();
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName.Trim();
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = _BranchKey;
                zCommand.Parameters.Add("@BranchID", SqlDbType.NVarChar).Value = _BranchID.Trim();
                zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = _BranchName.Trim();
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@DepartmentID", SqlDbType.NVarChar).Value = _DepartmentID.Trim();
                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = _DepartmentName.Trim();
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = _TeamKey;
                zCommand.Parameters.Add("@TeamID", SqlDbType.NVarChar).Value = _TeamID.Trim();
                zCommand.Parameters.Add("@TeamName", SqlDbType.NVarChar).Value = _TeamName.Trim();
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = _PositionKey;
                zCommand.Parameters.Add("@PositionID", SqlDbType.NVarChar).Value = _PositionID.Trim();
                zCommand.Parameters.Add("@PositionName", SqlDbType.NVarChar).Value = _PositionName.Trim();
                zCommand.Parameters.Add("@ATM", SqlDbType.NVarChar).Value = _ATM.Trim();
                zCommand.Parameters.Add("@CodeKey", SqlDbType.Int).Value = _CodeKey;
                zCommand.Parameters.Add("@CodeID", SqlDbType.NVarChar).Value = _CodeID.Trim();
                zCommand.Parameters.Add("@CodeName", SqlDbType.NVarChar).Value = _CodeName.Trim();
                zCommand.Parameters.Add("@Number", SqlDbType.Float).Value = _Number;
                zCommand.Parameters.Add("@Amount", SqlDbType.Money).Value = _Amount;
                zCommand.Parameters.Add("Type", SqlDbType.Int).Value = _Type;
                zCommand.Parameters.Add("@TypeName", SqlDbType.NVarChar).Value = _TypeName.Trim();
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName.Trim();
                zCommand.Parameters.Add("@Publish", SqlDbType.Int).Value = _Publish;
                zCommand.Parameters.Add("@Publish_Close", SqlDbType.Int).Value = _Publish_Close;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE SLR_ReportWorker SET [RecordStatus] = 99, [ModifiedOn] = GETDATE(), ModifiedBy = @ModifiedBy, ModifiedName = @ModifiedName WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (_AutoKey.Trim().Length != 36)
                {
                    zCommand.Parameters.Add("@AutoKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@AutoKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_AutoKey);
                }
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string Delete_Team(int TeamKey,DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month,1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"DELETE FROM SLR_ReportWorker 
WHERE TeamKey = @TeamKey AND DateWrite between @FromDate and @ToDate";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_AutoKey == "")
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        
        #endregion
    }
}
