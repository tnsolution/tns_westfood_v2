﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
using TNS.Misc;

namespace TNS.SLR
{
    public class ReportSupport_Data
    {
        public static DataTable DanhSachToNhom()
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT * FROM SYS_Team WHERE RecordStatus<> 99 AND BranchKey = 4 AND DepartmentKey = 26 ORDER BY RANK";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable DanhSachToNhom(int TeamKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT * FROM SYS_Team WHERE TeamKey = " + TeamKey + " AND RecordStatus<> 99 AND BranchKey = 4 AND DepartmentKey = 26 ORDER BY RANK";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        //Load danh sách công nhân tính lương
        //        public static DataTable Load_Data(int TeamKey,DateTime DateWrite)
        //        {
        //            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
        //            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
        //            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
        //            DataTable zTable = new DataTable();
        //            string zSQL = @"SELECT EmployeeKey,EmployeeID,EmployeeName
        //FROM [dbo].[HRM_Team_Employee_Month] 
        //WHERE RecordStatus<>99
        //AND ( MONTH(DateWrite)=MONTH(@Date) AND YEAR(DateWrite)=YEAR(@Date))";
        //            if (TeamKey > 0)
        //            {
        //                zSQL += " AND TeamKey = @TeamKey";
        //            }
        //            zSQL += " Order by  LEN(EmployeeID),EmployeeID";
        //            string zConnectionString = ConnectDataBase.ConnectionString;
        //            try
        //            {
        //                SqlConnection zConnect = new SqlConnection(zConnectionString);
        //                zConnect.Open();
        //                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
        //                zCommand.Parameters.Add("@Date", SqlDbType.DateTime).Value = zToDate;
        //                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
        //                zAdapter.Fill(zTable);
        //                zCommand.Dispose();
        //                zConnect.Close();
        //            }
        //            catch (Exception ex)
        //            {
        //                string zstrMessage = "08" + ex.ToString();
        //            }
        //            return zTable;
        //        }
        public static DataTable Load_DataV2(int TeamKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);

            DataTable zTable = new DataTable();
            string zSQL = @"
--declare @FromDate datetime='2020-04-01 00:00:00'
--declare @ToDate datetime='2020-04-30 23:59:59'

--Nhân sự làm việc trong tháng
CREATE TABLE #EmployeeTemp
(
EmployeeKey NVARCHAR(50),
EmployeeID NVARCHAR(50),
EmployeeName NVARCHAR(500),
BranchKey INT,
DepartmentKey INT ,
TeamKey INT ,
PositionKey INT,
ATM NVARCHAR(50)
)
INSERT INTO #EmployeeTemp
SELECT EmployeeKey,EmployeeID,EmployeeName,
BranchKey,DepartmentKey,TeamKey, PositionKey,ATM
 FROM (
		SELECT EmployeeKey,EmployeeID,[dbo].[Fn_GetFullName](EmployeeKey) AS EmployeeName,StartingDate,
			CASE
				WHEN LeavingDate IS NULL THEN GETDATE()
				ELSE LeavingDate
			END LeavingDate,
            [dbo].[Fn_BranchKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate) AS BranchKey,
            [dbo].[Fn_DepartmentKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate) AS DepartmentKey,
            [dbo].[Fn_TeamKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate) AS TeamKey ,
            [dbo].[Fn_PositionKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate) AS PositionKey,
            [dbo].[Fn_GetATM](EmployeeKey) AS ATM
			FROM [dbo].[HRM_Employee] 
			WHERE RecordStatus <>99 
	) X
	WHERE (
            ( StartingDate <=@FromDate AND @FromDate <=  LeavingDate  )
	     OR ( StartingDate <=@ToDate   AND @ToDate   <=  LeavingDate )
        )
    @Customer
----------Kết thúc tạo danh sách nhân sự
SELECT EmployeeKey,EmployeeID, EmployeeName,
BranchKey,
[dbo].[Fn_GetBranchID](BranchKey) AS BranchID,
[dbo].[Fn_GetBranchName](BranchKey) AS BranchName,
DepartmentKey,
[dbo].[Fn_GetDepartmentID](DepartmentKey) AS  DepartmentID,
[dbo].[Fn_GetDepartmentName](DepartmentKey) AS DepartmentName,
TeamKey,
[dbo].[Fn_GetTeamID](TeamKey) AS TeamID,
[dbo].[Fn_GetTeamName](TeamKey) AS TeamName,
PositionKey, 
[dbo].[Fn_GetPositionID](PositionKey) AS PositionID ,
[dbo].[Fn_GetPositionName](PositionKey) AS PositionName,
ATM
FROM #EmployeeTemp 
ORDER BY  LEN(EmployeeID),EmployeeID

DROP TABLE #EmployeeTemp
";
            string zFilter = "";

            if (TeamKey != 0)
            {
                zFilter += " AND TeamKey= @TeamKey";
            }
            if (zFilter.Length != 0)
            {
                zSQL = zSQL.Replace("@Customer", zFilter);
            }
            else
            {
                zSQL = zSQL.Replace("@Customer", "");
            }
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        #region[Close-14/08/2020--Vì chuyển nhóm]
        //Load danh sách công nhân tính lương
        //        public static DataTable Load_Data(int TeamKey, DateTime DateWrite)
        //        {
        //            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
        //            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
        //            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
        //            DataTable zTable = new DataTable();
        //            string zSQL = @"SELECT A.EmployeeKey, A.EmployeeID, B.FullName AS EmployeeName, B.CardID,B.PersonalKey, B.CreatedOn FROM [dbo].[HRM_Employee] A
        //LEFT JOIN [dbo].[SYS_Personal] B ON A.EmployeeKey = B.ParentKey 
        //WHERE A.RecordStatus != 99 AND A.WorkingStatus=1 AND A.StartingDate  < @Date AND (LeavingDate IS NULL OR LeavingDate >= @Date)";
        //            if (TeamKey > 0)
        //            {
        //                zSQL += " AND A.TeamKey = @TeamKey";
        //            }
        //            zSQL += "  Order by  LEN(A.EmployeeID),A.EmployeeID";
        //            string zConnectionString = ConnectDataBase.ConnectionString;
        //            try
        //            {
        //                SqlConnection zConnect = new SqlConnection(zConnectionString);
        //                zConnect.Open();
        //                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
        //                zCommand.Parameters.Add("@Date", SqlDbType.DateTime).Value = zToDate;
        //                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
        //                zAdapter.Fill(zTable);
        //                zCommand.Dispose();
        //                zConnect.Close();
        //            }
        //            catch (Exception ex)
        //            {
        //                string zstrMessage = "08" + ex.ToString();
        //            }
        //            return zTable;
        //        }
        #endregion

        //Load danh sách báo cáo lương bộ phận HỖ TRỢ sản xuất
        public static DataTable ListCodeReportSupport(DateTime DateWrite)
        {
            var zTime = DateWrite.FirstEndMonth();
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.AutoKey, A.ID,A.Name,A.Type,A.TypeName,A.CategoryKey,A.CategoryName,A.Rank,A.Publish,A.Publish_Close FROM [dbo].[SLR_CodeReportSupport] A 
WHERE A.RecordStatus <>99 AND  A.DateWrite BETWEEN @FromDate AND @ToDate 
ORDER BY A.Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zTime.Item1;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zTime.Item2;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        //Load danh sách báo cáo lương bộ phận HỖ TRỢ sản xuất
        public static DataTable ListCodeSupport(DateTime DateWrite)
        {
            var zTime = DateWrite.FirstEndMonth();
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT * FROM(
	SELECT CodeID,(TypeName+' '+CodeName) AS CodeName,Rank  FROM [dbo].[SLR_ReportSupport]
	WHERE RecordStatus <> 99 AND Publish = 1  
	AND DateWrite BETWEEN @FromDate AND @ToDate  
	GROUP BY CodeID,(TypeName+' '+CodeName),Rank
	)X
ORDER BY X.[Rank]";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zTime.Item1;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zTime.Item2;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable DanhSachKetThucThuViec(DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            DataTable zTable = new DataTable();
            //---------- String SQL Access Database ---------------
            string zSQL = @"
--Nhân viên thử việc
--declare @FromDate datetime='2020-06-01 00:00:00'
--declare @ToDate datetime='2020-06-30 23:59:59'

CREATE TABLE #Tam
(
EmployeeKey nvarchar(50),
BatDau datetime,
HetThuViec datetime,
NghiViec datetime
)
INSERT INTO #Tam (EmployeeKey,BatDau,HetThuViec,NghiViec)
SELECT *  FROM 
		(
			SELECT EmployeeKey,
			StartingDate, LeavingTryDate,
			CASE
				WHEN LeavingDate IS NULL THEN GETDATE()
				ELSE LeavingDate
			END LeavingDate
			FROM  [dbo].[HRM_Employee] WHERE [dbo].[Fn_BranchKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate)=4 
            AND [dbo].[Fn_DepartmentKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate)=26 
			
		) X
		WHERE( (StartingDate <=@FromDate AND @FromDate <=  LeavingDate  )
		OR	  (StartingDate <=@ToDate   AND @ToDate   <=  LeavingDate ) )
		AND  LeavingTryDate Between @FromDate AND @ToDate

SELECT EmployeeKey,BatDau,HetThuViec,NghiViec FROM #Tam 
DROP TABLE #Tam";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable DanhSachTangLuong(DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            DataTable zTable = new DataTable();
            //---------- String SQL Access Database ---------------
            string zSQL = @"
--Nhân viên thử việc
--declare @FromDate datetime='2020-06-01 00:00:00'
--declare @ToDate datetime='2020-06-30 23:59:59'

CREATE TABLE #Tam
(
EmployeeKey nvarchar(50),
MaDanhMuc Nvarchar(50),
BatDau datetime,
HetHan datetime,
SoTien money
)

INSERT INTO #Tam (EmployeeKey,MaDanhMuc,BatDau,HetHan,SoTien)
SELECT *  FROM 
		(
			SELECT EmployeeKey,CategoryID,
			FromDate AS StartDate,
			CASE
				WHEN ToDate IS NULL THEN GETDATE()
				ELSE ToDate
			END LeavingDate,
			[Value] AS SoTien
			FROM  [dbo].[HRM_SalaryDefault] WHERE [dbo].[Fn_BranchKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate)=2
			AND (CategoryID='MLC')  
			
		) X
		WHERE (StartDate <=@FromDate AND @FromDate <=  LeavingDate  )
		OR	  (StartDate <=@ToDate   AND @ToDate   <=  LeavingDate ) 
INSERT INTO #Tam (EmployeeKey,MaDanhMuc,BatDau,HetHan,SoTien)
SELECT *  FROM 
		(
			SELECT EmployeeKey,CategoryID,
			FromDate AS StartDate,
			CASE
				WHEN ToDate IS NULL THEN GETDATE()
				ELSE ToDate
			END LeavingDate,
			[Value] AS SoTien
			FROM  [dbo].[HRM_SalaryDefault] WHERE [dbo].[Fn_BranchKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate)=2
            AND [dbo].[Fn_DepartmentKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate)=26 
			AND (CategoryID='LHQ')   AND [Value] < 1500000 --Sợ trùng với thử việc
			
		) X
		WHERE (StartDate <=@FromDate AND @FromDate <=  LeavingDate  )
		OR	  (StartDate <=@ToDate   AND @ToDate   <=  LeavingDate ) 
INSERT INTO #Tam (EmployeeKey,MaDanhMuc,BatDau,HetHan,SoTien)
SELECT *  FROM 
		(
			SELECT EmployeeKey,CategoryID,
			FromDate AS StartDate,
			CASE
				WHEN ToDate IS NULL THEN GETDATE()
				ELSE ToDate
			END LeavingDate,
			[Value] AS SoTien
			FROM  [dbo].[HRM_SalaryDefault] WHERE [dbo].[Fn_BranchKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate)=2
			AND (CategoryID='HTX')  
			
		) X
		WHERE (StartDate <=@FromDate AND @FromDate <=  LeavingDate  )
		OR	  (StartDate <=@ToDate   AND @ToDate   <=  LeavingDate ) 

SELECT EmployeeKey,MaDanhMuc,HetHan,SoTien FROM #Tam WHERE  HetHan Between @FromDate AND @ToDate
GROUP BY EmployeeKey,MaDanhMuc,HetHan,SoTien
DROP TABLE #Tam";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        #region[Cơm]
        //Danh sách mã cơm con theo mục hiển thị cơm
        public static string GetRiceID(string ID)
        {
            string zResult = "";
            string zSQL = @"SELECT RiceID FROM HRM_Time_Rice WHERE RecordStatus <> 99 
            AND ( ID_Rice= @ID OR ID_Number= @ID OR ID_Money= @ID) ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ID", SqlDbType.NVarChar).Value = ID;
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zResult;
        }
        //Lấy số phần cơm
        public static float NumberRice(string EmployeeKey, string RiceID, DateTime DateWrite)
        {
            var zTime = DateWrite.FirstEndMonth();
            float zResult = 0;
            string zSQL = @"SELECT ISNULL(SUM(A.Number),0)  FROM [dbo].HRM_Employee_Rice A
WHERE A.EmployeeKey= @EmployeeKey  AND A.RiceID =  @RiceID
AND  A.DateWrite BETWEEN @FromDate AND @ToDate AND A.RecordStatus <>99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zTime.Item1;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zTime.Item2;
                zCommand.Parameters.Add("@RiceID", SqlDbType.NVarChar).Value = RiceID;
                zResult = zCommand.ExecuteScalar().ToFloat();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zResult;
        }
        //Lấy số tiền cơm
        public static float MoneyRice(string EmployeeKey, string RiceID, DateTime DateWrite)
        {
            var zTime = DateWrite.FirstEndMonth();
            float zResult = 0;
            string zSQL = @"SELECT ISNULL(SUM(A.Money),0)  FROM [dbo].HRM_Employee_Rice A
WHERE A.EmployeeKey= @EmployeeKey  AND A.RiceID =  @RiceID
AND  A.DateWrite BETWEEN @FromDate AND @ToDate AND A.RecordStatus <>99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zTime.Item1;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zTime.Item2;
                zCommand.Parameters.Add("@RiceID", SqlDbType.NVarChar).Value = RiceID;
                zResult = zCommand.ExecuteScalar().ToFloat();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zResult;
        }

        #endregion

        #region[Tính lương]
        //Lẫy mức lương chính
        public static double MLTT(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT [dbo].[SLR_SUPPORT_MLTT_MucLuongChinh](@EmployeeKey,@FromDate,@ToDate)";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(),out zResult))
                {

                }

                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //Lương hiệu quả công việc
        public static double HQCV(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_SUPPORT_HQCV_LuongHieuQuaCongViec(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //Tỉ lệ hoàn thành công việc
        public static double TLHT(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_SUPPORT_TLHT_TiLeHoanThanhCongViec(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //Tổng lương chính và hỗ trợ theo hiệu quả công việc
        public static double LCHQ(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_SUPPORT_LCHQ_LuongChinhVaHoTro_HTCV(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //Ngày Công quy đinh, công tính lương
        public static double NCQD(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_SUPPORT_NCQD_SoNgayCongQuyDinhTinhLuong(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //Ngày Công thực tế
        public static double NCTT(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_SUPPORT_NCTT_SoNgayCongThucTe(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //Ngày nghỉ không hưởng lương
        public static double NKHL(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_SUPPORT_NKHL_SoNgayNghiKhongHuongLuong(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //Số tiền nghỉ không hưởng lương
        public static double TKHL(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_SUPPORT_TKHL_SoTienNghiKhongHuongLuong(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //Tổng lương theo ngày công
        public static double TLNC(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_SUPPORT_TLNC_TongLuongTheoNgayCong(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //Số ngày công phép năm
        public static double NCPN(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_SUPPORT_NCPN_SoNgayPhepNam(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //Só tiền Hỗ trợ phép năm
        public static double HTPN(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_SUPPORT_HTPN_HoTroNgayPhepNam(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        // THÊM 25/11
        //Số ngày PHÉP- LỄ TẾT
        public static double NLLT(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.[SLR_SUPPORT_NLLT_SoNgayPhep_LeTet](@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //Só tiền Hỗ trợ phép -lễ tết
        public static double HTLT(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.[SLR_SUPPORT_HTLT_HoTroNgayPhep_LeTet](@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //Số ngày  thâm niên
        public static double NPTN(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.[SLR_SUPPORT_NPTN_SoNgayPhepThamNien](@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //Só tiền Hỗ trợ  thâm niên
        public static double HTTN(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.[SLR_SUPPORT_HTTN_HoTroNgayPhepThamNien](@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //--

        //Xuat hang lon
        public static double KXHL(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_SUPPORT_KXHL_KhoanXuatHangLon(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        //THÊM TÁCH KHOÁN CÔNG VIỆC KHÁC 25/11

        //  NGÀY CÔNG LÀM  LỄ TẾT
        public static double NCLT(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.[SLR_SUPPORT_NCLT_NgayCongLamLeTet](@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //  THÀNH TIỀN NGÀY CÔNG LÀM  LỄ TẾT
        public static double TTLT(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_SUPPORT_TTLT_ThanhTienLamLeTet(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //  NGÀY CÔNG /Số giờ công thừa kho lạnh
        public static double GCCC(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.[SLR_SUPPORT_GCCC_SoGioChuaChamCong](@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //  SỐ TIỀN công thừa kho lạnh
        public static double TCCC(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.[SLR_SUPPORT_TCCC_ThanhTienGioChuaChamCong](@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //  SỐ GIỜ DƯ
        public static double SLGD(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.[SLR_SUPPORT_SLGD_SoGioDu](@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //  thành tiền GIỜ DƯ
        public static double TTGD(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.[SLR_SUPPORT_TTGD_ThanhTienGioDu](@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //  số ngày làm  luân phiên
        public static double SNLP(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.[SLR_SUPPORT_SNLP_SoNgayLamLanhLuanPhien](@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //  số tiền làm luân phiên
        public static double HTLP(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.[SLR_SUPPORT_HTLP_HoTRoLamLanhLuanPhien](@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //  số giờ lãy xe nâng
        public static double SGLX(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.[SLR_SUPPORT_SGLX_SoGioLayXeSau21h] (@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //  hỗ trợ lãy xe nâng
        public static double HTLX(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.[SLR_SUPPORT_HTLX_HoTroLayXeSau21h]  (@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //  số giờ vệ sinh kho lạnh
        public static double SGVS(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.[SLR_SUPPORT_SGVS_SoGioVeSinhKhoLanh] (@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //  số tiền vệ sinh kho lạnh
        public static double TTVS(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.[SLR_SUPPORT_TTVS_ThanhTienVeSinhKhoLanh](@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //  số giờ xử lý rác
        public static double SGRA(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.[SLR_SUPPORT_SGRA_SoGioXuLyRac] (@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //  thành tiền  xử lý rác
        public static double TTRA(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.[SLR_SUPPORT_TTRA_ThanhTienXuLyRac]  (@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //  khoán hỗ trợ khác chưa tách cột
        public static double HTKH(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.[dbo].[SLR_SUPPORT_HTKH_KhoanHoTroKhacChuaTachCot](@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }



        //Khoán công việc khác
        public static double KCVK(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_SUPPORT_KCVK_KhoanCongViecKhac(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }

                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //Tiền khoán xuất hàng //bốc xếp đường
        public static double TKXH(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_SUPPORT_TKXH_KhoanXuatHang(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //Số tiền hỗ trợ
        public static double STHT(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_SUPPORT_STHT_CacKhoanHoTro(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        // Tổng cộng tiền sản xuất
        public static double TCSX(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_SUPPORT_TCSX_TongCongSanXuat(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        // Tổng cộng tiền sản xuất
        public static double HTTD(string EmployeeKey, DateTime DateWrite, int TinhThapDiem)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_SUPPORT_HTTD_HoTroThangThapDiem(@EmployeeKey,@FromDate,@ToDate,@TinhThapDiem)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TinhThapDiem", SqlDbType.Int).Value = TinhThapDiem;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        // Số lượng con nhỏ
        public static double SLCN(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_SUPPORT_SLCN_SoLuongConNho(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        // Số tiền con nhỏ
        public static double STCN(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_SUPPORT_STCN_SoTienConNho(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        // Số lượng cơm loại C
        public static double SLCC(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_SUPPORT_SLCC_SoLuongComLoai_C(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        // Số Tiền cơm loại C
        public static double STCC(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_SUPPORT_STCC_SoTienComLoai_C(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        // Số lượng cơm loại U
        public static double SLCU(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_SUPPORT_SLCU_SoLuongComLoai_U(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        // Số Tiền cơm loại U
        public static double STCU(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_SUPPORT_STCU_SoTienComLoai_U(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        // Số tiền hoàn ứng lương
        public static double HUUL(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_SUPPORT_HUUL_HoanUngLuong(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        // Số tiền hoàn ứng bốc xếp đường
        public static double HUXH(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_SUPPORT_HUXH_HoanUngBocXepDuong(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        //chi tiết bhxh
        // Số tiền bảo hiểm xã hội- CTY
        public static double XHCT(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_SUPPORT_XHCT_BaoHiemXaHoi_CongTy(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        // Số tiền bảo hiểm Y TẾ - CTY
        public static double YTCT(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_SUPPORT_BaoHiemYTe_CongTy(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        // Số tiền bảo hiểm THẤT NGHIỆP - CTY
        public static double NVCT(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_SUPPORT_NVCT_BaoHiemThatNghiep_CongTy(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        // Số tiền bảo hiểm xã hội CÁ NHÂN
        public static double XHCN(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_SUPPORT_XHCN_BaoHiemXaHoi_CaNhan(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        // Số tiền bảo hiểm Y TẾ - CÁ NHÂN
        public static double YTCN(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_SUPPORT_YTCN_BaoHiemYTe_CaNhan(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        // Số tiền bảo hiểm THẤT NGHIỆP - CTY
        public static double NVCN(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_SUPPORT_NVCN_BaoHiemThatNghiep_CaNhan(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //



        // Số tiền bảo hiểm xã hội -TỔNG CÁ NHÂN
        public static double BHXH(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_SUPPORT_BHXH_BaoHiemXaHoi(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        // Số tiền đóng công đoàn- CÁ NHÂN
        public static double STCD(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_SUPPORT_STCD_CongDoan(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        // Số tiền đóng công đoàn -CTY
        public static double CDCT(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_SUPPORT_CDCT_KinhPhiCongDoan_CongTy(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        // Các khoản trừ khác
        public static double CKTK(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_SUPPORT_CKTK_CacKhoanTruKhac(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        // Số tiền Tấm lòng vàng
        public static double TTLV(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_SUPPORT_TTLV_TamLongVang(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        // THU NHẬP CHỊU THUẾ
        public static double TNCT(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.[SLR_SUPPORT_TNCT_ThuNhapChiuThue](@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        // THUẾ THU NHẬP CÁ NHÂN
        public static double  TNCN(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT  [dbo].[SLR_SUPPORT_TNCN_ThueThuNhapCaNhan](@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        // Số tiền thực lãnh
        public static double STTL(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_SUPPORT_STTL_SoTienThucLanh(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        

        // Số ngày công trước khi thay đổi


        public static double SoNgayCongMucCu(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = new DateTime(DateWrite.Year, DateWrite.Month, DateWrite.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT [dbo].[SLR_SUPPORT_SoNgayCongMucCu] (@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //Ngày Công thực tế
        public static double NCTT_TangLuong(string EmployeeKey, DateTime DateWrite, double SoNgayCongCu)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_SUPPORT_NCTT_SoNgayCongThucTe_TangLuong(@EmployeeKey,@FromDate,@ToDate,@SoNgayCongCu)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@SoNgayCongCu", SqlDbType.Money).Value = SoNgayCongCu;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //Ngày khoán công việc khác
        public static string KCVK_TangLuong(string EmployeeKey, DateTime DateWrite, DateTime NgayKT)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);

            DateTime zNgayKT = new DateTime(NgayKT.Year, NgayKT.Month, NgayKT.Day, 23, 59, 59);

            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_SUPPORT_KCVK_KhoanCongViecKhac_TangLuong(@EmployeeKey,@FromDate,@ToDate,@NgayKT)   ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@NgayKT", SqlDbType.DateTime).Value = zNgayKT;
                zResult=zCommand.ExecuteScalar().ToString();
               
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        //  thành tiền GIỜ DƯ - tăng lương
        public static double TTGD_TangLuong(string EmployeeKey, DateTime DateWrite, DateTime NgayKT)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            DateTime zNgayKT = new DateTime(NgayKT.Year, NgayKT.Month, NgayKT.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.[SLR_SUPPORT_TTGD_ThanhTienGioDu_TangLuong] (@EmployeeKey,@FromDate,@ToDate,@NgayKT)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@NgayKT", SqlDbType.DateTime).Value = zNgayKT;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        #endregion
    }
}
