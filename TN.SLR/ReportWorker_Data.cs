﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
using TNS.Misc;
using TNS.SYS;

namespace TNS.SLR
{
    public class ReportWorker_Data
    {
        public static DataTable DanhSachToNhom()
        {
            //Lấy tất cả nhóm trực tiếp sx  và khác bộ phận hỗ trợ sx
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT * FROM SYS_Team WHERE RecordStatus <> 99 AND BranchKey=4 AND  DepartmentKey != 26 ORDER BY Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable DanhSachToNhom(int TeamKey)
        {
            DataTable zTable = new DataTable();

            string zSQL = @"SELECT * FROM SYS_Team WHERE TeamKey = " + TeamKey + " AND RecordStatus <> 99 AND BranchKey=4 AND  DepartmentKey != 26 ORDER BY Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        //        //Load danh sách công nhân tính lương
        //        public static DataTable Load_Data(int TeamKey, DateTime DateWrite)
        //        {
        //            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
        //            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
        //            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);

        //            DataTable zTable = new DataTable();
        //            string zSQL = @"SELECT EmployeeKey,EmployeeID,EmployeeName
        //FROM [dbo].[HRM_Team_Employee_Month] 
        //WHERE RecordStatus<>99
        //AND ( MONTH(DateWrite)=MONTH(@Date) AND YEAR(DateWrite)=YEAR(@Date))";
        //            if (TeamKey > 0)
        //            {
        //                zSQL += " AND TeamKey = @TeamKey";
        //            }
        //            zSQL += " Order by  LEN(EmployeeID),EmployeeID";
        //            string zConnectionString = ConnectDataBase.ConnectionString;
        //            try
        //            {
        //                SqlConnection zConnect = new SqlConnection(zConnectionString);
        //                zConnect.Open();
        //                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
        //                zCommand.Parameters.Add("@Date", SqlDbType.DateTime).Value = zToDate;
        //                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
        //                zAdapter.Fill(zTable);
        //                zCommand.Dispose();
        //                zConnect.Close();
        //            }
        //            catch (Exception ex)
        //            {
        //                string zstrMessage = "08" + ex.ToString();
        //            }
        //            return zTable;
        //        }
        //Load danh sách công nhân tính lương
        public static DataTable Load_DataV2(int TeamKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);

            DataTable zTable = new DataTable();
            string zSQL = @"
--declare @FromDate datetime='2020-04-01 00:00:00'
--declare @ToDate datetime='2020-04-30 23:59:59'

--Nhân sự làm việc trong tháng
CREATE TABLE #EmployeeTemp
(
EmployeeKey NVARCHAR(50),
EmployeeID NVARCHAR(50),
EmployeeName NVARCHAR(500),
BranchKey INT,
DepartmentKey INT ,
TeamKey INT ,
PositionKey INT,
ATM  NVARCHAR(50)
)
INSERT INTO #EmployeeTemp
SELECT EmployeeKey,EmployeeID,EmployeeName,
BranchKey,DepartmentKey,TeamKey, PositionKey,ATM
 FROM (
		SELECT EmployeeKey,EmployeeID,[dbo].[Fn_GetFullName](EmployeeKey) AS EmployeeName,StartingDate,
			CASE
				WHEN LeavingDate IS NULL THEN GETDATE()
				ELSE LeavingDate
			END LeavingDate,
            [dbo].[Fn_BranchKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate) AS BranchKey,
            [dbo].[Fn_DepartmentKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate) AS DepartmentKey,
            [dbo].[Fn_TeamKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate) AS TeamKey ,
            [dbo].[Fn_PositionKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate) AS PositionKey,
            [dbo].[Fn_GetATM](EmployeeKey) AS ATM
			FROM [dbo].[HRM_Employee] 
			WHERE RecordStatus <>99 
	) X
	WHERE (
            ( StartingDate <=@FromDate AND @FromDate <=  LeavingDate  )
	     OR ( StartingDate <=@ToDate   AND @ToDate   <=  LeavingDate )
        )
    @Customer
----------Kết thúc tạo danh sách nhân sự
SELECT EmployeeKey,EmployeeID, EmployeeName,
BranchKey,
[dbo].[Fn_GetBranchID](BranchKey) AS BranchID,
[dbo].[Fn_GetBranchName](BranchKey) AS BranchName,
DepartmentKey,
[dbo].[Fn_GetDepartmentID](DepartmentKey) AS  DepartmentID,
[dbo].[Fn_GetDepartmentName](DepartmentKey) AS DepartmentName,
TeamKey,
[dbo].[Fn_GetTeamID](TeamKey) AS TeamID,
[dbo].[Fn_GetTeamName](TeamKey) AS TeamName,
PositionKey, 
[dbo].[Fn_GetPositionID](PositionKey) AS PositionID ,
[dbo].[Fn_GetPositionName](PositionKey) AS PositionName,
ATM
FROM #EmployeeTemp 
ORDER BY  LEN(EmployeeID),EmployeeID

DROP TABLE #EmployeeTemp
";
            string zFilter = "";

            if (TeamKey != 0)
            {
                zFilter += " AND TeamKey= @TeamKey";
            }
            if (zFilter.Length != 0)
            {
                zSQL = zSQL.Replace("@Customer", zFilter);
            }
            else
            {
                zSQL = zSQL.Replace("@Customer", "");
            }
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }


        #region[close-14/08/2020-vì chuyển nhóm]
        //Load danh sách công nhân tính lương
        //public static DataTable Load_Data(int TeamKey, DateTime DateWrite)
        //{
        //    DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
        //    DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
        //    zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
        //    DataTable zTable = new DataTable();
        //    string zSQL = @"SELECT A.EmployeeKey, A.EmployeeID, B.FullName AS EmployeeName, B.CardID,B.PersonalKey, B.CreatedOn FROM [dbo].[HRM_Employee] A
        //    LEFT JOIN [dbo].[SYS_Personal] B ON A.EmployeeKey = B.ParentKey 
        //    WHERE A.RecordStatus != 99 AND A.WorkingStatus=1 AND A.StartingDate  < @Date AND (LeavingDate IS NULL OR LeavingDate >= @Date)";
        //    if (TeamKey > 0)
        //    {
        //        zSQL += " AND A.TeamKey = @TeamKey";
        //    }
        //    zSQL += " Order by  LEN(A.EmployeeID),A.EmployeeID";
        //    string zConnectionString = ConnectDataBase.ConnectionString;
        //    try
        //    {
        //        SqlConnection zConnect = new SqlConnection(zConnectionString);
        //        zConnect.Open();
        //        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //        zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
        //        zCommand.Parameters.Add("@Date", SqlDbType.DateTime).Value = zToDate;
        //        SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
        //        zAdapter.Fill(zTable);
        //        zCommand.Dispose();
        //        zConnect.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        string zstrMessage = "08" + ex.ToString();
        //    }
        //    return zTable;
        //}
        #endregion

        //Load danh sách báo cáo lương bộ phận sản xuất
        public static DataTable ListCodeReportWorker(DateTime DateWrite)
        {
            var zTime = DateWrite.FirstEndMonth();
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.AutoKey, A.ID,A.Name,A.Type, A.TypeName,A.CategoryKey,A.CategoryName,A.Rank,A.Publish,A.Publish_Close FROM [dbo].[SLR_CodeReportWorker] A 
WHERE A.RecordStatus <>99 AND  A.DateWrite BETWEEN @FromDate AND @ToDate 
ORDER BY A.Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zTime.Item1;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zTime.Item2;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable ListCodeWorker(DateTime DateWrite)
        {
            var zTime = DateWrite.FirstEndMonth();
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT * FROM(
	SELECT CodeID,(TypeName+' '+CodeName) AS CodeName,Rank  FROM [dbo].[SLR_ReportWorker]
	WHERE RecordStatus <> 99 AND Publish = 1  
	AND DateWrite BETWEEN @FromDate AND @ToDate  
	GROUP BY CodeID,(TypeName+' '+CodeName),Rank
	)X
ORDER BY X.[Rank]";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zTime.Item1;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zTime.Item2;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable List(int DepartmentKey, int TeamKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            DataTable zTable = new DataTable();
            string zSQL = @"
--declare @FromDate datetime='2020-04-01 00:00:00'
--declare @ToDate datetime='2020-04-30 23:59:59'

--Nhân sự làm việc trong tháng
CREATE TABLE #EmployeeTemp
(
EmployeeKey NVARCHAR(50),
EmployeeID NVARCHAR(50),
EmployeeName NVARCHAR(500),
BranchKey INT,
DepartmentKey INT ,
TeamKey INT ,
PositionKey INT 
)
INSERT INTO #EmployeeTemp
SELECT EmployeeKey,EmployeeID,EmployeeName,
BranchKey,DepartmentKey,TeamKey, PositionKey
 FROM (
		SELECT EmployeeKey,EmployeeID,[dbo].[Fn_GetFullName](EmployeeKey) AS EmployeeName,StartingDate,
			CASE
				WHEN LeavingDate IS NULL THEN GETDATE()
				ELSE LeavingDate
			END LeavingDate,
            [dbo].[Fn_BranchKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate) AS BranchKey,
            [dbo].[Fn_DepartmentKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate) AS DepartmentKey,
            [dbo].[Fn_TeamKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate) AS TeamKey,
            [dbo].[Fn_PositionKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate) AS PositionKey
			FROM [dbo].[HRM_Employee] 
			WHERE RecordStatus <>99 
	) X
	WHERE (
                ( StartingDate <=@FromDate AND @FromDate <=  LeavingDate  )
	        OR  ( StartingDate <=@ToDate   AND @ToDate   <=  LeavingDate )
          )
          AND BranchKey =4 
          --AND DepartmentKey !=26 
          @Customer
----------Kết thúc tạo danh sách nhân sự

DECLARE @DonGia MONEY=0;
DECLARE @DateMonthNext DATETIME;
SET @DateMonthNext = DATEADD(month,1,@ToDate);--Lấy tháng tiếp theo
SET @DateMonthNext =DATEFROMPARTS(YEAR(@DateMonthNext),MONTH(@DateMonthNext),15)--Lấy ngày 15

SET @DonGia =dbo.SLR_DonGiaHoTroThangThapDiem(@ToDate);
--Luu ý: nếu tháng làm mà nghỉ và báo nghỉ tháng sau  thì không được hỗ trơ
SELECT A.EmployeeName,A.EmployeeID,
A.TeamKey,B.TeamID,B.TeamName,
[dbo].[SLR_SoNgayNghiLuanPhienDaChot] (A.EmployeeKey,@FromDate,@ToDate) AS NghiLP,
@DonGia AS DonGia,
CASE 
WHEN ((SELECT Count(*) FROM [dbo].[HRM_Employee] WHERE  LeavingDate IS NOT NULL
	AND ( LeavingDate>=@FromDate AND LeavingDate <= @DateMonthNext)
	AND EmployeeKey  = A.EmployeeKey)) >0
	THEN  0
ELSE  ([dbo].[SLR_SoNgayNghiLuanPhienDaChot] (A.EmployeeKey,@FromDate,@ToDate)  * @DonGia )
END AS Money 
FROM #EmployeeTemp A
LEFT JOIN[dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
LEFT JOIN[dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
LEFT JOIN[dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
ORDER BY D.RANK, C.RANK, B.RANK, LEN(A.EmployeeID), A.EmployeeID ASC

DROP TABLE #EmployeeTemp
";
            string zFilter = "";
            if (DepartmentKey != 0)
            {
                zFilter += " AND DepartmentKey =@DepartmentKey";
            }
            if (TeamKey != 0)
            {
                zFilter += " AND TeamKey= @TeamKey";
            }
            if (zFilter.Length != 0)
            {
                zSQL = zSQL.Replace("@Customer", zFilter);
            }
            else
            {
                zSQL = zSQL.Replace("@Customer", "");
            }
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }


        #region[Cơm]
        //Danh sách mã cơm con theo mục hiển thị cơm
        public static string GetRiceID(string ID)
        {
            string zResult = "";
            string zSQL = @"SELECT RiceID FROM HRM_Time_Rice WHERE RecordStatus <> 99 
            AND ( ID_Rice= @ID OR ID_Number= @ID OR ID_Money= @ID) ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ID", SqlDbType.NVarChar).Value = ID;
                zResult=zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zResult;
        }
        //Lấy số phần cơm
        public static float NumberRice(string EmployeeKey, string RiceID, DateTime DateWrite)
        {
            var zTime = DateWrite.FirstEndMonth();
            float zResult = 0;
            string zSQL = @"SELECT ISNULL(SUM(A.Number),0)  FROM [dbo].HRM_Employee_Rice A
WHERE A.EmployeeKey= @EmployeeKey  AND A.RiceID =  @RiceID
AND  A.DateWrite BETWEEN @FromDate AND @ToDate AND A.RecordStatus <>99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zTime.Item1;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zTime.Item2;
                zCommand.Parameters.Add("@RiceID", SqlDbType.NVarChar).Value = RiceID;
                zResult = zCommand.ExecuteScalar().ToFloat();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zResult;
        }
        //Lấy số tiền cơm
        public static float MoneyRice(string EmployeeKey, string RiceID, DateTime DateWrite)
        {
            var zTime = DateWrite.FirstEndMonth();
            float zResult = 0;
            string zSQL = @"SELECT ISNULL(SUM(A.Money),0)  FROM [dbo].HRM_Employee_Rice A
WHERE A.EmployeeKey= @EmployeeKey  AND A.RiceID =  @RiceID
AND  A.DateWrite BETWEEN @FromDate AND @ToDate AND A.RecordStatus <>99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zTime.Item1;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zTime.Item2;
                zCommand.Parameters.Add("@RiceID", SqlDbType.NVarChar).Value = RiceID;
                zResult = zCommand.ExecuteScalar().ToFloat();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zResult;
        }

        #endregion

        #region[Tính lương]

        //Lẫy mức lương chính
        public static double MLTT(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT [dbo].[SLR_WORKER_MLTT_MucLuongChinh](@EmployeeKey,@FromDate,@ToDate)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        ////Lấy lương tăng ca ngoài giờ
        //public static double NGGC(string EmployeeKey, DateTime DateWrite)
        //{
        //    DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
        //    DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
        //    zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
        //    double zResult = 0;
        //    //---------- String SQL Access Database ---------------
        //    string zSQL = "SELECT  [dbo].[SLR_WORKER_NGGC_TongThanhTienNgoaiGio](@EmployeeKey, @DateWrite) ";
        //    string zConnectionString = ConnectDataBase.ConnectionString;
        //    SqlConnection zConnect = new SqlConnection(zConnectionString);
        //    zConnect.Open();
        //    try
        //    {
        //        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //        zCommand.CommandType = CommandType.Text;
        //        zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
        //        zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = DateWrite;
        //        if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
        //        {

        //        }
        //        zCommand.Dispose();
        //    }
        //    catch (Exception Ex)
        //    {

        //    }
        //    finally
        //    {
        //        zConnect.Close();
        //    }
        //    return zResult;
        //}
        ////Khoán sản phẩm thường (tại tổ)
        //public static double KSPT(string EmployeeKey, DateTime DateWrite)
        //{
        //    DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
        //    DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
        //    zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
        //    double zResult = 0;
        //    //---------- String SQL Access Database ---------------
        //    string zSQL = "SELECT dbo.FTR_Sum_Money(@EmployeeKey,@FromDate,@ToDate) ";
        //    string zConnectionString = ConnectDataBase.ConnectionString;
        //    SqlConnection zConnect = new SqlConnection(zConnectionString);
        //    zConnect.Open();
        //    try
        //    {
        //        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //        zCommand.CommandType = CommandType.Text;
        //        zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
        //        zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
        //        zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
        //        if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
        //        {

        //        }
        //        zCommand.Dispose();
        //    }
        //    catch (Exception Ex)
        //    {

        //    }
        //    finally
        //    {
        //        zConnect.Close();
        //    }
        //    return zResult;
        //}
        ////Khoán sản phẩm chủ nhật(tại tổ)
        //public static double LTCN(string EmployeeKey, DateTime DateWrite)
        //{
        //    DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
        //    DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
        //    zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
        //    double zResult = 0;
        //    //---------- String SQL Access Database ---------------
        //    string zSQL = "SELECT dbo.FTR_Sum_Money_Sunday(@EmployeeKey,@FromDate,@ToDate) ";
        //    string zConnectionString = ConnectDataBase.ConnectionString;
        //    SqlConnection zConnect = new SqlConnection(zConnectionString);
        //    zConnect.Open();
        //    try
        //    {
        //        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //        zCommand.CommandType = CommandType.Text;
        //        zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
        //        zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
        //        zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
        //        if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
        //        {

        //        }
        //        zCommand.Dispose();
        //    }
        //    catch (Exception Ex)
        //    {

        //    }
        //    finally
        //    {
        //        zConnect.Close();
        //    }
        //    return zResult;
        //}
        ////Khoán sản phẩm lễ (tại tổ)
        //public static double LTNL(string EmployeeKey, DateTime DateWrite)
        //{
        //    DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
        //    DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
        //    zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
        //    double zResult = 0;
        //    //---------- String SQL Access Database ---------------
        //    string zSQL = "SELECT dbo.FTR_Sum_Money_Holidays(@EmployeeKey,@FromDate,@ToDate)  ";
        //    string zConnectionString = ConnectDataBase.ConnectionString;
        //    SqlConnection zConnect = new SqlConnection(zConnectionString);
        //    zConnect.Open();
        //    try
        //    {
        //        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //        zCommand.CommandType = CommandType.Text;
        //        zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
        //        zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
        //        zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
        //        if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
        //        {

        //        }
        //        zCommand.Dispose();
        //    }
        //    catch (Exception Ex)
        //    {

        //    }
        //    finally
        //    {
        //        zConnect.Close();
        //    }
        //    return zResult;
        //}
        ////Khoán sản phẩm ngày thường (tại tổ khác)
        //public static double SPTK(string EmployeeKey, DateTime DateWrite)
        //{
        //    DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
        //    DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
        //    zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
        //    double zResult = 0;
        //    //---------- String SQL Access Database ---------------
        //    string zSQL = "SELECT dbo.FTR_Sum_Money_Dif(@EmployeeKey,@FromDate,@ToDate)  ";
        //    string zConnectionString = ConnectDataBase.ConnectionString;
        //    SqlConnection zConnect = new SqlConnection(zConnectionString);
        //    zConnect.Open();
        //    try
        //    {
        //        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //        zCommand.CommandType = CommandType.Text;
        //        zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
        //        zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
        //        zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
        //        if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
        //        {

        //        }
        //        zCommand.Dispose();
        //    }
        //    catch (Exception Ex)
        //    {

        //    }
        //    finally
        //    {
        //        zConnect.Close();
        //    }
        //    return zResult;
        //}
        ////Khoán sản phẩm ngày chủ nhật (tại tổ khác)
        //public static double CNTK(string EmployeeKey, DateTime DateWrite)
        //{
        //    DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
        //    DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
        //    zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
        //    double zResult = 0;
        //    //---------- String SQL Access Database ---------------
        //    string zSQL = "SELECT dbo.FTR_Sum_Money_Dif_Sunday(@EmployeeKey,@FromDate,@ToDate)  ";
        //    string zConnectionString = ConnectDataBase.ConnectionString;
        //    SqlConnection zConnect = new SqlConnection(zConnectionString);
        //    zConnect.Open();
        //    try
        //    {
        //        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //        zCommand.CommandType = CommandType.Text;
        //        zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
        //        zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
        //        zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
        //        if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
        //        {

        //        }
        //        zCommand.Dispose();
        //    }
        //    catch (Exception Ex)
        //    {

        //    }
        //    finally
        //    {
        //        zConnect.Close();
        //    }
        //    return zResult;
        //}
        ////Khoán sản phẩm ngày lễ (tại tổ khác)
        //public static double NLTK(string EmployeeKey, DateTime DateWrite)
        //{
        //    DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
        //    DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
        //    zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
        //    double zResult = 0;
        //    //---------- String SQL Access Database ---------------
        //    string zSQL = "SELECT dbo.FTR_Sum_Money_Dif_Holiday(@EmployeeKey,@FromDate,@ToDate)  ";
        //    string zConnectionString = ConnectDataBase.ConnectionString;
        //    SqlConnection zConnect = new SqlConnection(zConnectionString);
        //    zConnect.Open();
        //    try
        //    {
        //        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //        zCommand.CommandType = CommandType.Text;
        //        zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
        //        zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
        //        zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
        //        if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
        //        {

        //        }
        //        zCommand.Dispose();
        //    }
        //    catch (Exception Ex)
        //    {

        //    }
        //    finally
        //    {
        //        zConnect.Close();
        //    }
        //    return zResult;
        //}
        //Khoán dùng tính chuyên cần
        public static DataTable TongHopLuongNangSuat_V2(int TeamKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);

            DataTable zTable = new DataTable();

            string zSQL = @"
CREATE TABLE #temp(
DateWork datetime,
TeamKey int,
EmployeeKey nvarchar(50),
EmployeeID nvarchar(50),
EmployeeName nvarchar(250),
OverTime money,
MoneyBorrowShare money,		--chia lại
MoneyPersonal  money,		--ăn chung cá nhân
MoneyPrivate money,			--ăn riêng cá nhân
MoneyBorrowPrivate  money,	-- mượn ăn  riêng
MoneyTimePrivate money,		--công việc khác ăn riêng
MoneyTimeShare money		--công việc khác ăn chung

)
--Lấy thông tin Chia lại
INSERT INTO #temp (DateWork,TeamKey,EmployeeKey,EmployeeID,EmployeeName,
OverTime,MoneyBorrowShare,MoneyPersonal,MoneyPrivate,MoneyBorrowPrivate,MoneyTimePrivate,MoneyTimeShare)
SELECT OrderDate,[dbo].[Fn_TeamKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate),EmployeeKey,EmployeeID,EmployeeName,
0,
ISNULL(Sum(MoneyPersonal) ,0),
0,
0,
0,
0,
0
FROM [dbo].[FTR_Order_Adjusted]
WHERE  OrderDate BETWEEN  @FromDate AND  @ToDate 
AND Share=0 AND RecordStatus <>99 @CustomParamater --AND EmployeeID in ('108')
GROUP BY OrderDate,TeamKey,EmployeeKey,EmployeeID,EmployeeName
--Lấy thông tin tại tổ
INSERT INTO #temp (DateWork,TeamKey,EmployeeKey,EmployeeID,EmployeeName,
OverTime,MoneyBorrowShare,MoneyPersonal,MoneyPrivate,MoneyBorrowPrivate,MoneyTimePrivate,MoneyTimeShare)
SELECT OrderDate,[dbo].[Fn_TeamKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate),EmployeeKey,EmployeeID,EmployeeName,
0,
0,
ISNULL(SUM([Money]),0) ,
ISNULL(SUM(MoneyPrivate),0) ,
ISNULL(SUM(Money_Borrow),0) ,
ISNULL(SUM(Money_Dif_Private),0) ,
ISNULL(SUM(Money_Dif_General),0) 
FROM [dbo].[FTR_Order_Money]  
WHERE  OrderDate BETWEEN @FROMDATE AND @TODATE 
AND RecordStatus <>99 @CustomParamater --AND EmployeeID in ('108')
GROUP BY OrderDate,TeamKey,EmployeeKey,EmployeeID,EmployeeName
--Lấy thông tin giờ dư
INSERT INTO #temp (DateWork,TeamKey,EmployeeKey,EmployeeID,EmployeeName,
OverTime,MoneyBorrowShare,MoneyPersonal,MoneyPrivate,MoneyBorrowPrivate,MoneyTimePrivate,MoneyTimeShare)
SELECT DateImport,[dbo].[Fn_TeamKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate),EmployeeKey,EmployeeID,EmployeeName,
ISNULL([Money],0) ,
0,
0,
0,
0,
0,
0
FROM [dbo].[Temp_Import_Detail]
WHERE  DateImport BETWEEN @FROMDATE AND @TODATE 
AND RecordStatus <>99 @CustomParamater --AND EmployeeID in ('108')
---Tổng hợp thông tin từng ngày
CREATE TABLE #temp2(
DateWork datetime,
TeamKey int,
EmployeeKey nvarchar(50),
EmployeeID nvarchar(50),
EmployeeName nvarchar(250),
NgoaiGio money,	--Tiền ngoài giờ
KhoanSP money,	--Tại tổ ngày thường
LamCN money,	--Tại tỏ chủ nhật
LamLe money,	--Tại tổ lễ
KhoanSPKhac money,	--Tổ khác  ngày thường
KhoanSPCN money, -- Tổ khác chủ nhật
KhoanSPLe money	--Tổ khác lễ
)

INSERT INTO #temp2 (DateWork,TeamKey,EmployeeKey,EmployeeID,EmployeeName,
NgoaiGio,KhoanSP,LamCN,LamLe,KhoanSPKhac,KhoanSPCN,KhoanSPLe )
SELECT DateWork,TeamKey,EmployeeKey,EmployeeID,EmployeeName,
OverTime,
CASE
	WHEN [dbo].[LayHeSoNgayLeDonHang](DateWork) =0 AND DATENAME(weekday,DateWork) !='SUNDAY' THEN ISNULL(MoneyPersonal + MoneyPrivate,0)
END AS KhoanSP,
CASE
	WHEN [dbo].[LayHeSoNgayLeDonHang](DateWork) =0 AND DATENAME(weekday,DateWork) ='SUNDAY' THEN ISNULL( MoneyPersonal + MoneyPrivate ,0)
END AS LamCN,
CASE
	WHEN [dbo].[LayHeSoNgayLeDonHang](DateWork) >0 THEN ISNULL( MoneyPersonal+MoneyPrivate,0)
END AS LamLe,
CASE
	WHEN [dbo].[LayHeSoNgayLeDonHang](DateWork) =0 AND DATENAME(weekday,DateWork) !='SUNDAY' THEN ISNULL(MoneyBorrowPrivate + MoneyBorrowShare + MoneyTimePrivate + MoneyTimeShare ,0)
END AS KhoanSPKhac,
CASE
	WHEN [dbo].[LayHeSoNgayLeDonHang](DateWork) =0 AND DATENAME(weekday,DateWork) ='SUNDAY' THEN ISNULL(MoneyBorrowPrivate + MoneyBorrowShare + MoneyTimePrivate + MoneyTimeShare ,0)
END AS KhoanSPCN,
CASE
	WHEN [dbo].[LayHeSoNgayLeDonHang](DateWork) >0 THEN ISNULL( MoneyBorrowPrivate + MoneyBorrowShare + MoneyTimePrivate + MoneyTimeShare ,0)
END AS KhoanSPLe

FROM #temp 

;WITH BangTam AS (
SELECT TeamKey,EmployeeKey,EmployeeID,EmployeeName,dbo.Fn_GetTeamName(TeamKey) AS TeamName,
ISNULL(SUM(NgoaiGio),0) AS NgoaiGio,
ISNULL(SUM(KhoanSP),0) AS KhoanSP,
ISNULL(SUM(LamCN),0) AS LamCN,
ISNULL(SUM(LamLe),0) AS LamLe,
ISNULL(SUM(KhoanSPKhac),0)AS KhoanSPKhac,
ISNULL(SUM(KhoanSPCN),0) AS KhoanSPCN,
ISNULL(SUM(KhoanSPLe),0) AS KhoanSPLe
FROM #temp2 
GROUP BY TeamKey,EmployeeKey,EmployeeID,EmployeeName,dbo.Fn_GetTeamName(TeamKey) 
)
SELECT A.* FROM BangTam A
LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
ORDER BY D.RANK,C.RANK,B.RANK,LEN(EmployeeID), EmployeeID
DROP TABLE #temp
DROP TABLE #temp2";
            if (TeamKey > 0)
            {
                zSQL = zSQL.Replace("@CustomParamater", "AND [dbo].[Fn_TeamKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate) =@TeamKey");
            }
            else
            {
                zSQL = zSQL.Replace("@CustomParamater", "");
            }

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zCommand.CommandTimeout = 400;
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static double KTCC(string EmployeeKey, DateTime DateWrite, int GopTienGioDu)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_WORKER_KTCC_LaySotienKhoanDungDeTinhChuyenCan(@EmployeeKey,@FromDate,@ToDate,@GopTienGioDu)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@GopTienGioDu", SqlDbType.Int).Value = GopTienGioDu;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //Tiền chuyên cần
        public static double TCCT(string EmployeeKey, DateTime DateWrite, double TongLuongNangXuat, int GopTienGioDu)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_WORKER_TCCT_TinhTienChuyenCan(@EmployeeKey,@FromDate,@ToDate,@TongLuongNangXuat,@GopTienGioDu)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TongLuongNangXuat", SqlDbType.Money).Value = TongLuongNangXuat;
                zCommand.Parameters.Add("@GopTienGioDu", SqlDbType.Int).Value = GopTienGioDu;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //Số tiền hỗ trợ
        public static double STHT(string EmployeeKey, DateTime DateWrite, double TongLuongNangXuat)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_WORKER_STHT_TinhTienHoTro(@EmployeeKey,@FromDate,@ToDate,@TongLuongNangXuat)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TongLuongNangXuat", SqlDbType.Money).Value = TongLuongNangXuat;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        //Số ngày  phép năm
        public static double NCPN(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_WORKER_NCPN_SoNgayPhepNam(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //Só tiền Hỗ trợ phép năm
        public static double HTPN(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_WORKER_HTPN_HoTroPhepNam(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        //Số ngày PHÉP- LỄ TẾT
        public static double NLLT(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.[SLR_WORKER_NLLT_SoNgayPhep_LeTet](@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //Só tiền Hỗ trợ phép -lễ tết
        public static double HTLT(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.[SLR_WORKER_HTLT_HoTroNgayPhep_LeTet](@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }



        //Số ngày  thâm niên
        public static double NPTN(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.[SLR_WORKER_NPTN_SoNgayPhepThamNien](@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //Só tiền Hỗ trợ  thâm niên
        public static double HTTN(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.[SLR_WORKER_HTTN_HoTroNgayPhepThamNien] (@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        //Tiền khoán xuất hàng
        public static double TKXH(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_WORKER_TKXH_KhoanXuatHang(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //Đào tạo huấn luyện
        public static double DTHL(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_WORKER_DTHL_DaoTaoHuanLuyen(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        // KHOÁN HỖ TRỢ KHÁC- CỘT DỰ PHÒNG
        public static double HTKH(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_WORKER_HTKH_CacKhoanHoTroKhac(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        // Tổng cộng tiền sản xuất
        public static double TCSX(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_WORKER_TCSX_TongCongSanXuat(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        // Hỗ trợ tháng thấp điểm
        public static double HTTD(string EmployeeKey, DateTime DateWrite, int TinhThapDiem)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_WORKER_HTTD_HoTroThangThapDiem(@EmployeeKey,@FromDate,@ToDate,@TinhThapDiem)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TinhThapDiem", SqlDbType.Int).Value = TinhThapDiem;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        // Số lượng con nhỏ
        public static double SLCN(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_WORKER_SLCN_SoLuongConNho(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        // Số tiền con nhỏ
        public static double STCN(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_WORKER_STCN_SoTienConNho(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        // Số lượng cơm loại C
        public static double SLCC(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_WORKER_SLCC_SoLuongComLoai_C(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        // Số Tiền cơm loại C
        public static double STCC(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_WORKER_STCC_SoTienComLoai_C(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        // Số lượng cơm loại U
        public static double SLCU(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_WORKER_SLCU_SoLuongComLoai_U(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        // Số Tiền cơm loại C
        public static double STCU(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_WORKER_STCU_SoTienComLoai_U(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        // Số lượng công
        public static double SLTC(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_WORKER_SLTC_SoLuongCong(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        // Số tiền hoàn ứng lương
        public static double HUUL(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_WORKER_HUUL_HoanUngLuong(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        // Số tiền hoàn ứng xuất hàng
        public static double HUXH(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_WORKER_HUXH_HoanUngXuatHang(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        //chi tiết bhxh
        // Số tiền bảo hiểm xã hội- CTY
        public static double XHCT(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_WORKER_XHCT_BaoHiemXaHoi_CongTy(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        // Số tiền bảo hiểm Y TẾ - CTY
        public static double YTCT(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_WORKER_YTCT_BaoHiemYTe_CongTy(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        // Số tiền bảo hiểm THẤT NGHIỆP - CTY
        public static double NVCT(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_WORKER_NVCT_BaoHiemThatNghiep_CongTy(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        // Số tiền bảo hiểm xã hội CÁ NHÂN
        public static double XHCN(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_WORKER_XHCN_BaoHiemXaHoi_CaNhan(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        // Số tiền bảo hiểm Y TẾ - CÁ NHÂN
        public static double YTCN(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_WORKER_YTCN_BaoHiemYTe_CaNhan(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        // Số tiền bảo hiểm THẤT NGHIỆP - CTY
        public static double NVCN(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_WORKER_NVCN_BaoHiemThatNghiep_CaNhan(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //

        // Số tiền bảo hiểm xã hội
        public static double BHXH(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_WORKER_BHXH_BaoHiemXaHoi(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        // Số tiền đóng công đoàn-CÁ NHÂN
        public static double STCD(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_WORKER_STCD_CongDoan(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        // Số tiền đóng công đoàn -CTY
        public static double CDCT(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_WORKER_CDCT_KinhPhiCongDoan_CongTy(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //Thu nhập chịu thuế
        public static double TNCT(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_WORKER_TNCT_ThuNhapChiuThue(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        // Số tiền đóng thuế thu nhập cá nhân
        public static double TNCN(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_WORKER_TNCN_ThueThuNhapCaNhan(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        // Số tiền Tấm lòng vàng
        public static double TTLV(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_WORKER_TTLV_TamLongVang(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        // Số lượng cơm trừ
        public static double STCT(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_WORKER_STCT_SoLuongComLoai_T(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        // Số tiền cơm trừ
        public static double TTCT(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_WORKER_TTCT_SoTienComLoai_T(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        // Số tiền cơm trừ
        public static double CKTK(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.[SLR_WORKER_CKTK_CacKhoanTruKhac](@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        // Số tiền thực lãnh
        public static double STTL(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            double zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.SLR_WORKER_STTL_SoTienThucLanh(@EmployeeKey,@FromDate,@ToDate)  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
