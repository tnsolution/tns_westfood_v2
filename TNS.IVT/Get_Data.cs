﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Connect;

namespace TNS.IVT
{
    public class Get_Data
    {
        public static DataTable Products_HaveIn_Warehouse(int WarehouseKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.OrderID AS OrderID,B.ProductKey AS ProductKey,C.ProductID AS ProductID,C.ProductName AS ProductName,
B.UnitKey AS UnitKey,B.UnitName AS UnitName,B.QuantityReality AS InputQuatity,
[dbo].Fn_OutQuantity(A.OrderKey,B.ProductKey,A.WarehouseKey,B.UnitKey) AS OutputQuantity 
FROM [dbo].[IVT_Stock_Input] A
LEFT JOIN [dbo].[IVT_Stock_Input_Products] B ON A.OrderKey = B.OrderKey
LEFT JOIN [dbo].[IVT_Product] C ON CAST(C.ProductKey AS NVARCHAR(50))  = B.ProductKey
WHERE A.WarehouseKey = @WarehouseKey AND B.RecordStatus <> 99  AND A.RecordStatus <> 99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@WarehouseKey", SqlDbType.Int).Value = WarehouseKey;
                //zCommand.Parameters.Add("@ContentSearch", SqlDbType.NVarChar).Value = "%" + @ContentSearch + "%";
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable Get_One_Products_HaveIn_Warehouse(string OrderID)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.OrderID AS OrderID,B.ProductKey AS ProductKey,C.ProductID AS ProductID,C.ProductName AS ProductName,
B.UnitKey AS UnitKey,B.UnitName AS UnitName,B.QuantityReality AS InputQuatity,
[dbo].Fn_OutQuantity(A.OrderKey,B.ProductKey,A.WarehouseKey,B.UnitKey) AS OutputQuantity 
FROM [dbo].[IVT_Stock_Input] A
LEFT JOIN [dbo].[IVT_Stock_Input_Products] B ON A.OrderKey = B.OrderKey
LEFT JOIN [dbo].[IVT_Product] C ON CAST(C.ProductKey AS NVARCHAR(50))  = B.ProductKey
WHERE A.OrderID = @OrderID AND B.RecordStatus <> 99  AND A.RecordStatus <> 99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = OrderID;
                //zCommand.Parameters.Add("@ContentSearch", SqlDbType.NVarChar).Value = "%" + @ContentSearch + "%";
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable Get_List_Products_HaveIn_Warehouse(string ListOrder)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.OrderKey, A.OrderID,B.ProductKey,C.ProductID,C.ProductName,
B.UnitKey,B.UnitName,B.QuantityReality AS InputQuatity,
[dbo].Fn_OutQuantity(A.OrderKey,B.ProductKey,A.WarehouseKey,B.UnitKey) AS OutputQuantity 
FROM [dbo].[IVT_Stock_Input] A
LEFT JOIN [dbo].[IVT_Stock_Input_Products] B ON A.OrderKey = B.OrderKey
LEFT JOIN [dbo].[IVT_Product] C ON CAST(C.ProductKey AS NVARCHAR(50))  = B.ProductKey
WHERE A.OrderID IN (" + ListOrder + ") AND B.RecordStatus <> 99  AND A.RecordStatus <> 99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                //zCommand.Parameters.Add("@ListOrder", SqlDbType.NVarChar).Value = ListOrder;

                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable Get_Products_HaveIn_Warehouse(string OrderID, string ProductID)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
		SELECT A.OrderKey, A.OrderID,B.ProductKey,C.ProductID,C.ProductName,
B.UnitKey,B.UnitName,B.QuantityReality AS InputQuatity,
[dbo].Fn_OutQuantity(A.OrderKey,B.ProductKey,A.WarehouseKey,B.UnitKey) AS OutputQuantity 
FROM [dbo].[IVT_Stock_Input] A
LEFT JOIN [dbo].[IVT_Stock_Input_Products] B ON A.OrderKey = B.OrderKey
LEFT JOIN [dbo].[IVT_Product] C ON CAST(C.ProductKey AS NVARCHAR(50))  = B.ProductKey
WHERE A.OrderID IN (@OrderID) AND ProductID = @ProductID AND B.RecordStatus <> 99  AND A.RecordStatus <> 99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = OrderID;
                zCommand.Parameters.Add("@ProductID", SqlDbType.NVarChar).Value = ProductID;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable Get_Products_Output_Warehouse(string OrderID, string ProductID)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.OrderKey, A.OrderID,B.ProductKey,C.ProductID,C.ProductName,
B.UnitKey,B.UnitName,B.QuantityReality 
FROM [dbo].[IVT_Stock_Output] A
LEFT JOIN [dbo].[IVT_Stock_Output_Products] B ON A.OrderKey = B.OrderKey
LEFT JOIN [dbo].[IVT_Product] C ON CAST(C.ProductKey AS NVARCHAR(50))  = B.ProductKey
WHERE A.OrderID IN (@OrderID) AND ProductID = @ProductID AND B.RecordStatus <> 99  AND A.RecordStatus <> 99
";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = OrderID;
                zCommand.Parameters.Add("@ProductID", SqlDbType.NVarChar).Value = ProductID;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
    }
}
