﻿using System;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;

namespace TNS.IVT
{
    public class Product_Data
    {       

        public static DataTable List()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT TOP 100  * FROM IVT_Products  WHERE RecordStatus < 99 ORDER BY ProductID";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable SearchList(string Product)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.*,B.* FROM [dbo].[IVT_Product] A
                LEFT JOIN IVT_Product_Unit B ON A.BasicUnit = B.UnitKey
                 WHERE (A.ProductName LIKE @Product OR A.ProductID LIKE @Product) AND A.RecordStatus != 99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@Product", SqlDbType.NVarChar).Value = "%" + Product + "%";
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List(string ContentSearch)
        {
            DataTable zTable = new DataTable();
            string zSQL = "IVT_Product_Search_All";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@ContentSearch", SqlDbType.NVarChar).Value = "%" + @ContentSearch + "%";
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable Products_HaveIn_Warehouse(string ContentSearch, int WarehouseKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = "IVT_Product_HaveIn_Warehouse";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@WarehouseKey", SqlDbType.Int).Value = WarehouseKey;
                zCommand.Parameters.Add("@ContentSearch", SqlDbType.NVarChar).Value = "%" + @ContentSearch + "%";
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable Products_HaveIn_Warehouse(int WarehouseKey, DateTime FromDate, DateTime ToDate)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            DataTable zTable = new DataTable();
            string zSQL = @"
        SELECT A.OrderID AS OrderID,B.ProductKey AS ProductKey,C.ProductID AS ProductID,C.ProductName AS ProductName,
        B.UnitKey AS UnitKey,B.UnitName AS UnitName,B.QuantityReality AS InputQuatity,
        [dbo].Fn_OutQuantity(A.OrderKey,B.ProductKey,A.WarehouseKey,B.UnitKey) AS OutputQuantity FROM [dbo].[IVT_Stock_Input] A
        LEFT JOIN [dbo].[IVT_Stock_Input_Products] B ON A.OrderKey = B.OrderKey
        LEFT JOIN [dbo].[IVT_Product] C ON c.ProductKey = B.ProductKey
        WHERE A.WarehouseKey = @WarehouseKey AND 
		ROUND(B.QuantityReality-[dbo].Fn_OutQuantity(A.OrderKey,B.ProductKey,A.WarehouseKey,B.UnitKey),1) !=0 
		AND B.RecordStatus <> 99 AND A.RecordStatus = 43 AND A.OrderDate BETWEEN @FromDate AND @ToDate";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@WarehouseKey", SqlDbType.Int).Value = WarehouseKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                //zCommand.Parameters.Add("@ContentSearch", SqlDbType.NVarChar).Value = "%" + @ContentSearch + "%";
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable Products_HaveIn_Warehouse_Search(string ContentSearch, int WarehouseKey, DateTime Picker_From, DateTime Picker_To)
        {
            DateTime zFromDate = new DateTime(Picker_From.Year, Picker_From.Month, Picker_From.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(Picker_To.Year, Picker_To.Month, Picker_To.Day, 23, 0, 1);
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.OrderID AS OrderID,B.ProductKey AS ProductKey,C.ProductID AS ProductID,C.ProductName AS ProductName,
B.UnitKey AS UnitKey,B.UnitName AS UnitName,B.QuantityReality AS InputQuatity,
[dbo].Fn_OutQuantity(A.OrderKey,B.ProductKey,A.WarehouseKey,B.UnitKey) AS OutputQuantity FROM [dbo].[IVT_Stock_Input] A
LEFT JOIN [dbo].[IVT_Stock_Input_Products] B ON A.OrderKey = B.OrderKey
LEFT JOIN [dbo].[IVT_Product] C ON c.ProductKey = B.ProductKey
WHERE A.RecordStatus = 43 AND A.WarehouseKey = @WarehouseKey AND ROUND(B.QuantityReality-[dbo].Fn_OutQuantity(A.OrderKey,B.ProductKey,A.WarehouseKey,B.UnitKey),1) !=0 AND B.RecordStatus <> 99";
            if (ContentSearch.Length > 0)
            {
                zSQL += "AND A.OrderID LIKE @ContentSearch ";
            }
            else
            {
                zSQL += "AND A.OrderDate BETWEEN @Picker_From AND @Picker_To";
            }
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@WarehouseKey", SqlDbType.Int).Value = WarehouseKey;
                zCommand.Parameters.Add("@ContentSearch", SqlDbType.NVarChar).Value = "%" + @ContentSearch + "%";
                zCommand.Parameters.Add("@Picker_From", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@Picker_To", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable Products_Output_Warehouse_Search_All(string ContentSearch, int WarehouseKey, DateTime Picker_From, DateTime Picker_To)
        {
            DateTime zFromDate = new DateTime(Picker_From.Year, Picker_From.Month, Picker_From.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(Picker_To.Year, Picker_To.Month, Picker_To.Day, 23, 0, 1);
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT B.OrderKey,B.OrderID, A.ProductKey,C.ProductID,A.UnitKey AS UnitKey,
C.ProductName AS ProductName,A.UnitName AS UnitName ,A.QuantityReality 
FROM [dbo].[IVT_Stock_Output_Products] A
LEFT JOIN [dbo].[IVT_Stock_Output] B ON A.OrderKey = B.OrderKey
LEFT JOIN[dbo].[IVT_Product] C ON A.ProductKey = C.ProductKey
WHERE B.RecordStatus = 43 AND B.WarehouseKey = @WarehouseKey AND A.RecordStatus <>99  ";
            if (ContentSearch.Length > 0)
            {
                zSQL += "AND B.OrderID LIKE @ContentSearch ";
            }
            else
            {
                zSQL += "AND B.OrderDate BETWEEN @Picker_From AND @Picker_To";
            }
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@WarehouseKey", SqlDbType.Int).Value = WarehouseKey;
                zCommand.Parameters.Add("@ContentSearch", SqlDbType.NVarChar).Value = "%" + @ContentSearch + "%";
                zCommand.Parameters.Add("@Picker_From", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@Picker_To", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable Products_Output_Warehouse_Search_Product(string ContentSearch, int WarehouseKey, DateTime Picker_From, DateTime Picker_To)
        {
            DateTime zFromDate = new DateTime(Picker_From.Year, Picker_From.Month, Picker_From.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(Picker_To.Year, Picker_To.Month, Picker_To.Day, 23, 0, 1);
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT B.OrderKey,B.OrderID, A.ProductKey,C.ProductID,A.UnitKey AS UnitKey,
        C.ProductName AS ProductName,A.UnitName AS UnitName ,A.QuantityReality 
        FROM [dbo].[IVT_Stock_Output_Products] A
        LEFT JOIN [dbo].[IVT_Stock_Output] B ON A.OrderKey = B.OrderKey
        LEFT JOIN[dbo].[IVT_Product] C ON A.ProductKey = C.ProductKey
        WHERE B.RecordStatus = 43 AND B.WarehouseKey = @WarehouseKey AND A.RecordStatus <> 99 ";
            if (ContentSearch.Length > 0)
            {
                zSQL += "AND C.ProductName LIKE @ContentSearch ";
            }
            else
            {
                zSQL += "AND B.OrderDate BETWEEN @Picker_From AND @Picker_To";
            }
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@WarehouseKey", SqlDbType.Int).Value = WarehouseKey;
                zCommand.Parameters.Add("@ContentSearch", SqlDbType.NVarChar).Value = "%" + @ContentSearch + "%";
                zCommand.Parameters.Add("@Picker_From", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@Picker_To", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable Products_Output_Warehouse_Search_QuantityRealityReality(string ContentSearch, int WarehouseKey, DateTime Picker_From, DateTime Picker_To)
        {
            DateTime zFromDate = new DateTime(Picker_From.Year, Picker_From.Month, Picker_From.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(Picker_To.Year, Picker_To.Month, Picker_To.Day, 23, 0, 1);
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT B.OrderKey,B.OrderID, A.ProductKey,C.ProductID,A.UnitKey AS UnitKey,
        C.ProductName AS ProductName,A.UnitName AS UnitName ,A.QuantityReality 
        FROM [dbo].[IVT_Stock_Output_Products] A
        LEFT JOIN [dbo].[IVT_Stock_Output] B ON A.OrderKey = B.OrderKey
        LEFT JOIN[dbo].[IVT_Product] C ON A.ProductKey = C.ProductKey
        WHERE B.RecordStatus = 43 AND B.WarehouseKey = 1 AND A.RecordStatus<>99 ";
            if (ContentSearch.Length > 0)
            {
                zSQL += " AND A.QuantityReality LIKE @ContentSearch ";
            }
            else
            {
                zSQL += " AND B.OrderDate BETWEEN @Picker_From AND @Picker_To";
            }
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@WarehouseKey", SqlDbType.Int).Value = WarehouseKey;
                zCommand.Parameters.Add("@ContentSearch", SqlDbType.NVarChar).Value = "%" + @ContentSearch + "%";
                zCommand.Parameters.Add("@Picker_From", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@Picker_To", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List_Product(string ProductName)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM IVT_Product WHERE ProductName LIKE N'%" + ProductName + "%' ORDER BY ProductID";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable Products_HaveIn_Warehouse_Search_Product(string ContentSearch, int WarehouseKey, DateTime Picker_From, DateTime Picker_To)
        {
            DateTime zFromDate = new DateTime(Picker_From.Year, Picker_From.Month, Picker_From.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(Picker_To.Year, Picker_To.Month, Picker_To.Day, 23, 0, 1);
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.OrderID AS OrderID,B.ProductKey AS ProductKey,C.ProductID AS ProductID,C.ProductName AS ProductName,
B.UnitKey AS UnitKey,B.UnitName AS UnitName,B.QuantityReality AS InputQuatity,
[dbo].Fn_OutQuantity(A.OrderKey,B.ProductKey,A.WarehouseKey,B.UnitKey) AS OutputQuantity FROM [dbo].[IVT_Stock_Input] A
LEFT JOIN [dbo].[IVT_Stock_Input_Products] B ON A.OrderKey = B.OrderKey
LEFT JOIN [dbo].[IVT_Product] C ON c.ProductKey = B.ProductKey
WHERE A.RecordStatus = 43 AND A.WarehouseKey = @WarehouseKey AND ROUND(B.QuantityReality-[dbo].Fn_OutQuantity(A.OrderKey,B.ProductKey,A.WarehouseKey,B.UnitKey),1) !=0  AND  B.RecordStatus<>99 ";
            if (ContentSearch.Length > 0)
            {
                zSQL += "AND C.ProductName LIKE @ContentSearch ";
            }
            else
            {
                zSQL += "AND A.OrderDate BETWEEN @Picker_From AND @Picker_To";
            }
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@WarehouseKey", SqlDbType.Int).Value = WarehouseKey;
                zCommand.Parameters.Add("@ContentSearch", SqlDbType.NVarChar).Value = "%" + @ContentSearch + "%";
                zCommand.Parameters.Add("@Picker_From", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@Picker_To", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable Products_HaveIn_Warehouse_Search_Input(float ContentSearch, int WarehouseKey, DateTime Picker_From, DateTime Picker_To)
        {
            DateTime zFromDate = new DateTime(Picker_From.Year, Picker_From.Month, Picker_From.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(Picker_To.Year, Picker_To.Month, Picker_To.Day, 23, 0, 1);
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.OrderID AS OrderID,B.ProductKey AS ProductKey,C.ProductID AS ProductID,C.ProductName AS ProductName,
B.UnitKey AS UnitKey,B.UnitName AS UnitName,B.QuantityReality AS InputQuatity,
[dbo].Fn_OutQuantity(A.OrderKey,B.ProductKey,A.WarehouseKey,B.UnitKey) AS OutputQuantity FROM [dbo].[IVT_Stock_Input] A
LEFT JOIN [dbo].[IVT_Stock_Input_Products] B ON A.OrderKey = B.OrderKey
LEFT JOIN [dbo].[IVT_Product] C ON c.ProductKey = B.ProductKey
WHERE A.RecordStatus = 43 AND A.WarehouseKey = @WarehouseKey AND ROUND(B.QuantityReality-[dbo].Fn_OutQuantity(A.OrderKey,B.ProductKey,A.WarehouseKey,B.UnitKey),1) !=0  AND  B.RecordStatus<>99";
            if (ContentSearch > 0 || ContentSearch == 0)
            {
                zSQL += "AND B.QuantityReality = @ContentSearch ";
            }
            else
            {
                zSQL += "AND A.OrderDate BETWEEN @Picker_From AND @Picker_To";
            }
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@WarehouseKey", SqlDbType.Int).Value = WarehouseKey;
                zCommand.Parameters.Add("@ContentSearch", SqlDbType.Float).Value = ContentSearch;
                zCommand.Parameters.Add("@Picker_From", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@Picker_To", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable Search(string Name)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT TOP 100 * FROM IVT_Products WHERE ProductID LIKE @Name OR ProductName LIKE @Name AND RecordStatus < 99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List_Material()
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.*,dbo.Fn_GetUnitName(BasicUnit) AS UnitName,B.CategoryName AS CategoryName FROM IVT_Product  A
LEFT JOIN dbo.IVT_Product_Category B ON B.CategoryKey = A.CategoryKey
WHERE A.Parent = '0' AND A.RecordStatus < 99 ORDER BY LEN(A.ProductID), A.ProductID";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List_Material_Search(string Name)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.*,dbo.Fn_GetUnitName(BasicUnit) AS UnitName,B.CategoryName AS CategoryName FROM IVT_Product  A
LEFT JOIN dbo.IVT_Product_Category B ON B.CategoryKey = A.CategoryKey
WHERE  A.Parent = '0'  AND (ProductName LIKE @Name OR ProductID LIKE @Name ) AND A.RecordStatus < 99 ORDER BY ProductName";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List_Product_Of_Material(string Parent)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.*,dbo.Fn_GetUnitName(BasicUnit) AS UnitName,B.CategoryName AS CategoryName FROM IVT_Product  A
LEFT JOIN dbo.IVT_Product_Category B ON B.CategoryKey = A.CategoryKey
WHERE A.Parent = @Parent AND A.RecordStatus < 99 
ORDER BY LEN(A.ProductID), A.ProductID";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Parent", SqlDbType.NVarChar).Value = Parent;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List_Product_Of_Material_Search(string Parent, string Name)
        {
            DataTable zTable = new DataTable();
            string zSQL = "";
            if (Parent != string.Empty)
            {
                zSQL += @"

SELECT A.*,
dbo.Fn_GetUnitName(BasicUnit) AS UnitName,
B.CategoryName AS CategoryName 
FROM IVT_Product  A
LEFT JOIN dbo.IVT_Product_Category B ON B.CategoryKey = A.CategoryKey
WHERE A.RecordStatus <> 99 ";

                zSQL += " AND A.Parent = @Parent ";
                zSQL += "AND (ProductName LIKE @Name OR ProductID LIKE @Name ) ";
                zSQL += " ORDER BY LEN(A.ProductID), A.ProductID";
            }
        
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                zCommand.Parameters.Add("@Parent", SqlDbType.NVarChar).Value = Parent;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable SearchProduct(string Name)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT ProductKey, ProductID, ProductName FROM IVT_Product WHERE Parent != '0' AND ProductID LIKE N'%" + Name + "%' ORDER BY ProductID";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static int CountID(string Name)
        {
            int zResult = 0;
            string zSQL = "SELECT COUNT(ProductID) FROM IVT_Product WHERE ProductID = @Name AND RecordStatus <> 99 ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = Name.Trim();
                zResult = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }


        //mới 30/12/2020
        public static int CountID_NguyenLieu(string Name)
        {
            int zResult = 0;
            string zSQL = "SELECT COUNT(ProductID) FROM IVT_Product WHERE ProductID = @Name AND RecordStatus <> 99 AND Parent = '0' ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = Name.Trim();
                zResult = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
        public static DataTable List_NguyenLieu(string Name)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT ProductKey,ProductID,ProductName,dbo.Fn_GetUnitName(BasicUnit) AS UnitName,A.Rank,A.Description FROM IVT_Product  A
LEFT JOIN dbo.IVT_Product_Category B ON B.CategoryKey = A.CategoryKey
WHERE  A.Parent = '0'  AND (ProductName LIKE @Name OR ProductID LIKE @Name ) AND A.RecordStatus < 99 ORDER BY A.Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List_ThanhPham(string Parent, string Name)
        {
            DataTable zTable = new DataTable();
            string Filter = "";
            string zSQL = "";
            if (Parent != string.Empty && Parent!="0")
            {
                Filter += " AND A.Parent = @Parent ";
            }
            if(Name!=string.Empty)
            {
                Filter += " AND (ProductName LIKE @Name OR ProductID LIKE @Name )  ";
            }
                zSQL += @"
SELECT A.*,
dbo.Fn_GetUnitName(BasicUnit) AS UnitName,
B.CategoryName AS CategoryName 
FROM IVT_Product  A
LEFT JOIN dbo.IVT_Product_Category B ON B.CategoryKey = A.CategoryKey
WHERE A.RecordStatus <> 99 AND Parent!='0'  @Paramater 
ORDER BY A.Rank";
                zSQL = zSQL.Replace("@Paramater", Filter);
            

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                zCommand.Parameters.Add("@Parent", SqlDbType.NVarChar).Value = Parent;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
    }
}
