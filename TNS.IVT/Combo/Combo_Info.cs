﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Connect;

namespace TNS.IVT
{
    public class Combo_Info
    {
        #region [ Field Name ]
        private string _ComboKey = "";
        private string _ComboID = "";
        private string _ComboName = "";
        private double _ComboCost = 0;
        private string _ProductKey = "";
        private string _ProductID = "";
        private string _ProductName = "";
        private int _UnitKey = 0;
        private string _UnitName = "";
        private DateTime _FromDate;
        private DateTime _ToDate;
        private bool _Active;
        private string _Description = "";
        private int _RecordStatus = 0;
        private DateTime _CreatedOn;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _RoleID = "";
        private string _Message = "";
        private string _Parent = "";
        #endregion

        #region [ Properties ]
        //public bool IsCommandOK
        //{
        //    get
        //    {
        //        int zNumber = 0;
        //        if (_Message.Length > 0)
        //        {
        //            int.TryParse(_Message.Substring(0, 1), out zNumber);
        //        }
        //        if (zNumber <= 3)
        //            return true;
        //        else
        //            return false;
        //    }
        //}
        public bool HasInfo
        {
            get
            {
                if (_ComboKey.Trim().Length > 0)
                    return true;
                else
                    return false;
            }
        }
        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        public string Key
        {
            get { return _ComboKey; }
            set { _ComboKey = value; }
        }
        public string ComboID
        {
            get { return _ComboID; }
            set { _ComboID = value; }
        }
        public string ComboName
        {
            get { return _ComboName; }
            set { _ComboName = value; }
        }
        public double ComboCost
        {
            get { return _ComboCost; }
            set { _ComboCost = value; }
        }
        public string ProductKey
        {
            get { return _ProductKey; }
            set { _ProductKey = value; }
        }
        public string ProductID
        {
            get { return _ProductID; }
            set { _ProductID = value; }
        }
        public string ProductName
        {
            get { return _ProductName; }
            set { _ProductName = value; }
        }
        public int UnitKey
        {
            get { return _UnitKey; }
            set { _UnitKey = value; }
        }
        public string UnitName
        {
            get { return _UnitName; }
            set { _UnitName = value; }
        }
        public DateTime FromDate
        {
            get { return _FromDate; }
            set { _FromDate = value; }
        }
        public DateTime ToDate
        {
            get { return _ToDate; }
            set { _ToDate = value; }
        }
        public bool Active
        {
            get { return _Active; }
            set { _Active = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public string Parent
        {
            get
            {
                return _Parent;
            }

            set
            {
                _Parent = value;
            }
        }
        #endregion

        #region [ Constructor Get Information ]
        public Combo_Info()
        {
        }
        public Combo_Info(string ComboKey)
        {
            string zSQL = "SELECT * FROM IVT_Combo WHERE ComboKey = @ComboKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (ComboKey.Length == 36)
                    zCommand.Parameters.Add("@ComboKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(ComboKey);
                else
                    zCommand.Parameters.Add("@ComboKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _ComboKey = zReader["ComboKey"].ToString();
                    _ComboID = zReader["ComboID"].ToString().Trim();
                    _ComboName = zReader["ComboName"].ToString().Trim();
                    if (zReader["ComboCost"] != DBNull.Value)
                        _ComboCost = double.Parse(zReader["ComboCost"].ToString());
                    _ProductKey = zReader["ProductKey"].ToString().Trim();
                    _ProductID = zReader["ProductID"].ToString().Trim();
                    _ProductName = zReader["ProductName"].ToString().Trim();
                    if (zReader["FromDate"] != DBNull.Value)
                        _UnitKey = int.Parse(zReader["UnitKey"].ToString().Trim());
                    _UnitName = zReader["UnitName"].ToString().Trim();
                    if (zReader["FromDate"] != DBNull.Value)
                        _FromDate = (DateTime)zReader["FromDate"];
                    if (zReader["ToDate"] != DBNull.Value)
                        _ToDate = (DateTime)zReader["ToDate"];
                    if (zReader["Active"] != DBNull.Value)
                        _Active = (bool)zReader["Active"];
                    _Description = zReader["Description"].ToString().Trim();
                    _Parent = zReader["Parent"].ToString().Trim();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO IVT_Combo ("
        + " ComboKey, ComboID ,ComboName ,ComboCost ,ProductKey ,ProductID ,ProductName ,Parent, UnitKey ,UnitName ,FromDate ,ToDate ,Active ,Description ,RecordStatus ,CreatedOn ,CreatedBy ,CreatedName ,ModifiedOn ,ModifiedBy ,ModifiedName ) "
         + " VALUES ( "
         + "@ComboKey, @ComboID ,@ComboName ,@ComboCost ,@ProductKey ,@ProductID ,@ProductName , @Parent, @UnitKey ,@UnitName ,@FromDate ,@ToDate ,@Active ,@Description ,0 ,GETDATE(),@CreatedBy ,@CreatedName ,GETDATE(),@ModifiedBy ,@ModifiedName ) ";

            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                Guid zNewID = Guid.NewGuid();
                _ComboKey = zNewID.ToString();
                if (_ComboKey.Length == 36)
                    zCommand.Parameters.Add("@ComboKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_ComboKey);
                else
                    zCommand.Parameters.Add("@ComboKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@ComboID", SqlDbType.NVarChar).Value = _ComboID.Trim();
                zCommand.Parameters.Add("@ComboName", SqlDbType.NVarChar).Value = _ComboName.Trim();
                zCommand.Parameters.Add("@ComboCost", SqlDbType.Money).Value = _ComboCost;
                zCommand.Parameters.Add("@ProductKey", SqlDbType.NVarChar).Value = _ProductKey.Trim();
                zCommand.Parameters.Add("@ProductID", SqlDbType.NVarChar).Value = _ProductID.Trim();
                zCommand.Parameters.Add("@ProductName", SqlDbType.NVarChar).Value = _ProductName.Trim();
                zCommand.Parameters.Add("@UnitKey", SqlDbType.Int).Value = _UnitKey;
                zCommand.Parameters.Add("@UnitName", SqlDbType.NChar).Value = _UnitName.Trim();
                if (_FromDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = _FromDate;
                if (_ToDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = _ToDate;
                zCommand.Parameters.Add("@Active", SqlDbType.Bit).Value = _Active;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                zCommand.Parameters.Add("@Parent", SqlDbType.NVarChar).Value = _Parent.Trim();
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE IVT_Combo SET "
                        + " ComboID = @ComboID,"
                        + " ComboName = @ComboName,"
                        + " ComboCost = @ComboCost,"
                        + " ProductKey = @ProductKey,"
                        + " ProductID = @ProductID,"
                        + " ProductName = @ProductName,"
                        + " UnitKey = @UnitKey,"
                        + " UnitName = @UnitName,"
                        + " FromDate = @FromDate,"
                        + " ToDate = @ToDate,"
                        + " Active = @Active,"
                        + " Description = @Description,"
                        + " Parent = @Parent,"
                        + " RecordStatus = 1,"
                        + " ModifiedOn = GETDATE(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                       + " WHERE ComboKey = @ComboKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (_ComboKey.Length == 36)
                    zCommand.Parameters.Add("@ComboKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_ComboKey);
                else
                    zCommand.Parameters.Add("@ComboKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@ComboID", SqlDbType.NVarChar).Value = _ComboID.Trim();
                zCommand.Parameters.Add("@ComboName", SqlDbType.NVarChar).Value = _ComboName.Trim();
                zCommand.Parameters.Add("@ComboCost", SqlDbType.Money).Value = _ComboCost;
                zCommand.Parameters.Add("@ProductKey", SqlDbType.NVarChar).Value = _ProductKey.Trim();
                zCommand.Parameters.Add("@ProductID", SqlDbType.NVarChar).Value = _ProductID.Trim();
                zCommand.Parameters.Add("@ProductName", SqlDbType.NVarChar).Value = _ProductName.Trim();
                zCommand.Parameters.Add("@UnitKey", SqlDbType.Int).Value = _UnitKey;
                zCommand.Parameters.Add("@UnitName", SqlDbType.NChar).Value = _UnitName.Trim();
                if (_FromDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = _FromDate;
                if (_ToDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = _ToDate;
                zCommand.Parameters.Add("@Active", SqlDbType.Bit).Value = _Active;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                zCommand.Parameters.Add("@Parent", SqlDbType.NVarChar).Value = _Parent.Trim();
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE IVT_Combo SET [RecordStatus] = 99, [ModifiedOn] = GETDATE(), ModifiedBy = @ModifiedBy, ModifiedName = @ModifiedName WHERE ComboKey = @ComboKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ComboKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_ComboKey);
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string EmtyDetail()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM IVT_ComboItem  WHERE ComboKey = @ComboKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ComboKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_ComboKey);
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_ComboKey.Trim().Length == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        #endregion
    }
}
