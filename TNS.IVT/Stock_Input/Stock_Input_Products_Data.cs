﻿using System;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;

namespace TNS.IVT
{
    public class Stock_Input_Products_Data
    {
       
        public static DataTable GetProducts(string OrderKey)
        {
            string _Message = "";
            DataTable zTable = new DataTable();
            string zSQL = "SELECT A.*,B.ProductID, B.ProductName, C.OrderID FROM IVT_Stock_Input_Products A "
                       + "INNER JOIN IVT_Product B ON B.ProductKey = A.ProductKey  "
                       + "LEFT JOIN IVT_Stock_Input C ON C.OrderKey = A.OrderKey  "
                       + "WHERE A.OrderKey = @OrderKey AND A.RecordStatus <99 ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = new Guid(OrderKey);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();

            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            return zTable;
           
        }
    }
}
