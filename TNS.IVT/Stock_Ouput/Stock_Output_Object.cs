﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Connect;

namespace TNS.IVT
{
    public class Stock_Output_Object : Stock_Output_Info
    {
        private int _AutoKey = 0;
        private List<Stock_Output_Products_Info> _List_Products;
        private DataTable _ListNote;
        public DataTable ListNote
        {
            get
            {
                return _ListNote;
            }

            set
            {
                _ListNote = value;
            }
        }
        public List<Stock_Output_Products_Info> List_Products
        {
            get
            {
                return _List_Products;
            }

            set
            {
                _List_Products = value;
            }
        }
        public int AutoKey
        {
            get
            {
                return _AutoKey;
            }

            set
            {
                _AutoKey = value;
            }
        }

        public Stock_Output_Object() : base()
        {
            List_Products = new List<Stock_Output_Products_Info>();
            ListNote = new DataTable();
        }
        public Stock_Output_Object(string OrderID) : base(OrderID)
        {
            DataTable zTable = Stock_Output_Products_Data.GetProducts(base.Key);
            List_Products = new List<Stock_Output_Products_Info>();
            foreach (DataRow nRow in zTable.Rows)
            {
                Stock_Output_Products_Info Stock_Product = new Stock_Output_Products_Info(nRow);
                List_Products.Add(Stock_Product);
            }
            GetNotes();
        }
        public void Save_Object()
        {
            string zResult = "";
            base.Save();
            if (base.HasInfo && base.IsCommandOK)
            {
                for (int i = 0; i < List_Products.Count; i++)
                {
                    Stock_Output_Products_Info Stock_Product = (Stock_Output_Products_Info)List_Products[i];
                    Stock_Product.OrderKey = base.Key;
                    Stock_Product.CreatedBy = base.CreatedBy;
                    Stock_Product.CreatedName = base.CreatedName;
                    Stock_Product.ModifiedBy = base.ModifiedBy;
                    Stock_Product.ModifiedName = base.ModifiedName;
                    switch (Stock_Product.RecordStatus)
                    {
                        case 99:
                            Stock_Product.Delete_Detail();
                            break;
                        case 1:
                            Stock_Product.Create();
                            break;
                        case 2:
                            Stock_Product.Update();
                            break;
                    }
                    zResult += Stock_Product.Message;
                }
            }
            base.Message += zResult;
        }
        public void InsertNote(string NoteContent)
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "[dbo].[IVT_Stock_Output_Note_INSERT]";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = new Guid(base.Key);
                zCommand.Parameters.Add("@NoteContent", SqlDbType.NText).Value = NoteContent;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = base.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = base.CreatedName;

                base.Message = zCommand.ExecuteScalar().ToString();

                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                base.Message = "80" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        public void Delete_Object()
        {
            string zResult = "";
            base.Delete();
            Stock_Output_Products_Info Stock_Product = new Stock_Output_Products_Info();
            Stock_Product.ModifiedBy = base.ModifiedBy;
            Stock_Product.ModifiedName = base.ModifiedName;
            Stock_Product.OrderKey = base.Key;
            Stock_Product.Delete_All();
            zResult = Stock_Product.Message;
            base.Message = zResult;
        }
        private void GetNotes()
        {
            ListNote = new DataTable();
            string zSQL = "SELECT * FROM IVT_Stock_Output_Note "
                       + "WHERE OrderKey = @OrderKey AND RecordStatus != 99 ORDER BY CreatedOn DESC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = new Guid(base.Key);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(ListNote);
                zCommand.Dispose();

            }
            catch (Exception Err)
            {
                base.Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }

        }
        public void DeleteNote()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE IVT_Stock_Output_Note SET RecordStatus = 99 WHERE AutoKey = @AutoKey ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;

                zResult = zCommand.ExecuteNonQuery().ToString();

                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                base.Message = "80" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
    }
}
