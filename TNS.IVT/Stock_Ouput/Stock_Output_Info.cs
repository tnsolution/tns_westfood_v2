﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Connect;

namespace TNS.IVT
{
    public class Stock_Output_Info
    {
        #region [ Field Name ]
        private string _OrderKey = "";
        private string _OrderID = "";
        private DateTime? _OrderDate = null;
        private string _OrderDescription = "";
        private int _WarehouseKey = 0;
        private int _WarehouseLocal = 0;
        private bool _IsInternal;
        private string _Deliverer = "";
        private string _DocumentAttached = "";
        private DateTime? _DocumentDate = null;
        private string _DocumentOrigin = "";
        private double _AmountOrderMain = 0;
        private double _AmountOrderForeign = 0;
        private double _AmountToTalMain = 0;
        private string _CurrencyID = "";
        private float _CurrencyRate = 0;
        private string _CustomerKey = "";
        private int _Slug = 0;
        private int _CategoryKey = 0;
        private int _RecordStatus = 0;
        private string _Organization = "";
        private DateTime? _CreatedOn = null;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime? _ModifiedOn = null;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _Message = "";
        private string _RoleID = "";
        private string _CreditAccount = "";
        private string _DebitAccount = "";


        #endregion

        #region [ Constructor Get Information ]
        public Stock_Output_Info()
        {
        }
        public Stock_Output_Info(string OrderID)
        {
            string zSQL = "SELECT * FROM IVT_Stock_Output WHERE OrderID = @OrderID AND RecordStatus < 99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = OrderID;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _OrderKey = zReader["OrderKey"].ToString();
                    _OrderID = zReader["OrderID"].ToString();
                    if (zReader["OrderDate"] != DBNull.Value)
                        _OrderDate = (DateTime)zReader["OrderDate"];
                    _OrderDescription = zReader["OrderDescription"].ToString();
                    if (zReader["WarehouseKey"] != DBNull.Value)
                        _WarehouseKey = int.Parse(zReader["WarehouseKey"].ToString());
                    if (zReader["WarehouseLocal"] != DBNull.Value)
                        _WarehouseLocal = int.Parse(zReader["WarehouseLocal"].ToString());
                    _IsInternal = (bool)zReader["IsInternal"];
                    _Deliverer = zReader["Deliverer"].ToString();
                    _DocumentAttached = zReader["DocumentAttached"].ToString();
                    if (zReader["DocumentDate"] != DBNull.Value)
                        _DocumentDate = (DateTime)zReader["DocumentDate"];

                    if (zReader["AmountOrderMain"] != DBNull.Value)
                        _AmountOrderMain = double.Parse(zReader["AmountOrderMain"].ToString());
                    if (zReader["AmountOrderForeign"] != DBNull.Value)
                        _AmountOrderForeign = double.Parse(zReader["AmountOrderForeign"].ToString());
                    if (zReader["AmountTotalMain"] != DBNull.Value)
                        _AmountToTalMain = double.Parse(zReader["AmountTotalMain"].ToString());
                    _CurrencyID = zReader["CurrencyID"].ToString();
                    if (zReader["CurrencyRate"] != DBNull.Value)
                        _CurrencyRate = float.Parse(zReader["CurrencyRate"].ToString());

                    _CustomerKey = zReader["CustomerKey"].ToString();
                    if (zReader["CategoryKey"] != DBNull.Value)
                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    _Organization = zReader["Organization"].ToString();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString();
                    _CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    _ModifiedName = zReader["ModifiedName"].ToString();
                    _CreditAccount = zReader["CreditAccount"].ToString();
                    _DebitAccount = zReader["DebitAccount"].ToString();
                    if (zReader["Slug"] != DBNull.Value)
                        _Slug = int.Parse(zReader["Slug"].ToString());
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion

        #region [ Properties ]
        public string Key
        {
            get { return _OrderKey; }
            set { _OrderKey = value; }
        }
        public bool HasInfo
        {
            get
            {
                if (_OrderKey.Trim().Length > 0)
                    return true;
                else
                    return false;
            }
        }
        public string ID
        {
            get { return _OrderID; }
            set { _OrderID = value; }
        }
        public DateTime? OrderDate
        {
            get { return _OrderDate; }
            set { _OrderDate = value; }
        }
        public string Description
        {
            get { return _OrderDescription; }
            set { _OrderDescription = value; }
        }
        public int WarehouseKey
        {
            get { return _WarehouseKey; }
            set { _WarehouseKey = value; }
        }
        public int WarehouseLocal
        {
            get { return _WarehouseLocal; }
            set { _WarehouseLocal = value; }
        }
        public bool IsInternal
        {
            get { return _IsInternal; }
            set { _IsInternal = value; }
        }
        public string Deliverer
        {
            get { return _Deliverer; }
            set { _Deliverer = value; }
        }
        public string DocumentAttached
        {
            get { return _DocumentAttached; }
            set { _DocumentAttached = value; }
        }
        public DateTime? DocumentDate
        {
            get { return _DocumentDate; }
            set { _DocumentDate = value; }
        }
        public string DocumentOrigin
        {
            get { return _DocumentOrigin; }
            set { _DocumentOrigin = value; }
        }
        public double AmountOrderMain
        {
            get { return _AmountOrderMain; }
            set { _AmountOrderMain = value; }
        }
        public string CustomerKey
        {
            get { return _CustomerKey; }
            set { _CustomerKey = value; }
        }
        public int CategoryKey
        {
            get { return _CategoryKey; }
            set { _CategoryKey = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime? CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime? ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        public string RoleID
        {
            set { _RoleID = value; }
        }
        public bool IsCommandOK
        {
            get
            {
                int zNumber = 0;
                if (_Message.Length > 0)
                {
                    int.TryParse(_Message.Substring(0, 1), out zNumber);
                }
                if (zNumber <= 3)
                    return true;
                else
                    return false;
            }
        }

        public string CreditAccount
        {
            get
            {
                return _CreditAccount;
            }

            set
            {
                _CreditAccount = value;
            }
        }

        public string DebitAccount
        {
            get
            {
                return _DebitAccount;
            }

            set
            {
                _DebitAccount = value;
            }
        }

        public int Slug
        {
            get
            {
                return _Slug;
            }

            set
            {
                _Slug = value;
            }
        }

        public double AmountOrderForeign
        {
            get
            {
                return _AmountOrderForeign;
            }

            set
            {
                _AmountOrderForeign = value;
            }
        }

        public double AmountToTalMain
        {
            get
            {
                return _AmountToTalMain;
            }

            set
            {
                _AmountToTalMain = value;
            }
        }

        public string CurrencyID
        {
            get
            {
                return _CurrencyID;
            }

            set
            {
                _CurrencyID = value;
            }
        }

        public float CurrencyRate
        {
            get
            {
                return _CurrencyRate;
            }

            set
            {
                _CurrencyRate = value;
            }
        }

        public string Organization
        {
            get
            {
                return _Organization;
            }

            set
            {
                _Organization = value;
            }
        }
        #endregion

        #region [ Constructor Update Information ]

        public void Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "[dbo].[IVT_Stock_Output_INSERT]";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = _OrderID;
                if (_OrderDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = _OrderDate;
                zCommand.Parameters.Add("@OrderDescription", SqlDbType.NVarChar).Value = _OrderDescription;
                zCommand.Parameters.Add("@WarehouseKey", SqlDbType.Int).Value = _WarehouseKey;
                zCommand.Parameters.Add("@WarehouseLocal", SqlDbType.Int).Value = _WarehouseLocal;
                zCommand.Parameters.Add("@IsInternal", SqlDbType.Bit).Value = _IsInternal;
                zCommand.Parameters.Add("@Deliverer", SqlDbType.NVarChar).Value = _Deliverer;
                zCommand.Parameters.Add("@DocumentAttached", SqlDbType.NVarChar).Value = _DocumentAttached;
                if (_DocumentDate == null)
                    zCommand.Parameters.Add("@DocumentDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DocumentDate", SqlDbType.DateTime).Value = _DocumentDate;
                zCommand.Parameters.Add("@DocumentOrigin", SqlDbType.NVarChar).Value = _DocumentOrigin;
                zCommand.Parameters.Add("@AmountOrderMain", SqlDbType.Money).Value = _AmountOrderMain;
                zCommand.Parameters.Add("@AmountOrderForeign", SqlDbType.Money).Value = _AmountOrderForeign;
                zCommand.Parameters.Add("@AmountTotalMain", SqlDbType.Money).Value = _AmountToTalMain;
                zCommand.Parameters.Add("@CurrencyID", SqlDbType.NVarChar).Value = _CurrencyID;
                zCommand.Parameters.Add("@CurrencyRate", SqlDbType.Money).Value = _CurrencyRate;
                if (_CustomerKey.Length > 0)
                    zCommand.Parameters.Add("@CustomerKey", SqlDbType.UniqueIdentifier).Value = new Guid(_CustomerKey);
                else
                    zCommand.Parameters.Add("@CustomerKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@Organization", SqlDbType.NVarChar).Value = _Organization;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zCommand.Parameters.Add("@CreditAccount", SqlDbType.NVarChar).Value = _CreditAccount;
                zCommand.Parameters.Add("@DebitAccount", SqlDbType.NVarChar).Value = _DebitAccount;
                zCommand.Parameters.Add("@Slug", SqlDbType.NVarChar).Value = _Slug;
                _Message = zCommand.ExecuteScalar().ToString();
                if (_Message.Substring(0, 2) == "11")
                {
                    _OrderKey = _Message.Substring(2, 36);
                    _OrderID = _Message.Substring(38);
                }
                else
                    _OrderKey = "";

                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "80" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        public string Update()
        {
            string zSQL = "IVT_Stock_Output_UPDATE";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = new Guid(_OrderKey);
                zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = _OrderID;
                if (_OrderDate == null)
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = _OrderDate;
                zCommand.Parameters.Add("@OrderDescription", SqlDbType.NVarChar).Value = _OrderDescription;
                zCommand.Parameters.Add("@WarehouseKey", SqlDbType.Int).Value = _WarehouseKey;
                zCommand.Parameters.Add("@WarehouseLocal", SqlDbType.Int).Value = _WarehouseLocal;
                zCommand.Parameters.Add("@IsInternal", SqlDbType.Bit).Value = _IsInternal;
                zCommand.Parameters.Add("@Deliverer", SqlDbType.NVarChar).Value = _Deliverer;
                zCommand.Parameters.Add("@DocumentAttached", SqlDbType.NVarChar).Value = _DocumentAttached;
                if (_DocumentDate == null)
                    zCommand.Parameters.Add("@DocumentDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DocumentDate", SqlDbType.DateTime).Value = _DocumentDate;
                zCommand.Parameters.Add("@DocumentOrigin", SqlDbType.NVarChar).Value = _DocumentOrigin;
                zCommand.Parameters.Add("@AmountOrderMain", SqlDbType.Money).Value = _AmountOrderMain;
                zCommand.Parameters.Add("@AmountOrderForeign", SqlDbType.Money).Value = _AmountOrderForeign;
                zCommand.Parameters.Add("@AmountTotalMain", SqlDbType.Money).Value = _AmountToTalMain;
                zCommand.Parameters.Add("@CurrencyID", SqlDbType.NVarChar).Value = _CurrencyID;
                zCommand.Parameters.Add("@CurrencyRate", SqlDbType.Money).Value = _CurrencyRate;
                if (_CustomerKey.Length > 0)
                    zCommand.Parameters.Add("@CustomerKey", SqlDbType.UniqueIdentifier).Value = new Guid(_CustomerKey);
                else
                    zCommand.Parameters.Add("@CustomerKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;

                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zCommand.Parameters.Add("@CreditAccount", SqlDbType.NVarChar).Value = _CreditAccount;
                zCommand.Parameters.Add("@DebitAccount", SqlDbType.NVarChar).Value = _DebitAccount;
                zCommand.Parameters.Add("@Slug", SqlDbType.NVarChar).Value = _Slug;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public void Save()
        {
            if (_OrderKey.Trim().Length == 0)
                Create();
            else
                Update();
        }
        public void Delete()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "IVT_Stock_Output_DEL";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = new Guid(_OrderKey);
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "80" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        public void Send()
        {
            CommandRequest(41);
        }
        public void Approve()
        {
            CommandRequest(43);
        }
        public void Denied()
        {
            CommandRequest(42);
        }
        public void CommandRequest(int CommandID)
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "IVT_Stock_Output_PROCESS";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = new Guid(_OrderKey);
                zCommand.Parameters.Add("@CommandProcess", SqlDbType.Int).Value = CommandID;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "80" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        #endregion
    }
}
