﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TN.Connect;

namespace TNS.IVT
{
    public class Team_Working_Data
    {

        public static DataTable ListTeams()
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.TeamKey AS TeamKey,A.TeamName AS Nhom,
COUNT(B.TeamKey) AS SoLuong 
FROM [dbo].[SYS_Team] A
LEFT JOIN [dbo].[HRM_Team_Member_Default] B ON A.TeamKey = B.TeamKey
GROUP BY A.TeamKey,A.TeamName,B.TeamKey  ORDER BY A.TeamName ASC";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable ListTeams_Name(DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 1);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 0, 1);
            string zSQL = @"SELECT A.TeamWorkingKey AS TeamWorkingKey,A.TeamName AS TenNhom, COUNT(B.EmployeeKey) AS SoLuong,A.WorkingStatus AS TrangThai 
        FROM [dbo].[FTR_Team_Working] A
        LEFT JOIN [dbo].[FTR_Team_Member] B ON A.TeamWorkingKey = B.TeamWorkingKey
        WHERE A.WorkBegin BETWEEN @FromDate AND @ToDate
        GROUP BY a.TeamWorkingKey,A.TeamName,A.WorkingStatus  ORDER BY A.TeamName ASC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable ListNames(string TeamWorkingKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.TeamWorkingKey AS TeamWorkingKey,B.EmployeeKey AS EmployeeKey,
D.FullName AS EmployeeName,D.CardID AS CardID FROM [dbo].[FTR_Team_Working] A
LEFT JOIN [dbo].[FTR_Team_Member] B ON A.TeamWorkingKey = B.TeamWorkingKey
LEFT JOIN [dbo].[HRM_Employee] C ON C.EmployeeKey = B.EmployeeKey
LEFT JOIN [dbo].[SYS_Personal] D ON D.ParentKey =C.EmployeeKey
WHERE A.TeamWorkingKey = @TeamWorkingKey
ORDER BY B.Numerical";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TeamWorkingKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(TeamWorkingKey);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable ListNames(int TeamKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT C.EmployeeKey,D.FullName AS EmployeeName,D.CardID,B.Numerical FROM [dbo].[SYS_Team] A
LEFT JOIN [dbo].[HRM_Team_Member_Default] B ON A.TeamKey = B.TeamKey
LEFT JOIN [dbo].[HRM_Employee] C ON C.EmployeeKey = B.EmployeeKey
LEFT JOIN [dbo].[SYS_Personal] D ON D.ParentKey = C.EmployeeKey
WHERE A.TeamKey = @TeamKey
ORDER BY Numerical";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable Find_Employee()
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.EmployeeKey,B.LastName,B.FirstName,B.CardID FROM [dbo].[HRM_Employee] A
LEFT JOIN  [dbo].[SYS_Personal] B ON B.ParentKey = A.EmployeeKey
WHERE [EmployeeKey] NOT IN (SELECT [EmployeeKey] FROM [dbo].[FTR_Team_Member])";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable Find_EmployeeName(string CardID)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.EmployeeKey,B.LastName,B.FirstName,B.CardID FROM [dbo].[HRM_Employee] A
LEFT JOIN  [dbo].[SYS_Personal] B ON B.ParentKey = A.EmployeeKey
WHERE [EmployeeKey] NOT IN (SELECT [EmployeeKey] FROM [dbo].[HRM_Team_Member_Default]) AND B.CardID = N'" + CardID + "'";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        //        public static DataTable ListTeams(DateTime FromDate, DateTime ToDate)
        //        {
        //            DataTable zTable = new DataTable();
        //            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 1);
        //            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 0, 1);
        //            string zSQL = @"SELECT A.TeamKey,A.TeamName AS TenNhom
        //FROM [dbo].[FTR_Team_Working] A
        //LEFT JOIN [dbo].[FTR_Team_Member] B ON A.TeamWorkingKey = B.TeamWorkingKey
        //WHERE WorkBegin BETWEEN @FromDate AND @ToDate
        //GROUP BY A.TeamName ,A.WorkingStatus,A.TeamWorkingKey  ORDER BY A.TeamName ASC";
        //            string zConnectionString = ConnectDataBase.ConnectionString;
        //            try
        //            {
        //                SqlConnection zConnect = new SqlConnection(zConnectionString);
        //                zConnect.Open();
        //                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
        //                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
        //                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
        //                zAdapter.Fill(zTable);
        //                zCommand.Dispose();
        //                zConnect.Close();
        //            }
        //            catch (Exception ex)
        //            {
        //                string zstrMessage = ex.ToString();
        //            }
        //            return zTable;
        //        }

        //        public static DataTable List_Group()
        //        {
        //            DataTable zTable = new DataTable();
        //            string zSQL = @"SELECT A.TeamWorkingKey AS TeamWorkingKey,A.TeamName AS TenNhom, COUNT(B.EmployeeKey) AS SoLuong,A.WorkingStatus AS TrangThai 
        //FROM [dbo].[FTR_Team_Working] A
        //LEFT JOIN [dbo].[FTR_Team_Member] B ON A.TeamWorkingKey = B.TeamWorkingKey
        //WHERE A.WorkingStatus != 0
        //GROUP BY A.TeamName ,A.WorkingStatus,A.TeamWorkingKey  ORDER BY A.TeamName ASC";
        //            string zConnectionString = ConnectDataBase.ConnectionString;
        //            try
        //            {
        //                SqlConnection zConnect = new SqlConnection(zConnectionString);
        //                zConnect.Open();
        //                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
        //                zAdapter.Fill(zTable);
        //                zCommand.Dispose();
        //                zConnect.Close();
        //            }
        //            catch (Exception ex)
        //            {
        //                string zstrMessage = ex.ToString();
        //            }
        //            return zTable;
        //        }

        //        public static DataTable ListTeam_Productivity(DateTime FromDate, DateTime ToDate)
        //        {
        //            DataTable zTable = new DataTable();
        //            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 1);
        //            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 0, 1);
        //            string zSQL = @"SELECT A.TeamWorkingKey AS TeamWorkingKey,A.TeamName AS TenNhom,
        //COUNT(B.EmployeeKey) AS SoLuong,A.WorkingStatus AS TrangThai,A.TeamKey AS TeamKey 
        //FROM [dbo].[FTR_Team_Working] A
        //LEFT JOIN [dbo].[FTR_Team_Member] B ON A.TeamWorkingKey = B.TeamWorkingKey
        //WHERE A.WorkingStatus = 1 AND WorkBegin BETWEEN @FromDate AND @ToDate
        //GROUP BY a.TeamWorkingKey,A.TeamName,A.WorkingStatus,A.TeamKey  ORDER BY A.TeamName ASC";
        //            string zConnectionString = ConnectDataBase.ConnectionString;
        //            try
        //            {
        //                SqlConnection zConnect = new SqlConnection(zConnectionString);
        //                zConnect.Open();
        //                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
        //                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
        //                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
        //                zAdapter.Fill(zTable);
        //                zCommand.Dispose();
        //                zConnect.Close();
        //            }
        //            catch (Exception ex)
        //            {
        //                string zstrMessage = ex.ToString();
        //            }
        //            return zTable;
        //        }

        //        public static DataTable GetWorking(int TeamKey)
        //        {
        //            DataTable zTable = new DataTable();
        //            string zSQL = @"SELECT count(A.TeamKey) FROM [dbo].[FTR_Team_Working] A
        //LEFT JOIN[dbo].[HRM_Teams] B ON A.TeamKey =B.TeamKey
        //WHERE WorkingStatus <= 1 AND A.TeamKey = @TeamKey";
        //            string zConnectionString = ConnectDataBase.ConnectionString;
        //            try
        //            {
        //                SqlConnection zConnect = new SqlConnection(zConnectionString);
        //                zConnect.Open();
        //                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //                zCommand.CommandType = CommandType.Text;
        //                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
        //                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
        //                zAdapter.Fill(zTable);
        //                zCommand.Dispose();
        //                zConnect.Close();
        //            }
        //            catch (Exception ex)
        //            {
        //                string zstrMessage = ex.ToString();
        //            }
        //            return zTable;
        //        }

        public static DataTable GetWorking(int TeamKey, DateTime FromDate, DateTime ToDate)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 1);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 0, 1);
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT count(A.TeamKey) FROM [dbo].[FTR_Team_Working] A
        LEFT JOIN[dbo].[SYS_Team] B ON A.TeamKey =B.TeamKey
        WHERE WorkingStatus <= 1 AND A.TeamKey = @TeamKey
        AND WorkBegin BETWEEN @FromDate AND @ToDate";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        //        public static DataTable List(int TeamKey)
        //        {
        //            DataTable zTable = new DataTable();
        //            string zSQL = @"SELECT B.EmployeeKey,C.EmployeeName,C.CardID FROM [dbo].[FTR_Team_Working] A
        //LEFT JOIN [dbo].[FTR_Team_Member] B ON A.TeamWorkingKey = B.TeamWorkingKey
        //LEFT JOIN [dbo].[HRM_Employees] C ON B.EmployeeKey = C.EmployeeKey
        //WHERE A.TeamKey = @TeamKey";
        //            string zConnectionString = ConnectDataBase.ConnectionString;
        //            try
        //            {
        //                SqlConnection zConnect = new SqlConnection(zConnectionString);
        //                zConnect.Open();
        //                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
        //                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
        //                zAdapter.Fill(zTable);
        //                zCommand.Dispose();
        //                zConnect.Close();
        //            }
        //            catch (Exception ex)
        //            {
        //                string zstrMessage = ex.ToString();
        //            }
        //            return zTable;
        //        }


    }
}
