﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Connect;
using TNS.Misc;

namespace TNS.RPT
{
    public class Report_Salary
    {
        #region[báo cáo tổng hợp lương tháng  -chi tiết khối văn phòng - Chị Trang]
        public static DataTable ListCodeOfficeFull(DateTime DateWrite)
        {
            var zTime = DateWrite.FirstEndMonth();
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT * FROM(
	SELECT CodeID,(TypeName+' '+CodeName) AS CodeName,Rank  FROM [dbo].[SLR_ReportOffice_Close]
	WHERE RecordStatus <> 99 AND Parent IS NOT NULL 
	AND DateWrite BETWEEN @FromDate AND @ToDate  
	GROUP BY CodeID,(TypeName+' '+CodeName),Rank
	)X
ORDER BY X.[Rank]";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zTime.Item1;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zTime.Item2;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable BAOCAOLUONGTONGHOPGIANTIEP_CHITIET_NHOMMACDINH(DateTime FromDate, int DepartmentKey, int TeamKey, string Salary)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            string Fillter = "";
            if (Salary.Length > 0)
            {
                Fillter += " AND " + Salary;
            }
            if (DepartmentKey > 0)
            {
                Fillter += " AND A.DepartmentKey = @DepartmentKey";
            }
            if (TeamKey > 0)
            {
                Fillter += " AND A.TeamKey = @TeamKey";
            }
            DataTable zTable = new DataTable();
            string zSQL = @"
--declare @FromDate datetime ='2020-06-01 00:00:00'
--declare @ToDate datetime ='2020-06-30 23:59:59'

CREATE TABLE #Temp(
DepartmentKey nvarchar(10),
DepartmentID nvarchar(50),
DepartmentName nvarchar(500),
TeamKey nvarchar(10),
TeamID nvarchar(50),
TeamName nvarchar(500),
AccountCode nvarchar(50),
Amount money,
CodeID nvarchar(250),
CodeName nvarchar(500),
TypeName  nvarchar(500),
[Rank] INT
)
--Lấy tất cả mã trong bảng lương văn phòng
		INSERT INTO #Temp
		SELECT DepartmentKey,DepartmentID,DepartmentName,TeamKey,TeamID,TeamName,[dbo].[Fn_GetAccountCodeEmployee](TeamKey),ROUND(SUM(Amount),0) AS Amount,
		CodeID,CodeName,TypeName,[Rank]
		FROM [dbo].[SLR_ReportOffice_Close]
		WHERE DateWrite BETWEEN @FromDate and @ToDate AND Parent IS  NOT NULL  
		AND RecordStatus <> 99
		GROUP BY DepartmentKey,DepartmentID,DepartmentName,TeamKey,TeamID,TeamName,[dbo].[Fn_GetAccountCodeEmployee](TeamKey),CodeID,CodeName,TypeName,[Rank]
--Lấy tiền cơm trưa Cơm C +cơm U trong bảng lương văn phòng
		INSERT INTO #Temp
		SELECT DepartmentKey,DepartmentID,DepartmentName,TeamKey,TeamID,TeamName,[dbo].[Fn_GetAccountCodeEmployee](TeamKey),ROUND(SUM(Amount),0) AS Amount,
		'ZCOM',N'TIỀN CƠM TRƯA','',97
		FROM [dbo].[SLR_ReportOffice_Close]
		WHERE DateWrite BETWEEN @FromDate and @ToDate AND Parent IS  NOT NULL  
		AND RecordStatus <> 99
        AND CategoryKey = 6 -- Tiền cơm cộng
		--AND (CodeID='STCC' OR CodeID = 'STCU')
		GROUP BY DepartmentKey,DepartmentID,DepartmentName,TeamKey,TeamID,TeamName,[dbo].[Fn_GetAccountCodeEmployee](TeamKey),CodeID,CodeName,TypeName
--Lấy tiền cơm ăn căn teen
		;WITH #TAM_CANTEEN AS (
		(
				--Lấy tiền cơm
				SELECT DepartmentKey,TeamKey,TeamID,TeamName,
				''AS AccountCode,Amount
				FROM(
				SELECT DepartmentKey,TeamKey,TeamID,TeamName, SUM(Money1+Money2+Money3) AS Amount
				FROM [dbo].[FNC_RiceTeamCompany]
				WHERE DateWrite BETWEEN @FromDate and @ToDate 
				AND RecordStatus <> 99 AND BranchKey=2
				GROUP BY DepartmentKey,TeamKey,TeamID,TeamName)X
			)
			UNION  
			(
			--Lấy mã Accoutcode 
			SELECT DepartmentKey,TeamKey,TeamID,TeamName,
			[dbo].[Fn_GetAccountCodeEmployee](TeamKey) AS AccountCode,0
			FROM [dbo].[SLR_ReportOffice_Close]
			WHERE DateWrite BETWEEN @FromDate and @ToDate AND Parent IS  NOT NULL  
			AND RecordStatus <> 99
			GROUP BY DepartmentKey,TeamKey,TeamID,TeamName,[dbo].[Fn_GetAccountCodeEmployee](TeamKey)
			)
		)
		--SELECT * FROM #TAM_CANTEEN
		INSERT INTO #Temp
		SELECT DepartmentKey,[dbo].[Fn_GetDepartmentID](DepartmentKey),[dbo].[Fn_GetDepartmentName](DepartmentKey),TeamKey,TeamID,TeamName,
		AccountCode,Amount,
		'ZCCT',N'TIỀN CƠM TRẢ ĐƠN VỊ NẤU','',98
		FROM #TAM_CANTEEN
--Lấy sô lương lao động từ bảng lương văn phòng
		INSERT INTO #Temp
		SELECT DepartmentKey,DepartmentID,DepartmentName,TeamKey,TeamID,TeamName,
		[dbo].[Fn_GetAccountCodeEmployee](TeamKey) AS AccountCode, COUNT(DISTINCT(EmployeeKey)) AS Amount,
		'ZNLD',N'SỐ LAO ĐỘNG','',99
		FROM [dbo].[SLR_ReportOffice_Close]
		WHERE DateWrite BETWEEN @FromDate and @ToDate 
		AND RecordStatus <> 99 AND BranchKey=2
		GROUP BY DepartmentKey,DepartmentID,DepartmentName,TeamKey,TeamID,TeamName,[dbo].[Fn_GetAccountCodeEmployee](TeamKey)
--Lấy các khoản trích theo lương
		;WITH #TAM_TRICHLUONG AS(
			SELECT DepartmentKey,DepartmentID,DepartmentName,TeamKey,TeamID,TeamName,
			[dbo].[Fn_GetAccountCodeEmployee](TeamKey)AS AccountCode ,ROUND(SUM(Amount),0) AS CTYTRICH,0 AS TIENCOMCANTEEN
			FROM [dbo].[SLR_ReportOffice_Close]
			WHERE DateWrite BETWEEN @FromDate and @ToDate AND Parent IS  NOT NULL  
			AND RecordStatus <> 99
			AND (CategoryKey =6  OR CodeID='XHCT' OR CodeID='YTCT' OR CodeID='NVCT' OR CodeID = 'CDCT')
			GROUP BY DepartmentKey,DepartmentID,DepartmentName,TeamKey,TeamID,TeamName,[dbo].[Fn_GetAccountCodeEmployee](TeamKey)
		UNION
			SELECT DepartmentKey,DepartmentID,DepartmentName,TeamKey,TeamID,TeamName,AccountCode,0,Amount AS TIENCOMCANTEEN
			FROM  #Temp
			WHERE CodeID='ZCCT'
		)
		--SELECT * FROM #TAM_TRICHLUONG
		INSERT INTO #Temp
		SELECT DepartmentKey,[dbo].[Fn_GetDepartmentID](DepartmentKey),[dbo].[Fn_GetDepartmentName](DepartmentKey),TeamKey,TeamID,TeamName,
		AccountCode,(CTYTRICH + TIENCOMCANTEEN) AS Amount,
		'ZTTL',N'CÁC KHOẢN TRÍCH THEO LƯƠNG','',100
		FROM #TAM_TRICHLUONG
	

---------------------KQ
SELECT (CAST(A.TeamKey AS NVARCHAR(50)) +'|'+A.TeamName+'|'+A.TeamID+'|'+CAST(A.DepartmentKey AS NVARCHAR(50))+'|'+A.DepartmentName+'|'+AccountCode+'|'+CAST(D.Rank AS NVARCHAR(10))+'|'+CAST(C.Rank AS NVARCHAR(10))) AS LeftColumn,
(A.TypeName+'|'+A.CodeName) AS HeaderColumn, Amount,a.Rank
FROM #Temp A
LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=A.DepartmentKey
LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey 
WHERE 1=1 @Parameter
ORDER BY A.Rank

DROP TABLE #Temp
";
            zSQL = zSQL.Replace("@Parameter", Fillter);
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable BAOCAOLUONGTONGHOPGIANTIEP_CHITIET_NHOMQUANLY(DateTime FromDate, int DepartmentKey, int TeamKey, string Salary)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            string Fillter = "";
            if (Salary.Length > 0)
            {
                Fillter += " AND " + Salary;
            }
            if (DepartmentKey > 0)
            {
                Fillter += " AND A.DepartmentKey = @DepartmentKey";
            }
            if (TeamKey > 0)
            {
                Fillter += " AND A.TeamKey = @TeamKey";
            }
            DataTable zTable = new DataTable();
            string zSQL = @"
--declare @FromDate datetime ='2020-06-01 00:00:00'
--declare @ToDate datetime ='2020-06-30 23:59:59'

CREATE TABLE #Temp(
DepartmentKey nvarchar(10),
DepartmentID nvarchar(50),
DepartmentName nvarchar(500),
TeamKey nvarchar(10),
TeamID nvarchar(50),
TeamName nvarchar(500),
AccountCode nvarchar(50),
Amount money,
CodeID nvarchar(250),
CodeName nvarchar(500),
TypeName  nvarchar(500),
[Rank] INT
)
--Lấy tất cả mã trong bảng lương văn phòng
		INSERT INTO #Temp
		SELECT DepartmentKey_M,DepartmentID_M,DepartmentName_M,TeamKey_M,TeamID_M,TeamName_M,[dbo].[Fn_GetAccountCodeEmployee](TeamKey_M),ROUND(SUM(Amount),0) AS Amount,
		CodeID,CodeName,TypeName,[Rank]
		FROM [dbo].[SLR_ReportOffice_Close]
		WHERE DateWrite BETWEEN @FromDate and @ToDate AND Parent IS  NOT NULL  
		AND RecordStatus <> 99
		GROUP BY DepartmentKey_M,DepartmentID_M,DepartmentName_M,TeamKey_M,TeamID_M,TeamName_M,[dbo].[Fn_GetAccountCodeEmployee](TeamKey_M),CodeID,CodeName,TypeName,[Rank]
--Lấy tiền cơm trưa Cơm C +cơm U trong bảng lương văn phòng
		INSERT INTO #Temp
		SELECT DepartmentKey_M,DepartmentID_M,DepartmentName_M,TeamKey_M,TeamID_M,TeamName_M,[dbo].[Fn_GetAccountCodeEmployee](TeamKey_M),ROUND(SUM(Amount),0) AS Amount,
		'ZCOM',N'TIỀN CƠM TRƯA','',97
		FROM [dbo].[SLR_ReportOffice_Close]
		WHERE DateWrite BETWEEN @FromDate and @ToDate AND Parent IS  NOT NULL  
		AND RecordStatus <> 99
        AND CategoryKey = 6 -- Tiền cơm cộng
		--AND (CodeID='STCC' OR CodeID = 'STCU')
		GROUP BY DepartmentKey_M,DepartmentID_M,DepartmentName_M,TeamKey_M,TeamID_M,TeamName_M,[dbo].[Fn_GetAccountCodeEmployee](TeamKey_M),CodeID,CodeName,TypeName
--Lấy tiền cơm ăn căn teen
		;WITH #TAM_CANTEEN AS (
		(
				--Lấy tiền cơm
				SELECT DepartmentKey,TeamKey,TeamID,TeamName,
				''AS AccountCode,Amount
				FROM(
				SELECT DepartmentKey,TeamKey,TeamID,TeamName, SUM(Money1+Money2+Money3) AS Amount
				FROM [dbo].[FNC_RiceTeamCompany]
				WHERE DateWrite BETWEEN @FromDate and @ToDate 
				AND RecordStatus <> 99 AND BranchKey=2
				GROUP BY DepartmentKey,TeamKey,TeamID,TeamName)X
			)
			UNION  
			(
			--Lấy mã Accoutcode 
			SELECT DepartmentKey_M AS DepartmentKey,TeamKey_M AS TeamKey_M,TeamID_M AS TeamID,TeamName_M AS TeamName,
			[dbo].[Fn_GetAccountCodeEmployee](TeamKey_M) AS AccountCode,0
			FROM [dbo].[SLR_ReportOffice_Close]
			WHERE DateWrite BETWEEN @FromDate and @ToDate AND Parent IS  NOT NULL  
			AND RecordStatus <> 99
			GROUP BY DepartmentKey_M,TeamKey_M,TeamID_M,TeamName_M,[dbo].[Fn_GetAccountCodeEmployee](TeamKey_M)
			)
		)
		--SELECT * FROM #TAM_CANTEEN
		INSERT INTO #Temp
		SELECT DepartmentKey,[dbo].[Fn_GetDepartmentID](DepartmentKey),[dbo].[Fn_GetDepartmentName](DepartmentKey),TeamKey,TeamID,TeamName,
		AccountCode,Amount,
		'ZCCT',N'TIỀN CƠM TRẢ ĐƠN VỊ NẤU','',98
		FROM #TAM_CANTEEN
--Lấy sô lương lao động từ bảng lương văn phòng
		INSERT INTO #Temp
		SELECT DepartmentKey_M,DepartmentID_M,DepartmentName_M,TeamKey_M,TeamID_M,TeamName_M,
		[dbo].[Fn_GetAccountCodeEmployee](TeamKey_M) AS AccountCode, COUNT(DISTINCT(EmployeeKey)) AS Amount,
		'ZNLD',N'SỐ LAO ĐỘNG','',99
		FROM [dbo].[SLR_ReportOffice_Close]
		WHERE DateWrite BETWEEN @FromDate and @ToDate 
		AND RecordStatus <> 99 AND BranchKey=2
		GROUP BY DepartmentKey_M,DepartmentID_M,DepartmentName_M,TeamKey_M,TeamID_M,TeamName_M,[dbo].[Fn_GetAccountCodeEmployee](TeamKey_M)
--Lấy các khoản trích theo lương
		;WITH #TAM_TRICHLUONG AS(
			SELECT DepartmentKey_M AS DepartmentKey,DepartmentID_M AS DepartmentID,DepartmentName_M AS DepartmentName,TeamKey_M AS TeamKey,TeamID_M AS TeamID,TeamName_M AS TeamName,
			[dbo].[Fn_GetAccountCodeEmployee](TeamKey_M)AS AccountCode ,ROUND(SUM(Amount),0) AS CTYTRICH,0 AS TIENCOMCANTEEN
			FROM [dbo].[SLR_ReportOffice_Close]
			WHERE DateWrite BETWEEN @FromDate and @ToDate AND Parent IS  NOT NULL  
			AND RecordStatus <> 99
			AND (CategoryKey = 6 OR CodeID='XHCT' OR CodeID='YTCT' OR CodeID='NVCT'
			OR CodeID = 'CDCT')
			GROUP BY DepartmentKey_M,DepartmentID_M,DepartmentName_M,TeamKey_M,TeamID_M,TeamName_M,[dbo].[Fn_GetAccountCodeEmployee](TeamKey_M)
		UNION
			SELECT DepartmentKey,DepartmentID,DepartmentName,TeamKey,TeamID,TeamName,AccountCode,0,Amount AS TIENCOMCANTEEN
			FROM  #Temp
			WHERE CodeID='ZCCT'
		)
		--SELECT * FROM #TAM_TRICHLUONG
		INSERT INTO #Temp
		SELECT DepartmentKey,[dbo].[Fn_GetDepartmentID](DepartmentKey),[dbo].[Fn_GetDepartmentName](DepartmentKey),TeamKey,TeamID,TeamName,
		AccountCode,(CTYTRICH + TIENCOMCANTEEN) AS Amount,
		'ZTTL',N'CÁC KHOẢN TRÍCH THEO LƯƠNG','',100
		FROM #TAM_TRICHLUONG
	

---------------------KQ
SELECT (CAST(A.TeamKey AS NVARCHAR(50)) +'|'+A.TeamName+'|'+A.TeamID+'|'+CAST(A.DepartmentKey AS NVARCHAR(50))+'|'+A.DepartmentName+'|'+AccountCode+'|'+CAST(D.Rank AS NVARCHAR(10))+'|'+CAST(C.Rank AS NVARCHAR(10))) AS LeftColumn,
(A.TypeName+'|'+A.CodeName) AS HeaderColumn, Amount,a.Rank
FROM #Temp A
LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=A.DepartmentKey
LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey 
WHERE 1=1 @Parameter
ORDER BY A.Rank

DROP TABLE #Temp
";
            zSQL = zSQL.Replace("@Parameter", Fillter);
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        #endregion

        #region[báo cáo tổng hợp lương tháng  -chi tiết hỗ trợ - Anh Nhựt]
        public static DataTable ListCodeSupportFull(DateTime DateWrite)
        {
            var zTime = DateWrite.FirstEndMonth();
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT * FROM(
	SELECT CodeID,(TypeName+' '+CodeName) AS CodeName,Rank  FROM [dbo].[SLR_ReportSupport_Close]
	WHERE RecordStatus <> 99 AND Parent IS NOT NULL 
	AND DateWrite BETWEEN @FromDate AND @ToDate  
	GROUP BY CodeID,(TypeName+' '+CodeName),Rank
	)X
ORDER BY X.[Rank]";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zTime.Item1;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zTime.Item2;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable BAOCAOLUONGTONGHOPHOTRO_CHITIET_NHOMMACDINH(DateTime FromDate, int DepartmentKey, int TeamKey, string Salary)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            string Fillter = "";
            if (Salary.Length > 0)
            {
                Fillter += " AND " + Salary;
            }
            if (DepartmentKey > 0)
            {
                Fillter += " AND A.DepartmentKey = @DepartmentKey";
            }
            if (TeamKey > 0)
            {
                Fillter += " AND A.TeamKey = @TeamKey";
            }
            DataTable zTable = new DataTable();
            string zSQL = @"
--declare @FromDate datetime ='2020-06-01 00:00:00'
--declare @ToDate datetime ='2020-06-30 23:59:59'

CREATE TABLE #Temp(
DepartmentKey nvarchar(10),
DepartmentID nvarchar(50),
DepartmentName nvarchar(500),
TeamKey nvarchar(10),
TeamID nvarchar(50),
TeamName nvarchar(500),
AccountCode nvarchar(50),
Amount money,
CodeID nvarchar(250),
CodeName nvarchar(500),
TypeName  nvarchar(500),
[Rank] INT
)
--Lấy tất cả mã trong bảng lương văn phòng
		INSERT INTO #Temp
		SELECT DepartmentKey,DepartmentID,DepartmentName,TeamKey,TeamID,TeamName,[dbo].[Fn_GetAccountCodeEmployee](TeamKey),ROUND(SUM(Amount),0) AS Amount,
		CodeID,CodeName,TypeName,[Rank]
		FROM [dbo].[SLR_ReportSupport_Close]
		WHERE DateWrite BETWEEN @FromDate and @ToDate AND Parent IS  NOT NULL  
		AND RecordStatus <> 99
		GROUP BY DepartmentKey,DepartmentID,DepartmentName,TeamKey,TeamID,TeamName,[dbo].[Fn_GetAccountCodeEmployee](TeamKey),CodeID,CodeName,TypeName,[Rank]
--Lấy tiền cơm trưa Cơm C +cơm U trong bảng lương văn phòng
		INSERT INTO #Temp
		SELECT DepartmentKey,DepartmentID,DepartmentName,TeamKey,TeamID,TeamName,[dbo].[Fn_GetAccountCodeEmployee](TeamKey),ROUND(SUM(Amount),0) AS Amount,
		'ZCOM',N'TIỀN CƠM TRƯA','',97
		FROM [dbo].[SLR_ReportSupport_Close]
		WHERE DateWrite BETWEEN @FromDate and @ToDate AND Parent IS  NOT NULL  
		AND RecordStatus <> 99
        AND CategoryKey = 6 -- Tiền cơm cộng
		--AND (CodeID='STCC' OR CodeID = 'STCU')
		GROUP BY DepartmentKey,DepartmentID,DepartmentName,TeamKey,TeamID,TeamName,[dbo].[Fn_GetAccountCodeEmployee](TeamKey),CodeID,CodeName,TypeName
--Lấy tiền cơm ăn căn teen
		;WITH #TAM_CANTEEN AS (
		(
				--Lấy tiền cơm
				SELECT DepartmentKey,TeamKey,TeamID,TeamName,
				''AS AccountCode,Amount
				FROM(
				SELECT DepartmentKey,TeamKey,TeamID,TeamName, SUM(Money1+Money2+Money3) AS Amount
				FROM [dbo].[FNC_RiceTeamCompany]
				WHERE DateWrite BETWEEN @FromDate and @ToDate 
				AND RecordStatus <> 99 AND BranchKey=2
				GROUP BY DepartmentKey,TeamKey,TeamID,TeamName)X
			)
			UNION  
			(
			--Lấy mã Accoutcode 
			SELECT DepartmentKey,TeamKey,TeamID,TeamName,
			[dbo].[Fn_GetAccountCodeEmployee](TeamKey) AS AccountCode,0
			FROM [dbo].[SLR_ReportSupport_Close]
			WHERE DateWrite BETWEEN @FromDate and @ToDate AND Parent IS  NOT NULL  
			AND RecordStatus <> 99
			GROUP BY DepartmentKey,TeamKey,TeamID,TeamName,[dbo].[Fn_GetAccountCodeEmployee](TeamKey)
			)
		)
		--SELECT * FROM #TAM_CANTEEN
		INSERT INTO #Temp
		SELECT DepartmentKey,[dbo].[Fn_GetDepartmentID](DepartmentKey),[dbo].[Fn_GetDepartmentName](DepartmentKey),TeamKey,TeamID,TeamName,
		AccountCode,Amount,
		'ZCCT',N'TIỀN CƠM TRẢ ĐƠN VỊ NẤU','',98
		FROM #TAM_CANTEEN
--Lấy sô lương lao động từ bảng lương văn phòng
		INSERT INTO #Temp
		SELECT DepartmentKey,DepartmentID,DepartmentName,TeamKey,TeamID,TeamName,
		[dbo].[Fn_GetAccountCodeEmployee](TeamKey) AS AccountCode, COUNT(DISTINCT(EmployeeKey)) AS Amount,
		'ZNLD',N'SỐ LAO ĐỘNG','',99
		FROM [dbo].[SLR_ReportSupport_Close]
		WHERE DateWrite BETWEEN @FromDate and @ToDate 
		AND RecordStatus <> 99 AND BranchKey=2
		GROUP BY DepartmentKey,DepartmentID,DepartmentName,TeamKey,TeamID,TeamName,[dbo].[Fn_GetAccountCodeEmployee](TeamKey)
--Lấy các khoản trích theo lương
		;WITH #TAM_TRICHLUONG AS(
			SELECT DepartmentKey,DepartmentID,DepartmentName,TeamKey,TeamID,TeamName,
			[dbo].[Fn_GetAccountCodeEmployee](TeamKey)AS AccountCode ,ROUND(SUM(Amount),0) AS CTYTRICH,0 AS TIENCOMCANTEEN
			FROM [dbo].[SLR_ReportSupport_Close]
			WHERE DateWrite BETWEEN @FromDate and @ToDate AND Parent IS  NOT NULL  
			AND RecordStatus <> 99
			AND (CategoryKey = 6 OR CodeID='XHCT' OR CodeID='YTCT' OR CodeID='NVCT' OR CodeID = 'CDCT')
			GROUP BY DepartmentKey,DepartmentID,DepartmentName,TeamKey,TeamID,TeamName,[dbo].[Fn_GetAccountCodeEmployee](TeamKey)
		UNION
			SELECT DepartmentKey,DepartmentID,DepartmentName,TeamKey,TeamID,TeamName,AccountCode,0,Amount AS TIENCOMCANTEEN
			FROM  #Temp
			WHERE CodeID='ZCCT'
		)
		--SELECT * FROM #TAM_TRICHLUONG
		INSERT INTO #Temp
		SELECT DepartmentKey,[dbo].[Fn_GetDepartmentID](DepartmentKey),[dbo].[Fn_GetDepartmentName](DepartmentKey),TeamKey,TeamID,TeamName,
		AccountCode,(CTYTRICH + TIENCOMCANTEEN) AS Amount,
		'ZTTL',N'CÁC KHOẢN TRÍCH THEO LƯƠNG','',100
		FROM #TAM_TRICHLUONG
	

---------------------KQ
SELECT (CAST(A.TeamKey AS NVARCHAR(50)) +'|'+A.TeamName+'|'+A.TeamID+'|'+CAST(A.DepartmentKey AS NVARCHAR(50))+'|'+A.DepartmentName+'|'+AccountCode+'|'+CAST(D.Rank AS NVARCHAR(10))+'|'+CAST(C.Rank AS NVARCHAR(10))) AS LeftColumn,
(A.TypeName+'|'+A.CodeName) AS HeaderColumn, Amount,a.Rank
FROM #Temp A
LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=A.DepartmentKey
LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey 
WHERE 1=1 @Parameter
ORDER BY A.Rank

DROP TABLE #Temp
";
            zSQL = zSQL.Replace("@Parameter", Fillter);
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        #endregion

        #region[báo cáo tổng hợp lương tháng  -chi tiết sản xuất - Anh Nhựt]
        public static DataTable ListCodeWorkerFull(DateTime DateWrite)
        {
            var zTime = DateWrite.FirstEndMonth();
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT * FROM(
	SELECT CodeID,(TypeName+' '+CodeName) AS CodeName,Rank  FROM [dbo].[SLR_ReportWorker_Close]
	WHERE RecordStatus <> 99 AND Parent IS NOT NULL 
	AND DateWrite BETWEEN @FromDate AND @ToDate  
	GROUP BY CodeID,(TypeName+' '+CodeName),Rank
	)X
ORDER BY X.[Rank]";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zTime.Item1;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zTime.Item2;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable BAOCAOLUONGTONGHOPSANXUAT_CHITIET_NHOMMACDINH(DateTime FromDate, int DepartmentKey, int TeamKey, string Salary)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            string Fillter = "";
            if (Salary.Length > 0)
            {
                Fillter += " AND " + Salary;
            }
            if (DepartmentKey > 0)
            {
                Fillter += " AND A.DepartmentKey = @DepartmentKey";
            }
            if (TeamKey > 0)
            {
                Fillter += " AND A.TeamKey = @TeamKey";
            }
            DataTable zTable = new DataTable();
            string zSQL = @"
--declare @FromDate datetime ='2020-06-01 00:00:00'
--declare @ToDate datetime ='2020-06-30 23:59:59'

CREATE TABLE #Temp(
DepartmentKey nvarchar(10),
DepartmentID nvarchar(50),
DepartmentName nvarchar(500),
TeamKey nvarchar(10),
TeamID nvarchar(50),
TeamName nvarchar(500),
AccountCode nvarchar(50),
Amount money,
CodeID nvarchar(250),
CodeName nvarchar(500),
TypeName  nvarchar(500),
[Rank] INT
)
--Lấy tất cả mã trong bảng lương văn phòng
		INSERT INTO #Temp
		SELECT DepartmentKey,DepartmentID,DepartmentName,TeamKey,TeamID,TeamName,[dbo].[Fn_GetAccountCodeEmployee](TeamKey),ROUND(SUM(Amount),0) AS Amount,
		CodeID,CodeName,TypeName,[Rank]
		FROM [dbo].[SLR_ReportWorker_Close]
		WHERE DateWrite BETWEEN @FromDate and @ToDate AND Parent IS  NOT NULL  
		AND RecordStatus <> 99
		GROUP BY DepartmentKey,DepartmentID,DepartmentName,TeamKey,TeamID,TeamName,[dbo].[Fn_GetAccountCodeEmployee](TeamKey),CodeID,CodeName,TypeName,[Rank]
--Lấy tiền cơm trưa Cơm C +cơm U trong bảng lương văn phòng
		INSERT INTO #Temp
		SELECT DepartmentKey,DepartmentID,DepartmentName,TeamKey,TeamID,TeamName,[dbo].[Fn_GetAccountCodeEmployee](TeamKey),ROUND(SUM(Amount),0) AS Amount,
		'ZCOM',N'TIỀN CƠM TRƯA','',97
		FROM [dbo].[SLR_ReportWorker_Close]
		WHERE DateWrite BETWEEN @FromDate and @ToDate AND Parent IS  NOT NULL  
		AND RecordStatus <> 99
        AND CategoryKey = 6 -- Tiền cơm cộng
		--AND (CodeID='STCC' OR CodeID = 'STCU')
		GROUP BY DepartmentKey,DepartmentID,DepartmentName,TeamKey,TeamID,TeamName,[dbo].[Fn_GetAccountCodeEmployee](TeamKey),CodeID,CodeName,TypeName
--Lấy tiền cơm ăn căn teen
		;WITH #TAM_CANTEEN AS (
		(
				--Lấy tiền cơm
				SELECT DepartmentKey,TeamKey,TeamID,TeamName,
				0 AS AccountCode,Amount
				FROM(
				SELECT DepartmentKey,TeamKey,TeamID,TeamName, SUM(Money1+Money2+Money3) AS Amount
				FROM [dbo].[FNC_RiceTeamCompany]
				WHERE DateWrite BETWEEN @FromDate and @ToDate 
				AND RecordStatus <> 99 AND BranchKey=4
				GROUP BY DepartmentKey,TeamKey,TeamID,TeamName)X
			)
			UNION  
			(
			--Lấy mã Accoutcode 
			SELECT DepartmentKey ,TeamKey,TeamID ,TeamName,
			[dbo].[Fn_GetAccountCodeEmployee](TeamKey) AS AccountCode,0
			FROM [dbo].[SLR_ReportWorker_Close]
			WHERE DateWrite BETWEEN @FromDate and @ToDate AND Parent IS  NOT NULL  
			AND RecordStatus <> 99
			GROUP BY DepartmentKey,TeamKey,TeamID,TeamName,[dbo].[Fn_GetAccountCodeEmployee](TeamKey)
			)
		)
		--SELECT * FROM #TAM_CANTEEN
		INSERT INTO #Temp
		SELECT DepartmentKey,[dbo].[Fn_GetDepartmentID](DepartmentKey),[dbo].[Fn_GetDepartmentName](DepartmentKey),TeamKey,TeamID,TeamName,
		SUM(AccountCode),SUM(Amount),
		'ZCCT',N'TIỀN CƠM TRẢ ĐƠN VỊ NẤU','',98
		FROM #TAM_CANTEEN
        GROUP BY DepartmentKey,[dbo].[Fn_GetDepartmentID](DepartmentKey),[dbo].[Fn_GetDepartmentName](DepartmentKey),TeamKey,TeamID,TeamName
--Lấy sô lương lao động từ bảng lương văn phòng
		INSERT INTO #Temp
		SELECT DepartmentKey,DepartmentID,DepartmentName,TeamKey,TeamID,TeamName,
		[dbo].[Fn_GetAccountCodeEmployee](TeamKey) AS AccountCode, COUNT(DISTINCT(EmployeeKey)) AS Amount,
		'ZNLD',N'SỐ LAO ĐỘNG','',99
		FROM [dbo].[SLR_ReportWorker_Close]
		WHERE DateWrite BETWEEN @FromDate and @ToDate 
		AND RecordStatus <> 99 AND BranchKey=2
		GROUP BY DepartmentKey,DepartmentID,DepartmentName,TeamKey,TeamID,TeamName,[dbo].[Fn_GetAccountCodeEmployee](TeamKey)
--Lấy các khoản trích theo lương
		;WITH #TAM_TRICHLUONG AS(
			SELECT DepartmentKey,DepartmentID,DepartmentName,TeamKey,TeamID,TeamName,
			[dbo].[Fn_GetAccountCodeEmployee](TeamKey)AS AccountCode ,ROUND(SUM(Amount),0) AS CTYTRICH,0 AS TIENCOMCANTEEN
			FROM [dbo].[SLR_ReportWorker_Close]
			WHERE DateWrite BETWEEN @FromDate and @ToDate AND Parent IS  NOT NULL  
			AND RecordStatus <> 99
			AND ( CategoryKey = 6 OR CodeID='XHCT' OR CodeID='YTCT' OR CodeID='NVCT' OR CodeID = 'CDCT')
			GROUP BY DepartmentKey,DepartmentID,DepartmentName,TeamKey,TeamID,TeamName,[dbo].[Fn_GetAccountCodeEmployee](TeamKey)
		UNION
			SELECT DepartmentKey,DepartmentID,DepartmentName,TeamKey,TeamID,TeamName,AccountCode,0,Amount AS TIENCOMCANTEEN
			FROM  #Temp
			WHERE CodeID='ZCCT'
		)
		--SELECT * FROM #TAM_TRICHLUONG
		INSERT INTO #Temp
		SELECT DepartmentKey,[dbo].[Fn_GetDepartmentID](DepartmentKey),[dbo].[Fn_GetDepartmentName](DepartmentKey),TeamKey,TeamID,TeamName,
		AccountCode,(CTYTRICH + TIENCOMCANTEEN) AS Amount,
		'ZTTL',N'CÁC KHOẢN TRÍCH THEO LƯƠNG','',100
		FROM #TAM_TRICHLUONG
	

---------------------KQ
SELECT (CAST(A.TeamKey AS NVARCHAR(50)) +'|'+A.TeamName+'|'+A.TeamID+'|'+CAST(A.DepartmentKey AS NVARCHAR(50))+'|'+A.DepartmentName+'|'+AccountCode+'|'+CAST(D.Rank AS NVARCHAR(10))+'|'+CAST(C.Rank AS NVARCHAR(10))) AS LeftColumn,
(A.TypeName+'|'+A.CodeName) AS HeaderColumn, Amount,a.Rank
FROM #Temp A
LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=A.DepartmentKey
LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey 
WHERE 1=1 @Parameter
ORDER BY A.Rank

DROP TABLE #Temp
";
            zSQL = zSQL.Replace("@Parameter", Fillter);
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        #endregion

        #region[báo cáo tổng hợp lương theo tháng  -bộ phận sản xuất]
        public static DataTable ListCodeWorker(DateTime FromDate, DateTime ToDate)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);

            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, 1, 0, 0, 0);
            zToDate = zToDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT * FROM(
	SELECT CodeID,(TypeName+' '+CodeName) AS CodeName,Rank  FROM [dbo].[SLR_ReportWorker_Close]
	WHERE RecordStatus <> 99 AND Parent IS NOT NULL 
	AND DateWrite BETWEEN @FromDate AND @ToDate  AND Publish_Close =1 
	GROUP BY CodeID,(TypeName+' '+CodeName),Rank
	)X
ORDER BY X.[Rank]";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        //Xem theo nhân sự
        public static DataTable THUNHAPTHEOTHANG_BOPHANSANXUAT_NHANSU(DateTime FromDate, DateTime ToDate, int DepartmentKey, int TeamKey,string EmployeeID, string Salary)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);

            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, 1, 0, 0, 0);
            zToDate = zToDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            string Fillter = "";
            if (Salary.Length > 0)
            {
                Fillter += " AND " + Salary;
            }
            if (DepartmentKey > 0)
            {
                Fillter += " AND A.DepartmentKey = @DepartmentKey";
            }
            if (TeamKey > 0)
            {
                Fillter += " AND A.TeamKey = @TeamKey";
            }
            if (EmployeeID.Trim().Length > 0)
            {
                Fillter += " AND [dbo].[Fn_GetEmployeeID](A.EmployeeKey) = @EmployeeID";
            }

            DataTable zTable = new DataTable();
            string zSQL = @"
--declare @FromDate datetime ='2021-01-01 00:00:00'
--declare @ToDate datetime ='2021-12-31 23:59:59'

CREATE TABLE #Temp(
DateView nvarchar(50),
DepartmentKey nvarchar(10),
DepartmentID nvarchar(50),
DepartmentName nvarchar(500),
TeamKey nvarchar(10),
TeamID nvarchar(50),
TeamName nvarchar(500),
EmployeeKey nvarchar(50),
EmployeeID nvarchar(50),
EmployeeName nvarchar(500),
Amount money,
CodeID nvarchar(250),
CodeName nvarchar(500),
TypeName  nvarchar(500),
[Rank] INT
)
        INSERT INTO #Temp
        SELECT DateWrite, DepartmentKey,DepartmentID,DepartmentName,TeamKey,TeamID,TeamName,EmployeeKey,EmployeeID,EmployeeName,ROUND(SUM(Amount),0) AS Amount,
        CodeID,CodeName,TypeName,[Rank]
        FROM [dbo].[SLR_ReportWorker_Close]
        WHERE DateWrite BETWEEN @FromDate and @ToDate AND Parent IS  NOT NULL  
        AND RecordStatus <> 99
        GROUP BY DateWrite, DepartmentKey,DepartmentID,DepartmentName,TeamKey,TeamID,TeamName,EmployeeKey,EmployeeID,EmployeeName,CodeID,CodeName,TypeName,[Rank]

        ;WITH K AS 
                (
                SELECT TypeName,CodeName,CodeID,CONCAT(MONTH([DateView]),'/',YEAR([DateView])) AS [DATE],
                CAST(TeamKey AS NVARCHAR(10)) AS TeamKey,
                DepartmentKey,
				EmployeeKey,
                CAST(Amount AS money)AS Amount,Rank
                FROM #Temp 
	                UNION ALL
	                SELECT
	                TypeName,
			        CodeName,
					CodeID,
	                '99/9999'[DATE],
	                TeamKey,
                    DepartmentKey,
					EmployeeKey,
	                SUM(Amount) AS Amount,
					Rank
	                FROM #Temp 
	                GROUP BY TypeName,CodeName,CodeID,DepartmentKey,TeamKey,EmployeeKey,Rank
                ) 

SELECT LeftColumn,HeaderColumn,Amount FROM(
	        SELECT ISNULL(A.TypeName,'')+' '+A.CodeName +'|'+CAST(A.Rank AS NVARCHAR(50)) AS LeftColumn,[DATE]+'|'+CAST(A.EmployeeKey AS NVARCHAR(10))+'|'+[dbo].[Fn_GetEmployeeID](A.EmployeeKey)+' - ' +[dbo].[Fn_GetFullName](A.EmployeeKey) AS HeaderColumn,Amount,[DATE],
	        D.Rank AS BranchRank,C.Rank AS DepartmentRank,B.Rank AS TeamRank,[dbo].[Fn_GetEmployeeID](A.EmployeeKey) AS EmployeeID
	        FROM K A
			LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
	        LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
	        LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
            WHERE 1=1 @Parameter
         ) X
         ORDER BY  LEN([DATE]),[DATE],BranchRank,DepartmentRank,TeamRank,LEN(EmployeeID),EmployeeID

DROP TABLE #Temp
";
            zSQL = zSQL.Replace("@Parameter", Fillter);
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = EmployeeID.Trim();
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        //Xem theo nhóm
        public static DataTable THUNHAPTHEOTHANG_BOPHANSANXUAT_NHOM(DateTime FromDate,DateTime ToDate, int DepartmentKey, int TeamKey,  string Salary )
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);

            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, 1, 0, 0, 0);
            zToDate = zToDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            string Fillter = "";
            if (Salary.Length > 0)
            {
                Fillter += " AND " + Salary;
            }
            if (DepartmentKey > 0)
            {
                Fillter += " AND A.DepartmentKey = @DepartmentKey";
            }
            if (TeamKey > 0)
            {
                Fillter += " AND A.TeamKey = @TeamKey";
            }
           
            DataTable zTable = new DataTable();
            string zSQL = @"
--declare @FromDate datetime ='2020-01-01 00:00:00'
--declare @ToDate datetime ='2020-12-31 23:59:59'

CREATE TABLE #Temp(
DateView nvarchar(50),
DepartmentKey nvarchar(10),
DepartmentID nvarchar(50),
DepartmentName nvarchar(500),
TeamKey nvarchar(10),
TeamID nvarchar(50),
TeamName nvarchar(500),
Amount money,
CodeID nvarchar(250),
CodeName nvarchar(500),
TypeName  nvarchar(500),
[Rank] INT
)
          INSERT INTO #Temp
        SELECT DateWrite, DepartmentKey,DepartmentID,DepartmentName,TeamKey,TeamID,TeamName,ROUND(SUM(Amount),0) AS Amount,
        CodeID,CodeName,TypeName,[Rank]
        FROM [dbo].[SLR_ReportWorker_Close]
        WHERE DateWrite BETWEEN @FromDate and @ToDate AND Parent IS  NOT NULL  
        AND RecordStatus <> 99
        GROUP BY DateWrite, DepartmentKey,DepartmentID,DepartmentName,TeamKey,TeamID,TeamName,CodeID,CodeName,TypeName,[Rank]

        ;WITH K AS 
                (
                SELECT TypeName,CodeName,CodeID,CONCAT(MONTH([DateView]),'/',YEAR([DateView])) AS [DATE],
                CAST(TeamKey AS NVARCHAR(10)) AS TeamKey,
                DepartmentKey,
                CAST(Amount AS money)AS Amount,Rank
                FROM #Temp 
	                UNION ALL
	                SELECT
	                TypeName,
			        CodeName,
					CodeID,
	                '99/9999'[DATE],
	                TeamKey,
                    DepartmentKey,
	                SUM(Amount) AS Amount,
					Rank
	                FROM #Temp 
	                GROUP BY TypeName,CodeName,CodeID,DepartmentKey,TeamKey,Rank
                ) 

SELECT LeftColumn,HeaderColumn,Amount FROM(
	        SELECT ISNULL(A.TypeName,'')+' '+A.CodeName +'|'+CAST(A.Rank AS NVARCHAR(50)) AS LeftColumn,[DATE]+'|'+CAST(A.TeamKey AS NVARCHAR(10))+'|'+[dbo].[Fn_GetTeamName](A.TeamKey) AS HeaderColumn,Amount,[DATE],
	        D.Rank AS BranchRank,C.Rank AS DepartmentRank,B.Rank AS TeamRank
	        FROM K A
			LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
	        LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
	        LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
            WHERE 1=1 @Parameter
         ) X
         ORDER BY  LEN([DATE]),[DATE],BranchRank,DepartmentRank,TeamRank

DROP TABLE #Temp
";
            zSQL = zSQL.Replace("@Parameter", Fillter);
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        //Xem theo Bộ phận
        public static DataTable THUNHAPTHEOTHANG_BOPHANSANXUAT_BOPHAN(DateTime FromDate, DateTime ToDate, int DepartmentKey, string Salary)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);

            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, 1, 0, 0, 0);
            zToDate = zToDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            string Fillter = "";
            if (Salary.Length > 0)
            {
                Fillter += " AND " + Salary;
            }
            if (DepartmentKey > 0)
            {
                Fillter += " AND A.DepartmentKey = @DepartmentKey";
            }
           
            DataTable zTable = new DataTable();
            string zSQL = @"
--declare @FromDate datetime ='2020-06-01 00:00:00'
--declare @ToDate datetime ='2020-12-31 23:59:59'

CREATE TABLE #Temp(
DateView nvarchar(50),
DepartmentKey nvarchar(10),
DepartmentID nvarchar(50),
DepartmentName nvarchar(500),
Amount money,
CodeID nvarchar(250),
CodeName nvarchar(500),
TypeName  nvarchar(500),
[Rank] INT
)
        INSERT INTO #Temp
        SELECT DateWrite, DepartmentKey,DepartmentID,DepartmentName,ROUND(SUM(Amount),0) AS Amount,
        CodeID,CodeName,TypeName,[Rank]
        FROM [dbo].[SLR_ReportWorker_Close]
        WHERE DateWrite BETWEEN @FromDate and @ToDate AND Parent IS  NOT NULL  
        AND RecordStatus <> 99
        GROUP BY DateWrite, DepartmentKey,DepartmentID,DepartmentName,CodeID,CodeName,TypeName,[Rank]

        ;WITH K AS 
                (
                SELECT TypeName,CodeName,CodeID,CONCAT(MONTH([DateView]),'/',YEAR([DateView])) AS [DATE],
                CAST(DepartmentKey AS NVARCHAR(10)) AS DepartmentKey,
                CAST(Amount AS money)AS Amount,Rank
                FROM #Temp 
	                UNION ALL
	                SELECT
	                TypeName,
			        CodeName,
					CodeID,
	                '99/9999'[DATE],
	                DepartmentKey,
	                SUM(Amount) AS Amount,
					Rank
	                FROM #Temp 
	                GROUP BY TypeName,CodeName,CodeID,DepartmentKey,Rank
                ) 

SELECT LeftColumn,HeaderColumn,Amount FROM(
	        SELECT ISNULL(A.TypeName,'')+' '+A.CodeName +'|'+CAST(A.Rank AS NVARCHAR(50)) AS LeftColumn,[DATE]+'|'+CAST(A.DepartmentKey AS NVARCHAR(10))+'|'+[dbo].[Fn_GetDepartmentName](A.DepartmentKey) AS HeaderColumn,Amount,[DATE],
	        D.Rank AS BranchRank,C.Rank AS DepartmentRank
	        FROM K A
	        LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=A.DepartmentKey
	        LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
            WHERE 1=1 @Parameter
         ) X
         ORDER BY  LEN([DATE]),[DATE],BranchRank,DepartmentRank

DROP TABLE #Temp
";
            zSQL = zSQL.Replace("@Parameter", Fillter);
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        #endregion

        #region[báo cáo tổng hợp lương theo tháng  -bộ phận hỗ trợ]
        public static DataTable ListCodeSupport(DateTime FromDate, DateTime ToDate)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);

            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, 1, 0, 0, 0);
            zToDate = zToDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT * FROM(
	SELECT CodeID,(TypeName+' '+CodeName) AS CodeName,Rank  FROM [dbo].[SLR_ReportSupport_Close]
	WHERE RecordStatus <> 99 AND Parent IS NOT NULL 
	AND DateWrite BETWEEN @FromDate AND @ToDate   AND Publish_Close =1 
	GROUP BY CodeID,(TypeName+' '+CodeName),Rank
	)X
ORDER BY X.[Rank]";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        //Xem theo nhân sự
        public static DataTable THUNHAPTHEOTHANG_BOPHANHOTRO_NHANSU(DateTime FromDate, DateTime ToDate, int DepartmentKey, int TeamKey, string EmployeeID, string Salary)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);

            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, 1, 0, 0, 0);
            zToDate = zToDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            string Fillter = "";
            if (Salary.Length > 0)
            {
                Fillter += " AND " + Salary;
            }
            if (DepartmentKey > 0)
            {
                Fillter += " AND A.DepartmentKey = @DepartmentKey";
            }
            if (TeamKey > 0)
            {
                Fillter += " AND A.TeamKey = @TeamKey";
            }
            if (EmployeeID.Trim().Length > 0)
            {
                Fillter += " AND [dbo].[Fn_GetEmployeeID](A.EmployeeKey) = @EmployeeID";
            }

            DataTable zTable = new DataTable();
            string zSQL = @"
--declare @FromDate datetime ='2021-01-01 00:00:00'
--declare @ToDate datetime ='2021-12-31 23:59:59'

CREATE TABLE #Temp(
DateView nvarchar(50),
DepartmentKey nvarchar(10),
DepartmentID nvarchar(50),
DepartmentName nvarchar(500),
TeamKey nvarchar(10),
TeamID nvarchar(50),
TeamName nvarchar(500),
EmployeeKey nvarchar(50),
EmployeeID nvarchar(50),
EmployeeName nvarchar(500),
Amount money,
CodeID nvarchar(250),
CodeName nvarchar(500),
TypeName  nvarchar(500),
[Rank] INT
)
        INSERT INTO #Temp
        SELECT DateWrite, DepartmentKey,DepartmentID,DepartmentName,TeamKey,TeamID,TeamName,EmployeeKey,EmployeeID,EmployeeName,ROUND(SUM(Amount),0) AS Amount,
        CodeID,CodeName,TypeName,[Rank]
        FROM [dbo].[SLR_ReportSupport_Close]
        WHERE DateWrite BETWEEN @FromDate and @ToDate AND Parent IS  NOT NULL  
        AND RecordStatus <> 99
        GROUP BY DateWrite, DepartmentKey,DepartmentID,DepartmentName,TeamKey,TeamID,TeamName,EmployeeKey,EmployeeID,EmployeeName,CodeID,CodeName,TypeName,[Rank]

        ;WITH K AS 
                (
                SELECT TypeName,CodeName,CodeID,CONCAT(MONTH([DateView]),'/',YEAR([DateView])) AS [DATE],
                CAST(TeamKey AS NVARCHAR(10)) AS TeamKey,
                DepartmentKey,
				EmployeeKey,
                CAST(Amount AS money)AS Amount,Rank
                FROM #Temp 
	                UNION ALL
	                SELECT
	                TypeName,
			        CodeName,
					CodeID,
	                '99/9999'[DATE],
	                TeamKey,
                    DepartmentKey,
					EmployeeKey,
	                SUM(Amount) AS Amount,
					Rank
	                FROM #Temp 
	                GROUP BY TypeName,CodeName,CodeID,DepartmentKey,TeamKey,EmployeeKey,Rank
                ) 

SELECT LeftColumn,HeaderColumn,Amount FROM(
	        SELECT ISNULL(A.TypeName,'')+' '+A.CodeName +'|'+CAST(A.Rank AS NVARCHAR(50)) AS LeftColumn,[DATE]+'|'+CAST(A.EmployeeKey AS NVARCHAR(10))+'|'+[dbo].[Fn_GetEmployeeID](A.EmployeeKey)+' - ' +[dbo].[Fn_GetFullName](A.EmployeeKey) AS HeaderColumn,Amount,[DATE],
	        D.Rank AS BranchRank,C.Rank AS DepartmentRank,B.Rank AS TeamRank,[dbo].[Fn_GetEmployeeID](A.EmployeeKey) AS EmployeeID
	        FROM K A
			LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
	        LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
	        LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
            WHERE 1=1 @Parameter
         ) X
         ORDER BY  LEN([DATE]),[DATE],BranchRank,DepartmentRank,TeamRank,LEN(EmployeeID),EmployeeID

DROP TABLE #Temp
";
            zSQL = zSQL.Replace("@Parameter", Fillter);
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = EmployeeID.Trim();
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        //Xem theo nhóm
        public static DataTable THUNHAPTHEOTHANG_BOPHANHOTRO_NHOM(DateTime FromDate, DateTime ToDate, int DepartmentKey, int TeamKey, string Salary)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);

            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, 1, 0, 0, 0);
            zToDate = zToDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            string Fillter = "";
            if (Salary.Length > 0)
            {
                Fillter += " AND " + Salary;
            }
            if (DepartmentKey > 0)
            {
                Fillter += " AND A.DepartmentKey = @DepartmentKey";
            }
            if (TeamKey > 0)
            {
                Fillter += " AND A.TeamKey = @TeamKey";
            }

            DataTable zTable = new DataTable();
            string zSQL = @"
--declare @FromDate datetime ='2020-01-01 00:00:00'
--declare @ToDate datetime ='2020-12-31 23:59:59'

CREATE TABLE #Temp(
DateView nvarchar(50),
DepartmentKey nvarchar(10),
DepartmentID nvarchar(50),
DepartmentName nvarchar(500),
TeamKey nvarchar(10),
TeamID nvarchar(50),
TeamName nvarchar(500),
Amount money,
CodeID nvarchar(250),
CodeName nvarchar(500),
TypeName  nvarchar(500),
[Rank] INT
)
          INSERT INTO #Temp
        SELECT DateWrite, DepartmentKey,DepartmentID,DepartmentName,TeamKey,TeamID,TeamName,ROUND(SUM(Amount),0) AS Amount,
        CodeID,CodeName,TypeName,[Rank]
        FROM [dbo].[SLR_ReportSupport_Close]
        WHERE DateWrite BETWEEN @FromDate and @ToDate AND Parent IS  NOT NULL  
        AND RecordStatus <> 99
        GROUP BY DateWrite, DepartmentKey,DepartmentID,DepartmentName,TeamKey,TeamID,TeamName,CodeID,CodeName,TypeName,[Rank]

        ;WITH K AS 
                (
                SELECT TypeName,CodeName,CodeID,CONCAT(MONTH([DateView]),'/',YEAR([DateView])) AS [DATE],
                CAST(TeamKey AS NVARCHAR(10)) AS TeamKey,
                DepartmentKey,
                CAST(Amount AS money)AS Amount,Rank
                FROM #Temp 
	                UNION ALL
	                SELECT
	                TypeName,
			        CodeName,
					CodeID,
	                '99/9999'[DATE],
	                TeamKey,
                    DepartmentKey,
	                SUM(Amount) AS Amount,
					Rank
	                FROM #Temp 
	                GROUP BY TypeName,CodeName,CodeID,DepartmentKey,TeamKey,Rank
                ) 

SELECT LeftColumn,HeaderColumn,Amount FROM(
	        SELECT ISNULL(A.TypeName,'')+' '+A.CodeName +'|'+CAST(A.Rank AS NVARCHAR(50)) AS LeftColumn,[DATE]+'|'+CAST(A.TeamKey AS NVARCHAR(10))+'|'+[dbo].[Fn_GetTeamName](A.TeamKey) AS HeaderColumn,Amount,[DATE],
	        D.Rank AS BranchRank,C.Rank AS DepartmentRank,B.Rank AS TeamRank
	        FROM K A
			LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
	        LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
	        LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
            WHERE 1=1 @Parameter
         ) X
         ORDER BY  LEN([DATE]),[DATE],BranchRank,DepartmentRank,TeamRank

DROP TABLE #Temp
";
            zSQL = zSQL.Replace("@Parameter", Fillter);
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        //Xem theo Bộ phận
        public static DataTable THUNHAPTHEOTHANG_BOPHANHOTRO_BOPHAN(DateTime FromDate, DateTime ToDate, int DepartmentKey, string Salary)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);

            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, 1, 0, 0, 0);
            zToDate = zToDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            string Fillter = "";
            if (Salary.Length > 0)
            {
                Fillter += " AND " + Salary;
            }
            if (DepartmentKey > 0)
            {
                Fillter += " AND A.DepartmentKey = @DepartmentKey";
            }

            DataTable zTable = new DataTable();
            string zSQL = @"
--declare @FromDate datetime ='2020-06-01 00:00:00'
--declare @ToDate datetime ='2020-12-31 23:59:59'

CREATE TABLE #Temp(
DateView nvarchar(50),
DepartmentKey nvarchar(10),
DepartmentID nvarchar(50),
DepartmentName nvarchar(500),
Amount money,
CodeID nvarchar(250),
CodeName nvarchar(500),
TypeName  nvarchar(500),
[Rank] INT
)
        INSERT INTO #Temp
        SELECT DateWrite, DepartmentKey,DepartmentID,DepartmentName,ROUND(SUM(Amount),0) AS Amount,
        CodeID,CodeName,TypeName,[Rank]
        FROM [dbo].[SLR_ReportSupport_Close]
        WHERE DateWrite BETWEEN @FromDate and @ToDate AND Parent IS  NOT NULL  
        AND RecordStatus <> 99
        GROUP BY DateWrite, DepartmentKey,DepartmentID,DepartmentName,CodeID,CodeName,TypeName,[Rank]

        ;WITH K AS 
                (
                SELECT TypeName,CodeName,CodeID,CONCAT(MONTH([DateView]),'/',YEAR([DateView])) AS [DATE],
                CAST(DepartmentKey AS NVARCHAR(10)) AS DepartmentKey,
                CAST(Amount AS money)AS Amount,Rank
                FROM #Temp 
	                UNION ALL
	                SELECT
	                TypeName,
			        CodeName,
					CodeID,
	                '99/9999'[DATE],
	                DepartmentKey,
	                SUM(Amount) AS Amount,
					Rank
	                FROM #Temp 
	                GROUP BY TypeName,CodeName,CodeID,DepartmentKey,Rank
                ) 

SELECT LeftColumn,HeaderColumn,Amount FROM(
	        SELECT ISNULL(A.TypeName,'')+' '+A.CodeName +'|'+CAST(A.Rank AS NVARCHAR(50)) AS LeftColumn,[DATE]+'|'+CAST(A.DepartmentKey AS NVARCHAR(10))+'|'+[dbo].[Fn_GetDepartmentName](A.DepartmentKey) AS HeaderColumn,Amount,[DATE],
	        D.Rank AS BranchRank,C.Rank AS DepartmentRank
	        FROM K A
	        LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=A.DepartmentKey
	        LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
            WHERE 1=1 @Parameter
         ) X
         ORDER BY  LEN([DATE]),[DATE],BranchRank,DepartmentRank

DROP TABLE #Temp
";
            zSQL = zSQL.Replace("@Parameter", Fillter);
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }

        #endregion

        #region[báo cáo tổng hợp lương theo tháng  -bộ phận gián tiếp]
        public static DataTable ListCodeOffice(DateTime FromDate ,DateTime ToDate)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);

            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, 1, 0, 0, 0);
            zToDate = zToDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);

            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT * FROM(
	SELECT CodeID,(TypeName+' '+CodeName) AS CodeName,Rank  FROM [dbo].[SLR_ReportOffice_Close]
	WHERE RecordStatus <> 99 AND Parent IS NOT NULL 
	AND DateWrite BETWEEN @FromDate AND @ToDate   AND Publish_Close =1
	GROUP BY CodeID,(TypeName+' '+CodeName),Rank
	)X
ORDER BY X.[Rank]";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        //Xem theo nhân sự
        public static DataTable THUNHAPTHEOTHANG_BOPHANGIANTIEP_NHANSU(DateTime FromDate, DateTime ToDate, int DepartmentKey, int TeamKey, string EmployeeID, string Salary)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);

            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, 1, 0, 0, 0);
            zToDate = zToDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            string Fillter = "";
            if (Salary.Length > 0)
            {
                Fillter += " AND " + Salary;
            }
            if (DepartmentKey > 0)
            {
                Fillter += " AND A.DepartmentKey = @DepartmentKey";
            }
            if (TeamKey > 0)
            {
                Fillter += " AND A.TeamKey = @TeamKey";
            }
            if (EmployeeID.Trim().Length > 0)
            {
                Fillter += " AND [dbo].[Fn_GetEmployeeID](A.EmployeeKey) = @EmployeeID";
            }

            DataTable zTable = new DataTable();
            string zSQL = @"
--declare @FromDate datetime ='2021-01-01 00:00:00'
--declare @ToDate datetime ='2021-12-31 23:59:59'

CREATE TABLE #Temp(
DateView nvarchar(50),
DepartmentKey nvarchar(10),
DepartmentID nvarchar(50),
DepartmentName nvarchar(500),
TeamKey nvarchar(10),
TeamID nvarchar(50),
TeamName nvarchar(500),
EmployeeKey nvarchar(50),
EmployeeID nvarchar(50),
EmployeeName nvarchar(500),
Amount money,
CodeID nvarchar(250),
CodeName nvarchar(500),
TypeName  nvarchar(500),
[Rank] INT
)
        INSERT INTO #Temp
        SELECT DateWrite, DepartmentKey,DepartmentID,DepartmentName,TeamKey,TeamID,TeamName,EmployeeKey,EmployeeID,EmployeeName,ROUND(SUM(Amount),0) AS Amount,
        CodeID,CodeName,TypeName,[Rank]
        FROM [dbo].[SLR_ReportOffice_Close]
        WHERE DateWrite BETWEEN @FromDate and @ToDate AND Parent IS  NOT NULL  
        AND RecordStatus <> 99
        GROUP BY DateWrite, DepartmentKey,DepartmentID,DepartmentName,TeamKey,TeamID,TeamName,EmployeeKey,EmployeeID,EmployeeName,CodeID,CodeName,TypeName,[Rank]

        ;WITH K AS 
                (
                SELECT TypeName,CodeName,CodeID,CONCAT(MONTH([DateView]),'/',YEAR([DateView])) AS [DATE],
                CAST(TeamKey AS NVARCHAR(10)) AS TeamKey,
                DepartmentKey,
				EmployeeKey,
                CAST(Amount AS money)AS Amount,Rank
                FROM #Temp 
	                UNION ALL
	                SELECT
	                TypeName,
			        CodeName,
					CodeID,
	                '99/9999'[DATE],
	                TeamKey,
                    DepartmentKey,
					EmployeeKey,
	                SUM(Amount) AS Amount,
					Rank
	                FROM #Temp 
	                GROUP BY TypeName,CodeName,CodeID,DepartmentKey,TeamKey,EmployeeKey,Rank
                ) 

SELECT LeftColumn,HeaderColumn,Amount FROM(
	        SELECT ISNULL(A.TypeName,'')+' '+A.CodeName +'|'+CAST(A.Rank AS NVARCHAR(50)) AS LeftColumn,[DATE]+'|'+CAST(A.EmployeeKey AS NVARCHAR(10))+'|'+[dbo].[Fn_GetEmployeeID](A.EmployeeKey)+' - ' +[dbo].[Fn_GetFullName](A.EmployeeKey) AS HeaderColumn,Amount,[DATE],
	        D.Rank AS BranchRank,C.Rank AS DepartmentRank,B.Rank AS TeamRank,[dbo].[Fn_GetEmployeeID](A.EmployeeKey) AS EmployeeID
	        FROM K A
			LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
	        LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
	        LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
            WHERE 1=1 @Parameter
         ) X
         ORDER BY  LEN([DATE]),[DATE],BranchRank,DepartmentRank,TeamRank,LEN(EmployeeID),EmployeeID

DROP TABLE #Temp
";
            zSQL = zSQL.Replace("@Parameter", Fillter);
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = EmployeeID.Trim();
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        //Xem theo nhóm
        public static DataTable THUNHAPTHEOTHANG_BOPHANGIANTIEP_NHOM(DateTime FromDate, DateTime ToDate, int DepartmentKey, int TeamKey, string Salary)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);

            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, 1, 0, 0, 0);
            zToDate = zToDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            string Fillter = "";
            if (Salary.Length > 0)
            {
                Fillter += " AND " + Salary;
            }
            if (DepartmentKey > 0)
            {
                Fillter += " AND A.DepartmentKey = @DepartmentKey";
            }
            if (TeamKey > 0)
            {
                Fillter += " AND A.TeamKey = @TeamKey";
            }

            DataTable zTable = new DataTable();
            string zSQL = @"
--declare @FromDate datetime ='2020-01-01 00:00:00'
--declare @ToDate datetime ='2020-12-31 23:59:59'

CREATE TABLE #Temp(
DateView nvarchar(50),
DepartmentKey nvarchar(10),
DepartmentID nvarchar(50),
DepartmentName nvarchar(500),
TeamKey nvarchar(10),
TeamID nvarchar(50),
TeamName nvarchar(500),
Amount money,
CodeID nvarchar(250),
CodeName nvarchar(500),
TypeName  nvarchar(500),
[Rank] INT
)
          INSERT INTO #Temp
        SELECT DateWrite, DepartmentKey,DepartmentID,DepartmentName,TeamKey,TeamID,TeamName,ROUND(SUM(Amount),0) AS Amount,
        CodeID,CodeName,TypeName,[Rank]
        FROM [dbo].[SLR_ReportOffice_Close]
        WHERE DateWrite BETWEEN @FromDate and @ToDate AND Parent IS  NOT NULL  
        AND RecordStatus <> 99
        GROUP BY DateWrite, DepartmentKey,DepartmentID,DepartmentName,TeamKey,TeamID,TeamName,CodeID,CodeName,TypeName,[Rank]

        ;WITH K AS 
                (
                SELECT TypeName,CodeName,CodeID,CONCAT(MONTH([DateView]),'/',YEAR([DateView])) AS [DATE],
                CAST(TeamKey AS NVARCHAR(10)) AS TeamKey,
                DepartmentKey,
                CAST(Amount AS money)AS Amount,Rank
                FROM #Temp 
	                UNION ALL
	                SELECT
	                TypeName,
			        CodeName,
					CodeID,
	                '99/9999'[DATE],
	                TeamKey,
                    DepartmentKey,
	                SUM(Amount) AS Amount,
					Rank
	                FROM #Temp 
	                GROUP BY TypeName,CodeName,CodeID,DepartmentKey,TeamKey,Rank
                ) 

SELECT LeftColumn,HeaderColumn,Amount FROM(
	        SELECT ISNULL(A.TypeName,'')+' '+A.CodeName +'|'+CAST(A.Rank AS NVARCHAR(50)) AS LeftColumn,[DATE]+'|'+CAST(A.TeamKey AS NVARCHAR(10))+'|'+[dbo].[Fn_GetTeamName](A.TeamKey) AS HeaderColumn,Amount,[DATE],
	        D.Rank AS BranchRank,C.Rank AS DepartmentRank,B.Rank AS TeamRank
	        FROM K A
			LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
	        LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
	        LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
            WHERE 1=1 @Parameter
         ) X
         ORDER BY  LEN([DATE]),[DATE],BranchRank,DepartmentRank,TeamRank

DROP TABLE #Temp
";
            zSQL = zSQL.Replace("@Parameter", Fillter);
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        //Xem theo Bộ phận
        public static DataTable THUNHAPTHEOTHANG_BOPHANGIANTIEP_BOPHAN(DateTime FromDate, DateTime ToDate, int DepartmentKey, string Salary)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);

            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, 1, 0, 0, 0);
            zToDate = zToDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            string Fillter = "";
            if (Salary.Length > 0)
            {
                Fillter += " AND " + Salary;
            }
            if (DepartmentKey > 0)
            {
                Fillter += " AND A.DepartmentKey = @DepartmentKey";
            }

            DataTable zTable = new DataTable();
            string zSQL = @"
--declare @FromDate datetime ='2020-06-01 00:00:00'
--declare @ToDate datetime ='2020-12-31 23:59:59'

CREATE TABLE #Temp(
DateView nvarchar(50),
DepartmentKey nvarchar(10),
DepartmentID nvarchar(50),
DepartmentName nvarchar(500),
Amount money,
CodeID nvarchar(250),
CodeName nvarchar(500),
TypeName  nvarchar(500),
[Rank] INT
)
        INSERT INTO #Temp
        SELECT DateWrite, DepartmentKey,DepartmentID,DepartmentName,ROUND(SUM(Amount),0) AS Amount,
        CodeID,CodeName,TypeName,[Rank]
        FROM [dbo].[SLR_ReportOffice_Close]
        WHERE DateWrite BETWEEN @FromDate and @ToDate AND Parent IS  NOT NULL  
        AND RecordStatus <> 99
        GROUP BY DateWrite, DepartmentKey,DepartmentID,DepartmentName,CodeID,CodeName,TypeName,[Rank]

        ;WITH K AS 
                (
                SELECT TypeName,CodeName,CodeID,CONCAT(MONTH([DateView]),'/',YEAR([DateView])) AS [DATE],
                CAST(DepartmentKey AS NVARCHAR(10)) AS DepartmentKey,
                CAST(Amount AS money)AS Amount,Rank
                FROM #Temp 
	                UNION ALL
	                SELECT
	                TypeName,
			        CodeName,
					CodeID,
	                '99/9999'[DATE],
	                DepartmentKey,
	                SUM(Amount) AS Amount,
					Rank
	                FROM #Temp 
	                GROUP BY TypeName,CodeName,CodeID,DepartmentKey,Rank
                ) 

SELECT LeftColumn,HeaderColumn,Amount FROM(
	        SELECT ISNULL(A.TypeName,'')+' '+A.CodeName +'|'+CAST(A.Rank AS NVARCHAR(50)) AS LeftColumn,[DATE]+'|'+CAST(A.DepartmentKey AS NVARCHAR(10))+'|'+[dbo].[Fn_GetDepartmentName](A.DepartmentKey) AS HeaderColumn,Amount,[DATE],
	        D.Rank AS BranchRank,C.Rank AS DepartmentRank
	        FROM K A
	        LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=A.DepartmentKey
	        LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
            WHERE 1=1 @Parameter
         ) X
         ORDER BY  LEN([DATE]),[DATE],BranchRank,DepartmentRank

DROP TABLE #Temp
";
            zSQL = zSQL.Replace("@Parameter", Fillter);
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        #endregion
    }
}
