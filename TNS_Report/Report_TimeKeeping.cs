﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using TN.Connect;

namespace TNS.RPT
{
    public class Report_TimeKeeping
    {
        //Báo cáo thời gian làm việc tại xưởng xem theo ngày - chưa nhân hệ số
        #region[Báo cáo thời gian làm việc tại xưởng xem theo ngày - chưa nhân hệ số]
        public static DataTable TG_LamTaiXuong_TheoCongNhan(DateTime FromDate, DateTime ToDate, int DepartmentKey, int TeamKey,string Search)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);


            string Fillter = "";


            if (DepartmentKey > 0)
                Fillter += " AND A.DepartmentKey =@DepartmentKey ";
            if (TeamKey > 0)
                Fillter += " AND A.TeamKey =@TeamKey ";
            if (Search.Length > 0)
            {
                Fillter += " AND (A.EmployeeID LIKE @Search OR A.EmployeeName LIKE @Search )";
            }

            string zSQL = @"
--declare @FromDate datetime ='2020-06-01 00:00:00'
--declare @ToDate datetime ='2020-06-30 23:59:59'
--declare @TeamKey int = 79

CREATE TABLE Temp
(
	[DepartmentKey] INT ,
	[TeamKey] INT,
	[TeamName] nvarchar(500) ,
	[TeamID] nvarchar(500) ,
	EmployeeKey nvarchar(500),
	EmployeeID nvarchar(500),
	EmployeeName nvarchar(500),
	[DateWrite] nvarchar(500),
	[TotalTime] FLOAT,
	[OverTime] FLOAT ,
	[DifferentTime] FLOAT 
)
        --Biên số giờ thành tổng số phút =Tổng số phút từ quy đổi giờ + tổng số phút
INSERT INTO Temp
SELECT [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
TeamKey,TeamName,TeamID,EmployeeKey,EmployeeID,EmployeeName,
CONCAT(DAY(DateImport),'/',MONTH(DateImport),'/',YEAR(DateImport)),
ROUND(((SUM(CAST (LEFT(TotalTime ,2) AS MONEY)) *60 + SUM(CAST (Right(TotalTime ,2) AS MONEY))) ),0) AS TotalTime,
ROUND(((SUM(CAST (LEFT(OverTime ,2) AS MONEY)) *60 + SUM(CAST (Right(OverTime ,2) AS MONEY))) ),0) AS OverTime,
ROUND(((SUM(CAST (LEFT(DifferentTime ,2) AS MONEY)) *60 + SUM(CAST (Right(DifferentTime ,2) AS MONEY))) ),0) AS DifferentTime
FROM [dbo].[Temp_Import_Detail] WHERE RecordStatus <> 99 
AND DateImport BETWEEN @FromDate AND @ToDate 
GROUP BY DateImport,
 MONTH(DateImport),YEAR(DateImport),DAY(DateImport),[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
TeamKey,TeamName,TeamID,EmployeeKey,EmployeeID,EmployeeName

--SELECT * FROM Temp

SELECT  (CAST(P.EmployeeKey AS NVARCHAR(50)) +'|'+P.EmployeeName+'|'+P.EmployeeID+'|'+[dbo].[Fn_GetPositionName_FromEmployeeKey](P.EmployeeKey)+'|'+CAST(P.TeamKey AS NVARCHAR(50))+'|'+P.TeamName+'|'+CAST(DepartmentRank AS NVARCHAR(10))+'|'+CAST(TeamRank AS NVARCHAR(10))) AS LeftColumn,
P.DateWrite AS HeaderColumn,
P.[TotalTime], P.[OverTime], P.[DifferentTime]
FROM
(
	SELECT A.TeamKey,A.TeamID,A.TeamName,
	A.EmployeeKey,A.EmployeeID,A.EmployeeName, DateWrite ,
	A.[TotalTime], A.[OverTime], A.[DifferentTime],
	C.RANK AS DepartmentRank,B.RANK as TeamRank FROM Temp A 
	LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
	LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
    WHERE 1=1 @Parameter
) P
    ORDER BY LEN(DateWrite), DateWrite,DepartmentRank,TeamRank,LEN(EmployeeID), EmployeeID

DROP TABLE Temp";
            zSQL = zSQL.Replace("@Parameter", Fillter);


            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search + "%";
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable TG_LamTaiXuong_TheoNhom(DateTime FromDate, DateTime ToDate, int DepartmentKey, int TeamKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);


            string Fillter = "";


            if (DepartmentKey > 0)
                Fillter += " AND A.DepartmentKey =@DepartmentKey ";
            if (TeamKey > 0)
                Fillter += " AND A.TeamKey =@TeamKey ";
           
            string zSQL = @"
--declare @FromDate datetime ='2020-06-01 00:00:00'
--declare @ToDate datetime ='2020-06-30 23:59:59'
--declare @TeamKey int = 79

CREATE TABLE Temp
(
	[DepartmentKey] INT ,
	[TeamKey] INT,
	[TeamName] nvarchar(500) ,
	[TeamID] nvarchar(500) ,
	[DateWrite] nvarchar(500),
	[TotalTime] FLOAT,
	[OverTime] FLOAT ,
	[DifferentTime] FLOAT 
)
--Biên số giờ thành tổng số phút =Tổng số phút từ quy đổi giờ + tổng số phút

INSERT INTO Temp
SELECT [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
TeamKey,TeamName,TeamID,
CONCAT(DAY(DateImport),'/',MONTH(DateImport),'/',YEAR(DateImport)),
ROUND(((SUM(CAST (LEFT(TotalTime ,2) AS MONEY)) *60 + SUM(CAST (Right(TotalTime ,2) AS MONEY))) ),0) AS TotalTime,
ROUND(((SUM(CAST (LEFT(OverTime ,2) AS MONEY)) *60 + SUM(CAST (Right(OverTime ,2) AS MONEY))) ),0) AS OverTime,
ROUND(((SUM(CAST (LEFT(DifferentTime ,2) AS MONEY)) *60 + SUM(CAST (Right(DifferentTime ,2) AS MONEY))) ),0) AS DifferentTime
FROM [dbo].[Temp_Import_Detail] WHERE RecordStatus <> 99 
AND DateImport BETWEEN @FromDate AND @ToDate 
GROUP BY DateImport,
 MONTH(DateImport),YEAR(DateImport),DAY(DateImport),[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
TeamKey,TeamName,TeamID

--SELECT * FROM Temp
SELECT  (CAST(P.TeamKey AS NVARCHAR(50)) +'|'+P.TeamName+'|'+P.TeamID+'|'+CAST(P.DepartmentKey AS NVARCHAR(50))+'|'+P.DepartmentName+'|'+CAST(DepartmentRank AS NVARCHAR(10))+'|'+CAST(TeamRank AS NVARCHAR(10))) AS LeftColumn,
P.DateWrite AS HeaderColumn,
P.[TotalTime], P.[OverTime], P.[DifferentTime]
FROM
(
	SELECT A.TeamKey,A.TeamID,A.TeamName,
	A.DepartmentKey,C.DepartmentName,
	DateWrite ,
	A.[TotalTime], A.[OverTime], A.[DifferentTime],
	C.RANK AS DepartmentRank,B.RANK as TeamRank FROM Temp A 
	LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
	LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
    WHERE 1=1 @Parameter
) P
    ORDER BY LEN(DateWrite), DateWrite,DepartmentRank,TeamRank


DROP TABLE Temp
";
            zSQL = zSQL.Replace("@Parameter", Fillter);


            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable TG_LamTaiXuong_TheoBoPhan(DateTime FromDate, DateTime ToDate, int DepartmentKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);


            string Fillter = "AND A.BranchKey = 4";
            if (DepartmentKey > 0)
                Fillter += " AND A.DepartmentKey =@DepartmentKey ";

            string zSQL = @"
--declare @FromDate datetime ='2020-06-01 00:00:00'
--declare @ToDate datetime ='2020-06-30 23:59:59'
--declare @TeamKey int = 79

CREATE TABLE Temp
(
	[BranchKey] INT ,
	[DepartmentKey] INT,
	[DateWrite] nvarchar(500),
	[TotalTime] FLOAT,
	[OverTime] FLOAT ,
	[DifferentTime] FLOAT 
)
        --Biên số giờ thành tổng số phút =Tổng số phút từ quy đổi giờ + tổng số phút
INSERT INTO Temp
SELECT [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
CONCAT(DAY(DateImport),'/',MONTH(DateImport),'/',YEAR(DateImport)),
ROUND(((SUM(CAST (LEFT(TotalTime ,2) AS MONEY)) *60 + SUM(CAST (Right(TotalTime ,2) AS MONEY))) ),0) AS TotalTime,
ROUND(((SUM(CAST (LEFT(OverTime ,2) AS MONEY)) *60 + SUM(CAST (Right(OverTime ,2) AS MONEY))) ),0) AS OverTime,
ROUND(((SUM(CAST (LEFT(DifferentTime ,2) AS MONEY)) *60 + SUM(CAST (Right(DifferentTime ,2) AS MONEY))) ),0) AS DifferentTime
FROM [dbo].[Temp_Import_Detail] WHERE RecordStatus <> 99 
AND DateImport BETWEEN @FromDate AND @ToDate 
GROUP BY DateImport,
 MONTH(DateImport),YEAR(DateImport),DAY(DateImport),[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
TeamKey,TeamName,TeamID

--SELECT * FROM Temp
SELECT  (CAST(P.DepartmentKey AS NVARCHAR(50)) +'|'+P.DepartmentName+'|'+P.DepartmentID+'|'+CAST(P.BranchKey AS NVARCHAR(50))+'|'+P.BranchName+'|'+CAST( BranchRank AS NVARCHAR(10))+'|'+CAST(DepartmentRank AS NVARCHAR(10))) AS LeftColumn,
P.DateWrite AS HeaderColumn,
P.[TotalTime], P.[OverTime], P.[DifferentTime]
FROM
(
	SELECT A.DepartmentKey,B.DepartmentID,B.DepartmentName,
	A.BranchKey,C.BranchName,
	DateWrite ,
	A.[TotalTime], A.[OverTime], A.[DifferentTime],
	C.RANK AS BranchRank,B.RANK as DepartmentRank FROM Temp A 
	LEFT JOIN [dbo].[SYS_Department] B ON B.DepartmentKey=A.DepartmentKey
	LEFT JOIN [dbo].[SYS_Branch] C ON C.BranchKey=B.BranchKey
    WHERE 1=1 @Parameter
) P
    ORDER BY LEN(DateWrite), DateWrite,BranchRank,DepartmentRank


DROP TABLE Temp
";
            zSQL = zSQL.Replace("@Parameter", Fillter);


            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        #endregion
        //Báo cáo thời gian làm việc tại xưởng xem theo ngày - đã nhân hệ số
        #region[Báo cáo thời gian làm việc tại xưởng xem theo ngày - đã nhân hệ số]
        public static DataTable TG_LamTaiXuong_TheoCongNhan_DaNhanHeSo(DateTime FromDate, DateTime ToDate, int DepartmentKey, int TeamKey, string Search)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);


            string Fillter = "";


            if (DepartmentKey > 0)
                Fillter += " AND A.DepartmentKey =@DepartmentKey ";
            if (TeamKey > 0)
                Fillter += " AND A.TeamKey =@TeamKey ";
            if (Search.Length > 0)
            {
                Fillter += " AND (A.EmployeeID LIKE @Search OR A.EmployeeName LIKE @Search )";
            }

            string zSQL = @"
--declare @FromDate datetime ='2020-06-01 00:00:00'
--declare @ToDate datetime ='2020-06-30 23:59:59'
--declare @TeamKey int = 79

CREATE TABLE Temp
(
	[DepartmentKey] INT ,
	[TeamKey] INT,
	[TeamName] nvarchar(500) ,
	[TeamID] nvarchar(500) ,
	EmployeeKey nvarchar(500),
	EmployeeID nvarchar(500),
	EmployeeName nvarchar(500),
	[DateWrite] nvarchar(500),
	[TotalTime] FLOAT,
	[OverTime] FLOAT ,
	[DifferentTime] FLOAT 
)
--Biên số giờ thành tổng số phút = (Tổng số phút từ quy đổi giờ + tổng số phút)* hệ số
        --Tạo bảng tạm từng ngày
        INSERT INTO Temp
        SELECT [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
		TeamKey,TeamName,TeamID,EmployeeKey,EmployeeID,EmployeeName,
		CONCAT(DAY(DateImport),'/',MONTH(DateImport),'/',YEAR(DateImport)) AS DateWrite,
		ROUND((CAST (LEFT(TotalTime ,2) AS MONEY) *60 + CAST (Right(TotalTime ,2) AS MONEY)),0) * Rate AS TotalTime,
		ROUND((CAST (LEFT(OverTime ,2) AS MONEY) *60 + CAST (Right(OverTime ,2) AS MONEY)),0) * Rate  AS OverTime,
		ROUND((CAST (LEFT(DifferentTime ,2) AS MONEY) *60 + CAST (Right(DifferentTime ,2) AS MONEY)),0) * Rate AS DifferentTime
		FROM [dbo].[Temp_Import_Detail] WHERE RecordStatus <> 99 
		AND DateImport BETWEEN @FromDate AND @ToDate

 --SELECT * FROM Temp

SELECT  (CAST(P.EmployeeKey AS NVARCHAR(50)) +'|'+P.EmployeeName+'|'+P.EmployeeID+'|'+[dbo].[Fn_GetPositionName_FromEmployeeKey](P.EmployeeKey)+'|'+CAST(P.TeamKey AS NVARCHAR(50))+'|'+P.TeamName+'|'+CAST(DepartmentRank AS NVARCHAR(10))+'|'+CAST(TeamRank AS NVARCHAR(10))) AS LeftColumn,
P.DateWrite AS HeaderColumn,
P.[TotalTime], P.[OverTime], P.[DifferentTime]
FROM
(
	SELECT A.TeamKey,A.TeamID,A.TeamName,
	A.EmployeeKey,A.EmployeeID,A.EmployeeName, DateWrite ,
	A.[TotalTime], A.[OverTime], A.[DifferentTime],
	C.RANK AS DepartmentRank,B.RANK as TeamRank FROM Temp A 
	LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
	LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
    WHERE 1=1 @Parameter
) P
    ORDER BY LEN(DateWrite), DateWrite,DepartmentRank,TeamRank,LEN(EmployeeID), EmployeeID

DROP TABLE Temp";
            zSQL = zSQL.Replace("@Parameter", Fillter);


            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search + "%";
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable TG_LamTaiXuong_TheoNhom_DaNhanHeSo(DateTime FromDate, DateTime ToDate, int DepartmentKey, int TeamKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);


            string Fillter = "";


            if (DepartmentKey > 0)
                Fillter += " AND A.DepartmentKey =@DepartmentKey ";
            if (TeamKey > 0)
                Fillter += " AND A.TeamKey =@TeamKey ";

            string zSQL = @"
--declare @FromDate datetime ='2020-06-01 00:00:00'
--declare @ToDate datetime ='2020-06-30 23:59:59'
--declare @TeamKey int = 79

CREATE TABLE Temp
(
	[DepartmentKey] INT ,
	[TeamKey] INT,
	[TeamName] nvarchar(500) ,
	[TeamID] nvarchar(500) ,
	[DateWrite] nvarchar(500),
	[TotalTime] FLOAT,
	[OverTime] FLOAT ,
	[DifferentTime] FLOAT 
)
--Biên số giờ thành tổng số phút =Tổng số phút từ quy đổi giờ + tổng số phút

INSERT INTO Temp
SELECT DepartmentKey,TeamKey,TeamName,TeamID,
DateWrite,SUM(TotalTime),SUM(OverTime),SUM(DifferentTime)
FROM (
		SELECT [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey) AS DepartmentKey,
		TeamKey,TeamName,TeamID,EmployeeKey,EmployeeID,EmployeeName,
		CONCAT(DAY(DateImport),'/',MONTH(DateImport),'/',YEAR(DateImport)) AS DateWrite,
		ROUND((CAST (LEFT(TotalTime ,2) AS MONEY) *60 + CAST (Right(TotalTime ,2) AS MONEY)),0) * Rate AS TotalTime,
		ROUND((CAST (LEFT(OverTime ,2) AS MONEY) *60 + CAST (Right(OverTime ,2) AS MONEY)),0) * Rate AS OverTime,
		ROUND((CAST (LEFT(DifferentTime ,2) AS MONEY) *60 + CAST (Right(DifferentTime ,2) AS MONEY)),0) * Rate AS DifferentTime
		FROM [dbo].[Temp_Import_Detail] WHERE RecordStatus <> 99 
		AND DateImport BETWEEN @FromDate AND @ToDate
)X
GROUP BY DepartmentKey,TeamKey,TeamName,TeamID,DateWrite

--SELECT * FROM Temp
SELECT  (CAST(P.TeamKey AS NVARCHAR(50)) +'|'+P.TeamName+'|'+P.TeamID+'|'+CAST(P.DepartmentKey AS NVARCHAR(50))+'|'+P.DepartmentName+'|'+CAST(DepartmentRank AS NVARCHAR(10))+'|'+CAST(TeamRank AS NVARCHAR(10))) AS LeftColumn,
P.DateWrite AS HeaderColumn,
P.[TotalTime], P.[OverTime], P.[DifferentTime]
FROM
(
	SELECT A.TeamKey,A.TeamID,A.TeamName,
	A.DepartmentKey,C.DepartmentName,
	DateWrite ,
	A.[TotalTime], A.[OverTime], A.[DifferentTime],
	C.RANK AS DepartmentRank,B.RANK as TeamRank FROM Temp A 
	LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
	LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
    WHERE 1=1 @Parameter
) P
    ORDER BY LEN(DateWrite), DateWrite,DepartmentRank,TeamRank


DROP TABLE Temp
";
            zSQL = zSQL.Replace("@Parameter", Fillter);


            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable TG_LamTaiXuong_TheoBoPhan_DaNhanHeSo(DateTime FromDate, DateTime ToDate, int DepartmentKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);


            string Fillter = "AND A.BranchKey = 4";
            if (DepartmentKey > 0)
                Fillter += " AND A.DepartmentKey =@DepartmentKey ";

            string zSQL = @"
--declare @FromDate datetime ='2020-06-01 00:00:00'
--declare @ToDate datetime ='2020-06-30 23:59:59'
--declare @TeamKey int = 79

CREATE TABLE Temp
(
	[BranchKey] INT ,
	[DepartmentKey] INT,
	[DateWrite] nvarchar(500),
	[TotalTime] FLOAT,
	[OverTime] FLOAT ,
	[DifferentTime] FLOAT 
)
--Biên số giờ thành tổng số phút =Tổng số phút từ quy đổi giờ + tổng số phút
		INSERT INTO Temp
		SELECT BranchKey,DepartmentKey,
		DateWrite,SUM(TotalTime),SUM(OverTime),SUM(DifferentTime)
		FROM (
				SELECT[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey) AS BranchKey, [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey) AS DepartmentKey,
				TeamKey,TeamName,TeamID,EmployeeKey,EmployeeID,EmployeeName,
				CONCAT(DAY(DateImport),'/',MONTH(DateImport),'/',YEAR(DateImport)) AS DateWrite,
				ROUND((CAST (LEFT(TotalTime ,2) AS MONEY) *60 + CAST (Right(TotalTime ,2) AS MONEY)),0) * Rate AS TotalTime,
				ROUND((CAST (LEFT(OverTime ,2) AS MONEY) *60 + CAST (Right(OverTime ,2) AS MONEY)),0) * Rate AS OverTime,
				ROUND((CAST (LEFT(DifferentTime ,2) AS MONEY) *60 + CAST (Right(DifferentTime ,2) AS MONEY)),0) * Rate AS DifferentTime
				FROM [dbo].[Temp_Import_Detail] WHERE RecordStatus <> 99 
				AND DateImport BETWEEN @FromDate AND @ToDate
		)X
		GROUP BY BranchKey,DepartmentKey,DateWrite

--SELECT * FROM Temp
SELECT  (CAST(P.DepartmentKey AS NVARCHAR(50)) +'|'+P.DepartmentName+'|'+P.DepartmentID+'|'+CAST(P.BranchKey AS NVARCHAR(50))+'|'+P.BranchName+'|'+CAST( BranchRank AS NVARCHAR(10))+'|'+CAST(DepartmentRank AS NVARCHAR(10))) AS LeftColumn,
P.DateWrite AS HeaderColumn,
P.[TotalTime], P.[OverTime], P.[DifferentTime]
FROM
(
	SELECT A.DepartmentKey,B.DepartmentID,B.DepartmentName,
	A.BranchKey,C.BranchName,
	DateWrite ,
	A.[TotalTime], A.[OverTime], A.[DifferentTime],
	C.RANK AS BranchRank,B.RANK as DepartmentRank FROM Temp A 
	LEFT JOIN [dbo].[SYS_Department] B ON B.DepartmentKey=A.DepartmentKey
	LEFT JOIN [dbo].[SYS_Branch] C ON C.BranchKey=B.BranchKey
    WHERE 1=1 @Parameter
) P
    ORDER BY LEN(DateWrite), DateWrite,BranchRank,DepartmentRank


DROP TABLE Temp
";
            zSQL = zSQL.Replace("@Parameter", Fillter);


            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        #endregion


        //Báo cáo làm việc tại xưởng xem theo Tháng-Chưa nhân hệ số
        #region[Báo cáo làm việc tại xưởng xem theo Tháng-Chưa nhân hệ số]
        public static DataTable TG_LamTaiXuong_TheoThang_TheoCongNhan(DateTime FromDate, DateTime ToDate, int DepartmentKey, int TeamKey, string Search)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);


            string Fillter = "";


            if (DepartmentKey > 0)
                Fillter += " AND A.DepartmentKey =@DepartmentKey ";
            if (TeamKey > 0)
                Fillter += " AND A.TeamKey =@TeamKey ";
            if (Search.Length > 0)
            {
                Fillter += " AND (A.EmployeeID LIKE @Search OR A.EmployeeName LIKE @Search )";
            }

            string zSQL = @"
--declare @FromDate datetime ='2020-01-01 00:00:00'
--declare @ToDate datetime ='2020-07-31 23:59:59'
--declare @TeamKey int = 79

CREATE TABLE Temp
(
	[DepartmentKey] INT ,
	[TeamKey] INT,
	[TeamName] nvarchar(500) ,
	[TeamID] nvarchar(500) ,
	EmployeeKey nvarchar(500),
	EmployeeID nvarchar(500),
	EmployeeName nvarchar(500),
	[DateWrite] nvarchar(500),
	[TotalTime] FLOAT,
	[OverTime] FLOAT ,
	[DifferentTime] FLOAT 
)
        --Biên số giờ thành tổng số phút =Tổng số phút từ quy đổi giờ + tổng số phút
INSERT INTO Temp
SELECT [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
TeamKey,TeamName,TeamID,EmployeeKey,EmployeeID,EmployeeName,
CONCAT(MONTH(DateImport),'/',YEAR(DateImport)),
ROUND(((SUM(CAST (LEFT(TotalTime ,2) AS MONEY)) *60 + SUM(CAST (Right(TotalTime ,2) AS MONEY))) ),0) AS TotalTime,
ROUND(((SUM(CAST (LEFT(OverTime ,2) AS MONEY)) *60 + SUM(CAST (Right(OverTime ,2) AS MONEY))) ),0) AS OverTime,
ROUND(((SUM(CAST (LEFT(DifferentTime ,2) AS MONEY)) *60 + SUM(CAST (Right(DifferentTime ,2) AS MONEY))) ),0) AS DifferentTime
FROM [dbo].[Temp_Import_Detail] WHERE RecordStatus <> 99 
AND DateImport BETWEEN @FromDate AND @ToDate 
GROUP BY DateImport,
 MONTH(DateImport),YEAR(DateImport),[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
TeamKey,TeamName,TeamID,EmployeeKey,EmployeeID,EmployeeName

--SELECT * FROM Temp

SELECT  (CAST(P.EmployeeKey AS NVARCHAR(50)) +'|'+P.EmployeeName+'|'+P.EmployeeID+'|'+[dbo].[Fn_GetPositionName_FromEmployeeKey](P.EmployeeKey)+'|'+CAST(P.TeamKey AS NVARCHAR(50))+'|'+P.TeamName+'|'+CAST(DepartmentRank AS NVARCHAR(10))+'|'+CAST(TeamRank AS NVARCHAR(10))) AS LeftColumn,
P.DateWrite AS HeaderColumn,
P.[TotalTime], P.[OverTime], P.[DifferentTime]
FROM
(
	SELECT A.TeamKey,A.TeamID,A.TeamName,
	A.EmployeeKey,A.EmployeeID,A.EmployeeName, DateWrite ,
	A.[TotalTime], A.[OverTime], A.[DifferentTime],
	C.RANK AS DepartmentRank,B.RANK as TeamRank FROM Temp A 
	LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
	LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
    WHERE 1=1 @Parameter
) P
    ORDER BY LEN(DateWrite), DateWrite,DepartmentRank,TeamRank,LEN(EmployeeID), EmployeeID

DROP TABLE Temp";
            zSQL = zSQL.Replace("@Parameter", Fillter);


            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search + "%";
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable TG_LamTaiXuong_TheoThang_TheoNhom(DateTime FromDate, DateTime ToDate, int DepartmentKey, int TeamKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string Fillter = "";

            if (DepartmentKey > 0)
                Fillter += " AND A.DepartmentKey =@DepartmentKey ";
            if (TeamKey > 0)
                Fillter += " AND A.TeamKey =@TeamKey ";
            string zSQL = @"

--declare @FromDate datetime ='2020-01-01 00:00:00'
---declare @ToDate datetime ='2020-07-31 23:59:59'
--declare @TeamKey int = 79

CREATE TABLE Temp
(
	[DepartmentKey] INT ,
	[TeamKey] INT,
	[TeamName] nvarchar(500) ,
	[TeamID] nvarchar(500) ,
	[DateWrite] nvarchar(500),
	[TotalTime] FLOAT,
	[OverTime] FLOAT ,
	[DifferentTime] FLOAT 
)
--Biên số giờ thành tổng số phút =Tổng số phút từ quy đổi giờ + tổng số phút

INSERT INTO Temp
SELECT [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
TeamKey,TeamName,TeamID,
CONCAT(MONTH(DateImport),'/',YEAR(DateImport)),
ROUND(((SUM(CAST (LEFT(TotalTime ,2) AS MONEY)) *60 + SUM(CAST (Right(TotalTime ,2) AS MONEY))) ),0) AS TotalTime,
ROUND(((SUM(CAST (LEFT(OverTime ,2) AS MONEY)) *60 + SUM(CAST (Right(OverTime ,2) AS MONEY))) ),0) AS OverTime,
ROUND(((SUM(CAST (LEFT(DifferentTime ,2) AS MONEY)) *60 + SUM(CAST (Right(DifferentTime ,2) AS MONEY))) ),0) AS DifferentTime
FROM [dbo].[Temp_Import_Detail] WHERE RecordStatus <> 99 
AND DateImport BETWEEN @FromDate AND @ToDate 
GROUP BY 
 MONTH(DateImport),YEAR(DateImport),[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
TeamKey,TeamName,TeamID

--SELECT * FROM Temp
SELECT  (CAST(P.TeamKey AS NVARCHAR(50)) +'|'+P.TeamName+'|'+P.TeamID+'|'+CAST(P.DepartmentKey AS NVARCHAR(50))+'|'+P.DepartmentName+'|'+CAST(DepartmentRank AS NVARCHAR(10))+'|'+CAST(TeamRank AS NVARCHAR(10))) AS LeftColumn,
P.DateWrite AS HeaderColumn,
P.[TotalTime], P.[OverTime], P.[DifferentTime]
FROM
(
	SELECT A.TeamKey,A.TeamID,A.TeamName,
	A.DepartmentKey,C.DepartmentName,
	DateWrite ,
	A.[TotalTime], A.[OverTime], A.[DifferentTime],
	C.RANK AS DepartmentRank,B.RANK as TeamRank FROM Temp A 
	LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
	LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
    WHERE 1=1 @Parameter
) P
    ORDER BY LEN(DateWrite), DateWrite,DepartmentRank,TeamRank


DROP TABLE Temp
";
            zSQL = zSQL.Replace("@Parameter", Fillter);


            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable TG_LamTaiXuong_TheoThang_TheoBoPhan(DateTime FromDate, DateTime ToDate, int DepartmentKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);


            string Fillter = "AND A.BranchKey = 4";
            if (DepartmentKey > 0)
                Fillter += " AND A.DepartmentKey =@DepartmentKey ";

            string zSQL = @"
--declare @FromDate datetime ='2020-06-01 00:00:00'
--declare @ToDate datetime ='2020-06-30 23:59:59'
--declare @TeamKey int = 79

CREATE TABLE Temp
(
	[BranchKey] INT ,
	[DepartmentKey] INT,
	[DateWrite] nvarchar(500),
	[TotalTime] FLOAT,
	[OverTime] FLOAT ,
	[DifferentTime] FLOAT 
)
        --Biên số giờ thành tổng số phút =Tổng số phút từ quy đổi giờ + tổng số phút
INSERT INTO Temp
SELECT [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),
[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
CONCAT(MONTH(DateImport),'/',YEAR(DateImport)),
ROUND(((SUM(CAST (LEFT(TotalTime ,2) AS MONEY)) *60 + SUM(CAST (Right(TotalTime ,2) AS MONEY))) ),0) AS TotalTime,
ROUND(((SUM(CAST (LEFT(OverTime ,2) AS MONEY)) *60 + SUM(CAST (Right(OverTime ,2) AS MONEY))) ),0) AS OverTime,
ROUND(((SUM(CAST (LEFT(DifferentTime ,2) AS MONEY)) *60 + SUM(CAST (Right(DifferentTime ,2) AS MONEY))) ),0) AS DifferentTime
FROM [dbo].[Temp_Import_Detail] WHERE RecordStatus <> 99 
AND DateImport BETWEEN @FromDate AND @ToDate 
GROUP BY 
 MONTH(DateImport),YEAR(DateImport),DAY(DateImport),[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
TeamKey,TeamName,TeamID

--SELECT * FROM Temp
SELECT  (CAST(P.DepartmentKey AS NVARCHAR(50)) +'|'+P.DepartmentName+'|'+P.DepartmentID+'|'+CAST(P.BranchKey AS NVARCHAR(50))+'|'+P.BranchName+'|'+CAST( BranchRank AS NVARCHAR(10))+'|'+CAST(DepartmentRank AS NVARCHAR(10))) AS LeftColumn,
P.DateWrite AS HeaderColumn,
P.[TotalTime], P.[OverTime], P.[DifferentTime]
FROM
(
	SELECT A.DepartmentKey,B.DepartmentID,B.DepartmentName,
	A.BranchKey,C.BranchName,
	DateWrite ,
	A.[TotalTime], A.[OverTime], A.[DifferentTime],
	C.RANK AS BranchRank,B.RANK as DepartmentRank FROM Temp A 
	LEFT JOIN [dbo].[SYS_Department] B ON B.DepartmentKey=A.DepartmentKey
	LEFT JOIN [dbo].[SYS_Branch] C ON C.BranchKey=B.BranchKey
    WHERE 1=1 @Parameter
) P
    ORDER BY LEN(DateWrite), DateWrite,BranchRank,DepartmentRank


DROP TABLE Temp
";
            zSQL = zSQL.Replace("@Parameter", Fillter);


            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        #endregion
        //Báo cáo làm việc tại xưởng xem theo Tháng-Đã nhan hệ số
        #region[Báo cáo làm việc tại xưởng xem theo Tháng-Đã nhân hệ số]
        public static DataTable TG_LamTaiXuong_TheoThang_TheoCongNhan_DaNhanHeSo(DateTime FromDate, DateTime ToDate, int DepartmentKey, int TeamKey, string Search)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string Fillter = "";


            if (DepartmentKey > 0)
                Fillter += " AND A.DepartmentKey =@DepartmentKey ";
            if (TeamKey > 0)
                Fillter += " AND A.TeamKey =@TeamKey ";
            if (Search.Length > 0)
            {
                Fillter += " AND (A.EmployeeID LIKE @Search OR A.EmployeeName LIKE @Search )";
            }

            string zSQL = @"
--declare @FromDate datetime ='2020-01-01 00:00:00'
--declare @ToDate datetime ='2020-07-31 23:59:59'
--declare @TeamKey int = 79

CREATE TABLE Temp
(
	[DepartmentKey] INT ,
	[TeamKey] INT,
	[TeamName] nvarchar(500) ,
	[TeamID] nvarchar(500) ,
	EmployeeKey nvarchar(500),
	EmployeeID nvarchar(500),
	EmployeeName nvarchar(500),
	[DateWrite] nvarchar(500),
	[TotalTime] FLOAT,
	[OverTime] FLOAT ,
	[DifferentTime] FLOAT 
)
        --Biên số giờ thành tổng số phút =Tổng số phút từ quy đổi giờ + tổng số phút
INSERT INTO Temp
SELECT DepartmentKey,TeamKey,TeamName,TeamID,EmployeeKey,EmployeeID,EmployeeName,
DateWrite,SUM(TotalTime),SUM(OverTime),SUM(DifferentTime)
FROM (
		SELECT [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey) AS DepartmentKey,
		TeamKey,TeamName,TeamID,EmployeeKey,EmployeeID,EmployeeName,
		CONCAT(MONTH(DateImport),'/',YEAR(DateImport)) AS DateWrite,
		ROUND((CAST (LEFT(TotalTime ,2) AS MONEY) *60 + CAST (Right(TotalTime ,2) AS MONEY)),0) * Rate AS TotalTime,
		ROUND((CAST (LEFT(OverTime ,2) AS MONEY) *60 + CAST (Right(OverTime ,2) AS MONEY)),0) * Rate AS OverTime,
		ROUND((CAST (LEFT(DifferentTime ,2) AS MONEY) *60 + CAST (Right(DifferentTime ,2) AS MONEY)),0) * Rate AS DifferentTime
		FROM [dbo].[Temp_Import_Detail] WHERE RecordStatus <> 99 
		AND DateImport BETWEEN @FromDate AND @ToDate
)X
GROUP BY DepartmentKey,TeamKey,TeamName,TeamID,EmployeeKey,EmployeeID,EmployeeName,
DateWrite

--SELECT * FROM Temp

SELECT  (CAST(P.EmployeeKey AS NVARCHAR(50)) +'|'+P.EmployeeName+'|'+P.EmployeeID+'|'+[dbo].[Fn_GetPositionName_FromEmployeeKey](P.EmployeeKey)+'|'+CAST(P.TeamKey AS NVARCHAR(50))+'|'+P.TeamName+'|'+CAST(DepartmentRank AS NVARCHAR(10))+'|'+CAST(TeamRank AS NVARCHAR(10))) AS LeftColumn,
P.DateWrite AS HeaderColumn,
P.[TotalTime], P.[OverTime], P.[DifferentTime]
FROM
(
	SELECT A.TeamKey,A.TeamID,A.TeamName,
	A.EmployeeKey,A.EmployeeID,A.EmployeeName, DateWrite ,
	A.[TotalTime], A.[OverTime], A.[DifferentTime],
	C.RANK AS DepartmentRank,B.RANK as TeamRank FROM Temp A 
	LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
	LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
    WHERE 1=1 @Parameter
) P
    ORDER BY LEN(DateWrite), DateWrite,DepartmentRank,TeamRank,LEN(EmployeeID), EmployeeID

DROP TABLE Temp";
            zSQL = zSQL.Replace("@Parameter", Fillter);


            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search + "%";
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable TG_LamTaiXuong_TheoThang_TheoNhom_DaNhanHeSo(DateTime FromDate, DateTime ToDate, int DepartmentKey, int TeamKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string Fillter = "";

            if (DepartmentKey > 0)
                Fillter += " AND A.DepartmentKey =@DepartmentKey ";
            if (TeamKey > 0)
                Fillter += " AND A.TeamKey =@TeamKey ";
            string zSQL = @"

--declare @FromDate datetime ='2020-01-01 00:00:00'
--declare @ToDate datetime ='2020-07-31 23:59:59'
--declare @TeamKey int = 79

CREATE TABLE Temp
(
	[DepartmentKey] INT ,
	[TeamKey] INT,
	[TeamName] nvarchar(500) ,
	[TeamID] nvarchar(500) ,
	[DateWrite] nvarchar(500),
	[TotalTime] FLOAT,
	[OverTime] FLOAT ,
	[DifferentTime] FLOAT 
)
--Biên số giờ thành tổng số phút =Tổng số phút từ quy đổi giờ + tổng số phút

INSERT INTO Temp
SELECT DepartmentKey,TeamKey,TeamName,TeamID,
DateWrite,SUM(TotalTime),SUM(OverTime),SUM(DifferentTime)
FROM (
		SELECT [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey) AS DepartmentKey,
		TeamKey,TeamName,TeamID,EmployeeKey,EmployeeID,EmployeeName,
		CONCAT(MONTH(DateImport),'/',YEAR(DateImport)) AS DateWrite,
		ROUND((CAST (LEFT(TotalTime ,2) AS MONEY) *60 + CAST (Right(TotalTime ,2) AS MONEY)),0) * Rate AS TotalTime,
		ROUND((CAST (LEFT(OverTime ,2) AS MONEY) *60 + CAST (Right(OverTime ,2) AS MONEY)),0) * Rate AS OverTime,
		ROUND((CAST (LEFT(DifferentTime ,2) AS MONEY) *60 + CAST (Right(DifferentTime ,2) AS MONEY)),0) * Rate AS DifferentTime
		FROM [dbo].[Temp_Import_Detail] WHERE RecordStatus <> 99 
		AND DateImport BETWEEN @FromDate AND @ToDate
)X
GROUP BY DepartmentKey,TeamKey,TeamName,TeamID,DateWrite


--SELECT * FROM Temp
SELECT  (CAST(P.TeamKey AS NVARCHAR(50)) +'|'+P.TeamName+'|'+P.TeamID+'|'+CAST(P.DepartmentKey AS NVARCHAR(50))+'|'+P.DepartmentName+'|'+CAST(DepartmentRank AS NVARCHAR(10))+'|'+CAST(TeamRank AS NVARCHAR(10))) AS LeftColumn,
P.DateWrite AS HeaderColumn,
P.[TotalTime], P.[OverTime], P.[DifferentTime]
FROM
(
	SELECT A.TeamKey,A.TeamID,A.TeamName,
	A.DepartmentKey,C.DepartmentName,
	DateWrite ,
	A.[TotalTime], A.[OverTime], A.[DifferentTime],
	C.RANK AS DepartmentRank,B.RANK as TeamRank FROM Temp A 
	LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
	LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
    WHERE 1=1 @Parameter
) P
    ORDER BY LEN(DateWrite), DateWrite,DepartmentRank,TeamRank


DROP TABLE Temp
";
            zSQL = zSQL.Replace("@Parameter", Fillter);


            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable TG_LamTaiXuong_TheoThang_TheoBoPhan_DaNhanHeSo(DateTime FromDate, DateTime ToDate, int DepartmentKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string Fillter = "AND A.BranchKey = 4";
            if (DepartmentKey > 0)
                Fillter += " AND A.DepartmentKey =@DepartmentKey ";

            string zSQL = @"
--declare @FromDate datetime ='2020-06-01 00:00:00'
--declare @ToDate datetime ='2020-06-30 23:59:59'
--declare @TeamKey int = 79

CREATE TABLE Temp
(
	[BranchKey] INT ,
	[DepartmentKey] INT,
	[DateWrite] nvarchar(500),
	[TotalTime] FLOAT,
	[OverTime] FLOAT ,
	[DifferentTime] FLOAT 
)
        --Biên số giờ thành tổng số phút =Tổng số phút từ quy đổi giờ + tổng số phút
INSERT INTO Temp
SELECT BranchKey,DepartmentKey,
DateWrite,SUM(TotalTime),SUM(OverTime),SUM(DifferentTime)
FROM (
		SELECT [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey) AS BranchKey,
		[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey) AS DepartmentKey,
		CONCAT(MONTH(DateImport),'/',YEAR(DateImport)) AS DateWrite,
		ROUND((CAST (LEFT(TotalTime ,2) AS MONEY) *60 + CAST (Right(TotalTime ,2) AS MONEY)),0) * Rate AS TotalTime,
		ROUND((CAST (LEFT(OverTime ,2) AS MONEY) *60 + CAST (Right(OverTime ,2) AS MONEY)),0) * Rate AS OverTime,
		ROUND((CAST (LEFT(DifferentTime ,2) AS MONEY) *60 + CAST (Right(DifferentTime ,2) AS MONEY)),0) * Rate AS DifferentTime
		FROM [dbo].[Temp_Import_Detail] WHERE RecordStatus <> 99 
		AND DateImport BETWEEN @FromDate AND @ToDate
)X
GROUP BY BranchKey,DepartmentKey,DateWrite

--SELECT * FROM Temp
SELECT  (CAST(P.DepartmentKey AS NVARCHAR(50)) +'|'+P.DepartmentName+'|'+P.DepartmentID+'|'+CAST(P.BranchKey AS NVARCHAR(50))+'|'+P.BranchName+'|'+CAST( BranchRank AS NVARCHAR(10))+'|'+CAST(DepartmentRank AS NVARCHAR(10))) AS LeftColumn,
P.DateWrite AS HeaderColumn,
P.[TotalTime], P.[OverTime], P.[DifferentTime]
FROM
(
	SELECT A.DepartmentKey,B.DepartmentID,B.DepartmentName,
	A.BranchKey,C.BranchName,
	DateWrite ,
	A.[TotalTime], A.[OverTime], A.[DifferentTime],
	C.RANK AS BranchRank,B.RANK as DepartmentRank FROM Temp A 
	LEFT JOIN [dbo].[SYS_Department] B ON B.DepartmentKey=A.DepartmentKey
	LEFT JOIN [dbo].[SYS_Branch] C ON C.BranchKey=B.BranchKey
    WHERE 1=1 @Parameter
) P
    ORDER BY LEN(DateWrite), DateWrite,BranchRank,DepartmentRank


DROP TABLE Temp
";
            zSQL = zSQL.Replace("@Parameter", Fillter);


            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        #endregion
    }
}