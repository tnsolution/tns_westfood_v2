﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Connect;

namespace TNS_Report
{
    public class Report_HumanResource
    {
        public static DataTable DANHSACHNHANSU(DateTime FromDate, int BranchKey, int DepartmentKey, int TeamKey,int PositionKey, string Search, int Gender, int Overtime, int Score, int Status)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate;
            //ngày hiện tại sẽ bị lỗi vì đang lấy ngày trong sql là getdate
            if (DateTime.Now.Year == FromDate.Year && DateTime.Now.Month == FromDate.Month && DateTime.Now.Day == FromDate.Day)
                zToDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            else
                zToDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 23, 59, 59);


            string Fillter = "";
            if (BranchKey > 0)
            {
                Fillter += " AND A.BranchKey = @BranchKey";
            }
            if (DepartmentKey > 0)
            {
                Fillter += " AND A.DepartmentKey = @DepartmentKey";
            }
            if (TeamKey > 0)
            {
                Fillter += " AND A.TeamKey = @TeamKey";
            }
            if (PositionKey > 0)
            {
                Fillter += " AND A.PositionKey = @PositionKey";
            }
            if (Search.Length > 0)
            {
                Fillter += " AND ( A.EmployeeID LIKE @Search OR A.EmployeeID LIKE @Search OR A.CompanyPhone LIKE @Search OR A.IssueID LIKE @Search )";
            }
            if (Gender > 0)
            {
                if (Gender == 1)
                    Fillter += " AND A.Gender = 1";
                else
                    Fillter += " AND A.Gender = 0";
            }
            if (Overtime > 0)
            {
                if (Overtime == 1)
                    Fillter += " AND A.OverTime = 1";
                else
                    Fillter += " AND A.OverTime = 0";
            }
            if (Score > 0)
            {
                if (Score == 1)
                    Fillter += " AND A.ScoreStock = 1";
                else
                    Fillter += " AND A.ScoreStock = 0";
            }
            if (Status > 0)
            {
                if (Status == 1)
                    Fillter += " AND (A.WorkingStatus = 1 AND LeavingDate IS NULL )";
                else
                    Fillter += " AND (A.WorkingStatus = 2  AND LeavingDate IS NOT NULL)";
            }
            DataTable zTable = new DataTable();
            string zSQL = @"
--declare @FromDate datetime ='2020-06-01 00:00:00'
--declare @ToDate datetime ='2020-06-01 23:59:59'

SELECT A.EmployeeKey,A.EmployeeName,A.EmployeeID,A.PositionName,A.TeamName,A.DepartmentName,A.BranchName,
A.BirthDay,A.Gender,A.IssueID,A.IssueDate,A.IssuePlace,A.HomeTown,A.WorkingStatus,A.StartingDate,
A.LeavingTryDate,A.LeavingDate,A.CompanyPhone,A.CompanyEmail,A.BankCode,A.AccountCode,A.AccountCode2,
A.OverTime,A.ScoreStock
FROM
		(
		SELECT 
		A.EmployeeKey AS EmployeeKey,
		[dbo].[Fn_GetFullName](A.EmployeeKey) AS EmployeeName,
		A.EmployeeID AS EmployeeID,
		[dbo].[Fn_PositionKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate) AS PositionKey,
		[dbo].[Fn_GetPositionName]([dbo].[Fn_PositionKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate)) AS PositionName,
		[dbo].[Fn_TeamKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate) AS TeamKey,
		[dbo].[Fn_GetTeamName]([dbo].[Fn_TeamKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate)) AS TeamName ,
		[dbo].[Fn_DepartmentKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate) AS DepartmentKey,
		[dbo].[Fn_GetDepartmentName]([dbo].[Fn_DepartmentKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate)) AS DepartmentName,
		[dbo].[Fn_BranchKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate) AS BranchKey,
		[dbo].[Fn_GetBranchName]([dbo].[Fn_BranchKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate) ) AS BranchName,
		B.BirthDay AS BirthDay,
		B.Gender AS Gender,
		B.IssueID AS IssueID,
		B.IssueDate AS IssueDate,
		B.IssuePlace AS IssuePlace,
		B.HomeTown AS HomeTown,
		A.WorkingStatus AS WorkingStatus,
		A.StartingDate AS StartingDate,
		A.LeavingTryDate AS LeavingTryDate,
		A.LeavingDate AS LeavingDate,
		A.CompanyPhone AS CompanyPhone,
		A.CompanyEmail AS CompanyEmail,
		B.BankCode AS BankCode,
		[dbo].[Fn_GetAccountCodeEmployee](A.TeamKey) AS AccountCode,
		[dbo].[Fn_GetAccountCode2Employee](A.TeamKey) AS AccountCode2,
		A.OverTime AS OverTime,
		A.ScoreStock AS ScoreStock
		FROM HRM_Employee A
		LEFT JOIN [dbo].[SYS_Personal]  B ON B.ParentKey =A.EmployeeKey
		WHERE A.RecordStatus <> 99 AND A.Slug <>1
		AND (A.StartingDate<=@ToDate   )
		) A
LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
LEFT JOIN [dbo].SYS_Department C ON C.DepartmentKey=B.DepartmentKey
LEFT JOIN [dbo].SYS_Branch D ON D.BranchKey=C.BranchKey 
WHERE 1=1 @Parameter
ORDER BY D.Rank,C.Rank,B.Rank, LEN(A.EmployeeID),A.EmployeeID

";
            zSQL = zSQL.Replace("@Parameter", Fillter);
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BranchKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = PositionKey;
                zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search + "%";
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }


        #region[Ho so nhan sự]
        public static DataTable HOSONHANSU(DateTime FromDate,string EmployeeKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate;
            //ngày hiện tại sẽ bị lỗi vì đang lấy ngày trong sql là getdate
            if (DateTime.Now.Year == FromDate.Year && DateTime.Now.Month == FromDate.Month && DateTime.Now.Day == FromDate.Day)
                zToDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            else
                zToDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 23, 59, 59);


            
            DataTable zTable = new DataTable();
            string zSQL = @"
--declare @FromDate datetime ='2020-06-01 00:00:00'
--declare @ToDate datetime ='2020-06-01 23:59:59'

SELECT A.EmployeeKey,A.EmployeeName,A.EmployeeID,A.PositionName,A.TeamName,A.DepartmentName,A.BranchName,
A.BirthDay,A.Gender,A.IssueID,A.IssueDate,A.IssuePlace,A.HomeTown,A.WorkingStatus,A.StartingDate,
A.LeavingTryDate,A.LeavingDate,A.CompanyPhone,A.CompanyEmail,A.BankCode,A.AddressBank,A.AccountCode,A.AccountCode2,
A.OverTime,A.ScoreStock,A.ReportTo
FROM
		(
		SELECT 
		A.EmployeeKey AS EmployeeKey,
		[dbo].[Fn_GetFullName](A.EmployeeKey) AS EmployeeName,
		A.EmployeeID AS EmployeeID,
		[dbo].[Fn_PositionKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate) AS PositionKey,
		[dbo].[Fn_GetPositionName]([dbo].[Fn_PositionKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate)) AS PositionName,
		[dbo].[Fn_TeamKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate) AS TeamKey,
		[dbo].[Fn_GetTeamName]([dbo].[Fn_TeamKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate)) AS TeamName ,
		[dbo].[Fn_DepartmentKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate) AS DepartmentKey,
		[dbo].[Fn_GetDepartmentName]([dbo].[Fn_DepartmentKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate)) AS DepartmentName,
		[dbo].[Fn_BranchKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate) AS BranchKey,
		[dbo].[Fn_GetBranchName]([dbo].[Fn_BranchKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate) ) AS BranchName,
		B.BirthDay AS BirthDay,
		B.Gender AS Gender,
		B.IssueID AS IssueID,
		B.IssueDate AS IssueDate,
		B.IssuePlace AS IssuePlace,
		B.HomeTown AS HomeTown,
		A.WorkingStatus AS WorkingStatus,
		A.StartingDate AS StartingDate,
		A.LeavingTryDate AS LeavingTryDate,
		A.LeavingDate AS LeavingDate,
		A.CompanyPhone AS CompanyPhone,
		A.CompanyEmail AS CompanyEmail,
		B.BankCode AS BankCode,
		B.AddressBank AS AddressBank,
		[dbo].[Fn_GetAccountCodeEmployee](A.TeamKey) AS AccountCode,
		[dbo].[Fn_GetAccountCode2Employee](A.TeamKey) AS AccountCode2,
		A.OverTime AS OverTime,
		A.ScoreStock AS ScoreStock,
        '' AS ReportTo
		FROM HRM_Employee A
		LEFT JOIN [dbo].[SYS_Personal]  B ON B.ParentKey =A.EmployeeKey
		WHERE A.RecordStatus <> 99 AND A.Slug <>1
		AND A.EmployeeKey = @EmployeeKey
		) A
LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
LEFT JOIN [dbo].SYS_Department C ON C.DepartmentKey=B.DepartmentKey
LEFT JOIN [dbo].SYS_Branch D ON D.BranchKey=C.BranchKey 
WHERE 1=1 
ORDER BY D.Rank,C.Rank,B.Rank, LEN(A.EmployeeID),A.EmployeeID

";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable MUCLUONG(DateTime Date, string EmployeeKey)
        {
            DateTime zDate = new DateTime(Date.Year, Date.Month, Date.Day, 23, 59, 59);
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT AutoKey,CategoryID,CategoryName,Value,FromDate,ToDate,Description FROM [dbo].[HRM_SalaryDefault]
WHERE RecordStatus <> 99 AND EmployeeKey= @EmployeeKey 
AND FromDate <= @Date
ORDER BY FromDate DESC,ToDate ,CategoryID
";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Date", SqlDbType.DateTime).Value = zDate;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable TRICHLUONG(DateTime Date,string EmployeeKey)
        {
            DateTime zDate = new DateTime(Date.Year, Date.Month, Date.Day, 23, 59, 59);
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT AutoKey,CategoryID,CategoryName,
[dbo].HRM_LayGiaTri_Parametter(FromDate,CategoryID) AS Value,
FromDate,ToDate,Description FROM [dbo].[HRM_Employee_FeeDefault]
WHERE RecordStatus <> 99 AND EmployeeKey=@EmployeeKey 
AND FromDate <= @Date
ORDER BY FromDate DESC,ToDate ,CategoryID
";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Date", SqlDbType.DateTime).Value = zDate;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable CONNHO(DateTime Date,string EmployeeKey)
        {
            DateTime zDate = new DateTime(Date.Year, Date.Month, Date.Day, 23, 59, 59);
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT AutoKey,CategoryID,CategoryName,Number,FromDate,ToDate,Description FROM[dbo].[HRM_FeeChildren] 
WHERE RecordStatus <> 99 AND EmployeeKey= @EmployeeKey
AND FromDate <= @Date 
ORDER BY FromDate DESC,ToDate ,CategoryID
";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Date", SqlDbType.DateTime).Value = zDate;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable LICHSUCONGTAC(DateTime Date,string EmployeeKey)
        {
            DateTime zDate = new DateTime(Date.Year, Date.Month, Date.Day, 23, 59, 59);
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT HistoryKey,StartingDate,LeavingDate,BranchName,DepartmentName,TeamName,PositionName,Description FROM   [dbo].[HRM_WorkingHistory]
WHERE RecordStatus <> 99 AND EmployeeKey=@EmployeeKey 
AND StartingDate <= @Date
ORDER BY StartingDate DESC,LeavingDate 
";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Date", SqlDbType.DateTime).Value = zDate;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }


        //Xem theo nhóm
        public static DataTable PHANBO_CHUCVU(DateTime Date,int BranchKey, int DepartmentKey, int TeamKey, int  PositionKey)
        {

            DateTime zFromDate = new DateTime(Date.Year, Date.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);


            string Fillter = "";
            if (BranchKey > 0)
            {
                Fillter += " AND D.BranchKey = @BranchKey";
            }
            if (DepartmentKey > 0)
            {
                Fillter += " AND C.DepartmentKey = @DepartmentKey";
            }
            if (TeamKey > 0)
            {
                Fillter += " AND A.TeamKey = @TeamKey";
            }
            if (PositionKey > 0)
            {
                Fillter += " AND A.PositionKey = @PositionKey";
            }

            DataTable zTable = new DataTable();
            string zSQL = @"

--declare @FromDate datetime ='2021-01-01 00:00:00'
--declare @ToDate datetime ='2021-01-31 23:59:59'
CREATE TABLE #Temp
(
EmployeeKey nvarchar(500),
PositionKey int ,
TeamKey int
)

INSERT INTO  #Temp
SELECT A.EmployeeKey,A.PositionKey,A.TeamKey
FROM
		(
		SELECT 
		A.EmployeeKey AS EmployeeKey,
		[dbo].[Fn_PositionKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate) AS PositionKey,
		[dbo].[Fn_TeamKeyWorkingHistory](A.EmployeeKey,@FromDate,@ToDate) AS TeamKey
		FROM HRM_Employee A
		LEFT JOIN [dbo].[SYS_Personal]  B ON B.ParentKey =A.EmployeeKey
		WHERE A.RecordStatus <> 99 AND A.Slug <>1
		AND (A.LeavingDate>@ToDate OR LeavingDate IS NULL   )
		) A

SELECT PositionID +'|'+PositionName +'|'+ CAST(PositionRank AS NVARCHAR(10)) AS LeftColumn,
DepartmentName +'|'+TeamName AS HeaderColumn,
Amount
FROM (
		SELECT 
		A.PositionKey,
		F.PositionID,
		F.PositionName,
		C.DepartmentKey,
		C.DepartmentID,
		C.DepartmentName,
		A.TeamKey,
		B.TeamID,
		B.TeamName,
		F.Rank AS PositionRank,
		D.Rank AS BranchRank,
		C.Rank AS DepartmentRank,
		B.Rank AS TeamRank,
		COUNT (A.PositionKey) AS Amount
		FROM #Temp A
		LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
		LEFT JOIN [dbo].SYS_Department C ON C.DepartmentKey=B.DepartmentKey
		LEFT JOIN [dbo].SYS_Branch D ON D.BranchKey=C.BranchKey 
		LEFT JOIN DBO.SYS_Position F ON F.PositionKey=A.PositionKey
        WHERE 1=1 @Parameter
		GROUP BY A.PositionKey,F.PositionID,F.PositionName,
		A.TeamKey,B.TeamID,B.TeamName,
		C.DepartmentKey,C.DepartmentID,C.DepartmentName,
		F.Rank,D.Rank,C.Rank,B.Rank
		) X
		ORDER BY BranchRank,DepartmentRank,TeamRank
DROP TABLE #Temp

";
            zSQL = zSQL.Replace("@Parameter", Fillter);
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BranchKey;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = PositionKey;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        #endregion
    }
}
