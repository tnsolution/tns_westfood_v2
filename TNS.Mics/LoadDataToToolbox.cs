﻿using ComponentFactory.Krypton.Toolkit;
using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using TN.Connect;

namespace TNS.Misc
{
    public class LoadDataToToolbox
    {
        private static string zConnectionString = ConnectDataBase.ConnectionString;

        public static void KryptonComboBox(KryptonComboBox CB, string SQL, string IsHaveFirstItem)
        {
            string nSQL;
            try
            {
                nSQL = SQL;

                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(nSQL, zConnect);
                SqlDataReader zReader = zCommand.ExecuteReader();

                //----------------------------------------------------------

                ArrayList ListItems = new ArrayList();
                int n = zReader.FieldCount;
                TN_Item li;

                if (IsHaveFirstItem.Length > 0)
                {
                    li = new TN_Item();
                    li.Value = 0;
                    li.Name = IsHaveFirstItem;
                    ListItems.Add(li);
                }
                while (zReader.Read())
                {

                    li = new TN_Item();
                    int nValue = 0;
                    if (int.TryParse(zReader[0].ToString(), out nValue))
                        li.Value = zReader[0];
                    else
                        li.Value = zReader[0].ToString();

                    if (n == 2)
                        li.Name = zReader[1].ToString().Trim();
                    else
                    {
                        li.Name = "";
                        for (int i = 1; i < n; i++)
                        {
                            li.Name = li.Name + zReader[i].ToString().Trim();
                            if (i < n - 1)
                                li.Name = li.Name + "  ";
                        }
                    }
                    ListItems.Add(li);
                }
                zReader.Close();
                zCommand.Dispose();
                zConnect.Close();
                CB.DataSource = ListItems;
                if (ListItems.Count > 0)
                {
                    CB.DisplayMember = "Name";
                    CB.ValueMember = "Value";
                }

            }
            catch (Exception e)
            {
                MessageBox.Show("Error: " + e);
            }
        }
        public static void ComboBoxData(ComboBox CB, string SQL, string IsHaveFirstItem)
        {
            string nSQL;
            try
            {
                nSQL = SQL;

                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(nSQL, zConnect);
                SqlDataReader zReader = zCommand.ExecuteReader();

                //----------------------------------------------------------

                ArrayList ListItems = new ArrayList();
                int n = zReader.FieldCount;
                TN_Item li;

                if (IsHaveFirstItem.Length > 0)
                {
                    li = new TN_Item();
                    li.Value = 0;
                    li.Name = IsHaveFirstItem;
                    ListItems.Add(li);
                }
                while (zReader.Read())
                {

                    li = new TN_Item();
                    int nValue = 0;
                    if (int.TryParse(zReader[0].ToString(), out nValue))
                        li.Value = zReader[0];
                    else
                        li.Value = zReader[0].ToString();

                    if (n == 2)
                        li.Name = zReader[1].ToString().Trim();
                    else
                    {
                        li.Name = "";
                        for (int i = 1; i < n; i++)
                        {
                            li.Name = li.Name + zReader[i].ToString().Trim();
                            if (i < n - 1)
                                li.Name = li.Name + "  ";
                        }
                    }
                    ListItems.Add(li);
                }
                zReader.Close();
                zCommand.Dispose();
                zConnect.Close();
                CB.DataSource = ListItems;
                if (ListItems.Count > 0)
                {
                    CB.DisplayMember = "Name";
                    CB.ValueMember = "Value";
                }

            }
            catch (Exception e)
            {
                MessageBox.Show("Error: " + e);
            }
        }

        public static void ComboBoxData(ComboBox CB, int Month, int Year)
        {
            ArrayList ListItems = new ArrayList();
            int n = 12;
            int nMonthFinacial = 1;
            TN_Item li;
            for (int i = Month; i <= n; i++)
            {
                li = new TN_Item();
                li.Value = nMonthFinacial;
                if (i > 9)
                    li.Name = i.ToString() + "/" + Year.ToString();
                else
                    li.Name = "0" + i.ToString() + "/" + Year.ToString();

                ListItems.Add(li);

                nMonthFinacial++;
            }
            if (Month > 1)
            {
                Year = Year + 1;
                for (int i = 1; i < Month; i++)
                {
                    li = new TN_Item();
                    li.Value = nMonthFinacial;
                    if (i > 9)
                        li.Name = i.ToString() + "/" + Year.ToString();
                    else
                        li.Name = "0" + i.ToString() + "/" + Year.ToString();

                    ListItems.Add(li);
                    nMonthFinacial++;
                }
            }

            if (ListItems.Count > 0)
            {
                CB.DataSource = ListItems;
                CB.DisplayMember = "Name";
                CB.ValueMember = "Value";
            }
        }
        public static void ComboBoxData(ComboBox CB, string SQL, bool IsHaveFirstItem)
        {
            string zSQL;
            try
            {
                zSQL = SQL;

                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataReader zReader = zCommand.ExecuteReader();

                //----------------------------------------------------------

                ArrayList ListItems = new ArrayList();
                int n = zReader.FieldCount;
                TN_Item li;

                if (IsHaveFirstItem)
                {
                    li = new TN_Item();
                    li.Value = 0;
                    li.Name = "    ";
                    ListItems.Add(li);
                }
                while (zReader.Read())
                {

                    li = new TN_Item();
                    int nValue = 0;
                    if (int.TryParse(zReader[0].ToString(), out nValue))
                        li.Value = zReader[0];
                    else
                        li.Value = zReader[0].ToString();

                    if (n == 2)
                        li.Name = zReader[1].ToString().Trim();
                    else
                    {
                        li.Name = "";
                        for (int i = 1; i < n; i++)
                        {
                            li.Name = li.Name + zReader[i].ToString().Trim();
                            if (i < n - 1)
                                li.Name = li.Name + "  ";
                        }
                    }
                    ListItems.Add(li);
                }
                zReader.Close();
                zCommand.Dispose();
                zConnect.Close();
                CB.DataSource = ListItems;
                if (ListItems.Count > 0)
                {
                    CB.DisplayMember = "Name";
                    CB.ValueMember = "Value";
                }

            }
            catch (Exception e)
            {
                MessageBox.Show("Error: " + e);
            }
        }
        public static void ComboBoxData(ComboBox CB, TN_Item ItemFirst, string SQL)
        {
            string zSQL;
            try
            {
                zSQL = SQL;

                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataReader zReader = zCommand.ExecuteReader();

                //----------------------------------------------------------

                ArrayList ListItems = new ArrayList();
                int n = zReader.FieldCount;
                TN_Item li;
                ListItems.Add(ItemFirst);
                while (zReader.Read())
                {

                    li = new TN_Item();
                    int nValue = 0;
                    if (int.TryParse(zReader[0].ToString(), out nValue))
                        li.Value = zReader[0];
                    else
                        li.Value = zReader[0].ToString();

                    if (n == 2)
                        li.Name = zReader[1].ToString().Trim();
                    else
                    {
                        li.Name = "";
                        for (int i = 1; i < n; i++)
                        {
                            li.Name = li.Name + zReader[i].ToString().Trim();
                            if (i < n - 1)
                                li.Name = li.Name + "  ";
                        }
                    }
                    ListItems.Add(li);
                }
                zReader.Close();
                zCommand.Dispose();
                zConnect.Close();

                CB.DataSource = ListItems;
                if (ListItems.Count > 0)
                {
                    CB.DisplayMember = "Name";
                    CB.ValueMember = "Value";
                }

            }
            catch (Exception e)
            {
                MessageBox.Show("Error: " + e);
            }
        }
        public static void ComboBoxData(ComboBox CB, string SQL, int MaxFirstCol, bool IsHaveFirstItem)
        {
            string zSQL;
            try
            {
                zSQL = SQL;

                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataReader zReader = zCommand.ExecuteReader();

                //----------------------------------------------------------

                ArrayList ListItems = new ArrayList();
                int n = zReader.FieldCount;
                TN_Item li;

                if (IsHaveFirstItem)
                {
                    li = new TN_Item();
                    li.Value = 0;
                    li.Name = "    ";
                    ListItems.Add(li);
                }
                while (zReader.Read())
                {

                    li = new TN_Item();
                    int nValue = 0;
                    if (int.TryParse(zReader[0].ToString(), out nValue))
                        li.Value = zReader[0];
                    else
                        li.Value = zReader[0].ToString();
                    if (n == 2)
                        li.Name = zReader[1].ToString().Trim();
                    else
                    {
                        li.Name = "";
                        for (int i = 1; i < n; i++)
                        {
                            if (i == 1)
                                li.Name = zReader[i].ToString().Trim().PadRight(MaxFirstCol, ' ') + ":";
                            else
                                li.Name = li.Name + zReader[i].ToString().Trim();

                            if (i < n - 1)
                                li.Name = li.Name + "  ";
                        }
                    }
                    ListItems.Add(li);
                }
                zReader.Close();
                zCommand.Dispose();
                zConnect.Close();
                CB.DataSource = ListItems;
                if (ListItems.Count > 0)
                {
                    CB.DisplayMember = "Name";
                    CB.ValueMember = "Value";
                }

            }
            catch (Exception e)
            {
                MessageBox.Show("Error: " + e);
            }
        }

        public static void ComboBoxData(ComboBox CB, DataTable ListRecord, bool IsHaveFirstItem, int FieldValue, int FieldName)
        {
            try
            {
                TN_Item li;
                ArrayList ListItems = new ArrayList();
                if (IsHaveFirstItem)
                {
                    li = new TN_Item();
                    li.Value = 0;
                    li.Name = "    ";
                    ListItems.Add(li);
                }

                int n = ListRecord.Columns.Count;
                foreach (DataRow zRow in ListRecord.Rows)
                {

                    li = new TN_Item();
                    int nValue = 0;
                    if (int.TryParse(zRow[FieldValue].ToString(), out nValue))
                        li.Value = zRow[FieldValue];
                    else
                        li.Value = zRow[FieldValue].ToString();

                    li.Name = zRow[FieldName].ToString().Trim();

                    ListItems.Add(li);
                }

                CB.DataSource = ListItems;
                if (ListItems.Count > 0)
                {
                    CB.DisplayMember = "Name";
                    CB.ValueMember = "Value";
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Error: " + e);
            }
        }
        public static void ComboBoxData(DataGridViewComboBoxColumn CB, string SQL, bool IsHaveFirstItem)
        {
            string zSQL;
            try
            {
                zSQL = SQL;

                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataReader zReader = zCommand.ExecuteReader();

                //----------------------------------------------------------

                ArrayList ListItems = new ArrayList();
                int n = zReader.FieldCount;
                TN_Item li;

                if (IsHaveFirstItem)
                {
                    li = new TN_Item();
                    li.Value = 0;
                    li.Name = "    ";
                    ListItems.Add(li);
                }
                while (zReader.Read())
                {

                    li = new TN_Item();
                    li.Value = zReader[0];
                    if (n == 2)
                        li.Name = zReader[1].ToString().Trim();
                    else
                    {
                        li.Name = "";
                        for (int i = 1; i < n; i++)
                        {
                            li.Name = li.Name + zReader[i].ToString().Trim();
                            if (i < n - 1)
                                li.Name = li.Name + "  ";
                        }
                    }
                    ListItems.Add(li);
                }
                zReader.Close();
                zCommand.Dispose();
                zConnect.Close();
                CB.DataSource = ListItems;
                if (ListItems.Count > 0)
                {
                    CB.DisplayMember = "Name";
                    CB.ValueMember = "Value";
                }

            }
            catch (Exception e)
            {
                MessageBox.Show("Error: " + e);
            }
        }

        public static void KryptonTextBox(KryptonTextBox TB, string SQL)
        {
            string zSQL = SQL;
            AutoCompleteStringCollection Items = new AutoCompleteStringCollection();

            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataReader zReader = zCommand.ExecuteReader();

                //----------------------------------------------------------

                int n = zReader.FieldCount;

                while (zReader.Read())
                {
                    Items.Add(zReader[0].ToString().Trim());
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception ex)
            {
                string zMessage = ex.ToString();
            }
            zConnect.Close();

            TB.AutoCompleteCustomSource = Items;
            TB.AutoCompleteMode = AutoCompleteMode.Suggest;
            TB.AutoCompleteSource = AutoCompleteSource.CustomSource;

        }

        public static void AutoCompleteTextBox(TextBox TB, string SQL)
        {
            string zSQL = SQL;
            AutoCompleteStringCollection Items = new AutoCompleteStringCollection();

            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataReader zReader = zCommand.ExecuteReader();

                //----------------------------------------------------------

                int n = zReader.FieldCount;

                while (zReader.Read())
                {
                    Items.Add(zReader[0].ToString().Trim());
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception ex)
            {
                string zMessage = ex.ToString();
            }
            zConnect.Close();

            TB.AutoCompleteCustomSource = Items;
            TB.AutoCompleteMode = AutoCompleteMode.Suggest;
            TB.AutoCompleteSource = AutoCompleteSource.CustomSource;

        }
        public static void AutoCompleteTextBox(TextBox TB, DataTable ListRecord)
        {
            AutoCompleteStringCollection Items = new AutoCompleteStringCollection();
            foreach (DataRow zRow in ListRecord.Rows)
            {
                Items.Add(zRow[0].ToString().Trim());
            }

            TB.AutoCompleteCustomSource = Items;
            TB.AutoCompleteMode = AutoCompleteMode.Suggest;
            TB.AutoCompleteSource = AutoCompleteSource.CustomSource;

        }
        public static void AutoCompleteTextBox(DataGridViewTextBoxEditingControl TB, string SQL)
        {
            string zSQL = SQL;
            AutoCompleteStringCollection Items = new AutoCompleteStringCollection();

            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
            SqlDataReader zReader = zCommand.ExecuteReader();

            //----------------------------------------------------------

            int n = zReader.FieldCount;

            while (zReader.Read())
            {
                Items.Add(zReader[0].ToString().Trim());
            }
            zReader.Close();
            zCommand.Dispose();
            zConnect.Close();

            TB.AutoCompleteCustomSource = Items;
            TB.AutoCompleteMode = AutoCompleteMode.Suggest;
            TB.AutoCompleteSource = AutoCompleteSource.CustomSource;

        }
        public static void AutoCompleteTextBox(DataGridViewTextBoxEditingControl TB, string SQL, int NumberColumns)
        {
            string zSQL = SQL;
            AutoCompleteStringCollection Items = new AutoCompleteStringCollection();

            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
            SqlDataReader zReader = zCommand.ExecuteReader();

            while (zReader.Read())
            {
                string strItem = zReader[0].ToString().Trim().PadRight(15, ' ') + " : ";
                for (int i = 1; i < NumberColumns; i++)
                {
                    strItem += zReader[i].ToString().Trim() + " ";

                }
                Items.Add(strItem);
            }
            zReader.Close();
            zCommand.Dispose();
            zConnect.Close();

            TB.AutoCompleteCustomSource = Items;
            TB.AutoCompleteMode = AutoCompleteMode.Suggest;
            TB.AutoCompleteSource = AutoCompleteSource.CustomSource;

        }
        public static void AutoCompleteTextBoxMutiColumn(TextBox TB, string SQL)
        {
            string zSQL = SQL;
            AutoCompleteStringCollection Items = new AutoCompleteStringCollection();

            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
            SqlDataReader zReader = zCommand.ExecuteReader();

            //----------------------------------------------------------

            int n = zReader.FieldCount;

            while (zReader.Read())
            {
                Items.Add(zReader[0].ToString().Trim() + " : " + zReader[1].ToString().Trim());
            }
            zReader.Close();
            zCommand.Dispose();
            zConnect.Close();

            TB.AutoCompleteCustomSource = Items;
            TB.AutoCompleteMode = AutoCompleteMode.Suggest;
            TB.AutoCompleteSource = AutoCompleteSource.CustomSource;


        }
        public static void AutoCompleteComboBox(ComboBox CB, string SQL)
        {
            string zSQL = SQL;
            AutoCompleteStringCollection Items = new AutoCompleteStringCollection();

            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
            SqlDataReader zReader = zCommand.ExecuteReader();

            //----------------------------------------------------------

            int n = zReader.FieldCount;

            while (zReader.Read())
            {
                Items.Add(zReader[0].ToString().Trim());
            }
            zReader.Close();
            zCommand.Dispose();
            zConnect.Close();

            CB.AutoCompleteCustomSource = Items;
            CB.AutoCompleteMode = AutoCompleteMode.Suggest;
            CB.AutoCompleteSource = AutoCompleteSource.ListItems;

        }


    }
}
