﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using TN.Connect;

namespace TNS.LOG
{
    public class Login_Data
    {
        public static DataTable User()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT UserKey,UserName,[dbo].[Fn_GetUserActionCurrent](UserKey) AS Action FROM SYS_User  ORDER BY Action ,UserName";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable Login_Log(DateTime FromDate,DateTime ToDate, string UserKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT DateLogin,ComputerLogin,IPAddress,WindownsName,Description ,TypeLog
FROM [dbo].[LOG_Login] 
WHERE DateLogin BETWEEN @FromDate AND @ToDate
AND UserKey =@UserKey
ORDER BY DateLogin DESC 
            ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                if (UserKey.Length == 36)
                    zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(UserKey);
                else
                    zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable Login_Page(DateTime FromDate, DateTime ToDate, string UserKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT COUNT(*),ActionName FROM [dbo].[LOG_Application]
WHERE ActionType=2 AND CreatedOn BETWEEN @FromDate AND @ToDate
AND UserKey= @UserKey
GROUP BY ActionName
ORDER BY COUNT(*) DESC 
            ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                if (UserKey.Length == 36)
                    zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(UserKey);
                else
                    zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
    }
}
