﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TN.Libary.Inventory;
using TN.Library.Inventory;
using TN.Library.Miscellaneous;

namespace TN_WinApp
{
    public partial class Frm_Product : Form
    {
        private string _Parent = "";
        private string _Name = "";
        private string _Key = "";

        public string Parent
        {
            get
            {
                return _Parent;
            }

            set
            {
                _Parent = value;
            }
        }

        public string Name
        {
            get
            {
                return _Name;
            }

            set
            {
                _Name = value;
            }
        }

        public Frm_Product()
        {
            InitializeComponent();
            btn_Save.Click += Btn_Save_Click;
            btn_New.Click += Btn_New_Click;
            btn_Del.Click += Btn_Del_Click;
            btn_Search.Click += Btn_Search_Click;
            LV_List.Click += LV_List_Click;
        }
        private void Frm_Product_Load(object sender, EventArgs e)
        {
            Label();
            LV_Layout(LV_List);
            LV_LoadData();
            LoadDataToToolbox.ComboBoxData(cbo_Unit, Product_Unit_Data.List_Combobox_Unit(), false, 0, 1);
            LoadDataToToolbox.ComboBoxData(cbo_Category, Product_Category_Data.List_Combobox_Category(), false, 0, 1);
        }
        private void Label()
        {
            lbl_Title.Text = "CÀI ĐẶT SẢN PHẨM - " +_Name;
        }
        private void LoadData()
        {

            Product_Info zinfo = new Product_Info(_Key);
            cbo_Unit.SelectedValue = zinfo.BasicUnit;
            cbo_Category.SelectedValue = zinfo.CategoryKey;
            txt_ProductName.Text = zinfo.ProductName.ToString();
            txt_ProductID.Text = zinfo.ProductID.ToString();
            txt_Description.Text = zinfo.Description;
            txt_Rank.Text = zinfo.Rank.ToString();
        }
        private void SetDefault()
        {
            _Key = "";
            cbo_Unit.SelectedIndex = 0;
            cbo_Category.SelectedValue = 2;
            txt_ProductName.Text = "";
            txt_ProductID.Text = "";
            txt_Description.Text = "";
            txt_Rank.Text = "0";
        }

        #region[Event]
        private void Btn_Search_Click(object sender, EventArgs e)
        {

            DataTable zTable = Product_Data.List_Product_Of_Material_Search(_Parent,txt_Search.Text);
            LV_LoadDataSearch(zTable);
        }
        private void Btn_Save_Click(object sender, EventArgs e)
        {

            int zRank;
            if (!int.TryParse(txt_Rank.Text, out zRank))
            {
                MessageBox.Show("Vui lòng nhập đúng định dạng số!");
                return;
            }
            else
            {
                Product_Info zinfo = new Product_Info(_Key);

                zinfo.ProductID = txt_ProductID.Text.ToUpper();
                zinfo.ProductName = txt_ProductName.Text;
                zinfo.BasicUnit = int.Parse(cbo_Unit.SelectedValue.ToString());
                zinfo.CategoryKey = int.Parse(cbo_Category.SelectedValue.ToString());
                zinfo.Rank = int.Parse(txt_Rank.Text.Trim());
                zinfo.Parent = _Parent;
                zinfo.Description = txt_Description.Text.Trim();
                zinfo.Save();
                if (zinfo.Message.Substring(0, 2) == "20" || zinfo.Message.Substring(0, 2) == "11")
                {
                    MessageBox.Show("Cập nhật thành công!");
                    LV_LoadData();
                    SetDefault();
                }
                else
                {
                    MessageBox.Show("Cập nhật không thành công!Vui lòng kiểm tra lại!");
                }
            }


        }

        private void Btn_Del_Click(object sender, EventArgs e)
        {
            if (_Key == "")
            {
                MessageBox.Show("Chưa chọn thông tin!");
            }
            else
            {
                if (MessageBox.Show("Bạn có chắc xóa thông tin này ?.", "Thông báo", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    Product_Info zinfo = new Product_Info(_Key);
                    zinfo.Delete();
                    if (zinfo.Message.Substring(0, 2) == "30")
                    {
                        MessageBox.Show("Đã xóa !");
                        LV_LoadData();
                        SetDefault();
                    }
                    else
                    {
                        MessageBox.Show("Xóa không thành công!Vui lòng kiểm tra lại!");
                    }

                }
            }

        }


        private void Btn_New_Click(object sender, EventArgs e)
        {
            SetDefault();
        }
        #endregion
        private void LV_Layout(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "No";
            colHead.Width = 30;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên nguyên liệu";
            colHead.Width = 250;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            //colHead = new ColumnHeader();
            //colHead.Text = "Đơn vị";
            //colHead.Width = 150;
            //colHead.TextAlign = HorizontalAlignment.Center;
            //LV.Columns.Add(colHead);

            //colHead = new ColumnHeader();
            //colHead.Text = "Loại";
            //colHead.Width = 150;
            //colHead.TextAlign = HorizontalAlignment.Center;
            //LV.Columns.Add(colHead);

            //colHead = new ColumnHeader();
            //colHead.Text = "Diễn giải";
            //colHead.Width = 270;
            //colHead.TextAlign = HorizontalAlignment.Left;
            //LV.Columns.Add(colHead);
        }
        private void LV_LoadData()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LV_List;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            LV.Items.Clear();
            DataTable ztb = Product_Data.List_Product_Of_Material(_Parent);
            for (int i = 0; i < ztb.Rows.Count; i++)
            {
                DataRow nRow = ztb.Rows[i];

                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.Tag = nRow["ProductKey"];
                lvi.ForeColor = Color.Navy;

                lvi.BackColor = Color.White;
                lvi.ImageIndex = 0;


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["ProductID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["ProductName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                //lvsi = new ListViewItem.ListViewSubItem();
                //lvsi.Text = nRow["UnitName"].ToString().Trim();
                //lvi.SubItems.Add(lvsi);

                //lvsi = new ListViewItem.ListViewSubItem();
                //lvsi.Text = nRow["CategoryName"].ToString().Trim();
                //lvi.SubItems.Add(lvsi);

                //lvsi = new ListViewItem.ListViewSubItem();
                //lvsi.Text = nRow["Description"].ToString().Trim();
                //lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }

            this.Cursor = Cursors.Default;

        }
        private void LV_LoadDataSearch(DataTable In_Table)
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LV_List;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            LV.Items.Clear();
            for (int i = 0; i < In_Table.Rows.Count; i++)
            {
                DataRow nRow = In_Table.Rows[i];

                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.Tag = nRow["ProductKey"];
                lvi.ForeColor = Color.Navy;

                lvi.BackColor = Color.White;
                lvi.ImageIndex = 0;


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["ProductID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["ProductName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                //lvsi = new ListViewItem.ListViewSubItem();
                //lvsi.Text = nRow["UnitName"].ToString().Trim();
                //lvi.SubItems.Add(lvsi);

                //lvsi = new ListViewItem.ListViewSubItem();
                //lvsi.Text = nRow["CategoryName"].ToString().Trim();
                //lvi.SubItems.Add(lvsi);

                //lvsi = new ListViewItem.ListViewSubItem();
                //lvsi.Text = nRow["Description"].ToString().Trim();
                //lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);

                this.Cursor = Cursors.Default;
            }
        }

        private void LV_List_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < LV_List.Items.Count; i++)
            {
                if (LV_List.Items[i].Selected == true)
                {
                    LV_List.Items[i].BackColor = Color.LightBlue; // highlighted item
                }
                else
                {
                    LV_List.Items[i].BackColor = SystemColors.Window; // normal item
                }
            }
            _Key = LV_List.SelectedItems[0].Tag.ToString();
            LoadData();
        }

    }
}
