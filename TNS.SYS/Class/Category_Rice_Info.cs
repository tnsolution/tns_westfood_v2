﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;

namespace TNS.SYS
{
    public class Category_Rice_Info
    {

        #region [ Field Name ]
        private int _CategoryKey = 0;
        private string _CategoryName = "";
        private string _CategoryID = "";
        private string _ID_Rice = "";
        private string _ID_Number = "";
        private string _ID_Money = "";
        private int _Type = 0;
        private string _TypeName = "";
        private int _Rank = 0;
        private string _Description = "";
        private int _Slug = 1;
        private int _RecordStatus = 0;
        private DateTime? _CreatedOn = null;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime? _ModifiedOn = null;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _Message = "";
        #endregion

        #region [ Constructor Get Information ]
        public Category_Rice_Info()
        {
        }
        public Category_Rice_Info(int CategoryKey)
        {
            string zSQL = "SELECT * FROM [dbo].[HRM_Category_Rice] WHERE CategoryKey = @CategoryKey AND RecordStatus != 99 ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    _CategoryName = zReader["CategoryName"].ToString();
                    _CategoryID = zReader["CategoryID"].ToString();
                    _ID_Rice = zReader["ID_Rice"].ToString();
                    _ID_Number = zReader["ID_Number"].ToString();
                    _ID_Money = zReader["ID_Money"].ToString();
                    _Type = int.Parse(zReader["Type"].ToString());
                    _TypeName = zReader["TypeName"].ToString();
                    _Rank = int.Parse(zReader["Rank"].ToString());
                    _Description = zReader["Description"].ToString();
                    _Slug = int.Parse(zReader["Slug"].ToString());
                    _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString();
                    _CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    _ModifiedName = zReader["ModifiedName"].ToString();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Properties ]
        public int CategoryKey
        {
            get { return _CategoryKey; }
            set { _CategoryKey = value; }
        }
        public string CategoryName
        {
            get { return _CategoryName; }
            set { _CategoryName = value; }
        }
        public string ID_Number
        {
            get { return _ID_Number; }
            set { _ID_Number = value; }
        }
        public string ID_Money
        {
            get { return _ID_Money; }
            set { _ID_Money = value; }
        }
        public int Type
        {
            get { return _Type; }
            set { _Type = value; }
        }
        public string TypeName
        {
            get { return _TypeName; }
            set { _TypeName = value; }
        }
        public int Rank
        {
            get { return _Rank; }
            set { _Rank = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public int Slug
        {
            get { return _Slug; }
            set { _Slug = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime? CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime? ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                    return _Message.Substring(0, 3);
                else return "";
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public string CategoryID
        {
            get
            {
                return _CategoryID;
            }

            set
            {
                _CategoryID = value;
            }
        }

        public string ID_Rice
        {
            get
            {
                return _ID_Rice;
            }

            set
            {
                _ID_Rice = value;
            }
        }
        #endregion

        #region [ Constructor Update Information ]

        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = @"
         UPDATE [dbo].[HRM_Category_Rice] SET Rank = Rank + 1 WHERE Rank >= @Rank AND RecordStatus <> 99

         INSERT INTO [dbo].[HRM_Category_Rice] (
         CategoryName ,CategoryID,ID_Rice,ID_Number ,ID_Money ,Type ,TypeName ,Rank ,Description ,Slug ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) 
         VALUES ( 
         @CategoryName ,@CategoryID,@ID_Rice,@ID_Number ,@ID_Money ,@Type ,@TypeName ,@Rank ,@Description ,@Slug ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) ";
            zSQL += " SELECT '11' + CAST(CategoryKey AS NVARCHAR(50)) FROM HRM_Category_Rice WHERE CategoryKey = SCOPE_IDENTITY() ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName;
                zCommand.Parameters.Add("@CategoryID", SqlDbType.NVarChar).Value = _CategoryID;
                zCommand.Parameters.Add("@ID_Rice", SqlDbType.NVarChar).Value = ID_Rice;
                zCommand.Parameters.Add("@ID_Number", SqlDbType.NVarChar).Value = _ID_Number;
                zCommand.Parameters.Add("@ID_Money", SqlDbType.NVarChar).Value = _ID_Money;
                zCommand.Parameters.Add("@Type", SqlDbType.Int).Value = _Type;
                zCommand.Parameters.Add("@TypeName", SqlDbType.NVarChar).Value = _TypeName;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = _Slug;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zResult = zCommand.ExecuteScalar().ToString();
                if (zResult.Substring(0, 2) == "11")
                {
                    _CategoryKey = int.Parse(zResult.Substring(2));
                }
                else
                    CategoryKey = 0;
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "501 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO [dbo].[HRM_Category_Rice] ("
        + " CategoryKey ,CategoryName,CategoryID ,ID_Rice,ID_Number ,ID_Money ,Type ,TypeName ,Rank ,Description ,Slug ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) "
         + " VALUES ( "
         + "@CategoryKey ,@CategoryName,@CategoryID ,@ID_Rice,@ID_Number ,@ID_Money ,@Type ,@TypeName ,@Rank ,@Description ,@Slug ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName;
                zCommand.Parameters.Add("@CategoryID", SqlDbType.NVarChar).Value = _CategoryID;
                zCommand.Parameters.Add("@ID_Rice", SqlDbType.NVarChar).Value = ID_Rice;
                zCommand.Parameters.Add("@ID_Number", SqlDbType.NVarChar).Value = _ID_Number;
                zCommand.Parameters.Add("@ID_Money", SqlDbType.NVarChar).Value = _ID_Money;
                zCommand.Parameters.Add("@Type", SqlDbType.Int).Value = _Type;
                zCommand.Parameters.Add("@TypeName", SqlDbType.NVarChar).Value = _TypeName;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = _Slug;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE [dbo].[HRM_Category_Rice] SET "
                        + " CategoryName = @CategoryName,"
                        + " CategoryID =@CategoryID,"
                        + " ID_Rice =@ID_Rice,"
                        + " ID_Number = @ID_Number,"
                        + " ID_Money = @ID_Money,"
                        + " Type = @Type,"
                        + " TypeName = @TypeName,"
                        + " Rank = @Rank,"
                        + " Description = @Description,"
                        + " Slug = @Slug,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                       + " WHERE CategoryKey = @CategoryKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName;
                zCommand.Parameters.Add("@CategoryID", SqlDbType.NVarChar).Value = _CategoryID;
                zCommand.Parameters.Add("@ID_Rice", SqlDbType.NVarChar).Value = ID_Rice;
                zCommand.Parameters.Add("@ID_Number", SqlDbType.NVarChar).Value = _ID_Number;
                zCommand.Parameters.Add("@ID_Money", SqlDbType.NVarChar).Value = _ID_Money;
                zCommand.Parameters.Add("@Type", SqlDbType.Int).Value = _Type;
                zCommand.Parameters.Add("@TypeName", SqlDbType.NVarChar).Value = _TypeName;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = _Slug;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"         
UPDATE [dbo].[HRM_Category_Rice] SET Rank = Rank - 1 WHERE Rank > @Rank AND RecordStatus <> 99

UPDATE [dbo].[HRM_Category_Rice] Set RecordStatus = 99 WHERE CategoryKey = @CategoryKey

";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete_Object()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"         
UPDATE [dbo].[HRM_Category_Rice] SET Rank = Rank - 1 WHERE Rank > @Rank AND RecordStatus <> 99 

UPDATE [dbo].[HRM_Category_Rice] Set RecordStatus = 99 WHERE CategoryKey = @CategoryKey 

UPDATE [dbo].[HRM_Time_Rice] Set RecordStatus = 99 WHERE CategoryKey = @CategoryKey

";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM [dbo].[HRM_Category_Rice] WHERE CategoryKey = @CategoryKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
