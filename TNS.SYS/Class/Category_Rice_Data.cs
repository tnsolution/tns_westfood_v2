﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
using TNS.Misc;

namespace TNS.SYS
{
    public class Category_Rice_Data
    {
        public static DataTable Search(string Name)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT CategoryKey,CategoryID,CategoryName,TypeName,Rank,Description FROM HRM_Category_Rice WHERE RecordStatus != 99 ";
            if (Name.Trim().Length > 0)
            {
                zSQL += " AND (CategoryName LIKE @Name OR CategoryID LIKE @Name  ) ";
            }
            zSQL += "ORDER BY Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        //Lấy AutoKey cuối cùng



        #region[HRM_CodeReport]
        //Insert cột vào bảng chấm công - cơm cộng
        //Type= 4: thống kê cơm cộng
        public static bool Add_RicePlus(string ID,string CategoryName)
        {
            bool zResult = false;
            string zSQL = @"
DECLARE @Position int = 0
--1.Tạo cột theo mã cơm
--Lấy vị trí cuối cùng của mã cơm cộng
SELECT TOP 1 @Position =[Rank] FROM[dbo].[HRM_CodeReport]
WHERE Type = 4 AND RecordStatus<> 99 ORDER BY[Rank] DESC
IF(@Position = '')

    BEGIN
    SELECT @Position =[Rank] FROM[dbo].[HRM_CodeReport]
    WHERE ID = 'SNCC' AND RecordStatus<> 99             --Bắt đầu tên cơm cộng là kế mã số ngày chuyên cần
    END
--Chèn thay thế vị trí kế tiếp vừa tìm được
SET @Position = @Position + 1

UPDATE[dbo].[HRM_CodeReport] SET[Rank] =[Rank] + 1 WHERE [Rank] >= @Position AND RecordStatus<>99
INSERT INTO[dbo].[HRM_CodeReport] (ID, Name, Type, TypeName, Rank,RecordStatus,CreatedBy,CreatedName)
VALUES(@ID, @RiceName, 4, N'Cơm cộng theo báo cáo', @Position,0,@CreatedBy,@CreatedName)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ID", SqlDbType.NVarChar).Value = ID.Trim();// VD: PX0C
                zCommand.Parameters.Add("@RiceName", SqlDbType.NVarChar).Value = "Số phần "+CategoryName; // VD: Số phần Cơm công ty
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = SessionUser.UserLogin.Key;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = SessionUser.UserLogin.EmployeeName;
                zCommand.ExecuteNonQuery().ToString();
                zResult = true;
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                zResult = false;
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
        //Insert cột vào bảng chấm công - cơm trừ
        //Type= 4: thống kê cơm trừ
        public static bool Add_RiceMinus(string ID, string CategoryName)
        {
            bool zResult = false;
            string zSQL = @"
DECLARE @Position int =0 
--1.Tạo cột theo mã cơm
--Lấy vị trí cuối cùng của mã cơm trừ
SELECT TOP 1 @Position =[Rank] FROM [dbo].[HRM_CodeReport]
WHERE Type=5 AND RecordStatus <> 99 ORDER BY [Rank] DESC

IF(@Position='') 
	BEGIN
	SELECT TOP 1 @Position =[Rank] FROM [dbo].[HRM_CodeReport]
	WHERE Type=4 AND RecordStatus <> 99 ORDER BY [Rank] DESC        --Bắt đầu cơm cộng là kế mã cơm cộng báo cáo
	END
IF(@Position='') 
	BEGIN
	SELECT @Position =[Rank] FROM [dbo].[HRM_CodeReport]
	WHERE ID='SNCC' AND   RecordStatus <> 99                        --Bắt đầu cơm cộng là kế mã nghĩ luân phiên
	END
--Chèn thay thế vị trí kế tiếp vừa tìm được
SET @Position =@Position +1

UPDATE[dbo].[HRM_CodeReport] SET[Rank] =[Rank] + 1 WHERE [Rank] >= @Position AND RecordStatus<>99
INSERT INTO[dbo].[HRM_CodeReport] (ID, Name, Type, TypeName, Rank,RecordStatus,CreatedBy,CreatedName)
VALUES(@ID, @RiceName, 5, N'Cơm trừ theo báo cáo', @Position,0,@CreatedBy,@CreatedName)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ID", SqlDbType.NVarChar).Value = ID.Trim();// VD: PX0C
                zCommand.Parameters.Add("@RiceName", SqlDbType.NVarChar).Value = "Số phần " + CategoryName; // VD: Số phần Cơm công ty
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = SessionUser.UserLogin.Key;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = SessionUser.UserLogin.EmployeeName;
                zCommand.ExecuteNonQuery().ToString();
                zResult = true;
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                zResult = false;
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }

        public static bool Delete_Rice_HRM(string ID)
        {
            bool zResult = false;
            string zSQL = @"
DECLARE @Position int =0 
--1.Tạo cột theo mã cơm
--Lấy vị trí cuối cùng của mã cơm trừ
SELECT @Position =[Rank] FROM [dbo].[HRM_CodeReport]
WHERE Type=5 AND RecordStatus <> 99 AND ID = @ID

IF(@Position >0)
    UPDATE[dbo].[HRM_CodeReport] SET[Rank] =[Rank] -1 WHERE [Rank] > @Position AND RecordStatus<>99 
DELETE FROM [dbo].[HRM_CodeReport] WHERE ID = @ID";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ID", SqlDbType.NVarChar).Value = ID.Trim();// VD: PX0C
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = SessionUser.UserLogin.Key;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = SessionUser.UserLogin.EmployeeName;
                zCommand.ExecuteNonQuery().ToString();
                zResult = true;
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                zResult = false;
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }

        #endregion

        #region[SAL_ReportOffice]
        //Insert cột vào bảng VĂN PHÒNG - cơm cộng
        public static bool Add_RiceOffice(string ID_Number,string ID_Money, string CategoryRiceName)
        {
            bool zResult = false;
            string zSQL = @"
--Chú thích : CategoryKey
--1: SL cơm cộng /2:TT cơm cộng /3:SL cơm trừ/ 4:TT cơm trừ
DECLARE @Position int =0 
--1.Tạo cột theo mã cơm
--Lấy vị trí cuối cùng của mã cơm cộng
SELECT TOP 1 @Position =[Rank] FROM [SLR_CodeReportOffice]
WHERE CategoryKey=2 AND RecordStatus <> 99 ORDER BY [Rank] DESC --Lấy theo cột thành tiền vì Số Lượng trước thành tiền

IF(@Position='') 
	BEGIN
	SELECT @Position =[Rank] FROM [SLR_CodeReportOffice]
	WHERE ID='TCSX' AND   RecordStatus <> 99 --Bắt đầu cơm cộng là tổng công sản xuất
	END

--Chèn thay thế vị trí kế tiếp vừa tìm được
SET @Position =@Position +1
--Số lượng cơm
UPDATE [SLR_CodeReportOffice] SET [Rank]=[Rank] +1 WHERE [Rank] >=@Position AND RecordStatus <>99
INSERT INTO [SLR_CodeReportOffice] (ID,Name,Type,TypeName,CategoryKey,CategoryName,Rank,Publish,Publish_Close,RecordStatus,CreatedBy,CreatedName)
VALUES (@ID_Number,N'SỐ LƯỢNG',1,@RiceName,1,N'Số lượng cơm cộng',@Position,1,1,0,@CreatedBy,@CreatedName)
--Thành tiền cơm
SET @Position =@Position +1
UPDATE [SLR_CodeReportOffice] SET [Rank]=[Rank] +1 WHERE [Rank] >=@Position AND RecordStatus <>99
INSERT INTO [SLR_CodeReportOffice] (ID,Name,Type,TypeName,CategoryKey,CategoryName,Rank,Publish,Publish_Close,RecordStatus,CreatedBy,CreatedName)
VALUES (@ID_Money,N'THÀNH TIỀN',1,@RiceName,2,N'Thành tiền cơm cộng',@Position,1,1,0,@CreatedBy,@CreatedName)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ID_Number", SqlDbType.NVarChar).Value = ID_Number.Trim();
                zCommand.Parameters.Add("@ID_Money", SqlDbType.NVarChar).Value = ID_Money.Trim();
                zCommand.Parameters.Add("@RiceName", SqlDbType.NVarChar).Value = "TIỀN "+ CategoryRiceName; // VD: TIỀN CƠM CÔNG TY
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = SessionUser.UserLogin.Key;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = SessionUser.UserLogin.EmployeeName;
                zCommand.ExecuteNonQuery().ToString();
                zResult = true;
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                zResult = false;
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }

        //Insert cột vào bảng VĂN PHÒNG - cơm trừ
        public static bool Minus_RiceOffice(string ID_Number, string ID_Money, string CategoryRiceName)
        {
            bool zResult = false;
            string zSQL = @"
--Chú thích : CategoryKey
--1: SL cơm cộng /2:TT cơm cộng /3:SL cơm trừ/ 4:TT cơm trừ
DECLARE @Position int =0 
--1.Tạo cột theo mã cơm
--Lấy vị trí cuối cùng của mã cơm cộng
SELECT TOP 1 @Position =[Rank] FROM [SLR_CodeReportOffice]
WHERE CategoryKey=4 AND RecordStatus <> 99 ORDER BY [Rank] DESC --Lấy theo cột thành tiền vì Số Lượng trước thành tiền

IF(@Position='') 
	BEGIN
	SELECT @Position =[Rank] FROM [SLR_CodeReportOffice]
	WHERE ID='TNCN' AND   RecordStatus <> 99 --Bắt đầu cơm trừ là tổng thuế thu nhập cá nhân
	END

--Chèn thay thế vị trí kế tiếp vừa tìm được
SET @Position =@Position +1
--Số lượng cơm
UPDATE [SLR_CodeReportOffice] SET [Rank]=[Rank] +1 WHERE [Rank] >=@Position AND RecordStatus <>99
INSERT INTO [SLR_CodeReportOffice] (ID,Name,Type,TypeName,CategoryKey,CategoryName,Rank,Publish,Publish_Close,RecordStatus,CreatedBy,CreatedName)
VALUES (@ID_Number,N'SỐ LƯỢNG ' + @RiceName,1,N'TRỪ THU NHẬP',3,N'Số lượng cơm trừ',@Position,1,1,0,@CreatedBy,@CreatedName)
--Thành tiền cơm
SET @Position =@Position +1
UPDATE [SLR_CodeReportOffice] SET [Rank]=[Rank] +1 WHERE [Rank] >=@Position AND RecordStatus <>99
INSERT INTO [SLR_CodeReportOffice] (ID,Name,Type,TypeName,CategoryKey,CategoryName,Rank,Publish,Publish_Close,RecordStatus,CreatedBy,CreatedName)
VALUES (@ID_Money,N'THÀNH TIỀN '+ @RiceName,1,N'TRỪ THU NHẬP',4,N'Thành tiền cơm trừ',@Position,1,1,0,@CreatedBy,@CreatedName)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ID_Number", SqlDbType.NVarChar).Value = ID_Number.Trim();
                zCommand.Parameters.Add("@ID_Money", SqlDbType.NVarChar).Value = ID_Money.Trim();
                zCommand.Parameters.Add("@RiceName", SqlDbType.NVarChar).Value = CategoryRiceName; // VD: TIỀN CƠM CÔNG TY
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = SessionUser.UserLogin.Key;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = SessionUser.UserLogin.EmployeeName;
                zCommand.ExecuteNonQuery().ToString();
                zResult = true;
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                zResult = false;
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
        public static bool Delete_Rice_Office(string ID_Number,string ID_Money)
        {
            bool zResult = false;
            string zSQL = @"
DECLARE @Position int =0 
--1.Tạo cột theo mã cơm
--Lấy vị trí của mã cơm trừ
SELECT @Position =[Rank] FROM [dbo].[SLR_CodeReportOffice]
WHERE  RecordStatus <> 99 AND ID = @ID_Number

IF(@Position >0)
    UPDATE[dbo].[SLR_CodeReportOffice] SET[Rank] =[Rank] -1 WHERE [Rank] > @Position AND RecordStatus<>99 
DELETE FROM [dbo].[SLR_CodeReportOffice] WHERE ID = @ID_Number

SET @Position =0

SELECT @Position =[Rank] FROM [dbo].[SLR_CodeReportOffice]
WHERE  RecordStatus <> 99 AND ID = @ID_Money

IF(@Position >0)
    UPDATE[dbo].[SLR_CodeReportOffice] SET[Rank] =[Rank] -1 WHERE [Rank] > @Position AND RecordStatus<>99 
DELETE FROM [dbo].[SLR_CodeReportOffice] WHERE ID = @ID_Money


";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ID_Number", SqlDbType.NVarChar).Value = ID_Number.Trim();// VD: PX0C
                zCommand.Parameters.Add("@ID_Money", SqlDbType.NVarChar).Value = ID_Money.Trim();// VD: PX0C
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = SessionUser.UserLogin.Key;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = SessionUser.UserLogin.EmployeeName;
                zCommand.ExecuteNonQuery().ToString();
                zResult = true;
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                zResult = false;
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
        #endregion

        #region[SAL_ReportSupport]
        //Insert cột vào bảng HỖ TRỢ - cơm cộng
        public static bool Add_RiceSupport(string ID_Number, string ID_Money, string CategoryRiceName)
        {
            bool zResult = false;
            string zSQL = @"
--Chú thích : CategoryKey
--1: SL cơm cộng /2:TT cơm cộng /3:SL cơm trừ/ 4:TT cơm trừ
DECLARE @Position int =0 
--1.Tạo cột theo mã cơm
--Lấy vị trí cuối cùng của mã cơm cộng
SELECT TOP 1 @Position =[Rank] FROM [SLR_CodeReportSupport]
WHERE CategoryKey=2 AND RecordStatus <> 99 ORDER BY [Rank] DESC --Lấy theo cột thành tiền vì Số Lượng trước thành tiền

IF(@Position='') 
	BEGIN
	SELECT @Position =[Rank] FROM [SLR_CodeReportSupport]
	WHERE ID='HTTD' AND   RecordStatus <> 99 --Bắt đầu cơm cộng là HỖ TRỢ THÁNG THẤP ĐIỂM
	END

--Chèn thay thế vị trí kế tiếp vừa tìm được
SET @Position =@Position +1
--Số lượng cơm
UPDATE [SLR_CodeReportSupport] SET [Rank]=[Rank] +1 WHERE [Rank] >=@Position AND RecordStatus <>99
INSERT INTO [SLR_CodeReportSupport] (ID,Name,Type,TypeName,CategoryKey,CategoryName,Rank,Publish,Publish_Close,RecordStatus,CreatedBy,CreatedName)
VALUES (@ID_Number,N'SỐ LƯỢNG',1,@RiceName,1,N'Số lượng cơm cộng',@Position,1,1,0,@CreatedBy,@CreatedName)
--Thành tiền cơm
SET @Position =@Position +1
UPDATE [SLR_CodeReportSupport] SET [Rank]=[Rank] +1 WHERE [Rank] >=@Position AND RecordStatus <>99
INSERT INTO [SLR_CodeReportSupport] (ID,Name,Type,TypeName,CategoryKey,CategoryName,Rank,Publish,Publish_Close,RecordStatus,CreatedBy,CreatedName)
VALUES (@ID_Money,N'THÀNH TIỀN',1,@RiceName,2,N'Thành tiền cơm cộng',@Position,1,1,0,@CreatedBy,@CreatedName)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ID_Number", SqlDbType.NVarChar).Value = ID_Number.Trim();
                zCommand.Parameters.Add("@ID_Money", SqlDbType.NVarChar).Value = ID_Money.Trim();
                zCommand.Parameters.Add("@RiceName", SqlDbType.NVarChar).Value = "TIỀN " + CategoryRiceName; // VD: TIỀN CƠM CÔNG TY
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = SessionUser.UserLogin.Key;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = SessionUser.UserLogin.EmployeeName;
                zCommand.ExecuteNonQuery().ToString();
                zResult = true;
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                zResult = false;
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }

        //Insert cột vào bảng HỖ TRỢ - cơm trừ
        public static bool Minus_RiceSupport(string ID_Number, string ID_Money, string CategoryRiceName)
        {
            bool zResult = false;
            string zSQL = @"
--Chú thích : CategoryKey
--1: SL cơm cộng /2:TT cơm cộng /3:SL cơm trừ/ 4:TT cơm trừ
DECLARE @Position int =0 
--1.Tạo cột theo mã cơm
--Lấy vị trí cuối cùng của mã cơm cộng
SELECT TOP 1 @Position =[Rank] FROM [SLR_CodeReportSupport]
WHERE CategoryKey=4 AND RecordStatus <> 99 ORDER BY [Rank] DESC --Lấy theo cột thành tiền vì Số Lượng trước thành tiền

IF(@Position='') 
	BEGIN
	SELECT @Position =[Rank] FROM [SLR_CodeReportSupport]
	WHERE ID='TNCN' AND   RecordStatus <> 99 --Bắt đầu cơm trừ là tổng thuế thu nhập cá nhân
	END

--Chèn thay thế vị trí kế tiếp vừa tìm được
SET @Position =@Position +1
--Số lượng cơm
UPDATE [SLR_CodeReportSupport] SET [Rank]=[Rank] +1 WHERE [Rank] >=@Position AND RecordStatus <>99
INSERT INTO [SLR_CodeReportSupport] (ID,Name,Type,TypeName,CategoryKey,CategoryName,Rank,Publish,Publish_Close,RecordStatus,CreatedBy,CreatedName)
VALUES (@ID_Number,N'SỐ LƯỢNG ' + @RiceName,1,N'TRỪ THU NHẬP',3,N'Số lượng cơm trừ',@Position,1,1,0,@CreatedBy,@CreatedName)
--Thành tiền cơm
SET @Position =@Position +1
UPDATE [SLR_CodeReportSupport] SET [Rank]=[Rank] +1 WHERE [Rank] >=@Position AND RecordStatus <>99
INSERT INTO [SLR_CodeReportSupport] (ID,Name,Type,TypeName,CategoryKey,CategoryName,Rank,Publish,Publish_Close,RecordStatus,CreatedBy,CreatedName)
VALUES (@ID_Money,N'THÀNH TIỀN '+ @RiceName,1,N'TRỪ THU NHẬP',4,N'Thành tiền cơm trừ',@Position,1,1,0,@CreatedBy,@CreatedName)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ID_Number", SqlDbType.NVarChar).Value = ID_Number.Trim();
                zCommand.Parameters.Add("@ID_Money", SqlDbType.NVarChar).Value = ID_Money.Trim();
                zCommand.Parameters.Add("@RiceName", SqlDbType.NVarChar).Value = CategoryRiceName; // VD: TIỀN CƠM CÔNG TY
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = SessionUser.UserLogin.Key;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = SessionUser.UserLogin.EmployeeName;
                zCommand.ExecuteNonQuery().ToString();
                zResult = true;
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                zResult = false;
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }

        public static bool Delete_Rice_Support(string ID_Number, string ID_Money)
        {
            bool zResult = false;
            string zSQL = @"
DECLARE @Position int =0 
--1.Tạo cột theo mã cơm
--Lấy vị trí của mã cơm trừ
SELECT @Position =[Rank] FROM [dbo].[SLR_CodeReportSupport]
WHERE  RecordStatus <> 99 AND ID = @ID_Number

IF(@Position >0)
    UPDATE[dbo].[SLR_CodeReportSupport] SET[Rank] =[Rank] -1 WHERE [Rank] > @Position AND RecordStatus<>99 
DELETE FROM [dbo].[SLR_CodeReportSupport] WHERE ID = @ID_Number

SET @Position =0

SELECT @Position =[Rank] FROM [dbo].[SLR_CodeReportSupport]
WHERE  RecordStatus <> 99 AND ID = @ID_Money

IF(@Position >0)
    UPDATE[dbo].[SLR_CodeReportSupport] SET[Rank] =[Rank] -1 WHERE [Rank] > @Position AND RecordStatus<>99 
DELETE FROM [dbo].[SLR_CodeReportSupport] WHERE ID = @ID_Money


";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ID_Number", SqlDbType.NVarChar).Value = ID_Number.Trim();// VD: PX0C
                zCommand.Parameters.Add("@ID_Money", SqlDbType.NVarChar).Value = ID_Money.Trim();// VD: PX0C
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = SessionUser.UserLogin.Key;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = SessionUser.UserLogin.EmployeeName;
                zCommand.ExecuteNonQuery().ToString();
                zResult = true;
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                zResult = false;
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
        #endregion

        #region[SAL_ReportWorker]
        //Insert cột vào bảng công nhân - cơm cộng
        public static bool Add_RiceWorker(string ID_Number, string ID_Money, string CategoryRiceName)
        {
            bool zResult = false;
            string zSQL = @"
--Chú thích : CategoryKey
--1: SL cơm cộng /2:TT cơm cộng /3:SL cơm trừ/ 4:TT cơm trừ
DECLARE @Position int =0 
--1.Tạo cột theo mã cơm
--Lấy vị trí cuối cùng của mã cơm cộng
SELECT TOP 1 @Position =[Rank] FROM [SLR_CodeReportWorker]
WHERE CategoryKey=2 AND RecordStatus <> 99 ORDER BY [Rank] DESC --Lấy theo cột thành tiền vì Số Lượng trước thành tiền

IF(@Position='') 
	BEGIN
	SELECT @Position =[Rank] FROM [SLR_CodeReportWorker]
	WHERE ID='HTTD' AND   RecordStatus <> 99 --Bắt đầu cơm cộng là HỖ TRỢ THÁNG THẤP ĐIỂM
	END

--Chèn thay thế vị trí kế tiếp vừa tìm được
SET @Position =@Position +1
--Số lượng cơm
UPDATE [SLR_CodeReportWorker] SET [Rank]=[Rank] +1 WHERE [Rank] >=@Position AND RecordStatus <>99
INSERT INTO [SLR_CodeReportWorker] (ID,Name,Type,TypeName,CategoryKey,CategoryName,Rank,Publish,Publish_Close,RecordStatus,CreatedBy,CreatedName)
VALUES (@ID_Number,N'SỐ LƯỢNG',1,@RiceName,1,N'Số lượng cơm cộng',@Position,1,1,0,@CreatedBy,@CreatedName)
--Thành tiền cơm
SET @Position =@Position +1
UPDATE [SLR_CodeReportWorker] SET [Rank]=[Rank] +1 WHERE [Rank] >=@Position AND RecordStatus <>99
INSERT INTO [SLR_CodeReportWorker] (ID,Name,Type,TypeName,CategoryKey,CategoryName,Rank,Publish,Publish_Close,RecordStatus,CreatedBy,CreatedName)
VALUES (@ID_Money,N'THÀNH TIỀN',1,@RiceName,2,N'Thành tiền cơm cộng',@Position,1,1,0,@CreatedBy,@CreatedName)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ID_Number", SqlDbType.NVarChar).Value = ID_Number.Trim();
                zCommand.Parameters.Add("@ID_Money", SqlDbType.NVarChar).Value = ID_Money.Trim();
                zCommand.Parameters.Add("@RiceName", SqlDbType.NVarChar).Value = "TIỀN " + CategoryRiceName; // VD: TIỀN CƠM CÔNG TY
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = SessionUser.UserLogin.Key;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = SessionUser.UserLogin.EmployeeName;
                zCommand.ExecuteNonQuery().ToString();
                zResult = true;
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                zResult = false;
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }

        //Insert cột vào bảng công nhân - cơm trừ
        public static bool Minus_RiceWorker(string ID_Number, string ID_Money, string CategoryRiceName)
        {
            bool zResult = false;
            string zSQL = @"
--Chú thích : CategoryKey
--1: SL cơm cộng /2:TT cơm cộng /3:SL cơm trừ/ 4:TT cơm trừ
DECLARE @Position int =0 
--1.Tạo cột theo mã cơm
--Lấy vị trí cuối cùng của mã cơm cộng
SELECT TOP 1 @Position =[Rank] FROM [SLR_CodeReportWorker]
WHERE CategoryKey=4 AND RecordStatus <> 99 ORDER BY [Rank] DESC --Lấy theo cột thành tiền vì Số Lượng trước thành tiền

IF(@Position='') 
	BEGIN
	SELECT @Position =[Rank] FROM [SLR_CodeReportWorker]
	WHERE ID='TNCN' AND   RecordStatus <> 99 --Bắt đầu cơm trừ là tổng thuế thu nhập cá nhân
	END

--Chèn thay thế vị trí kế tiếp vừa tìm được
SET @Position =@Position +1
--Số lượng cơm
UPDATE [SLR_CodeReportWorker] SET [Rank]=[Rank] +1 WHERE [Rank] >=@Position AND RecordStatus <>99
INSERT INTO [SLR_CodeReportWorker] (ID,Name,Type,TypeName,CategoryKey,CategoryName,Rank,Publish,Publish_Close,RecordStatus,CreatedBy,CreatedName)
VALUES (@ID_Number,N'SỐ LƯỢNG ' + @RiceName,1,N'TRỪ THU NHẬP',3,N'Số lượng cơm trừ',@Position,1,1,0,@CreatedBy,@CreatedName)
--Thành tiền cơm
SET @Position =@Position +1
UPDATE [SLR_CodeReportWorker] SET [Rank]=[Rank] +1 WHERE [Rank] >=@Position AND RecordStatus <>99
INSERT INTO [SLR_CodeReportWorker] (ID,Name,Type,TypeName,CategoryKey,CategoryName,Rank,Publish,Publish_Close,RecordStatus,CreatedBy,CreatedName)
VALUES (@ID_Money,N'THÀNH TIỀN '+ @RiceName,1,N'TRỪ THU NHẬP',4,N'Thành tiền cơm trừ',@Position,1,1,0,@CreatedBy,@CreatedName)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ID_Number", SqlDbType.NVarChar).Value = ID_Number.Trim();
                zCommand.Parameters.Add("@ID_Money", SqlDbType.NVarChar).Value = ID_Money.Trim();
                zCommand.Parameters.Add("@RiceName", SqlDbType.NVarChar).Value =CategoryRiceName; // VD: TIỀN CƠM CÔNG TY
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = SessionUser.UserLogin.Key;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = SessionUser.UserLogin.EmployeeName;
                zCommand.ExecuteNonQuery().ToString();
                zResult = true;
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                zResult = false;
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
        public static bool Delete_Rice_Worker(string ID_Number, string ID_Money)
        {
            bool zResult = false;
            string zSQL = @"
DECLARE @Position int =0 
--1.Tạo cột theo mã cơm
--Lấy vị trí của mã cơm trừ
SELECT @Position =[Rank] FROM [dbo].[SLR_CodeReportWorker]
WHERE  RecordStatus <> 99 AND ID = @ID_Number

IF(@Position >0)
    UPDATE[dbo].[SLR_CodeReportWorker] SET[Rank] =[Rank] -1 WHERE [Rank] > @Position AND RecordStatus<>99 
DELETE FROM [dbo].[SLR_CodeReportWorker] WHERE ID = @ID_Number

SET @Position =0

SELECT @Position =[Rank] FROM [dbo].[SLR_CodeReportWorker]
WHERE  RecordStatus <> 99 AND ID = @ID_Money

IF(@Position >0)
    UPDATE[dbo].[SLR_CodeReportWorker] SET[Rank] =[Rank] -1 WHERE [Rank] > @Position AND RecordStatus<>99 
DELETE FROM [dbo].[SLR_CodeReportWorker] WHERE ID = @ID_Money


";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ID_Number", SqlDbType.NVarChar).Value = ID_Number.Trim();// VD: PX0C
                zCommand.Parameters.Add("@ID_Money", SqlDbType.NVarChar).Value = ID_Money.Trim();// VD: PX0C
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = SessionUser.UserLogin.Key;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = SessionUser.UserLogin.EmployeeName;
                zCommand.ExecuteNonQuery().ToString();
                zResult = true;
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                zResult = false;
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
        #endregion
    }
}
