﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Connect;

namespace TNS.SYS
{
    public class User_Info
    {

        private string _UserKey = "";
        private string _UserName = "";
        private string _Password = "";
        private string _Description = "";
        private int _RecordStatus = 0;
        private string _ResetPassword = "";
        private DateTime _ExpireDate = DateTime.Now.AddYears(1);
        private DateTime _LastLoginDate = DateTime.Now;
        private int _FailedPasswordAttemptCount = 0;
        private int _IsLock = 0;

        private string _EmployeeKey = "";
        private string _EmployeeName = "";

        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _CreatedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private DateTime _ModifiedOn;
        private int _IsLogin = 0;

        private string _Message = "";
        #region [Properties ]

        public string Key
        {
            set { _UserKey = value; }
            get { return _UserKey; }
        }
        public string Name
        {
            set { _UserName = value; }
            get { return _UserName; }
        }
        public string Password
        {
            set
            {
                string nPassword = value;
                _Password = MyCryptography.HashPass(nPassword);
            }
            get { return _Password; }
        }
        public string Description
        {
            set { _Description = value; }
            get { return _Description; }
        }

        public DateTime ExpireDate
        {
            set { _ExpireDate = value; }
            get { return _ExpireDate; }
        }

        public DateTime LastLoginDate
        {
            set { _LastLoginDate = value; }
            get { return _LastLoginDate; }
        }
        public int FailedPasswordAttemptCount
        {
            set { _FailedPasswordAttemptCount = value; }
            get { return _FailedPasswordAttemptCount; }
        }

        public string EmployeeKey
        {
            set { _EmployeeKey = value; }
            get { return _EmployeeKey; }
        }
        public string EmployeeName
        {
            set { _EmployeeName = value; }
            get { return _EmployeeName; }
        }

        public string CreatedBy
        {
            set { _CreatedBy = value; }
            get { return _CreatedBy; }
        }
        public DateTime CreatedOn
        {
            set { _CreatedOn = value; }
            get { return _CreatedOn; }
        }

        public string ModifiedBy
        {
            set { _ModifiedBy = value; }
            get { return _ModifiedBy; }
        }
        public DateTime ModifiedOn
        {
            set { _ModifiedOn = value; }
            get { return _ModifiedOn; }
        }

        public string Message
        {
            set { _Message = value; }
            get { return _Message; }
        }

        public int RecordStatus
        {
            get
            {
                return _RecordStatus;
            }

            set
            {
                _RecordStatus = value;
            }
        }

        public int IsLock
        {
            get
            {
                return _IsLock;
            }

            set
            {
                _IsLock = value;
            }
        }

        public string ResetPassword
        {
            get
            {
                return _ResetPassword;
            }

            set
            {
                _ResetPassword = value;
            }
        }

        public string CreatedName
        {
            get
            {
                return _CreatedName;
            }

            set
            {
                _CreatedName = value;
            }
        }

        public string ModifiedName
        {
            get
            {
                return _ModifiedName;
            }

            set
            {
                _ModifiedName = value;
            }
        }

        public int IsLogin
        {
            get
            {
                return _IsLogin;
            }

            set
            {
                _IsLogin = value;
            }
        }
        #endregion

        public User_Info()
        {
        }

        #region [ Constructor Get Information ]


        public User_Info(string UserName)
        {
            string zSQL = " SELECT A.*, [dbo].[Fn_GetFullName](A.EmployeeKey) AS EmployeeName FROM SYS_User A "
                        + " LEFT JOIN HRM_Employee B ON A.EmployeeKey = B.EmployeeKey "
                        + " WHERE A.UserName = @UserName AND A.RecordStatus <> 99";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserName", SqlDbType.NVarChar).Value = UserName;

                SqlDataReader nReader = zCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();
                    _UserKey = nReader["UserKey"].ToString();
                    _UserName = nReader["UserName"].ToString();
                    _Password = nReader["Password"].ToString();
                    _Description = nReader["Description"].ToString();
                    _EmployeeName = nReader["EmployeeName"].ToString();
                    if (nReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = (int)nReader["RecordStatus"];
                    _ExpireDate = (DateTime)nReader["ExpireDate"];
                    if (nReader["IsLogin"] != DBNull.Value)
                    _IsLogin = (int)nReader["IsLogin"];
                    _EmployeeKey = nReader["EmployeeKey"].ToString();

                    if (nReader["LastLoginDate"] != DBNull.Value)
                        _LastLoginDate = (DateTime)nReader["LastLoginDate"];
                    _FailedPasswordAttemptCount = (int)nReader["FailedPasswordAttemptCount"];


                }

                //---- Close Connect SQL ----
                nReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        public void GetEmployee(string UserKey)
        {
            string zSQL = " SELECT * , [dbo].[Fn_GetFullName](EmployeeKey) AS EmployeeName FROM SYS_User WHERE UserKey = @UserKey AND RecordStatus <> 99";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(UserKey);
                SqlDataReader nReader = zCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();
                    _EmployeeKey = nReader["EmployeeKey"].ToString();
                    _UserKey = nReader["UserKey"].ToString();
                    _UserName = nReader["UserName"].ToString();
                    _Password = nReader["Password"].ToString();
                    _Description = nReader["Description"].ToString();

                    if (nReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = (int)nReader["RecordStatus"];
                    _ExpireDate = (DateTime)nReader["ExpireDate"];
                    if (nReader["IsLogin"] != DBNull.Value)
                        _IsLogin = (int)nReader["IsLogin"];

                    _EmployeeKey = nReader["EmployeeKey"].ToString();
                    _EmployeeName = nReader["EmployeeName"].ToString();
                    if (nReader["LastLoginDate"] != DBNull.Value)
                        _LastLoginDate = (DateTime)nReader["LastLoginDate"];
                    if (nReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)nReader["CreatedOn"];
                }
                //---- Close Connect SQL ----
                nReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        public void GetUser(string EmployeeKey)
        {
            string zSQL = " SELECT * FROM SYS_Users WHERE EmployeeKey = @EmployeeKey AND RecordStatus <> 99";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(EmployeeKey);
                SqlDataReader nReader = zCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();
                    _UserKey = nReader["UserKey"].ToString();
                    _UserName = nReader["UserName"].ToString();
                    _Password = nReader["Password"].ToString();
                    _Description = nReader["Description"].ToString();

                    if (nReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = (int)nReader["RecordStatus"];
                    _ExpireDate = (DateTime)nReader["ExpireDate"];
                    if (nReader["IsLogin"] != DBNull.Value)
                        _IsLogin = (int)nReader["IsLogin"];

                    _EmployeeKey = nReader["EmployeeKey"].ToString();
                    _EmployeeName = nReader["EmployeeName"].ToString();
                    if (nReader["LastLoginDate"] != DBNull.Value)
                        _LastLoginDate = (DateTime)nReader["LastLoginDate"];

                    if (nReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)nReader["CreatedOn"];

                    _FailedPasswordAttemptCount = (int)nReader["FailedPasswordAttemptCount"];

                }
                //---- Close Connect SQL ----
                nReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        #endregion



        #region [ Constructor Update Information ]
        public string UpdateFailedPass()
        {

            string zResult = "";

            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE SYS_User SET "
                        + " FailedPasswordAttemptCount = FailedPasswordAttemptCount+1"
                        + " WHERE UserKey = @UserKey AND RecordStatus <> 99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();

            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zCommand.CommandType = CommandType.Text;

                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_UserKey);

                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();

            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string UpdateDateLogin()
        {

            string zResult = "";

            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE SYS_Users SET "
                        + " LastLoginDate = getdate(),"
                        + " FailedPasswordAttemptCount = 0"
                        + " WHERE UserKey = @UserKey AND RecordStatus <> 99";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_UserKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string UpdatePassWord()
        {

            string zResult = "";

            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE SYS_User SET "
                        + " Password = @Password"
                        + " WHERE UserKey = @UserKey AND RecordStatus <> 99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();

            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zCommand.CommandType = CommandType.Text;

                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_UserKey);
                zCommand.Parameters.Add("@Password", SqlDbType.NVarChar).Value = _Password;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();

            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string UpdateFaildPassDefault()
        {

            string zResult = "";

            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE SYS_User SET "
                        + " FailedPasswordAttemptCount = @FailedPasswordAttemptCount"
                        + " WHERE UserKey = @UserKey AND RecordStatus <> 99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();

            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zCommand.CommandType = CommandType.Text;

                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_UserKey);
                zCommand.Parameters.Add("@FailedPasswordAttemptCount", SqlDbType.Int).Value = _FailedPasswordAttemptCount;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();

            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string UpdateIsLock()
        {

            string zResult = "";

            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE SYS_User SET "
                        + " RecordStatus = 53,"
                        + " FailedPasswordAttemptCount = 0"
                        + " WHERE UserKey = @UserKey AND RecordStatus <> 99";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_UserKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string nResult = ""; ;

            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE SYS_User SET "
                        + " UserName = @UserName,"
                        + " Description = @Description,"
                        + " RecordStatus = @RecordStatus,"
                        + " ExpireDate = @ExpireDate,"
                        + " EmployeeKey = @EmployeeKey, "
                        + " Islogin = @Islogin,"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName,"
                        + " ModifiedOn = getdate() "
                        + " WHERE UserKey = @UserKey";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();

            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;

                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_UserKey);
                zCommand.Parameters.Add("@UserName", SqlDbType.NVarChar).Value = _UserName;
                zCommand.Parameters.Add("@Description", SqlDbType.NText).Value = _Description;
                zCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = _ExpireDate;
                if (_EmployeeKey != string.Empty)
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_EmployeeKey);
                else
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@Islogin", SqlDbType.Int).Value = _IsLogin;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                nResult = zCommand.ExecuteNonQuery().ToString();

                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return nResult;
        }
        public string Create()
        {

            string nResult = "";

            //---------- String SQL Access Database ---------------
            string zSQL = "[SYS_User_INSERT]";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();

            try
            {

                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@UserName", SqlDbType.NVarChar).Value = _UserName;
                zCommand.Parameters.Add("@Password", SqlDbType.NVarChar).Value = _Password;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = _EmployeeKey;
                if (_ExpireDate.Year == 001)
                    zCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = _ExpireDate;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@Islogin", SqlDbType.Int).Value = _IsLogin;
                nResult = zCommand.ExecuteNonQuery().ToString();

                zCommand.Dispose();

            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return nResult;
        }
        public string Delete()
        {
            string nResult = "";

            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE SYS_User SET  RecordStatus = 99  WHERE UserKey = @UserKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();

            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zCommand.CommandType = CommandType.Text;

                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_UserKey);
                nResult = zCommand.ExecuteNonQuery().ToString();

                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return nResult;
        }
        public string Save()
        {
            if (_UserKey.Trim().Length > 0)
                return Update();
            else
                return Create();
        }
        public string ResetPassWord()
        {
            string nResult = ""; ;

            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE SYS_User SET "
                        + " Password = @Password"
                        + " WHERE UserKey = @UserKey AND RecordStatus <> 99";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();

            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_UserKey);
                zCommand.Parameters.Add("@Password", SqlDbType.NVarChar).Value = _Password;
                nResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return nResult;
        }
        public string UpdateIsLogin(int IsLogin)
        {

            string zResult = "";

            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE SYS_User SET "
                        + " IsLogin = @IsLogin"
                        + " WHERE UserKey = @UserKey AND RecordStatus <> 99";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_UserKey);
                zCommand.Parameters.Add("@IsLogin", SqlDbType.Int).Value = IsLogin;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
