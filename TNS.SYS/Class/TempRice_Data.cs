﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Connect;
using TNS.Misc;

namespace TNS.SYS
{
    public class TempRice_Data
    {
        public static DataTable List(DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT AutoKey,ReportName,TypeName,Name,SubName,Rank,Type FROM HRM_TempRice WHERE RecordStatus <> 99 
                            AND DateWrite BETWEEN @FromDate AND @ToDate 
                            ORDER BY Report,Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List_FromRiceKey(int RiceKey, DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT AutoKey,ID,Report FROM HRM_TempRice WHERE RecordStatus <> 99  AND RiceKey = @RiceKey
                            AND DateWrite BETWEEN @FromDate AND @ToDate                             
                            ORDER BY Report,Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@RiceKey", SqlDbType.Int).Value = RiceKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static int CountRice(int AutoKey)
        {
            int zResult = 0;
            string zSQL = @"SELECT COUNT(RiceKey) FROM [dbo].[HRM_TempRice] WHERE RiceKey = @AutoKey AND RecordStatus <> 99 ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                zResult = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
        public static int KiemTraDuLieuThangTruoc(DateTime DateWrite)
        {
            DateWrite = DateWrite.AddMonths(-1);
            var zTime = DateWrite.FirstEndMonth();
            int zResult = 0;
            string zSQL = @"SELECT COUNT(*) FROM [dbo].[HRM_TempRice] WHERE RecordStatus <> 99 
                            AND DateWrite BETWEEN @FromDate AND @ToDate";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zTime.Item1;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zTime.Item2;
                zResult = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
        public static string DeleteFromRiceKey(int AutoKey, int Report, DateTime FromDate, DateTime ToDate)
        {
            string  zResult ="";
            DataTable zTable = new DataTable();
            string zSQL = @"
                        DECLARE @Position int =0
                        SELECT @Position= Rank FROM HRM_TempRice WHERE AutoKey=@AutoKey
                        IF(@Position>0)
                             UPDATE HRM_TempRice SET Rank = Rank-1 WHERE RecordStatus <> 99 AND Rank > @Position AND Report =@Report AND DateWrite BETWEEN @FromDate AND @ToDate
                        DELETE FROM HRM_TempRice WHERE AutoKey=@AutoKey
";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                zCommand.Parameters.Add("@Report", SqlDbType.Int).Value = Report;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                zResult = ex.ToString();
            }
            return zResult;
        }
        public static string UpDateRank(int AutoKey, int Rank)
        {
            string zResult = "";
            DataTable zTable = new DataTable();
            string zSQL = @"

                         UPDATE HRM_TempRice SET Rank = @Rank WHERE RecordStatus <> 99 AND AutoKey = @AutoKey
";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Rank;
                zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                zResult = ex.ToString();
            }
            return zResult;
        }
        public static string DeleteAll(DateTime FromDate, DateTime ToDate)
        {
            string zResult = "";
            DataTable zTable = new DataTable();
            string zSQL = @"

            DELETE FROM HRM_TempRice WHERE RecordStatus <> 99 AND DateWrite BETWEEN @FromDate AND @ToDate
";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                zResult = ex.ToString();
            }
            return zResult;
        }

        public static string CoppyTempRice(DateTime FromDate, DateTime ToDate,DateTime FromDateNew,DateTime ToDateNew)
        {
            string zResult = "";
            DataTable zTable = new DataTable();
            string zSQL = @"
                        UPDATE [dbo].[HRM_TempRice] SET RecordStatus = 99, ModifiedBy= @ModifiedBy,ModifiedName =@ModifiedName 
                        WHERE RecordStatus <> 99
                        AND DateWrite BETWEEN @FromDateNew AND @ToDateNew

                        INSERT INTO [dbo].[HRM_TempRice]
                        ([DateWrite],[Report],[ReportName],[RiceKey],[ID],[Name],[SubName],[Type]
                        ,[TypeName],[CategoryKey],[CategoryName],[Rank],[RecordStatus]
                        ,[CreatedOn],[CreatedBy],[CreatedName],[ModifiedOn],[ModifiedBy],[ModifiedName])
                         SELECT 
                         @FromDateNew,[Report],[ReportName],[RiceKey],[ID],[Name],[SubName],[Type]
                        ,[TypeName],[CategoryKey],[CategoryName],[Rank],[RecordStatus]
                        ,GETDATE(),@CreatedBy,@CreatedName,GETDATE(),@ModifiedBy,@ModifiedName
                         FROM [dbo].[HRM_TempRice] 
                         WHERE RecordStatus <> 99
                         AND DateWrite BETWEEN @FromDate AND @ToDate
";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zCommand.Parameters.Add("@FromDateNew", SqlDbType.DateTime).Value = FromDateNew;
                zCommand.Parameters.Add("@ToDateNew", SqlDbType.DateTime).Value = ToDateNew;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = SessionUser.UserLogin.Key;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = SessionUser.UserLogin.EmployeeName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = SessionUser.UserLogin.Key;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = SessionUser.UserLogin.EmployeeName;
                zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                zResult = ex.ToString();
            }
            return zResult;
        }
        public static string CoppyCodeReport(DateTime FromDate, DateTime ToDate, DateTime FromDateNew, DateTime ToDateNew)
        {
            string zResult = "";
            DataTable zTable = new DataTable();
            string zSQL = @"
                    Declare @Count int=0;
                    SELECT @Count = COUNT(AutoKey) FROM HRM_CodeReport
                    WHERE RecordStatus <> 99
                    AND DateWrite BETWEEN @FromDateNew AND @ToDateNew
                    IF(@Count=0)
                        BEGIN
                            INSERT INTO HRM_CodeReport
                            ([DateWrite],[ID],[Name],[Description],[Type],[TypeName]
                            ,[CategoryKey],[CategoryName],[Rank],[RecordStatus]
                            ,[CreatedOn],[CreatedBy],[CreatedName],[ModifiedOn],[ModifiedBy],[ModifiedName])
                            SELECT 
                            @FromDateNew,[ID],[Name],[Description],[Type],[TypeName]
                            ,[CategoryKey],[CategoryName],[Rank],[RecordStatus]
                            ,GETDATE(),@CreatedBy,@CreatedName,GETDATE(),@ModifiedBy,@ModifiedName
                            FROM HRM_CodeReport
                            WHERE RecordStatus <> 99
                            AND DateWrite BETWEEN @FromDate AND @ToDate
                        END
                    ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zCommand.Parameters.Add("@FromDateNew", SqlDbType.DateTime).Value = FromDateNew;
                zCommand.Parameters.Add("@ToDateNew", SqlDbType.DateTime).Value = ToDateNew;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = SessionUser.UserLogin.Key;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = SessionUser.UserLogin.EmployeeName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = SessionUser.UserLogin.Key;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = SessionUser.UserLogin.EmployeeName;
                zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                zResult = ex.ToString();
            }
            return zResult;
        }
        public static string CoppyCodeReportOffice(DateTime FromDate, DateTime ToDate, DateTime FromDateNew, DateTime ToDateNew)
        {
            string zResult = "";
            DataTable zTable = new DataTable();
            string zSQL = @"
                    Declare @Count int=0;
                    SELECT @Count = COUNT(AutoKey) FROM SLR_CodeReportOffice
                    WHERE RecordStatus <> 99
                    AND DateWrite BETWEEN @FromDateNew AND @ToDateNew
                    IF(@Count=0)
                        BEGIN
                            INSERT INTO SLR_CodeReportOffice
                            ([DateWrite],[ID],[Name],[Description],[Type],[TypeName],[CategoryKey],[CategoryName]
                            ,[Rank],[Publish],[Publish_Close],[RecordStatus],
                            [CreatedOn],[CreatedBy],[CreatedName],[ModifiedOn],[ModifiedBy],[ModifiedName])
                            SELECT 
                            @FromDateNew,[ID],[Name],[Description],[Type],[TypeName],[CategoryKey],[CategoryName]
                            ,[Rank],[Publish],[Publish_Close],[RecordStatus],
                            [CreatedOn],[CreatedBy],[CreatedName],[ModifiedOn],[ModifiedBy],[ModifiedName]
                            FROM SLR_CodeReportOffice
                            WHERE RecordStatus <> 99
                            AND DateWrite BETWEEN @FromDate AND @ToDate
                        END
                    ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zCommand.Parameters.Add("@FromDateNew", SqlDbType.DateTime).Value = FromDateNew;
                zCommand.Parameters.Add("@ToDateNew", SqlDbType.DateTime).Value = ToDateNew;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = SessionUser.UserLogin.Key;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = SessionUser.UserLogin.EmployeeName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = SessionUser.UserLogin.Key;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = SessionUser.UserLogin.EmployeeName;
                zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                zResult = ex.ToString();
            }
            return zResult;
        }
        public static string CoppyCodeReportSupport(DateTime FromDate, DateTime ToDate, DateTime FromDateNew, DateTime ToDateNew)
        {
            string zResult = "";
            DataTable zTable = new DataTable();
            string zSQL = @"
                    Declare @Count int=0;
                    SELECT @Count = COUNT(AutoKey) FROM SLR_CodeReportSupport
                    WHERE RecordStatus <> 99
                    AND DateWrite BETWEEN @FromDateNew AND @ToDateNew
                    IF(@Count=0)
                        BEGIN
                            INSERT INTO SLR_CodeReportSupport
                            ([DateWrite],[ID],[Name],[Description],[Type],[TypeName],[CategoryKey],[CategoryName]
                            ,[Rank],[Publish],[Publish_Close],[RecordStatus],
                            [CreatedOn],[CreatedBy],[CreatedName],[ModifiedOn],[ModifiedBy],[ModifiedName])
                            SELECT 
                            @FromDateNew,[ID],[Name],[Description],[Type],[TypeName],[CategoryKey],[CategoryName]
                            ,[Rank],[Publish],[Publish_Close],[RecordStatus],
                            [CreatedOn],[CreatedBy],[CreatedName],[ModifiedOn],[ModifiedBy],[ModifiedName]
                            FROM SLR_CodeReportSupport
                            WHERE RecordStatus <> 99
                            AND DateWrite BETWEEN @FromDate AND @ToDate
                        END
                    ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zCommand.Parameters.Add("@FromDateNew", SqlDbType.DateTime).Value = FromDateNew;
                zCommand.Parameters.Add("@ToDateNew", SqlDbType.DateTime).Value = ToDateNew;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = SessionUser.UserLogin.Key;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = SessionUser.UserLogin.EmployeeName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = SessionUser.UserLogin.Key;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = SessionUser.UserLogin.EmployeeName;
                zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                zResult = ex.ToString();
            }
            return zResult;
        }
        public static string CoppyCodeReportWorker(DateTime FromDate, DateTime ToDate, DateTime FromDateNew, DateTime ToDateNew)
        {
            string zResult = "";
            DataTable zTable = new DataTable();
            string zSQL = @"
                    Declare @Count int=0;
                    SELECT @Count = COUNT(AutoKey) FROM SLR_CodeReportWorker
                    WHERE RecordStatus <> 99
                    AND DateWrite BETWEEN @FromDateNew AND @ToDateNew
                    IF(@Count=0)
                        BEGIN
                            INSERT INTO SLR_CodeReportWorker
                            ([DateWrite],[ID],[Name],[Description],[Type],[TypeName],[CategoryKey],[CategoryName]
                            ,[Rank],[Publish],[Publish_Close],[RecordStatus],
                            [CreatedOn],[CreatedBy],[CreatedName],[ModifiedOn],[ModifiedBy],[ModifiedName])
                            SELECT 
                            @FromDateNew,[ID],[Name],[Description],[Type],[TypeName],[CategoryKey],[CategoryName]
                            ,[Rank],[Publish],[Publish_Close],[RecordStatus],
                            [CreatedOn],[CreatedBy],[CreatedName],[ModifiedOn],[ModifiedBy],[ModifiedName]
                            FROM SLR_CodeReportWorker
                            WHERE RecordStatus <> 99
                            AND DateWrite BETWEEN @FromDate AND @ToDate
                        END
                    ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zCommand.Parameters.Add("@FromDateNew", SqlDbType.DateTime).Value = FromDateNew;
                zCommand.Parameters.Add("@ToDateNew", SqlDbType.DateTime).Value = ToDateNew;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = SessionUser.UserLogin.Key;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = SessionUser.UserLogin.EmployeeName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = SessionUser.UserLogin.Key;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = SessionUser.UserLogin.EmployeeName;
                zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                zResult = ex.ToString();
            }
            return zResult;
        }

        #region[Mã cơm- chấm công]
        public static DataTable DanhSachMaIDCom_Cu(DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT AutoKey,ID,Name,Rank FROM [dbo].[HRM_CodeReport] WHERE RecordStatus<> 99  
                            AND DateWrite BETWEEN @FromDate AND @ToDate
                            AND CategoryKey IN (1,2) ORDER BY Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable DanhSachMaIDCom_Moi(DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT * FROM HRM_TempRice WHERE RecordStatus <> 99 
                            AND DateWrite BETWEEN @FromDate AND @ToDate
                            AND CategoryKey IN (1,2)
                            ORDER BY Report,Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static int  ViTriDauMaIDCom(DateTime FromDate, DateTime ToDate)
        {
            int zResult = 0;
            string zSQL = @"
            DECLARE @Position int = 0
            SELECT @Position =[Rank] FROM[dbo].[HRM_CodeReport]
            WHERE ID = 'NCLP' AND RecordStatus<> 99             --Bắt đầu tên cơm cộng là kế mã số ngày chuyên cần
            AND DateWrite BETWEEN @FromDate AND @ToDate
            SELECT @Position
            ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zResult =zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
        #endregion

        #region[Số phần cơm- chấm công]
        public static DataTable DanhSachSoPhanCom_Cu(DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT AutoKey,ID,Name,Rank FROM [dbo].[HRM_CodeReport] WHERE RecordStatus<> 99 
                            AND DateWrite BETWEEN @FromDate AND @ToDate
                            AND CategoryKey IN (3,4) ORDER BY Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable DanhSachSoPhanCom_Moi(DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT * FROM HRM_TempRice WHERE RecordStatus <> 99 
                            AND DateWrite BETWEEN @FromDate AND @ToDate
                            AND CategoryKey IN (3,4)
                            ORDER BY Report,Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static int ViTriDauSoPhanCom(DateTime FromDate, DateTime ToDate)
        {
            int zResult = 0;
            string zSQL = @"
            DECLARE @Position int = 0
            SELECT @Position =[Rank] FROM[dbo].[HRM_CodeReport]
            WHERE ID = 'SNCC' AND RecordStatus<> 99             --Bắt đầu tên cơm cộng là kế mã số ngày chuyên cần
            AND DateWrite BETWEEN @FromDate AND @ToDate
            SELECT @Position
            ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zResult = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
        #endregion

        #region[Cơm cộng- Công nhân]
        public static DataTable DanhSach_ComCongWorker_Cu(DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT AutoKey,ID,Name,Rank FROM [dbo].[SLR_CodeReportWorker] WHERE RecordStatus<> 99 
                            AND DateWrite BETWEEN @FromDate AND @ToDate
                            AND CategoryKey IN (5,6) ORDER BY Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable DanhSach_ComCongWorker_Moi(DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT * FROM HRM_TempRice WHERE RecordStatus <> 99 
                            AND DateWrite BETWEEN @FromDate AND @ToDate
                            AND CategoryKey IN (5,6)
                            ORDER BY Report,Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static int ViTriDauComCongWorker(DateTime FromDate, DateTime ToDate)
        {
            int zResult = 0;
            string zSQL = @"
            DECLARE @Position int = 0
            SELECT @Position =[Rank] FROM[dbo].[SLR_CodeReportWorker]
            WHERE ID = 'HTTD' AND RecordStatus<> 99             --Bắt đầu tên cơm cộng là hỗ trợ thấp điểm
            AND DateWrite BETWEEN @FromDate AND @ToDate
            SELECT @Position
            ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zResult = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
        #endregion

        #region[Cơm trừ- Công nhân]
        public static DataTable DanhSach_ComTruWorker_Cu(DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT AutoKey,ID,Name,Rank FROM [dbo].[SLR_CodeReportWorker] WHERE RecordStatus<> 99 
                            AND DateWrite BETWEEN @FromDate AND @ToDate
                            AND CategoryKey IN (7,8) ORDER BY Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable DanhSach_ComTruWorker_Moi(DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT * FROM HRM_TempRice WHERE RecordStatus <> 99 
                            AND DateWrite BETWEEN @FromDate AND @ToDate
                            AND CategoryKey IN (7,8)
                            ORDER BY Report,Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static int ViTriDauComTruWorker(DateTime FromDate, DateTime ToDate)
        {
            int zResult = 0;
            string zSQL = @"
            DECLARE @Position int = 0
            SELECT @Position =[Rank] FROM[dbo].[SLR_CodeReportWorker]
            WHERE ID = 'TNCN' AND RecordStatus<> 99             --Bắt đầu tên cơm trừ là thuế TNCN
            AND DateWrite BETWEEN @FromDate AND @ToDate
            SELECT @Position
            ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zResult = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
        #endregion

        #region[Cơm cộng- Hỗ trợ]
        public static DataTable DanhSach_ComCongSupport_Cu(DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT AutoKey,ID,Name,Rank FROM [dbo].[SLR_CodeReportSupport] WHERE RecordStatus<> 99 
                            AND DateWrite BETWEEN @FromDate AND @ToDate
                            AND CategoryKey IN (5,6) ORDER BY Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable DanhSach_ComCongSupport_Moi(DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT * FROM HRM_TempRice WHERE RecordStatus <> 99 
                            AND DateWrite BETWEEN @FromDate AND @ToDate
                            AND CategoryKey IN (5,6)
                            ORDER BY Report,Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static int ViTriDauComCongSupport(DateTime FromDate, DateTime ToDate)
        {
            int zResult = 0;
            string zSQL = @"
            DECLARE @Position int = 0
            SELECT @Position =[Rank] FROM[dbo].[SLR_CodeReportSupport]
            WHERE ID = 'HTTD' AND RecordStatus<> 99             --Bắt đầu tên cơm cộng là hỗ trợ thấp điểm
             AND DateWrite BETWEEN @FromDate AND @ToDate
            SELECT @Position
            ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zResult = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
        #endregion

        #region[Cơm trừ- Hỗ trợ]
        public static DataTable DanhSach_ComTruSupport_Cu(DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT AutoKey,ID,Name,Rank FROM [dbo].[SLR_CodeReportSupport] WHERE RecordStatus<> 99 
                            AND DateWrite BETWEEN @FromDate AND @ToDate
                            AND CategoryKey IN (7,8) ORDER BY Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable DanhSach_ComTruSupport_Moi(DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT * FROM HRM_TempRice WHERE RecordStatus <> 99 
                            AND DateWrite BETWEEN @FromDate AND @ToDate
                            AND CategoryKey IN (7,8)
                            ORDER BY Report,Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static int ViTriDauComTruSupport(DateTime FromDate, DateTime ToDate)
        {
            int zResult = 0;
            string zSQL = @"
            DECLARE @Position int = 0
            SELECT @Position =[Rank] FROM[dbo].[SLR_CodeReportSupport]
            WHERE ID = 'TNCN' AND RecordStatus<> 99             --Bắt đầu tên cơm trừ là thuế TNCN
            AND DateWrite BETWEEN @FromDate AND @ToDate
            SELECT @Position
            ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zResult = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
        #endregion

        #region[Cơm cộng- Văn phòng]
        public static DataTable DanhSach_ComCongOffice_Cu(DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT AutoKey,ID,Name,Rank FROM [dbo].[SLR_CodeReportOffice] WHERE RecordStatus<> 99 
                            AND DateWrite BETWEEN @FromDate AND @ToDate
                            AND CategoryKey IN (5,6) ORDER BY Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable DanhSach_ComCongOffice_Moi(DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT * FROM HRM_TempRice WHERE RecordStatus <> 99 
                            AND DateWrite BETWEEN @FromDate AND @ToDate
                            AND CategoryKey IN (5,6)
                            ORDER BY Report,Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static int ViTriDauComCongOffice(DateTime FromDate, DateTime ToDate)
        {
            int zResult = 0;
            string zSQL = @"
            DECLARE @Position int = 0
            SELECT @Position =[Rank] FROM[dbo].[SLR_CodeReportOffice]
            WHERE ID = 'TCSX' AND RecordStatus<> 99             --Bắt đầu tên cơm cộng là hỗ trợ thấp điểm
            AND DateWrite BETWEEN @FromDate AND @ToDate
            SELECT @Position
            ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zResult = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
        #endregion

        #region[Cơm trừ- văn phòng]
        public static DataTable DanhSach_ComTruOffice_Cu(DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT AutoKey,ID,Name,Rank FROM [dbo].[SLR_CodeReportOffice] WHERE RecordStatus<> 99 
                            AND DateWrite BETWEEN @FromDate AND @ToDate
                            AND CategoryKey IN (7,8) ORDER BY Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable DanhSach_ComTruOffice_Moi(DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT * FROM HRM_TempRice WHERE RecordStatus <> 99 
                            AND DateWrite BETWEEN @FromDate AND @ToDate
                            AND CategoryKey IN (7,8)
                            ORDER BY Report,Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static int ViTriDauComTruOffice(DateTime FromDate, DateTime ToDate)
        {
            int zResult = 0;
            string zSQL = @"
            DECLARE @Position int = 0
            SELECT @Position =[Rank] FROM[dbo].[SLR_CodeReportOffice]
            WHERE ID = 'TNCN' AND RecordStatus<> 99             --Bắt đầu tên cơm trừ là thuế TNCN
            AND DateWrite BETWEEN @FromDate AND @ToDate
            SELECT @Position
            ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zResult = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
        #endregion

    }
}
