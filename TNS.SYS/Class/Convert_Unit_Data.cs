﻿using System;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
namespace TNS.IVT
{
    public class Convert_Unit_Data
    {
        public static DataTable List()
        {
            DataTable zTable = new DataTable();
            string zSQL = @" SELECT A.*,
[dbo].[Fn_GetUnitName](A.Unit1) AS UnitName1 , 
[dbo].[Fn_GetUnitName](A.Unit2) AS UnitName2 
FROM [dbo].[IVT_Convert_Unit]  A 
WHERE A.RecordStatus <>99 ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable Search(string Name)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.*,
[dbo].[Fn_GetUnitName](A.Unit1) AS UnitName1 , 
[dbo].[Fn_GetUnitName](A.Unit2) AS UnitName2  
FROM IVT_Convert_Unit A
WHERE (A.Unit1 LIKE @Name or A.Unit2 LIKE @Name )
AND  A.RecordStatus < 99  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable ListConvert()
        {
            DataTable zTable = new DataTable();
            string zSQL = @" SELECT AutoKey,ProductID,ProductName,Quantity1,[dbo].[Fn_GetUnitName](Unit1) AS UnitName1 ,Quantity2,[dbo].[Fn_GetUnitName](Unit2) AS UnitName2,Description
FROM [dbo].[IVT_Convert_Unit]  A 
WHERE A.RecordStatus <>99 ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
    }
}
