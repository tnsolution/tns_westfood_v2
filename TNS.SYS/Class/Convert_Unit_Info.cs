﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
namespace TNS.IVT
{
    public class Convert_Unit_Info
    {

        #region [ Field Name ]
        private int _AutoKey = 0;
        private string _ProductKey = "";
        private string _ProductID = "";
        private string _ProductName = "";
        private int _Unit1 = 0;
        private int _Unit2 = 0;
        private float _Quantity1;
        private float _Quantity2;
        private string _Description = "";
        private int _RecordStatus = 0;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _CreatedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private DateTime _ModifiedOn;
        private string _RoleID = "";
        private string _Message = "";
        #endregion
        #region [ Properties ]
       
        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        public int Key
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public int Unit1
        {
            get { return _Unit1; }
            set { _Unit1 = value; }
        }
        public int Unit2
        {
            get { return _Unit2; }
            set { _Unit2 = value; }
        }
        public float Quantity1
        {
            get { return _Quantity1; }
            set { _Quantity1 = value; }
        }
        public float Quantity2
        {
            get { return _Quantity2; }
            set { _Quantity2 = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public string ProductKey
        {
            get
            {
                return _ProductKey;
            }

            set
            {
                _ProductKey = value;
            }
        }

        public string ProductID
        {
            get
            {
                return _ProductID;
            }

            set
            {
                _ProductID = value;
            }
        }

        public string ProductName
        {
            get
            {
                return _ProductName;
            }

            set
            {
                _ProductName = value;
            }
        }
        #endregion
        #region [ Constructor Get Information ]
        public Convert_Unit_Info()
        {
        }
        public Convert_Unit_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM IVT_Convert_Unit WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    if (zReader["Unit1"] != DBNull.Value)
                        _Unit1 = int.Parse(zReader["Unit1"].ToString());
                    if (zReader["Unit2"] != DBNull.Value)
                        _Unit2 = int.Parse(zReader["Unit2"].ToString());
                    if (zReader["Quantity1"] != DBNull.Value)
                        _Quantity1 = float.Parse(zReader["Quantity1"].ToString());
                    if (zReader["Quantity2"] != DBNull.Value)
                        _Quantity2 = float.Parse(zReader["Quantity2"].ToString());
                    _Description = zReader["Description"].ToString().Trim();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ProductKey = zReader["ProductKey"].ToString().Trim();
                    _ProductID = zReader["ProductID"].ToString().Trim();
                    _ProductName = zReader["ProductName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO IVT_Convert_Unit ("
        + " ProductKey, ProductID,ProductName, Unit1 ,Unit2 ,Quantity1 ,Quantity2 ,Description  ,CreatedBy ,CreatedName ,CreatedOn ,ModifiedBy ,ModifiedName ,ModifiedOn ) "
         + " VALUES ( "
         + " @ProductKey, @ProductID,@ProductName, @Unit1 ,@Unit2 ,@Quantity1 ,@Quantity2 ,@Description ,@CreatedBy ,@CreatedName ,GETDATE(),@ModifiedBy ,@ModifiedName ,GETDATE()) ";

            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@Unit1", SqlDbType.Int).Value = _Unit1;
                zCommand.Parameters.Add("@Unit2", SqlDbType.Int).Value = _Unit2;
                zCommand.Parameters.Add("@Quantity1", SqlDbType.Float).Value = _Quantity1;
                zCommand.Parameters.Add("@Quantity2", SqlDbType.Float).Value = _Quantity2;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                zCommand.Parameters.Add("@ProductKey", SqlDbType.NVarChar).Value = _ProductKey.Trim();
                zCommand.Parameters.Add("@ProductID", SqlDbType.NVarChar).Value = _ProductID.Trim();
                zCommand.Parameters.Add("@ProductName", SqlDbType.NVarChar).Value = _ProductName.Trim();

                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE IVT_Convert_Unit SET "
                        + " Unit1 = @Unit1,"
                        + " Unit2 = @Unit2,"
                        + " Quantity1 = @Quantity1,"
                        + " Quantity2 = @Quantity2,"
                        + " Description = @Description,"
                        + " ProductKey = @ProductKey,"
                        + " ProductID = @ProductID,"
                        + " ProductName = @ProductName,"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName,"
                        + " ModifiedOn = GETDATE()"
                       + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@Unit1", SqlDbType.Int).Value = _Unit1;
                zCommand.Parameters.Add("@Unit2", SqlDbType.Int).Value = _Unit2;
                zCommand.Parameters.Add("@Quantity1", SqlDbType.Float).Value = _Quantity1;
                zCommand.Parameters.Add("@Quantity2", SqlDbType.Float).Value = _Quantity2;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                zCommand.Parameters.Add("@ProductKey", SqlDbType.NVarChar).Value = _ProductKey.Trim();
                zCommand.Parameters.Add("@ProductID", SqlDbType.NVarChar).Value = _ProductID.Trim();
                zCommand.Parameters.Add("@ProductName", SqlDbType.NVarChar).Value = _ProductName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE IVT_Convert_Unit SET [RecordStatus] = 99, [ModifiedOn] = GETDATE(), ModifiedBy = @ModifiedBy, ModifiedName = @ModifiedName WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_AutoKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        #endregion
    }
}
