﻿using System;
using System.Data;

namespace TNS.SYS
{
    public class SessionUser
    {
        private static User_Info _UserLogin;
        private static DataTable _Table_Role;
        private static DateTime _Date_Work;
        private static Personal_Info _Personal_Info;
        private static DateTime _Date_Lock;

        public static User_Info UserLogin
        {
            get
            {
                return _UserLogin;
            }

        }
        public static Personal_Info Personal_Info
        {
            get
            {
                return _Personal_Info;
            }

            set
            {
                _Personal_Info = value;
            }
        }
        public static DataTable Table_Role
        {
            get
            {
                return _Table_Role;
            }

            set
            {
                _Table_Role = value;
            }
        }
        public static DateTime Date_Work
        {
            get
            {
                return _Date_Work;
            }

            set
            {
                _Date_Work = value;
            }
        }
        public static DateTime Date_Lock
        {
            get
            {
                return _Date_Lock;
            }

            set
            {
                _Date_Lock = value;
            }
        }


        public SessionUser()
        {
            _Personal_Info = new Personal_Info();
            _UserLogin = new User_Info();
            _Table_Role = new DataTable();
            _Date_Work = DateTime.Now;
            _Date_Lock = DateTime.MinValue;
        }
        public SessionUser(string UserName)
        {
            _UserLogin = new User_Info(UserName);
        }
        public SessionUser(string UserName, string UserKey)
        {
            _UserLogin = new User_Info(UserName);
            if(_UserLogin.Name=="tns")
            {
                _UserLogin.EmployeeName = "IT Hỗ trợ";
            }
            _Personal_Info = new Personal_Info(_UserLogin.EmployeeKey);
            _Table_Role = User_Data.UserRole_ID(UserKey);
            _Date_Work = DateTime.Now;
            _Date_Lock = DateClose_Data.GetDateLock();
        }
    }
}
