﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TN.Connect;
using TNS.Misc;
using TNS.SYS;

namespace TN.Library.System
{
    public class Time_Rice_Data
    {
        public static DataTable List()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT* FROM HRM_Time_Rice WHERE RecordStatus <> 99 ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable Search(string Name)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT RiceKey,RiceID,RiceName,TypeName,Money,Rank,Description FROM HRM_Time_Rice WHERE RecordStatus != 99 ";
            if (Name.Trim().Length > 0)
            {
                zSQL += " AND (RiceID LIKE @Name OR RiceName LIKE @Name )";
            }
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static int CountID(string Name)
        {
            int zResult = 0;
            string zSQL = "SELECT COUNT(RiceKey) FROM HRM_Time_Rice WHERE RiceID = @Name AND   RecordStatus <> 99 ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = Name.Trim();
                zResult = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
        public static int AutoKeyEnd()
        {
            int zResult = 0;
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT TOP 1 RIGHT(ID_Number,2) FROM [dbo].[HRM_Time_Rice]
ORDER BY CAST(RIGHT(ID_Number,2) AS INT)  DESC ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zResult = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }

        public static bool Add_RicePlus(int Key,string ID,string Name, string ID_Rice,string ID_Number,string ID_Money, DateTime FromDate ,DateTime ToDate)
        {
            bool zResult = false;
            string zSQL = @"
DECLARE @Position int = 0
--1.Tạo cột  mã cơm tại chấm công
    --Lấy vị trí cuối cùng của mã cơm cộng
    SELECT TOP 1 @Position =[Rank] FROM[dbo].[HRM_TempRice]
    WHERE Report = 1 AND Type = 0 AND CategoryKey=1 AND RecordStatus<> 99 AND DateWrite BETWEEN @FromDate AND @ToDate  ORDER BY[Rank] DESC
    IF(@Position = '') --Nếu cơm trừ đã tạo trước
                BEGIN
                   SELECT TOP 1 @Position =[Rank] FROM[dbo].[HRM_TempRice]  -- cơm trừ
                   WHERE Report = 1 AND Type = 1 AND CategoryKey=2 AND RecordStatus<> 99 AND DateWrite BETWEEN @FromDate AND @ToDate  ORDER BY[Rank] ASC
                    IF(@Position != '')
                            BEGIN
                                --Chèn thay thế vị trí kế tiếp vừa tìm được
                                SET @Position = @Position
                                UPDATE [dbo].[HRM_TempRice] SET Rank = Rank + 1 WHERE Rank >=@Position AND Report =1 AND RecordStatus<> 99 AND DateWrite BETWEEN @FromDate AND @ToDate 
                                INSERT INTO[dbo].[HRM_TempRice] (DateWrite,Report,ReportName,RiceKey,ID, Name, Type, TypeName,CategoryKey,CategoryName, Rank,RecordStatus,CreatedBy,CreatedName)
                                VALUES(@FromDate,1,N'Bảng chấm công',@RiceKey,@ID_Rice, @ID, 0, N'Cơm cộng',1,N'Mã cơm cộng', @Position,0,@CreatedBy,@CreatedName)
                             
                            END
                    ELSE 
                            BEGIN
                                --Chèn thay thế vị trí kế tiếp vừa tìm được
                                SET @Position = @Position
                                UPDATE [dbo].[HRM_TempRice] SET Rank = Rank + 1 WHERE Rank >@Position AND Report =1 AND RecordStatus<> 99 AND DateWrite BETWEEN @FromDate AND @ToDate 
                                SET @Position = @Position+1

                                INSERT INTO[dbo].[HRM_TempRice] (DateWrite,Report,ReportName,RiceKey,ID, Name, Type, TypeName,CategoryKey,CategoryName, Rank,RecordStatus,CreatedBy,CreatedName)
                                VALUES(@FromDate,1,N'Bảng chấm công',@RiceKey,@ID_Rice, @ID, 0, N'Cơm cộng',1,N'Mã cơm cộng', @Position,0,@CreatedBy,@CreatedName)
                            END
                END
    ELSE 
                BEGIN
                 --Chèn thay thế vị trí kế tiếp vừa tìm được
                    SET @Position = @Position
                    UPDATE [dbo].[HRM_TempRice] SET Rank = Rank + 1 WHERE Rank >@Position AND Report =1 AND RecordStatus<> 99 AND DateWrite BETWEEN @FromDate AND @ToDate 
                    SET @Position = @Position+1

                    INSERT INTO[dbo].[HRM_TempRice] (DateWrite,Report,ReportName,RiceKey,ID, Name, Type, TypeName,CategoryKey,CategoryName, Rank,RecordStatus,CreatedBy,CreatedName)
                    VALUES(@FromDate,1,N'Bảng chấm công',@RiceKey,@ID_Rice, @ID, 0, N'Cơm cộng',1,N'Mã cơm cộng', @Position,0,@CreatedBy,@CreatedName)
                END

--2.Tạo cột số lượng mã cơm tại chấm công
SET  @Position =0
SELECT TOP 1 @Position =[Rank] FROM[dbo].[HRM_TempRice]
WHERE Report = 1 AND Type = 0 AND CategoryKey=3 AND RecordStatus<> 99 AND DateWrite BETWEEN @FromDate AND @ToDate  ORDER BY[Rank] DESC
    IF(@Position = '')
                BEGIN
                   SELECT TOP 1 @Position =[Rank] FROM[dbo].[HRM_TempRice]  -- cơm trừ
                   WHERE Report = 1 AND Type = 1 AND CategoryKey=2 AND RecordStatus<> 99 AND DateWrite BETWEEN @FromDate AND @ToDate  ORDER BY[Rank] DESC
                END
    IF(@Position = '')
                BEGIN
                   SELECT TOP 1 @Position =[Rank] FROM[dbo].[HRM_TempRice]  -- cơm cộng đầu tiền
                   WHERE Report = 1 AND Type = 0 AND CategoryKey=1 AND RecordStatus<> 99 AND DateWrite BETWEEN @FromDate AND @ToDate  ORDER BY[Rank] DESC
                END
    --Chèn thay thế vị trí kế tiếp vừa tìm được
    IF(@Position = '')
                BEGIN
                    SELECT TOP 1 @Position =[Rank] FROM[dbo].[HRM_TempRice]  -- cơm trừ số 4 đứng trước vị trí số 3
                    WHERE Report = 1 AND Type = 1 AND CategoryKey=4 AND RecordStatus<> 99 AND DateWrite BETWEEN @FromDate AND @ToDate  ORDER BY[Rank] ASC
                       IF(@Position != '')
                            BEGIN
                                SET @Position = @Position
                                UPDATE [dbo].[HRM_TempRice] SET Rank = Rank + 1 WHERE Rank >=@Position AND Report =1 AND RecordStatus<> 99 AND DateWrite BETWEEN @FromDate AND @ToDate 
                                INSERT INTO[dbo].[HRM_TempRice] (DateWrite,Report,ReportName,RiceKey,ID, Name, Type, TypeName,CategoryKey,CategoryName, Rank,RecordStatus,CreatedBy,CreatedName)
                                VALUES(@FromDate,1,N'Bảng chấm công',@RiceKey,@ID_Number, N'Số lượng '+@Name, 0, N'Cơm cộng',3,N'Số lượng cơm cộng', @Position,0,@CreatedBy,@CreatedName)
                            END
                        ELSE
                            BEGIN
                                SET @Position = @Position
                                UPDATE [dbo].[HRM_TempRice] SET Rank = Rank + 1 WHERE Rank >@Position AND Report =1 AND RecordStatus<> 99 AND DateWrite BETWEEN @FromDate AND @ToDate 
                                SET @Position = @Position+1
                                INSERT INTO[dbo].[HRM_TempRice] (DateWrite,Report,ReportName,RiceKey,ID, Name, Type, TypeName,CategoryKey,CategoryName, Rank,RecordStatus,CreatedBy,CreatedName)
                                VALUES(@FromDate,1,N'Bảng chấm công',@RiceKey,@ID_Number, N'Số lượng '+@Name, 0, N'Cơm cộng',3,N'Số lượng cơm cộng', @Position,0,@CreatedBy,@CreatedName)
                            END
                END
    ELSE
                BEGIN
                    SET @Position = @Position
                    UPDATE [dbo].[HRM_TempRice] SET Rank = Rank + 1 WHERE Rank >@Position AND Report =1 AND RecordStatus<> 99 AND DateWrite BETWEEN @FromDate AND @ToDate 
                    SET @Position = @Position+1
                    INSERT INTO[dbo].[HRM_TempRice] (DateWrite,Report,ReportName,RiceKey,ID, Name, Type, TypeName,CategoryKey,CategoryName, Rank,RecordStatus,CreatedBy,CreatedName)
                    VALUES(@FromDate,1,N'Bảng chấm công',@RiceKey,@ID_Number, N'Số lượng '+@Name, 0, N'Cơm cộng',3,N'Số lượng cơm cộng', @Position,0,@CreatedBy,@CreatedName)
                END
--3.Tạo cột số lượng mã cơm tại bảng tính lương
SET  @Position =0
SELECT TOP 1 @Position =[Rank] FROM[dbo].[HRM_TempRice]
WHERE Report = 2 AND Type = 0 AND CategoryKey=6 AND RecordStatus<> 99 AND DateWrite BETWEEN @FromDate AND @ToDate ORDER BY[Rank] DESC -- thành tiền
    IF(@Position = '')
            BEGIN
                SELECT TOP 1 @Position =[Rank] FROM[dbo].[HRM_TempRice]
                WHERE Report = 2 AND Type = 1 AND CategoryKey=7 AND RecordStatus<> 99 AND DateWrite BETWEEN @FromDate AND @ToDate  ORDER BY[Rank] DESC -- số lượng
                IF(@Position !='')
                      BEGIN
                          UPDATE [dbo].[HRM_TempRice] SET Rank = Rank + 1 WHERE Rank >=@Position AND Report =2 AND RecordStatus<> 99 AND DateWrite BETWEEN @FromDate AND @ToDate 

                          INSERT INTO[dbo].[HRM_TempRice] (DateWrite,Report,ReportName,RiceKey,ID, Name,SubName, Type, TypeName,CategoryKey,CategoryName, Rank,RecordStatus,CreatedBy,CreatedName)
                          VALUES(@FromDate,2,N'Bảng lương',@RiceKey,@ID_Number, N'Số lượng',@Name, 0, N'Cơm cộng',5,N'Số lượng cơm cộng', @Position,0,@CreatedBy,@CreatedName)
                          SET @Position = @Position + 1
                          UPDATE [dbo].[HRM_TempRice] SET Rank = Rank + 1 WHERE Rank >=@Position AND Report =2 AND RecordStatus<> 99 AND DateWrite BETWEEN @FromDate AND @ToDate 
                          INSERT INTO[dbo].[HRM_TempRice] (DateWrite,Report,ReportName,RiceKey,ID, Name,SubName, Type, TypeName,CategoryKey,CategoryName, Rank,RecordStatus,CreatedBy,CreatedName)
                          VALUES(@FromDate,2,N'Bảng lương',@RiceKey,@ID_Money,N'Thành tiền', @Name, 0, N'Cơm cộng',6,N'Thành tiền cơm cộng', @Position,0,@CreatedBy,@CreatedName)
                      END
                 ELSE
                    BEGIN
                        --Chèn thay thế vị trí kế tiếp vừa tìm được
                        SET @Position = @Position + 1

                        INSERT INTO[dbo].[HRM_TempRice] (DateWrite,Report,ReportName,RiceKey,ID, Name,SubName, Type, TypeName,CategoryKey,CategoryName, Rank,RecordStatus,CreatedBy,CreatedName)
                        VALUES(@FromDate,2,N'Bảng lương',@RiceKey,@ID_Number,N'Số lượng', @Name, 0, N'Cơm cộng',5,N'Số lượng cơm cộng', @Position,0,@CreatedBy,@CreatedName)

                        SET @Position = @Position + 1
                        UPDATE [dbo].[HRM_TempRice] SET Rank = Rank + 1 WHERE Rank >=@Position AND Report =2 AND RecordStatus<> 99 AND DateWrite BETWEEN @FromDate AND @ToDate 
                        INSERT INTO[dbo].[HRM_TempRice] (DateWrite,Report,ReportName,RiceKey,ID, Name,SubName, Type, TypeName,CategoryKey,CategoryName, Rank,RecordStatus,CreatedBy,CreatedName)
                        VALUES(@FromDate,2,N'Bảng lương',@RiceKey,@ID_Money, N'Thành tiền',@Name, 0, N'Cơm cộng',6,N'Thành tiền cơm cộng', @Position,0,@CreatedBy,@CreatedName)
                    END
            END
    ELSE
            BEGIN
                --Chèn thay thế vị trí kế tiếp vừa tìm được
                UPDATE [dbo].[HRM_TempRice] SET Rank = Rank + 1 WHERE Rank >@Position AND Report =2 AND RecordStatus<> 99 AND DateWrite BETWEEN @FromDate AND @ToDate 
                SET @Position = @Position + 1

                INSERT INTO[dbo].[HRM_TempRice] (DateWrite,Report,ReportName,RiceKey,ID, Name,SubName, Type, TypeName,CategoryKey,CategoryName, Rank,RecordStatus,CreatedBy,CreatedName)
                VALUES(@FromDate,2,N'Bảng lương',@RiceKey,@ID_Number, N'Số lượng',@Name, 0, N'Cơm cộng',5,N'Số lượng cơm cộng', @Position,0,@CreatedBy,@CreatedName)

                UPDATE [dbo].[HRM_TempRice] SET Rank = Rank + 1 WHERE Rank >@Position AND Report =2 AND RecordStatus<> 99 AND DateWrite BETWEEN @FromDate AND @ToDate 
                SET @Position = @Position + 1

                INSERT INTO[dbo].[HRM_TempRice] (DateWrite,Report,ReportName,RiceKey,ID, Name,SubName, Type, TypeName,CategoryKey,CategoryName, Rank,RecordStatus,CreatedBy,CreatedName)
                VALUES(@FromDate,2,N'Bảng lương',@RiceKey,@ID_Money,N'Thành tiền', @Name, 0, N'Cơm cộng',6,N'Thành tiền cơm cộng', @Position,0,@CreatedBy,@CreatedName)
            END
";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@RiceKey", SqlDbType.Int).Value = Key;
                zCommand.Parameters.Add("@ID", SqlDbType.NVarChar).Value = ID;
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = Name;
                zCommand.Parameters.Add("@ID_Rice", SqlDbType.NVarChar).Value = ID_Rice;
                zCommand.Parameters.Add("@ID_Number", SqlDbType.NVarChar).Value = ID_Number;
                zCommand.Parameters.Add("@ID_Money", SqlDbType.NVarChar).Value = ID_Money;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = SessionUser.UserLogin.Key;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = SessionUser.UserLogin.EmployeeName;
                zCommand.ExecuteNonQuery().ToString();
                zResult = true;
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                zResult = false;
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
        public static bool Add_RiceMinus(int Key, string ID, string Name, string ID_Rice, string ID_Number, string ID_Money, DateTime FromDate, DateTime ToDate)
        {
            bool zResult = false;
            string zSQL = @"
DECLARE @Position int = 0
--1.Tạo cột  mã cơm tại chấm công
    --Lấy vị trí cuối cùng của mã cơm cộng
    SELECT TOP 1 @Position =[Rank] FROM[dbo].[HRM_TempRice]
    WHERE Report = 1 AND Type = 1 AND CategoryKey=2 AND RecordStatus<> 99 AND DateWrite BETWEEN @FromDate AND @ToDate  ORDER BY[Rank] DESC
    IF(@Position = '')
                BEGIN
                     SELECT TOP 1 @Position =[Rank] FROM[dbo].[HRM_TempRice]
                     WHERE Report = 1 AND Type = 0 AND CategoryKey=1 AND RecordStatus<> 99 AND DateWrite BETWEEN @FromDate AND @ToDate  ORDER BY[Rank] DESC
                END
    --Chèn thay thế vị trí kế tiếp vừa tìm được
    SET @Position = @Position
    UPDATE [dbo].[HRM_TempRice] SET Rank = Rank + 1 WHERE Rank >@Position AND Report =1 AND RecordStatus<> 99 AND DateWrite BETWEEN @FromDate AND @ToDate 
   SET @Position = @Position+1
    INSERT INTO[dbo].[HRM_TempRice] (DateWrite,Report,ReportName,RiceKey,ID, Name, Type, TypeName,CategoryKey,CategoryName, Rank,RecordStatus,CreatedBy,CreatedName)
    VALUES(@FromDate,1,N'Bảng chấm công',@RiceKey,@ID_Rice, @ID, 1, N'Cơm trừ',2,N'Mã cơm trừ', @Position,0,@CreatedBy,@CreatedName)

--2.Tạo cột số lượng mã cơm tại chấm công
SET  @Position =0
SELECT TOP 1 @Position =[Rank] FROM[dbo].[HRM_TempRice]
    WHERE Report = 1 AND Type = 1 AND CategoryKey=4 AND RecordStatus<> 99 AND DateWrite BETWEEN @FromDate AND @ToDate  ORDER BY[Rank] DESC
    IF(@Position = '')
                BEGIN
                   SELECT TOP 1 @Position =[Rank] FROM[dbo].[HRM_TempRice]  -- cơm trừ
                   WHERE Report = 1 AND Type = 0 AND CategoryKey=3 AND RecordStatus<> 99 AND DateWrite BETWEEN @FromDate AND @ToDate  ORDER BY[Rank] DESC
                END
    IF(@Position = '')
                BEGIN
                   SELECT TOP 1 @Position =[Rank] FROM[dbo].[HRM_TempRice]  -- cơm cộng đầu tiền
                   WHERE Report = 1 AND Type = 1 AND CategoryKey=2 AND RecordStatus<> 99 AND DateWrite BETWEEN @FromDate AND @ToDate  ORDER BY[Rank] DESC
                END
    IF(@Position = '')
                BEGIN
                   SELECT TOP 1 @Position =[Rank] FROM[dbo].[HRM_TempRice]  -- cơm trừ
                   WHERE Report = 1 AND Type = 0 AND CategoryKey=1 AND RecordStatus<> 99 AND DateWrite BETWEEN @FromDate AND @ToDate  ORDER BY[Rank] DESC
                END
    --Chèn thay thế vị trí kế tiếp vừa tìm được
   SET @Position = @Position
    UPDATE [dbo].[HRM_TempRice] SET Rank = Rank + 1 WHERE Rank >@Position AND Report =1 AND RecordStatus<> 99 AND DateWrite BETWEEN @FromDate AND @ToDate 
    SET @Position = @Position+1

    INSERT INTO[dbo].[HRM_TempRice] (DateWrite,Report,ReportName,RiceKey,ID, Name, Type, TypeName,CategoryKey,CategoryName, Rank,RecordStatus,CreatedBy,CreatedName)
    VALUES(@FromDate,1,N'Bảng chấm công',@RiceKey,@ID_Number, N'Số lượng '+@Name, 1, N'Cơm trừ',4,N'Số lượng cơm trừ', @Position,0,@CreatedBy,@CreatedName)

--3.Tạo cột số lượng mã cơm tại tính lương
SET  @Position =0
SELECT TOP 1 @Position =[Rank] FROM[dbo].[HRM_TempRice]
    WHERE Report = 2 AND Type = 1 AND CategoryKey=8 AND RecordStatus<> 99 AND DateWrite BETWEEN @FromDate AND @ToDate  ORDER BY[Rank] DESC -- thành tiền trừ
    IF(@Position = '')
                BEGIN
                    SELECT TOP 1 @Position =[Rank] FROM[dbo].[HRM_TempRice]
                    WHERE Report = 2 AND Type = 0 AND CategoryKey=6 AND RecordStatus<> 99 AND DateWrite BETWEEN @FromDate AND @ToDate  ORDER BY[Rank] DESC -- thành tiền cộng
                END
    IF(@Position = '')
                BEGIN
                    SET  @Position =0
                END
    --Chèn thay thế vị trí kế tiếp vừa tìm được
    SET @Position = @Position + 1

    INSERT INTO[dbo].[HRM_TempRice] (DateWrite,Report,ReportName,RiceKey,ID, Name,SubName, Type, TypeName,CategoryKey,CategoryName, Rank,RecordStatus,CreatedBy,CreatedName)
    VALUES(@FromDate,2,N'Bảng lương',@RiceKey,@ID_Number, N'Số lượng '+@Name,N'TRỪ THU NHẬP', 1, N'Cơm trừ',7,N'Số lượng cơm trừ', @Position,0,@CreatedBy,@CreatedName)

    SET @Position = @Position + 1

    INSERT INTO[dbo].[HRM_TempRice] (DateWrite,Report,ReportName,RiceKey,ID, Name,SubName, Type, TypeName,CategoryKey,CategoryName, Rank,RecordStatus,CreatedBy,CreatedName)
    VALUES(@FromDate,2,N'Bảng lương',@RiceKey,@ID_Money, N'Thành tiền '+@Name,N'TRỪ THU NHẬP', 1, N'Cơm trừ',8,N'Thành tiền cơm trừ', @Position,0,@CreatedBy,@CreatedName)
";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@RiceKey", SqlDbType.Int).Value = Key;
                zCommand.Parameters.Add("@ID", SqlDbType.NVarChar).Value = ID;
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = Name;
                zCommand.Parameters.Add("@ID_Rice", SqlDbType.NVarChar).Value = ID_Rice;
                zCommand.Parameters.Add("@ID_Number", SqlDbType.NVarChar).Value = ID_Number;
                zCommand.Parameters.Add("@ID_Money", SqlDbType.NVarChar).Value = ID_Money;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = SessionUser.UserLogin.Key;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = SessionUser.UserLogin.EmployeeName;
                zCommand.ExecuteNonQuery().ToString();
                zResult = true;
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                zResult = false;
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }






        //        //Insert cột vào bảng chấm công - cơm cộng
        //        public static bool Add_RicePlus(string ID)
        //        {
        //            bool zResult = false;
        //            string zSQL = @"
        //DECLARE @Position int = 0
        //--1.Tạo cột theo mã cơm
        //--Lấy vị trí cuối cùng của mã cơm cộng
        //SELECT TOP 1 @Position =[Rank] FROM[dbo].[HRM_CodeReport]
        //WHERE Type = 2 AND RecordStatus<> 99 ORDER BY[Rank] DESC
        //IF(@Position = '')

        //    BEGIN
        //    SELECT @Position =[Rank] FROM[dbo].[HRM_CodeReport]
        //    WHERE ID = 'NCLP' AND RecordStatus<> 99             --Bắt đầu cơm cộng là kế mã nghĩ luân phiên
        //    END
        //--Chèn thay thế vị trí kế tiếp vừa tìm được
        //SET @Position = @Position + 1

        //UPDATE[dbo].[HRM_CodeReport] SET[Rank] =[Rank] + 1 WHERE [Rank] >= @Position AND RecordStatus<>99
        //INSERT INTO[dbo].[HRM_CodeReport] (ID, Name, Type, TypeName, Rank,RecordStatus,CreatedBy,CreatedName)
        //VALUES(@ID, @RiceName, 2, N'Cơm cộng theo mã', @Position,0,@CreatedBy,@CreatedName)";
        //            string zConnectionString = ConnectDataBase.ConnectionString;
        //            try
        //            {
        //                SqlConnection zConnect = new SqlConnection(zConnectionString);
        //                zConnect.Open();
        //                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //                zCommand.Parameters.Add("@ID", SqlDbType.NVarChar).Value = "P"+ID.Trim().PadLeft(2, '0');// VD: PX0C
        //                zCommand.Parameters.Add("@RiceName", SqlDbType.NVarChar).Value = ID.Trim();// VD: C
        //                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = SessionUser.UserLogin.Key;
        //                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = SessionUser.UserLogin.EmployeeName;
        //                zCommand.ExecuteNonQuery().ToString();
        //                zResult = true;
        //                zCommand.Dispose();
        //                zConnect.Close();
        //            }
        //            catch (Exception ex)
        //            {
        //                zResult = false;
        //                string zstrMessage = ex.ToString();
        //            }
        //            return zResult;
        //        }
        //        //Insert cột vào bảng chấm công - cơm trừ
        //        public static bool Add_RiceMinus(string ID)
        //        {
        //            bool zResult = false;
        //            string zSQL = @"
        //DECLARE @Position int =0 
        //--1.Tạo cột theo mã cơm
        //--Lấy vị trí cuối cùng của mã cơm trừ
        //SELECT TOP 1 @Position =[Rank] FROM [dbo].[HRM_CodeReport]
        //WHERE Type=3 AND RecordStatus <> 99 ORDER BY [Rank] DESC

        //IF(@Position='') 
        //	BEGIN
        //	SELECT TOP 1 @Position =[Rank] FROM [dbo].[HRM_CodeReport]
        //	WHERE Type=2 AND RecordStatus <> 99 ORDER BY [Rank] DESC        --Bắt đầu cơm cộng là kế mã cơm cộng
        //	END
        //IF(@Position='') 
        //	BEGIN
        //	SELECT @Position =[Rank] FROM [dbo].[HRM_CodeReport]
        //	WHERE ID='NCLP' AND   RecordStatus <> 99                        --Bắt đầu cơm cộng là kế mã nghĩ luân phiên
        //	END
        //--Chèn thay thế vị trí kế tiếp vừa tìm được
        //SET @Position =@Position +1

        //UPDATE[dbo].[HRM_CodeReport] SET[Rank] =[Rank] + 1 WHERE [Rank] >= @Position AND RecordStatus<>99
        //INSERT INTO[dbo].[HRM_CodeReport] (ID, Name, Type, TypeName, Rank,RecordStatus,CreatedBy,CreatedName)
        //VALUES(@ID, @RiceName, 3, N'Cơm trừ theo mã', @Position,0,@CreatedBy,@CreatedName)";
        //            string zConnectionString = ConnectDataBase.ConnectionString;
        //            try
        //            {
        //                SqlConnection zConnect = new SqlConnection(zConnectionString);
        //                zConnect.Open();
        //                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //                zCommand.Parameters.Add("@ID", SqlDbType.NVarChar).Value = "P" + ID.Trim();// VD: PX0T
        //                zCommand.Parameters.Add("@RiceName", SqlDbType.NVarChar).Value = ID.Trim();// VD: T
        //                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = SessionUser.UserLogin.Key;
        //                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = SessionUser.UserLogin.EmployeeName;
        //                zCommand.ExecuteNonQuery().ToString();
        //                zResult = true;
        //                zCommand.Dispose();
        //                zConnect.Close();
        //            }
        //            catch (Exception ex)
        //            {
        //                zResult = false;
        //                string zstrMessage = ex.ToString();
        //            }
        //            return zResult;
        //        }


        //        public static bool Delete_Rice_HRM(string ID)
        //        {
        //            bool zResult = false;
        //            string zSQL = @"
        //DECLARE @Position int =0 
        //--1.Tạo cột theo mã cơm
        //--Lấy vị trí cuối cùng của mã cơm trừ
        //SELECT @Position =[Rank] FROM [dbo].[HRM_CodeReport]
        //WHERE  RecordStatus <> 99 AND ID = @ID
        //IF(@Position >0)
        //    UPDATE[dbo].[HRM_CodeReport] SET[Rank] =[Rank] -1 WHERE [Rank] > @Position AND RecordStatus<>99 
        //DELETE FROM [dbo].[HRM_CodeReport] WHERE ID = @ID";
        //            string zConnectionString = ConnectDataBase.ConnectionString;
        //            try
        //            {
        //                SqlConnection zConnect = new SqlConnection(zConnectionString);
        //                zConnect.Open();
        //                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //                zCommand.Parameters.Add("@ID", SqlDbType.NVarChar).Value = "P" + ID.Trim() ;// VD: PX0C
        //                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = SessionUser.UserLogin.Key;
        //                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = SessionUser.UserLogin.EmployeeName;
        //                zCommand.ExecuteNonQuery().ToString();
        //                zResult = true;
        //                zCommand.Dispose();
        //                zConnect.Close();
        //            }
        //            catch (Exception ex)
        //            {
        //                zResult = false;
        //                string zstrMessage = ex.ToString();
        //            }
        //            return zResult;
        //        }
    }
}
