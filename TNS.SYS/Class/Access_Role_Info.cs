﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TN.Connect;

namespace TNS.SYS
{
    public class Access_Role_Info
    {
        #region [ Field Name ]
        private string _UserKey = "";
        private string _RoleKey = "";
        private string _RoleID = "";
        private int _RoleAll = 0;
        private int _RoleRead = 0;
        private int _RoleAdd = 0;
        private int _RoleEdit = 0;
        private int _RoleDel = 0;
        private int _RoleApprove = 0;
        private int _RecordStatus = 0;
        private DateTime _CreatedOn;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _Message = "";
        private string _AccessBranch = "";
        #endregion

        #region [ Properties ]
        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        public string UserKey
        {
            get { return _UserKey; }
            set { _UserKey = value; }
        }
        public string RoleKey
        {
            get { return _RoleKey; }
            set { _RoleKey = value; }
        }
        public int RoleAll
        {
            get { return _RoleAll; }
            set { _RoleAll = value; }
        }
        public int RoleRead
        {
            get { return _RoleRead; }
            set { _RoleRead = value; }
        }
        public int RoleAdd
        {
            get { return _RoleAdd; }
            set { _RoleAdd = value; }
        }
        public int RoleEdit
        {
            get { return _RoleEdit; }
            set { _RoleEdit = value; }
        }
        public int RoleDel
        {
            get { return _RoleDel; }
            set { _RoleDel = value; }
        }
        public int RoleApprove
        {
            get { return _RoleApprove; }
            set { _RoleApprove = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public string AccessBranch
        {
            get
            {
                return _AccessBranch;
            }

            set
            {
                _AccessBranch = value;
            }
        }
        #endregion

        #region [ Constructor Get Information ]
        public Access_Role_Info()
        {
        }
        public Access_Role_Info(string UserKey,string RoleKey)
        {
            string zSQL = "SELECT * FROM SYS_Access_Role WHERE UserKey = @UserKey AND RoleKey = @RoleKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserKey",SqlDbType.NVarChar).Value = UserKey;
                zCommand.Parameters.Add("@RoleKey", SqlDbType.NVarChar).Value = RoleKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _UserKey = zReader["UserKey"].ToString().Trim();
                    _RoleKey = zReader["RoleKey"].ToString().Trim();
                    _RoleID = zReader["RoleID"].ToString().Trim();
                    if (zReader["RoleAll"] != DBNull.Value)
                        _RoleAll = int.Parse(zReader["RoleAll"].ToString());
                    if (zReader["RoleRead"] != DBNull.Value)
                        _RoleRead = int.Parse(zReader["RoleRead"].ToString());
                    if (zReader["RoleAdd"] != DBNull.Value)
                        _RoleAdd = int.Parse(zReader["RoleAdd"].ToString());
                    if (zReader["RoleEdit"] != DBNull.Value)
                        _RoleEdit = int.Parse(zReader["RoleEdit"].ToString());
                    if (zReader["RoleDel"] != DBNull.Value)
                        _RoleDel = int.Parse(zReader["RoleDel"].ToString());
                    if (zReader["RoleApprove"] != DBNull.Value)
                        _RoleApprove = int.Parse(zReader["RoleApprove"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        public void  CheckRoleForm(string UserKey, string RoleID)
        {
            string zSQL = @"SELECT *,[dbo].[Fn_Access_Branch]( @UserKey) AS AccessBranch FROM SYS_Access_Role 
WHERE RecordStatus != 99
AND UserKey = @UserKey
AND RoleID=@RoleID";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = UserKey;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = RoleID;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _UserKey = zReader["UserKey"].ToString().Trim();
                    _RoleKey = zReader["RoleKey"].ToString().Trim();
                    _RoleID = zReader["RoleID"].ToString().Trim();
                    if (zReader["RoleAll"] != DBNull.Value)
                        _RoleAll = int.Parse(zReader["RoleAll"].ToString());
                    if (zReader["RoleRead"] != DBNull.Value)
                        _RoleRead = int.Parse(zReader["RoleRead"].ToString());
                    if (zReader["RoleAdd"] != DBNull.Value)
                        _RoleAdd = int.Parse(zReader["RoleAdd"].ToString());
                    if (zReader["RoleEdit"] != DBNull.Value)
                        _RoleEdit = int.Parse(zReader["RoleEdit"].ToString());
                    if (zReader["RoleDel"] != DBNull.Value)
                        _RoleDel = int.Parse(zReader["RoleDel"].ToString());
                    if (zReader["RoleApprove"] != DBNull.Value)
                        _RoleApprove = int.Parse(zReader["RoleApprove"].ToString());
                    _AccessBranch = zReader["AccessBranch"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion

        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "SYS_Access_Role_INSERT";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = _UserKey.Trim();
                zCommand.Parameters.Add("@RoleKey", SqlDbType.NVarChar).Value = _RoleKey.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID.Trim();
                zCommand.Parameters.Add("@RoleAll", SqlDbType.Int).Value = _RoleAll;
                zCommand.Parameters.Add("@RoleRead", SqlDbType.Int).Value = _RoleRead;
                zCommand.Parameters.Add("@RoleAdd", SqlDbType.Int).Value = _RoleAdd;
                zCommand.Parameters.Add("@RoleEdit", SqlDbType.Int).Value = _RoleEdit;
                zCommand.Parameters.Add("@RoleDel", SqlDbType.Int).Value = _RoleDel;
                zCommand.Parameters.Add("@RoleApprove", SqlDbType.Int).Value = _RoleApprove;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "SYS_Access_Role_UPDATE";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = _UserKey.Trim();
                zCommand.Parameters.Add("@RoleKey", SqlDbType.NVarChar).Value = _RoleKey.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID.Trim();
                zCommand.Parameters.Add("@RoleAll", SqlDbType.Int).Value = _RoleAll;
                zCommand.Parameters.Add("@RoleRead", SqlDbType.Int).Value = _RoleRead;
                zCommand.Parameters.Add("@RoleAdd", SqlDbType.Int).Value = _RoleAdd;
                zCommand.Parameters.Add("@RoleEdit", SqlDbType.Int).Value = _RoleEdit;
                zCommand.Parameters.Add("@RoleDel", SqlDbType.Int).Value = _RoleDel;
                zCommand.Parameters.Add("@RoleApprove", SqlDbType.Int).Value = _RoleApprove;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "SYS_Access_Role_DELETE";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@UserKey",SqlDbType.NVarChar).Value = _UserKey;
                zCommand.Parameters.Add("@RoleKey", SqlDbType.NVarChar).Value = _RoleKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
