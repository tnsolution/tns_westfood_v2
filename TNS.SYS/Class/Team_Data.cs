﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;

namespace TNS.SYS
{
    public class Team_Data
    {
        /// <summary>
        /// Danh sách tổ nhóm
        /// </summary>
        /// <param name="WorkDif">true > Lấy thêm nhóm làm công việc khác, fase > không lấy nhóm làm công việc khác</param>
        /// <returns></returns>
        public static DataTable List_Team(bool WorkDif)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT TeamKey, TeamName, TeamID FROM SYS_Team WHERE RecordStatus < 99";
            if (!WorkDif)
                zSQL += "AND TeamKey <> 98";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List_Team()
        {
            DataTable zTable = new DataTable();
            //Lấy tất cả nhóm trực tiếp sx  và khác bộ phận hỗ trợ sx
            string zSQL = @"SELECT TeamKey, TeamName, TeamID FROM SYS_Team 
                            WHERE RecordStatus <> 99 AND BranchKey=4 AND  DepartmentKey != 26
                            ORDER BY Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable List()
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.*,B.DepartmentName AS DepartmentName,C.BranchName AS BranchName FROM SYS_Team A
LEFT JOIN SYS_Department B ON B.DepartmentKey = A.DepartmentKey
LEFT JOIN SYS_Branch C ON C.BranchKey = A.BranchKey
WHERE A.RecordStatus != 99 ORDER BY A.RANK";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable Search(int BranchKey,int DepartmentKey,string Name)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.TeamKey,A.TeamID,A.TeamName,B.DepartmentName,C.BranchName,A.Rank,A.AccountCode,A.AccountCode2,A.Description FROM SYS_Team A
            LEFT JOIN [dbo].[SYS_Department] B ON B.DepartmentKey = A.DepartmentKey
            LEFT JOIN SYS_Branch C ON C.BranchKey = A.BranchKey 
            WHERE (A.TeamName LIKE @Name OR A.TeamID LIKE @Name) AND   A.RecordStatus <> 99 ";
            if (BranchKey > 0)
                zSQL += " AND A.BranchKey =@BranchKey ";
            if (DepartmentKey > 0)
                zSQL += " AND A.DepartmentKey =@DepartmentKey ";
         zSQL +=" ORDER BY C.Rank,B.Rank, A.Rank ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BranchKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static int CountID(string Name)
        {
            int zResult = 0;
            string zSQL = "SELECT COUNT(TeamID) FROM SYS_Team WHERE TeamID = @Name AND   RecordStatus <> 99 ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = Name.Trim();
                zResult = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
        public static int GetTeamKeyCurrent(string EmployeeKey,DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, DateWrite.Day, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            int zResult = 0;
            string zSQL = "SELECT [dbo].[Fn_TeamKeyWorkingHistory] (@EmployeeKey,@FromDate,@ToDate) ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zResult = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }

    }
}
