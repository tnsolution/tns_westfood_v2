﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
using TNS.Misc;

namespace TNS.SYS
{
    public class DateClose_Data
    {
        public static DataTable List()
        {
            string PivotDate = "";
            for (int i = 1; i <= 12; i++)
            {
                PivotDate += "[" + i + "],";
            }
            if (PivotDate.Contains(","))
            {
                PivotDate = PivotDate.Remove(PivotDate.LastIndexOf(","));
            }

            DataTable zTable = new DataTable();
            string zSQL = @"
            CREATE TABLE #temp(
            [Year] INT,
            [Month] INT,
            Val NVARCHAR(250),
            )
            INSERT INTO #temp
            SELECT Year(DateClose) AS [Year],
            Month(DateClose) AS  [Month],
            CAST(AutoKey AS NVARCHAR(50))+'|'+CAST(Month(DateClose) AS NVARCHAR(50))+'|'+ CAST(Status AS NVARCHAR(50)) AS Val 
            FROM [dbo].[SYS_DateClose]

            ;WITH BangTam AS (
	            SELECT * FROM(
	            SELECT [Year],[Month],Val FROM #temp)
	            X PIVOT (
	            MAX(Val)
	            FOR [Month] IN @DetaiPivot ) P 
            )
            SELECT * FROM BangTam
            DROP TABLE #temp
	
            ";

            zSQL = zSQL.Replace("@DetaiPivot", "(" + PivotDate + ")");
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static bool MoLaiDaKhoa(DateTime Date)
        {
            bool zResult = false;
            string zSQL = @"
            UPDATE [dbo].[SYS_DateClose] SET Status=0  WHERE RecordStatus <> 99 ANd  DateClose >= @Date 
            ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Date", SqlDbType.DateTime).Value = Date;
                zCommand.ExecuteNonQuery();
                zCommand.Dispose();
                zConnect.Close();
                zResult = true;
            }
            catch (Exception ex)
            {
                zResult = false;
            }
            return zResult;
        }
        public static int KiemTraThangTruocDaKhoaChua(DateTime DateView)
        {
            DateView =DateView.AddMonths(-1);
            DateTime FromDate = new DateTime(DateView.Year, DateView.Month, 1, 0, 0, 0);
            DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            int zResult = 0;

            string zSQL = @"
            SELECT Status FROM [dbo].[SYS_DateClose]  WHERE RecordStatus <> 99 ANd  DateClose = @Date 
            ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Date", SqlDbType.DateTime).Value = ToDate;
                zResult =zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                
            }
            return zResult;
        }

        public static DateTime GetDateLock()
        {
            DateTime zResult = DateTime.MinValue;

            string zSQL = @"
            SELECT TOP 1 DateClose FROM [dbo].[SYS_DateClose]  WHERE RecordStatus <> 99 
            AND RecordStatus <> 99
            AND Status=1
            ORDER BY DateClose DESC 
            ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zResult = DateTime.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {

            }
            return zResult;
        }
    }
}
