﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
namespace TNS.SYS
{
    public class Menu_Info
    {

        #region [ Field Name ]
        private int _MenuKey = 0;
        private string _Module = "";
        private string _MenuName = "";
        private string _MenuLink = "";
        private string _MenuID = "";
        private string _ParamName = "";
        private string _ParamValue = "";
        private string _Icon = "";
        private int _MenuLevel = 0;
        private int _Parent = 0;
        private int _Type = 0;
        private int _Rank = 0;
        private int _Ref = 0;
        private int _RecordStatus = 0;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _CreatedOn;
        private DateTime _ModifiedOn;
        private string _ModifiedName = "";
        private string _ModifiedBy = "";
        private string _RoleID = "";
        private string _Message = "";
        #endregion
        #region [ Properties ]
        public bool IsCommandOK
        {
            get
            {
                int zNumber = 0;
                int.TryParse(_Message.Substring(0, 1), out zNumber);
                if (zNumber <= 3) return true; else return false;
            }
        }
       
        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        public int Key
        {
            get { return _MenuKey; }
            set { _MenuKey = value; }
        }
        public string Module
        {
            get { return _Module; }
            set { _Module = value; }
        }
        public string MenuName
        {
            get { return _MenuName; }
            set { _MenuName = value; }
        }
        public string MenuLink
        {
            get { return _MenuLink; }
            set { _MenuLink = value; }
        }
        public string MenuID
        {
            get { return _MenuID; }
            set { _MenuID = value; }
        }
        public string ParamName
        {
            get { return _ParamName; }
            set { _ParamName = value; }
        }
        public string ParamValue
        {
            get { return _ParamValue; }
            set { _ParamValue = value; }
        }
        public string Icon
        {
            get { return _Icon; }
            set { _Icon = value; }
        }
        public int MenuLevel
        {
            get { return _MenuLevel; }
            set { _MenuLevel = value; }
        }
        public int Parent
        {
            get { return _Parent; }
            set { _Parent = value; }
        }
        public int Type
        {
            get { return _Type; }
            set { _Type = value; }
        }
        public int Rank
        {
            get { return _Rank; }
            set { _Rank = value; }
        }
        public int Ref
        {
            get { return _Ref; }
            set { _Ref = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion
        #region [ Constructor Get Information ]
        public Menu_Info()
        {
        }
        public Menu_Info(int MenuKey)
        {
            string zSQL = "SELECT * FROM SYS_Menu WHERE MenuKey = @MenuKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@MenuKey", SqlDbType.Int).Value = MenuKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["MenuKey"] != DBNull.Value)
                        _MenuKey = int.Parse(zReader["MenuKey"].ToString());
                    _Module = zReader["Module"].ToString().Trim();
                    _MenuName = zReader["MenuName"].ToString().Trim();
                    _MenuLink = zReader["MenuLink"].ToString().Trim();
                    _MenuID = zReader["MenuID"].ToString().Trim();
                    _ParamName = zReader["ParamName"].ToString().Trim();
                    _ParamValue = zReader["ParamValue"].ToString().Trim();
                    _Icon = zReader["Icon"].ToString().Trim();
                    if (zReader["MenuLevel"] != DBNull.Value)
                        _MenuLevel = int.Parse(zReader["MenuLevel"].ToString());
                    if (zReader["Parent"] != DBNull.Value)
                        _Parent = int.Parse(zReader["Parent"].ToString());
                    if (zReader["Type"] != DBNull.Value)
                        _Type = int.Parse(zReader["Type"].ToString());
                    if (zReader["Rank"] != DBNull.Value)
                        _Rank = int.Parse(zReader["Rank"].ToString());
                    if (zReader["Ref"] != DBNull.Value)
                        _Ref = int.Parse(zReader["Ref"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE SYS_Menu SET Rank = Rank +1 WHERE Rank >= @Rank AND Parent = @Parent AND RecordStatus <> 99"
         + "INSERT INTO SYS_Menu ("
         + " Module ,MenuName ,MenuLink ,MenuID ,ParamName ,ParamValue ,Icon ,MenuLevel ,Parent ,Type ,Rank ,Ref ,RecordStatus ,CreatedBy ,CreatedName ,CreatedOn ,ModifiedOn ,ModifiedName ,ModifiedBy ) "
         + " VALUES ( "
         + "@Module ,@MenuName ,@MenuLink ,@MenuID ,@ParamName ,@ParamValue ,@Icon ,@MenuLevel ,@Parent ,@Type ,@Rank ,@Ref ,@RecordStatus ,@CreatedBy ,@CreatedName ,GETDATE(),GETDATE(),@ModifiedName ,@ModifiedBy ) ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@Module", SqlDbType.NVarChar).Value = _Module.Trim();
                zCommand.Parameters.Add("@MenuName", SqlDbType.NVarChar).Value = _MenuName.Trim();
                zCommand.Parameters.Add("@MenuLink", SqlDbType.NVarChar).Value = _MenuLink.Trim();
                zCommand.Parameters.Add("@MenuID", SqlDbType.NVarChar).Value = _MenuID.Trim();
                zCommand.Parameters.Add("@ParamName", SqlDbType.NVarChar).Value = _ParamName.Trim();
                zCommand.Parameters.Add("@ParamValue", SqlDbType.NVarChar).Value = _ParamValue.Trim();
                zCommand.Parameters.Add("@Icon", SqlDbType.NVarChar).Value = _Icon.Trim();
                zCommand.Parameters.Add("@MenuLevel", SqlDbType.Int).Value = _MenuLevel;
                zCommand.Parameters.Add("@Parent", SqlDbType.Int).Value = _Parent;
                zCommand.Parameters.Add("@Type", SqlDbType.Int).Value = _Type;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zCommand.Parameters.Add("@Ref", SqlDbType.Int).Value = _Ref;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
               
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE SYS_Menu SET "
                        + " Module = @Module,"
                        + " MenuName = @MenuName,"
                        + " MenuLink = @MenuLink,"
                        + " MenuID = @MenuID,"
                        + " ParamName = @ParamName,"
                        + " ParamValue = @ParamValue,"
                        + " Icon = @Icon,"
                        + " MenuLevel = @MenuLevel,"
                        + " Parent = @Parent,"
                        + " Type = @Type,"
                        + " Rank = @Rank,"
                        + " Ref = @Ref,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = GETDATE(),"
                        + " ModifiedName = @ModifiedName,"
                        + " ModifiedBy = @ModifiedBy"
                       + " WHERE MenuKey = @MenuKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@MenuKey", SqlDbType.Int).Value = _MenuKey;
                zCommand.Parameters.Add("@Module", SqlDbType.NVarChar).Value = _Module.Trim();
                zCommand.Parameters.Add("@MenuName", SqlDbType.NVarChar).Value = _MenuName.Trim();
                zCommand.Parameters.Add("@MenuLink", SqlDbType.NVarChar).Value = _MenuLink.Trim();
                zCommand.Parameters.Add("@MenuID", SqlDbType.NVarChar).Value = _MenuID.Trim();
                zCommand.Parameters.Add("@ParamName", SqlDbType.NVarChar).Value = _ParamName.Trim();
                zCommand.Parameters.Add("@ParamValue", SqlDbType.NVarChar).Value = _ParamValue.Trim();
                zCommand.Parameters.Add("@Icon", SqlDbType.NVarChar).Value = _Icon.Trim();
                zCommand.Parameters.Add("@MenuLevel", SqlDbType.Int).Value = _MenuLevel;
                zCommand.Parameters.Add("@Parent", SqlDbType.Int).Value = _Parent;
                zCommand.Parameters.Add("@Type", SqlDbType.Int).Value = _Type;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zCommand.Parameters.Add("@Ref", SqlDbType.Int).Value = _Ref;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"UPDATE SYS_Menu SET Rank = Rank - 1 WHERE Rank > @Rank AND Parent = @Parent AND RecordStatus <> 99
                            UPDATE SYS_Menu SET [RecordStatus] = 99, [ModifiedOn] = GETDATE(), ModifiedBy = @ModifiedBy, 
                            ModifiedName = @ModifiedName WHERE MenuKey = @MenuKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@MenuKey", SqlDbType.Int).Value = _MenuKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@Parent", SqlDbType.Int).Value = _Parent;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_MenuKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        #endregion
    }
}
