﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
namespace TNS.SYS
{
    public class Menu_Data
    {
        public static DataTable MenuMain()
        {
            DataTable zTable = new DataTable();
            string zSQL = "select * from [dbo].[SYS_Menu] where RecordStatus <> 99 AND MenuLevel=0 ORDER BY [Rank] ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable MenuSub()
        {
            DataTable zTable = new DataTable();
            string zSQL = "select * from [dbo].[SYS_Menu] where RecordStatus <> 99 AND MenuLevel=1 ORDER BY [Rank]";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static int CountID(string Name)
        {
            int zResult = 0;
            string zSQL = "SELECT COUNT(MenuID) FROM SYS_Menu WHERE MenuID = @Name AND   RecordStatus <> 99 ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = Name.Trim();
                zResult = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
        public static void UpdateRole(string MenuID,string MenuName,int Rank,int Type,string Parent)
        {
            int zResult = 0;
            string zSQL = @"

UPDATE SYS_Role SET RoleName = @RoleName,Rank =@Rank,Style =@Style,Parent=@Parent WHERE RoleID = @RoleID ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = MenuID.Trim();
                zCommand.Parameters.Add("@RoleName", SqlDbType.NVarChar).Value = MenuName.Trim();
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Rank;
                zCommand.Parameters.Add("@Style", SqlDbType.Int).Value = Type;
                zCommand.Parameters.Add("@Parent", SqlDbType.NVarChar).Value = Parent.Trim();
                zResult = zCommand.ExecuteNonQuery();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
        }
    }
}
