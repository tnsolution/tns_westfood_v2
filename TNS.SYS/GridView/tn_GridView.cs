﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace TN.Toolbox
{
    public class TN_GridView : DataGridView
    {
        #region [Variables]
        public bool AllowAddRow = false;
        private Color _GVHoverColor = Color.LightGreen;  // = Color.LightGreen;//For hover backcolor
        private Color _GVbackColor = Color.FromArgb(181, 213, 255);   //For backColor     Color.LightSkyBlue; // 
        private Point _hotSpot;

        private List<GV_Column> _Columns;
        private bool _IsPressKeyDown;
        private int _RowEditNumber;
        private int _ColumnEditNumber;
        #endregion

        public TN_GridView()
        {
            _Columns = new List<GV_Column>();
        }

        #region [ Public Properties ]
        /// <summary>
        /// Thêm dòng tổng Gridview
        /// </summary>
        public bool IsSum = false;
        /// <summary>
        /// Set width cột cuối của girdview full qua phải
        /// </summary>
        public bool FillLastColumn = false;
        #endregion

        #region [ Function ]
        /// <summary>
        /// Khởi tạo Gridivew
        /// </summary>
        public void Init()
        {
            AddColumn();
            DrawGVStyle();
            SetDoubleBuffered();
        }

        /// <summary>
        /// Thêm cột Gridview
        /// </summary>
        private void AddColumn()
        {
            int LastColumn = 0;
            for (int i = 0; i < _Columns.Count; i++)
            {
                //customer external control
                switch (_Columns[i].ComboBoxName)
                {
                    case "ListView":
                        _Columns[i].ComboBoxLV.SelectedIndexChanged += ComboBoxLV_SelectedIndexChanged;
                        _Columns[i].ComboBoxLV.Click += ComboBoxLV_Click;
                        _Columns[i].ComboBoxLV.Leave += ComboBoxLV_Leave;
                        _Columns[i].ComboBoxLV.ItemActivate += ComboBoxLV_ItemActivate;
                        _Columns[i].ComboBoxLV.KeyUp += ComboBoxLV_KeyUp;
                        this.Parent.Controls.Add(_Columns[i].ComboBoxLV);
                        break;
                }
                //------------------------------------
                switch (_Columns[i].Style)
                {
                    case "Number":
                        base.Columns.Add(_Columns[i].ColumnName, _Columns[i].HeaderText);
                        base.Columns[i].Width = _Columns[i].Width;
                        base.Columns[i].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                        break;

                    case "CheckBox":
                        DataGridViewCheckBoxColumn zColumn = new DataGridViewCheckBoxColumn();
                        zColumn.Name = _Columns[i].ColumnName;
                        zColumn.HeaderText = _Columns[i].HeaderText;
                        base.Columns.Add(zColumn);
                        break;

                    case "ComboBox":
                        DataGridViewComboBoxColumn cbo = new DataGridViewComboBoxColumn();
                        cbo.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
                        cbo.HeaderText = _Columns[i].HeaderText;
                        cbo.Name = _Columns[i].ColumnName;
                        cbo.Width = _Columns[i].Width;
                        cbo.DataSource = _Columns[i].TableSource;
                        base.Columns.Add(cbo);
                        break;

                    default:
                        base.Columns.Add(_Columns[i].ColumnName, _Columns[i].HeaderText);
                        base.Columns[i].Width = _Columns[i].Width;
                        if (_Columns[i].IsCenter)
                            base.Columns[i].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                        break;
                }

                LastColumn = i;
            }
            if (FillLastColumn)
                base.Columns[LastColumn].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }

        private bool IsRowEmpty(int RowIndex)
        {
            for (int i = 0; i < _Columns.Count; i++)
            {
                if (_Columns[i].CheckEmpty)
                    if (base.Rows[RowIndex].Cells[i].Value == null)
                        return true;
            }
            return false;
        }
        private void SetDoubleBuffered()
        {
            Type dgvType = base.GetType();
            PropertyInfo pi = dgvType.GetProperty("DoubleBuffered",
                BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.SetProperty);
            pi.SetValue(this, true, null);
        }
        /// <summary>
        /// Tính tổng dòng
        /// </summary>
        /// <param name="IsEdit">true danh cho khi thêm dòng = tay, hoặc xóa dòng, false khi load data gridview lần đầu</param>
        public void GetSum(bool IsEdit)
        {
            base.CommitEdit(DataGridViewDataErrorContexts.Commit);
            double[] Sum = new double[base.Columns.Count];

            try
            {

                if (IsEdit)
                {
                    base.Rows.RemoveAt(base.Rows.Count - 1);
                }

                base.Rows.Add();

                if (AllowAddRow)
                {
                    base.Rows.Add();
                }

                for (int i = 0; i < base.Rows.Count; i++)
                {
                    DataGridViewRow Gvr = base.Rows[i];
                    for (int j = 0; j < _Columns.Count; j++)
                    {
                        Sum[j] += 0;
                        if (_Columns[j].IsSum && Gvr.Visible)
                        {
                            if (Gvr.Cells[j].Value != DBNull.Value &&
                                Gvr.Cells[j].Value != null)
                                Sum[j] += double.Parse(Gvr.Cells[j].Value.ToString());
                        }
                    }
                }

                DataGridViewRow GvrSum = base.Rows[base.Rows.Count - 1];
                GvrSum.ReadOnly = true;
                GvrSum.DefaultCellStyle.Font = new Font("Tahoma", 10F, FontStyle.Bold);
                for (int i = 0; i < Sum.Length; i++)
                {
                    if (_Columns[i].IsSum)
                        GvrSum.Cells[i].Value = Sum[i].ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void ColumnAdd(string columnName, string headerText)
        {
            GV_Column zCol = new GV_Column(columnName, headerText);
            _Columns.Add(zCol);
        }
        public void ColumnAdd(string columnName, string headerText, int width)
        {
            GV_Column zCol = new GV_Column(columnName, headerText, width);
            _Columns.Add(zCol);
        }
        public void ColumnAdd(GV_Column column)
        {
            _Columns.Add(column);
        }

        //----- Render Style
        private void DrawGVStyle()
        {
            base.ColumnHeadersHeight = 25;
            base.BackgroundColor = Color.White;
            base.GridColor = Color.FromArgb(227, 239, 255);
            base.DefaultCellStyle.ForeColor = Color.Navy;
            base.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            base.ClipboardCopyMode = DataGridViewClipboardCopyMode.Disable;
            base.EnableHeadersVisualStyles = false;
            base.RowHeadersVisible = false;
            base.AllowUserToAddRows = false;
            base.AllowUserToResizeRows = false;
            base.AllowUserToResizeColumns = true;
            base.AllowUserToDeleteRows = false;
            base.MultiSelect = false;
            base.ShowCellErrors = true;
            base.ShowRowErrors = true;
            base.ClipboardCopyMode = DataGridViewClipboardCopyMode.Disable;
            base.SelectionMode = DataGridViewSelectionMode.CellSelect;
            base.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.DisplayedCells;
            base.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;
            base.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            base.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;
            base.DefaultCellStyle.Font = new Font("Tahoma", 9);
        }
        private void RenderColumnHeader(Graphics g, Rectangle headerBounds, Color c)
        {
            int topHeight = 10;
            Rectangle topRect = new Rectangle(headerBounds.Left, headerBounds.Top + 1, headerBounds.Width, topHeight);
            RectangleF bottomRect = new RectangleF(headerBounds.Left, headerBounds.Top + 1 + topHeight, headerBounds.Width, headerBounds.Height - topHeight - 4);
            Color c1 = Color.FromArgb(180, c);
            using (SolidBrush brush = new SolidBrush(c1))
            {
                g.FillRectangle(brush, topRect);
                brush.Color = c;
                g.FillRectangle(brush, bottomRect);
            }
        }
        private void RenderColumnHeaderBorder(DataGridView GV, Graphics g, Rectangle headerBounds, int colIndex)
        {
            g.DrawRectangle(new Pen(Color.White, 0.1f), headerBounds.Left + 0.5f, headerBounds.Top + 0.5f, headerBounds.Width - 1f, headerBounds.Height - 1f);
            ControlPaint.DrawBorder(g, headerBounds,
                Color.Gray, 0, ButtonBorderStyle.Solid,
                Color.Gray, 0, ButtonBorderStyle.Solid,
                Color.Gray, colIndex != GV.ColumnCount - 1 ? 1 : 0, ButtonBorderStyle.Solid,
                Color.Gray, 1, ButtonBorderStyle.Solid);
        }
        #endregion

        #region[ ComboBox ListView]
        private void ComboBoxLV_KeyUp(object sender, KeyEventArgs e)
        {

        }
        private void ComboBoxLV_Click(object sender, EventArgs e)
        {
            ListView zLV = (ListView)sender;
            if (_Columns[_ColumnEditNumber].ComboBoxLV.Name == zLV.Name)
            {
                if (zLV.SelectedItems.Count > 0)
                {
                    int No = _Columns[_ColumnEditNumber].ShowColumn;
                    string zKey = "";
                    string zID = "";
                    string zName = "";
                    if (No <= 0)
                    {
                        zKey = zLV.SelectedItems[0].Tag.ToString();
                        zID = zLV.SelectedItems[0].Text;
                        zName = zLV.SelectedItems[0].SubItems[No].Text;
                    }
                    else
                    {
                        zKey = zLV.SelectedItems[0].Tag.ToString();
                        zID = zLV.SelectedItems[0].SubItems[No - 1].Text;
                        zName = zLV.SelectedItems[0].SubItems[No].Text;
                    }

                    base.Rows[_RowEditNumber].Cells[_ColumnEditNumber].Tag = zKey;
                    base.Rows[_RowEditNumber].Cells[_ColumnEditNumber].Value = zID;
                    base.Rows[_RowEditNumber].Cells[_Columns[_ColumnEditNumber].DisplayNextColumn].Value = zName;
                }
            }
        }
        private void ComboBoxLV_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListView zLV = (ListView)sender;
            if (_Columns[_ColumnEditNumber].ComboBoxLV.Name == zLV.Name)
            {
                if (_IsPressKeyDown)
                {
                    if (zLV.SelectedItems.Count > 0)
                    {
                        string zKey = "";
                        string zID = "";
                        string zName = "";
                        int No = _Columns[_ColumnEditNumber].ShowColumn;
                        if (No <= 0)
                        {
                            zKey = zLV.SelectedItems[0].Tag.ToString();
                            zID = zLV.SelectedItems[0].Text;
                            zName = zLV.SelectedItems[0].SubItems[No].Text;
                        }
                        else
                        {
                            zKey = zLV.SelectedItems[0].Tag.ToString();
                            zID = zLV.SelectedItems[0].SubItems[No - 1].Text;
                            zName = zLV.SelectedItems[0].SubItems[No].Text;
                        }

                        base.Rows[_RowEditNumber].Cells[_ColumnEditNumber].Tag = zKey;
                        base.Rows[_RowEditNumber].Cells[_ColumnEditNumber].Value = zID;
                        base.Rows[_RowEditNumber].Cells[_Columns[_ColumnEditNumber].DisplayNextColumn].Value = zName;


                    }
                }
            }
        }
        private void ComboBoxLV_Leave(object sender, EventArgs e)
        {
            ListView zLV = (ListView)sender;
            if (_Columns[_ColumnEditNumber].ComboBoxLV.Name == zLV.Name)
            {
                zLV.Visible = false;
                if (_RowEditNumber >= 0)
                {
                    base.Focus();
                    base.CurrentCell = base.Rows[_RowEditNumber].Cells[_ColumnEditNumber];
                    object CellValue = base.CurrentCell.Value;
                    for (int i = 0; i < base.RowCount - 1; i++)
                    {
                        if (CellValue != null)
                        {
                            var isExist = base.Rows.Cast<DataGridViewRow>().Count(c => c.Cells[1].EditedFormattedValue.ToString() == CellValue.ToString()) > 1;
                            if (isExist)
                            {
                                base.Rows[i].Cells[_ColumnEditNumber].ErrorText = "Mã số bị trùng";

                            }
                            else
                            {
                                base.Rows[i].ErrorText = "";
                            }
                        }
                    }
                }
            }
        }
        private void ComboBoxLV_ItemActivate(object sender, EventArgs e)
        {
            ListView zLV = (ListView)sender;
            if (_Columns[_ColumnEditNumber].ComboBoxLV.Name == zLV.Name)
            {
                if (zLV.SelectedItems.Count > 0)
                {
                    string zKey = "";
                    string zID = "";
                    string zName = "";
                    int No = _Columns[_ColumnEditNumber].ShowColumn;
                    if (No <= 0)
                    {
                        zKey = zLV.SelectedItems[0].Tag.ToString();
                        zID = zLV.SelectedItems[0].Text;
                        zName = zLV.SelectedItems[0].SubItems[No].Text;
                    }
                    else
                    {
                        zKey = zLV.SelectedItems[0].Tag.ToString();
                        zID = zLV.SelectedItems[0].SubItems[No - 1].Text;
                        zName = zLV.SelectedItems[0].SubItems[No].Text;
                    }

                    base.Rows[_RowEditNumber].Cells[_ColumnEditNumber].Tag = zKey;
                    base.Rows[_RowEditNumber].Cells[_ColumnEditNumber].Value = zID;
                    base.Rows[_RowEditNumber].Cells[_Columns[_ColumnEditNumber].DisplayNextColumn].Value = zName;

                    zLV.Visible = false;
                }
            }
        }
        #endregion

        #region  [ Override Gridview ]
        protected override void OnCellClick(DataGridViewCellEventArgs e)
        {
            base.OnCellClick(e);
            //base.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LemonChiffon;
        }
        protected override void OnRowsAdded(DataGridViewRowsAddedEventArgs e)
        {
            base.OnRowsAdded(e);

        }
        protected override void OnEditingControlShowing(DataGridViewEditingControlShowingEventArgs e)
        {
            base.OnEditingControlShowing(e);

            DataGridViewTextBoxEditingControl zEditingControl = (DataGridViewTextBoxEditingControl)e.Control;
            int zCol = base.CurrentCell.ColumnIndex;
            GV_Column zGVCol = _Columns[zCol];
            switch (zGVCol.Style)
            {
                case "Number":
                    zEditingControl.KeyPress += new KeyPressEventHandler(ControlNumber_KeyPress);
                    break;
                case "Date":
                    zEditingControl.KeyPress += new KeyPressEventHandler(ControlDate_KeyPress);
                    break;
                case "Text":
                    zEditingControl.KeyPress += new KeyPressEventHandler(ControlText_KeyPress);
                    e.Control.PreviewKeyDown += new PreviewKeyDownEventHandler(Control_PreviewKeyDown);
                    break;
            }
        }
        protected override void OnCellBeginEdit(DataGridViewCellCancelEventArgs e)
        {
            base.OnCellBeginEdit(e);

            _IsPressKeyDown = false;
            GV_Column zCol = _Columns[e.ColumnIndex];
            Rectangle zRect = base.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, false);

            if (zCol.ComboBoxName == "ListView")
            {
                int x = zRect.X + base.Left;
                int y = zRect.Y + base.Top + zRect.Height;
                zCol.SetBoundListView(x, y);
                zCol.ComboBoxLV.BringToFront();
                return;
            }
        }
        protected override void OnCellEndEdit(DataGridViewCellEventArgs e)
        {
            base.OnCellEndEdit(e);
            _ColumnEditNumber = e.ColumnIndex;
            _RowEditNumber = e.RowIndex;
            GV_Column zCol = _Columns[e.ColumnIndex];
            object CellValue = base.Rows[_RowEditNumber].Cells[_ColumnEditNumber].Value;
            if (zCol.ComboBoxName == "ListView")
            {
                if (!_IsPressKeyDown)
                {
                    zCol.ComboBoxLV.Visible = false;
                }
                else
                {
                    zCol.ComboBoxLV.Focus();
                    zCol.ComboBoxLV.Items[0].Selected = true;
                }
            }

            if (base.Rows[_RowEditNumber].Cells[_ColumnEditNumber].Value == null)
            {
                base.Rows[_RowEditNumber].Cells[_ColumnEditNumber].ErrorText = "";
            }
            else
            {
                switch (zCol.Style)
                {
                    case "Number":
                        double zNumber = 0;
                        if (!double.TryParse(base.Rows[_RowEditNumber].Cells[_ColumnEditNumber].Value.ToString(), out zNumber))
                        {
                            base.Rows[_RowEditNumber].Cells[_ColumnEditNumber].ErrorText = "Error Number";
                        }
                        else
                        {
                            base.Rows[_RowEditNumber].Cells[_ColumnEditNumber].ErrorText = "";
                            base.Rows[_RowEditNumber].Cells[_ColumnEditNumber].Value = zNumber.ToString();
                        }
                        break;
                    case "Date":
                        bool zIsError = false;

                        if (CellValue == null)
                        {
                            zIsError = true;
                        }
                        else
                        {
                            string CellContent = CellValue.ToString();
                            string[] zString = CellContent.Split('/');
                            string zDate = "";
                            if (zString.Length != 3)
                                zIsError = true;
                            else
                            {
                                if (zString[2].Length != 4)
                                    zIsError = true;
                                else
                                {
                                    zDate = zString[0].PadLeft(2, '0') + "/" + zString[1].PadLeft(2, '0') + "/" + zString[2];
                                }
                            }
                            if (!zIsError)
                            {
                                CultureInfo zProvider = CultureInfo.InvariantCulture;
                                DateTime zDateTime;
                                if (!DateTime.TryParseExact(zDate, "dd/MM/yyyy", zProvider, DateTimeStyles.None, out zDateTime))
                                {
                                    zIsError = true;
                                }
                                else
                                {
                                    base.Rows[_RowEditNumber].Cells[_ColumnEditNumber].Value = zDate;
                                }
                            }
                        }

                        if (zIsError)
                        {
                            base.Rows[_RowEditNumber].Cells[_ColumnEditNumber].ErrorText = "Error Date";
                        }
                        else
                        {
                            base.Rows[_RowEditNumber].Cells[_ColumnEditNumber].ErrorText = "";
                        }

                        break;
                    case "Text":
                        break;
                }
            }
        }
        protected override void OnRowValidated(DataGridViewCellEventArgs e)
        {
            base.OnRowValidated(e);
            if (IsSum &&
                 !IsRowEmpty(e.RowIndex) &&
                e.RowIndex == base.Rows.Count - 2)
            {
                GetSum(true);
            }

            for (int i = 0; i < base.RowCount - 1; i++)
            {
                base.Rows[i].Cells[0].Value = (i + 1);
            }
        }
        protected override void OnCellValueChanged(DataGridViewCellEventArgs e)
        {
            base.OnCellValueChanged(e);

        }
        protected override void OnRowEnter(DataGridViewCellEventArgs e)
        {
            base.OnRowEnter(e);
            base.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LemonChiffon;
        }
        protected override void OnRowLeave(DataGridViewCellEventArgs e)
        {
            base.OnRowLeave(e);
            base.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.White;
        }
        protected override void OnCellPainting(DataGridViewCellPaintingEventArgs e)
        {
            base.OnCellPainting(e);
            if (e.RowIndex == -1 && e.ColumnIndex > -1)
            {
                e.PaintBackground(e.CellBounds, true);
                RenderColumnHeader(e.Graphics, e.CellBounds, e.CellBounds.Contains(_hotSpot) ? _GVHoverColor : _GVbackColor);
                RenderColumnHeaderBorder(this, e.Graphics, e.CellBounds, e.ColumnIndex);
                using (Brush brush = new SolidBrush(e.CellStyle.ForeColor))
                {
                    using (StringFormat sf = new StringFormat() { LineAlignment = StringAlignment.Center, Alignment = StringAlignment.Center })
                    {
                        e.Graphics.DrawString(e.Value.ToString(), new Font("Tahoma", 9, FontStyle.Bold), brush, e.CellBounds, sf);
                    }
                }
                e.Handled = true;
            }
        }
        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            _hotSpot = e.Location;
        }
        protected override void OnMouseLeave(EventArgs e)
        {
            base.OnMouseLeave(e);
            _hotSpot = Point.Empty;
        }
        private void ControlNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }

            if (e.KeyChar == '.')
            {
                e.Handled = false;
            }
            if (e.KeyChar == ',')
            {
                e.Handled = false;
            }
        }
        private void ControlText_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (_Columns[base.CurrentCell.ColumnIndex].ComboBoxName == "ListView")
            {
                var zBox = sender as TextBox;
                if (zBox != null)
                {
                    GV_Column zCol = _Columns[base.CurrentCell.ColumnIndex];
                    string zContent = "";
                    if (char.IsLetter(e.KeyChar) || char.IsDigit(e.KeyChar))
                    {
                        zContent = zBox.Text + e.KeyChar.ToString();
                        zCol.FilterDataLV(zContent);
                    }
                    else
                    {
                        if (e.KeyChar == (Char)Keys.Back)
                        {
                            string zTemp = zBox.Text;
                            if (zTemp.Length > 0)
                                zContent = zTemp.Substring(0, zTemp.Length - 1);
                            zCol.FilterDataLV(zContent);
                        }
                    }
                }
            }

            e.Handled = false;
        }
        private void ControlDate_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
            if (e.KeyChar == '-')
            {
                e.Handled = false;
            }
            if (e.KeyChar == '/')
            {
                e.Handled = false;
            }
        }
        private void Control_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                _IsPressKeyDown = true;
            }
        }
        #endregion               
    }
}
