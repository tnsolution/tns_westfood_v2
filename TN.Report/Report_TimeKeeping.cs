﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using TN.Connect;

namespace TNS.Report
{
    public class Report_TimeKeeping
    {

        public static DataTable TG_LamTaiXuong_TheoCongNhan(DateTime FromDate, DateTime ToDate, int DepartmentKey, int TeamKey,string Search)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);


            string Fillter = "";


            if (DepartmentKey > 0)
                Fillter += " AND A.DepartmentKey =@DepartmentKey ";
            if (TeamKey > 0)
                Fillter += " AND A.TeamKey =@TeamKey ";
            if (Search.Length > 0)
            {
                Fillter += " AND (A.EmployeeID LIKE @Search OR A.EmployeeName LIKE @Search )";
            }

            string zSQL = @"
--declare @FromDate datetime ='2020-06-01 00:00:00'
--declare @ToDate datetime ='2020-06-30 23:59:59'
--declare @TeamKey int = 79

CREATE TABLE Temp
(
	[DepartmentKey] INT ,
	[TeamKey] INT,
	[TeamName] nvarchar(500) ,
	[TeamID] nvarchar(500) ,
	EmployeeKey nvarchar(500),
	EmployeeID nvarchar(500),
	EmployeeName nvarchar(500),
	[DateWrite] nvarchar(500),
	[TotalTime] FLOAT,
	[OverTime] FLOAT ,
	[DifferentTime] FLOAT 
)
--1.Lây BHXH
INSERT INTO Temp
SELECT [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
TeamKey,TeamName,TeamID,EmployeeKey,EmployeeID,EmployeeName,
CONCAT(DAY(DateImport),'/',MONTH(DateImport),'/',YEAR(DateImport)),
ROUND(((SUM(CAST (LEFT(TotalTime ,2) AS MONEY)) *60 + SUM(CAST (Right(TotalTime ,2) AS MONEY))) /60),1) AS TotalTime,
ROUND(((SUM(CAST (LEFT(OverTime ,2) AS MONEY)) *60 + SUM(CAST (Right(OverTime ,2) AS MONEY))) /60),1) AS OverTime,
ROUND(((SUM(CAST (LEFT(DifferentTime ,2) AS MONEY)) *60 + SUM(CAST (Right(DifferentTime ,1) AS MONEY))) /60),0) AS DifferentTime
FROM [dbo].[Temp_Import_Detail] WHERE RecordStatus <> 99 
AND DateImport BETWEEN @FromDate AND @ToDate 
GROUP BY DateImport,
 MONTH(DateImport),YEAR(DateImport),DAY(DateImport),[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
TeamKey,TeamName,TeamID,EmployeeKey,EmployeeID,EmployeeName

--SELECT * FROM Temp

SELECT 
P.TeamName + '|' + P.EmployeeName + '|' + P.EmployeeID+'|'+CAST(DepartmentRank AS NVARCHAR(10))+'|'+CAST(TeamRank AS NVARCHAR(10)) AS LeftColumn,
P.DateWrite AS HeaderColumn,
P.[TotalTime], P.[OverTime], P.[DifferentTime]
FROM
(
	SELECT A.TeamKey,A.TeamID,A.TeamName,
	A.EmployeeKey,A.EmployeeID,A.EmployeeName, DateWrite ,
	A.[TotalTime], A.[OverTime], A.[DifferentTime],
	C.RANK AS DepartmentRank,B.RANK as TeamRank FROM Temp A 
	LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
	LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
    WHERE 1=1 @Parameter
) P
    ORDER BY LEN(DateWrite), DateWrite,DepartmentRank,TeamRank,LEN(EmployeeID), EmployeeID

DROP TABLE Temp";
            zSQL = zSQL.Replace("@Parameter", Fillter);


            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search + "%";
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
    }
}