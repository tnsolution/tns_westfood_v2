﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Connect;

namespace TNS.CRM
{
    public class Company_Info
    {
        #region [ Field Name ]
        private string _CompanyKey = "";
        private string _ParentKey = "";
        private string _ParentTable = "";
        private string _CompanyName = "";
        private string _CompanyPhone = "";
        private string _Fax = "";
        private string _TaxCode = "";
        private string _Email = "";
        private string _BankCode = "";
        private string _SwiftCode = "";
        private string _AddressBank = "";
        private string _Decription = "";
        private int _RecordStatus = 0;
        private DateTime _CreatedOn;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _RoleID = "";
        private string _Message = "";
        #endregion

        #region [ Properties ]
        public bool IsCommandOK
        {
            get
            {
                int zNumber = 0;
                int.TryParse(_Message.Substring(0, 1), out zNumber);
                if (zNumber <= 3) return true; else return false;
            }
        }
        public bool HasInfo
        {
            get
            {
                if (_CompanyKey.Trim().Length > 0) return true; else return false;
            }
        }
        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        public string Key
        {
            get { return _CompanyKey; }
            set { _CompanyKey = value; }
        }
        public string ParentKey
        {
            get { return _ParentKey; }
            set { _ParentKey = value; }
        }
        public string ParentTable
        {
            get { return _ParentTable; }
            set { _ParentTable = value; }
        }
        public string CompanyName
        {
            get { return _CompanyName; }
            set { _CompanyName = value; }
        }
        public string CompanyPhone
        {
            get { return _CompanyPhone; }
            set { _CompanyPhone = value; }
        }
        public string Fax
        {
            get { return _Fax; }
            set { _Fax = value; }
        }
        public string TaxCode
        {
            get { return _TaxCode; }
            set { _TaxCode = value; }
        }
        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }
        public string BankCode
        {
            get { return _BankCode; }
            set { _BankCode = value; }
        }
        public string SwiftCode
        {
            get { return _SwiftCode; }
            set { _SwiftCode = value; }
        }
        public string AddressBank
        {
            get { return _AddressBank; }
            set { _AddressBank = value; }
        }
        public string Decription
        {
            get { return _Decription; }
            set { _Decription = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion

        #region [ Constructor Get Information ]
        public Company_Info()
        {
        }

        public Company_Info(string ParentKey)
        {
            string zSQL = "SELECT * FROM CRM_Company WHERE  ParentKey = @ParentKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ParentKey", SqlDbType.NVarChar).Value = ParentKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _CompanyKey = zReader["CompanyKey"].ToString();
                    _ParentKey = zReader["ParentKey"].ToString().Trim();
                    _ParentTable = zReader["ParentTable"].ToString().Trim();
                    _CompanyName = zReader["CompanyName"].ToString().Trim();
                    _CompanyPhone = zReader["CompanyPhone"].ToString().Trim();
                    _Fax = zReader["CompanyFax"].ToString().Trim();
                    _TaxCode = zReader["TaxCode"].ToString().Trim();
                    _Email = zReader["Email"].ToString().Trim();
                    _BankCode = zReader["BankCode"].ToString().Trim();
                    _SwiftCode = zReader["SwiftCode"].ToString().Trim();
                    _AddressBank = zReader["AddressBank"].ToString().Trim();
                    _Decription = zReader["Decription"].ToString().Trim();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion

        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "CRM_Company_INSERT";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@ParentKey", SqlDbType.NVarChar).Value = _ParentKey.Trim();
                zCommand.Parameters.Add("@ParentTable", SqlDbType.NVarChar).Value = _ParentTable.Trim();
                zCommand.Parameters.Add("@CompanyName", SqlDbType.NVarChar).Value = _CompanyName.Trim();
                zCommand.Parameters.Add("@CompanyPhone", SqlDbType.NVarChar).Value = _CompanyPhone.Trim();
                zCommand.Parameters.Add("@CompanyFax", SqlDbType.NVarChar).Value = _Fax.Trim();
                zCommand.Parameters.Add("@TaxCode", SqlDbType.NVarChar).Value = _TaxCode.Trim();
                zCommand.Parameters.Add("@Email", SqlDbType.NVarChar).Value = _Email.Trim();
                zCommand.Parameters.Add("@BankCode", SqlDbType.NVarChar).Value = _BankCode.Trim();
                zCommand.Parameters.Add("@SwiftCode", SqlDbType.NVarChar).Value = _SwiftCode.Trim();
                zCommand.Parameters.Add("@AddressBank", SqlDbType.NVarChar).Value = _AddressBank.Trim();
                zCommand.Parameters.Add("@Decription", SqlDbType.NVarChar).Value = _Decription.Trim();
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                if (_Message.Substring(0, 2) == "11")
                    _CompanyKey = _Message.Substring(2, 36);
                else
                    _CompanyKey = "";
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "CRM_Company_UPDATE";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                if (_CompanyKey.Length == 36)
                    zCommand.Parameters.Add("@CompanyKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_CompanyKey);
                else
                    zCommand.Parameters.Add("@CompanyKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@CompanyName", SqlDbType.NVarChar).Value = _CompanyName.Trim();
                zCommand.Parameters.Add("@CompanyPhone", SqlDbType.NVarChar).Value = _CompanyPhone.Trim();
                zCommand.Parameters.Add("@CompanyFax", SqlDbType.NVarChar).Value = _Fax.Trim();
                zCommand.Parameters.Add("@TaxCode", SqlDbType.NVarChar).Value = _TaxCode.Trim();
                zCommand.Parameters.Add("@Email", SqlDbType.NVarChar).Value = _Email.Trim();
                zCommand.Parameters.Add("@BankCode", SqlDbType.NVarChar).Value = _BankCode.Trim();
                zCommand.Parameters.Add("@SwiftCode", SqlDbType.NVarChar).Value = _SwiftCode.Trim();
                zCommand.Parameters.Add("@AddressBank", SqlDbType.NVarChar).Value = _AddressBank.Trim();
                zCommand.Parameters.Add("@Decription", SqlDbType.NVarChar).Value = _Decription.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "CRM_Company_DELETE";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@ParentKey", SqlDbType.NVarChar).Value = _ParentKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_CompanyKey.Length == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        #endregion
    }
}
