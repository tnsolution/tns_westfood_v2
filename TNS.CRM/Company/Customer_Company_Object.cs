﻿namespace TNS.CRM
{
    public class Customer_Company_Object : Customer_Info
    {
        private Company_Object _CompanyObj;
        public Company_Object CompanyObj
        {
            get
            {
                return _CompanyObj;
            }

            set
            {
                _CompanyObj = value;
            }
        }
        public Customer_Company_Object() : base()
        {

        }

        public Customer_Company_Object(string CustomerKey) : base(CustomerKey)
        {
            _CompanyObj = new Company_Object(CustomerKey);
        }

        public void Save_Object()
        {
            base.Save();
            _CompanyObj.ParentKey = base.Key;
            _CompanyObj.ParentTable = "CRM_Customer";
            _CompanyObj.CreatedBy = base.CreatedBy;
            _CompanyObj.ModifiedBy = base.ModifiedBy;
            _CompanyObj.CreatedName = base.CreatedName;
            _CompanyObj.ModifiedName = base.ModifiedName;
            _CompanyObj.Save_Object();
        }
        public void Delete_Object()
        {
            base.Delete();
            _CompanyObj = new Company_Object();
            _CompanyObj.Key = base.Key;
            _CompanyObj.Delete_Object();
        }
    }
}
