﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Connect;

namespace TNS.CRM
{
    public class Customer_Data
    {
        public static DataTable List()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM CRM_Customer WHERE RecordStatus != 99 ORDER BY [RANK]";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable Search(string Name, int Slug)
        {
            string zSQL = "";
            DataTable zTable = new DataTable();
            zSQL += "";
            if (Slug == 1) //Khách hàng cá nhân
            {
                zSQL += @"  SELECT TOP 20 A.CustomerKey, A.CustomerID, B.FullName AS CustomerName, B.Phone, B.Email, B.TaxCode, dbo.fn_getaddress(B.PersonalKey) AS [Address]
                            FROM dbo.CRM_Customer A
                            LEFT JOIN dbo.SYS_Personal B ON B.ParentKey = A.CustomerKey
                            WHERE A.Slug = 1 AND( B.FullName LIKE @Name OR A.CustomerID LIKE @Name)";
            }
            if (Slug == 2)
            {
                zSQL += @"   SELECT A.CustomerKey, A.CustomerID, B.CompanyName AS CustomerName,B.CompanyKey, B.CompanyPhone AS Phone, B.Email, B.TaxCode, dbo.fn_getaddress(B.CompanyKey) AS [Address]
                        FROM dbo.CRM_Customer A
                        LEFT JOIN dbo.CRM_Company B ON B.ParentKey = A.CustomerKey
                        WHERE A.Slug = 2 AND (B.CompanyName LIKE @Name OR A.CustomerID LIKE @Name)";
            }
            zSQL += " AND A.RecordStatus <> 99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Slug;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable Search(int Slug)
        {
            string zSQL = "";
            DataTable zTable = new DataTable();
            zSQL += "";
            if (Slug == 1) //Khách hàng cá nhân
            {
                zSQL += @" SELECT A.CustomerKey,A.CustomerID,B.FullName  AS CustomerName,A.Slug,B.PersonalKey AS PersonalKey  FROM dbo.CRM_Customer A
                        LEFT JOIN dbo.SYS_Personal B ON B.ParentKey =A.CustomerKey
                        WHERE A.Slug = 1";
            }
            if (Slug == 2)
            {
                zSQL += @" SELECT A.CustomerKey,A.CustomerID,B.CompanyName AS CustomerName,A.Slug,CompanyKey FROM dbo.CRM_Customer A
                        LEFT JOIN dbo.CRM_Company B ON B.ParentKey = A.CustomerKey
                        WHERE A.Slug =2";
            }
            zSQL += " AND A.RecordStatus <> 99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Slug;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
    }
}
