﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
namespace TNS.HRM
{
    public class Score_Stock_Data
    {
        public static DataTable List()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM HRM_Score_Stock WHERE RecordStatus != 99 ORDER BY [RANK]";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable Search_List(DateTime FromDate, DateTime ToDate)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            DataTable zTable = new DataTable();
            string zSQL = @" SELECT A.*,B.DepartmentID FROM HRM_Score_Stock A 
 LEFT JOIN [dbo].SYS_Department B ON B.DepartmentKey =A.DepartmentKey 
WHERE DateWrite between @FromDate and @ToDate 
AND A.RecordStatus <> 99 ";

            zSQL += " Order by LEN(A.EmployeeID) ,A.EmployeeID";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                }

                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static int Count(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            int zResult = 0;
            string zSQL = @"SELECT COUNT(*) FROM HRM_Score_Stock WHERE DateWrite between @FromDate and @ToDate 
AND RecordStatus <> 99 AND EmployeeKey =@EmployeeKey ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zResult = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }

        public static DataTable Search_List(int TeamKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);

            DataTable zTable = new DataTable();
            string zSQL = @"
DECLARE @FromDateOld DATETIME = DATEADD(month, -1, @FromDate);
Declare @ToDateOld DATETIME = DATEADD (dd, -1, DATEADD(mm, DATEDIFF(mm, 0, @FromDateOld) + 1, 0))

SELECT  EmployeeKey, EmployeeName,
EmployeeID,TeamID,
[dbo].[HRM_ScoreStockBegin](EmployeeKey,@FromDateOld,@ToDateOld) AS StockBegin,
[dbo].[HRM_TIME_NCQD_LaySoNgayCongQuyDinh](EmPloyeeKey,@FromDate,@ToDate) AS NCQD,
[dbo].[HRM_TIME_SCTT_LaySoNgayCongThucTe](EmPloyeeKey,@FromDate,@ToDate) AS NCTT

--([dbo].[HRM_TIME_SCTT_LaySoNgayCongThucTe](EmPloyeeKey,@FromDate,@ToDate)-
--[dbo].[HRM_TIME_NCQD_LaySoNgayCongQuyDinh](EmPloyeeKey,@FromDate,@ToDate)) AS OverTime,

--[dbo].[HRM_ScoreStockEnd](EmPloyeeKey,@FromDate,@ToDate)  AS StockEnd

FROM [dbo].[HRM_Team_Employee_Month] 
WHERE RecordStatus <> 99  AND OverTime  <> 1 
AND ( MONTH(DateWrite)=MONTH(@FromDate) AND YEAR(DateWrite)=YEAR(@FromDate))";
            if (TeamKey > 0)
            {
                zSQL += " AND TeamKey = @TeamKey ";
            }
            zSQL += " ORDER BY TeamKey, LEN(EmployeeID),EmployeeID";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;

                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable List01(int BranchKey, int DepartmentKey, int TeamKey, string Search, DateTime DateSearch)
        {
            DateTime zFromDate = new DateTime(DateSearch.Year, DateSearch.Month, DateSearch.Day, 0, 0, 0);

            DataTable zTable = new DataTable();
            string zSQL = ";WITH #TEMP AS (";
            zSQL += @"SELECT AutoKey, EmployeeKey, EmployeeID,EmployeeName,TeamKey, ScoreStock, ScoreBorrow,Description,
                    [dbo].[Fn_GetTeamID](TeamKey) AS TeamID,
                    [dbo].[Fn_GetTeamName](TeamKey)AS TeamName,
                    BranchKey ,
                    DepartmentKey
                    FROM HRM_Score_Stock 
                    WHERE RecordStatus <> 99  AND Month(DateWrite)= Month(@Date) AND Year(DateWrite) =Year(@Date)
                    AND (ScoreStock >0 OR ScoreBorrow >0) ";

            if (BranchKey > 0)
            {
                zSQL += " AND BranchKey = @BranchKey";
            }
            if (DepartmentKey > 0)
                zSQL += " AND DepartmentKey =@DepartmentKey ";
            if (TeamKey > 0)
                zSQL += " AND TeamKey =@TeamKey ";
            if (Search.Trim().Length > 0)
            {
                zSQL += " AND ( EmployeeID LIKE @Search OR EmployeeName LIKE @Search )";
            }

            zSQL += ")";
            zSQL += @"SELECT A.AutoKey,A.EmployeeName,A.EmployeeID,A.TeamKey,A.TeamID,A.TeamName,A.ScoreStock,A.ScoreBorrow,A.Description FROM #TEMP A
LEFT JOIN[dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
LEFT JOIN[dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
LEFT JOIN[dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
ORDER BY D.RANK, C.RANK, B.RANK, LEN(EmployeeID), EmployeeID";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BranchKey;
                zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search.Trim() + "%";
                zCommand.Parameters.Add("@Date", SqlDbType.DateTime).Value = zFromDate;
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        #region[close-14/08/2020 -Search_List- vì chuyển nhóm]
        //        public static DataTable Search_List(int TeamKey, DateTime DateWrite)
        //        {
        //            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
        //            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
        //            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);

        //            DataTable zTable = new DataTable();
        //            string zSQL = @"
        //DECLARE @FromDateOld DATETIME = DATEADD(month, -1, @FromDate);
        //Declare @ToDateOld DATETIME = DATEADD (dd, -1, DATEADD(mm, DATEDIFF(mm, 0, @FromDateOld) + 1, 0))

        //SELECT  EmployeeKey,[dbo].[Fn_GetFullName](EmployeeKey) AS EmployeeName,
        //EmployeeID,[dbo].[Fn_GetTeamID](TeamKey) AS TeamID,
        //[dbo].[HRM_ScoreStockBegin](EmployeeKey,@FromDateOld,@ToDateOld) AS StockBegin,
        //[dbo].[HRM_TIME_NCQD_LaySoNgayCongQuyDinh](EmPloyeeKey,@FromDate,@ToDate) AS NCQD,
        //[dbo].[HRM_TIME_SCTT_LaySoNgayCongThucTe](EmPloyeeKey,@FromDate,@ToDate) AS NCTT

        //--([dbo].[HRM_TIME_SCTT_LaySoNgayCongThucTe](EmPloyeeKey,@FromDate,@ToDate)-
        //--[dbo].[HRM_TIME_NCQD_LaySoNgayCongQuyDinh](EmPloyeeKey,@FromDate,@ToDate)) AS OverTime,

        //--[dbo].[HRM_ScoreStockEnd](EmPloyeeKey,@FromDate,@ToDate)  AS StockEnd

        //FROM [dbo].[HRM_Employee]  
        //WHERE RecordStatus <> 99  AND OverTime  <> 1 ";
        //            if (TeamKey > 0)
        //            {
        //                zSQL += " AND TeamKey = @TeamKey ";
        //            }
        //            zSQL += " ORDER BY TeamKey, LEN(EmployeeID),EmployeeID";

        //            string zConnectionString = ConnectDataBase.ConnectionString;
        //            try
        //            {
        //                SqlConnection zConnect = new SqlConnection(zConnectionString);
        //                zConnect.Open();
        //                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
        //                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
        //                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;

        //                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
        //                zAdapter.Fill(zTable);
        //                zCommand.Dispose();
        //                zConnect.Close();
        //            }
        //            catch (Exception ex)
        //            {
        //                string zstrMessage = "08" + ex.ToString();
        //            }
        //            return zTable;
        //        }
        #endregion


    }
}
