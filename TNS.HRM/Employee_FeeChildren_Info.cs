﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
namespace TNS.HRM
{
    public class Employee_FeeChildren_Info
    {

        #region [ Field Name ]
        private int _AutoKey = 0;
        private string _EmployeeKey = "";
        private string _EmployeeID = "";
        private string _EmployeeName = "";
        private int _DepartmentKey = 0;
        private int _TeamKey = 0;
        private int _CategoryKey = 0;
        private string _CategoryID = "";
        private string _CategoryName = "";
        private float _Value;
        private string _Ext = "";
        private float _Number;
        private DateTime _FromDate;
        private DateTime _ToDate;
        private DateTime _LeavingDate;
        private DateTime _BirthDay;
        private string _Description = "";
        private int _Rank = 0;
        private int _RecordStatus = 0;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _CreatedOn;
        private DateTime _ModifiedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _RoleID = "";
        private string _Message = "";
        #endregion
        #region [ Properties ]

        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        public int Key
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public string EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public string EmployeeID
        {
            get { return _EmployeeID; }
            set { _EmployeeID = value; }
        }
        public string EmployeeName
        {
            get { return _EmployeeName; }
            set { _EmployeeName = value; }
        }
        public int DepartmentKey
        {
            get { return _DepartmentKey; }
            set { _DepartmentKey = value; }
        }
        public int TeamKey
        {
            get { return _TeamKey; }
            set { _TeamKey = value; }
        }
        public int CategoryKey
        {
            get { return _CategoryKey; }
            set { _CategoryKey = value; }
        }
        public string CategoryID
        {
            get { return _CategoryID; }
            set { _CategoryID = value; }
        }
        public string CategoryName
        {
            get { return _CategoryName; }
            set { _CategoryName = value; }
        }
        public float Value
        {
            get { return _Value; }
            set { _Value = value; }
        }
        public string Ext
        {
            get { return _Ext; }
            set { _Ext = value; }
        }
        public float Number
        {
            get { return _Number; }
            set { _Number = value; }
        }
        public DateTime FromDate
        {
            get { return _FromDate; }
            set { _FromDate = value; }
        }
        public DateTime ToDate
        {
            get { return _ToDate; }
            set { _ToDate = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public int Rank
        {
            get { return _Rank; }
            set { _Rank = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public DateTime LeavingDate
        {
            get
            {
                return _LeavingDate;
            }

            set
            {
                _LeavingDate = value;
            }
        }

        public DateTime BirthDay
        {
            get
            {
                return _BirthDay;
            }

            set
            {
                _BirthDay = value;
            }
        }
        #endregion
        #region [ Constructor Get Information ]
        public Employee_FeeChildren_Info()
        {
        }
        public Employee_FeeChildren_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM HRM_FeeChildren WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    _EmployeeKey = zReader["EmployeeKey"].ToString();
                    _EmployeeID = zReader["EmployeeID"].ToString().Trim();
                    _EmployeeName = zReader["EmployeeName"].ToString().Trim();
                    if (zReader["DepartmentKey"] != DBNull.Value)
                        _DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    if (zReader["TeamKey"] != DBNull.Value)
                        _TeamKey = int.Parse(zReader["TeamKey"].ToString());
                    if (zReader["CategoryKey"] != DBNull.Value)
                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    _CategoryID = zReader["CategoryID"].ToString().Trim();
                    _CategoryName = zReader["CategoryName"].ToString().Trim();
                    if (zReader["Value"] != DBNull.Value)
                        _Value = float.Parse(zReader["Value"].ToString());
                    _Ext = zReader["Ext"].ToString().Trim();
                    if (zReader["Number"] != DBNull.Value)
                        _Number = float.Parse(zReader["Number"].ToString());
                    if (zReader["BirthDay"] != DBNull.Value)
                        _BirthDay = (DateTime)zReader["BirthDay"];
                    if (zReader["FromDate"] != DBNull.Value)
                        _FromDate = (DateTime)zReader["FromDate"];
                    if (zReader["ToDate"] != DBNull.Value)
                        _ToDate = (DateTime)zReader["ToDate"];
                    if (zReader["LeavingDate"] != DBNull.Value)
                        _LeavingDate = (DateTime)zReader["LeavingDate"];
                    _Description = zReader["Description"].ToString().Trim();
                    if (zReader["Rank"] != DBNull.Value)
                        _Rank = int.Parse(zReader["Rank"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        public Employee_FeeChildren_Info(string EmployeeID, string CategoryID, DateTime FromDate, DateTime ToDate)
        {
            string zSQL = "SELECT * FROM HRM_FeeChildren WHERE EmployeeID = @EmployeeID AND CategoryID =@CategoryID AND RecordStatus <> 99 AND FromDate =@FromDate AND ToDate = @ToDate";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = EmployeeID.Trim();
                zCommand.Parameters.Add("@CategoryID", SqlDbType.NVarChar).Value = CategoryID.Trim();
                if (FromDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                if (ToDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    _EmployeeKey = zReader["EmployeeKey"].ToString();
                    _EmployeeID = zReader["EmployeeID"].ToString().Trim();
                    _EmployeeName = zReader["EmployeeName"].ToString().Trim();
                    if (zReader["DepartmentKey"] != DBNull.Value)
                        _DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    if (zReader["TeamKey"] != DBNull.Value)
                        _TeamKey = int.Parse(zReader["TeamKey"].ToString());
                    if (zReader["CategoryKey"] != DBNull.Value)
                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    _CategoryID = zReader["CategoryID"].ToString().Trim();
                    _CategoryName = zReader["CategoryName"].ToString().Trim();
                    if (zReader["Value"] != DBNull.Value)
                        _Value = float.Parse(zReader["Value"].ToString());
                    _Ext = zReader["Ext"].ToString().Trim();
                    if (zReader["Number"] != DBNull.Value)
                        _Number = float.Parse(zReader["Number"].ToString());
                    if (zReader["BirthDay"] != DBNull.Value)
                        _BirthDay = (DateTime)zReader["BirthDay"];
                    if (zReader["FromDate"] != DBNull.Value)
                        _FromDate = (DateTime)zReader["FromDate"];
                    if (zReader["ToDate"] != DBNull.Value)
                        _ToDate = (DateTime)zReader["ToDate"];
                    _Description = zReader["Description"].ToString().Trim();
                    if (zReader["LeavingDate"] != DBNull.Value)
                        _LeavingDate = (DateTime)zReader["LeavingDate"];
                    if (zReader["Rank"] != DBNull.Value)
                        _Rank = int.Parse(zReader["Rank"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_FeeChildren ("
        + "  EmployeeKey ,EmployeeID ,EmployeeName ,DepartmentKey ,TeamKey ,CategoryKey ,CategoryID ,CategoryName ,Value ,Ext ,Number ,BirthDay,FromDate ,ToDate ,LeavingDate ,Description ,Rank ,RecordStatus ,CreatedBy ,CreatedName ,CreatedOn ,ModifiedOn ,ModifiedBy ,ModifiedName ) "
         + " VALUES ( "
         + "@EmployeeKey ,@EmployeeID ,@EmployeeName ,@DepartmentKey ,@TeamKey ,@CategoryKey ,@CategoryID ,@CategoryName ,@Value ,@Ext ,@Number ,@BirthDay ,@FromDate ,@ToDate ,@LeavingDate ,@Description ,@Rank ,0 ,@CreatedBy ,@CreatedName ,GETDATE(),GETDATE(),@ModifiedBy ,@ModifiedName ) ";
            zSQL += " SELECT '11' + CAST(AutoKey AS NVARCHAR(50)) FROM HRM_FeeChildren WHERE AutoKey = SCOPE_IDENTITY() ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;

                if (_EmployeeKey.Length == 36)
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_EmployeeKey);
                else
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID.Trim();
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName.Trim();
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = _TeamKey;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@CategoryID", SqlDbType.NVarChar).Value = _CategoryID.Trim();
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName.Trim();
                zCommand.Parameters.Add("@Value", SqlDbType.Float).Value = _Value;
                zCommand.Parameters.Add("@Ext", SqlDbType.NVarChar).Value = _Ext.Trim();
                zCommand.Parameters.Add("@Number", SqlDbType.Float).Value = _Number;
                if (_BirthDay == DateTime.MinValue)
                    zCommand.Parameters.Add("@BirthDay", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@BirthDay", SqlDbType.DateTime).Value = _BirthDay;

                if (_FromDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = _FromDate;
                if (_ToDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = _ToDate;
                if (_LeavingDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@LeavingDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@LeavingDate", SqlDbType.DateTime).Value = _LeavingDate;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteScalar().ToString();
                if (zResult.Substring(0, 2) == "11")
                {
                    _Message = zResult.Substring(0, 2);
                    _AutoKey = int.Parse(zResult.Substring(2));
                }
                else
                    _AutoKey = 0;
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE HRM_FeeChildren SET "
                        + " EmployeeKey = @EmployeeKey,"
                        + " EmployeeID = @EmployeeID,"
                        + " EmployeeName = @EmployeeName,"
                        + " DepartmentKey = @DepartmentKey,"
                        + " TeamKey = @TeamKey,"
                        + " CategoryKey = @CategoryKey,"
                        + " CategoryID = @CategoryID,"
                        + " CategoryName = @CategoryName,"
                        + " Value = @Value,"
                        + " Ext = @Ext,"
                        + " Number = @Number,"
                        + " BirthDay =@BirthDay,"
                        + " FromDate = @FromDate,"
                        + " ToDate = @ToDate,"
                        + " LeavingDate = @LeavingDate,"
                        + " Description = @Description,"
                        + " Rank = @Rank,"
                        + " RecordStatus = 1,"
                        + " ModifiedOn = GETDATE(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                       + " WHERE AutoKey = @AutoKey SELECT 20";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;

                if (_EmployeeKey.Length == 36)
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_EmployeeKey);
                else
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID.Trim();
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName.Trim();
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = _TeamKey;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@CategoryID", SqlDbType.NVarChar).Value = _CategoryID.Trim();
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName.Trim();
                zCommand.Parameters.Add("@Value", SqlDbType.Float).Value = _Value;
                zCommand.Parameters.Add("@Ext", SqlDbType.NVarChar).Value = _Ext.Trim();
                zCommand.Parameters.Add("@Number", SqlDbType.Float).Value = _Number;
                if (_BirthDay == DateTime.MinValue)
                    zCommand.Parameters.Add("@BirthDay", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@BirthDay", SqlDbType.DateTime).Value = _BirthDay;

                if (_FromDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = _FromDate;
                if (_ToDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = _ToDate;
                if (_LeavingDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@LeavingDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@LeavingDate", SqlDbType.DateTime).Value = _LeavingDate;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE HRM_FeeChildren SET [RecordStatus] = 99, [ModifiedOn] = GETDATE(), ModifiedBy = @ModifiedBy, ModifiedName = @ModifiedName WHERE AutoKey = @AutoKey SELECT 30";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string DeleteAll()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE HRM_FeeChildren SET [RecordStatus] = 99, [ModifiedOn] = GETDATE(), ModifiedBy = @ModifiedBy, ModifiedName = @ModifiedName WHERE AutoKey = @AutoKey SELECT 30";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_AutoKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        #endregion
    }
}
