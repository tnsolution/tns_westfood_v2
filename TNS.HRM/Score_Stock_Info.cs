﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
namespace TNS.HRM
{
    public class Score_Stock_Info
    {

        #region [ Field Name ]
        private int _AutoKey = 0;
        private DateTime _DateWrite;
        private string _EmployeeKey = "";
        private string _EmployeeID = "";
        private string _EmployeeName = "";
        private int _TeamKey = 0;
        private int _DepartmentKey = 0;
        private int _BranchKey = 0;
        private float _ScoreStock =0;
        private float _ScoreBorrow = 0;
        private string _Description = "";
        private int _RecordStatus = 0;
        private DateTime _CreatedOn;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _RoleID = "";
        private string _Message = "";
        
        #endregion
        #region [ Properties ]
        public bool IsCommandOK
        {
            get
            {
                int zNumber = 0;
                int.TryParse(_Message.Substring(0, 1), out zNumber);
                if (zNumber <= 3) return true; else return false;
            }
        }

        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        public int Key
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public DateTime DateWrite
        {
            get { return _DateWrite; }
            set { _DateWrite = value; }
        }
        public string EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public string EmployeeID
        {
            get { return _EmployeeID; }
            set { _EmployeeID = value; }
        }
        public string EmployeeName
        {
            get { return _EmployeeName; }
            set { _EmployeeName = value; }
        }
        public int TeamKey
        {
            get { return _TeamKey; }
            set { _TeamKey = value; }
        }
        public int DepartmentKey
        {
            get { return _DepartmentKey; }
            set { _DepartmentKey = value; }
        }
       
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public float ScoreStock
        {
            get
            {
                return _ScoreStock;
            }

            set
            {
                _ScoreStock = value;
            }
        }

        public float ScoreBorrow
        {
            get
            {
                return _ScoreBorrow;
            }

            set
            {
                _ScoreBorrow = value;
            }
        }

        public int BranchKey
        {
            get
            {
                return _BranchKey;
            }

            set
            {
                _BranchKey = value;
            }
        }
        #endregion
        #region [ Constructor Get Information ]
        public Score_Stock_Info()
        {
        }
        public Score_Stock_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM HRM_Score_Stock WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    if (zReader["DateWrite"] != DBNull.Value)
                        _DateWrite = (DateTime)zReader["DateWrite"];
                    _EmployeeKey = zReader["EmployeeKey"].ToString().Trim();
                    _EmployeeID = zReader["EmployeeID"].ToString().Trim();
                    _EmployeeName = zReader["EmployeeName"].ToString().Trim();
                    if (zReader["TeamKey"] != DBNull.Value)
                        _TeamKey = int.Parse(zReader["TeamKey"].ToString());
                    if (zReader["DepartmentKey"] != DBNull.Value)
                        _DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    if (zReader["BranchKey"] != DBNull.Value)
                        _BranchKey = int.Parse(zReader["BranchKey"].ToString());
                    if (zReader["ScoreStock"] != DBNull.Value)
                        _ScoreStock = float.Parse(zReader["ScoreStock"].ToString());
                    if (zReader["ScoreBorrow"] != DBNull.Value)
                        _ScoreBorrow = float.Parse(zReader["ScoreBorrow"].ToString());
                    _Description = zReader["Description"].ToString().Trim();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        public Score_Stock_Info(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            string zSQL = "SELECT * FROM HRM_Score_Stock WHERE EmployeeKey = @EmployeeKey AND DateWrite BETWEEN @FromDate AND @ToDate AND RecordStatus <>99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey.Trim();
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    if (zReader["DateWrite"] != DBNull.Value)
                        _DateWrite = (DateTime)zReader["DateWrite"];
                    _EmployeeKey = zReader["EmployeeKey"].ToString().Trim();
                    _EmployeeID = zReader["EmployeeID"].ToString().Trim();
                    _EmployeeName = zReader["EmployeeName"].ToString().Trim();
                    if (zReader["TeamKey"] != DBNull.Value)
                        _TeamKey = int.Parse(zReader["TeamKey"].ToString());
                    if (zReader["BranchKey"] != DBNull.Value)
                        _BranchKey = int.Parse(zReader["BranchKey"].ToString());
                    if (zReader["DepartmentKey"] != DBNull.Value)
                        _DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    if (zReader["ScoreStock"] != DBNull.Value)
                        _ScoreStock = float.Parse(zReader["ScoreStock"].ToString());
                    if (zReader["ScoreBorrow"] != DBNull.Value)
                        _ScoreBorrow = float.Parse(zReader["ScoreBorrow"].ToString());
                    _Description = zReader["Description"].ToString().Trim();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Score_Stock ("
        + " DateWrite ,EmployeeKey ,EmployeeID ,EmployeeName ,TeamKey ,DepartmentKey ,BranchKey ,ScoreStock,ScoreBorrow ,Description ,RecordStatus ,CreatedOn ,CreatedBy ,CreatedName ,ModifiedOn ,ModifiedBy ,ModifiedName ) "
         + " VALUES ( "
         + "@DateWrite ,@EmployeeKey ,@EmployeeID ,@EmployeeName ,@TeamKey ,@DepartmentKey ,@BranchKey ,@ScoreStock , @ScoreBorrow ,@Description ,@RecordStatus ,GETDATE(),@CreatedBy ,@CreatedName ,GETDATE(),@ModifiedBy ,@ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (_DateWrite == DateTime.MinValue)
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = _DateWrite;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = _EmployeeKey.Trim();
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID.Trim();
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName.Trim();
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = _TeamKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = _BranchKey;
                zCommand.Parameters.Add("@ScoreStock", SqlDbType.Float).Value = _ScoreStock;
                zCommand.Parameters.Add("@ScoreBorrow", SqlDbType.Float).Value = _ScoreBorrow;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();

                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE HRM_Score_Stock SET "
                        + " DateWrite = @DateWrite,"
                        + " EmployeeKey = @EmployeeKey,"
                        + " EmployeeID = @EmployeeID,"
                        + " EmployeeName = @EmployeeName,"
                        + " TeamKey = @TeamKey,"
                        + " DepartmentKey = @DepartmentKey,"
                        + " BranchKey = @BranchKey,"
                        + " ScoreStock = @ScoreStock,"
                        + " ScoreBorrow = @ScoreBorrow,"
                        + " Description = @Description,"
                        + " RecordStatus = 1,"
                        + " ModifiedOn = GETDATE(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                       + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                if (_DateWrite == DateTime.MinValue)
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = _DateWrite;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = _EmployeeKey.Trim();
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID.Trim();
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName.Trim();
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = _TeamKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = _BranchKey;
                zCommand.Parameters.Add("@ScoreStock", SqlDbType.Float).Value = _ScoreStock;
                zCommand.Parameters.Add("@ScoreBorrow", SqlDbType.Float).Value = _ScoreBorrow;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE HRM_Score_Stock SET [RecordStatus] = 99, [ModifiedOn] = GETDATE(), ModifiedBy = @ModifiedBy, ModifiedName = @ModifiedName WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string DeleteEmpty(int BranchKey,int DepartmentKey, int TeamKey,DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);

            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"DELETE FROM HRM_Score_Stock  
WHERE DateWrite BETWEEN @FromDate AND @ToDate ";

            if (BranchKey != 0)
            {
                zSQL += " AND BranchKey = @BranchKey";
            }
            if (DepartmentKey != 0)
            {
                zSQL += " AND DepartmentKey =@DepartmentKey";
            }
            if (TeamKey != 0)
            {
                zSQL += "  AND TeamKey = @TeamKey";
            }
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BranchKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_AutoKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }

        public string LuuCongTichLuy(int BranchKey, int DepartmentKey, int TeamKey, DateTime DateWrite,string CreatedBy,string CreatedName)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);

            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"
--declare @FromDate datetime='2020-04-01 00:00:00'
--declare @ToDate datetime='2020-04-30 23:59:59'

--Nhân sự làm việc trong tháng
CREATE TABLE #EmployeeTemp
(
EmployeeKey NVARCHAR(50),
EmployeeID NVARCHAR(50),
EmployeeName NVARCHAR(500),
BranchKey INT,
DepartmentKey INT ,
TeamKey INT ,
PositionKey INT 
)
INSERT INTO #EmployeeTemp
SELECT EmployeeKey,EmployeeID,EmployeeName,
BranchKey,DepartmentKey,TeamKey, PositionKey
 FROM (
		SELECT EmployeeKey,EmployeeID,[dbo].[Fn_GetFullName](EmployeeKey) AS EmployeeName,StartingDate,
			CASE
				WHEN LeavingDate IS NULL THEN GETDATE()
				ELSE LeavingDate
			END LeavingDate,
            [dbo].[Fn_BranchKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate) AS BranchKey,
            [dbo].[Fn_DepartmentKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate) AS DepartmentKey,
            [dbo].[Fn_TeamKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate) AS TeamKey ,
            [dbo].[Fn_PositionKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate) AS PositionKey
			FROM [dbo].[HRM_Employee] 
			WHERE RecordStatus <>99 
	) X
	WHERE (
            ( StartingDate <=@FromDate AND @FromDate <=  LeavingDate  )
	     OR ( StartingDate <=@ToDate   AND @ToDate   <=  LeavingDate )
        )
    @Customer
----------Kết thúc tạo danh sách nhân sự

INSERT INTO[dbo].[HRM_Score_Stock] 
(DateWrite,EmployeeKey,EmployeeID,EmployeeName,TeamKey,DepartmentKey,BranchKey,ScoreStock,ScoreBorrow,
RecordStatus,CreatedOn,CreatedBy,CreatedName)

SELECT @ToDate,EmployeeKey, EmployeeID,EmployeeName,TeamKey,DepartmentKey,BranchKey,
[dbo].[HRM_LayTonCongTichLuyTuChamCong](EmployeeKey,@FromDate,@ToDate),--Tồn công tích lũy
[dbo].[HRM_LayTonMuonCongTuChamCong](EmployeeKey,@FromDate,@ToDate),--tồn công mượn
0,GETDATE(),@CreatedBy,@CreatedName 
FROM #EmployeeTemp 
WHERE ( [dbo].[HRM_LayTonCongTichLuyTuChamCong](EmployeeKey,@FromDate,@ToDate) >0
OR  [dbo].[HRM_LayTonMuonCongTuChamCong](EmployeeKey,@FromDate,@ToDate) >0 )
AND [dbo].HRM_KiemTraTinhLuongTheoCong_ThoiGian_HTSX_VP(EmployeeKey) =1 --thời gian-->không tích lũy công 

 DROP TABLE #EmployeeTemp
";

            string zFilter = "";
            if (BranchKey != 0)
            {
                zFilter += " AND BranchKey = @BranchKey ";
            }
            if (DepartmentKey != 0)
            {
                zFilter += " AND DepartmentKey =@DepartmentKey ";
            }
            if (TeamKey != 0)
            {
                zFilter += " AND TeamKey= @TeamKey";
            }
            if (zFilter.Length != 0)
            {
                zSQL = zSQL.Replace("@Customer", zFilter);
            }
            else
            {
                zSQL = zSQL.Replace("@Customer", "");
            }
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BranchKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = CreatedName.Trim();
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string HuyCongTichLuy(int BranchKey, int Department, int TeamKey, DateTime DateWrite, string CreatedBy, string CreatedName)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);

            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"
INSERT INTO[dbo].[HRM_Score_Stock] 
(DateWrite,EmployeeKey,EmployeeID,EmployeeName,TeamKey,DepartmentKey,ScoreStock,ScoreBorrow,
RecordStatus,CreatedOn,CreatedBy,CreatedName)
SELECT @ToDate,EmployeeKey, EmployeeID,EmployeeName,TeamKey,
[dbo].[Fn_GetDepartmentKey_FromEmployeeKey](EmployeeKey) AS DepartmentKey,
[dbo].[HRM_LayTonCongTichLuyXetDuyet](EmployeeKey,@FromDate,@ToDate),--Tồn công tích lũy xet duyệt
0,--tồn công mượn
0,GETDATE(),@CreatedBy,@CreatedName
FROM [dbo].[HRM_Team_Employee_Month]
WHERE RecordStatus <> 99  AND Month(DateWrite)= Month(@FromDate) AND Year(DateWrite) =Year(@FromDate)
AND [dbo].[HRM_LayTonCongTichLuyXetDuyet](EmployeeKey,@FromDate,@ToDate) >0 
";
           
            if (BranchKey != 0)
            {
                zSQL += " AND [dbo].[Fn_GetBranchKey_FromEmployeeKey](EmployeeKey) = @BranchKey ";
            }
            if (DepartmentKey != 0)
            {
                zSQL += " AND [dbo].[Fn_GetDepartmentKey_FromEmployeeKey](EmployeeKey) =@DepartmentKey ";
            }
            if (TeamKey != 0)
            {
                zSQL += "  AND TeamKey = @TeamKey ";
            }
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BranchKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = CreatedName.Trim();
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
