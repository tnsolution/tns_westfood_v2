﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
namespace TNS.HRM
{
    public class TimeKeeping_Month_Info
    {

        #region [ Field Name ]
        private int _AutoKey = 0;
        private DateTime _DateWrite;
        private string _EmployeeKey = "";
        private string _EmployeeID = "";
        private string _EmployeeName = "";
        private int _TeamKey = 0;
        private string _TeamID = "";
        private string _TeamName = "";
        private int _CodeKey = 0;
        private string _CodeID = "";
        private string _CodeName = "";
        private float _Number;
        private float _Amount;
        private string _RoleID = "";
        private string _Message = "";
        private int _Type = 0;
        private string _TypeName = "";
        private int _CategoryKey = 0;
        private string _CategoryName = "";
        private int _Rank = 0;
        private int _RecordStatus = 0;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _CreatedOn;
        private DateTime _ModifiedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        #endregion
        #region [ Properties ]

        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        public int Key
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public DateTime DateWrite
        {
            get { return _DateWrite; }
            set { _DateWrite = value; }
        }
        public string EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public string EmployeeID
        {
            get { return _EmployeeID; }
            set { _EmployeeID = value; }
        }
        public string EmployeeName
        {
            get { return _EmployeeName; }
            set { _EmployeeName = value; }
        }
        public int TeamKey
        {
            get { return _TeamKey; }
            set { _TeamKey = value; }
        }
        public string TeamID
        {
            get { return _TeamID; }
            set { _TeamID = value; }
        }
        public string TeamName
        {
            get { return _TeamName; }
            set { _TeamName = value; }
        }
        public int CodeKey
        {
            get { return _CodeKey; }
            set { _CodeKey = value; }
        }
        public string CodeID
        {
            get { return _CodeID; }
            set { _CodeID = value; }
        }
        public string CodeName
        {
            get { return _CodeName; }
            set { _CodeName = value; }
        }
        public float Number
        {
            get { return _Number; }
            set { _Number = value; }
        }
        public float Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public int RecordStatus
        {
            get
            {
                return _RecordStatus;
            }

            set
            {
                _RecordStatus = value;
            }
        }

        public string CreatedBy
        {
            get
            {
                return _CreatedBy;
            }

            set
            {
                _CreatedBy = value;
            }
        }

        public string CreatedName
        {
            get
            {
                return _CreatedName;
            }

            set
            {
                _CreatedName = value;
            }
        }

        public DateTime CreatedOn
        {
            get
            {
                return _CreatedOn;
            }

            set
            {
                _CreatedOn = value;
            }
        }

        public DateTime ModifiedOn
        {
            get
            {
                return _ModifiedOn;
            }

            set
            {
                _ModifiedOn = value;
            }
        }

        public string ModifiedBy
        {
            get
            {
                return _ModifiedBy;
            }

            set
            {
                _ModifiedBy = value;
            }
        }

        public string ModifiedName
        {
            get
            {
                return _ModifiedName;
            }

            set
            {
                _ModifiedName = value;
            }
        }

        public int Type
        {
            get
            {
                return _Type;
            }

            set
            {
                _Type = value;
            }
        }

        public string TypeName
        {
            get
            {
                return _TypeName;
            }

            set
            {
                _TypeName = value;
            }
        }

        public int CategoryKey
        {
            get
            {
                return _CategoryKey;
            }

            set
            {
                _CategoryKey = value;
            }
        }

        public string CategoryName
        {
            get
            {
                return _CategoryName;
            }

            set
            {
                _CategoryName = value;
            }
        }

        public int Rank
        {
            get
            {
                return _Rank;
            }

            set
            {
                _Rank = value;
            }
        }
        #endregion
        #region [ Constructor Get Information ]
        public TimeKeeping_Month_Info()
        {
        }
        public TimeKeeping_Month_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM HRM_TimeKeeping_Month WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    if (zReader["DateWrite"] != DBNull.Value)
                        _DateWrite = (DateTime)zReader["DateWrite"];
                    _EmployeeKey = zReader["EmployeeKey"].ToString();
                    _EmployeeID = zReader["EmployeeID"].ToString().Trim();
                    _EmployeeName = zReader["EmployeeName"].ToString().Trim();
                    if (zReader["TeamKey"] != DBNull.Value)
                        _TeamKey = int.Parse(zReader["TeamKey"].ToString());
                    _TeamID = zReader["TeamID"].ToString().Trim();
                    _TeamName = zReader["TeamName"].ToString().Trim();
                    if (zReader["CodeKey"] != DBNull.Value)
                        _CodeKey = int.Parse(zReader["CodeKey"].ToString());
                    if (zReader["Type"] != DBNull.Value)
                        _Type = int.Parse(zReader["Type"].ToString());
                    _TypeName = zReader["TypeName"].ToString().Trim();
                    if (zReader["CategoryKey"] != DBNull.Value)
                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    _CategoryName = zReader["CategoryName"].ToString().Trim();
                    _CodeID = zReader["CodeID"].ToString().Trim();
                    _CodeName = zReader["CodeName"].ToString().Trim();
                    if (zReader["Number"] != DBNull.Value)
                        _Number = float.Parse(zReader["Number"].ToString());
                    if (zReader["Amount"] != DBNull.Value)
                        _Amount = float.Parse(zReader["Amount"].ToString());
                    if (zReader["Rank"] != DBNull.Value)
                        _Rank = int.Parse(zReader["Rank"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_TimeKeeping_Month ("
        + " DateWrite ,EmployeeKey ,EmployeeID ,EmployeeName ,TeamKey ,TeamID ,TeamName ,CodeKey ,CodeID ,CodeName ,Number ,Amount,RecordStatus,Type,TypeName ,CategoryKey,CategoryName,Rank, CreatedOn,CreatedBy ,CreatedName ,ModifiedOn ,ModifiedBy ,ModifiedName ) "
         + " VALUES ( "
         + "@DateWrite ,@EmployeeKey ,@EmployeeID ,@EmployeeName ,@TeamKey ,@TeamID ,@TeamName ,@CodeKey ,@CodeID ,@CodeName ,@Number ,@Amount,0,@Type,@TypeName ,@CategoryKey,@CategoryName,@Rank,GETDATE(),@CreatedBy ,@CreatedName ,GETDATE(),@ModifiedBy ,@ModifiedName ) ";
            zSQL += " SELECT '11'";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (_DateWrite == DateTime.MinValue)
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = _DateWrite;
                if (_EmployeeKey.Length > 0)
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_EmployeeKey);
                else
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID.Trim();
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName.Trim();
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = _TeamKey;
                zCommand.Parameters.Add("@TeamID", SqlDbType.NVarChar).Value = _TeamID.Trim();
                zCommand.Parameters.Add("@TeamName", SqlDbType.NVarChar).Value = _TeamName.Trim();
                zCommand.Parameters.Add("@CodeKey", SqlDbType.Int).Value = _CodeKey;
                zCommand.Parameters.Add("@CodeID", SqlDbType.NVarChar).Value = _CodeID.Trim();
                zCommand.Parameters.Add("@CodeName", SqlDbType.NVarChar).Value = _CodeName.Trim();
                zCommand.Parameters.Add("@Number", SqlDbType.Float).Value = _Number;
                zCommand.Parameters.Add("@Amount", SqlDbType.Float).Value = _Amount;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zCommand.Parameters.Add("RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("Type", SqlDbType.Int).Value = _Type;
                zCommand.Parameters.Add("@TypeName", SqlDbType.NVarChar).Value = _TypeName.Trim();
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName.Trim();
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                _Message = zCommand.ExecuteScalar().ToString();
                if (_Message.Substring(0, 2) != "11")
                    _AutoKey = 0;
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE HRM_TimeKeeping_Month SET "
                        + " DateWrite = @DateWrite,"
                        + " EmployeeKey = @EmployeeKey,"
                        + " EmployeeID = @EmployeeID,"
                        + " EmployeeName = @EmployeeName,"
                        + " TeamKey = @TeamKey,"
                        + " TeamID = @TeamID,"
                        + " TeamName = @TeamName,"
                        + " CodeKey = @CodeKey,"
                        + " CodeID = @CodeID,"
                        + " CodeName = @CodeName,"
                        + " Number = @Number,"
                        + " Amount = @Amount,"
                        + " Type = @Type,"
                        + " TypeName = @TypeName,"
                        + " CategoryKey = @CategoryKey,"
                        + " CategoryName = @CategoryName,"
                        + " Rank = @Rank,"
                        + " ModifiedOn = GETDATE(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                       + " WHERE AutoKey = @AutoKey SELECT 20";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                if (_DateWrite == DateTime.MinValue)
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = _DateWrite;
                if (_EmployeeKey.Length == 36)
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_EmployeeKey);
                else
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID.Trim();
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName.Trim();
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = _TeamKey;
                zCommand.Parameters.Add("@TeamID", SqlDbType.NVarChar).Value = _TeamID.Trim();
                zCommand.Parameters.Add("@TeamName", SqlDbType.NVarChar).Value = _TeamName.Trim();
                zCommand.Parameters.Add("@CodeKey", SqlDbType.Int).Value = _CodeKey;
                zCommand.Parameters.Add("@CodeID", SqlDbType.NVarChar).Value = _CodeID.Trim();
                zCommand.Parameters.Add("@CodeName", SqlDbType.NVarChar).Value = _CodeName.Trim();
                zCommand.Parameters.Add("@Number", SqlDbType.Float).Value = _Number;
                zCommand.Parameters.Add("@Amount", SqlDbType.Float).Value = _Amount;
                zCommand.Parameters.Add("Type", SqlDbType.Int).Value = _Type;
                zCommand.Parameters.Add("@TypeName", SqlDbType.NVarChar).Value = _TypeName.Trim();
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName.Trim();
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE HRM_TimeKeeping_Month SET [RecordStatus] = 99, [ModifiedOn] = GETDATE(), ModifiedBy = @ModifiedBy, ModifiedName = @ModifiedName WHERE AutoKey = @AutoKey SELECT 30";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string Delete_Team(int TeamKey,DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"DELETE FROM HRM_TimeKeeping_Month 
WHERE TeamKey = @TeamKey AND DateWrite between @FromDate and @ToDate SELECT 30";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_AutoKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }



//        public string InsertRice(string EmployeeKey,DateTime DateWrite, string CreatedBy,string CreatedName)
//        {
//            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, DateWrite.Day, 0, 0, 0);
//            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
//            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);

//            //---------- String SQL Access Database ---------------
//            string zSQL = @"INSERT INTO dbo.HRM_TimeKeeping_Month
//( DateWrite , EmployeeKey ,EmployeeID ,EmployeeName ,TeamKey ,TeamID ,TeamName , 
//CodeKey ,CodeID ,CodeName ,Number ,Amount ,RecordStatus ,
//CreatedOn ,CreatedBy ,CreatedName ,ModifiedOn ,ModifiedBy ,ModifiedName)
//SELECT 
//@ToDate,A.EmployeeKey, A.EmployeeID,A.EmployeeName,A.TeamKey,[dbo].[Fn_GetTeamID](A.TeamKey) AS TeamID,A.TeamName, 
//A.RiceKey,A.RiceName AS RiceID,B.RiceName, COUNT(A.RiceKey)  AS Total, (COUNT(A.RiceKey)*B.Money) AS [money],0,
//GETDATE(),@CreatedBy,@CreatedName,GETDATE(),@ModifiedBy,@ModifiedName 
//FROM  dbo.HRM_Employee_Rice A
//LEFT JOIN  dbo.HRM_Time_Rice B ON B.RiceKey= A.RiceKey
//WHERE A.EmployeeKey = @EmployeeKey AND A.DateWrite BETWEEN @FromDate AND @ToDate 
//GROUP BY A.EmployeeKey,A.EmployeeID, A.EmployeeName ,A.TeamKey,
//[dbo].[Fn_GetTeamID](A.TeamKey),A.TeamName,A.RiceKey,A.RiceName, B.RiceName,B.Money 
//SELECT 11";
//            string zResult = "";
//            string zConnectionString = ConnectDataBase.ConnectionString;
//            SqlConnection zConnect = new SqlConnection(zConnectionString);
//            zConnect.Open();
//            try
//            {
//                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
//                zCommand.CommandType = CommandType.Text;
//                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey; ;
//                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
//                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
//                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = CreatedBy;
//                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = CreatedName;
//                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = CreatedBy;
//                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = CreatedName;
//                _Message = zCommand.ExecuteScalar().ToString();
//                zCommand.Dispose();
//            }
//            catch (Exception Err)
//            {
//                _Message = "08" + Err.ToString();
//            }
//            finally
//            {
//                zConnect.Close();
//            }
//            return zResult;
//        }

//        public string InsertDefine(string EmployeeKey, DateTime DateWrite, string CreatedBy, string CreatedName)
//        {
//            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, DateWrite.Day, 0, 0, 0);
//            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
//            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);

//            //---------- String SQL Access Database ---------------
//            string zSQL = @"INSERT INTO dbo.HRM_TimeKeeping_Month
//( DateWrite , EmployeeKey ,EmployeeID ,EmployeeName ,TeamKey ,TeamID ,TeamName , 
//CodeKey ,CodeID ,CodeName ,Number ,Amount ,RecordStatus ,
//CreatedOn ,CreatedBy ,CreatedName ,ModifiedOn ,ModifiedBy ,ModifiedName)
//SELECT 
//@ToDate,A.EmployeeKey, A.EmployeeID,A.EmployeeName,A.TeamKey,[dbo].[Fn_GetTeamID](A.TeamKey) AS TeamID,A.TeamName, 
//A.TimeKey,A.TimeName AS RiceID,B.CodeName, COUNT(A.TimeKey)  AS Total, 0,0,
//GETDATE(),@CreatedBy,@CreatedName,GETDATE(),@ModifiedBy,@ModifiedName 
//FROM   dbo.HRM_Employee_KeepingTime A
//LEFT JOIN   dbo.HRM_Time_Defines B ON B.CodeKey= A.TimeKey
//WHERE A.EmployeeKey = @EmployeeKey AND A.DateWrite BETWEEN @FromDate AND @ToDate  AND B.RecordStatus<>99
//GROUP BY A.EmployeeKey,A.EmployeeID, A.EmployeeName ,A.TeamKey,
//[dbo].[Fn_GetTeamID](A.TeamKey),A.TeamName,A.TimeKey,A.TimeName, B.CodeName
//SELECT 11";
//            string zResult = "";
//            string zConnectionString = ConnectDataBase.ConnectionString;
//            SqlConnection zConnect = new SqlConnection(zConnectionString);
//            zConnect.Open();
//            try
//            {
//                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
//                zCommand.CommandType = CommandType.Text;
//                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey; ;
//                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
//                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
//                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = CreatedBy;
//                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = CreatedName;
//                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = CreatedBy;
//                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = CreatedName;
//                _Message = zCommand.ExecuteScalar().ToString();
//                zCommand.Dispose();
//            }
//            catch (Exception Err)
//            {
//                _Message = "08" + Err.ToString();
//            }
//            finally
//            {
//                zConnect.Close();
//            }
//            return zResult;
//        }

      
        #endregion
    }
}
