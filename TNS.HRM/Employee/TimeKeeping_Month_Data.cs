﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
using TNS.Misc;

namespace TNS.HRM
{
    public class TimeKeeping_Month_Data
    {
        //Danh sách mã công cần báo cáo
        public static DataTable ListCodeReport(DateTime DateWrite)
        {
            var zTime = DateWrite.FirstEndMonth();
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.AutoKey, A.ID,A.Name,A.Type,A.TypeName,A.CategoryKey,A.CategoryName,A.Rank FROM [dbo].[HRM_CodeReport] A 
WHERE A.RecordStatus <>99 AND  A.DateWrite BETWEEN @FromDate AND @ToDate 
ORDER BY A.Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zTime.Item1;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zTime.Item2;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        //        //Load công nhân để tính báo cáo
        //        public static DataTable Load_Data(int TeamKey,DateTime LeaveingDate)
        //        {
        //            DataTable zTable = new DataTable();
        //            string zSQL = @"SELECT A.EmployeeKey, A.EmployeeID, A.EmployeeName
        //FROM [dbo].[HRM_Team_Employee_Month] A
        //WHERE A.RecordStatus != 99 
        //AND( MONTH(A.DateWrite ) =MONTH(@Date) AND YEAR(A.DateWrite) =YEAR(@Date))";
        //            if (TeamKey > 0)
        //            {
        //                zSQL += " AND A.TeamKey = @TeamKey ";
        //            }
        //            zSQL += " ORDER BY LEN(A.EmployeeID), A.EmployeeID";
        //            string zConnectionString = ConnectDataBase.ConnectionString;
        //            try
        //            {
        //                SqlConnection zConnect = new SqlConnection(zConnectionString);
        //                zConnect.Open();
        //                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
        //                zCommand.Parameters.Add("@Date", SqlDbType.DateTime).Value = LeaveingDate;
        //                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
        //                zAdapter.Fill(zTable);
        //                zCommand.Dispose();
        //                zConnect.Close();
        //            }
        //            catch (Exception ex)
        //            {
        //                string zstrMessage = "08" + ex.ToString();
        //            }
        //            return zTable;
        //        }
        public static DataTable Load_DataV2(int TeamKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);

            DataTable zTable = new DataTable();
            string zSQL = @"
--declare @FromDate datetime='2020-04-01 00:00:00'
--declare @ToDate datetime='2020-04-30 23:59:59'

--Nhân sự làm việc trong tháng
CREATE TABLE #EmployeeTemp
(
EmployeeKey NVARCHAR(50),
EmployeeID NVARCHAR(50),
EmployeeName NVARCHAR(500),
BranchKey INT,
DepartmentKey INT ,
TeamKey INT ,
PositionKey INT 
)
INSERT INTO #EmployeeTemp
SELECT EmployeeKey,EmployeeID,EmployeeName,
BranchKey,DepartmentKey,TeamKey, PositionKey
 FROM (
		SELECT EmployeeKey,EmployeeID,[dbo].[Fn_GetFullName](EmployeeKey) AS EmployeeName,StartingDate,
			CASE
				WHEN LeavingDate IS NULL THEN GETDATE()
				ELSE LeavingDate
			END LeavingDate,
            [dbo].[Fn_BranchKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate) AS BranchKey,
            [dbo].[Fn_DepartmentKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate) AS DepartmentKey,
            [dbo].[Fn_TeamKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate) AS TeamKey ,
            [dbo].[Fn_PositionKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate) AS PositionKey
			FROM [dbo].[HRM_Employee] 
			WHERE RecordStatus <>99 
	) X
	WHERE (
            ( StartingDate <=@FromDate AND @FromDate <=  LeavingDate  )
	     OR ( StartingDate <=@ToDate   AND @ToDate   <=  LeavingDate )
        )
    @Customer
----------Kết thúc tạo danh sách nhân sự
SELECT EmployeeKey,EmployeeID, EmployeeName FROM #EmployeeTemp 
ORDER BY  LEN(EmployeeID),EmployeeID

DROP TABLE #EmployeeTemp
";
            string zFilter = "";

            if (TeamKey != 0)
            {
                zFilter += " AND TeamKey= @TeamKey";
            }
            if (zFilter.Length != 0)
            {
                zSQL = zSQL.Replace("@Customer", zFilter);
            }
            else
            {
                zSQL = zSQL.Replace("@Customer", "");
            }
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        #region[close-18/08/2020-Load_Data -Thay đổi tổ nhóm]
        //        public static DataTable Load_Data(int TeamKey, DateTime LeaveingDate)
        //        {
        //            DataTable zTable = new DataTable();
        //            string zSQL = @"SELECT A.EmployeeKey, A.EmployeeID, B.FullName AS EmployeeName, B.CardID,B.PersonalKey, B.CreatedOn FROM [dbo].[HRM_Employee] A
        //LEFT JOIN [dbo].[SYS_Personal] B ON A.EmployeeKey = B.ParentKey WHERE A.RecordStatus != 99 AND (LeavingDate IS NULL OR LeavingDate >= @Date)";
        //            if (TeamKey > 0)
        //            {
        //                zSQL += " AND A.TeamKey = @TeamKey";
        //            }
        //            zSQL += " Order by B.CreatedOn";
        //            string zConnectionString = ConnectDataBase.ConnectionString;
        //            try
        //            {
        //                SqlConnection zConnect = new SqlConnection(zConnectionString);
        //                zConnect.Open();
        //                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
        //                zCommand.Parameters.Add("Date", SqlDbType.DateTime).Value = LeaveingDate;
        //                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
        //                zAdapter.Fill(zTable);
        //                zCommand.Dispose();
        //                zConnect.Close();
        //            }
        //            catch (Exception ex)
        //            {
        //                string zstrMessage = "08" + ex.ToString();
        //            }
        //            return zTable;
        //        }

        #endregion

        //Danh sách tất cả tổ nhóm
        public static DataTable DanhSachToNhom()
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT *  FROM SYS_Team A
LEFT JOIN dbo.SYS_Branch B ON b.BranchKey=A.BranchKey
LEFT JOIN dbo.SYS_Department C ON C.DepartmentKey=A.DepartmentKey
WHERE A.RecordStatus <> 99 
ORDER BY B.Rank,C.Rank,A.Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        //Danh sách tất cả khối gián tiếp
        public static DataTable DanhSachToNhomVanPhong()
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT *  FROM SYS_Team A
LEFT JOIN dbo.SYS_Branch B ON B.BranchKey=A.BranchKey
LEFT JOIN dbo.SYS_Department C ON C.DepartmentKey=A.DepartmentKey
WHERE A.RecordStatus <> 99 AND A.BranchKey = 2
ORDER BY B.Rank,C.Rank,A.Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        //Danh sách Tổ hỗ trợ sản xuất
        public static DataTable DanhSachNhomToTro()
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT *  FROM SYS_Team A
LEFT JOIN dbo.SYS_Branch B ON B.BranchKey=A.BranchKey
LEFT JOIN dbo.SYS_Department C ON C.DepartmentKey=A.DepartmentKey
WHERE A.RecordStatus <> 99 AND A.BranchKey = 4 AND A.DepartmentKey = 26 
ORDER BY B.Rank,C.Rank,A.Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        //Danh sách tổ nhóm công nhân
        public static DataTable DanhSachNhomCongNhan()
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT *  FROM SYS_Team A
LEFT JOIN dbo.SYS_Branch B ON B.BranchKey=A.BranchKey
LEFT JOIN dbo.SYS_Department C ON C.DepartmentKey=A.DepartmentKey
WHERE A.RecordStatus <> 99 AND A.BranchKey = 4 AND A.DepartmentKey != 26 
ORDER BY B.Rank,C.Rank,A.Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }

        //Chọn 1 tổ nhóm
        public static DataTable DanhSachToNhom(int TeamKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT *   FROM SYS_Team  WHERE   TeamKey = " + TeamKey + " AND RecordStatus <> 99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }


        //Danh sách mã cơm con theo mục hiển thị cơm
        public static string GetRiceID(string ID)
        {
            string zResult = "";
            string zSQL = @"SELECT RiceID FROM HRM_Time_Rice WHERE RecordStatus <> 99 
            AND ( ID_Rice= @ID OR ID_Number= @ID OR ID_Money= @ID) ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ID", SqlDbType.NVarChar).Value = ID;
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zResult;
        }
        //Lấy số phần cơm
        public static float NumberRice(string EmployeeKey,string RiceID, DateTime DateWrite)
        {
            var zTime = DateWrite.FirstEndMonth();
            float zResult = 0;
            string zSQL = @"SELECT ISNULL(SUM(A.Number),0)  FROM [dbo].HRM_Employee_Rice A
WHERE A.EmployeeKey= @EmployeeKey  AND A.RiceID =  @RiceID
AND  A.DateWrite BETWEEN @FromDate AND @ToDate AND A.RecordStatus <>99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zTime.Item1;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zTime.Item2;
                zCommand.Parameters.Add("@RiceID", SqlDbType.NVarChar).Value = RiceID;
                zResult = zCommand.ExecuteScalar().ToFloat();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zResult;
        }


        #region[Gọi Funtion tính chấm công]
        //Số ngày công trả chế độ phép năm
        public static float CDPN(DateTime Date)
        {
            float zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.HRM_TIME_CDPN_SoNgayCongTraCheDoPhepNam(@Date)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@Date", SqlDbType.DateTime).Value = Date;
                if (float.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //Số ngày công quy định
        public static float NCQD(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            float zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "select [dbo].HRM_TIME_NCQD_LaySoNgayCongQuyDinh(@EmployeeKey,@FromDate,@ToDate)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (float.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //Số ngày nghỉ phép năm
        public static float NNPN(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            float zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.HRM_TIME_NNPN_SoNgayNghiPhepNam(@EmployeeKey,@FromDate,@ToDate)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (float.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //Số ngày làm lễ trong tháng
        public static float NLTT(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            float zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.HRM_TIME_NLTT_LaySoNgayLamLeTrongThang( @EmployeeKey, @FromDate,@ToDate)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (float.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //Dem Số mã công
        public static float DemSoMaCong(string EmployeeKey, string TimeID, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            float zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.HRM_TIME_DemSoMaCong(@EmployeeKey,@TimeID,@FromDate,@ToDate)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@TimeID", SqlDbType.NVarChar).Value = TimeID;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (float.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //Dem số mã cơm
        public static float DemSoMaCom(string EmployeeKey, string RiceID, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            float zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.HRM_TIME_DemSoMaCom(@EmployeeKey,@RiceID,@FromDate,@ToDate)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@RiceID", SqlDbType.NVarChar).Value = RiceID;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (float.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //Số ngày nghỉ không phép
        public static float NNKP(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            float zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.HRM_TIME_NNKP_LaySoNgayNghiKhongPhep(@EmployeeKey,@FromDate,@ToDate)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (float.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //Số ngày chuyên cần
        public static float SNCC(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            float zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.HRM_TIME_SNCC_LaySoNgayChuyenCan(@EmployeeKey,@FromDate,@ToDate)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (float.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //Số ngày thực tế
        public static float SCTT(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            float zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.HRM_TIME_SCTT_LaySoNgayCongThucTe(@EmployeeKey,@FromDate,@ToDate)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (float.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //Số ngày thực tế tính lương
        public static float TTNT(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            float zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.HRM_TIME_TTNT_SoCongThucTeTinhLuong(@EmployeeKey,@FromDate,@ToDate)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (float.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        //Công tích lũy (tháng trước)
        public static float CBTT(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            float zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.HRM_TIME_CBTT_CongTichLuyThangTruoc(@EmployeeKey,@FromDate,@ToDate)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (float.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //Công mượn (tháng trước)
        public static float CMTT(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(-1);
            //zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            float zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.HRM_TIME_CMTT_CongMuonThangTruoc(@EmployeeKey,@ToDate)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                //zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (float.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //Công bù và thâm niên
        public static float CBTN(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            float zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.HRM_TIME_CBTN_CongBuTrongThangVaThamNien(@EmployeeKey,@FromDate,@ToDate)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (float.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        // Hoàn Ứng công mượn
        public static float HUNC(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            float zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.HRM_TIME_HUNC_HoanUngCongMuon(@EmployeeKey,@FromDate,@ToDate)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (float.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //Ứng công
        public static float UTNC(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            float zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.HRM_TIME_UTNC_UngCong(@EmployeeKey,@FromDate,@ToDate)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (float.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        //Số công  thừa nghỉ bù trong tháng (không phải công thừa tồn)
        public static float CTNB(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            float zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.HRM_TIME_CTNB_CongThuaNghiBu(@EmployeeKey,@FromDate,@ToDate)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (float.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        //Số công  thừa tháng đang tính -tồn công
        public static float CTCT(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            float zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.HRM_TIME_CTCT_CongThuaCuoiThangHienTai(@EmployeeKey,@FromDate,@ToDate)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (float.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        //Số công  mượn tháng đang tính -tồn công mượn
        public static float CMCT(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            float zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.HRM_TIME_CMCT_CongMuonCuoiThangHienTai(@EmployeeKey,@FromDate,@ToDate)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (float.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //Số ngày thực tế
        public static float CHPT(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            float zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.HRM_TIME_CHPT_LaySoNgayCuoiHoiVaPhepTang(@EmployeeKey,@FromDate,@ToDate)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (float.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //Tổng phần cơm 16
        public static float CO16(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            float zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.HRM_TIME_C016_LayTongPhanCom16(@EmployeeKey,@FromDate,@ToDate)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (float.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        //Tổng phần cơm 20
        public static float CO20(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            float zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.HRM_TIME_C020_LayTongPhanCom20(@EmployeeKey,@FromDate,@ToDate)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (float.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        //Tổng phần cơm trừ
        public static float COTR(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            float zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.HRM_TIME_C0TR_LayTongPhanComTru(@EmployeeKey,@FromDate,@ToDate)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if (float.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion

    }
}
