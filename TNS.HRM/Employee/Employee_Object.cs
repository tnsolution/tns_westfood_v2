﻿using TNS.SYS;

namespace TNS.HRM
{
    public class Employee_Object:Employee_Info
    {
        private Personal_Object _PersoObject;

        public Personal_Object PersoObject
        {
            get
            {
                return _PersoObject;
            }

            set
            {
                _PersoObject = value;
            }
        }
        public Employee_Object():base()
        {

        }
        public Employee_Object(string EmployeeKey):base(EmployeeKey)
        {
            _PersoObject = new Personal_Object(EmployeeKey);
        }
        public void SaveObject()
        {
            base.Save();
            PersoObject.ParentKey = base.Key;
            PersoObject.ParentTable = "HRM_Employee";
            PersoObject.Save_Object();
        }

        public void DeleteObject()
        {
            base.Delete();
            PersoObject.Delete_Object();
        }
    }
}
