﻿using System;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;

namespace TNS.HRM
{
    public class Contract_Fee_Data
    {
        public static DataTable List()
        {
            DataTable zTable = new DataTable();
            string zSQL = " SELECT * FROM  [dbo].[HRM_Contract_Fee] ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;

        }
        public static DataTable DataTable_Plus(string ContractKey)
        {

            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.*,B.* FROM dbo.HRM_Contract_Fee A
LEFT JOIN dbo.FNC_Fee_Category B ON B.CategoryKey= A.CategoryKey WHERE ContractKey = @ContractKey AND Slug = 3 AND CategoryID <> 'CN'";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ContractKey", SqlDbType.NVarChar).Value = ContractKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zCommand.Clone();
            }
            catch (Exception ex)
            {
                string zStrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable DataTable_Minus(string ContractKey)
        {

            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.*,B.* FROM dbo.HRM_Contract_Fee A
LEFT JOIN dbo.FNC_Fee_Category B ON B.CategoryKey= A.CategoryKey WHERE ContractKey = @ContractKey AND Slug = 4";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ContractKey", SqlDbType.NVarChar).Value = ContractKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zCommand.Clone();
            }
            catch (Exception ex)
            {
                string zStrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable DataTable_Chil(string ContractKey)
        {

            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.*,B.* FROM dbo.HRM_Contract_Fee A
LEFT JOIN dbo.FNC_Fee_Category B ON B.CategoryKey= A.CategoryKey WHERE ContractKey = @ContractKey AND Slug = 3 AND CategoryID = 'CN'";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ContractKey", SqlDbType.NVarChar).Value = ContractKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zCommand.Clone();
            }
            catch (Exception ex)
            {
                string zStrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable DataTable_Fee(string ContractKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT * FROM [dbo].[HRM_Contract_Fee] WHERE ContractKey = @ContractKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ContractKey", SqlDbType.NVarChar).Value = ContractKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zCommand.Clone();
            }
            catch (Exception ex)
            {
                string zStrMessage = ex.ToString();
            }
            return zTable;
        }

    }
}
