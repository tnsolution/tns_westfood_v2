﻿using System;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;

namespace TNS.HRM
{
    public class Contract_Info
    {
        private string _ContractKey = "";
        private string _ParentKey = "";
        private string _ContractID = "";
        private DateTime _SignDay;
        private DateTime _FinishDay;
        private int _CategoryKey = 0;
        private int _CompanyKey = 0;
        private string _PersonalKey = "";
        private int _Slug = 0;
        private string _ReportTo = "";
        private int _BranchKey = 0;
        private int _DepartmentKey = 0;
        private int _TeamKey = 0;
        private int _PositionKey = 0;
        private string _Description = "";
        private double _BasicSalary = 0;
        private DateTime _FromDate;
        private DateTime _ToDate;
        private string _Organization = "";
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _CreatedOn;
        private DateTime _ModifiedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private int _RecordStatus = 0;
        private string _Message = "";

        #region-----PARK-----
        public string Key
        {
            get
            {
                return _ContractKey;
            }

            set
            {
                _ContractKey = value;
            }
        }

        public string ParentKey
        {
            get
            {
                return _ParentKey;
            }

            set
            {
                _ParentKey = value;
            }
        }

        public string ContractID
        {
            get
            {
                return _ContractID;
            }

            set
            {
                _ContractID = value;
            }
        }

        public int CategoryKey
        {
            get
            {
                return _CategoryKey;
            }

            set
            {
                _CategoryKey = value;
            }
        }

        public int CompanyKey
        {
            get
            {
                return _CompanyKey;
            }

            set
            {
                _CompanyKey = value;
            }
        }

        public string PersonalKey
        {
            get
            {
                return _PersonalKey;
            }

            set
            {
                _PersonalKey = value;
            }
        }

        public string ReportTo
        {
            get
            {
                return _ReportTo;
            }

            set
            {
                _ReportTo = value;
            }
        }

        public int BranchKey
        {
            get
            {
                return _BranchKey;
            }

            set
            {
                _BranchKey = value;
            }
        }

        public int DepartmentKey
        {
            get
            {
                return _DepartmentKey;
            }

            set
            {
                _DepartmentKey = value;
            }
        }

        public int TeamKey
        {
            get
            {
                return _TeamKey;
            }

            set
            {
                _TeamKey = value;
            }
        }

        public int PositionKey
        {
            get
            {
                return _PositionKey;
            }

            set
            {
                _PositionKey = value;
            }
        }

        public string Description
        {
            get
            {
                return _Description;
            }

            set
            {
                _Description = value;
            }
        }

        public double BasicSalary
        {
            get
            {
                return _BasicSalary;
            }

            set
            {
                _BasicSalary = value;
            }
        }

        public string Organization
        {
            get
            {
                return _Organization;
            }

            set
            {
                _Organization = value;
            }
        }

        public string CreatedBy
        {
            get
            {
                return _CreatedBy;
            }

            set
            {
                _CreatedBy = value;
            }
        }

        public string CreatedName
        {
            get
            {
                return _CreatedName;
            }

            set
            {
                _CreatedName = value;
            }
        }

        public DateTime CreatedOn
        {
            get
            {
                return _CreatedOn;
            }

            set
            {
                _CreatedOn = value;
            }
        }

        public DateTime ModifiedOn
        {
            get
            {
                return _ModifiedOn;
            }

            set
            {
                _ModifiedOn = value;
            }
        }

        public string ModifiedBy
        {
            get
            {
                return _ModifiedBy;
            }

            set
            {
                _ModifiedBy = value;
            }
        }

        public string ModifiedName
        {
            get
            {
                return _ModifiedName;
            }

            set
            {
                _ModifiedName = value;
            }
        }

        public string Message
        {
            get
            {
                return _Message;
            }

            set
            {
                _Message = value;
            }
        }

        public int RecordStatus
        {
            get
            {
                return _RecordStatus;
            }

            set
            {
                _RecordStatus = value;
            }
        }

        public DateTime FromDate
        {
            get
            {
                return _FromDate;
            }

            set
            {
                _FromDate = value;
            }
        }

        public DateTime ToDate
        {
            get
            {
                return _ToDate;
            }

            set
            {
                _ToDate = value;
            }
        }

        public DateTime SignDay
        {
            get
            {
                return _SignDay;
            }

            set
            {
                _SignDay = value;
            }
        }

        public int Slug
        {
            get
            {
                return _Slug;
            }

            set
            {
                _Slug = value;
            }
        }

        public DateTime FinishDay
        {
            get
            {
                return _FinishDay;
            }

            set
            {
                _FinishDay = value;
            }
        }
        #endregion

        public Contract_Info()
        {

        }

        public Contract_Info(string PersonalKey)
        {
            string zSQL = "SELECT * FROM HRM_Contract WHERE PersonalKey = @PersonalKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@PersonalKey", SqlDbType.NVarChar).Value = PersonalKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    Key = zReader["ContractKey"].ToString();
                    _ContractID = zReader["ContractID"].ToString();
                    if (zReader["SignDay"] != DBNull.Value)
                        _SignDay = DateTime.Parse(zReader["SignDay"].ToString());
                    if (zReader["FinishDay"] != DBNull.Value)
                        _FinishDay = DateTime.Parse(zReader["FinishDay"].ToString());
                    if (zReader["CategoryKey"] != DBNull.Value)
                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    if (zReader["CompanyKey"] != DBNull.Value)
                        _CompanyKey = int.Parse(zReader["CompanyKey"].ToString());
                    if (zReader["ReportTo"] != DBNull.Value)
                        _ReportTo = zReader["ReportTo"].ToString();
                    if (zReader["PositionKey"] != DBNull.Value)
                        _PositionKey = int.Parse(zReader["PositionKey"].ToString());
                    if (zReader["Slug"] != DBNull.Value)
                        _Slug = int.Parse(zReader["Slug"].ToString());
                    if (zReader["BranchKey"] != DBNull.Value)
                        _BranchKey = int.Parse(zReader["BranchKey"].ToString());
                    if (zReader["DepartmentKey"] != DBNull.Value)
                        _DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    if (zReader["TeamKey"] != DBNull.Value)
                        _TeamKey = int.Parse(zReader["TeamKey"].ToString());
                    _Description = zReader["Description"].ToString();
                    if (zReader["BasicSalary"] != DBNull.Value)
                        _BasicSalary = double.Parse(zReader["BasicSalary"].ToString());
                    if (zReader["FromDate"] != DBNull.Value)
                        _FromDate = DateTime.Parse(zReader["FromDate"].ToString());
                    if (zReader["ToDate"] != DBNull.Value)
                        _ToDate = DateTime.Parse(zReader["ToDate"].ToString());
                    _Organization = zReader["Organization"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    //CreatedBy = zReader["CreatedBy"].ToString();
                    //CreatedName = zReader["CreatedName"].ToString();
                    //if (zReader["CreatedOn"] != DBNull.Value)
                    //    _CreatedOn = (DateTime)zReader["CreatedOn"];
                    //if (zReader["ModifiedOn"] != DBNull.Value)
                    //    _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    //ModifiedBy = zReader["ModifiedBy"].ToString();
                    //ModifiedName = zReader["ModifiedName"].ToString();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        public Contract_Info(string PersonalKey, string ContractKey)
        {
            string zSQL = "SELECT * FROM HRM_Contract WHERE PersonalKey = @PersonalKey AND ContractKey = @ContractKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@PersonalKey", SqlDbType.NVarChar).Value = PersonalKey;
                zCommand.Parameters.Add("@ContractKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(ContractKey);
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    Key = zReader["ContractKey"].ToString();
                    _ContractID = zReader["ContractID"].ToString();
                    if (zReader["SignDay"] != DBNull.Value)
                        _SignDay = DateTime.Parse(zReader["SignDay"].ToString());
                    if (zReader["FinishDay"] != DBNull.Value)
                        _FinishDay = DateTime.Parse(zReader["FinishDay"].ToString());
                    if (zReader["CategoryKey"] != DBNull.Value)
                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    if (zReader["CompanyKey"] != DBNull.Value)
                        _CompanyKey = int.Parse(zReader["CompanyKey"].ToString());
                    if (zReader["ReportTo"] != DBNull.Value)
                        _ReportTo = zReader["ReportTo"].ToString();
                    if (zReader["PositionKey"] != DBNull.Value)
                        _PositionKey = int.Parse(zReader["PositionKey"].ToString());
                    if (zReader["Slug"] != DBNull.Value)
                        _Slug = int.Parse(zReader["Slug"].ToString());
                    if (zReader["BranchKey"] != DBNull.Value)
                        _BranchKey = int.Parse(zReader["BranchKey"].ToString());
                    if (zReader["DepartmentKey"] != DBNull.Value)
                        _DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    if (zReader["TeamKey"] != DBNull.Value)
                        _TeamKey = int.Parse(zReader["TeamKey"].ToString());
                    _Description = zReader["Description"].ToString();
                    if (zReader["BasicSalary"] != DBNull.Value)
                        _BasicSalary = double.Parse(zReader["BasicSalary"].ToString());
                    if (zReader["FromDate"] != DBNull.Value)
                        _FromDate = DateTime.Parse(zReader["FromDate"].ToString());
                    if (zReader["ToDate"] != DBNull.Value)
                        _ToDate = DateTime.Parse(zReader["ToDate"].ToString());
                    _Organization = zReader["Organization"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    //CreatedBy = zReader["CreatedBy"].ToString();
                    //CreatedName = zReader["CreatedName"].ToString();
                    //if (zReader["CreatedOn"] != DBNull.Value)
                    //    _CreatedOn = (DateTime)zReader["CreatedOn"];
                    //if (zReader["ModifiedOn"] != DBNull.Value)
                    //    _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    //ModifiedBy = zReader["ModifiedBy"].ToString();
                    //ModifiedName = zReader["ModifiedName"].ToString();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        public Contract_Info(string ParentKey, bool Parent)
        {
            string zSQL = "SELECT * FROM SYS_Personal WHERE PersonalKey = @PersonalKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@PersonalKey", SqlDbType.NVarChar).Value = _PersonalKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    Key = zReader["ContractKey"].ToString();
                    _ContractID = zReader["ContractID"].ToString();
                    if (zReader["SignDay"] != DBNull.Value)
                        _SignDay = DateTime.Parse(zReader["SignDay"].ToString());
                    if (zReader["FinishDay"] != DBNull.Value)
                        _FinishDay = DateTime.Parse(zReader["FinishDay"].ToString());
                    if (zReader["CategoryKey"] != DBNull.Value)
                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    if (zReader["CompanyKey"] != DBNull.Value)
                        _CompanyKey = int.Parse(zReader["CompanyKey"].ToString());
                    if (zReader["ReportTo"] != DBNull.Value)
                        _ReportTo = zReader["ReportTo"].ToString();
                    if (zReader["PositionKey"] != DBNull.Value)
                        _PositionKey = int.Parse(zReader["PositionKey"].ToString());
                    if (zReader["Slug"] != DBNull.Value)
                        _Slug = int.Parse(zReader["Slug"].ToString());
                    if (zReader["BranchKey"] != DBNull.Value)
                        _BranchKey = int.Parse(zReader["BranchKey"].ToString());
                    if (zReader["DepartmentKey"] != DBNull.Value)
                        _DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    if (zReader["TeamKey"] != DBNull.Value)
                        _TeamKey = int.Parse(zReader["TeamKey"].ToString());
                    _Description = zReader["Description"].ToString();
                    if (zReader["BasicSalary"] != DBNull.Value)
                        _BasicSalary = double.Parse(zReader["BasicSalary"].ToString());
                    if (zReader["FromDate"] != DBNull.Value)
                        _FromDate = DateTime.Parse(zReader["FromDate"].ToString());
                    if (zReader["ToDate"] != DBNull.Value)
                        _ToDate = DateTime.Parse(zReader["ToDate"].ToString());
                    _Organization = zReader["Organization"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    //CreatedBy = zReader["CreatedBy"].ToString();
                    //CreatedName = zReader["CreatedName"].ToString();
                    //if (zReader["CreatedOn"] != DBNull.Value)
                    //    _CreatedOn = (DateTime)zReader["CreatedOn"];
                    //if (zReader["ModifiedOn"] != DBNull.Value)
                    //    _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    //ModifiedBy = zReader["ModifiedBy"].ToString();
                    //ModifiedName = zReader["ModifiedName"].ToString();
                    zReader.Close();
                    zCommand.Dispose();
                }
            }
            catch (Exception Err) { Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }

        public string Create()
        {
            string zSQL = "HRM_Contract_INSERT";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@ParentKey", SqlDbType.NVarChar).Value = _ParentKey;
                zCommand.Parameters.Add("@ContractID", SqlDbType.NVarChar).Value = _ContractID;
                if (_SignDay == DateTime.MinValue)
                    zCommand.Parameters.Add("@SignDay", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@SignDay", SqlDbType.DateTime).Value = _SignDay;
                if (_FinishDay == DateTime.MinValue)
                    zCommand.Parameters.Add("@FinishDay", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@FinishDay", SqlDbType.DateTime).Value = _FinishDay;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@CompanyKey", SqlDbType.Int).Value = _CompanyKey;
                zCommand.Parameters.Add("@PersonalKey", SqlDbType.NVarChar).Value = _PersonalKey;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = _Slug;
                zCommand.Parameters.Add("@ReportTo", SqlDbType.NVarChar).Value = _ReportTo;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = _BranchKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = _TeamKey;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = _PositionKey;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zCommand.Parameters.Add("@BasicSalary", SqlDbType.Money).Value = _BasicSalary;
                if (_FromDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = _FromDate;
                if (_ToDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = _ToDate;
                zCommand.Parameters.Add("@Organization", SqlDbType.NVarChar).Value = _Organization;
                zCommand.Parameters.Add("RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                _Message = zCommand.ExecuteScalar().ToString();
                if (_Message.Substring(0, 2) == "11")
                    _ContractKey = _Message.Substring(2, 36);
                else
                    _ContractKey = "";
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string Update()
        {
            string zSQL = "HRM_Contract_UPDATE";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                if (_ContractKey.Length == 36)
                    zCommand.Parameters.Add("@ContractKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_ContractKey);
                else
                    zCommand.Parameters.Add("@ContractKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@ParentKey", SqlDbType.NVarChar).Value = _ParentKey;
                zCommand.Parameters.Add("@ContractID", SqlDbType.NVarChar).Value = _ContractID;
                if (_SignDay == DateTime.MinValue)
                    zCommand.Parameters.Add("@SignDay", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@SignDay", SqlDbType.DateTime).Value = _SignDay;
                if (_FinishDay == DateTime.MinValue)
                    zCommand.Parameters.Add("@FinishDay", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@FinishDay", SqlDbType.DateTime).Value = _FinishDay;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@CompanyKey", SqlDbType.Int).Value = _CompanyKey;
                zCommand.Parameters.Add("@PersonalKey", SqlDbType.NVarChar).Value = _PersonalKey;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = _Slug;
                zCommand.Parameters.Add("@ReportTo", SqlDbType.NVarChar).Value = _ReportTo;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = _BranchKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = _TeamKey;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = _PositionKey;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zCommand.Parameters.Add("@BasicSalary", SqlDbType.Money).Value = _BasicSalary;
                if (_FromDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = _FromDate;
                if (_ToDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = _ToDate;
                zCommand.Parameters.Add("@Organization", SqlDbType.NVarChar).Value = _Organization;
                zCommand.Parameters.Add("RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                //zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                //zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                //zCommand.Parameters.Add("@CreatedOn", SqlDbType.DateTime).Value = _CreatedOn;
                //zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                //zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                _Message = zCommand.ExecuteScalar().ToString();
                if (_Message.Substring(0, 2) == "20")
                    _ContractKey = _Message.Substring(2, 36);
                else
                    _ContractKey = "";
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string Delete()
        {
            string zResult = "";
            string zSQL = "HRM_Contract_DELETE";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ContractKey", SqlDbType.NVarChar).Value = _ContractKey;
                //zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = ModifiedBy;
                //zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string Save()
        {
            string zResult = "";
            if (_ContractKey.Length == 0)
            {
                zResult = Create();
            }
            else if (_ContractKey.Length > 0)
            {
                zResult = Update();
            }
            return zResult;
        }

        public string Update(string ContractKey)
        {
            string zSQL = "UPDATE HRM_Contract SET RecordStatus = 62 WHERE ContractKey = @ContractKey SELECT 20";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (ContractKey.Length == 36)
                    zCommand.Parameters.Add("@ContractKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(ContractKey);
                else
                    zCommand.Parameters.Add("@ContractKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

    }
}
