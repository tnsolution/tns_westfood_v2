﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
namespace TNS.HRM
{
    public class WorkingHistory_Detail
    {

        #region [ Field Name ]
        private string _HistoryKey = "";
        private DateTime _StartingDate;
        private DateTime _LeavingDate;
        private string _EmployeeKey = "";
        private string _EmployeeID = "";
        private string _EmployeeName = "";
        private int _BranchKey = 0;
        private string _BranchName = "";
        private int _DepartmentKey = 0;
        private string _DepartmentName = "";
        private int _TeamKey = 0;
        private string _TeamName = "";
        private int _PositionKey = 0;
        private string _PositionName = "";
        private string _ReportToKey = "";
        private string _ReportToName = "";
        private string _OrganizationName = "";
        private string _JobName = "";
        private string _DocumentFile = "";
        private int _RecordStatus = 0;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _CreatedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private DateTime _ModifiedOn;
        private string _RoleID = "";
        private string _Message = "";
        private string _Description = "";
        #endregion
        #region [ Properties ]
        public bool IsCommandOK
        {
            get
            {
                int zNumber = 0;
                int.TryParse(_Message.Substring(0, 1), out zNumber);
                if (zNumber <= 3) return true; else return false;
            }
        }
        public bool HasInfo
        {
            get
            {
                if (_EmployeeKey.Trim().Length > 0) return true; else return false;
            }
        }
        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        public string Key
        {
            get { return _HistoryKey; }
            set { _HistoryKey = value; }
        }
        public DateTime StartingDate
        {
            get { return _StartingDate; }
            set { _StartingDate = value; }
        }
        public DateTime LeavingDate
        {
            get { return _LeavingDate; }
            set { _LeavingDate = value; }
        }
        public string EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public string EmployeeID
        {
            get { return _EmployeeID; }
            set { _EmployeeID = value; }
        }
        public string EmployeeName
        {
            get { return _EmployeeName; }
            set { _EmployeeName = value; }
        }
        public int BranchKey
        {
            get { return _BranchKey; }
            set { _BranchKey = value; }
        }
        public string BranchName
        {
            get { return _BranchName; }
            set { _BranchName = value; }
        }
        public int DepartmentKey
        {
            get { return _DepartmentKey; }
            set { _DepartmentKey = value; }
        }
        public string DepartmentName
        {
            get { return _DepartmentName; }
            set { _DepartmentName = value; }
        }
        public int TeamKey
        {
            get { return _TeamKey; }
            set { _TeamKey = value; }
        }
        public string TeamName
        {
            get { return _TeamName; }
            set { _TeamName = value; }
        }
        public int PositionKey
        {
            get { return _PositionKey; }
            set { _PositionKey = value; }
        }
        public string PositionName
        {
            get { return _PositionName; }
            set { _PositionName = value; }
        }
        public string ReportToKey
        {
            get { return _ReportToKey; }
            set { _ReportToKey = value; }
        }
        public string ReportToName
        {
            get { return _ReportToName; }
            set { _ReportToName = value; }
        }
        public string OrganizationName
        {
            get { return _OrganizationName; }
            set { _OrganizationName = value; }
        }
        public string JobName
        {
            get { return _JobName; }
            set { _JobName = value; }
        }
        public string DocumentFile
        {
            get { return _DocumentFile; }
            set { _DocumentFile = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public string Description
        {
            get
            {
                return _Description;
            }

            set
            {
                _Description = value;
            }
        }
        #endregion
        #region [ Constructor Get Information ]
        public WorkingHistory_Detail()
        {
        }
        public WorkingHistory_Detail(string HistoryKey)
        {
            string zSQL = "SELECT * FROM HRM_WorkingHistory WHERE HistoryKey = @HistoryKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (HistoryKey.Length == 36)
                    zCommand.Parameters.Add("@HistoryKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(HistoryKey);
                else
                    zCommand.Parameters.Add("@HistoryKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _HistoryKey = zReader["HistoryKey"].ToString();
                    if (zReader["StartingDate"] != DBNull.Value)
                        _StartingDate = (DateTime)zReader["StartingDate"];
                    if (zReader["LeavingDate"] != DBNull.Value)
                        _LeavingDate = (DateTime)zReader["LeavingDate"];
                    _EmployeeKey = zReader["EmployeeKey"].ToString();
                    _EmployeeID = zReader["EmployeeID"].ToString().Trim();
                    _EmployeeName = zReader["EmployeeName"].ToString().Trim();
                    if (zReader["BranchKey"] != DBNull.Value)
                        _BranchKey = int.Parse(zReader["BranchKey"].ToString());
                    _BranchName = zReader["BranchName"].ToString().Trim();
                    if (zReader["DepartmentKey"] != DBNull.Value)
                        _DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    _DepartmentName = zReader["DepartmentName"].ToString().Trim();
                    if (zReader["TeamKey"] != DBNull.Value)
                        _TeamKey = int.Parse(zReader["TeamKey"].ToString());
                    _TeamName = zReader["TeamName"].ToString().Trim();
                    if (zReader["PositionKey"] != DBNull.Value)
                        _PositionKey = int.Parse(zReader["PositionKey"].ToString());
                    _PositionName = zReader["PositionName"].ToString().Trim();
                    _ReportToKey = zReader["ReportToKey"].ToString();
                    _ReportToName = zReader["ReportToName"].ToString().Trim();
                    _OrganizationName = zReader["OrganizationName"].ToString().Trim();
                    _JobName = zReader["JobName"].ToString().Trim();
                    _DocumentFile = zReader["DocumentFile"].ToString().Trim();
                    _Description = zReader["Description"].ToString().Trim();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
     
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_WorkingHistory ("
        + " HistoryKey,StartingDate ,LeavingDate ,EmployeeKey ,EmployeeID ,EmployeeName ,BranchKey ,BranchName ,DepartmentKey ,DepartmentName ,TeamKey ,TeamName ,PositionKey ,PositionName ,ReportToKey ,ReportToName ,OrganizationName ,JobName ,DocumentFile ,Description, RecordStatus ,CreatedBy ,CreatedName ,CreatedOn ,ModifiedBy ,ModifiedName ,ModifiedOn ) "
         + " VALUES ( "
         + "@HistoryKey, @StartingDate ,@LeavingDate ,@EmployeeKey ,@EmployeeID ,@EmployeeName ,@BranchKey ,@BranchName ,@DepartmentKey ,@DepartmentName ,@TeamKey ,@TeamName ,@PositionKey ,@PositionName ,@ReportToKey ,@ReportToName ,@OrganizationName ,@JobName ,@DocumentFile , @Description ,@RecordStatus ,@CreatedBy ,@CreatedName ,GETDATE(),@ModifiedBy ,@ModifiedName ,GETDATE()) ";

            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                Guid zNewID = Guid.NewGuid();
                _HistoryKey = zNewID.ToString();
                zCommand.Parameters.Add("@HistoryKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_HistoryKey);
                if (_StartingDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@StartingDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@StartingDate", SqlDbType.DateTime).Value = _StartingDate;
                if (_LeavingDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@LeavingDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@LeavingDate", SqlDbType.DateTime).Value = _LeavingDate;
                if (_EmployeeKey.Length == 36)
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_EmployeeKey);
                else
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID.Trim();
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName.Trim();
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = _BranchKey;
                zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = _BranchName.Trim();
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = _DepartmentName.Trim();
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = _TeamKey;
                zCommand.Parameters.Add("@TeamName", SqlDbType.NVarChar).Value = _TeamName.Trim();
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = _PositionKey;
                zCommand.Parameters.Add("@PositionName", SqlDbType.NVarChar).Value = _PositionName.Trim();
                if (_ReportToKey.Length > 0)
                    zCommand.Parameters.Add("@ReportToKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_ReportToKey);
                else
                    zCommand.Parameters.Add("@ReportToKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@ReportToName", SqlDbType.NVarChar).Value = _ReportToName.Trim();
                zCommand.Parameters.Add("@OrganizationName", SqlDbType.NVarChar).Value = _OrganizationName.Trim();
                zCommand.Parameters.Add("@JobName", SqlDbType.NVarChar).Value = _JobName.Trim();
                zCommand.Parameters.Add("@DocumentFile", SqlDbType.NVarChar).Value = _DocumentFile.Trim();
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE HRM_WorkingHistory SET "
                        + " StartingDate = @StartingDate,"
                        + " LeavingDate = @LeavingDate,"
                        + " EmployeeKey = @EmployeeKey,"
                        + " EmployeeID = @EmployeeID,"
                        + " EmployeeName = @EmployeeName,"
                        + " BranchKey = @BranchKey,"
                        + " BranchName = @BranchName,"
                        + " DepartmentKey = @DepartmentKey,"
                        + " DepartmentName = @DepartmentName,"
                        + " TeamKey = @TeamKey,"
                        + " TeamName = @TeamName,"
                        + " PositionKey = @PositionKey,"
                        + " PositionName = @PositionName,"
                        + " ReportToKey = @ReportToKey,"
                        + " ReportToName = @ReportToName,"
                        + " OrganizationName = @OrganizationName,"
                        + " JobName = @JobName,"
                        + " DocumentFile = @DocumentFile,"
                        + " Description =@Description,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName,"
                        + " ModifiedOn = GETDATE()"
                       + " WHERE HistoryKey = @HistoryKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (_HistoryKey.Length == 36)
                    zCommand.Parameters.Add("@HistoryKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_HistoryKey);
                else
                    zCommand.Parameters.Add("@HistoryKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                if (_StartingDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@StartingDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@StartingDate", SqlDbType.DateTime).Value = _StartingDate;
                if (_LeavingDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@LeavingDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@LeavingDate", SqlDbType.DateTime).Value = _LeavingDate;
                if (_EmployeeKey.Length == 36)
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_EmployeeKey);
                else
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID.Trim();
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName.Trim();
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = _BranchKey;
                zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = _BranchName.Trim();
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = _DepartmentName.Trim();
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = _TeamKey;
                zCommand.Parameters.Add("@TeamName", SqlDbType.NVarChar).Value = _TeamName.Trim();
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = _PositionKey;
                zCommand.Parameters.Add("@PositionName", SqlDbType.NVarChar).Value = _PositionName.Trim();
                if (_ReportToKey.Length == 36)
                    zCommand.Parameters.Add("@ReportToKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_ReportToKey);
                else
                    zCommand.Parameters.Add("@ReportToKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@ReportToName", SqlDbType.NVarChar).Value = _ReportToName.Trim();
                zCommand.Parameters.Add("@OrganizationName", SqlDbType.NVarChar).Value = _OrganizationName.Trim();
                zCommand.Parameters.Add("@JobName", SqlDbType.NVarChar).Value = _JobName.Trim();
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                zCommand.Parameters.Add("@DocumentFile", SqlDbType.NVarChar).Value = _DocumentFile.Trim();
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE HRM_WorkingHistory SET [RecordStatus] = 99, [ModifiedOn] = GETDATE(), ModifiedBy = @ModifiedBy, ModifiedName = @ModifiedName WHERE HistoryKey = @HistoryKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@HistoryKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_HistoryKey);
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_HistoryKey.Trim().Length == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        #endregion
    }
}
