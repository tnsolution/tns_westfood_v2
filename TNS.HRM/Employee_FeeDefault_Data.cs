﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
namespace TNS.HRM
{
    public class Employee_FeeDefault_Data
    {
        public static DataTable ListCategory()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT CategoryID, CategoryName FROM [dbo].[HRM_Employee_FeeDefault] WHERE RecordStatus <> 99 group by CategoryID,CategoryName  ORDER BY CategoryID";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable ListDepartment()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM SYS_Department WHERE RecordStatus != 99 ORDER BY [RANK]";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable ListTeam(int DepartmentKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM SYS_Team WHERE DepartmentKey =@DepartmentKey AND RecordStatus != 99 ORDER BY [RANK]";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable List(int TeamKey, int DepartmentKey, string CategoryID, int Type)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT *,[dbo].HRM_LayGiaTri_Parametter(FromDate,@CategoryID) AS Persent 
            FROM [dbo].[HRM_Employee_FeeDefault] WHERE RecordStatus <> 99 ";
            if (Type == 1)
                zSQL += " AND ToDate is null ";
            if (Type == 2)
                zSQL += " AND ToDate is not null ";
            if (TeamKey > 0)
                zSQL += " AND TeamKey =@TeamKey ";
            if (DepartmentKey > 0)
                zSQL += " AND DepartmentKey =@DepartmentKey ";
            zSQL += " AND CategoryID =@CategoryID ";
            zSQL += " ORDER BY LEN(EmployeeID) ,EmployeeID";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@CategoryID", SqlDbType.NVarChar).Value = CategoryID.Trim();
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable List01(int BranchKey, int DepartmentKey, int TeamKey, int CategoryKey, int Type, string Search,DateTime DateSearch)
        {
            DateTime zFromDate = new DateTime(DateSearch.Year, DateSearch.Month, DateSearch.Day, 0, 0, 0);
            DataTable zTable = new DataTable();
            string zSQL = ";WITH #TEMP AS (";
            zSQL += @"SELECT *,[dbo].[Fn_GetTeamKey_FromEmployeeKey](EmployeeKey) AS TeamKeys, 
                    [dbo].[Fn_GetBranchKey_FromEmployeeKey](EmployeeKey) AS BranchKey ,
                    [dbo].HRM_LayGiaTri_Parametter(@DateSearch,CategoryID) AS GiaTri 
                    FROM [dbo].[HRM_Employee_FeeDefault] 
                    WHERE RecordStatus <> 99 ";
            if (Type == 1)
                zSQL += " AND ToDate is null ";
            if (Type == 2)
                zSQL += " AND ToDate is not null ";
            if (TeamKey > 0)
                zSQL += " AND [dbo].[Fn_GetTeamKey_FromEmployeeKey](EmployeeKey)=@TeamKey ";
            if (DepartmentKey > 0)
                zSQL += " AND [dbo].[Fn_GetDepartmentKey_FromEmployeeKey](EmployeeKey) =@DepartmentKey ";
            if (BranchKey > 0)
            {
                zSQL += " AND [dbo].[Fn_GetBranchKey_FromEmployeeKey](EmployeeKey) = @BranchKey";
            }
            if (Search.Trim().Length > 0)
            {
                zSQL += " AND ( EmployeeID LIKE @Search OR EmployeeName LIKE @Search )";
            }
            if (CategoryKey > 0)
            {
                zSQL += " AND CategoryKey =@CategoryKey ";
            }
            zSQL += ")";
            zSQL += @"SELECT A.AutoKey,A.EmployeeName,A.EmployeeID,A.TeamKeys AS TeamKey,B.TeamID,B.TeamName,A.CategoryID,A.GiaTri,A.FromDate,A.ToDate,A.Description FROM #TEMP A
LEFT JOIN[dbo].[SYS_Team] B ON B.TeamKey=A.TeamKeys
LEFT JOIN[dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
LEFT JOIN[dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
ORDER BY D.RANK, C.RANK, B.RANK, LEN(EmployeeID), EmployeeID,FromDate DESC";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BranchKey;
                zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search.Trim() + "%";
                zCommand.Parameters.Add("@DateSearch", SqlDbType.DateTime).Value = zFromDate;
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }

        public static string  FeeDefault_CapNhatNgayHetHan_NghiViec(string EmployeeKey,DateTime DateTime,string ModifiedBy, string ModifiedName)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"UPDATE [dbo].[HRM_Employee_FeeDefault]  SET ToDate = @ToDate , ModifiedOn =GETDATE(),ModifiedBy =@ModifiedBy, ModifiedName=@ModifiedName
WHERE EmployeeKey= @EmployeeKey AND ToDate IS NULL AND RecordStatus <> 99 ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = DateTime;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = ModifiedName;
                zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {
                zResult = Ex.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
    }
}
