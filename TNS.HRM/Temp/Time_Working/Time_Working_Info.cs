﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
namespace TNS.HRM
{
    public class Time_Working_Info
    {

        #region [ Field Name ]
        private string _WorkingKey = "";
        private string _WorkingID = "";
        private DateTime _BeginTime;
        private DateTime _EndTime;
        private float _OffTime;
        private string _Description = "";
        private int _Slug = 0;
        private int _CodeKey = 0;
        private int _RecordStatus = 0;
        private string _EmployeeKey = "";
        private string _Organization = "";
        private DateTime _CreatedOn;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _RoleID = "";
        private string _Message = "";
        private float _DifferenTime = 0;
        #endregion
        #region [ Properties ]
        public bool IsCommandOK
        {
            get
            {
                int zNumber = 0;
                int.TryParse(_Message.Substring(0, 1), out zNumber);
                if (zNumber <= 3) return true; else return false;
            }
        }
        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        public string Key
        {
            get { return _WorkingKey; }
            set { _WorkingKey = value; }
        }
        public string WorkingID
        {
            get { return _WorkingID; }
            set { _WorkingID = value; }
        }
        public DateTime BeginTime
        {
            get { return _BeginTime; }
            set { _BeginTime = value; }
        }
        public DateTime EndTime
        {
            get { return _EndTime; }
            set { _EndTime = value; }
        }
        public float OffTime
        {
            get { return _OffTime; }
            set { _OffTime = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public int Slug
        {
            get { return _Slug; }
            set { _Slug = value; }
        }
        public int CodeKey
        {
            get { return _CodeKey; }
            set { _CodeKey = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public string EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }

        public string Organization
        {
            get { return _Organization; }
            set { _Organization = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public float DifferenTime
        {
            get
            {
                return _DifferenTime;
            }

            set
            {
                _DifferenTime = value;
            }
        }
        #endregion
        #region [ Constructor Get Information ]
        public Time_Working_Info()
        {
        }
        public Time_Working_Info(string WorkingKey)
        {
            string zSQL = "SELECT * FROM HRM_Time_Working WHERE WorkingKey = @WorkingKey AND RecordStatus <> 99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (WorkingKey.Length == 36)
                    zCommand.Parameters.Add("@WorkingKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(WorkingKey);
                else
                    zCommand.Parameters.Add("@WorkingKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _WorkingKey = zReader["WorkingKey"].ToString();
                    _WorkingID = zReader["WorkingID"].ToString().Trim();
                    if (zReader["BeginTime"] != DBNull.Value)
                        _BeginTime = (DateTime)zReader["BeginTime"];
                    if (zReader["EndTime"] != DBNull.Value)
                        _EndTime = (DateTime)zReader["EndTime"];
                    if (zReader["OffTime"] != DBNull.Value)
                        _OffTime = float.Parse(zReader["OffTime"].ToString());
                    if (zReader["DifferenTime"] != DBNull.Value)
                        _DifferenTime = float.Parse(zReader["DifferenTime"].ToString());
                    _Description = zReader["Description"].ToString().Trim();
                    if (zReader["Slug"] != DBNull.Value)
                        _Slug = int.Parse(zReader["Slug"].ToString());
                    if (zReader["CodeKey"] != DBNull.Value)
                        _CodeKey = int.Parse(zReader["CodeKey"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                        _EmployeeKey = zReader["EmployeeKey"].ToString();                 
                    _Organization = zReader["Organization"].ToString().Trim();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message =  Err.ToString(); }
            finally { zConnect.Close(); }
        }

        public Time_Working_Info(string EmployeeKey,DateTime BeginTime,DateTime EndTime)
        {
            string zSQL = @"SELECT * FROM [dbo].[HRM_Time_Working] 
WHERE EmployeeKey = @EmployeeKey AND BeginTime = @BeginTime AND EndTime = @EndTime AND RecordStatus <> 99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (EmployeeKey.Length == 36)
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(EmployeeKey);
                else
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                if (BeginTime == DateTime.MinValue)
                    zCommand.Parameters.Add("@BeginTime", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@BeginTime", SqlDbType.DateTime).Value = BeginTime;
                if (EndTime == DateTime.MinValue)
                    zCommand.Parameters.Add("@EndTime", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@EndTime", SqlDbType.DateTime).Value = EndTime;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _WorkingKey = zReader["WorkingKey"].ToString();
                    _WorkingID = zReader["WorkingID"].ToString().Trim();
                    if (zReader["BeginTime"] != DBNull.Value)
                        _BeginTime = (DateTime)zReader["BeginTime"];
                    if (zReader["EndTime"] != DBNull.Value)
                        _EndTime = (DateTime)zReader["EndTime"];
                    if (zReader["OffTime"] != DBNull.Value)
                        _OffTime = float.Parse(zReader["OffTime"].ToString());
                    if (zReader["DifferenTime"] != DBNull.Value)
                        _DifferenTime = float.Parse(zReader["DifferenTime"].ToString());
                    _Description = zReader["Description"].ToString().Trim();
                    if (zReader["Slug"] != DBNull.Value)
                        _Slug = int.Parse(zReader["Slug"].ToString());
                    if (zReader["CodeKey"] != DBNull.Value)
                        _CodeKey = int.Parse(zReader["CodeKey"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    _EmployeeKey = zReader["EmployeeKey"].ToString();
                    _Organization = zReader["Organization"].ToString().Trim();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = @"INSERT INTO HRM_Time_Working
(WorkingKey,WorkingID, BeginTime, EndTime, OffTime, Description, Slug, 
CodeKey, EmployeeKey,  Organization,DifferenTime,
CreatedOn, CreatedBy, CreatedName, ModifiedOn,ModifiedBy, ModifiedName) 
VALUES 
(NewID(),@WorkingID,  @BeginTime,  @EndTime, @OffTime,  @Description, @Slug,  
@CodeKey,@EmployeeKey,@Organization, @DifferenTime,
 GETDATE(), @CreatedBy,  @CreatedName, GETDATE(),@ModifiedBy,  @ModifiedName )";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@WorkingID", SqlDbType.NVarChar).Value = _WorkingID.Trim();
                if (_BeginTime == DateTime.MinValue)
                    zCommand.Parameters.Add("@BeginTime", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@BeginTime", SqlDbType.DateTime).Value = _BeginTime;
                if (_EndTime == DateTime.MinValue)
                    zCommand.Parameters.Add("@EndTime", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@EndTime", SqlDbType.DateTime).Value = _EndTime;
                zCommand.Parameters.Add("@OffTime", SqlDbType.Float).Value = _OffTime;
                zCommand.Parameters.Add("@DifferenTime", SqlDbType.Float).Value = _DifferenTime;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = _Slug;
                zCommand.Parameters.Add("@CodeKey", SqlDbType.Int).Value = _CodeKey;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                if (_EmployeeKey.Length == 0)
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_EmployeeKey);

                zCommand.Parameters.Add("@Organization", SqlDbType.NVarChar).Value = _Organization.Trim();
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
               
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message =  Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = @" UPDATE dbo.HRM_Time_Working SET 
			WorkingID = @WorkingID,
			BeginTime = @BeginTime,
			EndTime = @EndTime,
			OffTime = @OffTime,
			Description = @Description,
			Slug = @Slug,
			CodeKey = @CodeKey,
			EmployeeKey = @EmployeeKey,
			Organization = @Organization,
            DifferenTime =@DifferenTime,
			ModifiedOn = GETDATE(),
			ModifiedBy = @ModifiedBy,
			ModifiedName = @ModifiedName
          WHERE RecordStatus != 99 AND WorkingKey = @WorkingKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (_WorkingKey.Length == 36)
                    zCommand.Parameters.Add("@WorkingKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_WorkingKey);
                else
                    zCommand.Parameters.Add("@WorkingKey", SqlDbType.Int).Value = DBNull.Value;
                zCommand.Parameters.Add("@WorkingID", SqlDbType.NVarChar).Value = _WorkingID.Trim();
                if (_BeginTime == DateTime.MinValue)
                    zCommand.Parameters.Add("@BeginTime", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@BeginTime", SqlDbType.DateTime).Value = _BeginTime;
                if (_EndTime == DateTime.MinValue)
                    zCommand.Parameters.Add("@EndTime", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@EndTime", SqlDbType.DateTime).Value = _EndTime;
                zCommand.Parameters.Add("@OffTime", SqlDbType.Float).Value = _OffTime;
                zCommand.Parameters.Add("@DifferenTime", SqlDbType.Float).Value = _DifferenTime;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = _Slug;
                zCommand.Parameters.Add("@CodeKey", SqlDbType.Int).Value = _CodeKey;
                if (_EmployeeKey.Length == 0)
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_EmployeeKey);
                zCommand.Parameters.Add("@Organization", SqlDbType.NVarChar).Value = _Organization.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message =  Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @" UPDATE dbo.HRM_Time_Working SET
      [RecordStatus] = 99,
      [ModifiedOn] = GETDATE(),
      [ModifiedBy] = @ModifiedBy,
      [ModifiedName] = @ModifiedName
      WHERE [WorkingKey] = @WorkingKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@WorkingKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_WorkingKey);
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }       
        public string Save()
        {
            string zResult;
            if (_WorkingKey.Trim().Length == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        #endregion
    }
}
