﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
namespace TNS.HRM
{
    public class DayOff_Support_Info
    {

        #region [ Field Name ]
        private int _AutoKey = 0;
        private string _EmployeeKey = "";
        private string _EmployeeID = "";
        private string _EmployeeName = "";
        private DateTime _CheckDate;
        private double _DayOff;
        private double _SalaryBasic = 0;
        private float _Day_Standard;
        private double _Money = 0;
        private string _Description = "";
        private int _Slug = 0;
        private int _RecordStatus = 0;
        private DateTime _CreatedOn;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _RoleID = "";
        private string _Message = "";
        private double _Unit_Price = 0;
        #endregion
        #region [ Properties ]

        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        public int Key
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public string EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public string EmployeeID
        {
            get { return _EmployeeID; }
            set { _EmployeeID = value; }
        }
        public string EmployeeName
        {
            get { return _EmployeeName; }
            set { _EmployeeName = value; }
        }
        public DateTime CheckDate
        {
            get { return _CheckDate; }
            set { _CheckDate = value; }
        }
        public double DayOff
        {
            get { return _DayOff; }
            set { _DayOff = value; }
        }
        public double SalaryBasic
        {
            get { return _SalaryBasic; }
            set { _SalaryBasic = value; }
        }
        public float Day_Standard
        {
            get { return _Day_Standard; }
            set { _Day_Standard = value; }
        }
        public double Money
        {
            get { return _Money; }
            set { _Money = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public int Slug
        {
            get { return _Slug; }
            set { _Slug = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public double Unit_Price
        {
            get
            {
                return _Unit_Price;
            }

            set
            {
                _Unit_Price = value;
            }
        }
        #endregion
        #region [ Constructor Get Information ]
        public DayOff_Support_Info()
        {
        }
        public DayOff_Support_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM HRM_DayOff_Support WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    _EmployeeKey = zReader["EmployeeKey"].ToString().Trim();
                    _EmployeeID = zReader["EmployeeID"].ToString().Trim();
                    _EmployeeName = zReader["EmployeeName"].ToString().Trim();
                    if (zReader["CheckDate"] != DBNull.Value)
                        _CheckDate = (DateTime)zReader["CheckDate"];
                    if (zReader["Unit_Price"] != DBNull.Value)
                        _Unit_Price = float.Parse(zReader["Unit_Price"].ToString());
                    if (zReader["DayOff"] != DBNull.Value)
                        _DayOff = float.Parse(zReader["DayOff"].ToString()); if (zReader["SalaryBasic"] != DBNull.Value)
                        _SalaryBasic = double.Parse(zReader["SalaryBasic"].ToString());
                    if (zReader["Day_Standard"] != DBNull.Value)
                        _Day_Standard = float.Parse(zReader["Day_Standard"].ToString());
                    if (zReader["Money"] != DBNull.Value)
                        _Money = double.Parse(zReader["Money"].ToString());
                    _Description = zReader["Description"].ToString().Trim();
                    if (zReader["Slug"] != DBNull.Value)
                        _Slug = int.Parse(zReader["Slug"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }

        public DayOff_Support_Info(DataRow zRow)
        {
            try
            {
                if (zRow["AutoKey"] != DBNull.Value)
                    _AutoKey = int.Parse(zRow["AutoKey"].ToString());
                _EmployeeKey = zRow["EmployeeKey"].ToString().Trim();
                _EmployeeID = zRow["EmployeeID"].ToString().Trim();
                _EmployeeName = zRow["EmployeeName"].ToString().Trim();
                if (zRow["CheckDate"] != DBNull.Value)
                    _CheckDate = (DateTime)zRow["CheckDate"];
                if (zRow["Unit_Price"] != DBNull.Value)
                    _Unit_Price = float.Parse(zRow["Unit_Price"].ToString());
                if (zRow["DayOff"] != DBNull.Value)
                    _DayOff = float.Parse(zRow["DayOff"].ToString());
                if (zRow["SalaryBasic"] != DBNull.Value)
                    _SalaryBasic = double.Parse(zRow["SalaryBasic"].ToString());
                if (zRow["Day_Standard"] != DBNull.Value)
                    _Day_Standard = float.Parse(zRow["Day_Standard"].ToString());
                if (zRow["Money"] != DBNull.Value)
                    _Money = double.Parse(zRow["Money"].ToString());
                _Description = zRow["Description"].ToString().Trim();
                if (zRow["Slug"] != DBNull.Value)
                    _Slug = int.Parse(zRow["Slug"].ToString());
                if (zRow["RecordStatus"] != DBNull.Value)
                    _RecordStatus = int.Parse(zRow["RecordStatus"].ToString());
                if (zRow["CreatedOn"] != DBNull.Value)
                    _CreatedOn = (DateTime)zRow["CreatedOn"];
                _CreatedBy = zRow["CreatedBy"].ToString().Trim();
                _CreatedName = zRow["CreatedName"].ToString().Trim();
                if (zRow["ModifiedOn"] != DBNull.Value)
                    _ModifiedOn = (DateTime)zRow["ModifiedOn"];
                _ModifiedBy = zRow["ModifiedBy"].ToString().Trim();
                _ModifiedName = zRow["ModifiedName"].ToString().Trim();
            }
            catch (Exception Err)
            { _Message = "08" + Err.ToString(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_DayOff_Support ("
        + " EmployeeKey ,EmployeeID ,EmployeeName ,CheckDate ,Unit_Price,DayOff ,SalaryBasic ,Day_Standard ,Money ,Description ,Slug ,RecordStatus ,CreatedOn ,CreatedBy ,CreatedName ,ModifiedOn ,ModifiedBy ,ModifiedName ) "
         + " VALUES ( "
         + "@EmployeeKey ,@EmployeeID ,@EmployeeName ,@CheckDate ,@Unit_Price,@DayOff ,@SalaryBasic ,@Day_Standard ,@Money ,@Description ,@Slug ,@RecordStatus ,GETDATE(),@CreatedBy ,@CreatedName ,GETDATE(),@ModifiedBy ,@ModifiedName ) ";
            zSQL += " SELECT 11 ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = _EmployeeKey.Trim();
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID.Trim();
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName.Trim();
                if (_CheckDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@CheckDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@CheckDate", SqlDbType.DateTime).Value = _CheckDate;
                zCommand.Parameters.Add("@Unit_Price", SqlDbType.Float).Value = _Unit_Price;
                zCommand.Parameters.Add("@DayOff", SqlDbType.Float).Value = _DayOff;
                zCommand.Parameters.Add("@SalaryBasic", SqlDbType.Money).Value = _SalaryBasic;
                zCommand.Parameters.Add("@Day_Standard", SqlDbType.Float).Value = _Day_Standard;
                zCommand.Parameters.Add("@Money", SqlDbType.Money).Value = _Money;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = _Slug;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();

                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE HRM_DayOff_Support SET "
                        + " EmployeeKey = @EmployeeKey,"
                        + " EmployeeID = @EmployeeID,"
                        + " EmployeeName = @EmployeeName,"
                        + " CheckDate = @CheckDate,"
                        + " Unit_Price = @Unit_Price,"
                        + " DayOff = @DayOff,"
                        + " SalaryBasic = @SalaryBasic,"
                        + " Day_Standard = @Day_Standard,"
                        + " Money = @Money,"
                        + " Description = @Description,"
                        + " Slug = @Slug,"
                        + " RecordStatus = 1,"
                        + " ModifiedOn = GETDATE(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                       + " WHERE AutoKey = @AutoKey ";
            zSQL += "SELECT 20";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = _EmployeeKey.Trim();
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID.Trim();
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName.Trim();
                if (_CheckDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@CheckDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@CheckDate", SqlDbType.DateTime).Value = _CheckDate;
                zCommand.Parameters.Add("@Unit_Price", SqlDbType.Float).Value = _Unit_Price;
                zCommand.Parameters.Add("@DayOff", SqlDbType.Float).Value = _DayOff;
                zCommand.Parameters.Add("@SalaryBasic", SqlDbType.Money).Value = _SalaryBasic;
                zCommand.Parameters.Add("@Day_Standard", SqlDbType.Float).Value = _Day_Standard;
                zCommand.Parameters.Add("@Money", SqlDbType.Money).Value = _Money;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = _Slug;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE HRM_DayOff_Support SET [RecordStatus] = 99, [ModifiedOn] = GETDATE(), ModifiedBy = @ModifiedBy, ModifiedName = @ModifiedName WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete(DateTime FromDate, DateTime ToDate)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE HRM_DayOff_Support SET [RecordStatus] = 99 WHERE CheckDate BETWEEN @FromDate AND @ToDate SELECT 30";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_AutoKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        #endregion
    }
}
