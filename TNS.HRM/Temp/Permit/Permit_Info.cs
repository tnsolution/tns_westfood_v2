﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
namespace TNS.HRM
{ 
    public class Permit_Info
    {


        #region [ Field Name ]
        private string _PermitKey = "";
        private string _PermitID = "";
        private DateTime _PermitDate;
        private string _Description = "";
        private DateTime _FromDate;
        private int _CategoryKey = 0;
        private int _CodeKey = 0;
        private int _Slug = 0;
        private DateTime _ToDate;
        private string _StartDay = "";
        private string _EndDay = "";
        private string _Color = "";
        private int _ShowPublic = 0;
        private float _NumberOff;
        private float _NumberLeft;
        private string _ProgressStep = "";
        private string _ProgressStatus = "";
        private int _RecordStatus = 0;
        private string _EmployeeKey = "";
        private string _Organization = "";
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _CreatedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private DateTime _ModifiedOn;
        private string _RoleID = "";
        private string _Message = "";
        #endregion
        #region [ Properties ]
        public bool IsCommandOK
        {
            get
            {
                int zNumber = 0;
                int.TryParse(_Message.Substring(0, 1), out zNumber);
                if (zNumber <= 3) return true; else return false;
            }
        }
        public bool HasInfo
        {
            get
            {
                if (_PermitKey.Trim().Length > 0) return true; else return false;
            }
        }
        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        public string Key
        {
            get { return _PermitKey; }
            set { _PermitKey = value; }
        }
        public string PermitID
        {
            get { return _PermitID; }
            set { _PermitID = value; }
        }
        public DateTime PermitDate
        {
            get { return _PermitDate; }
            set { _PermitDate = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public DateTime FromDate
        {
            get { return _FromDate; }
            set { _FromDate = value; }
        }
        public int CodeKey
        {
            get { return _CodeKey; }
            set { _CodeKey = value; }
        }
        public int Slug
        {
            get { return _Slug; }
            set { _Slug = value; }
        }
        public DateTime ToDate
        {
            get { return _ToDate; }
            set { _ToDate = value; }
        }
       
        public string EndDay
        {
            get { return _EndDay; }
            set { _EndDay = value; }
        }
        public string Color
        {
            get { return _Color; }
            set { _Color = value; }
        }
        public int ShowPublic
        {
            get { return _ShowPublic; }
            set { _ShowPublic = value; }
        }
        public float NumberOff
        {
            get { return _NumberOff; }
            set { _NumberOff = value; }
        }
        public float NumberLeft
        {
            get { return _NumberLeft; }
            set { _NumberLeft = value; }
        }
        public string ProgressStep
        {
            get { return _ProgressStep; }
            set { _ProgressStep = value; }
        }
        public string ProgressStatus
        {
            get { return _ProgressStatus; }
            set { _ProgressStatus = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
      
        public string EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
       
        public string Organization
        {
            get { return _Organization; }
            set { _Organization = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public int CategoryKey
        {
            get
            {
                return _CategoryKey;
            }

            set
            {
                _CategoryKey = value;
            }
        }

        public string StartDay
        {
            get
            {
                return _StartDay;
            }

            set
            {
                _StartDay = value;
            }
        }
        #endregion
        #region [ Constructor Get Information ]
        public Permit_Info()
        {
        }
        public Permit_Info(string PermitKey)
        {
            string zSQL = "SELECT * FROM HRM_Permit WHERE PermitKey = @PermitKey AND RecordStatus <> 99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (PermitKey.Length > 0)
                    zCommand.Parameters.Add("@PermitKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(PermitKey);
                else
                    zCommand.Parameters.Add("@PermitKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
             
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _PermitKey = zReader["PermitKey"].ToString();
                    _PermitID = zReader["PermitID"].ToString().Trim();
                    if (zReader["PermitDate"] != DBNull.Value)
                        _PermitDate = (DateTime)zReader["PermitDate"];
                    _Description = zReader["Description"].ToString().Trim();
                    if (zReader["FromDate"] != DBNull.Value)
                        _FromDate = (DateTime)zReader["FromDate"];
                    if (zReader["CategoryKey"] != DBNull.Value)
                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    if (zReader["CodeKey"] != DBNull.Value)
                        _CodeKey = int.Parse(zReader["CodeKey"].ToString());
                    if (zReader["Slug"] != DBNull.Value)
                        _Slug = int.Parse(zReader["Slug"].ToString());
                    if (zReader["ToDate"] != DBNull.Value)
                        _ToDate = (DateTime)zReader["ToDate"];
                    _StartDay = zReader["StartDay"].ToString().Trim();
                    _EndDay = zReader["EndDay"].ToString().Trim();
                    _Color = zReader["Color"].ToString().Trim();
                    if (zReader["ShowPublic"] != DBNull.Value)
                        _ShowPublic = int.Parse(zReader["ShowPublic"].ToString());
                    if (zReader["NumberOff"] != DBNull.Value)
                        _NumberOff = float.Parse(zReader["NumberOff"].ToString());
                    if (zReader["NumberLeft"] != DBNull.Value)
                        _NumberLeft = float.Parse(zReader["NumberLeft"].ToString());
                    _ProgressStep = zReader["ProgressStep"].ToString().Trim();
                    _ProgressStatus = zReader["ProgressStatus"].ToString().Trim();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    _EmployeeKey = zReader["EmployeeKey"].ToString();
                    _Organization = zReader["Organization"].ToString().Trim();
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message =  Err.ToString(); }
            finally { zConnect.Close(); }
        }

        public Permit_Info(string EmployeeKey,DateTime FromDate,DateTime ToDate)
        {
            string zSQL = @"SELECT * FROM [dbo].[HRM_Permit] 
WHERE EmployeeKey = @EmployeeKey AND FromDate = @FromDate AND ToDate = @ToDate AND RecordStatus <> 99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (EmployeeKey.Length == 36)
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(EmployeeKey);
                else
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                if (FromDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                if (ToDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;

                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _PermitKey = zReader["PermitKey"].ToString();
                    _PermitID = zReader["PermitID"].ToString().Trim();
                    if (zReader["PermitDate"] != DBNull.Value)
                        _PermitDate = (DateTime)zReader["PermitDate"];
                    _Description = zReader["Description"].ToString().Trim();
                    if (zReader["FromDate"] != DBNull.Value)
                        _FromDate = (DateTime)zReader["FromDate"];
                    if (zReader["CategoryKey"] != DBNull.Value)
                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    if (zReader["CodeKey"] != DBNull.Value)
                        _CodeKey = int.Parse(zReader["CodeKey"].ToString());
                    if (zReader["Slug"] != DBNull.Value)
                        _Slug = int.Parse(zReader["Slug"].ToString());
                    if (zReader["ToDate"] != DBNull.Value)
                        _ToDate = (DateTime)zReader["ToDate"];
                    _StartDay = zReader["StartDay"].ToString().Trim();
                    _EndDay = zReader["EndDay"].ToString().Trim();
                    _Color = zReader["Color"].ToString().Trim();
                    if (zReader["ShowPublic"] != DBNull.Value)
                        _ShowPublic = int.Parse(zReader["ShowPublic"].ToString());
                    if (zReader["NumberOff"] != DBNull.Value)
                        _NumberOff = float.Parse(zReader["NumberOff"].ToString());
                    if (zReader["NumberLeft"] != DBNull.Value)
                        _NumberLeft = float.Parse(zReader["NumberLeft"].ToString());
                    _ProgressStep = zReader["ProgressStep"].ToString().Trim();
                    _ProgressStatus = zReader["ProgressStatus"].ToString().Trim();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    _EmployeeKey = zReader["EmployeeKey"].ToString();
                    _Organization = zReader["Organization"].ToString().Trim();
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Permit ("
         + "PermitKey, PermitID ,PermitDate ,Description ,FromDate ,CategoryKey ,CodeKey ,Slug ,ToDate ,StartDay ,EndDay ,Color ,ShowPublic ,NumberOff ,NumberLeft ,ProgressStep ,ProgressStatus ,RecordStatus ,EmployeeKey ,Organization ,CreatedBy ,CreatedName ,CreatedOn ,ModifiedBy ,ModifiedName ,ModifiedOn ) "
          + " VALUES ( "
          + "NEWID(),@PermitID ,@PermitDate ,@Description ,@FromDate ,@CategoryKey ,@CodeKey ,@Slug ,@ToDate ,@StartDay ,@EndDay ,@Color ,@ShowPublic ,@NumberOff ,@NumberLeft ,@ProgressStep ,@ProgressStatus ,0 ,@EmployeeKey ,@Organization ,@CreatedBy ,@CreatedName ,GETDATE(),@ModifiedBy ,@ModifiedName ,GETDATE()) ";

            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@PermitID", SqlDbType.NVarChar).Value = _PermitID.Trim();
                if (_PermitDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@PermitDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@PermitDate", SqlDbType.DateTime).Value = _PermitDate;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                if (_FromDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = _FromDate;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@CodeKey", SqlDbType.Int).Value = _CodeKey;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = _Slug;
                if (_ToDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = _ToDate;
                zCommand.Parameters.Add("@StartDay", SqlDbType.NVarChar).Value = _StartDay.Trim();
                zCommand.Parameters.Add("@EndDay", SqlDbType.NVarChar).Value = _EndDay.Trim();
                zCommand.Parameters.Add("@Color", SqlDbType.NVarChar).Value = _Color.Trim();
                zCommand.Parameters.Add("@ShowPublic", SqlDbType.Int).Value = _ShowPublic;
                zCommand.Parameters.Add("@NumberOff", SqlDbType.Float).Value = _NumberOff;
                zCommand.Parameters.Add("@NumberLeft", SqlDbType.Float).Value = _NumberLeft;
                zCommand.Parameters.Add("@ProgressStep", SqlDbType.NVarChar).Value = _ProgressStep.Trim();
                zCommand.Parameters.Add("@ProgressStatus", SqlDbType.NVarChar).Value = _ProgressStatus.Trim();
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                if (_EmployeeKey.Length > 0)
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_EmployeeKey);
                else
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@Organization", SqlDbType.NVarChar).Value = _Organization.Trim();
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();

                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE HRM_Permit SET "
             + " PermitID = @PermitID,"
             + " PermitDate = @PermitDate,"
             + " Description = @Description,"
             + " FromDate = @FromDate,"
             + " CategoryKey = @CategoryKey,"
             + " CodeKey = @CodeKey,"
             + " Slug = @Slug,"
             + " ToDate = @ToDate,"
             + " StartDay = @StartDay,"
             + " EndDay = @EndDay,"
             + " Color = @Color,"
             + " ShowPublic = @ShowPublic,"
             + " NumberOff = @NumberOff,"
             + " NumberLeft = @NumberLeft,"
             + " ProgressStep = @ProgressStep,"
             + " ProgressStatus = @ProgressStatus,"
             + " RecordStatus = @RecordStatus,"
             + " EmployeeKey = @EmployeeKey,"
             + " Organization = @Organization,"
             + " CreatedBy = @CreatedBy,"
             + " CreatedName = @CreatedName,"
             + " CreatedOn = @CreatedOn,"
             + " ModifiedBy = @ModifiedBy,"
             + " ModifiedName = @ModifiedName,"
             + " ModifiedOn = @ModifiedOn"
            + " WHERE PermitKey = @PermitKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (_PermitKey.Length == 36)
                    zCommand.Parameters.Add("@PermitKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_PermitKey);
                else
                    zCommand.Parameters.Add("@PermitKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@PermitID", SqlDbType.NVarChar).Value = _PermitID.Trim();
                if (_PermitDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@PermitDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@PermitDate", SqlDbType.DateTime).Value = _PermitDate;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                if (_FromDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = _FromDate;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@CodeKey", SqlDbType.Int).Value = _CodeKey;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = _Slug;
                if (_ToDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = _ToDate;
                zCommand.Parameters.Add("@StartDay", SqlDbType.NVarChar).Value = _StartDay.Trim();
                zCommand.Parameters.Add("@EndDay", SqlDbType.NVarChar).Value = _EndDay.Trim();
                zCommand.Parameters.Add("@Color", SqlDbType.NVarChar).Value = _Color.Trim();
                zCommand.Parameters.Add("@ShowPublic", SqlDbType.Int).Value = _ShowPublic;
                zCommand.Parameters.Add("@NumberOff", SqlDbType.Float).Value = _NumberOff;
                zCommand.Parameters.Add("@NumberLeft", SqlDbType.Float).Value = _NumberLeft;
                zCommand.Parameters.Add("@ProgressStep", SqlDbType.NVarChar).Value = _ProgressStep.Trim();
                zCommand.Parameters.Add("@ProgressStatus", SqlDbType.NVarChar).Value = _ProgressStatus.Trim();
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                if (_EmployeeKey.Length > 0)
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_EmployeeKey);
                else
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@Organization", SqlDbType.NVarChar).Value = _Organization.Trim();
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"UPDATE dbo.HRM_Permit SET
      [RecordStatus] = 99,
      [ModifiedOn] = GETDATE(),
      [ModifiedBy] = @ModifiedBy,
      [ModifiedName] = @ModifiedName
      WHERE[PermitKey] = @PermitKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@PermitKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_PermitKey);
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_PermitKey.Trim().Length == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        #endregion
    }
}
