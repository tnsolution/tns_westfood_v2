﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
namespace TNS.HRM
{
    public class Score_Info
    {

        #region [ Field Name ]
        private int _AutoKey = 0;
        private DateTime _DateWrite;
        private string _EmployeeKey = "";
        private string _EmployeeID = "";
        private string _EmployeeName = "";
        private int _TeamKey = 0;
        private int _DepartmentKey = 0;
        private float _NCQD;
        private float _NCTT;
        private float _BeginScore;
        private float _EndScore;
        private float _BorrowScore;
        private float _CancelScore;
        private int _RecordStatus = 0;
        private DateTime _CreatedOn;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _RoleID = "";
        private string _Message = "";
        #endregion
        #region [ Properties ]
        public bool IsCommandOK
        {
            get
            {
                int zNumber = 0;
                int.TryParse(_Message.Substring(0, 1), out zNumber);
                if (zNumber <= 3) return true; else return false;
            }
        }
        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        public int Key
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public DateTime DateWrite
        {
            get { return _DateWrite; }
            set { _DateWrite = value; }
        }
        public string EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public string EmployeeID
        {
            get { return _EmployeeID; }
            set { _EmployeeID = value; }
        }
        public string EmployeeName
        {
            get { return _EmployeeName; }
            set { _EmployeeName = value; }
        }
        public int TeamKey
        {
            get { return _TeamKey; }
            set { _TeamKey = value; }
        }
        public int DepartmentKey
        {
            get { return _DepartmentKey; }
            set { _DepartmentKey = value; }
        }
        public float NCQD
        {
            get { return _NCQD; }
            set { _NCQD = value; }
        }
        public float NCTT
        {
            get { return _NCTT; }
            set { _NCTT = value; }
        }
        public float BeginScore
        {
            get { return _BeginScore; }
            set { _BeginScore = value; }
        }
        public float EndScore
        {
            get { return _EndScore; }
            set { _EndScore = value; }
        }
        public float BorrowScore
        {
            get { return _BorrowScore; }
            set { _BorrowScore = value; }
        }
        public float CancelScore
        {
            get { return _CancelScore; }
            set { _CancelScore = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion
        #region [ Constructor Get Information ]
        public Score_Info()
        {
        }
        public Score_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM HRM_Score WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    if (zReader["DateWrite"] != DBNull.Value)
                        _DateWrite = (DateTime)zReader["DateWrite"];
                    _EmployeeKey = zReader["EmployeeKey"].ToString().Trim();
                    _EmployeeID = zReader["EmployeeID"].ToString().Trim();
                    _EmployeeName = zReader["EmployeeName"].ToString().Trim();
                    if (zReader["TeamKey"] != DBNull.Value)
                        _TeamKey = int.Parse(zReader["TeamKey"].ToString());
                    if (zReader["DepartmentKey"] != DBNull.Value)
                        _DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    if (zReader["NCQD"] != DBNull.Value)
                        _NCQD = float.Parse(zReader["NCQD"].ToString());
                    if (zReader["NCTT"] != DBNull.Value)
                        _NCTT = float.Parse(zReader["NCTT"].ToString());
                    if (zReader["BeginScore"] != DBNull.Value)
                        _BeginScore = float.Parse(zReader["BeginScore"].ToString());
                    if (zReader["EndScore"] != DBNull.Value)
                        _EndScore = float.Parse(zReader["EndScore"].ToString());
                    if (zReader["BorrowScore"] != DBNull.Value)
                        _BorrowScore = float.Parse(zReader["BorrowScore"].ToString());
                    if (zReader["CancelScore"] != DBNull.Value)
                        _CancelScore = float.Parse(zReader["CancelScore"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }

        public Score_Info(string EmployeeKey, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, DateWrite.Day, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            string zSQL = "SELECT * FROM HRM_Score WHERE EmployeeKey = @EmployeeKey AND DateWrite BETWEEN @FromDate AND @ToDate AND RecordStatus <>99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey.Trim();
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    if (zReader["DateWrite"] != DBNull.Value)
                        _DateWrite = (DateTime)zReader["DateWrite"];
                    _EmployeeKey = zReader["EmployeeKey"].ToString().Trim();
                    _EmployeeID = zReader["EmployeeID"].ToString().Trim();
                    _EmployeeName = zReader["EmployeeName"].ToString().Trim();
                    if (zReader["TeamKey"] != DBNull.Value)
                        _TeamKey = int.Parse(zReader["TeamKey"].ToString());
                    if (zReader["DepartmentKey"] != DBNull.Value)
                        _DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    if (zReader["NCQD"] != DBNull.Value)
                        _NCQD = float.Parse(zReader["NCQD"].ToString());
                    if (zReader["NCTT"] != DBNull.Value)
                        _NCTT = float.Parse(zReader["NCTT"].ToString());
                    if (zReader["BeginScore"] != DBNull.Value)
                        _BeginScore = float.Parse(zReader["BeginScore"].ToString());
                    if (zReader["EndScore"] != DBNull.Value)
                        _EndScore = float.Parse(zReader["EndScore"].ToString());
                    if (zReader["BorrowScore"] != DBNull.Value)
                        _BorrowScore = float.Parse(zReader["BorrowScore"].ToString());
                    if (zReader["CancelScore"] != DBNull.Value)
                        _CancelScore = float.Parse(zReader["CancelScore"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Score ("
        + " DateWrite ,EmployeeKey ,EmployeeID ,EmployeeName ,TeamKey ,DepartmentKey ,NCQD ,NCTT ,BeginScore ,EndScore ,BorrowScore ,CancelScore ,RecordStatus ,CreatedOn ,CreatedBy ,CreatedName ,ModifiedOn ,ModifiedBy ,ModifiedName ) "
         + " VALUES ( "
         + "@DateWrite ,@EmployeeKey ,@EmployeeID ,@EmployeeName ,@TeamKey ,@DepartmentKey ,@NCQD ,@NCTT ,@BeginScore ,@EndScore ,@BorrowScore ,@CancelScore ,@RecordStatus ,GETDATE(),@CreatedBy ,@CreatedName ,GETDATE(),@ModifiedBy ,@ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (_DateWrite == DateTime.MinValue)
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = _DateWrite;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = _EmployeeKey.Trim();
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID.Trim();
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName.Trim();
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = _TeamKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@NCQD", SqlDbType.Float).Value = _NCQD;
                zCommand.Parameters.Add("@NCTT", SqlDbType.Float).Value = _NCTT;
                zCommand.Parameters.Add("@BeginScore", SqlDbType.Float).Value = _BeginScore;
                zCommand.Parameters.Add("@EndScore", SqlDbType.Float).Value = _EndScore;
                zCommand.Parameters.Add("@BorrowScore", SqlDbType.Float).Value = _BorrowScore;
                zCommand.Parameters.Add("@CancelScore", SqlDbType.Float).Value = _CancelScore;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE HRM_Score SET "
                        + " DateWrite = @DateWrite,"
                        + " EmployeeKey = @EmployeeKey,"
                        + " EmployeeID = @EmployeeID,"
                        + " EmployeeName = @EmployeeName,"
                        + " TeamKey = @TeamKey,"
                        + " DepartmentKey = @DepartmentKey,"
                        + " NCQD = @NCQD,"
                        + " NCTT = @NCTT,"
                        + " BeginScore = @BeginScore,"
                        + " EndScore = @EndScore,"
                        + " BorrowScore = @BorrowScore,"
                        + " CancelScore = @CancelScore,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = GETDATE(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                       + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                if (_DateWrite == DateTime.MinValue)
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = _DateWrite;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = _EmployeeKey.Trim();
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID.Trim();
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName.Trim();
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = _TeamKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@NCQD", SqlDbType.Float).Value = _NCQD;
                zCommand.Parameters.Add("@NCTT", SqlDbType.Float).Value = _NCTT;
                zCommand.Parameters.Add("@BeginScore", SqlDbType.Float).Value = _BeginScore;
                zCommand.Parameters.Add("@EndScore", SqlDbType.Float).Value = _EndScore;
                zCommand.Parameters.Add("@BorrowScore", SqlDbType.Float).Value = _BorrowScore;
                zCommand.Parameters.Add("@CancelScore", SqlDbType.Float).Value = _CancelScore;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE HRM_Score SET [RecordStatus] = 99, [ModifiedOn] = GETDATE(), ModifiedBy = @ModifiedBy, ModifiedName = @ModifiedName WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_AutoKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        #endregion
    }
}
