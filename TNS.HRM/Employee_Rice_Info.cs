﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
namespace TNS.HRM
{
    public class Employee_Rice_Info
    {

        #region [ Field Name ]
        private int _AutoKey = 0;
        private DateTime _DateWrite;
        private int _KeepingKey = 0;
        private int _TeamKey = 0;
        private string _TeamName = "";
        private string _EmployeeKey = "";
        private string _EmployeeName = "";
        private string _EmployeeID = "";
        private int _RiceKey = 0;
        private string _RiceID = "";
        private string _RiceName = "";
        private int _Number = 0;
        private double _Price = 0;
        private double _Money = 0;
        private string _Description = "";
        private int _RecordStatus = 0;
        private DateTime _CreatedOn;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _RoleID = "";
        private string _Message = "";
        private int _Slug = 0;
        #endregion
        #region [ Properties ]

        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        public int Key
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public DateTime DateWrite
        {
            get { return _DateWrite; }
            set { _DateWrite = value; }
        }
        public int TeamKey
        {
            get { return _TeamKey; }
            set { _TeamKey = value; }
        }
        public string TeamName
        {
            get { return _TeamName; }
            set { _TeamName = value; }
        }
        public string EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public string EmployeeName
        {
            get { return _EmployeeName; }
            set { _EmployeeName = value; }
        }
        public string EmployeeID
        {
            get { return _EmployeeID; }
            set { _EmployeeID = value; }
        }
        public int RiceKey
        {
            get { return _RiceKey; }
            set { _RiceKey = value; }
        }
        public string RiceName
        {
            get { return _RiceName; }
            set { _RiceName = value; }
        }
        public double Money
        {
            get { return _Money; }
            set { _Money = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public int KeepingKey
        {
            get
            {
                return _KeepingKey;
            }

            set
            {
                _KeepingKey = value;
            }
        }

        public int Slug
        {
            get
            {
                return _Slug;
            }

            set
            {
                _Slug = value;
            }
        }

        public string RiceID
        {
            get
            {
                return _RiceID;
            }

            set
            {
                _RiceID = value;
            }
        }

        public int Number
        {
            get
            {
                return _Number;
            }

            set
            {
                _Number = value;
            }
        }

        public double Price
        {
            get
            {
                return _Price;
            }

            set
            {
                _Price = value;
            }
        }
        #endregion
        #region [ Constructor Get Information ]
        public Employee_Rice_Info()
        {
        }
        public Employee_Rice_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM HRM_Employee_Rice WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    if (zReader["KeepingKey"] != DBNull.Value)
                        _KeepingKey = int.Parse(zReader["KeepingKey"].ToString());
                    if (zReader["DateWrite"] != DBNull.Value)
                        _DateWrite = (DateTime)zReader["DateWrite"];
                    if (zReader["TeamKey"] != DBNull.Value)
                        _TeamKey = int.Parse(zReader["TeamKey"].ToString());
                    _TeamName = zReader["TeamName"].ToString().Trim();
                    _EmployeeKey = zReader["EmployeeKey"].ToString();
                    _EmployeeName = zReader["EmployeeName"].ToString().Trim();
                    _EmployeeID = zReader["EmployeeID"].ToString().Trim();
                    if (zReader["RiceKey"] != DBNull.Value)
                        _RiceKey = int.Parse(zReader["RiceKey"].ToString());
                    _RiceID = zReader["RiceID"].ToString().Trim();
                    _RiceName = zReader["RiceName"].ToString().Trim();
                    if (zReader["Number"] != DBNull.Value)
                        _Number = int.Parse(zReader["Number"].ToString());
                    if (zReader["Price"] != DBNull.Value)
                        _Price = double.Parse(zReader["Price"].ToString());
                    if (zReader["Money"] != DBNull.Value)
                        _Money = double.Parse(zReader["Money"].ToString());
                    _Description = zReader["Description"].ToString().Trim();
                    if (zReader["Slug"] != DBNull.Value)
                        _Slug = int.Parse(zReader["Slug"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Employee_Rice ("
        + " DateWrite ,TeamKey ,TeamName ,EmployeeKey ,EmployeeName ,EmployeeID ,KeepingKey,Slug,RiceKey,RiceID ,RiceName ,Number,Price, Money ,Description ,RecordStatus ,CreatedOn ,CreatedBy ,CreatedName ,ModifiedOn ,ModifiedBy ,ModifiedName ) "
         + " VALUES ( "
         + "@DateWrite ,@TeamKey ,@TeamName ,@EmployeeKey ,@EmployeeName ,@EmployeeID ,@KeepingKey,@Slug,@RiceKey,@RiceID ,@RiceName ,@Number, @Price ,@Money ,@Description ,@RecordStatus ,GETDATE(),@CreatedBy ,@CreatedName ,GETDATE(),@ModifiedBy ,@ModifiedName ) ";
            zSQL += " SELECT '11' +CAST(AutoKey AS Nvarchar(50)) FROM HRM_Employee_Rice WHERE AutoKey = SCOPE_IDENTITY() ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (_DateWrite == DateTime.MinValue)
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = _DateWrite;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = _TeamKey;
                zCommand.Parameters.Add("@TeamName", SqlDbType.NVarChar).Value = _TeamName.Trim();
                if (_EmployeeKey.Length > 0)
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_EmployeeKey);
                else
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName.Trim();
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID.Trim();
                zCommand.Parameters.Add("@RiceKey", SqlDbType.Int).Value = _RiceKey;
                zCommand.Parameters.Add("@KeepingKey", SqlDbType.Int).Value = _KeepingKey;
                zCommand.Parameters.Add("@RiceID", SqlDbType.NVarChar).Value = _RiceID.Trim();
                zCommand.Parameters.Add("@RiceName", SqlDbType.NVarChar).Value = _RiceName.Trim();
                zCommand.Parameters.Add("@Number", SqlDbType.Int).Value = _Number;
                zCommand.Parameters.Add("@Price", SqlDbType.Money).Value = _Price;
                zCommand.Parameters.Add("@Money", SqlDbType.Money).Value = _Money;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = _Slug;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteScalar().ToString();
                if (zResult.Substring(0, 2) == "11")
                {
                    _Message = zResult.Substring(0, 2);
                    _AutoKey = int.Parse(zResult.Substring(2));
                }

                else
                    _AutoKey = 0;
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE HRM_Employee_Rice SET "
                        + " DateWrite = @DateWrite,"
                        + " TeamKey = @TeamKey,"
                        + " KeepingKey = @KeepingKey,"
                        + " TeamName = @TeamName,"
                        + " EmployeeKey = @EmployeeKey,"
                        + " EmployeeName = @EmployeeName,"
                        + " EmployeeID = @EmployeeID,"
                        + " RiceKey = @RiceKey,"
                        + " RiceID = @RiceID,"
                        + " RiceName = @RiceName,"
                        + " Number = @Number,"
                        + " Price = @Price,"
                        + " Money = @Money,"
                        + " Description = @Description,"
                        + " RecordStatus = @RecordStatus,"
                        + " Slug = @Slug,"
                        + " ModifiedOn = GETDATE(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                       + " WHERE AutoKey = @AutoKey SELECT 20";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                if (_DateWrite == DateTime.MinValue)
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = _DateWrite;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = _TeamKey;
                zCommand.Parameters.Add("@TeamName", SqlDbType.NVarChar).Value = _TeamName.Trim();
                if (_EmployeeKey.Length == 36)
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_EmployeeKey);
                else
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName.Trim();
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID.Trim();
                zCommand.Parameters.Add("@RiceKey", SqlDbType.Int).Value = _RiceKey;
                zCommand.Parameters.Add("@RiceID", SqlDbType.NVarChar).Value = _RiceID.Trim();
                zCommand.Parameters.Add("@RiceName", SqlDbType.NVarChar).Value = _RiceName.Trim();
                zCommand.Parameters.Add("@KeepingKey", SqlDbType.Int).Value = _KeepingKey;
                zCommand.Parameters.Add("@Number", SqlDbType.Int).Value = _Number;
                zCommand.Parameters.Add("@Price", SqlDbType.Money).Value = _Price;
                zCommand.Parameters.Add("@Money", SqlDbType.Money).Value = _Money;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = _Slug;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE HRM_Employee_Rice SET [RecordStatus] = 99, [ModifiedOn] = GETDATE(), ModifiedBy = @ModifiedBy, ModifiedName = @ModifiedName WHERE AutoKey = @AutoKey SELECT 30";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete(string EmployeeID,DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, DateWrite.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(DateWrite.Year, DateWrite.Month, DateWrite.Day, 23, 0, 0);
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"DELETE FROM HRM_Employee_Rice 
                            WHERE EmployeeID = @EmployeeID AND DateWrite BETWEEN @FromDate AND @ToDate SELECT 30";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = EmployeeID;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_AutoKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        #endregion
    }
}
