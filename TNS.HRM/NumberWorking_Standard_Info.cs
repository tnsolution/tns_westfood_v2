﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
namespace TNS.HRM
{
    public class NumberWorking_Standard_Info
    {

        #region [ Field Name ]
        private int _AutoKey = 0;
        private DateTime _DateWrite;
        private string _EmployeeKey = "";
        private string _EmployeeID = "";
        private string _EmployeeName = "";
        private int _DepartmentKey = 0;
        private int _TeamKey = 0;
        private string _TeamID = "";
        private string _TeamName = "";
        private float _Number =0; //Số ngày công quy định
        private float _PercentLHQ = 0;//Tỉ lệ lương hiệu quả trong tháng
        private int _RecordStatus = 0;
        private DateTime _CreatedOn;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _RoleID = "";
        private string _Message = "";
        #endregion
        #region [ Properties ]
        public bool IsCommandOK
        {
            get
            {
                int zNumber = 0;
                int.TryParse(_Message.Substring(0, 1), out zNumber);
                if (zNumber <= 3) return true; else return false;
            }
        }

        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        public int Key
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public DateTime DateWrite
        {
            get { return _DateWrite; }
            set { _DateWrite = value; }
        }
        public string EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public string EmployeeID
        {
            get { return _EmployeeID; }
            set { _EmployeeID = value; }
        }
        public string EmployeeName
        {
            get { return _EmployeeName; }
            set { _EmployeeName = value; }
        }
        public int DepartmentKey
        {
            get { return _DepartmentKey; }
            set { _DepartmentKey = value; }
        }
        public int TeamKey
        {
            get { return _TeamKey; }
            set { _TeamKey = value; }
        }
        public string TeamID
        {
            get { return _TeamID; }
            set { _TeamID = value; }
        }
        public string TeamName
        {
            get { return _TeamName; }
            set { _TeamName = value; }
        }
        public float Number
        {
            get { return _Number; }
            set { _Number = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public float PercentLHQ
        {
            get
            {
                return _PercentLHQ;
            }

            set
            {
                _PercentLHQ = value;
            }
        }
        #endregion
        #region [ Constructor Get Information ]
        public NumberWorking_Standard_Info()
        {
        }
        public NumberWorking_Standard_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM HRM_NumberWorking_Standard WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    if (zReader["DateWrite"] != DBNull.Value)
                        _DateWrite = (DateTime)zReader["DateWrite"];
                    _EmployeeKey = zReader["EmployeeKey"].ToString();
                    _EmployeeID = zReader["EmployeeID"].ToString().Trim();
                    _EmployeeName = zReader["EmployeeName"].ToString().Trim();
                    if (zReader["DepartmentKey"] != DBNull.Value)
                        _DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    if (zReader["TeamKey"] != DBNull.Value)
                        _TeamKey = int.Parse(zReader["TeamKey"].ToString());
                    _TeamID = zReader["TeamID"].ToString().Trim();
                    _TeamName = zReader["TeamName"].ToString().Trim();
                    if (zReader["Number"] != DBNull.Value)
                        _Number = float.Parse(zReader["Number"].ToString());
                    if (zReader["PercentLHQ"] != DBNull.Value)
                        _PercentLHQ = float.Parse(zReader["PercentLHQ"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        public NumberWorking_Standard_Info(string EmployeeID, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, DateWrite.Day, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            string zSQL = @"SELECT * FROM HRM_NumberWorking_Standard WHERE EmployeeID = @EmployeeID 
 AND DateWrite BETWEEN @FromDate AND @ToDate AND RecordStatus <> 99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = EmployeeID;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    if (zReader["DateWrite"] != DBNull.Value)
                        _DateWrite = (DateTime)zReader["DateWrite"];
                    _EmployeeKey = zReader["EmployeeKey"].ToString();
                    _EmployeeID = zReader["EmployeeID"].ToString().Trim();
                    _EmployeeName = zReader["EmployeeName"].ToString().Trim();
                    if (zReader["DepartmentKey"] != DBNull.Value)
                        _DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    if (zReader["TeamKey"] != DBNull.Value)
                        _TeamKey = int.Parse(zReader["TeamKey"].ToString());
                    _TeamID = zReader["TeamID"].ToString().Trim();
                    _TeamName = zReader["TeamName"].ToString().Trim();
                    if (zReader["Number"] != DBNull.Value)
                        _Number = float.Parse(zReader["Number"].ToString());
                    if (zReader["PercentLHQ"] != DBNull.Value)
                        _PercentLHQ = float.Parse(zReader["PercentLHQ"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_NumberWorking_Standard ("
        + " DateWrite ,EmployeeKey ,EmployeeID ,EmployeeName ,DepartmentKey ,TeamKey ,TeamID ,TeamName ,Number,PercentLHQ ,RecordStatus ,CreatedOn ,CreatedBy ,CreatedName ,ModifiedOn ,ModifiedBy ,ModifiedName ) "
         + " VALUES ( "
         + "@DateWrite ,@EmployeeKey ,@EmployeeID ,@EmployeeName ,@DepartmentKey ,@TeamKey ,@TeamID ,@TeamName ,@Number ,@PercentLHQ ,0 ,GETDATE(),@CreatedBy ,@CreatedName ,GETDATE(),@ModifiedBy ,@ModifiedName ) ";
            zSQL += " SELECT '11' + CAST(AutoKey AS NVARCHAR(50)) FROM HRM_NumberWorking_Standard WHERE AutoKey = SCOPE_IDENTITY() ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (_DateWrite == DateTime.MinValue)
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = _DateWrite;
                if (_EmployeeKey.Length > 0)
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_EmployeeKey);
                else
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID.Trim();
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName.Trim();
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = _TeamKey;
                zCommand.Parameters.Add("@TeamID", SqlDbType.NVarChar).Value = _TeamID.Trim();
                zCommand.Parameters.Add("@TeamName", SqlDbType.NVarChar).Value = _TeamName.Trim();
                zCommand.Parameters.Add("@Number", SqlDbType.Float).Value = _Number; 
                zCommand.Parameters.Add("@PercentLHQ", SqlDbType.Float).Value = _PercentLHQ;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteScalar().ToString();
                if (zResult.Substring(0, 2) == "11")
                {
                    _Message = zResult.Substring(0, 2);
                    _AutoKey = int.Parse(zResult.Substring(2));
                }

                else
                    _AutoKey = 0;
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE HRM_NumberWorking_Standard SET "
                        + " DateWrite = @DateWrite,"
                        + " EmployeeKey = @EmployeeKey,"
                        + " EmployeeID = @EmployeeID,"
                        + " EmployeeName = @EmployeeName,"
                        + " DepartmentKey = @DepartmentKey,"
                        + " TeamKey = @TeamKey,"
                        + " TeamID = @TeamID,"
                        + " TeamName = @TeamName,"
                        + " Number = @Number,"
                        + " PercentLHQ = @PercentLHQ,"
                        + " RecordStatus = 1,"
                        + " ModifiedOn = GETDATE(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                       + " WHERE AutoKey = @AutoKey SELECT 20";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                if (_DateWrite == DateTime.MinValue)
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = _DateWrite;
                if (_EmployeeKey.Length == 36)
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_EmployeeKey);
                else
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID.Trim();
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName.Trim();
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = _TeamKey;
                zCommand.Parameters.Add("@TeamID", SqlDbType.NVarChar).Value = _TeamID.Trim();
                zCommand.Parameters.Add("@TeamName", SqlDbType.NVarChar).Value = _TeamName.Trim();
                zCommand.Parameters.Add("@Number", SqlDbType.Float).Value = _Number;
                zCommand.Parameters.Add("@PercentLHQ", SqlDbType.Float).Value = _PercentLHQ;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE HRM_NumberWorking_Standard SET [RecordStatus] = 99, [ModifiedOn] = GETDATE(), ModifiedBy = @ModifiedBy, ModifiedName = @ModifiedName WHERE AutoKey = @AutoKey SELECT 30";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_AutoKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        #endregion
    }
}
