﻿using System;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
namespace TNS.HRM
{
    public class Fee_Category_Data
    {
        public static DataTable List()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM FNC_Fee_Category WHERE RecordStatus != 99 ORDER BY [RANK]";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable Search(int Slug, string Name)
        {
            DataTable zTable = new DataTable();

            string zFilter = "";
            if (Slug > 0)
            {
                if (Slug == 1)
                    zFilter += " AND Slug = 1 ";
                if (Slug == 2)
                    zFilter += " AND Slug = 2 ";
            }


            string zSQL = "SELECT CategoryKey,CategoryID,CategoryName,Rank,Slug,Description FROM FNC_Fee_Category WHERE (CategoryName LIKE @Name OR CategoryID LIKE @Name) AND   RecordStatus <> 99 @Paramater ORDER BY Slug ASC, [Rank] ASC";

            zSQL = zSQL.Replace("@Paramater",zFilter);

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable Fee_Combobox_Payment()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT CategoryKey,CategoryName FROM FNC_Fee_Category WHERE Slug = 2 AND   RecordStatus <> 99 ORDER BY [Rank] ASC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable Fee_Combobox_Receipt()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT CategoryKey,CategoryName FROM FNC_Fee_Category WHERE Slug = 1 AND   RecordStatus <> 99 ORDER BY [Rank] ASC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static int CountID(string ID)
        {
            int zResult = 0;
            string zSQL = "SELECT COUNT(*) FROM FNC_Fee_Category WHERE CategoryID = @ID AND   RecordStatus <> 99 ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ID", SqlDbType.NVarChar).Value = ID.Trim();
                zResult = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
        public static int CountPaymentCategoryID(string ID)
        {
            int zResult = 0;
            string zSQL = "SELECT COUNT(*) FROM FNC_Fee_Category WHERE CategoryID = @ID AND Slug=2 AND   RecordStatus <> 99 ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ID", SqlDbType.NVarChar).Value = ID.Trim();
                zResult = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
        public static int CountReceiptCategoryID(string ID)
        {
            int zResult = 0;
            string zSQL = "SELECT COUNT(*) FROM FNC_Fee_Category WHERE CategoryID = @ID AND Slug=1 AND   RecordStatus <> 99 ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ID", SqlDbType.NVarChar).Value = ID.Trim();
                zResult = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
    }
}
