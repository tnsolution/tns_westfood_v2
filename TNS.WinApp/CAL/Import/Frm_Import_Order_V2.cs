﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using TNS.CORE;
using TNS.HRM;
using TNS.IVT;
using TNS.Misc;
using TNS.SYS;
using TN.Toolbox;
using System.Text;
using TNS.LOG;

namespace TNS.WinApp
{
    /*
     * 1 chọn tập tin
     * 2 tạo đối tượng đơn hàng
     * 3 hiển thị lưới thông tin đơn hàng
     * 4 cập nhật sửa 1 đơn hàng
     * 5 cập nhật tất cả đơn hàng
     */

    public partial class Frm_Import_Order_V2 : Form
    {
        bool _IsSaved = false;//kiem tra đã lưu hay chưa
        FileInfo FileInfo;
        DataTable _TableOrder;
        DataTable _TableEmployee;
        DataTable _TableRate;

        int zError = 0;
        int zSuccess = 0;
        int _IndexGV = 0;
        int _TotalGV = 0;

        public Frm_Import_Order_V2()
        {
            InitializeComponent();
            btn_Seach_Work.Click += Btn_Seach_Work_Click;
            btn_Search_CT.Click += Btn_Search_CT_Click;
            btnClose_Panel_Message.Click += btnClose_Panel_Message_Click;

            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            txt_QuantityReality.KeyPress += ControlNumber_KeyPress;
            txt_QuantityLose.KeyPress += ControlNumber_KeyPress;
            txt_QuantityDocument.KeyPress += ControlNumber_KeyPress;
            txt_Percent.KeyPress += ControlNumber_KeyPress;

            txt_QuantityReality.Leave += Txt_QuantityReality_Leave;
            txt_QuantityLose.Leave += Txt_QuantityLose_Leave;
            txt_QuantityDocument.Leave += Txt_QuantityDocument_Leave;

            txt_QuantityReality.Leave += (o, e) => { CalculatorQuantity(); };
            txt_QuantityLose.Leave += (o, e) => { CalculatorQuantity(); };
            txt_QuantityDocument.Leave += (o, e) => { CalculatorQuantity(); };
        }

        

        private void Frm_Import_Order_V2_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);

            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            this.DoubleBuffered = true;
            Utils.DoubleBuffered(GVOrder, true);
            Utils.DoubleBuffered(GVRate, true);

            Utils.DrawGVStyle(ref GVRate);
            Utils.DrawGVStyle(ref GVOrder);

            btn_Show.Visible = true;
            btn_Hide.Visible = false;
            btn_SaveOrder.Enabled = false;

            Panel_Employee.Visible = false;
            Panel_Info.Visible = false;

            splitContainer1.SplitterDistance = Screen.PrimaryScreen.Bounds.Width;
            splitContainer1.Panel1MinSize = 320;
            splitContainer1.Panel2MinSize = 0;

            SetupLayoutGVEmployee();
            SetupLayoutGVOrder();
            SetupLayoutGVRate();
            //Lấy tất cả nhóm trực tiếp sx  và khác bộ phận hỗ trợ sx
            string zSQL = @"SELECT TeamKey,TeamID +'-'+ TeamName AS TeamName  FROM SYS_Team 
                            WHERE RecordStatus <> 99 AND BranchKey=4 AND  DepartmentKey != 26
                            ORDER BY Rank";
            LoadDataToToolbox.ComboBoxData(cbo_Team.ComboBox, zSQL, false);
            dte_OrderDate.Value = SessionUser.Date_Work;
        }
        private void Txt_QuantityDocument_Leave(object sender, EventArgs e)
        {
            double zAmount = 0;
            if (double.TryParse(txt_QuantityDocument.Text.Trim(), out zAmount))
            {
                txt_QuantityDocument.Text = zAmount.Toe1String();
                //txt_QuantityDocument.Focus();
                txt_QuantityDocument.SelectionStart = txt_QuantityDocument.Text.Length;
            }
            else
            {
                Utils.TNMessageBoxOK("Lỗi!.Định dạng số.",1);
            }
        }

        private void Txt_QuantityLose_Leave(object sender, EventArgs e)
        {
            double zAmount = 0;
            if (double.TryParse(txt_QuantityLose.Text.Trim(), out zAmount))
            {
                txt_QuantityLose.Text = zAmount.Toe1String();
                //txt_QuantityLose.Focus();
                txt_QuantityLose.SelectionStart = txt_QuantityLose.Text.Length;
            }
            else
            {
                Utils.TNMessageBoxOK("Lỗi!.Định dạng số.",1);
            }
        }

        private void Txt_QuantityReality_Leave(object sender, EventArgs e)
        {
            double zAmount = 0;
            if (double.TryParse(txt_QuantityReality.Text.Trim(), out zAmount))
            {
                txt_QuantityReality.Text = zAmount.Toe1String();
                //txt_QuantityReality.Focus();
                txt_QuantityReality.SelectionStart = txt_QuantityReality.Text.Length - 2;
            }
            else
            {
                Utils.TNMessageBoxOK("Lỗi!.Định dạng số.",1);
            }
        }
        private void SetupLayoutGVOrder()
        {
            // Setup Column 
            GVOrder.Columns.Add("No", "STT");
            GVOrder.Columns.Add("OrderID_Compare", "Mã Đơn Hàng đối chiếu");
            GVOrder.Columns.Add("OrderID", "Mã Đơn Hàng");
            GVOrder.Columns.Add("OrderDate", "Ngày Đơn Hàng");
            GVOrder.Columns.Add("TeamID", "Mã Nhóm");
            GVOrder.Columns.Add("TeamName", "Tên Nhóm");
            GVOrder.Columns.Add("ProductID", "Mã Sản Phẩm");
            GVOrder.Columns.Add("ProductName", "Tên Sản Phẩm");
            GVOrder.Columns.Add("StageID", "Mã Công Đoạn");
            GVOrder.Columns.Add("StageName", "Tên Công Đoạn");
            GVOrder.Columns.Add("PriceStage", "Đơn Giá");
            GVOrder.Columns.Add("OrderIDFollow", "Số Chứng Từ");
            GVOrder.Columns.Add("QuantityDocument", "SL Chứng Từ");
            GVOrder.Columns.Add("QuantityReality", "SL Thực Tế");
            GVOrder.Columns.Add("QuantityLose", "SL Hao Hụt");
            GVOrder.Columns.Add("Unit", "Đơn Vị");
            GVOrder.Columns.Add("WorkStatus", "Tình Trạng");
            GVOrder.Columns.Add("Percent", "Tỷ lệ HT");
            GVOrder.Columns.Add("Note", "Ghi Chú");
            GVOrder.Columns.Add("Message", "Thông Báo");

            GVOrder.Columns["No"].Width = 40;
            GVOrder.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GVOrder.Columns["Message"].Width = 120;
            GVOrder.Columns["Message"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GVOrder.Columns["Message"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GVOrder.Columns["OrderID_Compare"].Width = 130;
            GVOrder.Columns["OrderID_Compare"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GVOrder.Columns["OrderID"].Width = 130;
            GVOrder.Columns["OrderID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GVOrder.Columns["OrderDate"].Width = 120;
            GVOrder.Columns["OrderDate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GVOrder.Columns["TeamID"].Width = 120;
            GVOrder.Columns["TeamID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GVOrder.Columns["TeamID"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GVOrder.Columns["TeamName"].Width = 250;
            GVOrder.Columns["TeamName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GVOrder.Columns["TeamName"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GVOrder.Columns["ProductID"].Width = 120;
            GVOrder.Columns["ProductID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GVOrder.Columns["ProductName"].Width = 200;
            GVOrder.Columns["ProductName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GVOrder.Columns["StageID"].Width = 120;
            GVOrder.Columns["StageID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GVOrder.Columns["StageID"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GVOrder.Columns["StageName"].Width = 200;
            GVOrder.Columns["StageName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GVOrder.Columns["StageName"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GVOrder.Columns["PriceStage"].Width = 80;
            GVOrder.Columns["PriceStage"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GVOrder.Columns["PriceStage"].ReadOnly = true;

            GVOrder.Columns["OrderIDFollow"].Width = 120;
            GVOrder.Columns["OrderIDFollow"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GVOrder.Columns["QuantityDocument"].Width = 120;
            GVOrder.Columns["QuantityDocument"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GVOrder.Columns["QuantityReality"].Width = 120;
            GVOrder.Columns["QuantityReality"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GVOrder.Columns["QuantityLose"].Width = 120;
            GVOrder.Columns["QuantityLose"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GVOrder.Columns["Unit"].Width = 90;
            GVOrder.Columns["Unit"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GVOrder.Columns["WorkStatus"].Width = 90;
            GVOrder.Columns["WorkStatus"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GVOrder.Columns["Percent"].Width = 90;
            GVOrder.Columns["Percent"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GVOrder.Columns["Note"].Width = 200;
            GVOrder.Columns["Note"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GVOrder.Columns["Message"].Width = 300;
            GVOrder.Columns["Message"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GVOrder.SelectionChanged += GVOrder_SelectionChanged;
        }
        private void SetupLayoutGVRate()
        {
            GVRate.Columns.Add("RateID", "Mã");
            GVRate.Columns.Add("RateName", "Hệ số");
            GVRate.Columns.Add("Rate", "Giá trị");

            GVRate.Columns["RateID"].Width = 90;
            GVRate.Columns["RateID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GVRate.Columns["RateName"].Width = 250;
            GVRate.Columns["RateName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GVRate.Columns["RateName"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GVRate.Columns["RateName"].ReadOnly = true;

            GVRate.Columns["Rate"].Width = 50;
            GVRate.Columns["Rate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GVRate.Columns["Rate"].ReadOnly = true;

            GVRate.EditingControlShowing += GVRate_EditingControlShowing;
            GVRate.CellEndEdit += GVRate_CellEndEdit;
            GVRate.KeyDown += GVRate_KeyDown;
        }
        private void SetupLayoutGVEmployee()
        {
            DataTable zTable = Employee_Data.Load_Data("EmployeeKey, EmployeeID, FullName");
            string[] zHeaderLV = new string[] { "Mã NV", "Tên" };

            GVEmployee.ColumnAdd("No", "STT", 40);

            GV_Column zCol = new GV_Column("EmployeeID", "ListView", 100, 400);
            zCol.TableSource = zTable;
            zCol.FilterColumn = true;
            zCol.CheckEmpty = true;
            zCol.ShowColumn = 1;
            zCol.DisplayNextColumn = "FullName";
            zCol.HeaderText = "Mã NV";
            zCol.InitColumnLV(zHeaderLV);
            zCol.LoadDataToLV();
            GVEmployee.ColumnAdd(zCol);

            GVEmployee.ColumnAdd("FullName", "Họ tên", 200);

            zCol = new GV_Column();
            zCol.IsSum = true;
            zCol.CheckEmpty = true;
            zCol.ColumnName = "Basket";
            zCol.HeaderText = "Số Rổ";
            zCol.Style = "Number";
            GVEmployee.ColumnAdd(zCol);

            zCol = new GV_Column();
            zCol.IsSum = true;
            zCol.CheckEmpty = true;
            zCol.ColumnName = "Kg";
            zCol.HeaderText = "Số Lượng";
            zCol.Style = "Number";
            GVEmployee.ColumnAdd(zCol);

            zCol = new GV_Column();
            zCol.IsSum = true;
            zCol.CheckEmpty = true;
            zCol.ColumnName = "Time";
            zCol.HeaderText = "Thời Gian";
            zCol.Style = "Number";
            GVEmployee.ColumnAdd(zCol);

            zCol = new GV_Column();
            zCol.ColumnName = "Borrow";
            zCol.HeaderText = "Mượn Người";
            zCol.Style = "CheckBox";
            GVEmployee.ColumnAdd(zCol);

            zCol = new GV_Column();
            zCol.ColumnName = "Private";
            zCol.HeaderText = "Ăn Riêng";
            zCol.Style = "CheckBox";
            GVEmployee.ColumnAdd(zCol);

            zCol = new GV_Column();
            zCol.ColumnName = "General";
            zCol.HeaderText = "Ăn Chung";
            zCol.Style = "CheckBox";
            GVEmployee.ColumnAdd(zCol);

            GVEmployee.ColumnAdd("Message", "Thông báo");

            GVEmployee.FillLastColumn = true;
            GVEmployee.IsSum = true;
            GVEmployee.AllowAddRow = false;
            GVEmployee.Init();
            GVEmployee.GetSum(false);

            GVEmployee.RowValidated += GVEmployee_RowValidated;
        }

        void ProcessData()
        {
            _IndexGV = 0;
            _TotalGV = 0;
            zSuccess = 0;
            zError = 0;
            ShowCount();
           //-----------------Read Excel
           ExcelPackage package = new ExcelPackage(FileInfo);
            _TableOrder = Data_Access.GetTable(FileInfo.FullName, package.Workbook.Worksheets[1].Name);
            _TableEmployee = Data_Access.GetTable(FileInfo.FullName, package.Workbook.Worksheets[2].Name);
            _TableRate = Data_Access.GetTable(FileInfo.FullName, package.Workbook.Worksheets[3].Name);
            package.Dispose();
            //-----------------Read Data Excel
            ReadData();
        }

        void InsertData()
        {
            zError = 0;
            zSuccess = 0;
            ShowCount();
            _ListOrder = Order_Exe.SaveList(_ListOrder);
            if(_ListOrder.Count>0)
            {
                zError = _ListOrder.Count;
                zSuccess = _TotalGV - zError;
                ShowCount();
            }    
        }

        #region [1-Create List Object Order]
        void ReadData()
        {
            _TotalGV = _TableOrder.Rows.Count;
            ShowCount();
            for (int i = 0; i < _TableOrder.Rows.Count; i++)
            {
                DataRow zRow = _TableOrder.Rows[i];
                CreateOrder(zRow);
            }
        }
        List<Order_Object> _ListOrder = new List<Order_Object>();
        void CreateOrder(DataRow RowExcel)
        {
            string Key = RowExcel[0].ToString().Trim();
            string IDCompare = RowExcel[1].ToString().Trim();
            string ID = RowExcel[2].ToString().Trim();

            //DateTime OrderDate = DateTime.Parse(RowExcel[3].ToString().Trim());
            DateTime OrderDate = new DateTime();
            if (!DateTime.TryParse(RowExcel[3].ToString().Trim(), out OrderDate))
            {
                OrderDate = DateTime.MinValue;
            }
            

            Order_Object zOrder = new Order_Object();
            zOrder.OrderID = ID;
            zOrder.OrderID_Compare = IDCompare;
            zOrder.OrderDate = OrderDate;

            string zTeamID = RowExcel[4].ToString().Trim();
            Team_Info zTeam = new Team_Info();
            zTeam.Get_Team_ID(zTeamID);
            zOrder.TeamKey = zTeam.Key;

            string zProductID = RowExcel[6].ToString().Trim();
            if (zProductID.Length > 0)
            {
                Product_Info zProduct = new Product_Info();
                zProduct.Get_Product_Info_ID(zProductID);
                zOrder.ProductKey = zProduct.ProductKey;
                zOrder.ProductID = zProduct.ProductID;
                zOrder.ProductName = zProduct.ProductName;
            }

            string zStageID = RowExcel[8].ToString().Trim();
            Product_Stages_Info zStage = new Product_Stages_Info();
            zStage.GetStagesID_Info(zStageID);
            if (zStage.Key > 0)
            {
                zOrder.Category_Stage = zStage.Key;
                zOrder.GroupKey = zStage.GroupKey;
                zOrder.GroupID = zStage.GroupID;
            }
            zOrder.OrderIDFollow = RowExcel[11].ToString().Trim();

            float Quantity = 0;
            float.TryParse(RowExcel[12].ToString().Trim(), out Quantity);
            zOrder.QuantityDocument = Quantity;
            float.TryParse(RowExcel[13].ToString().Trim(), out Quantity);
            zOrder.QuantityReality = Quantity;
            float.TryParse(RowExcel[14].ToString().Trim(), out Quantity);
            zOrder.QuantityLoss = Quantity;
            string UnitName = RowExcel[15].ToString().Trim();
            Product_Unit_Info zUnit = new Product_Unit_Info(UnitName);
            zOrder.UnitKey = zUnit.Key;
            zOrder.UnitName = zUnit.Unitname;
            if (RowExcel[16].ToString().Trim().ToUpper() == "ĐÃ THỰC HIỆN")
            {
                zOrder.WorkStatus = 1;
            }
            else
            {
                zOrder.WorkStatus = 0;
            }
            float.TryParse(RowExcel[17].ToString().Trim(), out Quantity);
            zOrder.PercentFinish = Quantity;
            zOrder.Note = RowExcel[19].ToString().Trim();

            //Check Record
            zOrder.Message += CheckOrder(RowExcel);

            string zSelectEmployee = "[" + _TableEmployee.Columns[9].ColumnName + "]='" + RowExcel[2].ToString().Trim() + "'";
            string zSelectRate = "[" + _TableRate.Columns[3].ColumnName + "]='" + RowExcel[2].ToString().Trim() + "'";
            DataRow[] zEmployee = _TableEmployee.Select(zSelectEmployee);
            DataRow[] zRate = _TableRate.Select(zSelectRate);

            if (zRate.Length > 0)
            {
                List<Order_Rate_Info> zRateList = new List<Order_Rate_Info>();
                foreach (DataRow r in zRate)
                {
                    string Message = CheckRate(r);
                    zOrder.Message += Message;

                    Order_Rate_Info zOrderRate = new Order_Rate_Info();
                    string zRateID = r[2].ToString().Trim();
                    Category_Info zCategory_Info = new Category_Info();
                    zCategory_Info.GetID(zRateID);
                    zOrderRate.OrderDate = OrderDate;
                    zOrderRate.CategoryKey = zCategory_Info.AutoKey;
                    zOrderRate.RateID = zCategory_Info.RateID;
                    zOrderRate.RateName = zCategory_Info.RateName;
                    zOrderRate.Rate = zCategory_Info.Rate;
                    zOrderRate.RecordStatus = 1;
                    zOrderRate.CreatedName = SessionUser.UserLogin.EmployeeName;
                    zOrderRate.CreatedBy = SessionUser.UserLogin.Key;
                    zOrderRate.ModifiedName = SessionUser.UserLogin.EmployeeName;
                    zOrderRate.ModifiedBy = SessionUser.UserLogin.Key;
                    zRateList.Add(zOrderRate);
                }
                zOrder.List_Rate = zRateList;
            }
            if (zEmployee.Length > 0)
            {
                List<Order_Employee_Info> zEmployeeList = new List<Order_Employee_Info>();
                foreach (DataRow r in zEmployee)
                {
                    string Message = CheckEmployee(r);
                    zOrder.Message += Message;

                    Order_Employee_Info zOrderEmployee = new Order_Employee_Info();
                    zOrderEmployee.OrderDate = OrderDate;
                    string zEmployeeID = r[1].ToString().Trim();
                    Employee_Info zEmployeeInfo = new Employee_Info();
                    zEmployeeInfo.GetEmployeeID(zEmployeeID);
                    zOrderEmployee.EmployeeID = zEmployeeID;
                    zOrderEmployee.EmployeeKey = zEmployeeInfo.Key;
                    zOrderEmployee.FullName = zEmployeeInfo.FullName;
                    float zBasket = 0;
                    float.TryParse(r[3].ToString().Trim(), out zBasket);
                    zOrderEmployee.Basket = zBasket;
                    float zKilogram = 0;
                    float.TryParse(r[4].ToString().Trim(), out zKilogram);
                    zOrderEmployee.Kilogram = zKilogram;
                    float zTime = 0;
                    float.TryParse(r[5].ToString().Trim(), out zTime);
                    zOrderEmployee.Time = zTime;

                    //mươn người
                    if (r[6].ToString().Trim() == "1")
                    {
                        zOrderEmployee.Borrow = 1;
                    }
                    else
                    {
                        zOrderEmployee.Borrow = 0;
                    }
                    string a = r[6].ToString().Trim();
                    //an chung
                    if (r[8].ToString().Trim() == "1")
                    {
                        zOrderEmployee.Share = 1;
                    }
                    else
                    {
                        zOrderEmployee.Share = 0;
                    }
                    //an rieng
                    if (r[7].ToString().Trim() == "1")
                    {
                        zOrderEmployee.Private = 1;
                    }
                    else
                    {
                        zOrderEmployee.Private = 0;
                    }

                    zOrderEmployee.RecordStatus = 1;

                    zOrderEmployee.CreatedName = SessionUser.UserLogin.EmployeeName;
                    zOrderEmployee.CreatedBy = SessionUser.UserLogin.Key;
                    zOrderEmployee.ModifiedName = SessionUser.UserLogin.EmployeeName;
                    zOrderEmployee.ModifiedBy = SessionUser.UserLogin.Key;
                    zEmployeeList.Add(zOrderEmployee);
                }
                zOrder.List_Employee = zEmployeeList;
            }

            zOrder.CreatedName = SessionUser.UserLogin.EmployeeName;
            zOrder.CreatedBy = SessionUser.UserLogin.Key;
            zOrder.ModifiedName = SessionUser.UserLogin.EmployeeName;
            zOrder.ModifiedBy = SessionUser.UserLogin.Key;

            _ListOrder.Add(zOrder);
            if(zOrder.Message!= string.Empty)
            {
                zError++; 
                ShowCount();
            }    
            
        }

        #region [Check Value Record Excel]
        string CheckOrder(DataRow RowExcel)
        {
            bool isError = false;
            string zMessage = "Vui lòng kiểm tra: " + Environment.NewLine;

            //---kiểm tra mã đơn hàng tham chiếu gốc-----------------------------------
            //kiểm tra order id gốc có giá trị
            string zOrderID = RowExcel[1].ToString();
            if (zOrderID.Trim() == "")
            {
                zMessage += "Mã đơn hàng tham chiếu bị rỗng" + Environment.NewLine;
                isError = true;
            }
            else
            {
                //kiểm tra order key gốc đã tồn tại chưa
                int zOrderKey = Order_Data.FindOrderID_Compare(zOrderID);
                if (zOrderKey != 0)
                {
                    zMessage += "Mã đơn hàng tham chiếu [" + zOrderID + "] đã tồn tại" + Environment.NewLine;
                    isError = true;
                }
            }

            //---kiểm tra mã đơn hàng của pm----------------------------------------------
            //kiểm tra order id có giá trị
            string zOrderIDPM = RowExcel[2].ToString();
            if (zOrderIDPM.Trim() == "")
            {
                zMessage += "Mã đơn hàng bị rỗng" + Environment.NewLine;
                isError = true;
            }
            else
            {
                //kiểm tra order key đã tồn tại chưa
                int zOrderKeyPM = Order_Data.FindOrderID(zOrderID);
                if (zOrderKeyPM != 0)
                {
                    zMessage += "Mã đơn hàng [" + zOrderID + "] đã tồn tại" + Environment.NewLine;
                    isError = true;
                }
            }

            DateTime OrderDate = new DateTime();
            if (!DateTime.TryParse(RowExcel[3].ToString().Trim(), out OrderDate))
            {
                zMessage += "Ngày tháng không đúng" + Environment.NewLine;
                isError = true;
            }
            else
            {
                if (SessionUser.Date_Lock >= OrderDate)
                {
                    zMessage += "Thời gian này đã khóa sổ.!" + Environment.NewLine;
                    isError = true;
                }
            }
            //--- kiểm tra nhóm đã có cài đặt chưa -------------------------------------------------------
            string zTeamID = RowExcel[4].ToString();
            Team_Info zTeam = new Team_Info();
            zTeam.Get_Team_ID(zTeamID);
            if (zTeamID == "")
            {
                zMessage += "Mã tổ nhóm bị rỗng" + Environment.NewLine;
                isError = true;
            }
            else
            {
                if (zTeam.Key == 0)
                {
                    zMessage += "Tổ nhóm: [" + zTeamID + "] không tồn tại" + Environment.NewLine;
                    isError = true;
                }
            }

            //--- kiểm tra product đã có cài đặt chưa -----------------------------------------------------
            string zProductID = RowExcel[6].ToString().Trim();
            if (zProductID == "")
            {
                zMessage += "Mã sản phẩm bị rỗng" + Environment.NewLine;
                isError = true;
            }
            else
            {
                Product_Info zProduct = new Product_Info();
                zProduct.Get_Product_Info_ID(zProductID);
                if (zProduct.ProductKey == "")
                {
                    zMessage += "Sản phẩm: [" + zProductID + "] không tồn tại" + Environment.NewLine;
                    isError = true;
                }
            }

            //--- kiểm tra công đoạn đã có cài đặt chưa ---------------------------------------------------
            string zStageID = RowExcel[8].ToString().Trim();
            Product_Stages_Info zStage = new Product_Stages_Info();
            zStage.GetStagesID_Info(zStageID);
            if (zStageID == "")
            {
                zMessage += "Mã công việc bị rỗng" + Environment.NewLine;
                isError = true;
            }
            else
            {
                if (zStage.Key == 0)
                {
                    zMessage += "Công việc: [" + zStageID + "] không tồn tại" + Environment.NewLine;
                    isError = true;
                }
            }

            if (isError)
                return zMessage;
            else
                return string.Empty;
        }
        string CheckEmployee(DataRow RowExcel)
        {
            bool isError = false;
            string zMessage = "Vui lòng kiểm tra: " + Environment.NewLine;

            //---kiểm tra mã thẻ nhân viên----------------------------------------------
            string zEmployeeID = RowExcel[1].ToString();
            if (zEmployeeID.Trim() == "")
            {
                zMessage += "Mã thẻ công nhân bị rỗng" + Environment.NewLine;
                isError = true;
            }
            else
            {
                Employee_Info zEmployee = new Employee_Info();
                zEmployee.GetEmployeeID(zEmployeeID);

                if (zEmployee.Key == "")
                {
                    zMessage += "Số thẻ:" + zEmployeeID + " không tồn tại" + Environment.NewLine;
                    isError = true;
                }
                else
                {
                    if (zEmployee.TeamKey == 0)
                    {
                        zMessage += "Số thẻ:" + zEmployeeID + " chưa chọn tổ nhóm" + Environment.NewLine;
                        isError = true;
                    }
                }
            }

            if (RowExcel[6].ToString() != "")
            {
                if (RowExcel[7].ToString() == "" &&
                    RowExcel[8].ToString() == "")
                {
                    zMessage += "Khi mượn người phải chọn ăn chung hay ăn riêng" + Environment.NewLine;
                    isError = true;
                }

                if (RowExcel[7].ToString() != "" &&
                    RowExcel[8].ToString() != "")
                {
                    zMessage += "Khi mượn người không được vừa ăn chung hay ăn riêng" + Environment.NewLine;
                    isError = true;
                }
            }

            if (isError)
                return zMessage;
            else
                return string.Empty;
        }
        string CheckRate(DataRow RowExcel)
        {
            bool isError = false;
            string zMessage = "Vui lòng kiểm tra: " + Environment.NewLine;

            //---kiểm tra hệ số đơn hàng của pm----------------------------------------------
            string zRateID = RowExcel[2].ToString();
            if (zRateID.Trim() == "")
            {
                zMessage += "Mã hệ số bị rỗng" + Environment.NewLine;
                isError = true;
            }
            else
            {
                Category_Info zInfo = new Category_Info();
                zInfo.GetID(zRateID);
                if (zInfo.AutoKey == 0)
                {
                    zMessage += "Mã hế số: [" + zRateID + "] không tồn tại" + Environment.NewLine;
                    isError = true;
                }
            }
            if (isError)
                return zMessage;
            else
                return string.Empty;
        }
        #endregion
        #endregion

        #region [2-Thread Show Order to GV]
        void ShowOrder(Order_Object zOrder)
        {
            GVOrder.Rows.Add();
            GVOrder.Rows[_IndexGVOrder].Tag = _ListOrder[_IndexGVOrder];
            GVOrder.Rows[_IndexGVOrder].Cells["No"].Value = (_IndexGVOrder + 1).ToString();
            GVOrder.Rows[_IndexGVOrder].Cells["OrderID_Compare"].Value = zOrder.OrderID_Compare;
            GVOrder.Rows[_IndexGVOrder].Cells["OrderID"].Value = zOrder.OrderID;
            DateTime zOrderDate = DateTime.Parse(zOrder.OrderDate.ToString());
            GVOrder.Rows[_IndexGVOrder].Cells["OrderDate"].Value = zOrderDate.ToString("dd/MM/yyyy");

            Team_Info zTeam = new Team_Info(zOrder.TeamKey);
            GVOrder.Rows[_IndexGVOrder].Cells["TeamID"].Value = zTeam.TeamID;
            GVOrder.Rows[_IndexGVOrder].Cells["TeamName"].Value = zTeam.TeamName;

            GVOrder.Rows[_IndexGVOrder].Cells["ProductID"].Value = zOrder.ProductID;
            GVOrder.Rows[_IndexGVOrder].Cells["ProductName"].Value = zOrder.ProductName;

            Product_Stages_Info zStage = new Product_Stages_Info(zOrder.Category_Stage);
            GVOrder.Rows[_IndexGVOrder].Cells["StageID"].Value = zStage.StagesID;
            GVOrder.Rows[_IndexGVOrder].Cells["StageName"].Value = zStage.StageName;
            GVOrder.Rows[_IndexGVOrder].Cells["PriceStage"].Value = zStage.Price;

            GVOrder.Rows[_IndexGVOrder].Cells["OrderIDFollow"].Value = zOrder.OrderIDFollow;

            GVOrder.Rows[_IndexGVOrder].Cells["QuantityDocument"].Value = zOrder.QuantityDocument;

            GVOrder.Rows[_IndexGVOrder].Cells["QuantityReality"].Value = zOrder.QuantityReality;
            GVOrder.Rows[_IndexGVOrder].Cells["QuantityLose"].Value = zOrder.QuantityLoss;

            GVOrder.Rows[_IndexGVOrder].Cells["Unit"].Value = zOrder.UnitName;
            if (zOrder.WorkStatus == 1)
                GVOrder.Rows[_IndexGVOrder].Cells["WorkStatus"].Value = "ĐÃ THỰC HIỆN";
            else
                GVOrder.Rows[_IndexGVOrder].Cells["WorkStatus"].Value = "";

            GVOrder.Rows[_IndexGVOrder].Cells["Percent"].Value = zOrder.PercentFinish;

            GVOrder.Rows[_IndexGVOrder].Cells["Note"].Value = zOrder.Note;
            GVOrder.Rows[_IndexGVOrder].Cells["Message"].Value = zOrder.Message;

            if (zOrder.Message != string.Empty)
            {
                GVOrder.Rows[_IndexGVOrder].DefaultCellStyle.ForeColor = Color.Maroon;
            }
            _IndexGVOrder++;
        }

        private Thread _ThreadExcelOrder;
        private int _IndexGVOrder = 0;

        delegate void SetItemExcelOrderCallback(Order_Object zObj);

        void ShowListOrderToGV()
        {
            for (int i = 0; i < _ListOrder.Count; i++)
            {
                ShowObjectOrderToGV(_ListOrder[i]);
            }
            _ThreadExcelOrder.Abort();

        }
        void ShowObjectOrderToGV(Order_Object zObj)
        {
            if (GVOrder.InvokeRequired)
            {
                SetItemExcelOrderCallback d = new SetItemExcelOrderCallback(ShowObjectOrderToGV);
                this.Invoke(d, new object[] { zObj });
            }
            else
            {
                PBar.Value++;
                ShowOrder(zObj);
            }
        }
        #endregion

        void LoadOrderInfo(Order_Object zOrder)
        {
            cbo_Status.SelectedValue = zOrder.WorkStatus;
            dte_OrderDate.Value = zOrder.OrderDate;
            txt_OrderIDCompare.Text = zOrder.OrderID_Compare;
            txt_OrderID.Text = zOrder.OrderID;
            Team_Info zTeam = new Team_Info(zOrder.TeamKey);
            cbo_Team.SelectedValue = zTeam.Key;
            txt_ProductID.Tag = zOrder.ProductKey;
            txt_ProductID.Text = zOrder.ProductID;
            txt_ProductName.Text = zOrder.ProductName;
            Product_Stages_Info zStage = new Product_Stages_Info(zOrder.Category_Stage);
            txt_WorkID.Tag = zOrder.Category_Stage;
            txt_WorkID.Text = zStage.StagesID; ;
            txt_WorkName.Text = zStage.StageName;
            txt_Price.Text = zStage.Price.Ton1String();
            txt_Unit.Tag = zOrder.UnitKey;
            txt_Unit.Text = zOrder.UnitName;
            txt_Note.Text = zOrder.Note;
            txt_OrderIDFollow.Text = zOrder.OrderIDFollow;
            txt_QuantityDocument.Text = zOrder.QuantityDocument.Ton1String();
            txt_QuantityReality.Text = zOrder.QuantityReality.Ton1String();
            txt_QuantityLose.Text = zOrder.QuantityLoss.Ton1String();
            txt_Percent.Text = zOrder.PercentFinish.ToString();

            if (zOrder.Message != string.Empty)
            {
                Panel_Message.Visible = true;
                lbl_Message.Text = zOrder.Message + Environment.NewLine;
            }
            else
            {
                Panel_Message.Visible = false;
                lbl_Message.Text = "";
            }

            if (zOrder.List_Rate.Count > 0)
            {
                for (int i = 0; i < zOrder.List_Rate.Count; i++)
                {
                    Order_Rate_Info zRate = zOrder.List_Rate[i];
                    LoadGVRate(zRate, i);

                    if (zRate.Message != string.Empty)
                    {
                        if (!Panel_Message.Visible)
                            Panel_Message.Visible = true;
                        lbl_Message.Text += zRate.Message + Environment.NewLine;
                    }
                }
            }
            if (zOrder.List_Employee.Count > 0)
            {
                for (int i = 0; i < zOrder.List_Employee.Count; i++)
                {
                    Order_Employee_Info zEmployee = zOrder.List_Employee[i];
                    LoadGVEmployee(zEmployee, i);

                    if (zEmployee.Message != string.Empty)
                    {
                        if (!Panel_Message.Visible)
                            Panel_Message.Visible = true;
                        lbl_Message.Text += zEmployee.Message + Environment.NewLine;
                    }
                }

                GVEmployee.GetSum(false);
            }


        }
        void LoadGVRate(Order_Rate_Info zOrderRate, int index)
        {
            GVRate.Rows.Add();
            GVRate.Rows[index].Tag = zOrderRate;
            GVRate.Rows[index].Cells["RateID"].Tag = zOrderRate.CategoryKey;
            Category_Info zCategory_Info = new Category_Info(zOrderRate.CategoryKey);
            GVRate.Rows[index].Cells["RateID"].Value = zCategory_Info.RateID;
            GVRate.Rows[index].Cells["RateName"].Value = zOrderRate.RateName; ;
            GVRate.Rows[index].Cells["Rate"].Value = zOrderRate.Rate;
        }
        void LoadGVEmployee(Order_Employee_Info zOrderEmployee, int index)
        {
            GVEmployee.Rows.Add();
            GVEmployee.Rows[index].Tag = zOrderEmployee;
            GVEmployee.Rows[index].Cells["No"].Value = (index + 1).ToString();
            GVEmployee.Rows[index].Cells["EmployeeID"].Tag = zOrderEmployee.EmployeeKey;
            GVEmployee.Rows[index].Cells["EmployeeID"].Value = zOrderEmployee.EmployeeID;
            GVEmployee.Rows[index].Cells["FullName"].Value = zOrderEmployee.FullName;
            GVEmployee.Rows[index].Cells["Kg"].Value = zOrderEmployee.Kilogram.ToString();
            GVEmployee.Rows[index].Cells["Basket"].Value = zOrderEmployee.Basket.ToString();
            GVEmployee.Rows[index].Cells["Time"].Value = zOrderEmployee.Time.ToString();

            if (zOrderEmployee.Borrow.ToString() == "1")
            {
                GVEmployee.Rows[index].Cells["Borrow"].Value = true;
            }
            if (zOrderEmployee.Private.ToString() == "1")
            {
                GVEmployee.Rows[index].Cells["Private"].Value = true;
            }
            if (zOrderEmployee.Share.ToString() == "1")
            {
                GVEmployee.Rows[index].Cells["General"].Value = true;
            }
            if (zOrderEmployee.Message != string.Empty)
            {
                GVEmployee.Rows[index].Cells["Message"].Value = zOrderEmployee.Message;
            }
        }

        //chưa sd
        void ShowCount()
        {
            Invoke(new MethodInvoker(delegate
            {
                txt_Amount_Excel.Text = _TotalGV.ToString("n0");
                txt_Error.Text = zError.ToString("n0");
                txt_Sql.Text = zSuccess.ToString("n0");
            }));
        }
        //Hàm không thấy sử dụng
        string CreateInserCommand()
        {
            StringBuilder zSb = new StringBuilder();
            foreach (Order_Object zOrder in _ListOrder)
            {
                if (zOrder.Message == string.Empty)
                {
                    zSb.AppendLine("-- BEGIN ORDER OBJECT --");

                    #region [Order SQL]
                    zSb.AppendLine(@" 
INSERT INTO FTR_Order
(
OrderKey, OrderID_Compare, OrderID, OrderDate, TeamKey, ProductKey, ProductID, ProductName, QuantityDocument, QuantityReality, QuantityLoss, UnitKey, UnitName, 
Category_Stage, OrderIDFollow, PercentFinish, RecordStatus, WorkStatus, Note, Rank, CreatedOn, CreatedBy, CreatedName, ModifiedOn, ModifiedBy, ModifiedName
)");
                    zSb.AppendLine("VALUES");
                    zSb.AppendLine(@"
(
@OrderKey, @OrderID_Compare, @OrderID, @OrderDate, @TeamKey, @ProductKey, @ProductID, @ProductName, @QuantityDocument, @QuantityReality, @QuantityLoss, @UnitKey, @UnitName,
@Category_Stage, @OrderIDFollow, @PercentFinish, 0, @WorkStatus, @Note, @Rank, GETDATE(), @CreatedBy, @CreatedName, GETDATE(), @ModifiedBy, @ModifiedName 
)");

                    #region [Paramater]
                    zSb.Replace("@OrderKey", "'" + zOrder.Key + "'");
                    zSb.Replace("@OrderID_Compare", "'" + zOrder.OrderID_Compare + "'");
                    zSb.Replace("@OrderID", "'" + zOrder.OrderID + "'");
                    zSb.Replace("@OrderDate", "'" + zOrder.OrderDate.ToString("yyyy-MM-dd 00:00:01") + "'");
                    zSb.Replace("@TeamKey", "'" + zOrder.TeamKey.ToString() + "'");
                    zSb.Replace("@ProductKey", "'" + zOrder.ProductKey + "'");
                    zSb.Replace("@ProductID", "'" + zOrder.ProductID + "'");
                    zSb.Replace("@ProductName", "'" + zOrder.ProductName + "'");
                    zSb.Replace("@QuantityDocument", "'" + zOrder.QuantityDocument.ToString() + "'");
                    zSb.Replace("@QuantityReality", "'" + zOrder.QuantityReality.ToString() + "'");
                    zSb.Replace("@QuantityLoss", "'" + zOrder.QuantityLoss.ToString() + "'");
                    zSb.Replace("@UnitKey", "'" + zOrder.UnitKey.ToString() + "'");
                    zSb.Replace("@UnitName", "'" + zOrder.UnitName + "'");
                    zSb.Replace("@Category_Stage", "'" + zOrder.Category_Stage.ToString() + "'");
                    zSb.Replace("@OrderIDFollow", "'" + zOrder.OrderIDFollow + "'");
                    zSb.Replace("@PercentFinish", "'" + zOrder.PercentFinish.ToString() + "'");
                    zSb.Replace("@WorkStatus", "'" + zOrder.WorkStatus.ToString() + "'");
                    zSb.Replace("@Note", "'" + zOrder.Note + "'");
                    zSb.Replace("@Rank", "'" + 0 + "'");
                    zSb.Replace("@CreatedBy", "'" + zOrder.CreatedBy + "'");
                    zSb.Replace("@CreatedName", "'" + zOrder.CreatedName + "'");
                    zSb.Replace("@ModifiedBy", "'" + zOrder.ModifiedBy + "'");
                    zSb.Replace("@ModifiedName", "'" + zOrder.ModifiedName + "'");
                    #endregion

                    #endregion

                    zSb.AppendLine("-- BEGIN RATE --");
                    foreach (Order_Rate_Info zRate in zOrder.List_Rate)
                    {
                        zSb.AppendLine(@"
INSERT INTO FTR_Order_Rate
(
OrderKey, OrderDate, CategoryKey,  RecordStatus, CreatedOn, CreatedBy, CreatedName,  ModifiedOn,  ModifiedBy, ModifiedName
)");
                        zSb.AppendLine("VALUES");
                        zSb.AppendLine(@"
(
@OrderKey, @OrderDate, @CategoryKey, 0, GETDATE(), @CreatedBy, @CreatedName, GETDATE(), @ModifiedBy, @ModifiedName
)");

                        #region [Paramater]
                        zSb.Replace("@OrderKey", "'" + zRate.Key + "'");
                        zSb.Replace("@OrderDate", "'" + zRate.OrderDate.ToString("yyyy-MM-dd 00:00:01") + "'");
                        zSb.Replace("@CategoryKey", "'" + zRate.CategoryKey + "'");
                        zSb.Replace("@CreatedBy", "'" + zRate.CreatedBy + "'");
                        zSb.Replace("@CreatedName", "'" + zRate.CreatedName + "'");
                        zSb.Replace("@ModifiedBy", "'" + zRate.ModifiedBy + "'");
                        zSb.Replace("@ModifiedName", "'" + zRate.ModifiedName + "'");
                        #endregion
                    }

                    zSb.AppendLine("-- BEGIN EMPLOYEE --");
                    foreach (Order_Employee_Info zEmployee in zOrder.List_Employee)
                    {
                        zSb.AppendLine(@"
 INSERT INTO FTR_Order_Employee
(
EmployeeKey, EmployeeID, OrderKey, OrderDate, Basket, Kilogram, Time, Borrow, Share, Private, RecordStatus, CreatedOn, CreatedBy, CreatedName, ModifiedOn, ModifiedBy, ModifiedName
)");
                        zSb.AppendLine("VALUES");
                        zSb.AppendLine(@"
(
@EmployeeKey ,@EmployeeID, @OrderKey, @OrderDate, @Basket, @Kilogram, @Time, @Borrow, @Share, @Private, 0, GETDATE(), @CreatedBy, @CreatedName, GETDATE(), @ModifiedBy, @ModifiedName
)");

                        #region [Paramater]     
                        zSb.Replace("@EmployeeKey", "'" + zEmployee.EmployeeKey + "'");
                        zSb.Replace("@EmployeeID", "'" + zEmployee.EmployeeID + "'");
                        zSb.Replace("@OrderKey", "'" + zEmployee.OrderKey + "'");
                        zSb.Replace("@OrderDate", "'" + zEmployee.OrderDate.ToString("yyyy-MM-dd 00:00:01") + "'");
                        zSb.Replace("@Basket", "'" + zEmployee.Basket + "'");
                        zSb.Replace("@Kilogram", "'" + zEmployee.Kilogram + "'");
                        zSb.Replace("@Time", "'" + zEmployee.Time + "'");
                        zSb.Replace("@Borrow", "'" + zEmployee.Borrow + "'");
                        zSb.Replace("@Share", "'" + zEmployee.Share + "'");
                        zSb.Replace("@Private", "'" + zEmployee.Private + "'");
                        zSb.Replace("@CreatedBy", "'" + zEmployee.CreatedBy + "'");
                        zSb.Replace("@CreatedName", "'" + zEmployee.CreatedName + "'");
                        zSb.Replace("@ModifiedBy", "'" + zEmployee.ModifiedBy + "'");
                        zSb.Replace("@ModifiedName", "'" + zEmployee.ModifiedName + "'");
                        #endregion

                        zSb.AppendLine("-- END ORDER OBJECT --");

                        //
                    }

                    return zSb.ToString();
                }
            }

            return zSb.ToString();
        }
        #region[Event]

        private void GVEmployee_RowValidated(object sender, DataGridViewCellEventArgs e)
        {
            if (GVEmployee.Rows[e.RowIndex].Cells["EmployeeID"].Value != null)
            {
                Order_Employee_Info zOrderEmployee;
                if (GVEmployee.Rows[e.RowIndex].Tag == null)
                {
                    zOrderEmployee = new Order_Employee_Info();
                    GVEmployee.Rows[e.RowIndex].Tag = zOrderEmployee;
                }
                else
                {
                    zOrderEmployee = new Order_Employee_Info();
                }

                string ID = GVEmployee.Rows[e.RowIndex].Cells["EmployeeID"].Value.ToString();
                Employee_Info zInfo = new Employee_Info();
                zInfo.GetEmployeeID(ID);
                zOrderEmployee.EmployeeKey = zInfo.Key;
                GVEmployee.Rows[e.RowIndex].Cells["EmployeeID"].Tag = zInfo.Key;
                if (zOrderEmployee.Key == 0)
                    zOrderEmployee.RecordStatus = 1;
                else
                    zOrderEmployee.RecordStatus = 2;
            }
        }

        private void Control_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = false;
        }

        private void GVRate_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            switch (GVRate.CurrentCell.ColumnIndex)
            {
                case 0:
                    DataGridViewTextBoxEditingControl te = (DataGridViewTextBoxEditingControl)e.Control;
                    te.AutoCompleteMode = AutoCompleteMode.None;
                    LoadDataToToolbox.AutoCompleteTextBox(te, "SELECT RateID FROM FTR_Category WHERE RecordStatus <> 99");
                    e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);
                    break;
            }
        }
        private void GVRate_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                if(Utils.TNMessageBox("Bạn có xóa thông tin này ?", 2)=="Y")
                {
                    if (GVRate.CurrentRow.Tag != null)
                    {
                        Order_Rate_Info zOrder_Rate = (Order_Rate_Info)GVRate.CurrentRow.Tag;
                        GVRate.CurrentRow.Visible = false;
                        zOrder_Rate.RecordStatus = 99;
                    }
                }
            }
        }
        private void GVRate_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            Order_Rate_Info zOrderRate;
            if (GVRate.Rows[e.RowIndex].Tag == null)
            {
                zOrderRate = new Order_Rate_Info();
                GVRate.Rows[e.RowIndex].Tag = zOrderRate;
            }
            else
            {
                zOrderRate = (Order_Rate_Info)GVRate.Rows[e.RowIndex].Tag;
            }
            if (GVRate.CurrentCell.ColumnIndex == 0)
            {
                string ID = GVRate.CurrentCell.Value.ToString();
                Category_Info zCategory = new Category_Info(ID, true);
                if (zCategory.AutoKey != 0)
                {
                    GVRate.CurrentRow.Cells["RateID"].Tag = zCategory.AutoKey;
                    GVRate.CurrentRow.Cells["RateName"].Value = zCategory.RateName;
                    GVRate.CurrentRow.Cells["Rate"].Value = zCategory.Rate.ToString("n2");

                    zOrderRate.CategoryKey = zCategory.AutoKey;
                    zOrderRate.RateName = zCategory.RateName;
                    zOrderRate.Rate = zCategory.Rate;
                }
            }
            if (zOrderRate.Key == 0)
                zOrderRate.RecordStatus = 1;
            else
                zOrderRate.RecordStatus = 2;

            GVRate.Rows[e.RowIndex].Tag = zOrderRate;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GVOrder_SelectionChanged(object sender, EventArgs e)
        {
            if (GVOrder.CurrentRow.Tag != null)
            {
                Order_Object zOrder = (Order_Object)GVOrder.CurrentRow.Tag;

                GVRate.Rows.Clear();
                GVEmployee.Rows.Clear();
                LoadOrderInfo(zOrder);

                btn_SaveOrder.Enabled = true;
            }
            else
            {
                btn_SaveOrder.Enabled = false;
            }
        }

        private void btn_SaveAll_Click(object sender, EventArgs e)
        {
            if (GVOrder.Rows.Count <= 0)
            {
                Utils.TNMessageBoxOK("Không có dữ liệu !.",1);
                return;
            }
            if(zError>0)
            {
                Utils.TNMessageBoxOK("Số đơn hàng lỗi: "+zError.ToString("n0")+" .Vui lòng kiểm tra lại!.", 2);
                return;
            }    
            using (Frm_Loading frm = new Frm_Loading(InsertData)) { frm.ShowDialog(this); }

            if (_ListOrder.Count > 0)
            {
                _IndexGVOrder = 0;
                GVOrder.Rows.Clear();
                foreach (Order_Object zOrder in _ListOrder)
                {
                    ShowOrder(zOrder);
                }

                string Status = "Import Excel form " + HeaderControl.Text + " > lỗi > SL Lỗi:" + _ListOrder.Count ;
                 Application_Data.Save_LogApplication(SessionUser.UserLogin.Key,SessionUser.UserLogin.EmployeeKey, Status, 3);

                Utils.TNMessageBoxOK("Còn " + _ListOrder.Count + " Đơn hàng chưa lưu !.",2);

            }
            else
            {
                _IsSaved = true;

                string Status = "Import Excel form " + HeaderControl.Text + " > thành công";
                 Application_Data.Save_LogApplication(SessionUser.UserLogin.Key,SessionUser.UserLogin.EmployeeKey, Status, 3);

                Utils.TNMessageBoxOK("Cập nhật thành công !.",3);
                this.Close();
            }
        }

        private void btn_SaveOrder_Click(object sender, EventArgs e)
        {
            try
            {
                Order_Object zOrder = new Order_Object();
                zOrder.OrderID = txt_OrderID.Text.Trim();
                zOrder.TeamKey = cbo_Team.SelectedValue.ToInt();
                if (txt_ProductID.Tag != null)
                    zOrder.ProductKey = txt_ProductID.Tag.ToString();
                zOrder.ProductID = txt_ProductID.Text.Trim();
                zOrder.ProductName = txt_ProductName.Text.Trim();
                if (txt_WorkID.Tag != null)
                {
                    Product_Stages_Info zStage = new Product_Stages_Info(txt_WorkID.Tag.ToInt());
                    zOrder.Category_Stage = zStage.Key;
                    zOrder.GroupKey = zStage.GroupKey;
                    zOrder.GroupID = zStage.GroupID;
                }
                zOrder.UnitKey = txt_Unit.Tag.ToInt();
                zOrder.UnitName = txt_Unit.Text;
                if (cbo_Status.SelectedValue != null)
                    zOrder.WorkStatus = int.Parse(cbo_Status.SelectedValue.ToString());
                zOrder.OrderDate = dte_OrderDate.Value;
                zOrder.OrderID_Compare = txt_OrderIDCompare.Text.Trim();
                zOrder.OrderIDFollow = txt_OrderIDFollow.Text.Trim();
                float zPercentFinish = 0;
                if(float.TryParse(txt_Percent.Text.Trim(),out zPercentFinish))
                {

                }
                zOrder.PercentFinish = zPercentFinish;
                float zQuantityDocument = 0;
                if (float.TryParse(txt_QuantityDocument.Text.Trim(), out zQuantityDocument))
                {

                }
                zOrder.QuantityDocument = zQuantityDocument;
                float zQuantityReality = 0;
                if (float.TryParse(txt_QuantityReality.Text.Trim(), out zQuantityReality))
                {

                }
                zOrder.QuantityReality = zQuantityReality;
                float zQuantityLoss = 0;
                if (float.TryParse(txt_QuantityLose.Text.Trim(), out zQuantityLoss))
                {

                }
                zOrder.QuantityLoss = zQuantityLoss;
                zOrder.Note = txt_Note.Text.Trim();

                if (GVRate.Rows.Count > 0)
                {
                    List<Order_Rate_Info> zRateList = new List<Order_Rate_Info>();
                    for (int i = 0; i < GVRate.Rows.Count; i++)
                    {
                        DataGridViewRow zRow = GVRate.Rows[i];
                        if (zRow.Tag != null)
                        {
                            Order_Rate_Info zOrderRate = (Order_Rate_Info)zRow.Tag;
                            float zRate = float.Parse(zRow.Cells["Rate"].Value.ToString());
                            string zRateName = zRow.Cells["RateName"].Value.ToString().Trim();
                            int zRateKey = zRow.Cells["RateID"].Tag.ToInt();
                            zOrderRate.CategoryKey = zRateKey;
                            zOrderRate.RateName = zRateName;
                            zOrderRate.Rate = zRate;
                            zOrderRate.OrderDate = dte_OrderDate.Value;
                            if (zOrderRate.Key == 0)
                                zOrderRate.RecordStatus = 1;
                            else
                                zOrderRate.RecordStatus = 2;

                            zRateList.Add(zOrderRate);
                        }
                    }
                    zOrder.List_Rate = zRateList;
                }
                if (GVEmployee.Rows.Count > 0)
                {
                    List<Order_Employee_Info> zEmployeeList = new List<Order_Employee_Info>();
                    for (int i = 0; i < GVEmployee.Rows.Count; i++)
                    {
                        DataGridViewRow zRow = GVEmployee.Rows[i];
                        if (zRow.Tag != null)
                        {
                            Order_Employee_Info zOrderEmployee = (Order_Employee_Info)zRow.Tag;
                            zOrderEmployee.OrderDate = dte_OrderDate.Value;
                            zOrderEmployee.EmployeeID = zRow.Cells["EmployeeID"].Value.ToString().Trim();
                            zOrderEmployee.EmployeeKey = zRow.Cells["EmployeeID"].Tag.ToString().Trim();
                            zOrderEmployee.FullName = zRow.Cells["FullName"].Value.ToString().Trim();
                            float zBasket = 0;
                            float.TryParse(zRow.Cells["Basket"].Value.ToString().Trim(), out zBasket);
                            zOrderEmployee.Basket = zBasket;
                            float zKilogram = 0;
                            float.TryParse(zRow.Cells["Kg"].Value.ToString().Trim(), out zKilogram);
                            zOrderEmployee.Kilogram = zKilogram;
                            float zTime = 0;
                            float.TryParse(zRow.Cells["Time"].Value.ToString().Trim(), out zTime);
                            zOrderEmployee.Time = zTime;

                            if (zRow.Cells["Borrow"].Value != null)
                            {
                                if (bool.Parse(zRow.Cells["Borrow"].Value.ToString()) == true)
                                {
                                    zOrderEmployee.Borrow = 1;
                                }
                                else
                                {
                                    zOrderEmployee.Borrow = 0;
                                }
                            }
                            if (zRow.Cells["Private"].Value != null)
                            {
                                if (bool.Parse(zRow.Cells["Private"].Value.ToString()) == true)
                                {
                                    zOrderEmployee.Private = 1;
                                }
                                else
                                {
                                    zOrderEmployee.Private = 0;
                                }
                            }
                            if (zRow.Cells["General"].Value != null)
                            {
                                if (bool.Parse(zRow.Cells["General"].Value.ToString()) == true)
                                {
                                    zOrderEmployee.Share = 1;
                                }
                                else
                                {
                                    zOrderEmployee.Share = 0;
                                }
                            }

                            if (zOrderEmployee.Key == 0)
                                zOrderEmployee.RecordStatus = 1;
                            else
                                zOrderEmployee.RecordStatus = 2;

                            zEmployeeList.Add(zOrderEmployee);
                        }
                    }
                    zOrder.List_Employee = zEmployeeList;
                }

                int Index = _ListOrder.FindIndex(v => v.OrderID == zOrder.OrderID);
                _ListOrder.RemoveAt(Index);
                _ListOrder.Add(zOrder);
            }
            catch (Exception ex)
            {
                Utils.TNMessageBoxOK(ex.ToString(),4);
            }
            finally
            {
                Utils.TNMessageBoxOK("Đã cập nhật",3);
            }
        }

        private void btn_Open_Click(object sender, EventArgs e)
        {
            OpenFileDialog zOpf = new OpenFileDialog
            {
                InitialDirectory = @"C:\",
                DefaultExt = "xlsx",
                Title = "Chọn tập tin Excel",
                Filter = "All Files|*.*",
                FilterIndex = 2,
                ReadOnlyChecked = true,
                CheckFileExists = true,
                CheckPathExists = true,
                RestoreDirectory = true,
                ShowReadOnly = true
            };

            if (zOpf.ShowDialog() == DialogResult.OK)
            {
                GVOrder.Rows.Clear();
                PBar.Maximum = 0;
                _IndexGVOrder = 0;
                _TableEmployee = new DataTable();
                _TableOrder = new DataTable();
                _TableRate = new DataTable();
                _ListOrder = new List<Order_Object>();

                FileInfo = new FileInfo(zOpf.FileName);
                bool zcheck = Data_Access.CheckFileStatus(FileInfo);
                if (zcheck == true)
                {
                    Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                }
                else
                {
                    try
                    {
                        lblFileName.Text = FileInfo.Name;
                        Cursor.Current = Cursors.WaitCursor;
                        using (Frm_Loading frm = new Frm_Loading(ProcessData)) { frm.ShowDialog(this); }
                        Cursor.Current = Cursors.Default;
                        PBar.Maximum = _ListOrder.Count;
                        _ThreadExcelOrder = new Thread(new ThreadStart(ShowListOrderToGV));
                        _ThreadExcelOrder.Start();
                    }
                    catch (Exception ex)
                    {
                        Utils.TNMessageBoxOK(ex.ToString(),4);
                    }
                }
            }
        }

        private void btn_Show_Click(object sender, EventArgs e)
        {
            splitContainer1.SplitterDistance = 300;
            btn_Hide.Visible = true;
            btn_Show.Visible = false;

            Panel_Info.Visible = true;
            Panel_Employee.Visible = true;
        }

        private void btn_Hide_Click(object sender, EventArgs e)
        {
            splitContainer1.SplitterDistance = Screen.PrimaryScreen.Bounds.Width;
            btn_Hide.Visible = false;
            btn_Show.Visible = true;

            Panel_Info.Visible = false;
            Panel_Employee.Visible = false;
        }
        private void Btn_Search_CT_Click(object sender, EventArgs e)
        {
            Frm_Search_Order frm = new Frm_Search_Order();
            frm.ShowDialog();

            if (frm.ProductKey.Length > 0)
            {
                txt_ProductID.Tag = frm.ProductKey;
                txt_OrderIDFollow.Text = frm.OrderID;
                txt_ProductName.Text = frm.ProductName;
                txt_ProductID.Text = frm.ProductID;
                txt_QuantityDocument.Text = frm.Quantity_Document.ToString();
                txt_Unit.Tag = frm.UnitKey;
                txt_Unit.Text = frm.UnitName;
            }
        }

        private void Btn_Seach_Work_Click(object sender, EventArgs e)
        {
            Frm_Setup_Work Frm = new Frm_Setup_Work();
            Frm.TeamKey = int.Parse(cbo_Team.SelectedValue.ToString());
            Frm.SearchStyle = true;
            Frm.ShowDialog();

            Product_Stages_Info zInfo = Frm.Product_Stages;
            if (zInfo != null)
            {
                txt_WorkID.Tag = zInfo.Key;
                txt_WorkID.Text = zInfo.StagesID;
                txt_WorkName.Text = zInfo.StageName;
                txt_Price.Text = zInfo.Price.Ton1String();
            }
        }
        private void CalculatorQuantity()
        {
            decimal zQuantityReal = 0;
            decimal zQuantityDocument = 0;
            decimal zQuantityLoss = 0;

            if (txt_QuantityDocument.Text.Length > 0)
            {
                zQuantityReal = decimal.Parse(txt_QuantityReality.Text);
            }
            if (txt_QuantityDocument.Text.Length > 0)
            {
                zQuantityDocument = decimal.Parse(txt_QuantityDocument.Text);
            }
            if (txt_QuantityDocument.Text.Length > 0)
            {
                zQuantityLoss = zQuantityDocument - zQuantityReal;
            }

            txt_QuantityReality.Text = zQuantityReal.ToString("n1");
            txt_QuantityLose.Text = zQuantityLoss.ToString("n1");
        }
        private void ControlNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }

            if (e.KeyChar == '.')
            {
                e.Handled = false;
            }
            if (e.KeyChar == ',')
            {
                e.Handled = false;
            }
        }
        private void btnClose_Panel_Message_Click(object sender, EventArgs e)
        {
            Panel_Message.Visible = false;
        }
        #endregion
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            if (_IsSaved == true)
            {
                this.Close();
            }
            else
            {
                if (Utils.TNMessageBox("Dữ liệu bạn chưa được cập nhật !.", 2) == "Y")
                {
                    this.Close();
                }
            }
        }
        #endregion


    }
}