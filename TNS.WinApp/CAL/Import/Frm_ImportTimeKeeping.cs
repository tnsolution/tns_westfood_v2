﻿using HHT.HRM;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TN.Library.System;
using TNS.CORE;
using TNS.HRM;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_ImportTimeKeeping : Form
    {
        DateTime _DateLog;
        int _CheckErr = 0;
        string _txtLog = "";
        FileInfo _FileInfo;
        DataTable _TableErr;
        DataTable _TableTime = new DataTable();
        DateTime _ExcelDate = SessionUser.Date_Work;
        private string _Log_Detail = "";
        private string _FileName = "";

        public Frm_ImportTimeKeeping()
        {
            InitializeComponent();
            this.DoubleBuffered = true;

            Utils.DoubleBuffered(GVData, true);
            Utils.DrawGVStyle(ref GVData);
            Utils.DrawLVStyle(ref LVFile);

            btnClose.Click += btnClose_Click;
            btnMax.Click += btnMax_Click;
            btnMini.Click += btnMini_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            btn_New.Click += Btn_New_Click;
            btn_Save.Click += btn_Save_Click;
            MonthCalendar.DateSelected += MonthCalendar_DateSelected;
            btn_Log.Click += Btn_Log_Click;

            SetupLayoutLV(LVFile);
            SetupLayoutGV(GVData);
        }
        private void Frm_ImportTimeKeeping_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);

            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            Panel_Done.Visible = false;
            MonthCalendar.SetDate(SessionUser.Date_Work);
            LoadDataLV();
        }
        private void Btn_Log_Click(object sender, EventArgs e)
        {
            if (btn_Log.Tag.ToInt() == 0)
            {
                Panel_Done.Visible = true;
                btn_Log.Tag = 1;
            }
            else
            {
                Panel_Done.Visible = false;
                btn_Log.Tag = 0;
            }
        }
        void CreateTableError()
        {
            _TableErr = new DataTable();
            _TableErr.Columns.Add("No");
            _TableErr.Columns.Add("MaThe");
            _TableErr.Columns.Add("MaNhom");
            _TableErr.Columns.Add("HoVaTen");
            _TableErr.Columns.Add("GioBD");
            _TableErr.Columns.Add("GioKT");
            _TableErr.Columns.Add("GioNghi");
            _TableErr.Columns.Add("NgoaiGio");
            _TableErr.Columns.Add("TongGio");
            _TableErr.Columns.Add("MaCong");
            _TableErr.Columns.Add("MaCom");
            _TableErr.Columns.Add("MaPhepTru");
            _TableErr.Columns.Add("NgayQuaDemTu");
            _TableErr.Columns.Add("NgayQuaDemDen");
            _TableErr.Columns.Add("ThongBao");
        }

        #region[Listview]
        private void SetupLayoutLV(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "No";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên";
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            Utils.SizeLastColumn_LV(LV);
            LV.Click += LV_Click;
        }
        private void LoadDataLV()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LVFile;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            LV.Items.Clear();

            DataTable zTable = TimeKeeping_Month_Data.DanhSachToNhom();

            for (int i = 0; i < zTable.Rows.Count; i++)
            {
                DataRow nRow = zTable.Rows[i];

                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.Tag = nRow["TeamKey"];
                lvi.ForeColor = Color.Navy;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["TeamID"].ToString();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["TeamName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }
            Utils.SizeLastColumn_LV(LVFile);
            this.Cursor = Cursors.Default;
        }
        private void LV_Click(object sender, EventArgs e)
        {
            if (LVFile.SelectedItems.Count > 0)
            {
                _ExcelDate = MonthCalendar.SelectionStart.Date;
                ShowDataGV(LVFile.SelectedItems[0].Tag.ToInt(), _ExcelDate);
            }
        }
        #endregion

        #region[GV_Data]
        private void SetupLayoutGV(DataGridView GV)
        {
            // Setup Column 
            GV.Columns.Add("No", "STT");
            GV.Columns.Add("EmployeeID", "Mã NV");
            GV.Columns.Add("EmployeeName", "Tên Nhân Viên");
            GV.Columns.Add("TeamID", "Bộ phận");

            GV.Columns.Add("BeginTime", "Giờ bắt đầu");
            GV.Columns.Add("EndTime", "Giờ kết thúc");
            GV.Columns.Add("OffTime", "Số giờ nghỉ trưa");
            GV.Columns.Add("OverTime", "Ngoài giờ");
            GV.Columns.Add("TotalTime", "Tổng giờ");

            GV.Columns.Add("Time", "Mã công");
            GV.Columns.Add("Rice_Plus", "Mã cơm");
            GV.Columns.Add("Rice_Minus", "Mã phép trừ");
            GV.Columns.Add("OverTimeBegin", "Qua đêm từ ngày");
            GV.Columns.Add("OverTimeEnd", "Qua đêm đến ngày");
            GV.Columns.Add("Message", "Thông báo");

            GV.Columns["No"].Width = 40;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV.Columns["No"].ReadOnly = true;
            GV.Columns["No"].Frozen = true;

            GV.Columns["EmployeeID"].Width = 90;
            GV.Columns["EmployeeID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["EmployeeID"].Frozen = true;

            GV.Columns["EmployeeName"].Width = 160;
            GV.Columns["EmployeeName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["EmployeeName"].ReadOnly = true;
            GV.Columns["EmployeeName"].Frozen = true;

            GV.Columns["TeamID"].Width = 90;
            GV.Columns["TeamID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["TeamID"].ReadOnly = true;
            GV.Columns["TeamID"].Frozen = true;

            GV.Columns["BeginTime"].Width = 80;
            GV.Columns["BeginTime"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns["EndTime"].Width = 80;
            GV.Columns["EndTime"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns["OffTime"].Width = 80;
            GV.Columns["OffTime"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns["OverTime"].Width = 80;
            GV.Columns["OverTime"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV.Columns["OverTime"].ReadOnly = true;

            GV.Columns["TotalTime"].Width = 80;
            GV.Columns["TotalTime"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns["Time"].Width = 80;
            GV.Columns["Time"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns["Rice_Plus"].Width = 80;
            GV.Columns["Rice_Plus"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns["Rice_Minus"].Width = 80;
            GV.Columns["Rice_Minus"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns["OverTimeBegin"].Visible = false;
            GV.Columns["OverTimeEnd"].Visible = false;

            GV.Columns["Message"].Width = 150;
            GV.Columns["Message"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            GV.AllowUserToAddRows = true;
            GV.ColumnHeadersHeight = 40;

            GV.EditingControlShowing += GV_EditingControlShowing;
            GV.CellEndEdit += GV_CellEndEdit;
            GV.RowsAdded += GV_RowsAdded;
            GV.RowLeave += GV_RowLeave;
            GV.RowEnter += GV_RowEnter;
            GV.CellClick += GV_CellClick;
            GV.KeyUp += GV_KeyUp;
        }

        private void GV_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {

        }

        private void GV_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            GVData.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LemonChiffon;
        }

        private void GV_RowLeave(object sender, DataGridViewCellEventArgs e)
        {
            GVData.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.White;
        }

        private void GV_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //GVData.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LemonChiffon;
        }

        private void GV_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                if (MessageBox.Show("Bạn muốn xóa dòng này !.", "Xóa thông tin", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    int no = GVData.SelectedRows[0].Index;
                    if (GVData.Rows[no].Cells["No"].Tag == null)
                    {
                        DeleteGridviewRecord(GVData.Rows[no]);
                    }
                    else
                    {
                        Employee_KeepingTime_Info zDetail = new Employee_KeepingTime_Info();
                        zDetail.Key = GVData.Rows[no].Cells["No"].Tag.ToInt();
                        zDetail.RecordStatus = 99;
                        zDetail.ModifiedBy = SessionUser.UserLogin.Key;
                        zDetail.ModifiedName = SessionUser.UserLogin.EmployeeName;
                        zDetail.DeleteAll(GVData.Rows[no].Cells["EmployeeID"].Value.ToString(), _ExcelDate);
                        DeleteGridviewRecord(GVData.Rows[no]);
                    }
                }
            }
        }

        private void LoadDataGV(DataTable InTable)
        {
            GVData.Rows.Clear();
            for (int i = 0; i < InTable.Rows.Count; i++)
            {
                DataRow zRow = InTable.Rows[i];
                GVData.Rows.Add();
                GVData.Rows[i].Cells["No"].Value = (i + 1).ToString();
                GVData.Rows[i].Cells["No"].Tag = zRow["KeepingKey"].ToString();
                GVData.Rows[i].Cells["EmployeeName"].Value = zRow["EmployeeName"].ToString();
                GVData.Rows[i].Cells["EmployeeID"].Value = zRow["EmployeeID"].ToString();
                GVData.Rows[i].Cells["TeamID"].Value = zRow["TeamID"].ToString();
                if (zRow["BeginTime"].ToString() == "" || zRow["BeginTime"].ToString() == "0")
                {
                    GVData.Rows[i].Cells["BeginTime"].Value = "0:0";
                }
                else
                {
                    GVData.Rows[i].Cells["BeginTime"].Value = zRow["BeginTime"].ToString();
                }
                if (zRow["EndTime"].ToString() == "" || zRow["EndTime"].ToString() == "0")
                {
                    GVData.Rows[i].Cells["EndTime"].Value = "0:0";
                }
                else
                {
                    GVData.Rows[i].Cells["EndTime"].Value = zRow["EndTime"].ToString();
                }
                if (zRow["OffTime"].ToString() == "" || zRow["OffTime"].ToString() == "0")
                {
                    GVData.Rows[i].Cells["OffTime"].Value = "0:0";
                }
                else
                {
                    GVData.Rows[i].Cells["OffTime"].Value = zRow["OffTime"].ToString();
                }

                if (zRow["OverTime"].ToString() == "" || zRow["OverTime"].ToString() == "0")
                {
                    GVData.Rows[i].Cells["OverTime"].Value = "0:0";
                }
                else
                {
                    GVData.Rows[i].Cells["OverTime"].Value = zRow["OverTime"].ToString();
                }
                if (zRow["TotalTime"].ToString() == "" || zRow["TotalTime"].ToString() == "0")
                {
                    GVData.Rows[i].Cells["TotalTime"].Value = "0:0";
                }
                else
                {
                    GVData.Rows[i].Cells["TotalTime"].Value = zRow["TotalTime"].ToString();
                }
                GVData.Rows[i].Cells["Time"].Tag = zRow["TimeKey"].ToString();
                GVData.Rows[i].Cells["Time"].Value = zRow["TimeID"].ToString();
                GVData.Rows[i].Cells["Rice_Plus"].Value = zRow["RicePlus"].ToString();
                GVData.Rows[i].Cells["Rice_Minus"].Value = zRow["RiceMinus"].ToString();
                GVData.Rows[i].Cells["OverTimeBegin"].Value = zRow["OverTimeBegin"].ToString();
                GVData.Rows[i].Cells["OverTimeEnd"].Value = zRow["OverTimeEnd"].ToString();
                GVData.Rows[i].Cells["Message"].Tag = 0;
            }

        }
        private void GV_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            DataGridViewTextBoxEditingControl te = (DataGridViewTextBoxEditingControl)e.Control;
            te.AutoCompleteMode = AutoCompleteMode.None;
            switch (GVData.CurrentCell.ColumnIndex)
            {
                case 1: // autocomplete for product
                    if (LVFile.SelectedItems.Count > 0)
                    {
                        int TeamKey = LVFile.SelectedItems[0].Tag.ToInt();
                        LoadDataToToolbox.AutoCompleteTextBox(te, "SELECT EmployeeID FROM HRM_Employee WHERE TeamKey = " + TeamKey + " AND RecordStatus <> 99  ");
                    }
                    else
                    {
                        MessageBox.Show("Bạn phải chọn tổ nhóm");
                    }
                    e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);
                    break;
                case 4:
                    e.Control.KeyPress += new KeyPressEventHandler(ControlNumber_KeyPress);
                    break;
                case 5:
                    e.Control.KeyPress += new KeyPressEventHandler(ControlNumber_KeyPress);
                    break;
                case 6:
                    e.Control.KeyPress += new KeyPressEventHandler(ControlNumber_KeyPress);
                    break;
                case 7:
                    e.Control.KeyPress += new KeyPressEventHandler(ControlNumber_KeyPress);
                    break;

                default:
                    e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);

                    break;
            }
        }
        private void GV_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow zRowEdit = GVData.Rows[e.RowIndex];
            switch (e.ColumnIndex)
            {
                case 1:
                    if (zRowEdit.Cells[e.ColumnIndex].Value != null)
                    {
                        string zEmployeeID = zRowEdit.Cells["EmployeeID"].Value.ToString().Trim();
                        Employee_Info zEmployee = new Employee_Info();
                        zEmployee.GetEmployeeID(zEmployeeID);
                        if (zEmployee.EmployeeID.Trim().Length > 0)
                        {
                            zRowEdit.Cells["EmployeeName"].Value = zEmployee.FullName;
                            zRowEdit.Cells["TeamID"].Value = LVFile.SelectedItems[0].SubItems[1].Text;
                            //zRowEdit.Cells["Amount"].Value = _Sotien.ToString("n0");
                        }
                        else
                        {
                            MessageBox.Show("Chưa có mã công nhân này !.");
                        }
                    }
                    break;
                case 4: // giờ bđ
                    if (zRowEdit.Cells[e.ColumnIndex].Value != null)
                    {
                        zRowEdit.Cells[e.ColumnIndex].Tag = zRowEdit.Cells[e.ColumnIndex].Value.ToString();// he so gio
                    }
                    else
                    {
                        zRowEdit.Cells[e.ColumnIndex].Tag = "0:0";
                    }
                    break;
                case 5: // giờ kt
                    if (zRowEdit.Cells[e.ColumnIndex].Value != null)
                    {
                        zRowEdit.Cells[e.ColumnIndex].Tag = zRowEdit.Cells[e.ColumnIndex].Value.ToString();// he so gio
                    }
                    else
                    {
                        zRowEdit.Cells[e.ColumnIndex].Tag = "0:0";
                    }
                    break;
                case 6: // giờ nghi
                    if (zRowEdit.Cells[e.ColumnIndex].Value != null)
                    {
                        zRowEdit.Cells[e.ColumnIndex].Tag = zRowEdit.Cells[e.ColumnIndex].Value.ToString();// he so gio
                    }
                    else
                    {
                        zRowEdit.Cells[e.ColumnIndex].Tag = "0:0";
                    }
                    break;
                case 7: // tong giờ
                    if (zRowEdit.Cells[e.ColumnIndex].Value != null)
                    {
                        zRowEdit.Cells[e.ColumnIndex].Tag = zRowEdit.Cells[e.ColumnIndex].Value.ToString();// he so gio
                    }
                    else
                    {
                        zRowEdit.Cells[e.ColumnIndex].Tag = "0:0";
                    }
                    break;
                case 8:
                    if (zRowEdit.Cells[e.ColumnIndex].Value != null)
                    {
                        zRowEdit.Cells[e.ColumnIndex].Tag = zRowEdit.Cells[e.ColumnIndex].Value.ToString();// mã công                                                                                                         
                    }
                    else
                    {
                        zRowEdit.Cells[e.ColumnIndex].Tag = "0:0";
                    }
                    break;
                case 9:
                    if (zRowEdit.Cells[e.ColumnIndex].Value != null)
                    {
                        zRowEdit.Cells[e.ColumnIndex].Tag = zRowEdit.Cells[e.ColumnIndex].Value.ToString();// mã cơm cộng                                                                                                         
                    }
                    else
                    {
                        zRowEdit.Cells[e.ColumnIndex].Tag = "0:0";
                    }
                    break;
                case 10:
                    if (zRowEdit.Cells[e.ColumnIndex].Value != null)
                    {
                        zRowEdit.Cells[e.ColumnIndex].Tag = zRowEdit.Cells[e.ColumnIndex].Value.ToString();// mã cơm trừ                                                                                                        
                    }
                    else
                    {
                        zRowEdit.Cells[e.ColumnIndex].Tag = "0:0";
                    }
                    break;
            }
            if (CheckGridviewRow(GVData.Rows[e.RowIndex]).Length <= 0)
            {
                // Calculator(zRowEdit);
            }
        }
        private void ControlNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }

            if (e.KeyChar == ':')
            {
                e.Handled = false;
            }
            //if (e.KeyChar == ',')
            //{
            //    e.Handled = false;
            //}
        }
        private void Control_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = false;
        }
        #endregion

        #region[Event]
        private void MonthCalendar_DateSelected(object sender, DateRangeEventArgs e)
        {
            if (LVFile.SelectedItems.Count > 0)
            {
                _ExcelDate = MonthCalendar.SelectionStart.Date;
                txtSubTitle.Text = "Chi tiết thời gian nhóm làm việc trong ngày: " + _ExcelDate.ToString("dd/MM/yyyy");
                ShowDataGV(LVFile.SelectedItems[0].Tag.ToInt(), _ExcelDate);
            }
        }
        private void Btn_New_Click(object sender, EventArgs e)
        {
            Panel_Done.Visible = true;
            btn_Log.Tag = 1;

            OpenFileDialog zOpf = new OpenFileDialog();

            zOpf.InitialDirectory = @"C:\";
            zOpf.DefaultExt = "txt";
            zOpf.Title = "Browse Excel Files";
            zOpf.Filter = "All Files|*.*";
            zOpf.FilterIndex = 2;
            zOpf.CheckFileExists = true;
            zOpf.CheckPathExists = true;
            zOpf.RestoreDirectory = true;
            zOpf.ReadOnlyChecked = true;
            zOpf.ShowReadOnly = true;
            _FileName = "";
            if (zOpf.ShowDialog() == DialogResult.OK)
            {
                _FileInfo = new FileInfo(zOpf.FileName);
                string[] s = _FileInfo.Name.Split('_');
                _FileName = _FileInfo.Name;

                if ((s.Length < 2) || (s[0].Length != 4 && s[1].Length != 2))
                {
                    Utils.TNMessageBoxOK("Lỗi: Định dạng tên file không đúng! \n Định dạng đúng là: yyyy_mm_Tenfile.xlsx", 1);
                }
                else
                {
                    bool zcheck = Data_Access.CheckFileStatus(_FileInfo);
                    string Path = @"C:\Loi_Excel_Chua_Import\";
                    if (Directory.Exists(Path.Substring(0,Path.Length-1)))
                    {
                        Directory.Delete(Path.Substring(0, Path.Length - 1),true);
                    }
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                    }
                    else
                    {
                        try
                        {
                            txt_Log.Items.Clear();
                            txt_LogShort.Items.Clear();

                            _DateLog = new DateTime(s[0].ToInt(), s[1].ToInt(), 1, 0, 0, 0);

                            using (Frm_Loading frm = new Frm_Loading(ProcessData)) { frm.ShowDialog(this); }

                            //string Path = @"C:\Loi_Excel_Chua_Import\";
                            if (Directory.Exists(Path))
                            {
                                Utils.TNMessageBoxOK("Có lỗi tập tin Excel, click OK để mở thư mục chứa !.", 4);
                                Process.Start(Path);
                            }
                            else
                            {
                                Utils.TNMessageBoxOK("Import thành công!",3);
                            }    
                        }
                        catch (Exception ex)
                        {
                            Utils.TNMessageBoxOK(ex.ToString(), 4);
                            btn_Save.Enabled = false;
                            AddLogShort("Lỗi.Xử lý Import");
                        }
                        
                    }
                }
            }
        }
        private void btn_Save_Click(object sender, EventArgs e)
        {
            if (GVData.Rows.Count > 0)
            {
                this.Cursor = Cursors.WaitCursor;
                int Count = CheckBeforeSaveDataGridView();
                if (Count > 0)
                {
                    Utils.TNMessageBoxOK("Vui lòng kiểm tra dữ liệu còn :" + Count + " dòng đang báo lỗi ", 2);
                }
                else
                {
                    int Mesage = SaveDataGridView();
                    if (Mesage == 0)
                    {
                        string Status = "Import Excel form " + HeaderControl.Text + " > thành công";
                        Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, Status, 3);

                        Utils.TNMessageBoxOK("Cập nhật dữ liêu thành công", 3);
                    }
                    else
                    {
                        string Status = "Import Excel form " + HeaderControl.Text + " > lỗi > SL Lỗi:" + Mesage;
                        Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, Status, 3);

                        Utils.TNMessageBoxOK("Vui lòng kiểm tra dữ liệu còn :" + Mesage + " dòng đang báo lỗi ", 4);
                    }
                }
                this.Cursor = Cursors.Default;
            }
        }
        #endregion


        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        //-------------------------------------Help function process excel file
        void AddRowError(DataRow zRow, string Message)
        {
            string ztext = "";
            _CheckErr++;
            DataRow r = _TableErr.NewRow();
            r["No"] = zRow[0].ToString().Trim();
            r["MaThe"] = zRow[1].ToString().Trim();
            r["MaNhom"] = zRow[2].ToString().Trim();
            r["HoVaTen"] = zRow[3].ToString().Trim();
            r["GioBD"] = zRow[4].ToString().Trim();
            r["GioKT"] = zRow[5].ToString().Trim();
            r["GioNghi"] = zRow[6].ToString().Trim();
            r["NgoaiGio"] = zRow[7].ToString().Trim();
            r["TongGio"] = zRow[8].ToString().Trim();
            r["MaCong"] = zRow[9].ToString().Trim();
            r["MaCom"] = zRow[10].ToString().Trim();
            r["MaPhepTru"] = zRow[11].ToString().Trim();

            r["NgayQuaDemTu"] = zRow[12].ToString().Trim();
            r["NgayQuaDemDen"] = zRow[13].ToString().Trim();
            r["ThongBao"] = Message;
            _TableErr.Rows.Add(r);
            ztext = Environment.NewLine;
            ztext += "Dòng       :" + zRow[0].ToString().Trim() + Environment.NewLine;
            ztext += "Mã thẻ     :" + zRow[1].ToString().Trim() + Environment.NewLine;
            ztext += "Mã nhóm    :" + zRow[2].ToString().Trim() + Environment.NewLine;
            ztext += "Họ tên     :" + zRow[3].ToString().Trim() + Environment.NewLine;
            ztext += "Giờ BĐ     :" + zRow[4].ToString().Trim() + Environment.NewLine;
            ztext += "Giờ KT     :" + zRow[5].ToString().Trim() + Environment.NewLine;
            ztext += "Giờ nghỉ   :" + zRow[6].ToString().Trim() + Environment.NewLine;
            ztext += "Ngoài giờ  :" + zRow[7].ToString().Trim() + Environment.NewLine;
            ztext += "Tổng giờ   :" + zRow[8].ToString().Trim() + Environment.NewLine;
            ztext += "Mã công    :" + zRow[9].ToString().Trim() + Environment.NewLine;
            ztext += "Mã cơm     :" + zRow[10].ToString().Trim() + Environment.NewLine;
            ztext += "Mã cơm trư :" + zRow[11].ToString().Trim() + Environment.NewLine;
            ztext += "Qua đêm từ :" + zRow[12].ToString().Trim() + Environment.NewLine;
            ztext += "Qua đêm đến:" + zRow[13].ToString().Trim() + Environment.NewLine;
            ztext += "Lỗi        :" + Message;
            AddLog_Detail("--- " + ztext + " ---");
        }
        string CheckExcelRow(DataRow zRow, DateTime SheetDate)
        {
            string zMessage = "";

            string zEmployeeName = zRow[3].ToString().Trim();
            string zEmployeeID = "";

            zEmployeeID = zRow[1].ToString().Trim();
            string zTeamID = "";

            zTeamID = zRow[2].ToString().Trim();

            string zTime = zRow[9].ToString().Trim();
            string zRice_Plus = zRow[10].ToString().Trim();
            string zRice_Minus = zRow[11].ToString().Trim();

            string zOverTimeBegin = "";
            zOverTimeBegin = zRow[12].ToString().Trim();
            string zOverTimeEnd = "";
            zOverTimeEnd = zRow[13].ToString().Trim();

            if (SessionUser.Date_Lock >= SheetDate)
            {
                zMessage += "\n Stt:" + (zRow[0].ToString().Trim()) + ";Thời gian này đã khóa sổ.!";
            }

            Employee_Info zEmployee = new Employee_Info();
            zEmployee.GetEmployeeID(zEmployeeID);
            if (zEmployeeID == "")
            {
                zMessage += "\n Stt:" + (zRow[0].ToString().Trim()) + ";Lỗi!Chưa nhập mã thẻ";
            }
            else if (zEmployee.Key == "")
            {
                zMessage += "\n Stt:" + (zRow[0].ToString().Trim()) + ";Lỗi!Mã thẻ [ " + zEmployeeID + " ]không tồn tại";
            }
            else
            {
                if (zEmployee.TeamKey == 0)
                    zMessage += "\n Stt:" + (zRow[0].ToString().Trim()) + ";Lỗi!Mã thẻ [ " + zEmployeeID + " ]chưa thuộc nhóm nào";
            }
            if (zEmployee.TeamKey == 0)
            {
                zMessage += "\n Stt:" + (zRow[0].ToString().Trim()) + ";Lỗi!Nhân viên Chưa cài đặt tổ nhóm";
            }
            if (zEmployee.DepartmentKey == 0)
            {
                zMessage += "\n Stt:" + (zRow[0].ToString().Trim()) + ";Lỗi!Nhân viên Chưa cài đặt phòng ban";
            }
            //Team_Info zTeam = new Team_Info();
            //zTeam.Get_Team_ID(zTeamID);
            //if (zTeam.Key == 0)
            //{
            //    zMessage += "\n Stt:" + (zRow[0].ToString().Trim()) + ";Lỗi!Mã nhóm:" + zTeamID + "không tồn tại";
            //}
            //

            string zCheckTime = "";
            zCheckTime = CheckTime(zRow[4].ToString().Trim());
            if (zCheckTime == "Err")
            {
                zMessage += "\n Stt:" + (zRow[0].ToString().Trim()) + ";Lỗi!Giờ bắt đầu [ " + zRow[4].ToString().Trim() + " ]không đúng định dạng";
            }
            else
            {
                zRow[4] = zCheckTime;
            }

            zCheckTime = CheckTime(zRow[5].ToString().Trim());
            if (zCheckTime == "Err")
            {
                zMessage += "\n Stt:" + (zRow[0].ToString().Trim()) + ";Lỗi!Giờ kết thúc [ " + zRow[5].ToString().Trim() + " ]không đúng định dạng";
            }
            else
            {
                zRow[5] = zCheckTime;
            }
            zCheckTime = CheckTime(zRow[6].ToString().Trim());
            if (zCheckTime == "Err")
            {
                zMessage += "\n Stt:" + (zRow[0].ToString().Trim()) + ";Lỗi!Giờ nghỉ [ " + zRow[6].ToString().Trim() + " ]không đúng định dạng";
            }
            else
            {
                zRow[6] = zCheckTime;
            }
            zCheckTime = CheckTime(zRow[7].ToString().Trim());
            if (zCheckTime == "Err")
            {
                zMessage += "\n Stt:" + (zRow[0].ToString().Trim()) + ";Lỗi!Ngoài giờ [ " + zRow[7].ToString().Trim() + " ]không đúng định dạng";
            }
            else
            {
                zRow[7] = zCheckTime;
            }
            zCheckTime = CheckTime(zRow[8].ToString().Trim());
            if (zCheckTime == "Err")
            {
                zMessage += "\n Stt:" + (zRow[0].ToString().Trim()) + ";Lỗi!Tổng giờ [ " + zRow[8].ToString().Trim() + " ]không đúng định dạng";
            }
            else
            {
                zRow[8] = zCheckTime;
            }



            if (zTime == "")
            {
                zMessage += "\n Stt:" + (zRow[0].ToString().Trim()) + ";Lỗi!Mã công [ " + zTime + " ]không tồn tại";
            }
            else
            {
                Time_Defines_Info zDefine = new Time_Defines_Info();
                zDefine.Get_Info(zTime);
                if (zDefine.Key == 0)
                {
                    zMessage += "\n Stt:" + (zRow[0].ToString().Trim()) + ";Lỗi!Mã công [ " + zTime + " ]không tồn tại";
                }

            }
            if (zRice_Plus != "")
            {
                string[] s = zRice_Plus.ToString().Split(',');
                int zLoi = 0;
                for (int k = 0; k < s.Count(); k++)
                {
                    int zNumber = s[k].Trim().Substring(0, 1).ToInt();
                    string zRiceID = s[k].Trim().Substring(1).ToString();

                    Time_Rice_Info zRice = new Time_Rice_Info();
                    zRice.Get_Info(zRiceID);
                    if (zRice.Key == 0 || zRice.Slug == 1 || zNumber==0)
                    {
                        zLoi++;
                    }
                }
                if (zLoi > 0)
                {
                    zMessage += "\n Stt:" + (zRow[0].ToString().Trim()) + ";Lỗi!Mã cơm [ " + zRice_Plus + " ]không tồn tại";
                }
            }
            if (zRice_Minus != "")
            {
                string[] s = zRice_Minus.ToString().Split(',');
                int zLoi = 0;
                for (int k = 0; k < s.Count(); k++)
                {
                    int zNumber = s[k].Trim().Substring(0, 1).ToInt();
                    string zRiceID = s[k].Trim().Substring(1).ToString();

                    Time_Rice_Info zRice = new Time_Rice_Info();
                    zRice.Get_Info(zRiceID);
                    if (zRice.Key == 0 || zRice.Type == 0 || zNumber == 0)
                    {
                        zLoi++;
                    }
                }
                if (zLoi > 0)
                {
                    zMessage += "\n Stt:" + (zRow[0].ToString().Trim()) + ";Lỗi!Mã cơm [ " + zRice_Minus + " ]không tồn tại";
                }
            }
            
            if (zOverTimeBegin != "")
            {
                DateTime OverTimeBegin; ;
                if (!DateTime.TryParse(zOverTimeBegin, out OverTimeBegin))
                {
                    zMessage += "\n Stt:" + (zRow[0].ToString().Trim()) + ";Lỗi!Ngày qua đêm từ [ " + zOverTimeBegin + " ]không đúng định dạng";
                }
            }
            if (zOverTimeEnd != "")
            {
                DateTime OverTimeEnd;
                if (!DateTime.TryParse(zOverTimeEnd, out OverTimeEnd))
                {
                    zMessage += "\n Stt:" + (zRow[0].ToString().Trim()) + ";Lỗi!Ngày qua đêm đến [ " + zOverTimeBegin + " ]không đúng định dạng";
                }
            }

            return zMessage;
        }
        //funtion
        private string CheckTime(string Time)
        {
            DateTime zFromDate = DateTime.MinValue;
            if (Time == "" || Time == "0")
            {
                return "";
            }
            else if (DateTime.TryParse(Time, out zFromDate) && Time.Length > 5)
            {
                return Time.Substring(11);
            }

            else if (Time.Length > 2 && Time.Length <= 5 && Time.Substring(2, 1) == ":")
            {
                return Time;
            }
            else
            {
                return "Err";
            }
        }

        string ExportTableToExcel(DataTable zTable, string Path,string FileName)
        {
            if (!Directory.Exists(Path))
            {
                Directory.CreateDirectory(Path);
            }
            try
            {
                var newFile = new FileInfo(Path+ FileName);
                if (newFile.Exists)
                {
                    if (!IsFileLocked(newFile))
                    {
                        newFile.Delete();
                    }
                    else
                    {
                        return "Tập tin đang sử dụng !.";
                    }
                }
                using (var package = new ExcelPackage(newFile))
                {
                    //Tạo Sheet 1 mới với tên Sheet là DonHang
                    ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Sheet1");
                    worksheet.Cells["A1"].LoadFromDataTable(zTable, true);
                    worksheet.Cells.Style.Font.Name = "Times New Roman";
                    worksheet.Cells.Style.Font.Size = 12;
                    // worksheet.Cells.AutoFilter = true;
                    worksheet.Cells.AutoFitColumns();
                    // worksheet.View.FreezePanes(2, 4);

                    //Rowheader và row tổng
                    worksheet.Row(1).Height = 30;
                    worksheet.Row(1).Style.WrapText = true;
                    worksheet.Row(1).Style.Font.Bold = true;
                    worksheet.Row(1).Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                    worksheet.Row(1).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    //worksheet.Row(zTable.Rows.Count + 1).Style.Font.Bold = true;//dòng tổng
                    //Chỉnh fix độ rộng cột
                    //for (int j = 4; j <= zTable.Columns.Count; j++)
                    //{
                    //    worksheet.Column(j).Width = 15;
                    //}
                    //worksheet.Column(1).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    //worksheet.Column(1).Width = 5;
                    //worksheet.Column(2).Width = 30;
                    //worksheet.Column(3).Width = 15;
                    //worksheet.Column(3).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    //worksheet.Column(zTable.Columns.Count).Style.Font.Bold = true;//côt tổng
                    //Canh phải số
                    //for (int i = 2; i <= zTable.Rows.Count + 1; i++)
                    //{
                    //    for (int j = 4; j <= zTable.Columns.Count; j++)
                    //    {
                    //        worksheet.Cells[i, j].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    //    }
                    //}
                    //Lưu file Excel
                    package.SaveAs(newFile);
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
            return "OK";
        }
        protected virtual bool IsFileLocked(FileInfo file)
        {
            try
            {
                using (FileStream stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    stream.Close();
                }
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }

            //file is not locked
            return false;
        }

        //-------------------------------------Main process excel data file
        void ProcessData()
        {
            #region[Log]
            _txtLog = "";
            _Log_Detail = "";

            string zStatus = "Khởi động Import chấm công > tháng:" + _DateLog.ToString("MM/yyyy") + " >Tên file:" + _FileName; ;
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, zStatus, 3);

            AddLog_Detail("/--- " + zStatus + " ---/");
            AddLog_Detail("/---Lúc " + DateTime.Now.ToString("HH: mm") + "---/");
            AddLog_Detail(Environment.NewLine);

            #endregion

            AddLogShort("---Bắt đầu : " + DateTime.Now.ToString("HH:mm") + " --//");

            ReadingExcel();
            this.Invoke(new MethodInvoker(delegate ()
            {
                ShowDataGV(0, _ExcelDate);
            }));

            AddLogShort("---Đã xong: " + DateTime.Now.ToString("HH:mm") + " --//");

            #region[Log]
            zStatus = "Kết thúc Import chấm công > tháng:" + _DateLog.ToString("MM/yyyy") + " >Tên file:" + _FileName; ;

            AddLog_Detail("/--- " + zStatus + " ---/");
            AddLog_Detail("/---Đã xong lúc " + DateTime.Now.ToString("HH: mm") + "---/");
            Save_LogCaculator_Import(zStatus);
            #endregion
        }

        void ReadingExcel()
        {
            DataTable TableSheet = Data_Access.GetSheet(_FileInfo.FullName);
            for (int i = 0; i < TableSheet.Rows.Count; i++)
            {
                string SheetNo = TableSheet.Rows[i][0].ToString();
                //kiem tra nếu sheet là số thì thực hiện xử lý dữ liệu
                int isNumber = 0;
                if (int.TryParse(SheetNo, out isNumber))
                {
                    ReadingSheet(SheetNo);
                }
                else
                {
                    AddLog_Detail("--Sheet sai nguyên tắc đặt tên.Bỏ qua không tính > Tên Sheet:[" + SheetNo + "]--");
                    AddLogShort("Sheet:[" + SheetNo + "] vi phạm nguyên tắc đặt tên");
                }
            }
        }
        void ReadingSheet(string SheetNo)
        {
            AddLogShort("//-------Đang đọc sheet " + SheetNo + " --------//");
            AddLog_Detail("//-------Đang đọc sheet " + SheetNo + " --------//");

            //--------------
            ExcelPackage package = new ExcelPackage(_FileInfo);
            DataTable TableData = Data_Access.GetTable(_FileInfo.FullName, SheetNo);
            package.Dispose();
            if (TableData.Rows.Count > 0)
                SaveDataSheet(TableData, SheetNo);
            else
            {
                AddLogShort("Sheet " + SheetNo + " không tìm có dữ liệu.");
                AddLog_Detail("Sheet " + SheetNo + " không tìm có dữ liệu.");
            }
        }
        void SaveDataSheet(DataTable InTable, string SheetNo)
        {
            if (SheetNo.Length == 2)
            {
                string[] temp = _FileInfo.Name.Split('_');
                string year = temp[0].ToString();
                string month = temp[1].ToString();
                string day = SheetNo.ToString();
                int DayInMonth = System.DateTime.DaysInMonth(year.ToInt(), month.ToInt());
                if (day.ToInt() <= DayInMonth)
                {
                    DateTime zDate;
                    if (DateTime.TryParse(new DateTime(year.ToInt(), month.ToInt(), day.ToInt(), 0, 0, 0).ToString(), out zDate))
                    {
                        try
                        {
                            AddLogShort( "Tổng dòng :" + InTable.Rows.Count  );
                            AddLog_Detail("Tổng dòng :" + InTable.Rows.Count + "");

                            CreateTableError();
                            int zCountErr = 0;
                            int zCountSuccess = 0;
                            for (int i = 0; i < InTable.Rows.Count; i++)
                            {
                                DataRow zRow = InTable.Rows[i];
                                string MessageExcel = "";

                                AddLog_Detail("--Bắt đầu kiểm tra lỗi dòng:[" + i + "]--");

                                MessageExcel = CheckExcelRow(zRow, zDate);
                                if (MessageExcel == string.Empty)
                                {
                                    AddLog_Detail("--Kết thúc kiểm tra lỗi dòng:[" + i + "] > Tình trạng:Không lỗi >Bắt đầu lưu SQL--");
                                    string MessageSQL = InsertToSQL(zRow, zDate);
                                    if (MessageSQL != string.Empty)
                                    {
                                        AddRowError(zRow, MessageSQL);
                                        zCountErr++;
                                    }
                                    else
                                    {
                                        zCountSuccess++;
                                    }
                                }
                                else
                                {
                                    AddLog_Detail("--Kết thúc kiểm tra lỗi dòng:[" + i + "] > Tình trạng:Lỗi > Bắt đầu lưu lỗi--");
                                    AddRowError(zRow, MessageExcel);
                                    zCountErr++;
                                }
                            }
                            AddLogShort("Thành công : " + zCountSuccess.ToString());
                            AddLogShort("Bị lỗi : " + zCountErr );

                            AddLog_Detail("--Thành công/Lỗi: " + zCountSuccess.ToString() + "/" + zCountErr + "--");

                            ExportTableError(zDate);
                        }
                        catch (Exception Ex)
                        {
                            AddLogShort("Lỗi.Vui lòng liên hệ IT.");
                            AddLog_Detail("Lỗi.Vui lòng liên hệ IT > Chi tiết:" + Ex.ToString());
                        }
                    }
                }
                else
                {
                    AddLogShort("Ngày [ " + day + " ]không có trong tháng tải lên!");
                    AddLog_Detail("Ngày [ " + day + " ]không có trong tháng tải lên!");
                }
            }
            else
            {
                AddLogShort("Sheet " + SheetNo + "> 2 kí tự không tải lên ");
                AddLog_Detail("Sheet " + SheetNo + " > 2 kí tự không tải lên ");
            }
        }

        string InsertToSQL(DataRow zRow, DateTime SheetDate)
        {

            string zEmployeeID = zRow[1].ToString().Trim();
            Employee_KeepingTime_Info zKeeping = new Employee_KeepingTime_Info(zEmployeeID, SheetDate);
            if (zKeeping.Key == 0)
            {
                AddLog_Detail("Dữ liệu thêm mới");
            }
            else
            {
                AddLog_Detail("Dữ liệu cập nhật");
            }

            zKeeping.DateWrite = SheetDate;

            Employee_Info zEmployee = new Employee_Info();
            zEmployee.GetEmployeeID(zEmployeeID);
            zKeeping.EmployeeKey = zEmployee.Key;
            zKeeping.EmployeeID = zEmployeeID;
            zKeeping.EmployeeName = zRow[3].ToString().Trim();

            //string zTeamID = zRow[2].ToString().Trim();
            //Team_Info zTeam = new Team_Info(zEmployee.TeamKey);
            //zTeam.Get_Team_ID(zTeamID);
            int zTeamKey = Team_Data.GetTeamKeyCurrent(zEmployee.Key, SheetDate);
            Team_Info zTeam = new Team_Info(zTeamKey);
            zKeeping.TeamKey = zTeam.Key;
            zKeeping.TeamID = zTeam.TeamID;
            zKeeping.TeamName = zTeam.TeamName;

            if (zRow[4].ToString().Trim() != "")
            {

                zKeeping.BeginTime = zRow[4].ToString().Trim().Substring(0, 5);
                zKeeping.BeginHour = int.Parse(zRow[4].ToString().Trim().Substring(0, 2));
                zKeeping.BeginMinute = int.Parse(zRow[4].ToString().Trim().Substring(3, 2));
            }
            else
            {
                zKeeping.BeginTime = "00:00";
            }
            if (zRow[5].ToString().Trim() != "")
            {
                zKeeping.EndTime = zRow[5].ToString().Trim().Substring(0, 5);
                zKeeping.EndHour = int.Parse(zRow[5].ToString().Trim().Substring(0, 2));
                zKeeping.EndMinute = int.Parse(zRow[5].ToString().Trim().Substring(3, 2));
            }
            else
            {
                zKeeping.EndTime = "00:00";
            }
            if (zRow[6].ToString().Trim() != "")
            {
                zKeeping.OffTime = zRow[6].ToString().Trim().Substring(0, 5);
            }
            else
            {
                zKeeping.OffTime = "00:00";
            }
            if (zRow[7].ToString().Trim() != "")
            {
                zKeeping.OverTime = zRow[9].ToString().Trim().Substring(0, 5);
            }
            else
            {
                zKeeping.OverTime = "00:00";
            }
            if (zRow[8].ToString().Trim() != "")
            {
                zKeeping.TotalTime = zRow[8].ToString().Trim().Substring(0, 5);
            }
            else
            {
                zKeeping.TotalTime = "00:00";
            }

            if (zRow[9].ToString().Trim() != "")
            {
                Time_Defines_Info zDefine = new Time_Defines_Info();
                zDefine.Get_Info(zRow[9].ToString().Trim());
                zKeeping.TimeKey = zDefine.Key;
                zKeeping.TimeID = zRow[9].ToString().Trim();
                zKeeping.TimeName = zDefine.CodeName;
            }
            if (zRow[10].ToString().Trim() != "")
                zKeeping.RicePlus = zRow[10].ToString().Trim();
            else
                zKeeping.RicePlus = "";
            if (zRow[11].ToString().Trim() != "")
                zKeeping.RiceMinus = zRow[11].ToString().Trim();
            else
                zKeeping.RiceMinus = "";

            if (zRow[12].ToString().Trim() == "")
            {
                zKeeping.OverTimeBegin = DateTime.MinValue;
            }
            else
            {
                zKeeping.OverTimeBegin = DateTime.Parse(zRow[12].ToString().Trim());
            }
            if (zRow[13].ToString().Trim() == "")
            {
                zKeeping.OverTimeEnd = DateTime.MinValue;
            }
            else
            {
                zKeeping.OverTimeEnd = DateTime.Parse(zRow[13].ToString().Trim());
            }

            if (zKeeping.OverTimeBegin != DateTime.MinValue && zKeeping.OverTimeEnd != DateTime.MinValue)
            {
                zKeeping.Slug = 1;
            }
            else
            {
                zKeeping.Slug = 0;
            }

            float zHeSoLe = HoTroSanXuat.LayHeSoNgayLe(SheetDate); //Lấy hệ số lễ

            zKeeping.Class = "TH";//Ngày thường

            if (zKeeping.DateWrite.DayOfWeek == DayOfWeek.Sunday) //chủ nhật
                zKeeping.Class = "CN";//Chủ nhật
            if (zHeSoLe > 0)
                zKeeping.Class = "LE";//Lễ

            zKeeping.CreatedBy = SessionUser.UserLogin.Key;
            zKeeping.CreatedName = SessionUser.UserLogin.EmployeeName;
            zKeeping.ModifiedBy = SessionUser.UserLogin.Key;
            zKeeping.ModifiedName = SessionUser.UserLogin.EmployeeName;
            zKeeping.Save();

            string text = "--Dữ liệu chấm công chuẩn bị--" + Environment.NewLine;
            text += "Mã thẻ     :" + zKeeping.EmployeeID + Environment.NewLine;
            text += "Họ tên     :" + zKeeping.EmployeeName + Environment.NewLine;
            text += "Key        :" + zKeeping.Key + Environment.NewLine;
            text += "Ngày       :" + SheetDate + Environment.NewLine;
            text += "Key NV     :" + zKeeping.EmployeeKey + Environment.NewLine;
            text += "Key nhóm   :" + zKeeping.TeamKey + Environment.NewLine;
            text += "Mã nhóm    :" + zKeeping.TeamID + Environment.NewLine;
            text += "Tên nhóm   :" + zKeeping.TeamName + Environment.NewLine;
            text += "Giờ  BĐ    :" + zKeeping.BeginTime + Environment.NewLine;
            text += "Giờ KT     :" + zKeeping.EndTime + Environment.NewLine;
            text += "Giờ nghỉ   :" + zKeeping.OffTime + Environment.NewLine;
            text += "Tăng ca    :" + zKeeping.OverTime + Environment.NewLine;
            text += "Tổng giờ   :" + zKeeping.TotalTime + Environment.NewLine;
            text += "Key công   :" + zKeeping.TimeKey + Environment.NewLine;
            text += "Mã công    :" + zKeeping.TimeID + Environment.NewLine;
            text += "Tên công   :" + zKeeping.TimeName + Environment.NewLine;
            text += "Mã cơm cộng:" + zKeeping.RicePlus + Environment.NewLine;
            text += "Mã cơm trừ :" + zKeeping.RiceMinus + Environment.NewLine;
            text += "Qua đêm từ :" + zKeeping.OverTimeBegin + Environment.NewLine;
            text += "Qua đêm đến:" + zKeeping.OverTimeEnd + Environment.NewLine;
            text += "1:Có/2:Không qua đêm:" + zKeeping.Slug + Environment.NewLine;
            AddLog_Detail(text);

            string zError = "";
            string zMesage = "";
            if (zKeeping.Message.Substring(0, 2) == "11" ||
                zKeeping.Message.Substring(0, 2) == "20")
            {
                AddLog_Detail("--Lưu bảng chấm công thành công--");
                AddLog_Detail("--Xóa bảng chấm cơm cộng-cơm trừ--");
                //Xóa bảng con chấm công
                Employee_Rice_Info zEmployeeRice;
                zEmployeeRice = new Employee_Rice_Info();
                zEmployeeRice.Delete(zKeeping.EmployeeID, zKeeping.DateWrite);
                if (zEmployeeRice.Message == "30")
                    AddLog_Detail("--Xóa bảng chấm cơm cộng-cơm trừ thành công --");
                else
                    AddLog_Detail("--Xóa bảng chấm cơm cộng-cơm trừ lỗi > chi tiết:" + zEmployeeRice.Message);

                //Thêm tiền cơm cộng 
                string[] s = zRow[10].ToString().Trim().Split(',');
                AddLog_Detail("--Số mã cơm cộng:" + s.Count());
                Time_Rice_Info zRice;
                if (s.Length > 0)
                {
                    for (int k = 0; k < s.Count(); k++)
                    {
                        if (s[k].Length > 0)
                        {
                            //2C,3C
                            int zNumber = s[k].Trim().Substring(0, 1).ToInt();
                            string zRiceID = s[k].Trim().Substring(1).ToString();

                            zRice = new Time_Rice_Info();
                            zRice.Get_Info(zRiceID);
                            if (zRice.Key != 0 && zRice.Type == 0)
                            {
                                zMesage = Create_Rice(zKeeping.DateWrite, zKeeping.Key, zKeeping.EmployeeKey, zKeeping.EmployeeID, zKeeping.EmployeeName, zRice.Key, zRice.RiceID, zRice.RiceName, zNumber, zRice.Money,
                                zKeeping.TeamKey, zKeeping.TeamName, 0);
                                if (zMesage != "")
                                {
                                    zError += zMesage;
                                }
                            }
                        }
                    }
                }
                //Thêm tiền cơm trừ 
                s = zRow[11].ToString().Trim().Split(',');
                AddLog_Detail("--Tới lưu mã cơm trừ");
                if (s.Length > 0)
                {
                    for (int k = 0; k < s.Count(); k++)
                    {
                        if (s[k].Length > 0)
                        {
                            //2C,3C
                            int zNumber = s[k].Trim().Substring(0, 1).ToInt();
                            string zRiceID = s[k].Trim().Substring(1).ToString();

                            zRice = new Time_Rice_Info();
                            zRice.Get_Info(zRiceID);
                            if (zRice.Key != 0 && zRice.Type == 1)
                            {
                                zMesage = Create_Rice(zKeeping.DateWrite, zKeeping.Key, zKeeping.EmployeeKey, zKeeping.EmployeeID, zKeeping.EmployeeName, zRice.Key, zRice.RiceID, zRice.RiceName, zNumber, zRice.Money,
                                    zKeeping.TeamKey, zKeeping.TeamName, 1);
                                if (zMesage != "")
                                {
                                    zError += zMesage;
                                }
                            }
                        }
                    }
                }
                return zError;
            }
            else
            {
                return zError += zKeeping.Message;
            }

        }
        string Create_Rice(DateTime DateWrite, int KeepingKey, string EmployeeKey, string EmployeeID, string EmployeeName,
        int RiceKey, string RiceID, string RiceName, int Number, float Money, int TeamKey, string TeamName, int Slug)// Slug =0 là phí cộng, 1 là phí trừ
        {
            string zMessage = "";
            Employee_Rice_Info zEmployeeRice = new Employee_Rice_Info();
            zEmployeeRice.DateWrite = DateWrite;
            zEmployeeRice.KeepingKey = KeepingKey;
            zEmployeeRice.TeamKey = TeamKey;
            zEmployeeRice.TeamName = TeamName;
            zEmployeeRice.EmployeeKey = EmployeeKey;
            zEmployeeRice.EmployeeID = EmployeeID;
            zEmployeeRice.EmployeeName = EmployeeName;
            zEmployeeRice.RiceKey = RiceKey;
            zEmployeeRice.RiceID = RiceID;
            zEmployeeRice.RiceName = RiceName;
            zEmployeeRice.Number = Number;
            zEmployeeRice.Price = Money;
            zEmployeeRice.Money = Math.Round((Number * Money), 0);
            zEmployeeRice.Slug = Slug;
            zEmployeeRice.CreatedBy = SessionUser.UserLogin.Key;
            zEmployeeRice.CreatedName = SessionUser.UserLogin.EmployeeName;
            zEmployeeRice.ModifiedBy = SessionUser.UserLogin.Key;
            zEmployeeRice.ModifiedName = SessionUser.UserLogin.EmployeeName;
            zEmployeeRice.Create();

            string text = Environment.NewLine;
            text += "Ngày          :" + DateWrite + Environment.NewLine;
            text += "Key công      :" + KeepingKey + Environment.NewLine;
            text += "Key nhóm      :" + TeamKey + Environment.NewLine;
            text += "Tên nhóm      :" + TeamName + Environment.NewLine;
            text += "Key NV        :" + EmployeeKey + Environment.NewLine;
            text += "Mã thẻ        :" + EmployeeID + Environment.NewLine;
            text += "Tên NV        :" + EmployeeName + Environment.NewLine;
            text += "Key cơm       :" + RiceKey + Environment.NewLine;
            text += "Mã cơm        :" + RiceID + Environment.NewLine;
            text += "Tên cơm       :" + RiceName + Environment.NewLine;
            text += "Số phần       :" + Number + Environment.NewLine;
            text += "Đơn giá       :" + Money + Environment.NewLine;
            text += "Thành tiền    :" + Math.Round((Number * Money), 0) + Environment.NewLine;
            text += "0:cộng/1 trừ  :" + Slug + Environment.NewLine;

            AddLog_Detail(text);
            if (zEmployeeRice.Message.Substring(0, 2) != "11" &&
                        zEmployeeRice.Message.Substring(0, 2) != "20")
            {
                zMessage += zEmployeeRice.Message;
                AddLog_Detail("Lưu mã cơm[" + RiceID + "] >Lỗi >Chi tiết:" + zMessage);
            }
            else
            {
                AddLog_Detail("Lưu mã cơm[" + RiceID + "] >Thành công");
            }
            return zMessage;
        }
        void ShowDataGV(int TeamKey, DateTime DateView)
        {
            DataTable zTable = Employee_KeepingTime_Data.ListDetail(TeamKey, DateView);
            LoadDataGV(zTable);
        }
        void ExportTableError(DateTime SheetDate)
        {
            string Path = @"C:\Loi_Excel_Chua_Import\";
            string FileName = SheetDate.ToString("yyyy_MM_dd") + "_Chưa_Import.xlsx";
            if (_TableErr.Rows.Count > 0)
            {

                if (!Directory.Exists(Path))
                {
                    Directory.CreateDirectory(Path);
                }
                ExportTableToExcel(_TableErr, Path, FileName);
                _txtLog += "Xuất file Excel ngày [ " + SheetDate.ToString("dd") + " ] - " + _TableErr.Rows.Count + " lỗi" + Environment.NewLine;
            }
            else
            {
                //if (Directory.Exists(Path))
                //{
                //Directory.Delete(Path);
                //}
                var newFile = new FileInfo(Path + FileName);
                if (newFile.Exists)
                {
                    if (!IsFileLocked(newFile))
                    {
                        newFile.Delete();
                    }
                    else
                    {
                        _txtLog += "Tệp tin " + FileName + " đang sử dụng không thể xóa! " + Environment.NewLine;
                    }
                }

            }
        }

        //-------------------------------------Main process SQL data
        int SaveDataGridView()
        {
            int zError = 0;
            for (int i = 0; i < GVData.Rows.Count - 1; i++)
            {
                DataGridViewRow zRow = GVData.Rows[i];
                zRow.DefaultCellStyle.ForeColor = Color.Navy;
                if (zRow.Cells["Message"].Tag.ToInt() == 1)
                {
                    string Message = CheckGridviewRow(zRow);
                    if (Message == string.Empty)
                    {
                        Message = UpdateGridviewRecord(zRow);
                        if (Message != string.Empty)
                        {
                            zError++;
                        }
                    }
                }
            }

            return zError;
        }
        int CheckBeforeSaveDataGridView()
        {
            int zError = 0;
            for (int i = 0; i < GVData.Rows.Count - 1; i++)
            {
                DataGridViewRow zRow = GVData.Rows[i];
                zRow.DefaultCellStyle.ForeColor = Color.Navy;
                if (zRow.Cells["Message"].Tag == null)
                {
                    string Message = CheckGridviewRow(zRow);
                    if (Message != "")
                    {
                        zError++;
                    }
                }

            }

            return zError;
        }
        string CheckGridviewRow(DataGridViewRow zRow)
        {
            string zMessage = "";

            zRow.DefaultCellStyle.ForeColor = Color.Navy;
            //string zEmployeeName = zRow.Cells["EmployeeName"].Value.ToString();
            string zEmployeeID = "";
            if (zRow.Cells["EmployeeID"].Value != null)
            {
                zEmployeeID = zRow.Cells["EmployeeID"].Value.ToString();
            }
            string zTeamID = "";
            if (zRow.Cells["TeamID"].Value != null)
            {
                zTeamID = zRow.Cells["TeamID"].Value.ToString();
            }

            string zOverTimeBegin = "";
            if (zRow.Cells["OverTimeBegin"].Value != null)
            {
                zOverTimeBegin = zRow.Cells["OverTimeBegin"].Value.ToString();
            }
            string zOverTimeEnd = "";
            if (zRow.Cells["OverTimeEnd"].Value != null)
            {
                zOverTimeBegin = zRow.Cells["OverTimeEnd"].Value.ToString();
            }

            Employee_Info zEmployee = new Employee_Info();
            zEmployee.GetEmployeeID(zEmployeeID);
            if (zEmployeeID == "")
            {
                zMessage += "\n Stt:" + (zRow.Cells["No"].Value) + ";Lỗi!Chưa nhập mã thẻ";
            }
            else if (zEmployee.Key == "")
            {
                zMessage += "\n Stt:" + (zRow.Cells["No"].Value) + ";Lỗi!Mã thẻ [ " + zEmployeeID + " ]không tồn tại";
            }
            else
            {
                if (zEmployee.TeamKey == 0)
                    zMessage += "\n Stt:" + (zRow.Cells["No"].Value) + ";Lỗi!Mã thẻ [ " + zEmployeeID + " ]chưa thuộc nhóm nào";
            }

            Team_Info zTeam = new Team_Info();
            zTeam.Get_Team_ID(zTeamID);
            if (zTeam.Key == 0)
            {
                zMessage += "\n Stt:" + (zRow.Cells["No"].Value) + ";Lỗi!Mã nhóm [ " + zTeamID + " ]không tồn tại";


            }
            if (zRow.Cells["Time"].Value == null || zRow.Cells["Time"].Value.ToString() == "")
            {
                zMessage += "\n Stt:" + (zRow.Cells["No"].Value) + ";Lỗi!Mã công không thể bỏ trống";
            }
            else
            {
                Time_Defines_Info zDefine = new Time_Defines_Info();
                zDefine.Get_Info(zRow.Cells["Time"].Value.ToString().Trim());
                if (zDefine.Key == 0)
                {
                    zMessage += "\n Stt:" + (zRow.Cells["No"].Value) + ";Lỗi!Mã công [ " + zRow.Cells["Time"].Value.ToString() + " ]không tồn tại";
                }

            }
            if (zRow.Cells["Rice_Plus"].Value != null && zRow.Cells["Rice_Plus"].Value.ToString() != "")
            {
                string[] s = zRow.Cells["Rice_Plus"].Value.ToString().Split(',');
                int zLoi = 0;
                for (int k = 0; k < s.Count(); k++)
                {
                    Time_Rice_Info zRice = new Time_Rice_Info();
                    zRice.Get_Info(s[k].Trim());
                    if (zRice.Key == 0 || zRice.Slug == 1)
                    {
                        zLoi++;
                    }
                }
                if (zLoi > 0)
                {
                    zMessage += "\n Stt:" + (zRow.Cells["No"].Value) + ";Lỗi!Mã cơm [ " + zRow.Cells["Rice_Plus"].Value.ToString() + " ]không tồn tại";
                }
            }
            if (zRow.Cells["Rice_Minus"].Value != null && zRow.Cells["Rice_Minus"].Value.ToString() != "")
            {
                Time_Rice_Info zRice = new Time_Rice_Info();
                zRice.Get_Info(zRow.Cells["Rice_Minus"].Value.ToString());
                if (zRice.Key == 0 || zRice.Slug == 0)
                {
                    zMessage += "\n Stt:" + (zRow.Cells["No"].Value) + ";Mã phép trừ [ " + zRow.Cells["Rice_Minus"].Value.ToString() + " ]không tồn tại";
                }
            }
            if (zOverTimeBegin != "")
            {
                DateTime OverTimeBegin; ;
                if (!DateTime.TryParse(zOverTimeBegin, out OverTimeBegin))
                {
                    zMessage += "\n Stt:" + (zRow.Cells["No"].Value) + ";Lỗi!Ngày qua đêm từ [ " + zOverTimeBegin + " ]không đúng định dạng";
                }
            }
            if (zOverTimeEnd != "")
            {
                DateTime OverTimeEnd;
                if (!DateTime.TryParse(zOverTimeEnd, out OverTimeEnd))
                {
                    zMessage += "\n Stt:" + (zRow.Cells["No"].Value) + ";Lỗi!Ngày qua đêm đến [ " + zOverTimeBegin + " ]không đúng định dạng";
                }
            }
            if (zMessage == "")
            {
                zRow.Cells["Message"].Tag = 1;
                zRow.Cells["Message"].Value = "";
            }
            else
            {
                zRow.Cells["Message"].Tag = null;
                zRow.DefaultCellStyle.ForeColor = Color.Red;
                zRow.Cells["Message"].Value = zMessage;
            }

            return zMessage;
        }
        string UpdateGridviewRecord(DataGridViewRow zRow)
        {

            string zEmployeeID = zRow.Cells["EmployeeID"].Value.ToString().Trim();
            Employee_KeepingTime_Info zKeeping = new Employee_KeepingTime_Info(zRow.Cells["No"].Tag.ToInt());
            zKeeping.DateWrite = _ExcelDate;

            Employee_Info zEmployee = new Employee_Info();
            zEmployee.GetEmployeeID(zEmployeeID);
            zKeeping.EmployeeKey = zEmployee.Key;
            zKeeping.EmployeeID = zEmployeeID;
            zKeeping.EmployeeName = zRow.Cells["EmployeeName"].Value.ToString().Trim();

            //string zTeamID = zRow.Cells["TeamID"].Value.ToString();
            int zTeamKeyOption = Team_Employee_Month.OptionTeamKey(zEmployee.Key, _ExcelDate);
            Team_Info zTeam = new Team_Info(zTeamKeyOption);
            //zTeam.Get_Team_ID(zTeamID);
            zKeeping.TeamKey = zTeam.Key;
            zKeeping.TeamID = zTeam.TeamID;
            zKeeping.TeamName = zTeam.TeamName;


            if (zRow.Cells["BeginTime"].Value != null && zRow.Cells["BeginTime"].Value.ToString().Trim() != "")
            {

                zKeeping.BeginTime = zRow.Cells["BeginTime"].Value.ToString().Trim();
                zKeeping.BeginHour = int.Parse(zRow.Cells["BeginTime"].Value.ToString().Substring(0, 2));
                zKeeping.BeginMinute = int.Parse(zRow.Cells["BeginTime"].Value.ToString().Substring(3, 2));
            }
            else
            {
                zKeeping.BeginTime = "00:00";
            }

            if (zRow.Cells["EndTime"].Value != null && zRow.Cells["EndTime"].Value.ToString() != "")
            {
                zKeeping.EndTime = zRow.Cells["EndTime"].Value.ToString();
                zKeeping.EndHour = int.Parse(zRow.Cells["EndTime"].Value.ToString().Substring(0, 2));
                zKeeping.EndMinute = int.Parse(zRow.Cells["EndTime"].Value.ToString().Substring(3, 2));
            }
            else
            {
                zKeeping.EndTime = "00:00";
            }
            if (zRow.Cells["OffTime"].Value != null && zRow.Cells["OffTime"].Value.ToString() != "")
            {
                zKeeping.OffTime = zRow.Cells["OffTime"].Value.ToString();
            }
            else
            {
                zKeeping.OffTime = "00:00";
            }
            if (zRow.Cells["OverTime"].Value != null && zRow.Cells["OverTime"].Value.ToString() != "")
            {
                zKeeping.OverTime = zRow.Cells["OverTime"].Value.ToString();
            }
            else
            {
                zKeeping.OverTime = "00:00";
            }

            if (zRow.Cells["TotalTime"].Value != null && zRow.Cells["TotalTime"].Value.ToString() != "")
            {
                zKeeping.TotalTime = zRow.Cells["TotalTime"].Value.ToString();
            }
            else
            {
                zKeeping.TotalTime = "00:00";
            }


            if (zRow.Cells["Time"].Value != null && zRow.Cells["Time"].Value.ToString() != "")
            {
                Time_Defines_Info zDefine = new Time_Defines_Info();
                zDefine.Get_Info(zRow.Cells["Time"].Value.ToString());
                zKeeping.TimeKey = zDefine.Key;
                zKeeping.TimeID = zRow.Cells["Time"].Value.ToString();
                zKeeping.TimeName = zDefine.CodeName;
            }

            if (zRow.Cells["Rice_Plus"].Value != null && zRow.Cells["Rice_Plus"].Value.ToString() != "")
                zKeeping.RicePlus = zRow.Cells["Rice_Plus"].Value.ToString();
            else
            {
                zKeeping.RicePlus = "";
            }
            if (zRow.Cells["Rice_Minus"].Value != null && zRow.Cells["Rice_Minus"].Value.ToString().Trim() != "")
                zKeeping.RiceMinus = zRow.Cells["Rice_Minus"].Value.ToString().Trim();
            else
                zKeeping.RiceMinus = "";

            if (zRow.Cells["OverTimeBegin"].Value == null || zRow.Cells["OverTimeBegin"].Value.ToString().Trim() == "")
            {
                zKeeping.OverTimeBegin = DateTime.MinValue;
            }
            else
            {
                zKeeping.OverTimeBegin = DateTime.Parse(zRow.Cells["OverTimeBegin"].Value.ToString().Trim());
            }
            if (zRow.Cells["OverTimeEnd"].Value == null || zRow.Cells["OverTimeEnd"].Value.ToString().Trim() == "")
            {
                zKeeping.OverTimeEnd = DateTime.MinValue;
            }
            else
            {
                zKeeping.OverTimeEnd = DateTime.Parse(zRow.Cells["OverTimeEnd"].Value.ToString().Trim());
            }

            if (zKeeping.OverTimeBegin != DateTime.MinValue && zKeeping.OverTimeEnd != DateTime.MinValue)
            {
                zKeeping.Slug = 1;
            }
            else
            {
                zKeeping.Slug = 0;
            }

            float zHeSoLe = HoTroSanXuat.LayHeSoNgayLe(_ExcelDate); //Lấy hệ số lễ

            zKeeping.Class = "TH";//Ngày thường

            if (_ExcelDate.DayOfWeek == DayOfWeek.Sunday) //chủ nhật
                zKeeping.Class = "CN";//Chủ nhật
            if (zHeSoLe > 0)
                zKeeping.Class = "LE";//Lễ


            zKeeping.CreatedBy = SessionUser.UserLogin.Key;
            zKeeping.CreatedName = SessionUser.UserLogin.EmployeeName;
            zKeeping.ModifiedBy = SessionUser.UserLogin.Key;
            zKeeping.ModifiedName = SessionUser.UserLogin.EmployeeName;
            zKeeping.Save();
            string zError = "";
            string zMesage = "";
            if (zKeeping.Message.Substring(0, 2) == "11" ||
                zKeeping.Message.Substring(0, 2) == "20")
            {
                //Xóa bảng con chấm công
                Employee_Rice_Info zEmployeeRice;
                zEmployeeRice = new Employee_Rice_Info();
                zEmployeeRice.Delete(zKeeping.EmployeeID, zKeeping.DateWrite);
                //Thêm tiền cơm cộng
                if (zRow.Cells["Rice_Plus"].Value != null)
                {
                    string[] s = zRow.Cells["Rice_Plus"].Value.ToString().Trim().Split(',');
                    if (s.Length > 0)
                    {
                        for (int k = 0; k < s.Count(); k++)
                        {
                            int zNumber = s[k].Trim().Substring(0, 1).ToInt();
                            string zRiceID = s[k].Trim().Substring(1).ToString();

                            Time_Rice_Info zRice = new Time_Rice_Info();
                            zRice.Get_Info(zRiceID);
                            //  zRice
                            if (zRice.Key != 0 && zRice.Slug == 0)
                            {
                                zMesage = Create_Rice(zKeeping.DateWrite, zKeeping.Key, zKeeping.EmployeeKey, zKeeping.EmployeeID, zKeeping.EmployeeName, zRice.Key, zRice.RiceID, zRice.RiceName, zNumber, zRice.Money,
                                zKeeping.TeamKey, zKeeping.TeamName, 0);
                                if (zMesage != "")
                                {
                                    zError += zMesage;
                                }
                            }
                        }
                    }
                }
                //Thêm tiền cơm trừ
                if (zRow.Cells["Rice_Minus"].Value != null)
                {
                    string[] s = zRow.Cells["Rice_Minus"].Value.ToString().Trim().Split(',');
                    if (s.Length > 0)
                    {
                        for (int k = 0; k < s.Count(); k++)
                        {
                            int zNumber = s[k].Trim().Substring(0, 1).ToInt();
                            string zRiceID = s[k].Trim().Substring(1).ToString();

                            Time_Rice_Info zRice = new Time_Rice_Info();
                            zRice.Get_Info(zRiceID);
                            if (zRice.Key != 0 && zRice.Slug == 1)
                            {
                                zMesage = Create_Rice(zKeeping.DateWrite, zKeeping.Key, zKeeping.EmployeeKey, zKeeping.EmployeeID, zKeeping.EmployeeName, zRice.Key, zRice.RiceID, zRice.RiceName, zNumber, zRice.Money,
                                zKeeping.TeamKey, zKeeping.TeamName, 1);
                                if (zMesage != "")
                                {
                                    zError += zMesage;
                                }
                            }
                        }
                    }
                }
                return zError;
            }
            else
            {
                zRow.DefaultCellStyle.ForeColor = Color.Maroon;
                zRow.Cells["Message"].Value = zKeeping.Message;
                return zError += zKeeping.Message;
            }
        }
        void DeleteGridviewRecord(DataGridViewRow zRow)
        {
            GVData.Rows.Remove(zRow);
        }

        void Calculator(DataGridViewRow Row)
        {
            //    string Giobatdau = "0.0";
            //    string Gioketthuc = "0.0";
            //    string Gionghi = "0.0";
            //    string Giokhac = "0.0";
            //    float tonggio = 0;
            //    float sogiodu = 0;

            //    if (Row.Cells["BeginTime"].Tag != null)
            //    {
            //        Giobatdau = Row.Cells["BeginTime"].Tag.ToString();
            //        Row.Cells["BeginTime"].Value = GetTime(Giobatdau);
            //    }
            //    if (Row.Cells["EndTime"].Tag != null)
            //    {
            //        Gioketthuc = Row.Cells["EndTime"].Tag.ToString();
            //        Row.Cells["EndTime"].Value = GetTime(Gioketthuc);
            //    }
            //    if (Row.Cells["OffTime"].Tag != null)
            //    {
            //        Gionghi = Row.Cells["OffTime"].Tag.ToString();
            //        Row.Cells["OffTime"].Value = GetTime(Gionghi);
            //    }
            //    if (Row.Cells["DifferentTime"].Tag != null)
            //    {
            //        Giokhac = Row.Cells["DifferentTime"].Tag.ToString();
            //        Row.Cells["DifferentTime"].Value = GetTime(Giokhac);
            //    }

            //    if (Giobatdau != "0.0" || Gioketthuc != "0.0")
            //    {
            //        tonggio = float.Parse(Gioketthuc) - float.Parse(Giobatdau) - float.Parse(Gionghi) - float.Parse(Giokhac);
            //        sogiodu = 0;

            //        Row.Cells["TotalTime"].Value = GetTime(tonggio.ToString("n2"));

            //        string[] s = tonggio.ToString("n2").Split('.');
            //        if (tonggio > 8 && s[1].ToInt() < 95) //Nếu  giờ dư >=0.95 thì lam tron thanh 1
            //        {
            //            sogiodu = tonggio - 8;
            //            Row.Cells["OverTime"].Value = GetTime(sogiodu.ToString());
            //        }
            //        else if (tonggio > 8 && s[1].ToInt() >= 95)
            //        {
            //            sogiodu = tonggio - 8;
            //            Row.Cells["OverTime"].Value = GetTime(sogiodu.ToString("n2"));
            //        }
            //        else
            //        {
            //            Row.Cells["OverTime"].Value = 0;
            //            Row.Cells["Money"].Value = 0;
            //        }
            //    }
            //    else // trong hợp chỉ nhập tổng giờ
            //    {
            //        if (Row.Cells["TotalTime"].Tag != null)
            //        {
            //            tonggio = Row.Cells["TotalTime"].Tag.ToString().ToFloat();
            //            Row.Cells["TotalTime"].Value = GetTime(tonggio.ToString("n2"));
            //            string[] s = tonggio.ToString("n2").Split('.');
            //            if (tonggio > 8 && s[1].ToInt() < 95) //Nếu  giờ dư >=0.95 thì lam tron thanh 1
            //            {
            //                sogiodu = tonggio - 8;
            //                Row.Cells["OverTime"].Value = GetTime(sogiodu.ToString());
            //            }
            //            else if (tonggio > 8 && s[1].ToInt() >= 95)
            //            {
            //                sogiodu = tonggio - 8;
            //                Row.Cells["OverTime"].Value = GetTime(sogiodu.ToString("n2"));
            //            }
            //            else
            //            {
            //                Row.Cells["OverTime"].Value = 0;
            //                Row.Cells["Money"].Value = 0;
            //            }
            //        }
            //    }
        }


        void AddLog_Detail(string Text)
        {
            Invoke(new MethodInvoker(delegate
            {
                txt_Log.Items.Add(DateTime.Now.ToString("HH:mm:ss.fff") + " : " + Text);
                txt_Log.SelectedIndex = txt_Log.Items.Count - 1;
            }));

            _Log_Detail += DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss.fff") + " " + Text + Environment.NewLine;
            //txt_Log_Detail.ScrollToCaret();
        }
        void AddLogShort(string Text)
        {
            Invoke(new MethodInvoker(delegate
            {
                txt_LogShort.Items.Add(Text);
                txt_LogShort.SelectedIndex = txt_LogShort.Items.Count - 1;
            }));

            _txtLog += Text + Environment.NewLine;
        }

        void Save_LogCaculator_Import(string ActionName)
        {
            Application_Info zinfo = new Application_Info();
            zinfo.ActionName = ActionName;
            zinfo.DateLog = _DateLog;
            zinfo.ActionType = 3;
            zinfo.SubDescription = _txtLog;//Log tung ngay
            zinfo.Description = _Log_Detail;//Log chi tiet tung nhan vien 
            zinfo.Form = 5;//Dành cho tính lương công nhân.
            zinfo.FormName = "Form " + HeaderControl.Text;
            zinfo.UserKey = SessionUser.UserLogin.Key;
            zinfo.EmployeeKey = SessionUser.UserLogin.EmployeeKey;
            zinfo.CreatedBy = SessionUser.UserLogin.Key;
            zinfo.CreatedName = SessionUser.UserLogin.EmployeeName;
            zinfo.ModifiedBy = SessionUser.UserLogin.Key;
            zinfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
            zinfo.Create();
        }
    }
}
