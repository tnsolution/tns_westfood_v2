﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using TNS.CORE;
using TNS.HRM;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Production_Money : Form
    {
        private Salary_Product_Worker_Info zSalary;

        #region [Khai Báo Biến]
        private int _TeamKey = 0;
        private int _Count = 0;
        private DateTime _Date;
        private DataTable _Table;
        private string _Message = "";
        // ---- Time
        private float _TimeTeam = 0;
        private float _TimePrivateTeam = 0;
        private float _TimeTeamPrivate = 0;
        private float _TimeTeamGeneral = 0;
        private float _TimeDifPrivate = 0;
        private float _TimeDifGeneral = 0;
        // ---- Money
        /// <summary>
        /// Lương Tại Tổ
        /// </summary>
        private double _MoneyTeam = 0;
        /// <summary>
        /// Lương Tại Tổ Ăn Riêng
        /// </summary>        
        private double _MoneyTeamPrivate = 0;
        /// <summary>
        /// Lương Tổ Khác Ăn Riêng
        /// </summary>
        private double _MoneyPrivateTeam = 0;
        /// <summary>
        /// Lương Tổ Khác Ăn Chung
        /// </summary>
        private double _MoneyGeneralTeam = 0;
        /// <summary>
        /// Lương Việc Khác Ăn Riêng
        /// </summary>
        private double _MoneyDifPrivate = 0;
        /// <summary>
        /// Lương Việc Khác Ăn Chung
        /// </summary>
        private double _MoneyDifGeneral = 0;

        // ToTal Money , Time
        /// <summary>
        /// Tổng Thời Gian Làm Tại Tổ
        /// </summary>
        private float _ToTalTimeTeam = 0;
        /// <summary>
        /// Tổng Thời Gian Làm Việc Khác Ăn Chung
        /// </summary>
        private float _ToTalTimeDifGeneral = 0;
        /// <summary>
        /// Tổng Thời Gian Làm Việc Tổ Khác Ăn Chung
        /// </summary>
        private float _ToTalTimeTeamGeneral = 0;
        /// <summary>
        /// Tổng Tiền Việc Khác Ăn Chung
        /// </summary>
        private double _ToTalMoneyDifGeneral = 0;
        /// <summary>
        /// Tổng Tiền Làm Tại Tổ
        /// </summary>
        private double _ToTalMoneyTeam = 0;

        // Medium Time 
        /// <summary>
        /// Thời Gian Trung Bình Làm Việc Khác Ăn Chung
        /// </summary>
        private float _MedTimeDifGeneral = 0;

        /// <summary>
        /// Hiển thị số tiền đã tính  
        /// </summary>
        private double _ToTal = 0;
        #endregion
        public Frm_Production_Money()
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            Utils.DoubleBuffered(GVData1, true);
            Utils.DoubleBuffered(GVData2, true);
            Utils.DrawGVStyle(ref GVData1);
            Utils.DrawGVStyle(ref GVData2);
            Utils.DrawLVStyle(ref LVData1);
            Utils.DrawLVStyle(ref LVData2);

            InitLayout_LV1(LVData1);
            InitLayout_LV2(LVData2);
            InitLayout_GV1();
            InitLayout_GV2();

            //---- List View
            LVData1.Click += LVData1_Click;          
            LVData2.Click += LVData2_Click;
            //---- DataGridView

            //---- Button 
            timer1.Tick += Timer1_Tick;

            btn_Count_Date.Click += Btn_Count_Date_Click;
            btn_Save_Date.Click += Btn_Save_Date_Click;        
            btn_Count_Month.Click += Btn_Count_Month_Click;

            //btn_Hide_Log.Visible = false;
            //btn_Show_Log.Visible = true;
            //groupBox6.Visible = false;
            //btn_Hide_Log.Click += Btn_Hide_Click;
            //btn_Show_Log.Click += Btn_Show_Log_Click;

            Utils.SizeLastColumn_LV(LVData1);
            Utils.SizeLastColumn_LV(LVData2);
        }

        private void Btn_Show_Log_Click(object sender, EventArgs e)
        {
            //groupBox6.Visible = true;
            //btn_Hide_Log.Visible = true;
            //btn_Show_Log.Visible = false;
        }
        private void Btn_Hide_Click(object sender, EventArgs e)
        {
            //groupBox6.Visible = false;
            //btn_Hide_Log.Visible = false;
            //btn_Show_Log.Visible = true;
        }

        private void Frm_Money_Productivity_Load(object sender, EventArgs e)
        {
            dte_Teams.Value = SessionUser.Date_Work;
            dte_Month.Value = SessionUser.Date_Work;
            InitData_LV1();
            InitData_LV2();
            btn_Save_Date.Visible = false;
            _Date = dte_Month.Value;
            timer1.Interval = 500;

            this.Bounds = Screen.PrimaryScreen.WorkingArea;
        }

        #region [TỔNG HỢP LƯƠNG NGÀY]               

        #region [Panel Left]

        //---- Tạo ListView
        private void InitLayout_LV1(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "No";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên Nhóm";
            colHead.Width = 350;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);


        }
        //---- LoadListView
        public void InitData_LV1()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LVData1;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            DataTable In_Table = new DataTable();

            In_Table = Teams_Data.List();

            LV.Items.Clear();
            int n = In_Table.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.ForeColor = Color.Navy;
                DataRow nRow = In_Table.Rows[i];
                lvi.Tag = nRow["TeamKey"].ToString();
                lvi.BackColor = Color.White;

                lvi.ImageIndex = 0;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["TeamName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);

            }

            this.Cursor = Cursors.Default;

        }
        //---- Tạo Sự Kiện List View
        private void LVData1_Click(object sender, EventArgs e)
        {
            txtTitle.Text = "TÍNH LƯƠNG NĂNG SUẤT NGÀY " + dte_Teams.Value.ToString("dd/MM/yyyy");
            for (int i = 0; i < LVData1.Items.Count; i++)
            {
                if (LVData1.Items[i].Selected == true)
                {
                    LVData1.Items[i].BackColor = Color.LightBlue; // highlighted item
                }
                else
                {
                    LVData1.Items[i].BackColor = SystemColors.Window; // normal item
                }
            }
            _TeamKey = LVData1.SelectedItems[0].Tag.ToInt();
            LoadDataGirdView();
            btn_Count_Date.Visible = true;
            btn_Save_Date.Visible = false;
        }

        #endregion

        #region [Panel Right]
        //---- Tạo DataGridView
        private void InitLayout_GV1()
        {
            DataGridViewCheckBoxColumn zColumn;
            // Setup Column 
            GVData1.Columns.Add("No", "STT");
            GVData1.Columns.Add("EmployeeName", "Tên Nhân Viên");
            GVData1.Columns.Add("EmployeeID", "Mã NV");
            GVData1.Columns.Add("TimeDoTeam", "TG Làm Tại Tổ");
            GVData1.Columns.Add("TimeDoTeamPrivate", "TG Làm Tại Tổ (Ăn Riêng");
            GVData1.Columns.Add("TimePrivateTeamDif", "TG Tổ Khác (Ăn Riêng)");
            GVData1.Columns.Add("TimeGeneralTeamDif", "TG Tổ Khác (Ăn Chung)");
            GVData1.Columns.Add("TimeDifPrivate", "TG Việc Khác (Ăn Riêng)");
            GVData1.Columns.Add("TimeDifGeneral", "TG Việc Khác (Ăn Chung)");
            GVData1.Columns.Add("MoneyTeam", "Lương Tại Tổ");
            GVData1.Columns.Add("MoneyTeamPrivate", "Lương Tại Tổ (Ăn Riêng)");
            GVData1.Columns.Add("MoneyPrivateTeam", "Lương Tổ Khác (Ăn Riêng)");
            GVData1.Columns.Add("MoneyGeneralTeam", "Lương Tổ Khác (Ăn Chung)");
            GVData1.Columns.Add("MoneyDifPrivate", "Lương TG (Ăn Riêng)");
            GVData1.Columns.Add("MoneyDifGeneral", "Lương TG (Ăn Chung)");
            GVData1.Columns.Add("ToTal", "Lương Ngày");

            zColumn = new DataGridViewCheckBoxColumn();
            zColumn.Name = "Befor15days";
            zColumn.HeaderText = "15 ngày đầu";
            GVData1.Columns.Add(zColumn);


            zColumn = new DataGridViewCheckBoxColumn();
            zColumn.Name = "After15days";
            zColumn.HeaderText = "15 ngày sau";
            GVData1.Columns.Add(zColumn);

            GVData1.Columns["No"].Width = 60;
            GVData1.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GVData1.Columns["No"].ReadOnly = true;

            GVData1.Columns["EmployeeName"].Width = 170;
            GVData1.Columns["EmployeeName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GVData1.Columns["EmployeeName"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GVData1.Columns["EmployeeName"].ReadOnly = true;

            GVData1.Columns["EmployeeID"].Width = 70;
            GVData1.Columns["EmployeeID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GVData1.Columns["EmployeeID"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GVData1.Columns["EmployeeID"].ReadOnly = true;
            GVData1.Columns["EmployeeID"].Frozen = true;

            GVData1.Columns["TimeDoTeam"].Width = 95;
            GVData1.Columns["TimeDoTeam"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GVData1.Columns["TimeDoTeam"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GVData1.Columns["TimeDoTeam"].ReadOnly = true;

            GVData1.Columns["TimeDoTeamPrivate"].Width = 95;
            GVData1.Columns["TimeDoTeamPrivate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GVData1.Columns["TimeDoTeamPrivate"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GVData1.Columns["TimeDoTeamPrivate"].ReadOnly = true;


            GVData1.Columns["TimePrivateTeamDif"].Width = 95;
            GVData1.Columns["TimePrivateTeamDif"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GVData1.Columns["TimePrivateTeamDif"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GVData1.Columns["TimePrivateTeamDif"].ReadOnly = true;

            GVData1.Columns["TimeGeneralTeamDif"].Width = 95;
            GVData1.Columns["TimeGeneralTeamDif"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GVData1.Columns["TimeGeneralTeamDif"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GVData1.Columns["TimeGeneralTeamDif"].ReadOnly = true;

            GVData1.Columns["TimeDifPrivate"].Width = 95;
            GVData1.Columns["TimeDifPrivate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GVData1.Columns["TimeDifPrivate"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GVData1.Columns["TimeDifPrivate"].ReadOnly = true;

            GVData1.Columns["TimeDifGeneral"].Width = 95;
            GVData1.Columns["TimeDifGeneral"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GVData1.Columns["TimeDifGeneral"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GVData1.Columns["TimeDifGeneral"].ReadOnly = true;


            GVData1.Columns["MoneyTeam"].Width = 95;
            GVData1.Columns["MoneyTeam"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GVData1.Columns["MoneyTeam"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GVData1.Columns["MoneyTeam"].ReadOnly = true;

            GVData1.Columns["MoneyTeamPrivate"].Width = 95;
            GVData1.Columns["MoneyTeamPrivate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GVData1.Columns["MoneyTeamPrivate"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GVData1.Columns["MoneyTeamPrivate"].ReadOnly = true;

            GVData1.Columns["MoneyPrivateTeam"].Width = 110;
            GVData1.Columns["MoneyPrivateTeam"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GVData1.Columns["MoneyPrivateTeam"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GVData1.Columns["MoneyPrivateTeam"].ReadOnly = true;

            GVData1.Columns["MoneyGeneralTeam"].Width = 110;
            GVData1.Columns["MoneyGeneralTeam"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GVData1.Columns["MoneyGeneralTeam"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GVData1.Columns["MoneyGeneralTeam"].ReadOnly = true;

            GVData1.Columns["MoneyDifPrivate"].Width = 90;
            GVData1.Columns["MoneyDifPrivate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GVData1.Columns["MoneyDifPrivate"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GVData1.Columns["MoneyDifPrivate"].ReadOnly = true;

            GVData1.Columns["MoneyDifGeneral"].Width = 90;
            GVData1.Columns["MoneyDifGeneral"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GVData1.Columns["MoneyDifGeneral"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GVData1.Columns["MoneyDifGeneral"].ReadOnly = true;

            GVData1.Columns["ToTal"].Width = 90;
            GVData1.Columns["ToTal"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GVData1.Columns["ToTal"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GVData1.Columns["ToTal"].ReadOnly = true;


            GVData1.Columns["Befor15days"].Width = 95;
            GVData1.Columns["Befor15days"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GVData1.Columns["Befor15days"].ReadOnly = true;

            GVData1.Columns["After15days"].Width = 95;
            GVData1.Columns["After15days"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GVData1.Columns["After15days"].ReadOnly = true;

            GVData1.ColumnHeadersHeight = 52;
        }
        //---- Load Data In DataGridView 
        private void LoadDataGirdView()
        {
            _ToTalTimeTeam = 0;
            _ToTalTimeDifGeneral = 0;
            _ToTalMoneyTeam = 0;
            _ToTalMoneyDifGeneral = 0;
            _ToTal = 0;
            GVData1.Rows.Clear();
            _Table = Order_Money_Data.List_Team(_TeamKey, dte_Teams.Value);
            int i = 0;
            foreach (DataRow nRow in _Table.Rows)
            {
                GVData1.Rows.Add();
                DataGridViewRow nRowView = GVData1.Rows[i];
                nRowView.Cells["No"].Value = (i + 1).ToString();
                nRowView.Cells["EmployeeName"].Tag = nRow["EmployeeKey"].ToString().Trim();
                nRowView.Cells["EmployeeName"].Value = nRow["EmployeeName"].ToString().Trim();
                nRowView.Cells["EmployeeID"].Value = nRow["EmployeeID"].ToString().Trim();

                #region [Thời Gian]
                if (nRow["TGTaiTo"].ToString().Trim() != "")
                {
                    _TimeTeam = nRow["TGTaiTo"].ToFloat();
                    _ToTalTimeTeam += _TimeTeam;
                    nRowView.Cells["TimeDoTeam"].Value = _TimeTeam.ToString();
                }
                else
                {
                    _TimeTeam = 0;
                    nRowView.Cells["TimeDoTeam"].Value = _TimeTeam.ToString();
                }
                if (nRow["TGTaiToAnRieng"].ToString().Trim() != "")
                {
                    _TimePrivateTeam = nRow["TGTaiToAnRieng"].ToFloat();
                    nRowView.Cells["TimeDoTeamPrivate"].Value = _TimePrivateTeam.ToString();
                }
                else
                {
                    _TimePrivateTeam = 0;
                    nRowView.Cells["TimeDoTeamPrivate"].Value = _TimePrivateTeam.ToString();
                }
                if (nRow["TGMuonAnRieng"].ToString().Trim() != "")
                {
                    _TimeTeamPrivate = nRow["TGMuonAnRieng"].ToFloat();
                    nRowView.Cells["TimePrivateTeamDif"].Value = _TimeTeamPrivate;
                }
                else
                {
                    _TimeTeamPrivate = 0;
                    nRowView.Cells["TimePrivateTeamDif"].Value = _TimeTeamPrivate;
                }
                if (nRow["TGMuonAnChung"].ToString().Trim() != "")
                {
                    _TimeTeamGeneral = nRow["TGMuonAnChung"].ToFloat();
                    _ToTalTimeTeamGeneral += _TimeTeamGeneral;
                    nRowView.Cells["TimeGeneralTeamDif"].Value = _TimeTeamGeneral;
                }
                else
                {
                    _TimeTeamGeneral = 0;
                    nRowView.Cells["TimeGeneralTeamDif"].Value = _TimeTeamGeneral;
                }
                if (nRow["TGKhacMuonAnChung"].ToString().Trim() != "")
                {
                    _TimeDifGeneral = nRow["TGKhacMuonAnChung"].ToFloat();
                    _ToTalTimeDifGeneral += _TimeDifGeneral;
                    nRowView.Cells["TimeDifGeneral"].Value = _TimeDifGeneral;
                }
                else
                {
                    _TimeDifGeneral = 0;
                    nRowView.Cells["TimeDifGeneral"].Value = _TimeDifGeneral;
                }
                #endregion

                #region [Lương Năng Suất]
                if (nRow["TienTaiTo"].ToString().Trim() != "")
                {
                    _MoneyTeam = nRow["TienTaiTo"].ToDouble();
                    _ToTalMoneyTeam += _MoneyTeam;
                    nRowView.Cells["MoneyTeam"].Value = _MoneyTeam.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);
                }
                else
                {
                    _MoneyTeam = 0;
                    nRowView.Cells["MoneyTeam"].Value = _MoneyTeam.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);
                }

                if (nRow["MoneyPrivate"].ToString().Trim() != "")
                {
                    _MoneyTeamPrivate = nRow["MoneyPrivate"].ToDouble();
                    nRowView.Cells["MoneyTeamPrivate"].Value = _MoneyTeamPrivate.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);
                }
                else
                {
                    _MoneyTeamPrivate = 0;
                    nRowView.Cells["MoneyTeamPrivate"].Value = _MoneyTeamPrivate.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);
                }

                if (nRow["TienToKhacRieng"].ToString().Trim() != "")
                {
                    _MoneyPrivateTeam = nRow["TienToKhacRieng"].ToDouble();
                    nRowView.Cells["MoneyPrivateTeam"].Value = _MoneyPrivateTeam.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);
                }
                else
                {
                    _MoneyPrivateTeam = 0;
                    nRowView.Cells["MoneyPrivateTeam"].Value = _MoneyPrivateTeam.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);
                }

                if (nRow["TienToKhacChung"].ToString().Trim() != "")
                {
                    _MoneyGeneralTeam = nRow["TienToKhacChung"].ToDouble();
                    nRowView.Cells["MoneyGeneralTeam"].Value = _MoneyGeneralTeam.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);
                }
                else
                {
                    _MoneyGeneralTeam = 0;
                    nRowView.Cells["MoneyGeneralTeam"].Value = _MoneyGeneralTeam.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);
                }

                if (nRow["MoneyDifPrivate"].ToString().Trim() != "")
                {
                    _MoneyDifPrivate = nRow["MoneyDifPrivate"].ToDouble();
                    nRowView.Cells["MoneyDifPrivate"].Value = _MoneyDifPrivate.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);
                }
                else
                {
                    _MoneyDifPrivate = 0;
                    nRowView.Cells["MoneyDifPrivate"].Value = _MoneyDifPrivate.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);
                }

                if (nRow["MoneyDifGeneral"].ToString().Trim() != "")
                {
                    _MoneyDifGeneral = nRow["MoneyDifGeneral"].ToDouble();
                    _ToTalMoneyDifGeneral += _MoneyDifGeneral;
                    nRowView.Cells["MoneyDifGeneral"].Value = _MoneyDifGeneral.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);
                }
                else
                {
                    _MoneyDifGeneral = 0;
                    nRowView.Cells["MoneyDifGeneral"].Value = _MoneyDifGeneral.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);
                }

                if (nRow["TongTien"].ToString().Trim() != "")
                {
                    _ToTal = nRow["TongTien"].ToDouble();
                    nRowView.Cells["ToTal"].Value = _ToTal.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);
                }
                else
                {
                    _ToTal = 0;
                    nRowView.Cells["ToTal"].Value = _ToTal.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);
                }
                #endregion


                if (nRow["Slug"].ToString() == "0")
                {
                    nRowView.Cells["Befor15days"].Value = true;
                }
                if (nRow["Slug"].ToString() == "1")
                {
                    nRowView.Cells["After15days"].Value = true;
                }
                if (nRow["Slug"].ToString() == "")
                {
                    nRowView.Cells["Befor15days"].Value = false;
                    nRowView.Cells["After15days"].Value = false;
                }
                i++;
            }
            //Hiễn Thị Thời Gian Của Tổ
            txt_ToTal_Time.Text = _ToTalTimeTeam.ToString("n0");

            //Hiển Thị Thời Gian Làm Việc Khác
            txt_ToTalTimeDifGeneral.Text = _ToTalTimeDifGeneral.ToString();
            _MedTimeDifGeneral = (float)Math.Round((_ToTalTimeDifGeneral / 8), 1);
            txt_MedTimeDifGeneral.Text = _MedTimeDifGeneral.ToString();
            txt_ToTalDifGeneral.Text = _ToTalMoneyDifGeneral.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);
            // Hiển Thị Tổng Tiền Nhóm Làm Tại Tổ
            txt_ToTal_Money.Text = _ToTalMoneyTeam.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);

        }
        //---- Process Data
        private void TienThoiGianViec()
        {
            float zTienLamViecKhac = float.Parse(txt_ToTalDifGeneral.Text);
            float zTimeToTeam = float.Parse(txt_ToTal_Time.Text);
            double zTienTB = zTienLamViecKhac / zTimeToTeam;
            for (int i = 0; i < GVData1.Rows.Count - 1; i++)
            {
                float zTGLamViec = float.Parse(GVData1.Rows[i].Cells["TimeDoTeam"].Value.ToString());

                double zTien = zTienTB * zTGLamViec;
                if (float.Parse(GVData1.Rows[i].Cells["TimeDoTeam"].Value.ToString()) != 0)
                {

                    GVData1.Rows[i].Cells["MoneyDifGeneral"].Value = zTien.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);
                }
            }
        }

        private void TienToKhacMuonAnChung()
        {
            double zMoneyShare = 0;
            double zTBMoneyShare = 0;
            for (int i = 0; i < GVData1.Rows.Count - 1; i++)
            {
                zMoneyShare += double.Parse(GVData1.Rows[i].Cells["MoneyGeneralTeam"].Value.ToString());
            }
            float zToTalTime = float.Parse(txt_ToTal_Time.Text);
            if (zMoneyShare > 0 && zToTalTime > 0)
                zTBMoneyShare = (zMoneyShare / zToTalTime);
            for (int i = 0; i < GVData1.Rows.Count - 1; i++)
            {
                float zTime = float.Parse(GVData1.Rows[i].Cells["TimeDoTeam"].Value.ToString());
                GVData1.Rows[i].Cells["MoneyGeneralTeam"].Value = (zTBMoneyShare * zTime).ToString("n0");
            }
        }
        private void TinhTongTien()
        {
            double zMoneyTeam = 0;
            double zMoneyTeamPrivate = 0;
            double zMoneyPrivateTeam = 0;
            double zMoneyGeneralTeam = 0;
            double zMoneyTeamDifPrivate = 0;
            double zMoneyTeamDifGeneral = 0;
            double zToTalMoneyTeam = 0;

            for (int i = 0; i < GVData1.Rows.Count - 1; i++)
            {
                if (GVData1.Rows[i].Cells["MoneyTeam"].Value != null && GVData1.Rows[i].Cells["MoneyTeam"].Value.ToString() != "")
                    zMoneyTeam = double.Parse(GVData1.Rows[i].Cells["MoneyTeam"].Value.ToString());
                if (GVData1.Rows[i].Cells["MoneyTeamPrivate"].Value != null && GVData1.Rows[i].Cells["MoneyTeamPrivate"].Value.ToString() != "")
                    zMoneyTeamPrivate = double.Parse(GVData1.Rows[i].Cells["MoneyTeamPrivate"].Value.ToString());
                if (GVData1.Rows[i].Cells["MoneyPrivateTeam"].Value != null && GVData1.Rows[i].Cells["MoneyPrivateTeam"].Value.ToString() != "")
                    zMoneyPrivateTeam = double.Parse(GVData1.Rows[i].Cells["MoneyPrivateTeam"].Value.ToString());
                if (GVData1.Rows[i].Cells["MoneyGeneralTeam"].Value != null && GVData1.Rows[i].Cells["MoneyGeneralTeam"].Value.ToString() != "")
                    zMoneyGeneralTeam = double.Parse(GVData1.Rows[i].Cells["MoneyGeneralTeam"].Value.ToString());
                if (GVData1.Rows[i].Cells["MoneyDifPrivate"].Value != null && GVData1.Rows[i].Cells["MoneyDifPrivate"].Value.ToString() != "")
                    zMoneyTeamDifPrivate = double.Parse(GVData1.Rows[i].Cells["MoneyDifPrivate"].Value.ToString());
                if (GVData1.Rows[i].Cells["MoneyDifGeneral"].Value != null && GVData1.Rows[i].Cells["MoneyDifGeneral"].Value.ToString() != "")
                    zMoneyTeamDifGeneral = double.Parse(GVData1.Rows[i].Cells["MoneyDifGeneral"].Value.ToString());

                zToTalMoneyTeam = zMoneyTeam + zMoneyTeamPrivate + zMoneyPrivateTeam + zMoneyGeneralTeam + zMoneyTeamDifPrivate + zMoneyTeamDifGeneral;
                GVData1.Rows[i].Cells["ToTal"].Value = zToTalMoneyTeam.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);
            }
        }
        private void CountMoney()
        {
            #region Tiền Làm Việc Khác Mượn Ăn Chung
            TienThoiGianViec();
            #endregion

            #region Tiền Tổ Khác Mượn Ăn Chung
            //TienToKhacMuonAnChung();
            #endregion

            #region [Tinh Tiền Tổng Một Ngày]
            TinhTongTien();
            #endregion
        }
        private void Save_All()
        {
            double zKhoanSPTaiTo = 0;
            double zKhoanSPToKhacAnRieg = 0;
            double zKhoanSPToKhacAnChung = 0;
            double zKhoanTGAnRieng = 0;
            double zKhoanTGAnChung = 0;
            double zTongTien = 0;
            int zFind_Date = 0;
            for (int i = 0; i < GVData1.Rows.Count - 1; i++)
            {
                Order_Money_Date_Info zMoneyDate = new Order_Money_Date_Info();
                zMoneyDate.EmployeeKey = GVData1.Rows[i].Cells["EmployeeName"].Tag.ToString();
                zMoneyDate.EmployeeID = GVData1.Rows[i].Cells["EmployeeID"].Value.ToString();
                zMoneyDate.EmployeeName = GVData1.Rows[i].Cells["EmployeeName"].Value.ToString();
                zMoneyDate.TeamKey = _TeamKey;
                zMoneyDate.Date = dte_Teams.Value;
                zKhoanSPTaiTo = double.Parse(GVData1.Rows[i].Cells["MoneyTeam"].Value.ToString());
                zKhoanSPToKhacAnRieg = double.Parse(GVData1.Rows[i].Cells["MoneyPrivateTeam"].Value.ToString());
                zKhoanSPToKhacAnChung = double.Parse(GVData1.Rows[i].Cells["MoneyGeneralTeam"].Value.ToString());
                zKhoanTGAnChung = double.Parse(GVData1.Rows[i].Cells["MoneyDifPrivate"].Value.ToString());
                zKhoanTGAnRieng = double.Parse(GVData1.Rows[i].Cells["MoneyDifGeneral"].Value.ToString());
                zTongTien = double.Parse(GVData1.Rows[i].Cells["ToTal"].Value.ToString());
                zFind_Date = Caculater_Productivity_Data.Find_Date(dte_Teams.Value);
                if (zFind_Date > 0)
                {
                    zMoneyDate.Money = 0;
                    zMoneyDate.Money_Sundays = 0;
                    if (GVData1.Rows[i].Cells["ToTal"].Value.ToString() != "")
                        zMoneyDate.Money_Holidays = zKhoanSPTaiTo;
                    else
                        zMoneyDate.Money_Holidays = 0;

                    zMoneyDate.Money_Dif = 0;
                    zMoneyDate.Money_Dif_Holiday = zKhoanSPToKhacAnRieg + zKhoanSPToKhacAnChung + zKhoanTGAnChung + zKhoanTGAnRieng;
                    zMoneyDate.Money_Dif_Sunday = 0;
                    zMoneyDate.Total = zTongTien;
                }
                if (zFind_Date == 0 && dte_Teams.Value.DayOfWeek != DayOfWeek.Sunday)
                {
                    if (GVData1.Rows[i].Cells["ToTal"].Value.ToString() != "")
                        zMoneyDate.Money = zKhoanSPTaiTo;
                    else
                        zMoneyDate.Money = 0;
                    zMoneyDate.Money_Sundays = 0;
                    zMoneyDate.Money_Holidays = 0;
                    zMoneyDate.Money_Dif = zKhoanSPToKhacAnRieg + zKhoanSPToKhacAnChung + zKhoanTGAnChung + zKhoanTGAnRieng;
                    zMoneyDate.Money_Dif_Holiday = 0;
                    zMoneyDate.Money_Dif_Sunday = 0;
                    zMoneyDate.Total = zTongTien;
                }
                if (dte_Teams.Value.DayOfWeek == DayOfWeek.Sunday)
                {
                    zMoneyDate.Money = 0;
                    if (GVData1.Rows[i].Cells["ToTal"].Value.ToString() != "")
                        zMoneyDate.Money_Sundays = zKhoanSPTaiTo;
                    else
                        zMoneyDate.Money_Sundays = 0;
                    zMoneyDate.Money_Holidays = 0;
                    zMoneyDate.Money_Dif = 0;
                    zMoneyDate.Money_Dif_Holiday = 0;
                    zMoneyDate.Money_Dif_Sunday = zKhoanSPToKhacAnRieg + zKhoanSPToKhacAnChung + zKhoanTGAnChung + zKhoanTGAnRieng;
                    zMoneyDate.Total = zTongTien;
                }
                _Count = Order_Money_Date_Data.Find_Employee(zMoneyDate.EmployeeKey, dte_Teams.Value);
                if (_Count == 0)
                {
                    zMoneyDate.Create();
                }
                else
                {
                    zMoneyDate.Update();
                }
                _Message += zMoneyDate.Message;
            }
            string zMessage = TN_Message.Show(_Message);
            if (zMessage.Length == 0)
            {
                MessageBox.Show("Câp nhật thành công", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Vui lòng kiểm tra lại", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        //---- Process Envent
        private void Btn_Count_Date_Click(object sender, EventArgs e)
        {
            CountMoney();
            btn_Count_Date.Visible = false;
            btn_Save_Date.Visible = true;
        }
        private void Btn_Save_Date_Click(object sender, EventArgs e)
        {
            Save_All();
            btn_Count_Date.Visible = true;
            btn_Save_Date.Visible = false;
        }
        #endregion

        #endregion

        #region [TỔNG HỢP LƯƠNG THÁNG]

        #region [Desgin Layout ListView]
        private void InitLayout_LV2(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "No";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên Nhóm";
            colHead.Width = 350;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);


        }
        public void InitData_LV2()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LVData2;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            DataTable In_Table = new DataTable();

            In_Table = Teams_Data.List();

            LV.Items.Clear();
            int n = In_Table.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.ForeColor = Color.Navy;
                DataRow nRow = In_Table.Rows[i];
                lvi.Tag = nRow["TeamKey"].ToString();
                lvi.BackColor = Color.White;

                lvi.ImageIndex = 0;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["TeamName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);

            }

            this.Cursor = Cursors.Default;

        }
        private void LVData2_Click(object sender, EventArgs e)
        {
            txtTitle.Text = "TÍNH LƯƠNG NĂNG SUẤT THÁNG " + dte_Month.Value.ToString("MM/yyyy");
            for (int i = 0; i < LVData2.Items.Count; i++)
            {
                if (LVData2.Items[i].Selected == true)
                {
                    LVData2.Items[i].BackColor = Color.LightBlue; // highlighted item
                }
                else
                {
                    LVData2.Items[i].BackColor = SystemColors.Window; // normal item
                }
            }
            _TeamKey = LVData2.SelectedItems[0].Tag.ToInt();
            GV_Team_Month_LoadData();
            btn_Count_Month.Visible = true;

            //LB_Log.Items.Clear();
        }
        #endregion

        #region [Design Layout DataGridView]
        private void InitLayout_GV2()
        {
            // Setup Column 
            GVData2.Columns.Add("No", "STT");
            GVData2.Columns.Add("EmployeeName", "Tên Nhân Viên");
            GVData2.Columns.Add("CardID", "Mã NV");
            GVData2.Columns.Add("Money", "Lương Ngày Thường");
            GVData2.Columns.Add("Money_Holiday", "Lương Ngày Lễ");
            GVData2.Columns.Add("Money_Sunday", "Lương Ngày Chủ Nhật");
            GVData2.Columns.Add("Money_Dif", "Lương Khác Ngày Thường");
            GVData2.Columns.Add("Money_Dif_Sunday", "Lương Khác Ngày Chủ Nhật");
            GVData2.Columns.Add("Money_Dif_Holiday", "Lương Việc Ngày Ngày Lễ");
            GVData2.Columns.Add("Money_Order", "Lương Khác");
            GVData2.Columns.Add("Money_TimeKeep", "Lương Ngày Công");
            GVData2.Columns.Add("ToTal", "Tổng Tiền");

            GVData2.Columns["No"].Width = 60;
            GVData2.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GVData2.Columns["No"].ReadOnly = true;

            GVData2.Columns["EmployeeName"].Width = 170;
            GVData2.Columns["EmployeeName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GVData2.Columns["EmployeeName"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GVData2.Columns["EmployeeName"].ReadOnly = true;

            GVData2.Columns["CardID"].Width = 70;
            GVData2.Columns["CardID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GVData2.Columns["CardID"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GVData2.Columns["CardID"].ReadOnly = true;
            GVData2.Columns["CardID"].Frozen = true;

            GVData2.Columns["Money"].Width = 95;
            GVData2.Columns["Money"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GVData2.Columns["Money"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GVData2.Columns["Money_Holiday"].Width = 95;
            GVData2.Columns["Money_Holiday"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GVData2.Columns["Money_Holiday"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GVData2.Columns["Money_Sunday"].Width = 150;
            GVData2.Columns["Money_Sunday"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GVData2.Columns["Money_Dif"].Width = 130;
            GVData2.Columns["Money_Dif"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GVData2.Columns["Money_Dif_Sunday"].Width = 130;
            GVData2.Columns["Money_Dif_Sunday"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GVData2.Columns["Money_Dif_Holiday"].Width = 130;
            GVData2.Columns["Money_Dif_Holiday"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GVData2.Columns["Money_Order"].Width = 130;
            GVData2.Columns["Money_Order"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GVData2.Columns["Money_TimeKeep"].Width = 130;
            GVData2.Columns["Money_TimeKeep"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GVData2.Columns["ToTal"].Width = 130;
            GVData2.Columns["ToTal"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GVData2.Columns["ToTal"].ReadOnly = true;

            GVData2.ColumnHeadersHeight = 52;
        }
        private void GV_Team_Month_LoadData()
        {
            GVData2.Rows.Clear();
            _Table = Order_Money_Data.List_Team_Month(_TeamKey, dte_Month.Value);

            int i = 0;
            foreach (DataRow nRow in _Table.Rows)
            {
                GVData2.Rows.Add();
                DataGridViewRow nRowView = GVData2.Rows[i];
                nRowView.Cells["No"].Value = (i + 1).ToString();
                nRowView.Cells["EmployeeName"].Tag = nRow["EmployeeKey"].ToString().Trim();
                nRowView.Cells["EmployeeName"].Value = nRow["EmployeeName"].ToString().Trim();
                nRowView.Cells["CardID"].Value = nRow["EmployeeID"].ToString().Trim();
                float zMoney = float.Parse(nRow["Money"].ToString());
                nRowView.Cells["Money"].Value = zMoney.ToString("#,###,###");
                float zMoneyholiday = float.Parse(nRow["MoneyHoliday"].ToString());
                nRowView.Cells["Money_Holiday"].Value = zMoneyholiday.ToString("#,###,###");
                float zMoneSyunday = float.Parse(nRow["MoneySunday"].ToString().Trim());
                nRowView.Cells["MOney_Sunday"].Value = zMoneSyunday.ToString("#,###,###");
                float zMoneyDif = float.Parse(nRow["MoneyDif"].ToString());
                nRowView.Cells["Money_Dif"].Value = zMoneyDif.ToString("#,###,###");
                float zMoneyDifHoliday = float.Parse(nRow["MoneyDifSunday"].ToString());
                nRowView.Cells["Money_Dif_Sunday"].Value = zMoneyDifHoliday.ToString("#,###,###");
                float zMoneyDifSunday = float.Parse(nRow["MoneyDifHoliday"].ToString());
                nRowView.Cells["Money_Dif_Holiday"].Value = zMoneyDifSunday.ToString("#,###,###");
                float zMoneyTimeKeep = float.Parse(nRow["MoneyTimeKeep"].ToString());
                nRowView.Cells["Money_TimeKeep"].Value = zMoneyTimeKeep.ToString("#,###,###");
                i++;
            }

        }
        #endregion

        #region [Process Data]
        private void Message_System(string Message)
        {
            Invoke(new MethodInvoker(delegate
            {
                //LB_Log.Items.Add(DateTime.Now.ToString("dd-MM-yy hh:mm:ss") + " : " + Message);
            }));
        }
        private void Count_Month()
        {
            float zMoney = 0;
            float zMoneyHoliday = 0;
            float zMoneySunday = 0;
            float zMoneyDif = 0;
            float zMoneyDifHoliday = 0;
            float zMoneyDifSunday = 0;
            float zMoney_ToTal = 0;
            if (GVData2.Rows[_indexttable].Cells["CardID"].Value != null)
            {
                if (GVData2.Rows[_indexttable].Cells["Money"].Value.ToString() != "")
                    zMoney = float.Parse(GVData2.Rows[_indexttable].Cells["Money"].Value.ToString());
                else
                    zMoney = 0;

                if (GVData2.Rows[_indexttable].Cells["Money_Holiday"].Value.ToString() != "")
                    zMoneyHoliday = float.Parse(GVData2.Rows[_indexttable].Cells["Money_Holiday"].Value.ToString());
                else
                    zMoneyHoliday = 0;

                if (GVData2.Rows[_indexttable].Cells["Money_Sunday"].Value.ToString() != "")
                    zMoneySunday = float.Parse(GVData2.Rows[_indexttable].Cells["Money_Sunday"].Value.ToString());
                else
                    zMoneySunday = 0;

                if (GVData2.Rows[_indexttable].Cells["MOney_Dif"].Value.ToString() != "")
                    zMoneyDif = float.Parse(GVData2.Rows[_indexttable].Cells["MOney_Dif"].Value.ToString());
                else
                    zMoneyDif = 0;

                if (GVData2.Rows[_indexttable].Cells["Money_Dif_Holiday"].Value.ToString() != "")
                    zMoneyDifHoliday = float.Parse(GVData2.Rows[_indexttable].Cells["Money_Dif_Holiday"].Value.ToString());
                else
                    zMoneyDifSunday = 0;

                if (GVData2.Rows[_indexttable].Cells["Money_Dif_Sunday"].Value.ToString() != "")
                    zMoneyDifSunday = float.Parse(GVData2.Rows[_indexttable].Cells["Money_Dif_Sunday"].Value.ToString());
                else
                    zMoneyDifSunday = 0;

                float zToTal = zMoney + zMoneyHoliday + zMoneySunday + zMoneyDif + zMoneyDifHoliday + zMoneyDifSunday;
                if (zToTal > 0)
                    GVData2.Rows[_indexttable].Cells["Total"].Value = zToTal.ToString("#,###,###");
                else
                    GVData2.Rows[_indexttable].Cells["Total"].Value = "0";
                if (GVData2.Rows[_indexttable].Cells["Total"].Value.ToString() != "")
                {
                    Save();
                    Message_System("Đang xử lý " + GVData2.Rows[_indexttable].Cells["EmployeeName"].Value.ToString());
                    if (_Message == "11" || _Message == "20")
                    {
                        Message_System("Đã chuyển thành công");
                        Message_System("--------------------");
                        timer1.Start();
                    }
                    else
                    {
                        timer1.Stop();
                        //_FrmLoading.Close();
                    }
                }
            }
            if (GVData2.Rows[_indexttable].Cells["CardID"].Value == null)
            {
                MessageBox.Show("Đã xong");
                timer1.Stop();
                //_FrmLoading.Close();
                for (int i = 0; i < GVData2.Rows.Count; i++)
                {
                    if (GVData2.Rows[i].Cells["CardID"].Value != null)
                    {
                        float zMoneyToTal = float.Parse(GVData2.Rows[i].Cells["Total"].Value.ToString());
                        zMoney_ToTal += zMoneyToTal;
                    }
                }
                txt_Money_Month.Text = zMoney_ToTal.ToString("#,###,###");
            }

        }

        private void Save()
        {
            DateTime zDate = dte_Month.Value;
            DateTime zFromDate = new DateTime(zDate.Year, zDate.Month, 1);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day);

            zSalary = new Salary_Product_Worker_Info();
            zSalary.SalaryMonth = zToDate;
            zSalary.EmployeeKey = GVData2.Rows[_indexttable].Cells["EmployeeName"].Tag.ToString();
            zSalary.EmployeeID = GVData2.Rows[_indexttable].Cells["CardID"].Value.ToString();
            zSalary.EmployeeName = GVData2.Rows[_indexttable].Cells["EmployeeName"].Value.ToString();
            if (GVData2.Rows[_indexttable].Cells["Money"].Value.ToString() != "")
                zSalary.SalaryProduct = double.Parse(GVData2.Rows[_indexttable].Cells["Money"].Value.ToString());
            else
                zSalary.SalaryProduct = 0;
            if (GVData2.Rows[_indexttable].Cells["Money_Sunday"].Value.ToString() != "")
                zSalary.SalarySunday = double.Parse(GVData2.Rows[_indexttable].Cells["Money_Sunday"].Value.ToString());
            else
                zSalary.SalarySunday = 0;
            if (GVData2.Rows[_indexttable].Cells["Money_Holiday"].Value.ToString() != "")
                zSalary.SalaryHoliday = double.Parse(GVData2.Rows[_indexttable].Cells["Money_Holiday"].Value.ToString());
            else
                zSalary.SalaryHoliday = 0;
            if (GVData2.Rows[_indexttable].Cells["MOney_Dif"].Value.ToString() != "")
                zSalary.SalaryDif = double.Parse(GVData2.Rows[_indexttable].Cells["MOney_Dif"].Value.ToString());
            else
                zSalary.SalaryDif = 0;
            if (GVData2.Rows[_indexttable].Cells["Money_Dif_Sunday"].Value.ToString() != "")
                zSalary.SalaryDifSunday = double.Parse(GVData2.Rows[_indexttable].Cells["Money_Dif_Sunday"].Value.ToString());
            else
                zSalary.SalaryDifSunday = 0;
            if (GVData2.Rows[_indexttable].Cells["Money_Dif_Holiday"].Value.ToString() != "")
                zSalary.SalaryDifHoliday = double.Parse(GVData2.Rows[_indexttable].Cells["Money_Dif_Holiday"].Value.ToString());
            else
                zSalary.SalaryDifHoliday = 0;
            if (GVData2.Rows[_indexttable].Cells["Money_TimeKeep"].Value.ToString() != "")
                zSalary.MoneyTimeKeep = double.Parse(GVData2.Rows[_indexttable].Cells["Money_TimeKeep"].Value.ToString());
            else
                zSalary.MoneyTimeKeep = 0;
            int zCount = Salary_Product_Worker_Data.Find_Employee(zSalary.EmployeeKey, dte_Month.Value);
            if (zCount == 0)
            {
                zSalary.CreatedBy = SessionUser.UserLogin.Key;
                zSalary.CreatedName = SessionUser.Personal_Info.FullName;
                zSalary.Create();
                _Message = zSalary.Message;
            }
            if (zCount > 0)
            {
                zSalary.Update();
                _Message = zSalary.Message;
            }
        }

        #endregion

        #region [Process Event]
        private int _indexttable = 0;
        private void Btn_Count_Month_Click(object sender, EventArgs e)
        {
            timer1.Start();
            _indexttable = -1;
            //_FrmLoading.ShowDialog();
        }
        //Frm_Loading _FrmLoading = new Frm_Loading();
        private void Timer1_Tick(object sender, EventArgs e)
        {
            timer1.Stop();
            _indexttable++;
            Count_Month();
        }

        #endregion

        #endregion

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtTitle.Text = "Tổng hợp năng suất [" + tabControl1.SelectedTab.Text + "]";
        }
    }
}
