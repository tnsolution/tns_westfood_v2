﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.HRM;
using TN.Toolbox;
using TNS.CORE;
using TNS.Misc;
using TNS.SYS;
using TNS.LOG;

namespace TNS.WinApp
{
    public partial class Frm_AdjustV2 : Form
    {
        #region[ Thông tin đơn hàng đang chọn]
        int _TeamKey = 0;
        string _TeamID = "";
        string _TeamName = "";
        string _OrderKey = "";
        string _OrderID = "";
        string _OrderIDCompare = "";
        int _StageKey = 0;
        string _StageID = "";
        string _StageName = "";
        float _Price = 0;
        float _TotalTime = 0; //Tổng giờ do người dùng đã chỉnh sửa
        float _TotalKg = 0;//tổng kg đem về 1 cv
        float _TotalSoGio = 0;// Tổng số giờ đem về
        float _TotalBasket = 0; //Tổng số rổ đem về
        float _TotalMoney = 0; //Tổng tiền dem về của 1 cv
        #endregion

        private DataTable _TableRemark = new DataTable(); //Bảng tạm dùng để tính tất cả đơn hàng
        Order_Adjusted_Info _Info = new Order_Adjusted_Info();
        public Frm_AdjustV2()
        {
            InitializeComponent();
            this.DoubleBuffered = true;

            btnClose.Click += btnClose_Click;
            btnMax.Click += btnMax_Click;
            btnMini.Click += btnMini_Click;

            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            GV_Team.Click += GV_Team_Click;
            GV_Order.Click += GV_Order_Click;
            btn_SaveAll.Click += Btn_SaveAll_Click;
            btn_Save.Click += Btn_Save_Click;
            GV_Employee.CellEndEdit += GV_Employee_CellEndEdit;
            GV_Employee.KeyDown += GV_Employee_KeyDown;
            btn_Search.Click += Btn_Search_Click;
            btn_New.Click += Btn_New_Click;
        }



        private void Frm_Adjust_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);

            SetupLayoutGV_Team();
            SetupLayoutGV_Order();
            SetupLayoutGVEmployee();
            SetupLayoutGVEmployee_View();
            dte_Date.Value = SessionUser.Date_Work;
            DataTable ztable = Order_Adjusted_Data.List_TeamShare1(dte_Date.Value, dte_Date.Value);
            InitData_GVTeam(GV_Team, ztable);
        }

        #region [GV Team]
        private void SetupLayoutGV_Team()
        {
            GV_Column zCol;

            GV_Team.ColumnAdd("No", "Stt", 30);
            GV_Team.ColumnAdd("TeamID", "Mã nhóm", 80);
            GV_Team.ColumnAdd("TeamName", "Tên nhóm", 180);


            zCol = new GV_Column();
            zCol.IsSum = true;
            zCol.CheckEmpty = true;
            zCol.ColumnName = "TimeShare";
            zCol.HeaderText = "Số Giờ";
            zCol.Style = "Number";
            zCol.Width = 80;
            GV_Team.ColumnAdd(zCol);

            zCol = new GV_Column();
            zCol.IsSum = true;
            zCol.CheckEmpty = true;
            zCol.ColumnName = "BasketShare";
            zCol.HeaderText = "Số Rổ";
            zCol.Style = "Number";
            zCol.Width = 80;
            GV_Team.ColumnAdd(zCol);

            zCol = new GV_Column();
            zCol.IsSum = true;
            zCol.CheckEmpty = true;
            zCol.ColumnName = "KgShare";
            zCol.HeaderText = "Số kí";
            zCol.Style = "Number";
            zCol.Width = 80;
            GV_Team.ColumnAdd(zCol);

            zCol = new GV_Column();
            zCol.IsSum = true;
            zCol.CheckEmpty = true;
            zCol.ColumnName = "MoneyShare";
            zCol.HeaderText = "Số tiền";
            zCol.Width = 110;
            zCol.Style = "Number";

            GV_Team.ColumnAdd(zCol);

            //GV_Team.FillLastColumn = true;
            GV_Team.IsSum = true;
            GV_Team.AllowAddRow = false;
            GV_Team.Init();
            GV_Team.GetSum(false);
        }
        private void InitData_GVTeam(DataGridView GV, DataTable Table)
        {
            GV.Rows.Clear();
            if (Table != null)
            {
                int i = 0;
                foreach (DataRow r in Table.Rows)
                {

                    GV.Rows.Add();
                    DataGridViewRow GvRow = GV.Rows[i];
                    GvRow.Cells["No"].Value = (i + 1).ToString();
                    GvRow.Cells["TeamID"].Tag = r["TeamKey"].ToString();
                    GvRow.Cells["TeamID"].Value = r["TeamID"].ToString().Trim();
                    GvRow.Cells["TeamName"].Value = r["TeamName"].ToString();
                    float zTimeShare = 0;
                    if (float.TryParse(r["TimeShare"].ToString(), out zTimeShare))
                    {

                    }
                    GvRow.Cells["TimeShare"].Value = zTimeShare.ToString("n4").Trim();
                    float zBasketShare = 0;
                    if (float.TryParse(r["BasketShare"].ToString(), out zBasketShare))
                    {

                    }
                    GvRow.Cells["BasketShare"].Value = zBasketShare.ToString("n4").Trim();
                    float zKgShare = 0;
                    if (float.TryParse(r["KgShare"].ToString(), out zKgShare))
                    {

                    }
                    GvRow.Cells["KgShare"].Value = zKgShare.ToString("n4");
                    GvRow.Cells["MoneyShare"].Value = r["MoneyShare"].Ton0String().Trim();
                    i++;
                }
                GV_Team.GetSum(false);
                GV.ClearSelection();
            }
        }
        private void GV_Team_Click(object sender, EventArgs e)
        {
            if (GV_Team.CurrentRow.Cells["TeamID"].Tag != null)
            {
                //this.Cursor = Cursors.WaitCursor;
                _TeamKey = int.Parse(GV_Team.CurrentRow.Cells["TeamID"].Tag.ToString());
                _TeamID = GV_Team.CurrentRow.Cells["TeamID"].Value.ToString();
                _TeamName = GV_Team.CurrentRow.Cells["TeamName"].Value.ToString();

                DataTable zTableOrder = Order_Adjusted_Data.List_OrderShare1(dte_Date.Value, dte_Date.Value, _TeamKey);
                InitData_GVOrder(GV_Order, zTableOrder);

                GV_Employee.Rows.Clear();
                GV_EmployeeView.Rows.Clear();
                //this.Cursor = Cursors.Default;
            }
        }
        #endregion

        #region[GV Order]
        private void SetupLayoutGV_Order()
        {
            GV_Order.ColumnAdd("No", "Stt", 30);
            GV_Order.ColumnAdd("OrderIDCompare", "Mã ĐH đối chiếu", 130);
            GV_Order.ColumnAdd("OrderID", "Mã đơn hàng", 130);
            GV_Order.ColumnAdd("StageID", "Mã công việc", 100);
            GV_Order.ColumnAdd("StageName", "Tên công việc", 150);

            GV_Column zCol = new GV_Column();
            zCol.IsSum = true;
            zCol.CheckEmpty = true;
            zCol.ColumnName = "Price";
            zCol.HeaderText = "Đơn giá";
            zCol.Style = "Number";
            zCol.Width = 70;
            GV_Order.ColumnAdd(zCol);

            zCol = new GV_Column();
            zCol.IsSum = true;
            zCol.CheckEmpty = true;
            zCol.ColumnName = "TimeShare";
            zCol.HeaderText = "Số giờ";
            zCol.Style = "Number";
            zCol.Width = 70;
            GV_Order.ColumnAdd(zCol);

            zCol = new GV_Column();
            zCol.IsSum = true;
            zCol.CheckEmpty = true;
            zCol.ColumnName = "BasketShare";
            zCol.HeaderText = "Số rổ";
            zCol.Style = "Number";
            zCol.Width = 70;
            GV_Order.ColumnAdd(zCol);

            zCol = new GV_Column();
            zCol.IsSum = true;
            zCol.CheckEmpty = true;
            zCol.ColumnName = "KgShare";
            zCol.HeaderText = "Số kí";
            zCol.Style = "Number";
            zCol.Width = 70;
            GV_Order.ColumnAdd(zCol);

            zCol = new GV_Column();
            zCol.IsSum = true;
            zCol.CheckEmpty = true;
            zCol.ColumnName = "MoneyShare";
            zCol.HeaderText = "Số tiền";
            zCol.Style = "Number";
            zCol.Width = 110;
            GV_Order.ColumnAdd(zCol);

            GV_Order.IsSum = true;
            GV_Order.Init();

        }
        private void InitData_GVOrder(DataGridView GV, DataTable Table)
        {
            GV.Rows.Clear();
            if (Table != null)
            {
                for (int i = 0; i < Table.Rows.Count; i++)
                {
                    DataRow r = Table.Rows[i];
                    GV.Rows.Add();
                    DataGridViewRow GvRow = GV.Rows[i];
                    GvRow.Cells["No"].Value = (i + 1).ToString();
                    GvRow.Cells["OrderID"].Tag = r["OrderKey"].ToString();
                    GvRow.Cells["OrderIDCompare"].Value = r["OrderID_Compare"].ToString();
                    GvRow.Cells["OrderID"].Value = r["OrderID"].ToString();
                    GvRow.Cells["StageID"].Tag = r["StageKey"].ToString();
                    GvRow.Cells["StageID"].Value = r["StageID"].ToString();
                    GvRow.Cells["StageName"].Value = r["StageName"].ToString();
                    GvRow.Cells["Price"].Value = r["Price"].Ton2String();
                    float zTimeShare = 0;
                    if(float.TryParse(r["TimeShare"].ToString(),out zTimeShare))
                    {

                    }
                    GvRow.Cells["TimeShare"].Value = zTimeShare.ToString("n4");
                    float zBasketShare = 0;
                    if (float.TryParse(r["BasketShare"].ToString(), out zBasketShare))
                    {

                    }
                    GvRow.Cells["BasketShare"].Value = zBasketShare.ToString("n4");
                    float zKgShare = 0;
                    if (float.TryParse(r["KgShare"].ToString(), out zKgShare))
                    {

                    }
                    GvRow.Cells["KgShare"].Value = zKgShare.ToString("n4");
                    GvRow.Cells["MoneyShare"].Value = r["MoneyShare"].Ton0String();
                }
                GV_Order.GetSum(false);
                GV.ClearSelection();
            }
        }
        private void GV_Order_Click(object sender, EventArgs e)
        {
            if (GV_Order.CurrentRow.Cells["OrderID"].Tag != null)
            {
                this.Cursor = Cursors.WaitCursor;
                _OrderKey = GV_Order.CurrentRow.Cells["OrderID"].Tag.ToString();
                _OrderID = GV_Order.CurrentRow.Cells["OrderID"].Value.ToString();
                _OrderIDCompare = GV_Order.CurrentRow.Cells["OrderIDCompare"].Value.ToString();
                _StageKey = int.Parse(GV_Order.CurrentRow.Cells["StageID"].Tag.ToString());
                _StageID = GV_Order.CurrentRow.Cells["StageID"].Value.ToString();
                _StageName = GV_Order.CurrentRow.Cells["StageName"].Value.ToString();
                _Price = float.Parse(GV_Order.CurrentRow.Cells["Price"].Value.ToString());
                _TotalSoGio = float.Parse(GV_Order.CurrentRow.Cells["TimeShare"].Value.ToString());
                _TotalBasket = float.Parse(GV_Order.CurrentRow.Cells["BasketShare"].Value.ToString());
                _TotalKg = float.Parse(GV_Order.CurrentRow.Cells["KgShare"].Value.ToString());
                _TotalMoney = float.Parse(GV_Order.CurrentRow.Cells["MoneyShare"].Value.ToString());

                GV_Employee.Rows.Clear();
                GV_EmployeeView.Rows.Clear();
                DataTable zTableView = Order_Adjusted_Data.List_Employee1(_OrderKey, _TeamKey);
                if (zTableView.Rows.Count > 0)
                {
                    InitData_GVEmployee_View(GV_EmployeeView, zTableView);

                }
                else
                {
                    DataTable zTableOrder = Order_Adjusted_Data.List_EmployeeShare1(_OrderKey, _TeamKey, dte_Date.Value, dte_Date.Value);
                    InitData_GVEmployee(GV_Employee, zTableOrder);
                }

                this.Cursor = Cursors.Default;
            }
        }
        #endregion

        #region[GV Employee]
        private void SetupLayoutGVEmployee()
        {
            DataTable zTable = Employee_Data.Load_Data("EmployeeKey, EmployeeID, FullName");
            string[] zHeaderLV = new string[] { "Mã NV", "Tên" };

            GV_Employee.ColumnAdd("No", "STT", 40);

            GV_Column zCol = new GV_Column("EmployeeID", "ListView", 70, 400);
            zCol.TableSource = zTable;
            zCol.FilterColumn = true;
            zCol.CheckEmpty = true;
            zCol.IsPrimaryID = true;
            zCol.ShowColumn = 1;
            zCol.DisplayNextColumn = "FullName";
            zCol.HeaderText = "Mã NV";
            zCol.InitColumnLV(zHeaderLV);
            zCol.LoadDataToLV();
            GV_Employee.ColumnAdd(zCol);

            GV_Employee.ColumnAdd("FullName", "Họ tên", 150);

            zCol = new GV_Column();
            zCol.IsSum = true;
            zCol.CheckEmpty = true;
            zCol.ColumnName = "Time";
            zCol.HeaderText = "Số Giờ";
            zCol.Style = "Number";
            zCol.Width = 70;
            GV_Employee.ColumnAdd(zCol);

            zCol = new GV_Column();
            zCol.ColumnName = "Borrow";
            zCol.HeaderText = "Mượn";
            zCol.Style = "CheckBox";
            zCol.Width = 60;
            GV_Employee.ColumnAdd(zCol);

            zCol = new GV_Column();
            zCol.ColumnName = "Private";
            zCol.HeaderText = "Ăn Riêng";
            zCol.Style = "CheckBox";
            zCol.Width = 60;
            GV_Employee.ColumnAdd(zCol);

            zCol = new GV_Column();
            zCol.ColumnName = "Share";
            zCol.HeaderText = "Ăn Chung";
            zCol.Style = "CheckBox";
            zCol.Width = 60;
            GV_Employee.ColumnAdd(zCol);

            zCol = new GV_Column();
            zCol.IsSum = true;
            zCol.CheckEmpty = true;
            zCol.ColumnName = "SoGio";
            zCol.HeaderText = "Số giờ đem về";
            zCol.Style = "Number";
            zCol.Width = 100;
            GV_Employee.ColumnAdd(zCol);

            zCol = new GV_Column();
            zCol.IsSum = true;
            zCol.CheckEmpty = true;
            zCol.ColumnName = "Basket";
            zCol.HeaderText = "Số rổ đêm về";
            zCol.Style = "Number";
            zCol.Width = 90;
            GV_Employee.ColumnAdd(zCol);


            zCol = new GV_Column();
            zCol.IsSum = true;
            zCol.CheckEmpty = true;
            zCol.ColumnName = "Kg";
            zCol.HeaderText = "Số kí đem về";
            zCol.Style = "Number";
            zCol.Width = 100;
            GV_Employee.ColumnAdd(zCol);


            //zCol = new GV_Column();
            //zCol.IsSum = true;
            //zCol.CheckEmpty = true;
            //zCol.ColumnName = "KgProduct";
            //zCol.HeaderText = "Số kí chia lại";
            //zCol.Style = "Number";
            //zCol.Width = 100;
            //GV_Employee.ColumnAdd(zCol);

            zCol = new GV_Column();
            zCol.IsSum = true;
            zCol.CheckEmpty = true;
            zCol.ColumnName = "Money";
            zCol.HeaderText = "Tiền đem về";
            zCol.Style = "Number";
            zCol.Width = 100;
            GV_Employee.ColumnAdd(zCol);

            //zCol = new GV_Column();
            //zCol.IsSum = true;
            //zCol.CheckEmpty = true;
            //zCol.ColumnName = "MoneyProduct";
            //zCol.HeaderText = "Số tiền chia lại";
            //zCol.Style = "Number";
            //zCol.Width = 100;
            //GV_Employee.ColumnAdd(zCol);

            GV_Employee.FillLastColumn = false;
            GV_Employee.IsSum = true;
            GV_Employee.AllowAddRow = true;
            GV_Employee.Init();
            GV_Employee.GetSum(false);
        }
        private void InitData_GVEmployee(DataGridView GV, DataTable Table)
        {
            int i = 0;
            GV.Rows.Clear();
            foreach (DataRow nRow in Table.Rows)
            {

                GV.Rows.Add();
                DataGridViewRow nRowView = GV.Rows[i];
                nRowView.Cells["No"].Value = (i + 1).ToString();
                nRowView.Cells["EmployeeID"].Tag = nRow["EmployeeKey"].ToString().Trim();
                nRowView.Cells["EmployeeID"].Value = nRow["EmployeeID"].ToString().Trim();
                nRowView.Cells["FullName"].Value = nRow["EmployeeName"].ToString().Trim();
                if (nRow["Time"].ToString().Trim() != "")
                {
                    float zTime = 0;
                    if (float.TryParse(nRow["Time"].ToString(), out zTime))
                    {

                    }
                    nRowView.Cells["Time"].Value = zTime.ToString("n2");
                }
                else
                {
                    nRowView.Cells["Time"].Value = 0;
                }
                if (nRow["SoGio"].ToString() != "")
                {
                    float zSoGio = float.Parse(nRow["SoGio"].ToString().Trim());
                    nRowView.Cells["SoGio"].Value = zSoGio.ToString("n4");
                }
                else
                {
                    nRowView.Cells["SoGio"].Value = 0;
                }
                if (nRow["Basket"].ToString() != "")
                {
                    float zBasket = float.Parse(nRow["Basket"].ToString().Trim());
                    nRowView.Cells["Basket"].Value = zBasket.ToString("n4");
                }
                else
                {
                    nRowView.Cells["Basket"].Value = 0;
                }
                if (nRow["Kg"].ToString() != "")
                {
                    float zSoLuong = float.Parse(nRow["Kg"].ToString().Trim());
                    nRowView.Cells["Kg"].Value = zSoLuong.ToString("n4");
                    if (zSoLuong != 0)
                        nRowView.DefaultCellStyle.BackColor = Color.LightBlue;
                }
                else
                {
                    nRowView.Cells["Kg"].Value = 0;
                }
                if (nRow["Money"].ToString() != "")
                {
                    float zMoney = float.Parse(nRow["Money"].ToString().Trim());
                    nRowView.Cells["Money"].Value = zMoney.ToString("n0");
                }
                else
                {
                    nRowView.Cells["Money"].Value = "0";
                }
                i++;
            }
            GV_Employee.GetSum(false);
        }
        private void GV_Employee_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 1)
            {
                if (GV_Employee.Rows[e.RowIndex].Cells["EmployeeID"].Value != null)
                {
                    Order_Employee_Info zOrderEmployee;
                    if (GV_Employee.Rows[e.RowIndex].Tag == null)
                    {
                        zOrderEmployee = new Order_Employee_Info();
                        GV_Employee.Rows[e.RowIndex].Tag = zOrderEmployee;
                    }
                    else
                    {
                        zOrderEmployee = new Order_Employee_Info();
                    }

                    string ID = GV_Employee.Rows[e.RowIndex].Cells["EmployeeID"].Value.ToString();
                    int nRowCount = Employee_Data.Count(_TeamKey, ID, _OrderKey);

                    if (nRowCount > 0)
                    {
                        Utils.TNMessageBoxOK("Nhân viên này đã có rồi!",1);
                    }
                    else
                    {
                        Employee_Info zInfo = new Employee_Info();
                        zInfo.GetEmployeeID(ID);
                        zOrderEmployee.EmployeeKey = zInfo.Key;
                        GV_Employee.Rows[e.RowIndex].Cells["EmployeeID"].Tag = zInfo.Key;
                        GV_Employee.Rows[e.RowIndex].Cells["FullName"].Value = zInfo.FullName;
                        nRowCount = Employee_Data.Count_CardID(ID, _TeamKey,dte_Date.Value);
                        if (nRowCount == 0)
                        {
                            GV_Employee.CurrentRow.Cells["Borrow"].Value = true;
                        }
                        else
                        {
                            GV_Employee.CurrentRow.Cells["Borrow"].Value = false;
                        }
                        GV_Employee.Rows[e.RowIndex].Cells["Time"].Value = 0;
                        GV_Employee.Rows[e.RowIndex].Cells["SoGio"].Value = 0;
                        GV_Employee.Rows[e.RowIndex].Cells["Basket"].Value = 0;
                        GV_Employee.Rows[e.RowIndex].Cells["Kg"].Value = 0;
                        GV_Employee.Rows[e.RowIndex].Cells["Money"].Value = 0;
                    }
                    if (zOrderEmployee.Key == 0)
                        zOrderEmployee.RecordStatus = 1;
                    else
                        zOrderEmployee.RecordStatus = 2;
                }
            }
        }
        private void GV_Employee_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.Delete)
            //{
            //    DialogResult dlr = MessageBox.Show("Bạn có xóa thông tin này ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            //    if (dlr == DialogResult.Yes)
            //    {
            //        int Index = GV_Employee.CurrentCell.RowIndex;
            //        DataGridViewRow Gvrow = GV_Employee.Rows[Index];

            //        if (Gvrow.Tag != null)
            //        {
            //            GV_Employee.CurrentRow.Visible = false;
            //            Order_Employee_Info zOrder_Employee = (Order_Employee_Info)Gvrow.Tag;
            //            zOrder_Employee.RecordStatus = 99;
            //        }
            //        else
            //        {
            //            GV_Employee.Rows.Remove(Gvrow);
            //        }

            //        GV_Employee.GetSum(true);
            //    }
            //}
        }
        #endregion

        #region[GV Employee_View]
        private void SetupLayoutGVEmployee_View()
        {
            DataTable zTable = Employee_Data.Load_Data("EmployeeKey, EmployeeID, FullName");
            string[] zHeaderLV = new string[] { "Mã NV", "Tên" };

            GV_EmployeeView.ColumnAdd("No", "STT", 40);

            GV_Column zCol = new GV_Column("EmployeeID", "ListView", 70, 400);
            zCol.TableSource = zTable;
            zCol.FilterColumn = true;
            zCol.CheckEmpty = true;
            zCol.IsPrimaryID = true;
            zCol.ShowColumn = 1;
            zCol.DisplayNextColumn = "FullName";
            zCol.HeaderText = "Mã NV";
            zCol.InitColumnLV(zHeaderLV);
            zCol.LoadDataToLV();
            GV_EmployeeView.ColumnAdd(zCol);

            GV_EmployeeView.ColumnAdd("FullName", "Họ tên", 150);

            zCol = new GV_Column();
            zCol.IsSum = true;
            zCol.CheckEmpty = true;
            zCol.ColumnName = "Time";
            zCol.HeaderText = "Số Giờ";
            zCol.Style = "Number";
            zCol.Width = 70;
            GV_EmployeeView.ColumnAdd(zCol);

            zCol = new GV_Column();
            zCol.ColumnName = "Borrow";
            zCol.HeaderText = "Mượn";
            zCol.Style = "CheckBox";
            zCol.Width = 20;
            GV_EmployeeView.ColumnAdd(zCol);

            zCol = new GV_Column();
            zCol.ColumnName = "Private";
            zCol.HeaderText = "Ăn Riêng";
            zCol.Style = "CheckBox";
            zCol.Width = 20;
            GV_EmployeeView.ColumnAdd(zCol);

            zCol = new GV_Column();
            zCol.ColumnName = "Share";
            zCol.HeaderText = "Ăn Chung";
            zCol.Style = "CheckBox";
            zCol.Width = 20;
            GV_EmployeeView.ColumnAdd(zCol);

            zCol = new GV_Column();
            zCol.IsSum = true;
            zCol.CheckEmpty = true;
            zCol.ColumnName = "TimeEat";
            zCol.HeaderText = "Số giờ đem về";
            zCol.Style = "Number";
            zCol.Width = 100;
            GV_EmployeeView.ColumnAdd(zCol);

            zCol = new GV_Column();
            zCol.IsSum = true;
            zCol.CheckEmpty = true;
            zCol.ColumnName = "TimeProduct";
            zCol.HeaderText = "Số giờ chia lại";
            zCol.Style = "Number";
            zCol.Width = 100;
            GV_EmployeeView.ColumnAdd(zCol);

            zCol = new GV_Column();
            zCol.IsSum = true;
            zCol.CheckEmpty = true;
            zCol.ColumnName = "Basket";
            zCol.HeaderText = "Số rổ đem về";
            zCol.Style = "Number";
            zCol.Width = 90;
            GV_EmployeeView.ColumnAdd(zCol);

            zCol = new GV_Column();
            zCol.IsSum = true;
            zCol.CheckEmpty = true;
            zCol.ColumnName = "BasketProduct";
            zCol.HeaderText = "Số rổ chia lại";
            zCol.Style = "Number";
            zCol.Width = 90;
            GV_EmployeeView.ColumnAdd(zCol);

            zCol = new GV_Column();
            zCol.IsSum = true;
            zCol.CheckEmpty = true;
            zCol.ColumnName = "Kg";
            zCol.HeaderText = "Số kí đem về";
            zCol.Style = "Number";
            zCol.Width = 90;
            GV_EmployeeView.ColumnAdd(zCol);

            zCol = new GV_Column();
            zCol.IsSum = true;
            zCol.CheckEmpty = true;
            zCol.ColumnName = "KgProduct";
            zCol.HeaderText = "Số kí chia lại";
            zCol.Style = "Number";
            zCol.Width = 90;
            GV_EmployeeView.ColumnAdd(zCol);

            zCol = new GV_Column();
            zCol.IsSum = true;
            zCol.CheckEmpty = true;
            zCol.ColumnName = "Money";
            zCol.HeaderText = "Tiền đem về";
            zCol.Style = "Number";
            zCol.Width = 100;
            GV_EmployeeView.ColumnAdd(zCol);

            zCol = new GV_Column();
            zCol.IsSum = true;
            zCol.CheckEmpty = true;
            zCol.ColumnName = "MoneyProduct";
            zCol.HeaderText = "Tiền chia lại";
            zCol.Style = "Number";
            zCol.Width = 100;
            GV_EmployeeView.ColumnAdd(zCol);

            GV_EmployeeView.FillLastColumn = false;
            GV_EmployeeView.IsSum = true;
            GV_EmployeeView.AllowAddRow = false;

            GV_EmployeeView.Init();
            GV_EmployeeView.GetSum(false);
        }
        private void InitData_GVEmployee_View(DataGridView GV, DataTable Table)
        {
            int i = 0;
            GV.Rows.Clear();
            foreach (DataRow nRow in Table.Rows)
            {
                GV.Rows.Add();
                DataGridViewRow nRowView = GV.Rows[i];
                nRowView.Cells["No"].Value = (i + 1).ToString();
                nRowView.Cells["EmployeeID"].Tag = nRow["EmployeeKey"].ToString().Trim();
                nRowView.Cells["EmployeeID"].Value = nRow["EmployeeID"].ToString().Trim();
                nRowView.Cells["FullName"].Value = nRow["EmployeeName"].ToString().Trim();
                if (nRow["Time"].ToString().Trim() != "")
                {
                    float zTime = 0;
                    if (float.TryParse(nRow["Time"].ToString(), out zTime))
                    {

                    }
                    nRowView.Cells["Time"].Value = zTime.Ton2String();
                }
                else
                {
                    nRowView.Cells["Time"].Value = 0;
                }
                if (nRow["Borrow"].ToInt() == 1 && nRow["Share"].ToInt() == 1)
                {
                    nRowView.Cells["Borrow"].Value = true;
                    if (nRow["MoneyShare"].ToString() != "")
                    {
                        float zMoney = float.Parse(nRow["MoneyShare"].ToString().Trim());
                        nRowView.Cells["MoneyProduct"].Value = zMoney.ToString("n0");
                    }

                }
                else if (nRow["Borrow"].ToInt() == 1 && nRow["Private"].ToInt() == 1)
                {
                    nRowView.Cells["Borrow"].Value = true;
                    if (nRow["MoneyPersonal"].ToString() != "")
                    {
                        float zMoney = float.Parse(nRow["MoneyPersonal"].ToString().Trim());
                        nRowView.Cells["MoneyProduct"].Value = zMoney.ToString("n0");
                    }
                }
                else
                {
                    if (nRow["MoneyPersonal"].ToString() != "")
                    {
                        float zMoney = float.Parse(nRow["MoneyPersonal"].ToString().Trim());
                        nRowView.Cells["MoneyProduct"].Value = zMoney.ToString("n0");
                    }
                }
                if (nRow["Private"].ToInt() == 1)
                {
                    nRowView.Cells["Private"].Value = true;
                    nRowView.DefaultCellStyle.BackColor = Color.LightBlue;
                }
                if (nRow["Share"].ToInt() == 1)
                {
                    nRowView.Cells["Share"].Value = true;
                    nRowView.DefaultCellStyle.BackColor = Color.LightBlue;
                }
                if (nRow["TimeEat"].ToString() != "")
                {
                    float zSoGio = float.Parse(nRow["TimeEat"].ToString().Trim());
                    nRowView.Cells["TimeEat"].Value = zSoGio.ToString("n4");
                }
                if (nRow["TimeProduct"].ToString() != "")
                {
                    float zTimeProduct = float.Parse(nRow["TimeProduct"].ToString().Trim());
                    nRowView.Cells["TimeProduct"].Value = zTimeProduct.ToString("n4");
                }
                if (nRow["Basket"].ToString() != "")
                {
                    float zBasket = float.Parse(nRow["Basket"].ToString().Trim());
                    nRowView.Cells["Basket"].Value = zBasket.ToString("n4");
                }
                if (nRow["BasketProduct"].ToString() != "")
                {
                    float zBasketProduct = float.Parse(nRow["BasketProduct"].ToString().Trim());
                    nRowView.Cells["BasketProduct"].Value = zBasketProduct.ToString("n4");
                }
                if (nRow["Kg"].ToString() != "")
                {
                    float zSoLuong = float.Parse(nRow["Kg"].ToString().Trim());
                    nRowView.Cells["Kg"].Value = zSoLuong.ToString("n4");
                }
                if (nRow["KgProduct"].ToString() != "")
                {
                    float zSoLuong = float.Parse(nRow["KgProduct"].ToString().Trim());
                    nRowView.Cells["KgProduct"].Value = zSoLuong.ToString("n4");
                }
                if (nRow["Money"].ToString() != "")
                {
                    float zMoney = float.Parse(nRow["Money"].ToString().Trim());
                    nRowView.Cells["Money"].Value = zMoney.ToString("n0");
                }

                i++;
            }
            GV_EmployeeView.GetSum(false);
        }
        private void GV_Employee_View_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {

        }

        #endregion

        #region[Event]
        private void Btn_New_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            DataTable zTableOrder = Order_Adjusted_Data.List_EmployeeShare1(_OrderKey, _TeamKey, dte_Date.Value, dte_Date.Value);
            InitData_GVEmployee(GV_Employee, zTableOrder);
            this.Cursor = Cursors.Default;
        }
        private void Btn_Save_Click(object sender, EventArgs e)
        {
            if (SessionUser.Date_Lock >= dte_Date.Value)
            {
                Utils.TNMessageBoxOK("Thời gian này đã khóa sổ.!", 2);
                return;
            }
            this.Cursor = Cursors.WaitCursor;
            string zMessage = CheckBeForSave();
            if (zMessage == "")
            {
                Create_TableRemark();

                //tính số kí trung bình
                float zTBKg = 0;
                zTBKg = _TotalKg / _TotalTime;
                //tính số giờ đem về trung bình
                float zTBSoGio = 0;
                zTBSoGio = _TotalSoGio / _TotalTime;
                //tính tổng số rổ trung bình
                float zTBBasket = 0;
                zTBBasket = _TotalBasket / _TotalTime;
                //tính tiền trung bình ,và số kí trung bình
                float zTBMoney = 0;
                zTBMoney = _TotalMoney / _TotalTime;

                //Xóa tất cả tiền chia lại của nhóm đã tính
                _Info = new Order_Adjusted_Info();
                _Info.DeleteTeam_OrderShare1(_TeamKey, _OrderKey);
                KhoiTaoColumnBangSoSanh();
                foreach (DataRow zRow in _TableRemark.Rows)
                {
                    string zEmployeeKey = zRow["EmployeeKey"].ToString();
                    string zEmployeeID = zRow["EmployeeID"].ToString();
                    string zEmployeeName = zRow["FullName"].ToString();
                    float zTime = float.Parse(zRow["Time"].ToString());
                    float zKg = Order_Adjusted_Data.LaySoKiAnChungChiaLaiLan1(_OrderKey, zEmployeeKey);
                    float zTimeEat = Order_Adjusted_Data.LaySoGioAnChungChiaLaiLan1(_OrderKey, zEmployeeKey);
                    float zBasket = Order_Adjusted_Data.LaySoRoAnChungChiaLaiLan1(_OrderKey, zEmployeeKey);
                    float zMoney = Order_Adjusted_Data.LaySoTienAnChungChiaLaiLan1(_OrderKey, zEmployeeKey);
                    float zBorrow = int.Parse(zRow["Borrow"].ToString());
                    float zPrivate = int.Parse(zRow["Private"].ToString());
                    float zShare = int.Parse(zRow["Share"].ToString());

                    //Tính số giờ được chia
                    float zTimeProduct = 0;
                    if (float.TryParse((zTBSoGio * zTime).ToString(), out zTimeProduct))
                    {

                    }
                    //Tính số rổ được chia
                    float zBasketProduct = 0;
                    if (float.TryParse((zTBBasket * zTime).ToString(), out zBasketProduct))
                    {

                    }
                    //Tính kg được chia
                    float zKgProduct = 0;
                    if (float.TryParse((zTBKg * zTime).ToString(), out zKgProduct))
                    {

                    }
                    float zMoneyShare = 0;
                    float zMoneyPersonal = 0;
                    if (zBorrow == 1 && zShare == 1)
                    {
                        if (float.TryParse((zTBMoney * zTime).ToString(), out zMoneyShare))
                        {

                        }
                    }
                    else
                    {
                        if (float.TryParse((zTBMoney * zTime).ToString(), out zMoneyPersonal))
                        {

                        }
                    }
                    DataRow row = _BangSoSanh.NewRow();
                    //Lưu bảng so sánh dữ liệu trước khi hiện ra
                    row["OrderKey"] = _OrderKey;
                    row["OrderID"] = _OrderID;
                    row["OrderID_Compare"] = _OrderIDCompare;
                    row["OrderDate"] = dte_Date.Value;
                    row["StageKey"] = _StageKey;
                    row["StageID"] = _StageID;
                    row["StageName"] = _StageName;
                    row["Price"] = _Price;

                    row["EmployeeKey"] = zEmployeeKey;
                    row["EmployeeID"] = zEmployeeID;
                    row["EmployeeName"] = zEmployeeName;
                    row["Kilogram"] = zKg;
                    row["Basket"] = zBasket;
                    row["TimeEat"] = zTimeEat;
                    row["Money"] = zMoney;

                    row["Time"] = zTime;
                    row["KgProduct"] = float.Parse(zKgProduct.ToString("n4"));
                    row["TimeProduct"] = float.Parse(zTimeProduct.ToString("n4"));
                    row["BasketProduct"] = float.Parse(zBasketProduct.ToString("n4"));
                    row["MoneyPersonal"] = float.Parse(zMoneyPersonal.ToString("n0"));
                    row["MoneyShare"] = float.Parse(zMoneyShare.ToString("n0"));

                    row["Borrow"] = zRow["Borrow"].ToInt();//mượn
                    row["Private"] = zRow["Private"].ToInt();     //ăn riêng
                    row["Share"] = zRow["Share"].ToInt();
                    _BangSoSanh.Rows.Add(row);


                }
               
                #region[Chỉnh sửa 09/03/2021]
                //-----lọc dữ liệu có số tiền thấp nhất, lấy mã nhân viên có số tiền thấp nhất, nếu bằng nhau hết lấy người đầu tiên----
                string EmployeeKey = "";
                float sumkgptoduct = 0;
                float sumtimeproduct = 0;
                float sumbasketproduct = 0;
                float summoneypersonal = 0;
                float summoneyshare = 0;
                float money = 0;


                // ưu tiên lấy người thấp nhất
                DataRow dr = _BangSoSanh.AsEnumerable().OrderBy(r => r["KgProduct"]).Where(r => ((float)r["KgProduct"] > 0)).First(); // getting the row to edit , change it as you need
                                                                                                                                      // lấy tổng số tiền chừa người thấp nhất
                EmployeeKey = dr["EmployeeKey"].ToString();

                if (float.TryParse(_BangSoSanh.Compute("Sum(KgProduct)", "EmployeeKey NOT IN ('" + EmployeeKey + "')").ToString(), out sumkgptoduct))
                {

                }
                if (float.TryParse(_BangSoSanh.Compute("Sum(TimeProduct)", "EmployeeKey NOT IN ('" + EmployeeKey + "')").ToString(), out sumtimeproduct))
                {

                }
                if (float.TryParse(_BangSoSanh.Compute("Sum(BasketProduct)", "EmployeeKey NOT IN ('" + EmployeeKey + "')").ToString(), out sumbasketproduct))
                {

                }
                if (float.TryParse(_BangSoSanh.Compute("Sum(MoneyPersonal)", "EmployeeKey NOT IN ('" + EmployeeKey + "')").ToString(), out summoneypersonal))
                {

                }
                if (float.TryParse(_BangSoSanh.Compute("Sum(MoneyShare)", "EmployeeKey NOT IN ('" + EmployeeKey + "')").ToString(), out summoneyshare))
                {

                }
                // chia tiền lại cho người thấp nhất
                money = (_TotalMoney - summoneypersonal - summoneyshare);
                if (money >= 0) // trường hợp số tiền dương
                {
                    dr["KgProduct"] = (_TotalKg - sumkgptoduct).ToString("n4");
                    dr["TimeProduct"] = (_TotalSoGio - sumtimeproduct).ToString("n4");
                    dr["BasketProduct"] = (_TotalBasket - sumbasketproduct).ToString("n4");
                    if (dr["Money"].ToInt() == 1)
                    {
                        dr["MoneyShare"] = money.ToString("n0");
                    }
                    else
                    {
                        dr["MoneyPersonal"] = money.ToString("n0");
                    }
                }
                else
                { // số tiền âm
                    // lấy người số tiền cao nhất
                    dr = _BangSoSanh.AsEnumerable().OrderBy(r => r["KgProduct"]).Where(r => ((float)r["KgProduct"] > 0)).Last(); // getting the row to edit , change it as you need
                                                                                                                                          // lấy tổng số tiền chừa người cao nhất
                    EmployeeKey = dr["EmployeeKey"].ToString();

                    if (float.TryParse(_BangSoSanh.Compute("Sum(KgProduct)", "EmployeeKey NOT IN ('" + EmployeeKey + "')").ToString(), out sumkgptoduct))
                    {

                    }
                    if (float.TryParse(_BangSoSanh.Compute("Sum(TimeProduct)", "EmployeeKey NOT IN ('" + EmployeeKey + "')").ToString(), out sumtimeproduct))
                    {

                    }
                    if (float.TryParse(_BangSoSanh.Compute("Sum(BasketProduct)", "EmployeeKey NOT IN ('" + EmployeeKey + "')").ToString(), out sumbasketproduct))
                    {

                    }
                    if (float.TryParse(_BangSoSanh.Compute("Sum(MoneyPersonal)", "EmployeeKey NOT IN ('" + EmployeeKey + "')").ToString(), out summoneypersonal))
                    {

                    }
                    if (float.TryParse(_BangSoSanh.Compute("Sum(MoneyShare)", "EmployeeKey NOT IN ('" + EmployeeKey + "')").ToString(), out summoneyshare))
                    {

                    }
                    // chia tiền lại cho người cao nhất
                    money = (_TotalMoney - summoneypersonal - summoneyshare);
                    dr["KgProduct"] = (_TotalKg - sumkgptoduct).ToString("n4");
                    dr["TimeProduct"] = (_TotalSoGio - sumtimeproduct).ToString("n4");
                    dr["BasketProduct"] = (_TotalBasket - sumbasketproduct).ToString("n4");
                    if (dr["Money"].ToInt() == 1)
                    {
                        dr["MoneyShare"] = money.ToString("n0");
                    }
                    else
                    {
                        dr["MoneyPersonal"] = money.ToString("n0");
                    }
                }
                #endregion
                foreach (DataRow zRow in _BangSoSanh.Rows)
                {
                    Order_Adjusted_Item zItem = new Order_Adjusted_Item();
                    zItem.OrderKey = zRow["OrderKey"].ToString();
                    zItem.OrderID = zRow["OrderID"].ToString();
                    zItem.OrderIDCompare = zRow["OrderID_Compare"].ToString();
                    zItem.OrderDate = DateTime.Parse(zRow["OrderDate"].ToString());
                    zItem.StageKey = zRow["StageKey"].ToInt();
                    zItem.StageID = zRow["StageID"].ToString();
                    zItem.StageName = zRow["StageName"].ToString();
                    zItem.Price = float.Parse(zRow["Price"].ToString());

                    zItem.EmployeeKey = zRow["EmployeeKey"].ToString();
                    zItem.EmployeeID = zRow["EmployeeID"].ToString();
                    zItem.EmployeeName = zRow["EmployeeName"].ToString();
                    zItem.Kg = float.Parse(zRow["Kilogram"].ToString());
                    zItem.Basket = float.Parse(zRow["Basket"].ToString());
                    zItem.TimeEat = float.Parse(zRow["TimeEat"].ToString());
                    zItem.Money = float.Parse(zRow["Money"].ToString());
                    zItem.Time = float.Parse(zRow["Time"].ToString());
                    zItem.KgProduct = float.Parse(zRow["KgProduct"].ToString());
                    zItem.TimeProduct = float.Parse(zRow["TimeProduct"].ToString());
                    zItem.BasketProduct = float.Parse(zRow["BasketProduct"].ToString());
                    zItem.MoneyPersonal = float.Parse(zRow["MoneyPersonal"].ToString());
                    zItem.MoneyShare = float.Parse(zRow["MoneyShare"].ToString());

                    zItem.Borrow = int.Parse(zRow["Borrow"].ToString());
                    zItem.Private = int.Parse(zRow["Private"].ToString());
                    zItem.Share = int.Parse(zRow["Share"].ToString());

                    zMessage += Save_OrderAdjusted(zItem);
                }


                if (zMessage == "")
                {
                    Utils.TNMessageBoxOK("Đã lưu thành công!",3);
                    GV_Order_Click(sender, e);
                    this.Cursor = Cursors.Default;
                }
                else
                {
                    Utils.TNMessageBoxOK(zMessage,4);
                    this.Cursor = Cursors.Default;
                }
            }
            else
            {
                Utils.TNMessageBoxOK("Vui lòng kiểm tra lại \n" + zMessage,4);
                this.Cursor = Cursors.Default;
            }

        }

        private void Btn_SaveAll_Click(object sender, EventArgs e)
        {
            if (SessionUser.Date_Lock >= dte_Date.Value)
            {
                Utils.TNMessageBoxOK("Thời gian này đã khóa sổ.!", 2);
                return;
            }
            this.Cursor = Cursors.WaitCursor;
            string zMessage = CheckBeForSave();
            if (zMessage == "")
            {
                Create_TableRemark();
                DataTable zTableOrder = Order_Adjusted_Data.List_OrderShare1(dte_Date.Value, dte_Date.Value, _TeamKey);
                if (zTableOrder.Rows.Count > 0)
                {
                    zMessage += Save(zTableOrder);
                    if (zMessage == "")
                    {
                        Utils.TNMessageBoxOK("Đã lưu thành công!",3);
                        this.Cursor = Cursors.Default;
                        GV_Order_Click(sender, e);
                    }
                    else
                    {
                        Utils.TNMessageBoxOK(zMessage,4);
                        this.Cursor = Cursors.Default;
                    }
                }
                else
                {
                   Utils.TNMessageBoxOK("Không tìm thấy đơn hàng cần tính!",1);
                    this.Cursor = Cursors.Default;
                }
            }
            else
            {
                Utils.TNMessageBoxOK("Vui lòng kiểm tra lại \n" + zMessage,4);
                this.Cursor = Cursors.Default;
            }
        }

        private void Btn_Search_Click(object sender, EventArgs e)
        {
            //this.Cursor = Cursors.WaitCursor;
            DataTable ztable = Order_Adjusted_Data.List_TeamShare1(dte_Date.Value, dte_Date.Value);
            InitData_GVTeam(GV_Team, ztable);
            GV_Order.Rows.Clear();
            GV_Employee.Rows.Clear();
            GV_EmployeeView.Rows.Clear();
            //this.Cursor = Cursors.Default;
        }
        #endregion

        #region[Progess]

        private void Create_TableRemark()
        {
            _TotalTime = 0;
            _TableRemark = new DataTable();
            _TableRemark.Columns.Add("EmployeeKey");
            _TableRemark.Columns.Add("EmployeeID");
            _TableRemark.Columns.Add("FullName");
            _TableRemark.Columns.Add("Time");
            _TableRemark.Columns.Add("Borrow");
            _TableRemark.Columns.Add("Private");
            _TableRemark.Columns.Add("Share");
            for (int i = 0; i < GV_Employee.Rows.Count - 1; i++)
            {
                DataGridViewRow nRowView = GV_Employee.Rows[i];
                if (nRowView.Cells["EmployeeID"].Tag != null && nRowView.Cells["EmployeeID"].Value != null &&
                    nRowView.Cells["EmployeeID"].Value.ToString() != "")
                {
                    DataRow nRow = _TableRemark.NewRow();
                    nRow["EmployeeKey"] = nRowView.Cells["EmployeeID"].Tag;
                    nRow["EmployeeID"] = nRowView.Cells["EmployeeID"].Value;
                    nRow["FullName"] = nRowView.Cells["FullName"].Value;
                    float zTime = 0;
                    if (nRowView.Cells["Time"].Value != null)
                    {
                        nRow["Time"] = nRowView.Cells["Time"].Value;
                        zTime = float.Parse(nRowView.Cells["Time"].Value.ToString());
                    }
                    else
                    {
                        nRow["Time"] = 0;
                    }
                    _TotalTime += zTime;
                    if (nRowView.Cells["Borrow"].Value != null && bool.Parse(nRowView.Cells["Borrow"].Value.ToString()) == true)
                    {
                        nRow["Borrow"] = 1;
                    }
                    else
                    {
                        nRow["Borrow"] = 0;
                    }
                    if (nRowView.Cells["Private"].Value != null && bool.Parse(nRowView.Cells["Private"].Value.ToString()) == true)
                    {
                        nRow["Private"] = 1;
                    }
                    else
                    {
                        nRow["Private"] = 0;
                    }
                    if (nRowView.Cells["Share"].Value != null && bool.Parse(nRowView.Cells["Share"].Value.ToString()) == true)
                    {
                        nRow["Share"] = 1;
                    }
                    else
                    {
                        nRow["Share"] = 0;
                    }
                    _TableRemark.Rows.Add(nRow);
                }

            }
        }
        private string CheckBeForSave()
        {
            _TotalTime = 0;
            string zStrError = "";
            for (int i = 0; i < GV_Employee.Rows.Count - 1; i++)
            {
                DataGridViewRow nRowView = GV_Employee.Rows[i];
                if (nRowView.Cells["EmployeeID"].Tag != null && nRowView.Cells["EmployeeID"].Value != null &&
                   nRowView.Cells["EmployeeID"].Value.ToString() != "")
                {
                    float zTime = 0;
                    if (nRowView.Cells["Time"].Value == null || nRowView.Cells["Time"].Value.ToString() == "")
                    {
                        zTime = 0;
                    }
                    else
                    {
                        zTime = float.Parse(nRowView.Cells["Time"].Value.ToString());

                    }
                    _TotalTime += zTime;
                    bool CellBorrow = false;
                    bool CellShare = false;
                    bool CellPrivate = false;
                    if (nRowView.Cells["Borrow"].Value != null && bool.Parse(nRowView.Cells["Borrow"].Value.ToString()) == true)
                    {
                        CellBorrow = true;
                    }
                    if (nRowView.Cells["Private"].Value != null && bool.Parse(nRowView.Cells["Private"].Value.ToString()) == true)
                    {
                        CellPrivate = true;
                    }
                    if (nRowView.Cells["Share"].Value != null && bool.Parse(nRowView.Cells["Share"].Value.ToString()) == true)
                    {
                        CellShare = true;
                    }

                    if (CellBorrow == true)
                    {
                        if (CellPrivate == false &&
                            CellShare == false)
                        {
                            zStrError += "- Kiểm tra lại tình huống mượn nhân viên \n\r";
                            break;
                        }
                        if (CellPrivate == true &&
                                CellShare == true)
                        {
                            zStrError += "- Kiểm tra lại tình huống mượn nhân viên \n\r";
                            break;
                        }

                    }
                }

            }
            if (_TotalTime <= 0)
            {
                zStrError += "- Kiểm tra lại tổng giờ phải lớn hơn 0 \n\r";
            }
            return zStrError;
        }
        private DataTable _BangSoSanh;
        void KhoiTaoColumnBangSoSanh()
        {
            _BangSoSanh = new DataTable();

            _BangSoSanh.Columns.Add("OrderKey");
            _BangSoSanh.Columns.Add("OrderID");
            _BangSoSanh.Columns.Add("OrderID_Compare");
            _BangSoSanh.Columns.Add("OrderDate");
            _BangSoSanh.Columns.Add("StageKey");
            _BangSoSanh.Columns.Add("StageID");
            _BangSoSanh.Columns.Add("StageName");
            _BangSoSanh.Columns.Add("Price");

            _BangSoSanh.Columns.Add("EmployeeKey");
            _BangSoSanh.Columns.Add("EmployeeID");
            _BangSoSanh.Columns.Add("EmployeeName");
            _BangSoSanh.Columns.Add("Kilogram");//số kí đem về 
            _BangSoSanh.Columns.Add("Basket");// số rổ đem về
            _BangSoSanh.Columns.Add("TimeEat");// số giờ đem về 
            _BangSoSanh.Columns.Add("Money");// số tiền chia lại

            _BangSoSanh.Columns.Add("Time");// số giờ nhập

            _BangSoSanh.Columns.Add("TimeProduct");// số gio được chia
            _BangSoSanh.Columns["TimeProduct"].DataType = typeof(float);
            _BangSoSanh.Columns.Add("BasketProduct");// số ro được chia
            _BangSoSanh.Columns["BasketProduct"].DataType = typeof(float);
            _BangSoSanh.Columns.Add("KgProduct");// số kí được chia
            _BangSoSanh.Columns["KgProduct"].DataType = typeof(float);
            _BangSoSanh.Columns.Add("MoneyPersonal");// số tiền được chia
            _BangSoSanh.Columns["MoneyPersonal"].DataType = typeof(float);
            _BangSoSanh.Columns.Add("MoneyShare");// số tiền được chia
            _BangSoSanh.Columns["MoneyShare"].DataType = typeof(float);

            _BangSoSanh.Columns.Add("Borrow");
            _BangSoSanh.Columns.Add("Private");
            _BangSoSanh.Columns.Add("Share");
        }
        private DateTime _OrderDate;
        private string Save(DataTable In_Table)
        {
            string zMessage = "";
            //Tính lại các đơn hàng chia lại cho nhóm

            for (int i = 0; i < In_Table.Rows.Count; i++)
            {

                DataRow r = In_Table.Rows[i];
                _OrderKey = r["OrderKey"].ToString();
                _OrderID = r["OrderID"].ToString();
                _OrderIDCompare = r["OrderID_Compare"].ToString();
                _OrderDate = DateTime.Parse(r["OrderDate"].ToString());
                _StageKey = int.Parse(r["StageKey"].ToString());
                _StageID = r["StageID"].ToString();
                _StageName = r["StageName"].ToString();
                _Price = float.Parse(r["Price"].ToString());

                _TotalSoGio = float.Parse(r["TimeShare"].ToString());
                _TotalBasket = float.Parse(r["BasketShare"].ToString());
                _TotalKg = float.Parse(r["KgShare"].ToString());
                _TotalMoney = float.Parse(r["MoneyShare"].ToString());
                //chua lam

                //tính số kí trung bình
                float zTBKg = 0;
                zTBKg = _TotalKg / _TotalTime;
                //tính số giờ đem về trung bình
                float zTBSoGio = 0;
                zTBSoGio = _TotalSoGio / _TotalTime;
                //tính tổng số rổ trung bình
                float zTBBasket = 0;
                zTBBasket = _TotalBasket / _TotalTime;
                //tính tiền trung bình ,và số kí trung bình
                float zTBMoney = 0;

                zTBMoney = _TotalMoney / _TotalTime;

                //Xóa tất cả tiền chia lại của nhóm đã tính

                _Info = new Order_Adjusted_Info();
                _Info.DeleteTeam_OrderShare1(_TeamKey, _OrderKey);

                KhoiTaoColumnBangSoSanh();
                foreach (DataRow zRow in _TableRemark.Rows)
                {
                    string zEmployeeKey = zRow["EmployeeKey"].ToString();
                    string zEmployeeID = zRow["EmployeeID"].ToString();
                    string zEmployeeName = zRow["FullName"].ToString();
                    float zTime = float.Parse(zRow["Time"].ToString());
                    float zKg = Order_Adjusted_Data.LaySoKiAnChungChiaLaiLan1(_OrderKey, zEmployeeKey);
                    float zTimeEat = Order_Adjusted_Data.LaySoGioAnChungChiaLaiLan1(_OrderKey, zEmployeeKey);
                    float zBasket = Order_Adjusted_Data.LaySoRoAnChungChiaLaiLan1(_OrderKey, zEmployeeKey);
                    float zMoney = Order_Adjusted_Data.LaySoTienAnChungChiaLaiLan1(_OrderKey, zEmployeeKey);
                    float zBorrow = int.Parse(zRow["Borrow"].ToString());
                    float zPrivate = int.Parse(zRow["Private"].ToString());
                    float zShare = int.Parse(zRow["Share"].ToString());

                    //Tính số giờ được chia
                    float zTimeProduct = 0;
                    if (float.TryParse((zTBSoGio * zTime).ToString(), out zTimeProduct))
                    {

                    }
                    //Tính số rổ được chia
                    float zBasketProduct = 0;
                    if (float.TryParse((zTBBasket * zTime).ToString(), out zBasketProduct))
                    {

                    }
                    //Tính kg được chia
                    float zKgProduct = 0;
                    if (float.TryParse((zTBKg * zTime).ToString(), out zKgProduct))
                    {

                    }
                    float zMoneyShare = 0;
                    float zMoneyPersonal = 0;
                    if (zBorrow == 1 && zShare == 1)
                    {
                        if (float.TryParse((zTBMoney * zTime).ToString(), out zMoneyShare))
                        {

                        }
                    }
                    else
                    {
                        if (float.TryParse((zTBMoney * zTime).ToString(), out zMoneyPersonal))
                        {

                        }
                    }
                    DataRow row = _BangSoSanh.NewRow();
                    //Lưu bảng so sánh dữ liệu trước khi hiện ra
                    row["OrderKey"] = _OrderKey;
                    row["OrderID"] = _OrderID;
                    row["OrderID_Compare"] = _OrderIDCompare;
                    row["OrderDate"] = dte_Date.Value;
                    row["StageKey"] = _StageKey;
                    row["StageID"] = _StageID;
                    row["StageName"] = _StageName;
                    row["Price"] = _Price;

                    row["EmployeeKey"] = zEmployeeKey;
                    row["EmployeeID"] = zEmployeeID;
                    row["EmployeeName"] = zEmployeeName;
                    row["Kilogram"] = zKg;
                    row["Basket"] = zBasket;
                    row["TimeEat"] = zTimeEat;
                    row["Money"] = zMoney;

                    row["Time"] = zTime;
                    row["KgProduct"] = float.Parse(zKgProduct.ToString("n4"));
                    row["TimeProduct"] = float.Parse(zTimeProduct.ToString("n4"));
                    row["BasketProduct"] = float.Parse(zBasketProduct.ToString("n4"));
                    row["MoneyPersonal"] = float.Parse(zMoneyPersonal.ToString("n0"));
                    row["MoneyShare"] = float.Parse(zMoneyShare.ToString("n0"));

                    row["Borrow"] = zRow["Borrow"].ToInt();//mượn
                    row["Private"] = zRow["Private"].ToInt();     //ăn riêng
                    row["Share"] = zRow["Share"].ToInt();
                    _BangSoSanh.Rows.Add(row);
                }
                //-----lọc dữ liệu có số tiền thấp nhất, lấy mã nhân viên có số tiền thấp nhất, nếu bằng nhau hết lấy người đầu tiên----
                string EmployeeKey = "";

                float sumkgptoduct = 0;
                float sumtimeproduct = 0;
                float sumbasketproduct = 0;
                float summoneypersonal = 0;
                float summoneyshare = 0;
                float money = 0;
                DataRow dr = _BangSoSanh.AsEnumerable().OrderBy(row => row["KgProduct"]).Where(row => ((float)row["KgProduct"] > 0)).First(); // getting the row to edit , change it as you need
                                                                                                                                              // lấy tổng số tiền chừa người thấp nhất
                EmployeeKey = dr["EmployeeKey"].ToString();

                if (float.TryParse(_BangSoSanh.Compute("Sum(KgProduct)", "EmployeeKey NOT IN ('" + EmployeeKey + "')").ToString(), out sumkgptoduct))
                {

                }
                if (float.TryParse(_BangSoSanh.Compute("Sum(TimeProduct)", "EmployeeKey NOT IN ('" + EmployeeKey + "')").ToString(), out sumtimeproduct))
                {

                }
                if (float.TryParse(_BangSoSanh.Compute("Sum(BasketProduct)", "EmployeeKey NOT IN ('" + EmployeeKey + "')").ToString(), out sumbasketproduct))
                {

                }
                if (float.TryParse(_BangSoSanh.Compute("Sum(MoneyPersonal)", "EmployeeKey NOT IN ('" + EmployeeKey + "')").ToString(), out summoneypersonal))
                {

                }
                if (float.TryParse(_BangSoSanh.Compute("Sum(MoneyShare)", "EmployeeKey NOT IN ('" + EmployeeKey + "')").ToString(), out summoneyshare))
                {

                }
                // chia tiền lại cho người thấp nhất

                dr["KgProduct"] = (_TotalKg - sumkgptoduct).ToString("n4");
                dr["TimeProduct"] = (_TotalSoGio - sumtimeproduct).ToString("n4");
                dr["BasketProduct"] = (_TotalBasket - sumbasketproduct).ToString("n4");
                money = (_TotalMoney - summoneypersonal - summoneyshare);
                if (money >= 0) // trường hợp số tiền dương
                {
                    dr["KgProduct"] = (_TotalKg - sumkgptoduct).ToString("n4");
                    dr["TimeProduct"] = (_TotalSoGio - sumtimeproduct).ToString("n4");
                    dr["BasketProduct"] = (_TotalBasket - sumbasketproduct).ToString("n4");
                    if (dr["Money"].ToInt() == 1)
                    {
                        dr["MoneyShare"] = money.ToString("n0");
                    }
                    else
                    {
                        dr["MoneyPersonal"] = money.ToString("n0");
                    }
                }
                else
                { // số tiền âm
                    // lấy người số tiền cao nhất
                    dr = _BangSoSanh.AsEnumerable().OrderBy(row => row["KgProduct"]).Where(row => ((float)row["KgProduct"] > 0)).Last(); // getting the row to edit , change it as you need
                                                                                                                                          // lấy tổng số tiền chừa người thấp nhất
                    EmployeeKey = dr["EmployeeKey"].ToString();

                    if (float.TryParse(_BangSoSanh.Compute("Sum(KgProduct)", "EmployeeKey NOT IN ('" + EmployeeKey + "')").ToString(), out sumkgptoduct))
                    {

                    }
                    if (float.TryParse(_BangSoSanh.Compute("Sum(TimeProduct)", "EmployeeKey NOT IN ('" + EmployeeKey + "')").ToString(), out sumtimeproduct))
                    {

                    }
                    if (float.TryParse(_BangSoSanh.Compute("Sum(BasketProduct)", "EmployeeKey NOT IN ('" + EmployeeKey + "')").ToString(), out sumbasketproduct))
                    {

                    }
                    if (float.TryParse(_BangSoSanh.Compute("Sum(MoneyPersonal)", "EmployeeKey NOT IN ('" + EmployeeKey + "')").ToString(), out summoneypersonal))
                    {

                    }
                    if (float.TryParse(_BangSoSanh.Compute("Sum(MoneyShare)", "EmployeeKey NOT IN ('" + EmployeeKey + "')").ToString(), out summoneyshare))
                    {

                    }
                    // chia tiền lại cho người cao nhất
                    money = (_TotalMoney - summoneypersonal - summoneyshare);
                    dr["KgProduct"] = (_TotalKg - sumkgptoduct).ToString("n4");
                    dr["TimeProduct"] = (_TotalSoGio - sumtimeproduct).ToString("n4");
                    dr["BasketProduct"] = (_TotalBasket - sumbasketproduct).ToString("n4");
                    if (dr["Money"].ToInt() == 1)
                    {
                        dr["MoneyShare"] = money.ToString("n0");
                    }
                    else
                    {
                        dr["MoneyPersonal"] = money.ToString("n0");
                    }
                }
                //lưu
                foreach (DataRow zRow in _BangSoSanh.Rows)
                {
                    Order_Adjusted_Item zItem = new Order_Adjusted_Item();
                    zItem.OrderKey = zRow["OrderKey"].ToString();
                    zItem.OrderID = zRow["OrderID"].ToString();
                    zItem.OrderIDCompare = zRow["OrderID_Compare"].ToString();
                    zItem.OrderDate = DateTime.Parse(zRow["OrderDate"].ToString());
                    zItem.StageKey = zRow["StageKey"].ToInt();
                    zItem.StageID = zRow["StageID"].ToString();
                    zItem.StageName = zRow["StageName"].ToString();
                    zItem.Price = float.Parse(zRow["Price"].ToString());

                    zItem.EmployeeKey = zRow["EmployeeKey"].ToString();
                    zItem.EmployeeID = zRow["EmployeeID"].ToString();
                    zItem.EmployeeName = zRow["EmployeeName"].ToString();
                    zItem.Kg = float.Parse(zRow["Kilogram"].ToString());
                    zItem.Basket = float.Parse(zRow["Basket"].ToString());
                    zItem.TimeEat = float.Parse(zRow["TimeEat"].ToString());
                    zItem.Money = float.Parse(zRow["Money"].ToString());
                    zItem.Time = float.Parse(zRow["Time"].ToString());
                    zItem.KgProduct = float.Parse(zRow["KgProduct"].ToString());
                    zItem.TimeProduct = float.Parse(zRow["TimeProduct"].ToString());
                    zItem.BasketProduct = float.Parse(zRow["BasketProduct"].ToString());
                    zItem.MoneyPersonal = float.Parse(zRow["MoneyPersonal"].ToString());
                    zItem.MoneyShare = float.Parse(zRow["MoneyShare"].ToString());

                    zItem.Borrow = int.Parse(zRow["Borrow"].ToString());
                    zItem.Private = int.Parse(zRow["Private"].ToString());
                    zItem.Share = int.Parse(zRow["Share"].ToString());

                    zMessage += Save_OrderAdjusted(zItem);
                }

            }
            return zMessage;
        }

        private string Save_OrderAdjusted(Order_Adjusted_Item zItem)
        {
            //Gọi hàm save
            _Info = new Order_Adjusted_Info();
            _Info.OrderKey = zItem.OrderKey;
            _Info.OrderID = zItem.OrderID;
            _Info.OrderIDCompare = zItem.OrderIDCompare;
            _Info.OrderDate = zItem.OrderDate;
            _Info.StageKey = zItem.StageKey;
            _Info.StageID = zItem.StageID;
            _Info.StageName = zItem.StageName;
            _Info.Price = zItem.Price;
            _Info.EmployeeKey = zItem.EmployeeKey;
            _Info.EmployeeID = zItem.EmployeeID;
            _Info.EmployeeName = zItem.EmployeeName;
            _Info.TeamKey = _TeamKey;
            _Info.TeamName = _TeamName;
            _Info.TeamID = _TeamID;
            _Info.Time = zItem.Time;
            _Info.TimeEat = zItem.TimeEat;
            _Info.Kg = zItem.Kg;
            _Info.Basket = zItem.Basket;
            _Info.Money = zItem.Money;
            _Info.Borrow = zItem.Borrow;
            _Info.Private = zItem.Private;
            _Info.Share = zItem.Share;
            _Info.KgProduct = zItem.KgProduct;
            _Info.TimeProduct = zItem.TimeProduct;
            _Info.BasketProduct = zItem.BasketProduct;
            _Info.CategoryKey = 1; // Quy định chia lại cho nhóm lần 1
            _Info.CategoryName = "CHIA LẠI LẦN 1";
            _Info.MoneyShare = zItem.MoneyShare;
            _Info.MoneyPersonal = zItem.MoneyPersonal;

            _Info.CreatedBy = SessionUser.UserLogin.Key;
            _Info.CreatedName = SessionUser.UserLogin.EmployeeName;
            _Info.ModifiedBy = SessionUser.UserLogin.Key;
            _Info.ModifiedName = SessionUser.UserLogin.EmployeeName;
            _Info.Create();
            if (_Info.Message == "11")
            {
                _Info.Message = "";
            }
            return _Info.Message;
        }
        #endregion

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion

       
    }
}
