﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using TNS.CORE;
using TNS.HRM;
using TNS.IVT;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Order : Form
    {
        #region -----Filed Name -----
        private int _AutoKey = 0;
        private int _AutoKeyProduct = 0;
        private int _Slug = 0;
        private string _OrderKey = "";
        private string _OrderID = "";
        private string _RoleID = "";
        private string _ProductKey = "";
        private int _UnitKey = 0;
        private int _TeamKey = 0;
        private int _WorkStatus = 0;
        private DateTime _OrderDate;
        private DateTime _WorkBegin;
        private DateTime _WorkEnd;
        private Order_Object _Order_Obj;
        private Order_Rate_Info zOrder_Rate;
        private List<Order_Rate_Info> _List_Rate;
        private List<Order_Employee_Info> _List_Employee;
        #endregion

        #region -----Properties-----
        public string OrderKey
        {
            get
            {
                return _OrderKey;
            }

            set
            {
                _OrderKey = value;
            }
        }

        public int TeamKey
        {
            get
            {
                return _TeamKey;
            }

            set
            {
                _TeamKey = value;
            }
        }

        public DateTime OrderDate
        {
            get
            {
                return _OrderDate;
            }

            set
            {
                _OrderDate = value;
            }
        }

        public DateTime WorkBegin
        {
            get
            {
                return _WorkBegin;
            }

            set
            {
                _WorkBegin = value;
            }
        }

        public DateTime WorkEnd
        {
            get
            {
                return _WorkEnd;
            }

            set
            {
                _WorkEnd = value;
            }
        }

        public int WorkStatus
        {
            get
            {
                return _WorkStatus;
            }

            set
            {
                _WorkStatus = value;
            }
        }

        public string RoleID
        {
            get
            {
                return _RoleID;
            }

            set
            {
                _RoleID = value;
            }
        }
        #endregion

        public Frm_Order()
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            this.ResizeBegin += (s, e) => { this.SuspendLayout(); };
            this.ResizeEnd += (s, e) => { this.ResumeLayout(true); };

            Utils.DoubleBuffered(GVEmployee, true);
            Utils.DoubleBuffered(GVRate, true);

            Utils.DrawGVStyle(ref GVRate);
            Utils.DrawGVStyle(ref GVSum);
            Utils.DrawGVStyle(ref GVEmployee);

            //----- ListView
            //LV_Search.ItemActivate += LV_Search_ItemActivate;
            //LV_Search.ItemSelectionChanged += LV_Search_ItemSelectionChanged;
            //LV_Search.KeyDown += LV_Search_KeyDown;
            //----- DataGridView 
            GVEmployee_Layout();
            GVRate_Layout();
            GVSum_Layout();

            GVRate.EditingControlShowing += GVRate_EditingControlShowing;
            GVEmployee.EditingControlShowing += GVEmployee_EditingControlShowing;

            GVRate.CellEndEdit += GVRate_CellEndEdit;
            GVEmployee.CellEndEdit += GVEmployee_CellEndEdit;

            GVRate.KeyDown += GVRate_KeyDown;
            GVEmployee.KeyDown += GVEmployee_KeyDown;
            //----- TextBox 
            txt_ProductID.Leave += Txt_ProductID_Leave;
            txt_ProductName.Leave += Txt_ProductName_Leave;
            txt_OrderID.Enabled = false;
            txt_Unit.Leave += Txt_Unit_Leave;
            //----- Button
            btn_Add.Click += Btn_Add_Click;
            btn_Search_CT.Click += Btn_Search_CT_Click;
            txt_Stage.KeyUp += Txt_Stage_KeyUp;
            txt_Stage.Leave += Txt_Stage_Leave;
            btn_New.Click += Btn_New_Click;
            //btn_AddCate.Click += Btn_AddCate_Click;
            btn_Often.Click += Btn_Often_Click; ;
            //----- TextBox
            txt_QuantityReality.Leave += Txt_QuantityReality_Leave;
            txt_Note.ScrollBars = ScrollBars.Both;
            //----ComboBox
            //cbo_Category.SelectedIndexChanged += Cbo_Category_SelectedIndexChanged;
            cbo_Sheet.SelectedValueChanged += Cbo_Sheet_SelectedValueChanged;

            this.Bounds = Screen.PrimaryScreen.WorkingArea;
        }

        private void Cbo_Sheet_SelectedValueChanged(object sender, EventArgs e)
        {
            DataTable In_Table = Data_Access.GetTable(txt_Excel.Text, cbo_Sheet.Text);
            GVEmployee_LoadData(In_Table);
        }

        private void Frm_Order_Load(object sender, EventArgs e)
        {
            if (_OrderKey.Length > 0 && _WorkStatus == 0)
            {
                txtTitle.Text = "CẬP NHẬT ĐƠN HÀNG " + _OrderID;
                LoadData();
                DataRow zEdit = User_Data.GetUserEdit(SessionUser.UserLogin.Key, _RoleID).Rows[0];
                if (Convert.ToInt32(zEdit[0]) > 0)
                {
                    btn_Add.Visible = true;
                }
                else
                {
                    btn_Add.Visible = false;
                }
            }
            if (_OrderKey.Length > 0 && _WorkStatus == 1)
            {
                txtTitle.Text = _OrderID + " ĐÃ HOÀN THÀNH";
                LoadData();
                DataRow zEdit = User_Data.GetUserEdit(SessionUser.UserLogin.Key, _RoleID).Rows[0];
                if (Convert.ToInt32(zEdit[0]) > 0)
                {
                    btn_Add.Visible = true;
                }
                else
                {
                    btn_Add.Visible = false;
                }
            }
            if (_OrderKey.Length == 0 && _WorkStatus == 0)
            {
                dte_OrderDate.Value = SessionUser.Date_Work;
                txtTitle.Text = "TẠO MỚI ĐƠN HÀNG";
                LoadData();
                GVEmployee_LoadData();
                Check_New();
                DataRow zAdd = User_Data.GetUserAdd(SessionUser.UserLogin.Key, _RoleID).Rows[0];
                if (Convert.ToInt32(zAdd[0]) > 0)
                {
                    btn_Add.Visible = true;
                }
                else
                {
                    btn_Add.Visible = false;
                }
            }
            Load_TextBox();

            //LV_Search.Hide();
            //LV_Search_Layout(LV_Search);
            //LoadDataToToolbox.ComboBoxData(cbo_Category, "SELECT AutoKey,RateName FROM FTR_Category WHERE RecordStatus < 99", "---Chọn Hệ Số---");
        }
        private void Load_TextBox()
        {
            LoadDataToToolbox.KryptonTextBox(txt_ProductID, "SELECT ProductID FROM IVT_Product WHERE Parent != '0' AND RecordStatus < 99  ");
            LoadDataToToolbox.KryptonTextBox(txt_ProductName, "SELECT ProductName FROM IVT_Product WHERE Parent != '0' AND RecordStatus < 99  ");
            LoadDataToToolbox.KryptonTextBox(txt_Unit, "SELECT UnitName FROM IVT_Product_Unit WHERE  RecordStatus < 99  ");
        }

        #region ----- Design Layout ----

        #region ----- ListView -----
        private void LV_Search_Layout(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên công đoạn";
            colHead.Width = 220;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Đơn giá";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Đơn vị";
            colHead.Width = 60;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);
        }
        //public void LV_SearchLoadData()
        //{
        //    this.Cursor = Cursors.WaitCursor;
        //    ListView LV = LV_Search;
        //    ListViewItem lvi;
        //    ListViewItem.ListViewSubItem lvsi;

        //    DataTable In_Table = new DataTable();

        //    In_Table = Product_Stages_Data.List(txt_Stage.Text, _TeamKey);

        //    LV.Items.Clear();
        //    int n = In_Table.Rows.Count;
        //    for (int i = 0; i < n; i++)
        //    {
        //        lvi = new ListViewItem();
        //        lvi.Text = (i + 1).ToString();
        //        lvi.ForeColor = Color.Navy;
        //        DataRow nRow = In_Table.Rows[i];
        //        lvi.Tag = nRow["StageKey"].ToString();
        //        lvi.BackColor = Color.White; ;

        //        lvi.ImageIndex = 0;
        //        lvsi = new ListViewItem.ListViewSubItem();
        //        lvsi.Text = nRow["StageName"].ToString().Trim();
        //        lvi.SubItems.Add(lvsi);

        //        lvi.ImageIndex = 0;
        //        lvsi = new ListViewItem.ListViewSubItem();
        //        lvsi.Text = nRow["Price"].ToString().Trim();
        //        lvi.SubItems.Add(lvsi);

        //        lvi.ImageIndex = 0;
        //        lvsi = new ListViewItem.ListViewSubItem();
        //        lvsi.Text = nRow["UnitName"].ToString().Trim();
        //        lvi.SubItems.Add(lvsi);

        //        LV.Items.Add(lvi);

        //    }
        //    LV_Search.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
        //    LV_Search.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
        //    this.Cursor = Cursors.Default;

        //}
        #endregion

        #region ----- DataGridView -----

        #region [Nhân viên]
        private void GVEmployee_Layout()
        {
            DataGridViewCheckBoxColumn zColumn;

            // Setup Column 
            GVEmployee.Columns.Add("No", "STT");
            GVEmployee.Columns.Add("EmployeeID", "Mã NV");
            GVEmployee.Columns.Add("Name", "Tên Nhân Viên");
            GVEmployee.Columns.Add("Basket", "Số Rổ");
            GVEmployee.Columns.Add("Kg", "Số Lượng");
            GVEmployee.Columns.Add("Time", "Thời Gian");
            GVEmployee.Columns.Add("Borrow", "Mượn Người");

            zColumn = new DataGridViewCheckBoxColumn();
            zColumn.Name = "Private";
            zColumn.HeaderText = "Ăn Riêng";
            GVEmployee.Columns.Add(zColumn);

            zColumn = new DataGridViewCheckBoxColumn();
            zColumn.Name = "General";
            zColumn.HeaderText = "Ăn Chung";
            GVEmployee.Columns.Add(zColumn);

            GVEmployee.Columns["No"].Width = 40;
            GVEmployee.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GVEmployee.Columns["EmployeeID"].Width = 70;
            GVEmployee.Columns["EmployeeID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GVEmployee.Columns["Name"].Width = 315;
            GVEmployee.Columns["Name"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GVEmployee.Columns["Name"].Frozen = true;

            GVEmployee.Columns["Basket"].Width = 100;
            GVEmployee.Columns["Basket"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GVEmployee.Columns["Kg"].Width = 100;
            GVEmployee.Columns["Kg"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GVEmployee.Columns["Time"].Width = 100;
            GVEmployee.Columns["Time"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GVEmployee.Columns["Borrow"].Width = 100;
            GVEmployee.Columns["Borrow"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GVEmployee.Columns["Borrow"].ReadOnly = true;

            GVEmployee.Columns["Private"].Width = 100;
            GVEmployee.Columns["Private"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GVEmployee.Columns["General"].Width = 100;
            GVEmployee.Columns["General"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }
        private void GVEmployee_LoadData()
        {
            GVEmployee.Rows.Clear();
            DataTable zTable = Employee_Data.ListNames(_TeamKey);

            int i = 0;
            foreach (DataRow nRow in zTable.Rows)
            {
                GVEmployee.Rows.Add();
                DataGridViewRow nRowView = GVEmployee.Rows[i];
                nRowView.Cells["No"].Tag = nRow["EmployeeKey"].ToString().Trim();
                nRowView.Cells["No"].Value = (i + 1).ToString();
                nRowView.Cells["EmployeeID"].Value = nRow["EmployeeID"].ToString().Trim();
                nRowView.Cells["Name"].Value = nRow["FullName"].ToString().Trim();
                nRowView.Cells["Borrow"].Value = "";
                nRowView.Cells["Basket"].Value = "0";
                nRowView.Cells["Kg"].Value = "0";
                nRowView.Cells["Time"].Value = "0";
                i++;
            }
        }
        private void GVEmployee_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            switch (GVEmployee.CurrentCell.ColumnIndex)
            {
                case 1: // autocomplete for product
                    DataGridViewTextBoxEditingControl te = (DataGridViewTextBoxEditingControl)e.Control;
                    te.AutoCompleteMode = AutoCompleteMode.None;
                    LoadDataToToolbox.AutoCompleteTextBox(te, "SELECT EmployeeID  FROM HRM_Employee WHERE RecordStatus < 99");
                    e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);
                    break;
            }
        }
        private void GVEmployee_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                DialogResult dlr = MessageBox.Show("Bạn có xóa thông tin này ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlr == DialogResult.Yes)
                {
                    for (int i = 0; i < GVEmployee.Rows.Count; i++)
                    {
                        if (GVEmployee.Rows[i].Selected)
                        {
                            if (GVEmployee.Rows[i].Tag != null)
                            {
                                Order_Employee_Info zOrder_Employee = (Order_Employee_Info)GVEmployee.Rows[i].Tag;
                                GVEmployee.Rows[i].Visible = false;
                                zOrder_Employee.RecordStatus = 99;
                            }
                        }
                    }
                }
            }
        }
        private void GVEmployee_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            int i = 0;
            Order_Employee_Info zOrder_Employee;
            if (GVEmployee.Rows[e.RowIndex].Tag == null)
            {
                zOrder_Employee = new Order_Employee_Info();
                GVEmployee.Rows[e.RowIndex].Tag = zOrder_Employee;
            }
            else
            {
                zOrder_Employee = (Order_Employee_Info)GVEmployee.Rows[e.RowIndex].Tag;
            }
            if (GVEmployee.CurrentCell.ColumnIndex == 1)
            {
                if (GVEmployee.CurrentCell.Value != null)
                {
                    string EmployeeID = GVEmployee.CurrentCell.Value.ToString();
                    Employee_Info zEm = new Employee_Info();
                    zEm.GetEmployee(EmployeeID);
                    if (zOrder_Employee.Key == 0)
                    {
                        if (zEm.Key == "")
                        {
                            MessageBox.Show("Không có tên nhân viên này", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            GVEmployee.CurrentRow.Cells[1].Value = "";
                            GVEmployee.CurrentRow.Cells[2].Value = "";
                            GVEmployee.CurrentRow.Cells[6].Value = "";
                        }
                        else
                        {
                            int nRowCount = Employee_Data.Count_CardID(EmployeeID, _TeamKey);
                            if (nRowCount > 0)
                            {
                                for (int y = 0; y < GVEmployee.Rows.Count - 2; y++)
                                {
                                    if (EmployeeID == GVEmployee.Rows[y].Cells["EmployeeID"].Value.ToString())
                                    {
                                        i++;
                                    }
                                }
                                if (i > 0)
                                {
                                    MessageBox.Show("Đã có trong danh sách", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    GVEmployee.CurrentRow.Cells[1].Value = "";
                                    GVEmployee.CurrentRow.Cells[2].Value = "";
                                    GVEmployee.CurrentRow.Cells[6].Value = "";
                                }
                                if (i == 0)
                                {
                                    nRowCount = Employee_Data.Count(_TeamKey, EmployeeID, _OrderKey);
                                    if (nRowCount > 0)
                                    {
                                        MessageBox.Show("Nhân viên này đã có rồi!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    }
                                    else
                                    {
                                        nRowCount = Employee_Data.Count_CardID(EmployeeID, _TeamKey);
                                        if (nRowCount > 0)
                                        {
                                            ShowProductInGridView(e.RowIndex);
                                            GVEmployee.CurrentRow.Cells[0].Tag = zEm.Key;
                                            GVEmployee.CurrentRow.Cells[2].Value = zEm.FullName;
                                        }
                                        else
                                        {
                                            ShowProductInGridView(e.RowIndex);
                                            GVEmployee.CurrentRow.Cells[0].Tag = zEm.Key;
                                            GVEmployee.CurrentRow.Cells[2].Value = zEm.FullName;
                                            GVEmployee.CurrentRow.Cells[6].Value = "X";
                                        }
                                    }
                                }
                            }
                            else
                            {
                                ShowProductInGridView(e.RowIndex);
                                GVEmployee.CurrentRow.Cells[0].Tag = zEm.Key;
                                GVEmployee.CurrentRow.Cells[2].Value = zEm.FullName;
                                GVEmployee.CurrentRow.Cells[6].Value = "X";
                            }
                        }
                        zOrder_Employee.RecordStatus = 1;
                    }
                    else
                    {
                        _AutoKey = zOrder_Employee.Key;
                        zOrder_Employee.RecordStatus = 2;
                        if (zEm.Key == "")
                        {
                            MessageBox.Show("Không có tên nhân viên này", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            GVEmployee.CurrentRow.Cells[1].Value = "";
                            GVEmployee.CurrentRow.Cells[2].Value = "";
                            GVEmployee.CurrentRow.Cells[6].Value = "";
                        }
                        else
                        {
                            int nRowCount = Employee_Data.Count_CardID(EmployeeID, _TeamKey);
                            if (nRowCount > 0)
                            {
                                nRowCount = Employee_Data.Count(_TeamKey, EmployeeID, _OrderKey);
                                if (nRowCount > 0)
                                {
                                    MessageBox.Show("Nhân viên này đã có rồi!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                                else
                                {
                                    ShowProductInGridView(e.RowIndex);
                                    GVEmployee.CurrentRow.Cells[0].Tag = zEm.Key;
                                    GVEmployee.CurrentRow.Cells[2].Value = zEm.FullName;
                                }
                            }
                            else
                            {
                                ShowProductInGridView(e.RowIndex);
                                GVEmployee.CurrentRow.Cells[0].Tag = zEm.Key;
                                GVEmployee.CurrentRow.Cells[2].Value = zEm.FullName;
                            }
                        }
                    }
                }
            }
            if (zOrder_Employee.Key == 0)
                zOrder_Employee.RecordStatus = 1;
            else
                zOrder_Employee.RecordStatus = 2;
        }
        private void ShowProductInGridView(int RowIndex)
        {
            DataGridViewRow zRowView = GVEmployee.Rows[RowIndex];
            zRowView.Cells["No"].Value = (RowIndex + 1).ToString();
        }
        #endregion

        #region [Hệ số]
        private void GVRate_Layout()
        {
            // Setup Column 
            GVRate.Columns.Add("No", "STT");
            GVRate.Columns.Add("ID", "Mã");
            GVRate.Columns.Add("CategoryName", "Hệ số");
            GVRate.Columns.Add("Rate", "Giá trị");

            GVRate.Columns["No"].Width = 40;
            GVRate.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GVRate.Columns["ID"].Width = 80;
            GVRate.Columns["ID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GVRate.Columns["CategoryName"].Width = 250;
            GVRate.Columns["CategoryName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GVRate.Columns["CategoryName"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GVRate.Columns["CategoryName"].ReadOnly = true;

            GVRate.Columns["Rate"].Width = 80;
            GVRate.Columns["Rate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GVRate.Columns["Rate"].ReadOnly = true;
        }
        private void GVRate_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {

            switch (GVRate.CurrentCell.ColumnIndex)
            {
                case 1: // autocomplete for product
                    DataGridViewTextBoxEditingControl te = (DataGridViewTextBoxEditingControl)e.Control;
                    te.AutoCompleteMode = AutoCompleteMode.None;
                    LoadDataToToolbox.AutoCompleteTextBox(te, "SELECT RateID FROM FTR_Category WHERE RecordStatus <> 99");
                    e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);
                    break;
            }
        }
        private void GVRate_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                DialogResult dlr = MessageBox.Show("Bạn có xóa thông tin này ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlr == DialogResult.Yes)
                {
                    if (GVRate.CurrentRow.Tag != null)
                    {
                        Order_Rate_Info zOrder_Rate = (Order_Rate_Info)GVRate.CurrentRow.Tag;
                        GVRate.CurrentRow.Visible = false;
                        zOrder_Rate.RecordStatus = 99;
                    }
                }
            }
        }
        private void GVRate_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            Order_Rate_Info zOrder_Rate;
            if (GVRate.Rows[e.RowIndex].Tag == null)
            {
                zOrder_Rate = new Order_Rate_Info();
                GVRate.Rows[e.RowIndex].Tag = zOrder_Rate;
            }
            else
            {
                zOrder_Rate = (Order_Rate_Info)GVRate.Rows[e.RowIndex].Tag;
            }
            if (GVRate.CurrentCell.ColumnIndex == 1)
            {
                string ID = GVRate.CurrentCell.Value.ToString();
                Category_Info zCategory = new Category_Info(ID, true);
                if (zCategory.AutoKey != 0)
                {
                    ShowProductInGridView_Rate(e.RowIndex);
                    GVRate.CurrentRow.Cells[0].Tag = zCategory.AutoKey;
                    GVRate.CurrentRow.Cells[2].Value = zCategory.RateName;
                    GVRate.CurrentRow.Cells[3].Value = zCategory.Rate;
                }
            }
            if (zOrder_Rate.Key == 0)
                zOrder_Rate.RecordStatus = 1;
            else
                zOrder_Rate.RecordStatus = 2;
        }
        private void Control_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = false;
        }
        private void ShowProductInGridView_Rate(int RowIndex)
        {
            DataGridViewRow zRowView = GVRate.Rows[RowIndex];
            zRowView.Cells["No"].Value = (RowIndex + 1).ToString();
        }
        #endregion

        #region [Sum]
        private void GVSum_Layout()
        {
            // Setup Column 
            GVSum.Columns.Add("SUM", "TỔNG");
            GVSum.Columns.Add("Sum_Basket", "Tổng Rổ");
            GVSum.Columns.Add("Sum_Kg", "Tổng KG");
            GVSum.Columns.Add("Sum_Time", "Tổng TG");

            GVSum.Columns["SUM"].Width = 385;
            GVSum.Columns["SUM"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GVSum.Columns["SUM"].ReadOnly = true;

            GVSum.Columns["Sum_Basket"].Width = 120;
            GVSum.Columns["Sum_Basket"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GVSum.Columns["Sum_Basket"].ReadOnly = true;

            GVSum.Columns["Sum_Kg"].Width = 100;
            GVSum.Columns["Sum_Kg"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GVSum.Columns["Sum_Kg"].ReadOnly = true;

            GVSum.Columns["Sum_Time"].Width = 100;
            GVSum.Columns["Sum_Time"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GVSum.Columns["Sum_Time"].ReadOnly = true;
        }
        private void GVSum_LoadData()
        {
            DataTable zTable = new DataTable();
            GVSum.Rows.Clear();
            zTable = Order_Employee_Data.Sum_Product(_OrderKey);
            int i = 0;
            foreach (DataRow nRow in zTable.Rows)
            {
                GVSum.Rows.Add();
                DataGridViewRow nRowView = GVSum.Rows[i];
                nRowView.Cells["Sum_Basket"].Value = nRow["Basket"].ToString();
                nRowView.Cells["Sum_Kg"].Value = nRow["Kilogram"].ToString();
                nRowView.Cells["Sum_Time"].Value = nRow["Time"].ToString();
                i++;
            }
        }
        #endregion
        #endregion

        #endregion

        #region ----- Process Data -----
        private void Check_New()
        {
            dte_OrderDate.Enabled = true;
            txt_OrderID.Enabled = true;
            txt_OrderIDFollow.Enabled = true;
            btn_Search_CT.Visible = true;
            txt_ProductID.Enabled = true;
            txt_ProductName.Enabled = true;
            txt_Stage.Enabled = true;
            txt_Price.Enabled = true;
            txt_QuantityDocument.Enabled = true;
            txt_QuantityReality.Enabled = true;
            txt_QuantityLose.Enabled = true;
            cbo_Status.Enabled = true;
            txt_Note.Enabled = true;
            txt_Unit.Enabled = true;
            GVRate.Enabled = true;
            GVEmployee.Enabled = true;
        }
        private void Load_GridView()
        {
            GVRate.Rows.Clear();
            for (int i = 0; i < _Order_Obj.List_Rate.Count; i++)
            {
                GVRate.Rows.Add();
                Order_Rate_Info zOrder_Rate = (Order_Rate_Info)_Order_Obj.List_Rate[i];
                GVRate.Rows[i].Tag = zOrder_Rate;
                GVRate.Rows[i].Cells["No"].Value = (i + 1).ToString();
                GVRate.Rows[i].Cells["CategoryName"].Value = zOrder_Rate.RateName;
                GVRate.Rows[i].Cells["Rate"].Value = zOrder_Rate.Rate;
            }
        }
        private void Load_GridView_Employee()
        {
            GVEmployee.Rows.Clear();
            for (int i = 0; i < _Order_Obj.List_Employee.Count; i++)
            {
                GVEmployee.Rows.Add();
                Order_Employee_Info zOrder_Employee = (Order_Employee_Info)_Order_Obj.List_Employee[i];
                GVEmployee.Rows[i].Tag = zOrder_Employee;
                GVEmployee.Rows[i].Cells["No"].Value = (i + 1).ToString();
                GVEmployee.Rows[i].Cells["No"].Tag = zOrder_Employee.EmployeeKey;
                GVEmployee.Rows[i].Cells["EmployeeID"].Value = zOrder_Employee.EmployeeID;
                GVEmployee.Rows[i].Cells["Name"].Value = zOrder_Employee.FullName;
                GVEmployee.Rows[i].Cells["Basket"].Value = zOrder_Employee.Basket;
                GVEmployee.Rows[i].Cells["Kg"].Value = zOrder_Employee.Kilogram;
                GVEmployee.Rows[i].Cells["Time"].Value = zOrder_Employee.Time;
                if (zOrder_Employee.Borrow == 1)
                {
                    GVEmployee.Rows[i].Cells["Borrow"].Value = "X";
                }
                else
                {
                    GVEmployee.Rows[i].Cells["Borrow"].Value = "";
                }
                if (zOrder_Employee.Share == 1)
                {
                    GVEmployee.Rows[i].Cells["General"].Value = true;
                }
                if (zOrder_Employee.Private == 1)
                {
                    GVEmployee.Rows[i].Cells["Private"].Value = true;
                }
            }
        }
        private void LoadData()
        {
            _Order_Obj = new Order_Object(OrderKey);
            txt_OrderID.Text = _Order_Obj.OrderID;

            if (_Order_Obj.OrderDate != DateTime.MinValue)
                dte_OrderDate.Value = _Order_Obj.OrderDate;
            else
                dte_OrderDate.Value = SessionUser.Date_Work;

            Product_Stages_Info zStages = new Product_Stages_Info(_Order_Obj.Category_Stage);
            txt_Stage.Tag = zStages.Key;
            txt_Stage.Text = zStages.StageName;
            txt_Price.Text = zStages.Price.ToString();

            _ProductKey = _Order_Obj.ProductKey;
            txt_ProductID.Text = _Order_Obj.ProductID;
            txt_ProductName.Text = _Order_Obj.ProductName;

            txt_QuantityDocument.Text = _Order_Obj.QuantityDocument.ToString();
            txt_QuantityReality.Text = _Order_Obj.QuantityReality.ToString();
            txt_QuantityLose.Text = _Order_Obj.QuantityLoss.ToString();

            txt_OrderIDFollow.Text = _Order_Obj.OrderIDFollow;
            cbo_Status.SelectedIndex = _Order_Obj.WorkStatus;

            txt_Unit.Text = _Order_Obj.UnitName;
            txt_Note.Text = _Order_Obj.Note;
            if (_OrderKey.Length > 0)
                txt_Percent.Text = _Order_Obj.PercentFinish.ToString();
            else
                txt_Percent.Text = "100";
            Load_GridView();
            Load_GridView_Employee();
            GVSum_LoadData();
            dte_OrderDate.Focus();
        }
        private string CheckBeforeSave()
        {
            string zAmountErr = "";
            if (_TeamKey != 98)
            {
                if (txt_ProductID.Text.Length == 0)
                {
                    txt_ProductID.BackColor = Color.FromArgb(230, 100, 100);
                    zAmountErr += "- Kiểm tra lại Mã Sản Phẩm \n";
                }
                else
                {
                    txt_ProductID.BackColor = Color.FromArgb(255, 255, 225);
                }

                if (txt_ProductName.Text.Length == 0)
                {
                    txt_ProductName.BackColor = Color.FromArgb(230, 100, 100);
                    zAmountErr += "- Kiểm tra lại Tên Sản Phẩm \n";
                }
                else
                {
                    txt_ProductName.BackColor = Color.FromArgb(255, 255, 225);
                }
            }
            float zQuantityDocument = float.Parse(txt_QuantityDocument.Text);
            float zQuantityRality = float.Parse(txt_QuantityReality.Text);
            if (zQuantityDocument < zQuantityRality)
            {
                txt_QuantityDocument.BackColor = Color.FromArgb(230, 100, 100);
                txt_QuantityReality.BackColor = Color.FromArgb(230, 100, 100);
                zAmountErr += "- Số lương chứng từ không được nhỏ hơn số lương thực tế \n";
            }
            else
            {
                txt_QuantityDocument.BackColor = Color.FromArgb(255, 255, 225);
                txt_QuantityReality.BackColor = Color.FromArgb(255, 255, 225);
            }

            int A = 0;
            int B = 0;
            for (int i = 0; i < GVRate.Rows.Count; i++)
            {
                if (GVRate.Rows[i].Cells["CategoryName"].Value != null && GVRate.Rows[i].Visible == true)
                {
                    A++;
                }
                if (GVRate.Rows[i].Cells["CategoryName"].Value == null && GVRate.Rows[i].Visible == true)
                {
                    B++;
                }
            }
            if (A == 0 && B > 0)
            {
                zAmountErr += "- Kiểm tra lại hệ số \n";
            }
            return zAmountErr;
        }
        private void Save()
        {
            _Order_Obj = new Order_Object(_OrderKey);

            _Order_Obj.OrderDate = dte_OrderDate.Value;
            _Order_Obj.TeamKey = TeamKey;

            _Order_Obj.ProductKey = _ProductKey;
            _Order_Obj.ProductID = txt_ProductID.Text;
            _Order_Obj.ProductName = txt_ProductName.Text;

            if (txt_Stage.Tag != null)
                _Order_Obj.Category_Stage = int.Parse(txt_Stage.Tag.ToString());
            else
                _Order_Obj.Category_Stage = 0;

            _Order_Obj.OrderIDFollow = txt_OrderIDFollow.Text;
            _Order_Obj.QuantityDocument = float.Parse(txt_QuantityDocument.Text);
            _Order_Obj.QuantityReality = float.Parse(txt_QuantityReality.Text);
            _Order_Obj.QuantityLoss = float.Parse(txt_QuantityLose.Text);

            _Order_Obj.UnitKey = _UnitKey;
            _Order_Obj.UnitName = txt_Unit.Text;

            _Order_Obj.WorkStatus = cbo_Status.SelectedIndex;
            _Order_Obj.Note = txt_Note.Text;
            _Order_Obj.PercentFinish = float.Parse(txt_Percent.Text);

            _Order_Obj.CreatedBy = SessionUser.UserLogin.Key;
            _Order_Obj.CreatedName = SessionUser.Personal_Info.FullName;
            _Order_Obj.ModifiedBy = SessionUser.UserLogin.Key;
            _Order_Obj.ModifiedName = SessionUser.Personal_Info.FullName;

            _List_Rate = new List<Order_Rate_Info>();

            for (int i = 0; i < GVRate.Rows.Count - 1; i++)
            {
                zOrder_Rate = (Order_Rate_Info)GVRate.Rows[i].Tag;
                if (GVRate.Rows[i].Cells["No"].Tag != null && GVRate.Rows[i].Visible == true)
                {
                    zOrder_Rate.CategoryKey = GVRate.Rows[i].Cells["No"].Tag.ToInt();
                    if (zOrder_Rate.Key == 0)
                    {
                        zOrder_Rate.RecordStatus = 1;
                    }
                }
                _List_Rate.Add(zOrder_Rate);
                _Order_Obj.List_Rate = _List_Rate;
            }


            _List_Employee = new List<Order_Employee_Info>();
            Order_Employee_Info zOrder_Employee;
            for (int i = 0; i < GVEmployee.Rows.Count; i++)
            {
                if (GVEmployee.Rows[i].Cells["No"].Tag != null)
                {
                    if (GVEmployee.Rows[i].Tag == null)
                    {
                        zOrder_Employee = new Order_Employee_Info();
                        GVEmployee.Rows[i].Tag = zOrder_Employee;
                        zOrder_Employee.RecordStatus = 1;
                    }
                    else
                    {
                        zOrder_Employee = (Order_Employee_Info)GVEmployee.Rows[i].Tag;
                    }
                    if (_AutoKey != 0)
                        zOrder_Employee.Key = _AutoKey;
                    zOrder_Employee.EmployeeID = GVEmployee.Rows[i].Cells["EmployeeID"].Value.ToString();
                    if (GVEmployee.Rows[i].Cells["No"].Tag != null)
                        zOrder_Employee.EmployeeKey = GVEmployee.Rows[i].Cells["No"].Tag.ToString();
                    if (GVEmployee.Rows[i].Cells["Basket"].Value != null)
                        zOrder_Employee.Basket = float.Parse(GVEmployee.Rows[i].Cells["Basket"].Value.ToString());
                    else
                        zOrder_Employee.Basket = 0;
                    if (GVEmployee.Rows[i].Cells["Kg"].Value != null)
                        zOrder_Employee.Kilogram = float.Parse(GVEmployee.Rows[i].Cells["Kg"].Value.ToString());
                    else
                        zOrder_Employee.Kilogram = 0;
                    if (GVEmployee.Rows[i].Cells["Time"].Value != null)
                        zOrder_Employee.Time = float.Parse(GVEmployee.Rows[i].Cells["Time"].Value.ToString());
                    else
                        zOrder_Employee.Time = 0;
                    if (GVEmployee.Rows[i].Cells["Borrow"].Value != null || GVEmployee.Rows[i].Cells["Borrow"].Value.ToString() != "")
                    {
                        zOrder_Employee.Borrow = 1;
                    }
                    if (GVEmployee.Rows[i].Cells["Borrow"].Value.ToString() == "" || GVEmployee.Rows[i].Cells["Borrow"].Value == null)
                    {
                        zOrder_Employee.Borrow = 0;
                    }

                    if (GVEmployee.Rows[i].Cells["General"].Value != null)
                    {
                        if (Convert.ToBoolean(GVEmployee.Rows[i].Cells["General"].Value) == true)
                        {
                            zOrder_Employee.Share = 1;
                        }
                        else
                        {
                            zOrder_Employee.Share = 0;
                        }
                    }

                    if (GVEmployee.Rows[i].Cells["Private"].Value != null)
                    {
                        if (Convert.ToBoolean(GVEmployee.Rows[i].Cells["Private"].Value) == true)
                        {
                            zOrder_Employee.Private = 1;
                        }
                        else
                        {
                            zOrder_Employee.Private = 0;
                        }
                    }


                    _List_Employee.Add(zOrder_Employee);
                    _Order_Obj.List_Employee = _List_Employee;
                }
            }

            _Order_Obj.Save_Object();
            string zMessage = TN_Message.Show(_Order_Obj.Message);
            if (zMessage.Length == 0)
            {
                MessageBox.Show("Câp nhật thành công", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                if (_Slug == 0)
                {
                    Stock_Input_Products_Info zInput = new Stock_Input_Products_Info();
                    zInput.AutoKey = _AutoKeyProduct;
                    zInput.UpdateStutas();
                }
                if (_Slug == 1)
                {
                    Stock_Output_Products_Info zOuput = new Stock_Output_Products_Info();
                    zOuput.AutoKey = _AutoKeyProduct;
                    zOuput.UpdateStutas();
                }
            }
            else
            {
                MessageBox.Show("Vui lòng kiểm tra lại", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        #region ----- Process Event -----
        private void Btn_Add_Click(object sender, EventArgs e)
        {
            int zCount = 0;
            for (int i = 0; i < GVEmployee.Rows.Count; i++)
            {
                if (GVEmployee.Rows[i].Cells["Borrow"].Value != null && GVEmployee.Rows[i].Cells["Borrow"].Value.ToString() != "")
                {
                    if (GVEmployee.Rows[i].Cells["Private"].Value == null && GVEmployee.Rows[i].Cells["General"].Value == null)
                    {
                        zCount = 1;
                    }
                }
            }
            if (zCount == 1)
            {
                MessageBox.Show("Vui lòng chọn lại nhân viên mượn Ăn riêng hoặc ăn chung", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if (CheckBeforeSave().Length == 0)
                {
                    Save();
                    _OrderKey = _Order_Obj.Key;
                    LoadData();
                }
                else
                {
                    MessageBox.Show("Vui lòng kiểm tra lại thông tin : \n" + CheckBeforeSave(), "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void Txt_ProductID_Leave(object sender, EventArgs e)
        {
            if (txt_ProductID.Text.Length > 0)
            {
                Product_Info zProduct = new Product_Info();
                zProduct.Get_Product_Info_ID(txt_ProductID.Text);
                txt_ProductName.Text = zProduct.ProductName;
            }
        }
        private void Txt_ProductName_Leave(object sender, EventArgs e)
        {
            if (txt_ProductName.Text.Length > 0)
            {
                Product_Info zProduct = new Product_Info();
                zProduct.Get_Product_Info(txt_ProductName.Text);
                txt_ProductID.Text = zProduct.ProductID;
            }
        }
        private void Btn_Search_CT_Click(object sender, EventArgs e)
        {
            Frm_Search_Order frm = new Frm_Search_Order();
            frm.ShowDialog();
            _OrderID = frm.OrderID;
            _ProductKey = frm.ProductKey;
            txt_OrderIDFollow.Text = _OrderID;
            txt_ProductName.Text = frm.ProductName;
            txt_ProductID.Text = frm.ProductID;
            _AutoKeyProduct = frm.Autokey;
            _Slug = frm.Slug;
            float _QuantityDocument = frm.Quantity_Document;
            txt_QuantityDocument.Text = _QuantityDocument.ToString();
            _UnitKey = frm.UnitKey;
            txt_Unit.Text = frm.UnitName;

        }
        private void Txt_Stage_KeyUp(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.Down)
            //{
            //    LV_Search.Focus();
            //    if (LV_Search.Items.Count > 0)
            //        LV_Search.Items[0].Selected = true;
            //}
            //else
            //{
            //    if (e.KeyCode != Keys.Enter)
            //    {
            //        if (txt_Stage.Text.Length > 0)
            //        {
            //            LV_Search.Visible = true;
            //            LV_SearchLoadData();
            //        }
            //        else
            //            LV_Search.Visible = false;
            //    }
            //}
        }
        private void Txt_Stage_Leave(object sender, EventArgs e)
        {
            //if (!LV_Search.Focused)
            //    LV_Search.Visible = false;
        }
        private void LV_Search_ItemActivate(object sender, EventArgs e)
        {
            //txt_Stage.Tag = LV_Search.SelectedItems[0].Tag;
            //LV_Search.Visible = false;
            //txt_Stage.Focus();
        }
        private void LV_Search_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            //txt_Stage.Text = LV_Search.Items[e.ItemIndex].SubItems[1].Text.Trim();
            //txt_Stage.Tag = LV_Search.Items[e.ItemIndex].Tag;
            //txt_Price.Text = LV_Search.Items[e.ItemIndex].SubItems[2].Text.Trim();
        }
        private void LV_Search_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.Enter)
            //{
            //    txt_Stage.Tag = LV_Search.SelectedItems[0].Tag;
            //    LV_Search.Visible = false;
            //}
        }
        private void Btn_New_Click(object sender, EventArgs e)
        {
            _OrderKey = "";
            _Order_Obj = new Order_Object(_OrderKey);
            LoadData();
            GVEmployee_LoadData();
            Check_New();
            DataRow zAdd = User_Data.GetUserAdd(SessionUser.UserLogin.Key, _RoleID).Rows[0];
            if (Convert.ToInt32(zAdd[0]) > 0)
            {
                btn_Add.Visible = true;
            }
            else
            {
                btn_Add.Visible = false;
            }
        }
        private void Txt_Unit_Leave(object sender, EventArgs e)
        {
            Product_Unit_Info zUnit = new Product_Unit_Info(txt_Unit.Text);
            _UnitKey = zUnit.Key;
        }
        private void Txt_QuantityReality_Leave(object sender, EventArgs e)
        {
            MathFunctions.MathParser mp = new MathFunctions.MathParser();
            decimal zQuantity = 0;
            decimal zQuantityDocument = 0;
            decimal zLoss = 0;
            if (txt_QuantityDocument.Text.Length > 0)
            {
                zQuantity = mp.Calculate(txt_QuantityReality.Text);
            }
            if (txt_QuantityDocument.Text.Length > 0)
            {
                zQuantityDocument = decimal.Parse(txt_QuantityDocument.Text);
            }

            if (txt_QuantityDocument.Text.Length > 0)
            {
                zLoss = zQuantityDocument - zQuantity;
            }
            txt_QuantityReality.Text = zQuantity.ToString();

            txt_QuantityLose.Text = zLoss.ToString();
        }
        private void Btn_Choose_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }
        //------ Khai bao combobox
        private int _AutoKeyCategory = 0;
        private string _CategoryName = "";
        private float _Rate = 0;
        private void Cbo_Category_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (cbo_Category.SelectedIndex != 0)
            //{
            //    Category_Info zCategory = new Category_Info((int)cbo_Category.SelectedValue);
            //    _AutoKeyCategory = (int)cbo_Category.SelectedValue;
            //    _CategoryName = zCategory.RateName;
            //    _Rate = zCategory.Rate;
            //}
        }
        private void Btn_AddCate_Click(object sender, EventArgs e)
        {
            if (_AutoKeyCategory != 0)
            {
                for (int i = 0; i < GVRate.Rows.Count; i++)
                {
                    if (GVRate.Rows[i].Cells["CategoryName"].Value == null)
                    {
                        GVRate.Rows.Add();
                        GVRate.Rows[i].Cells["No"].Value = (i + 1);
                        GVRate.Rows[i].Cells["No"].Tag = _AutoKeyCategory;
                        GVRate.Rows[i].Cells["CategoryName"].Value = _CategoryName;
                        GVRate.Rows[i].Cells["Rate"].Value = _Rate;
                        if (GVRate.Rows[i].Tag == null)
                        {
                            zOrder_Rate = new Order_Rate_Info();
                            GVRate.Rows[i].Tag = zOrder_Rate;
                        }
                        zOrder_Rate.RecordStatus = 1;
                        break;

                    }
                }
            }
        }
        private bool _IsPostback = false;
        private void Btn_Often_Click(object sender, EventArgs e)
        {
            _IsPostback = false;
            OpenFileDialog zOpf = new OpenFileDialog();

            zOpf.InitialDirectory = @"C:\";
            zOpf.Title = "Browse Excel Files";

            zOpf.CheckFileExists = true;
            zOpf.CheckPathExists = true;

            zOpf.DefaultExt = "txt";
            zOpf.Filter = "All Files|*.*";
            zOpf.FilterIndex = 2;
            zOpf.RestoreDirectory = true;

            zOpf.ReadOnlyChecked = true;
            zOpf.ShowReadOnly = true;

            if (zOpf.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    txt_Excel.Text = zOpf.FileName;
                    System.IO.FileInfo zFileImport = new System.IO.FileInfo(zOpf.FileName);
                    txt_Excel.Tag = zFileImport.Name;
                    ExcelPackage package = new OfficeOpenXml.ExcelPackage(zFileImport);

                    foreach (var sheet in package.Workbook.Worksheets)
                    {
                        cbo_Sheet.Items.Add(sheet.Name);
                    }
                    package.Dispose();

                    // DataTable zTable = OfficeOpenXml.FormulaParsing.Excel.ImportToDataTable(txt_Excel.Text, cbo_Sheet.Text);

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
            _IsPostback = true;
        }
        Employee_Info _Employee;
        private string _CardID = "";
        private void GVEmployee_LoadData(DataTable InTable)
        {
            if (_OrderKey.Length > 0)
            {
                Order_Employee_Info zEmployee = new Order_Employee_Info();
                zEmployee.OrderKey = _OrderKey;
                zEmployee.Delete_All();
            }
            GVEmployee.Rows.Clear();
            int i = 0;
            foreach (DataRow nRow in InTable.Rows)
            {
                GVEmployee.Rows.Add();
                DataGridViewRow nRowView = GVEmployee.Rows[i];
                _CardID = nRow[1].ToString().Trim();
                _Employee = new Employee_Info();
                _Employee.GetEmployee(_CardID);
                nRowView.Cells["No"].Value = (i + 1).ToString();
                nRowView.Cells["No"].Tag = _Employee.Key;
                nRowView.Cells["EmployeeID"].Value = nRow[1].ToString().Trim();
                nRowView.Cells["Name"].Value = nRow[2].ToString().Trim();
                float zBasket = 0;
                float.TryParse(nRow[3].ToString().Trim(), out zBasket);
                nRowView.Cells["Basket"].Value = zBasket;

                float zKg = 0;
                float.TryParse(nRow[4].ToString().Trim(), out zKg);
                nRowView.Cells["Kg"].Value = zKg;

                float zTime = 0;
                float.TryParse(nRow[5].ToString().Trim(), out zTime);
                nRowView.Cells["Time"].Value = zTime;

                int zBorrow = 0;
                int.TryParse(nRow[6].ToString().Trim(), out zBorrow);
                if (zBorrow == 0)
                    nRowView.Cells["Borrow"].Value = "";
                else
                    nRowView.Cells["Borrow"].Value = "X";

                int zPrivate = 0;
                int.TryParse(nRow[7].ToString().Trim(), out zPrivate);
                nRowView.Cells["Private"].Value = zPrivate;

                int zGeneral = 0;
                int.TryParse(nRow[8].ToString().Trim(), out zGeneral);
                nRowView.Cells["General"].Value = zGeneral;
                i++;
            }
        }

        #endregion

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        private void btn_Search_Work_Click(object sender, EventArgs e)
        {
            Frm_Setup_Work Frm = new Frm_Setup_Work();
            Frm.TeamKey = _TeamKey;
            Frm.SearchStyle = true;
            Frm.ShowDialog();

            Product_Stages_Info zInfo = Frm.Product_Stages;
            if (zInfo != null)
            {
                txt_Stage.Tag = zInfo.Key;
                txt_Stage.Text = zInfo.StageName;
                txt_Price.Text = zInfo.Price.Ton1String();
            }
        }

        private void Panel_Left_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
