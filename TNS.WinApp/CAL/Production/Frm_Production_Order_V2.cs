﻿using OfficeOpenXml;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using TN.Toolbox;
using TNS.CORE;
using TNS.HRM;
using TNS.IVT;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Production_Order_V2 : Form
    {
        Color ColorWarning = Color.FromArgb(199, 21, 112);
        Color ColorNormal = Color.FromArgb(255, 255, 225);
        int _TeamKey_Search = 0;
        bool _IsNew = false;
        string _OrderKey = "";
        Order_Object _Order;
        public Frm_Production_Order_V2()
        {
            InitializeComponent();

            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            LV_Product.ItemSelectionChanged += LV_Product_ItemSelectionChanged;
            LV_Product.ItemActivate += LV_Product_ItemActivate;
            LV_Product.KeyDown += LV_Product_KeyDown;

            cbo_Team.SelectedIndexChanged += cbo_Team_SelectedIndexChanged;
            cbo_TeamID_Search.SelectedIndexChanged += cbo_TeamID_Search_SelectedIndexChanged;

            GVOrder.SelectionChanged += GVOrder_SelectionChanged;
            GVOrder.KeyDown += GVOrder_KeyDown;

            GVEmployee.KeyDown += GVEmployee_KeyDown;
            GVEmployee.RowValidated += GVEmployee_RowValidated;
            GVEmployee.CellEndEdit += GVEmployee_CellEndEdit;

            GVRate.EditingControlShowing += GVRate_EditingControlShowing;
            GVRate.CellEndEdit += GVRate_CellEndEdit;
            GVRate.KeyDown += GVRate_KeyDown;

            txt_OrderID.Leave += txt_OrderID_Leave;
            txt_OrderIDFollow.Leave += txt_OrderIDFollow_Leave;
            txt_WorkID.Leave += txt_WorkID_Leave;
            txt_ProductID.Leave += txt_ProductID_Leave;
            txt_Search_ProductID.KeyUp += txt_Search_ProductID_KeyUp;
            txt_Search_ProductID.Leave += txt_Search_ProductID_Leave;
            txt_Search_WorkID.Leave += txt_Search_WorkID_Leave;

            btn_SearchInput_Document.Click += btn_SearchInput_Document_Click;
            btn_SearchInput_Work.Click += btn_SearchInput_Work_Click;
            btnClose_Panel_Message.Click += btnClose_Panel_Message_Click;

            txt_QuantityReality.Leave += Txt_QuantityReality_Leave;
            txt_QuantityLose.Leave += Txt_QuantityLose_Leave;
            txt_QuantityDocument.Leave += Txt_QuantityDocument_Leave;

            txt_QuantityDocument.Leave += (o, e) => { QuantityCalculator(); };
            txt_QuantityLose.Leave += (o, e) => { QuantityCalculator(); };
            txt_QuantityReality.Leave += (o, e) => { QuantityCalculator(); };

            txt_QuantityDocument.KeyPress += txt_Number_KeyPress;
            txt_QuantityLose.KeyPress += txt_Number_KeyPress;
            txt_QuantityReality.KeyPress += txt_Number_KeyPress;

            btn_DeleteAll.Click += btn_DeleteAllOrder_Click;
            btn_Search.Click += Btn_Search_Click;
            btn_Export.Click += Btn_Export_Click;
        }

        
        private void Frm_Import_Order_V2_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            this.DoubleBuffered = true;
            Utils.DoubleBuffered(GVOrder, true);
            Utils.DoubleBuffered(GVRate, true);

            Utils.DrawGVStyle(ref GVRate);
            Utils.DrawGVStyle(ref GVOrder);

            btn_Show.Visible = true;
            btn_Hide.Visible = false;
            btn_DeleteOrder.Enabled = false;
            Panel_Employee.Visible = false;
            Panel_Info.Visible = false;

            splitContainer1.SplitterDistance = Screen.PrimaryScreen.Bounds.Width;
            splitContainer1.Panel1MinSize = 320;
            splitContainer1.Panel2MinSize = 0;

            Layout_Product();
            SetupLayoutGVEmployee();
            SetupLayoutGVOrder();
            SetupLayoutGVRate();
            //Lấy tất cả nhóm trực tiếp sx  và khác bộ phận hỗ trợ sx
            string zSQL = @"SELECT TeamKey,TeamID +'-'+ TeamName AS TeamName  FROM SYS_Team 
                            WHERE RecordStatus <> 99 AND BranchKey=4 AND  DepartmentKey != 26
                            ORDER BY Rank";
            LoadDataToToolbox.KryptonComboBox(cbo_TeamID_Search, zSQL, "---Chọn---");
            LoadDataToToolbox.KryptonComboBox(cbo_Team, zSQL, "---Chọn---");

            dte_OrderDate.Value = SessionUser.Date_Work;
            dte_FromDate.Value = SessionUser.Date_Work;
            dte_ToDate.Value = SessionUser.Date_Work;


            ArrayList ListItems = new ArrayList();
            TN_Item li = new TN_Item();
            li.Value = -1;
            li.Name = "--Chọn--";
            ListItems.Add(li);
            li = new TN_Item();
            li.Value = 0;
            li.Name = "Đang thực hiện";
            ListItems.Add(li);
            li = new TN_Item();
            li.Value = 1;
            li.Name = "Đã thực hiện";
            ListItems.Add(li);

            cbo_Status.DataSource = ListItems;
            cbo_Status.DisplayMember = "Name";
            cbo_Status.ValueMember = "Value";

            DataTable zTable = Order_Data.List_Search(dte_FromDate.Value, dte_ToDate.Value, "", "", "", "", 0);
            LoadGVOrder(zTable);
        }
        private void Txt_QuantityDocument_Leave(object sender, EventArgs e)
        {
            double zAmount = 0;
            if (double.TryParse(txt_QuantityDocument.Text.Trim(), out zAmount))
            {
                txt_QuantityDocument.Text = zAmount.Toe1String();
                //txt_QuantityDocument.Focus();
                txt_QuantityDocument.SelectionStart = txt_QuantityDocument.Text.Length;
            }
            else
            {
                Utils.TNMessageBoxOK("Lỗi!.Định dạng số.",1);
            }
        }

        private void Txt_QuantityLose_Leave(object sender, EventArgs e)
        {
            double zAmount = 0;
            if (double.TryParse(txt_QuantityLose.Text.Trim(), out zAmount))
            {
                txt_QuantityLose.Text = zAmount.Toe1String();
                //txt_QuantityLose.Focus();
                txt_QuantityLose.SelectionStart = txt_QuantityLose.Text.Length;
            }
            else
            {
                Utils.TNMessageBoxOK("Lỗi!.Định dạng số.",1);
            }
        }

        private void Txt_QuantityReality_Leave(object sender, EventArgs e)
        {
            double zAmount = 0;
            if (double.TryParse(txt_QuantityReality.Text.Trim(), out zAmount))
            {
                txt_QuantityReality.Text = zAmount.Toe1String();
                //txt_QuantityReality.Focus();
                txt_QuantityReality.SelectionStart = txt_QuantityReality.Text.Length - 2;
            }
            else
            {
                Utils.TNMessageBoxOK("Lỗi!.Định dạng số.",1);
            }
        }
        #region[Layout and Event GV]
        private void Layout_Product()
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 0;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV_Product.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã sản phẩm";
            colHead.Width = 127;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV_Product.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên sản phẩm";
            colHead.Width = -1;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV_Product.Columns.Add(colHead);
        }
        private void SetupLayoutGVOrder()
        {
            // Setup Column 
            GVOrder.Columns.Add("No", "STT");
            GVOrder.Columns.Add("OrderID_Compare", "Mã Tham Chiếu");
            GVOrder.Columns.Add("OrderID", "Mã Đơn Hàng");
            GVOrder.Columns.Add("OrderDate", "Ngày");
            GVOrder.Columns.Add("TeamID", "Mã Nhóm");
            GVOrder.Columns.Add("TeamName", "Tên Nhóm");
            GVOrder.Columns.Add("ProductID", "Mã Sản Phẩm");
            GVOrder.Columns.Add("ProductName", "Tên Sản Phẩm");
            GVOrder.Columns.Add("StageID", "Mã Công Đoạn");
            GVOrder.Columns.Add("StageName", "Tên Công Đoạn");
            GVOrder.Columns.Add("StagePrice", "Đơn Giá");
            GVOrder.Columns.Add("OrderIDFollow", "Số Chứng Từ");
            GVOrder.Columns.Add("QuantityDocument", "SL Chứng Từ");
            GVOrder.Columns.Add("QuantityReality", "SL Thực Tế");
            GVOrder.Columns.Add("QuantityLose", "SL Hao Hụt");
            GVOrder.Columns.Add("Unit", "Đơn Vị");
            GVOrder.Columns.Add("WorkStatus", "Tình Trạng");
            GVOrder.Columns.Add("Percent", "Tỷ lệ HT");
            GVOrder.Columns.Add("Note", "Ghi Chú");
            GVOrder.Columns.Add("Message", "Thông Báo");
            GVOrder.Columns.Add("ProductType", "Loại Sản Phẩm");
            GVOrder.Columns.Add("RateID", "Hệ số");

            GVOrder.Columns["No"].Width = 40;
            GVOrder.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GVOrder.Columns["OrderID_Compare"].Width = 150;
            GVOrder.Columns["OrderID_Compare"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GVOrder.Columns["OrderID"].Width = 170;
            GVOrder.Columns["OrderID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GVOrder.Columns["OrderDate"].Width = 100;
            GVOrder.Columns["OrderDate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GVOrder.Columns["TeamID"].Width = 100;
            GVOrder.Columns["TeamID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GVOrder.Columns["TeamID"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GVOrder.Columns["TeamName"].Width = 250;
            GVOrder.Columns["TeamName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GVOrder.Columns["TeamName"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GVOrder.Columns["ProductID"].Width = 120;
            GVOrder.Columns["ProductID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GVOrder.Columns["ProductName"].Width = 200;
            GVOrder.Columns["ProductName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GVOrder.Columns["StageID"].Width = 120;
            GVOrder.Columns["StageID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GVOrder.Columns["StageID"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GVOrder.Columns["StageName"].Width = 300;
            GVOrder.Columns["StageName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GVOrder.Columns["StageName"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GVOrder.Columns["StagePrice"].Width = 80;
            GVOrder.Columns["StagePrice"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GVOrder.Columns["OrderIDFollow"].Width = 120;
            GVOrder.Columns["OrderIDFollow"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GVOrder.Columns["QuantityDocument"].Width = 120;
            GVOrder.Columns["QuantityDocument"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GVOrder.Columns["QuantityReality"].Width = 120;
            GVOrder.Columns["QuantityReality"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GVOrder.Columns["QuantityLose"].Width = 120;
            GVOrder.Columns["QuantityLose"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GVOrder.Columns["Unit"].Width = 90;
            GVOrder.Columns["Unit"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GVOrder.Columns["WorkStatus"].Width = 120;
            GVOrder.Columns["WorkStatus"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GVOrder.Columns["Percent"].Width = 90;
            GVOrder.Columns["Percent"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GVOrder.Columns["Note"].Width = 200;
            GVOrder.Columns["Note"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GVOrder.Columns["Message"].Width = 300;
            GVOrder.Columns["Message"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GVOrder.Columns["ProductType"].Width = 120;
            GVOrder.Columns["ProductType"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GVOrder.Columns["RateID"].Width = 120;
            GVOrder.Columns["RateID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GVOrder.ReadOnly = true;
        }
        private void SetupLayoutGVRate()
        {
            GVRate.Columns.Add("RateID", "Mã");
            GVRate.Columns.Add("RateName", "Hệ số");
            GVRate.Columns.Add("Rate", "Giá trị");

            GVRate.Columns["RateID"].Width = 90;
            GVRate.Columns["RateID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GVRate.Columns["RateName"].Width = 250;
            GVRate.Columns["RateName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GVRate.Columns["RateName"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GVRate.Columns["RateName"].ReadOnly = true;

            GVRate.Columns["Rate"].Width = 50;
            GVRate.Columns["Rate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GVRate.Columns["Rate"].ReadOnly = true;
        }
        private void SetupLayoutGVEmployee()
        {
            DataTable zTable = Employee_Data.Load_Data("EmployeeKey, EmployeeID, FullName");
            string[] zHeaderLV = new string[] { "Mã NV", "Tên" };

            GVEmployee.ColumnAdd("No", "STT", 40);

            GV_Column zCol = new GV_Column("EmployeeID", "ListView", 70, 400);
            zCol.TableSource = zTable;
            zCol.FilterColumn = true;
            zCol.CheckEmpty = true;
            zCol.IsPrimaryID = true;
            zCol.ShowColumn = 1;
            zCol.DisplayNextColumn = "FullName";
            zCol.HeaderText = "Mã NV";
            zCol.InitColumnLV(zHeaderLV);
            zCol.LoadDataToLV();
            GVEmployee.ColumnAdd(zCol);

            GVEmployee.ColumnAdd("FullName", "Họ tên", 200);

            zCol = new GV_Column();
            zCol.IsSum = true;
            zCol.CheckEmpty = true;
            zCol.ColumnName = "Basket";
            zCol.HeaderText = "Số Rổ";
            zCol.Style = "Number";
            GVEmployee.ColumnAdd(zCol);

            zCol = new GV_Column();
            zCol.IsSum = true;
            zCol.CheckEmpty = true;
            zCol.ColumnName = "Kg";
            zCol.HeaderText = "Số Lượng NL";
            zCol.Style = "Number";
            GVEmployee.ColumnAdd(zCol);

            zCol = new GV_Column();
            zCol.IsSum = true;
            zCol.CheckEmpty = true;
            zCol.ColumnName = "Time";
            zCol.HeaderText = "Thời Gian";
            zCol.Style = "Number";
            GVEmployee.ColumnAdd(zCol);

            zCol = new GV_Column();
            zCol.ColumnName = "Borrow";
            zCol.HeaderText = "Mượn Người";
            zCol.Style = "CheckBox";
            GVEmployee.ColumnAdd(zCol);

            zCol = new GV_Column();
            zCol.ColumnName = "Private";
            zCol.HeaderText = "Ăn Riêng";
            zCol.Style = "CheckBox";
            GVEmployee.ColumnAdd(zCol);

            zCol = new GV_Column();
            zCol.ColumnName = "General";
            zCol.HeaderText = "Ăn Chung";
            zCol.Style = "CheckBox";
            GVEmployee.ColumnAdd(zCol);

            GVEmployee.ColumnAdd("Message", "Thông báo");

            GVEmployee.FillLastColumn = true;
            GVEmployee.IsSum = true;
            GVEmployee.AllowAddRow = true;
            GVEmployee.Init();
            GVEmployee.GetSum(false);
        }
        private void GVOrder_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                if (Utils.TNMessageBox("Bạn có xóa hết thông tin đơn hàng này ?", 2) == "Y")
                {
                    if (GVOrder.CurrentRow.Tag != null)
                    {
                        bool IsDeleted = false;
                        string Message = "";
                        _OrderKey = GVOrder.CurrentRow.Tag.ToString();
                        Order_Money_Info zInfo = new Order_Money_Info();
                        zInfo.OrderKey = _OrderKey;
                        zInfo.Delete_Order();
                        if (zInfo.Message.Length == 0)
                        {
                            _Order = new Order_Object();
                            _Order.Key = _OrderKey;
                            _Order.Delete_Object();

                            if (_Order.Message.Substring(0, 2) == "30")
                            {
                                _OrderKey = string.Empty;
                                IsDeleted = true;
                                GVOrder.ClearSelection();
                                GVOrder.Rows.Remove(GVOrder.CurrentRow);
                            }
                            else
                            {
                                Message += _Order.Message;
                            }
                        }
                        else
                        {
                            Message += zInfo.Message;
                        }

                        if (!IsDeleted)
                        {
                            Utils.TNMessageBoxOK(Message,4);
                        }
                    }
                }
            }
        }
        private void GVOrder_SelectionChanged(object sender, EventArgs e)
        {
            if (GVOrder.SelectedRows.Count > 0 &&
                GVOrder.CurrentRow.Tag != null)
            {
                _IsNew = false;
                _OrderKey = GVOrder.CurrentRow.Tag.ToString();

                GVRate.Rows.Clear();
                GVEmployee.Rows.Clear();
                LoadOrderInfo(_OrderKey, false);

                btn_DeleteOrder.Enabled = true;
                btn_SaveOrder.Enabled = true;
                btn_CopyOrder.Enabled = true;
            }
            else
            {
                btn_DeleteOrder.Enabled = false;
                btn_SaveOrder.Enabled = false;
                btn_CopyOrder.Enabled = false;
            }
        }
        private void GVEmployee_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                if (Utils.TNMessageBox("Bạn có muốn xóa thông tin này?",2) == "Y")
                {
                    int Index = GVEmployee.CurrentCell.RowIndex;
                    DataGridViewRow Gvrow = GVEmployee.Rows[Index];

                    if (Gvrow.Tag != null)
                    {
                        GVEmployee.CurrentRow.Visible = false;
                        Order_Employee_Info zOrder_Employee = (Order_Employee_Info)Gvrow.Tag;
                        zOrder_Employee.RecordStatus = 99;
                    }
                    else
                    {
                        GVEmployee.Rows.Remove(Gvrow);
                    }

                    GVEmployee.GetSum(true);
                }
            }
        }
        private void GVEmployee_RowValidated(object sender, DataGridViewCellEventArgs e)
        {
            //for (int i = 0; i < GVEmployee.Rows.Count - 1; i++)
            //{
            //    GVEmployee.Rows[i].Cells[0].Value = (i + 1);
            //}
        }
        private void GVEmployee_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 1)
            {
                if (GVEmployee.Rows[e.RowIndex].Cells["EmployeeID"].Value != null)
                {
                    Order_Employee_Info zOrderEmployee;
                    if (GVEmployee.Rows[e.RowIndex].Tag == null)
                    {
                        zOrderEmployee = new Order_Employee_Info();
                        GVEmployee.Rows[e.RowIndex].Tag = zOrderEmployee;
                    }
                    else
                    {
                        zOrderEmployee = new Order_Employee_Info();
                    }

                    string ID = GVEmployee.Rows[e.RowIndex].Cells["EmployeeID"].Value.ToString();
                    int nRowCount = Employee_Data.Count(cbo_Team.SelectedValue.ToInt(), ID, _OrderKey);

                    if (nRowCount > 0)
                    {
                        Utils.TNMessageBoxOK("Nhân viên này đã có rồi!",1);
                    }
                    else
                    {
                        Employee_Info zInfo = new Employee_Info();
                        zInfo.GetEmployeeID(ID);
                        zOrderEmployee.EmployeeKey = zInfo.Key;
                        GVEmployee.Rows[e.RowIndex].Cells["EmployeeID"].Tag = zInfo.Key;
                        GVEmployee.Rows[e.RowIndex].Cells["FullName"].Value = zInfo.FullName;
                        nRowCount = Employee_Data.Count_CardID(ID, cbo_Team.SelectedValue.ToInt(),dte_OrderDate.Value);
                        if (nRowCount == 0)
                        {
                            GVEmployee.CurrentRow.Cells["Borrow"].Value = true;
                        }
                        else
                        {
                            GVEmployee.CurrentRow.Cells["Borrow"].Value = false;
                        }
                    }
                    if (zOrderEmployee.Key == 0)
                        zOrderEmployee.RecordStatus = 1;
                    else
                        zOrderEmployee.RecordStatus = 2;
                }
            }
        }
        private void GVRate_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            switch (GVRate.CurrentCell.ColumnIndex)
            {
                case 0:
                    DataGridViewTextBoxEditingControl te = (DataGridViewTextBoxEditingControl)e.Control;
                    te.AutoCompleteMode = AutoCompleteMode.None;
                    LoadDataToToolbox.AutoCompleteTextBox(te, "SELECT RateID FROM FTR_Category WHERE RecordStatus <> 99");
                    e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);
                    break;
            }
        }
        private void GVRate_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                if (Utils.TNMessageBox("Bạn có muốn xóa thông tin này?",2)=="Y")
                {
                    if (GVRate.CurrentRow.Tag != null)
                    {
                        Order_Rate_Info zOrder_Rate = (Order_Rate_Info)GVRate.CurrentRow.Tag;
                        GVRate.CurrentRow.Visible = false;
                        zOrder_Rate.RecordStatus = 99;
                    }
                }
            }
        }
        private void GVRate_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            Order_Rate_Info zOrderRate;
            if (GVRate.Rows[e.RowIndex].Tag == null)
            {
                zOrderRate = new Order_Rate_Info();
                GVRate.Rows[e.RowIndex].Tag = zOrderRate;
            }
            else
            {
                zOrderRate = (Order_Rate_Info)GVRate.Rows[e.RowIndex].Tag;
            }
            if (GVRate.CurrentCell.ColumnIndex == 0)
            {
                string ID = GVRate.CurrentCell.Value.ToString();
                Category_Info zCategory = new Category_Info(ID, true);
                if (zCategory.AutoKey != 0)
                {
                    GVRate.CurrentRow.Cells["RateID"].Tag = zCategory.AutoKey;
                    GVRate.CurrentRow.Cells["RateName"].Value = zCategory.RateName;
                    GVRate.CurrentRow.Cells["Rate"].Value = zCategory.Rate.ToString("n2");

                    //zOrderRate.RateID = zCategory.RateID;
                    zOrderRate.RateName = zCategory.RateName;
                    zOrderRate.Rate = zCategory.Rate;
                }
            }
            if (zOrderRate.Key == 0)
                zOrderRate.RecordStatus = 1;
            else
                zOrderRate.RecordStatus = 2;

            GVRate.Rows[e.RowIndex].Tag = zOrderRate;
        }
        private void Control_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = false;
        }
        #endregion

        #region [Function]        
        private void LoadLVProduct()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LV_Product;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            LV.Items.Clear();
            DataTable zTable = Product_Data.SearchProduct(txt_Search_ProductID.Text.Trim());
            if (zTable.Rows.Count > 0)
            {
                int n = zTable.Rows.Count;
                for (int i = 0; i < n; i++)
                {
                    lvi = new ListViewItem();
                    lvi.Text = (i + 1).ToString();
                    DataRow nRow = zTable.Rows[i];
                    lvi.Tag = nRow["ProductKey"].ToString();
                    lvi.BackColor = Color.White;

                    lvsi = new ListViewItem.ListViewSubItem();
                    lvsi.Text = nRow["ProductID"].ToString().Trim();
                    lvi.SubItems.Add(lvsi);

                    lvsi = new ListViewItem.ListViewSubItem();
                    lvsi.Text = nRow["ProductName"].ToString().Trim();

                    lvi.SubItems.Add(lvsi);
                    LV.Items.Add(lvi);
                }

                Utils.SizeLastColumn_LV(LV_Product);
            }
            else
            {
                LV_Product.Visible = false;
            }
            this.Cursor = Cursors.Default;
        }
        private void QuantityCalculator()
        {
            decimal zQuantityReality = 0;
            decimal zQuantityDocument = 0;
            decimal zQuantityLoss = 0;

            if (txt_QuantityDocument.Text.Length > 0)
            {
                zQuantityReality = decimal.Parse(txt_QuantityReality.Text);
            }
            if (txt_QuantityDocument.Text.Length > 0)
            {
                zQuantityDocument = decimal.Parse(txt_QuantityDocument.Text);
            }
            if (txt_QuantityDocument.Text.Length > 0)
            {
                zQuantityLoss = zQuantityDocument - zQuantityReality;
            }

            txt_QuantityReality.Text = zQuantityReality.ToString("n1");
            txt_QuantityLose.Text = zQuantityLoss.ToString("n1");
        }
        void LoadGVOrder(DataTable InTable)
        {
            int Index = 0;
            GVOrder.Rows.Clear();
            foreach (DataRow Row in InTable.Rows)
            {
                GVOrder.Rows.Add();
                GVOrder.Rows[Index].Tag = Row["OrderKey"].ToString();
                GVOrder.Rows[Index].Cells["No"].Value = (Index + 1).ToString();
                GVOrder.Rows[Index].Cells["OrderID_Compare"].Value = Row["OrderID_Compare"].ToString();
                GVOrder.Rows[Index].Cells["OrderID"].Value = Row["OrderID"].ToString();
                DateTime zOrderDate = DateTime.Parse(Row["OrderDate"].ToString());
                GVOrder.Rows[Index].Cells["OrderDate"].Value = zOrderDate.ToString("dd/MM/yyyy");

                GVOrder.Rows[Index].Cells["TeamID"].Tag = Row["TeamKey"].ToString();
                GVOrder.Rows[Index].Cells["TeamID"].Value = Row["TeamID"].ToString();
                GVOrder.Rows[Index].Cells["TeamName"].Value = Row["TeamName"].ToString();

                GVOrder.Rows[Index].Cells["ProductID"].Tag = Row["ProductKey"].ToString();
                GVOrder.Rows[Index].Cells["ProductID"].Value = Row["ProductID"].ToString();
                GVOrder.Rows[Index].Cells["ProductName"].Value = Row["ProductName"].ToString();

                GVOrder.Rows[Index].Cells["StageID"].Tag = Row["StageKey"].ToString();
                GVOrder.Rows[Index].Cells["StageID"].Value = Row["StagesID"].ToString();
                GVOrder.Rows[Index].Cells["StageName"].Value = Row["StageName"].ToString();
                GVOrder.Rows[Index].Cells["StagePrice"].Value = Row["StagePrice"].Ton2String();

                GVOrder.Rows[Index].Cells["OrderIDFollow"].Value = Row["OrderIDFollow"].ToString();
                GVOrder.Rows[Index].Cells["QuantityDocument"].Value = Row["QuantityDocument"].Ton2String();
                GVOrder.Rows[Index].Cells["QuantityReality"].Value = Row["QuantityReality"].Ton2String();
                GVOrder.Rows[Index].Cells["QuantityLose"].Value = Row["QuantityLoss"].Ton2String();

                GVOrder.Rows[Index].Cells["Unit"].Value = Row["UnitName"].ToString();
                if (Row["WorkStatus"].ToInt() == 1)
                    GVOrder.Rows[Index].Cells["WorkStatus"].Value = "ĐÃ THỰC HIỆN";
                else
                    GVOrder.Rows[Index].Cells["WorkStatus"].Value = "";

                GVOrder.Rows[Index].Cells["Percent"].Value = Row["PercentFinish"].Ton2String();

                GVOrder.Rows[Index].Cells["Note"].Value = Row["Note"].ToString();

                GVOrder.Rows[Index].Cells["Message"].Value = "";

                GVOrder.Rows[Index].Cells["ProductType"].Value = Row["ProductType"].ToString();
                GVOrder.Rows[Index].Cells["RateID"].Value = Row["RateID"].ToString();

                Index++;
            }
            GVOrder.ClearSelection();
            btn_CopyOrder.Enabled = true;
            btn_SaveOrder.Enabled = true;
        }
        void LoadOrderInfo(string OrderKey, bool Copy)
        {
            _Order = new Order_Object(OrderKey);

            cbo_Status.SelectedValue = _Order.WorkStatus;
            dte_OrderDate.Value = _Order.OrderDate;
            txt_OrderID.Tag = OrderKey;
            txt_OrderID.Text = _Order.OrderID;
            Team_Info zTeam = new Team_Info(_Order.TeamKey);
            cbo_Team.SelectedValue = zTeam.Key;
            txt_ProductID.Tag = _Order.ProductKey;
            txt_ProductID.Text = _Order.ProductID;
            txt_ProductName.Text = _Order.ProductName;
            Product_Stages_Info zStage = new Product_Stages_Info(_Order.Category_Stage);
            txt_WorkID.Tag = _Order.Category_Stage;
            txt_WorkID.Text = zStage.StagesID;
            txt_WorkName.Text = zStage.StageName;
            txt_Price.Text = zStage.Price.Ton2String();
            txt_Unit.Tag = _Order.UnitKey;
            txt_Unit.Text = _Order.UnitName;
            txt_Note.Text = _Order.Note;
            txt_OrderIDFollow.Text = _Order.OrderIDFollow;
            txt_OrderIDCompare.Text = _Order.OrderID_Compare;
            txt_QuantityDocument.Text = _Order.QuantityDocument.Ton2String();
            txt_QuantityReality.Text = _Order.QuantityReality.Ton2String();
            txt_QuantityLose.Text = _Order.QuantityLoss.Ton2String();
            txt_Percent.Text = _Order.PercentFinish.ToString();

            txt_OrderIDFollow.StateCommon.Back.Color1 = ColorNormal;
            txt_WorkID.StateCommon.Back.Color1 = ColorNormal;
            txt_ProductID.StateCommon.Back.Color1 = ColorNormal;
            txt_OrderID.StateCommon.Back.Color1 = ColorNormal;
            dte_OrderDate.BackColor = ColorNormal;
            cbo_Team.StateCommon.DropBack.Color1 = ColorNormal;
            cbo_Status.StateCommon.DropBack.Color1 = ColorNormal;
            txt_Unit.StateCommon.Back.Color1 = ColorNormal;

            if (_Order.List_Rate.Count > 0)
            {
                for (int i = 0; i < _Order.List_Rate.Count; i++)
                {
                    Order_Rate_Info zRate = _Order.List_Rate[i];
                    LoadGVRate(zRate, i);
                }
            }
            if (_Order.List_Employee.Count > 0)
            {
                for (int i = 0; i < _Order.List_Employee.Count; i++)
                {
                    Order_Employee_Info zEmployee = _Order.List_Employee[i];
                    LoadGVEmployee(zEmployee, i);
                }

                GVEmployee.GetSum(false);
            }
            else
            {
                GVEmployee.Rows.Add();
                GVEmployee.GetSum(true);
            }

            if (Copy)
            {
                _IsNew = true;
                txtHeader.Tag = null;
                txtHeader.Text = "Chi tiết đơn hàng: ";

                txt_OrderID.Tag = null;
                txt_OrderID.Clear();
                txt_OrderIDCompare.Tag = null;
                txt_OrderIDCompare.Clear();

                GVEmployee.Rows.Clear();
                GVRate.Rows.Clear();

                if (_Order.List_Rate.Count > 0)
                {
                    for (int i = 0; i < _Order.List_Rate.Count; i++)
                    {
                        Order_Rate_Info zRate = _Order.List_Rate[i];
                        zRate.Key = 0;
                        LoadGVRate(zRate, i);
                    }
                }
                if (_Order.List_Employee.Count > 0)
                {
                    for (int i = 0; i < _Order.List_Employee.Count; i++)
                    {
                        Order_Employee_Info zEmployee = _Order.List_Employee[i];
                        zEmployee.Key = 0;
                        LoadGVEmployee(zEmployee, i);
                    }

                    GVEmployee.GetSum(false);
                }
            }
            else
            {
                txtHeader.Text = "Chi tiết đơn hàng: " + txt_OrderID.Text;
                txtHeader.Tag = _Order.OrderID;
            }
        }
        void LoadGVRate(Order_Rate_Info zOrderRate, int index)
        {
            GVRate.Rows.Add();
            GVRate.Rows[index].Tag = zOrderRate;
            GVRate.Rows[index].Cells["RateID"].Tag = zOrderRate.CategoryKey;
            Category_Info zRate = new Category_Info(zOrderRate.CategoryKey);
            GVRate.Rows[index].Cells["RateID"].Value = zRate.RateID;
            GVRate.Rows[index].Cells["RateName"].Value = zOrderRate.RateName; ;
            GVRate.Rows[index].Cells["Rate"].Value = zOrderRate.Rate;
        }
        void LoadGVEmployee(Order_Employee_Info zOrderEmployee, int index)
        {
            GVEmployee.Rows.Add();
            GVEmployee.Rows[index].Tag = zOrderEmployee;
            GVEmployee.Rows[index].Cells["No"].Value = (index + 1).ToString();
            GVEmployee.Rows[index].Cells["EmployeeID"].Tag = zOrderEmployee.EmployeeKey;
            GVEmployee.Rows[index].Cells["EmployeeID"].Value = zOrderEmployee.EmployeeID;
            GVEmployee.Rows[index].Cells["FullName"].Value = zOrderEmployee.FullName;
            GVEmployee.Rows[index].Cells["Kg"].Value = zOrderEmployee.Kilogram.ToString();
            GVEmployee.Rows[index].Cells["Basket"].Value = zOrderEmployee.Basket.ToString();
            GVEmployee.Rows[index].Cells["Time"].Value = zOrderEmployee.Time.ToString();
            if (zOrderEmployee.Borrow.ToString() == "1")
            {
                GVEmployee.Rows[index].Cells["Borrow"].Value = true;
            }
            if (zOrderEmployee.Private.ToString() == "1")
            {
                GVEmployee.Rows[index].Cells["Private"].Value = true;
            }
            if (zOrderEmployee.Share.ToString() == "1")
            {
                GVEmployee.Rows[index].Cells["General"].Value = true;
            }
        }
        void LoadGVEmployee()
        {
            GVEmployee.Rows.Clear();
            DataTable zTable = Employee_Data.ListNamesV2(cbo_Team.SelectedValue.ToInt(),dte_OrderDate.Value);
            int i = 0;
            foreach (DataRow nRow in zTable.Rows)
            {
                GVEmployee.Rows.Add();
                DataGridViewRow nRowView = GVEmployee.Rows[i];

                nRowView.Cells["No"].Value = (i + 1).ToString();
                nRowView.Cells["EmployeeID"].Tag = nRow["EmployeeKey"].ToString().Trim();
                nRowView.Cells["EmployeeID"].Value = nRow["EmployeeID"].ToString().Trim();
                nRowView.Cells["FullName"].Value = nRow["FullName"].ToString().Trim();
                nRowView.Cells["Basket"].Value = "0";
                nRowView.Cells["Kg"].Value = "0";
                nRowView.Cells["Time"].Value = "0";
                nRowView.Cells["Borrow"].Value = false;
                nRowView.Cells["Private"].Value = false;
                nRowView.Cells["General"].Value = false;

                //------------------------------------------------------------------------------------------
                Order_Employee_Info zOrderEmployee = new Order_Employee_Info();
                zOrderEmployee.EmployeeKey = nRow["EmployeeKey"].ToString().Trim();
                zOrderEmployee.EmployeeID = nRow["EmployeeID"].ToString().Trim();
                zOrderEmployee.FullName = nRow["FullName"].ToString().Trim();
                zOrderEmployee.Basket = 0;
                zOrderEmployee.Kilogram = 0;
                zOrderEmployee.Time = 0;
                zOrderEmployee.Borrow = 0;
                zOrderEmployee.Private = 0;
                zOrderEmployee.Share = 0;
                zOrderEmployee.RecordStatus = 1;
                nRowView.Tag = zOrderEmployee;
                //------------------------------------------------------------------------------------------
                i++;
            }
            GVEmployee.GetSum(false);
        }
        #endregion

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion
        private void btn_Show_Click(object sender, EventArgs e)
        {
            splitContainer1.SplitterDistance = 300;
            btn_Hide.Visible = true;
            btn_Show.Visible = false;

            Panel_Info.Visible = true;
            Panel_Employee.Visible = true;
        }

        private void btn_Hide_Click(object sender, EventArgs e)
        {
            splitContainer1.SplitterDistance = Screen.PrimaryScreen.Bounds.Width;
            btn_Hide.Visible = false;
            btn_Show.Visible = true;

            Panel_Info.Visible = false;
            Panel_Employee.Visible = false;
        }
        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region[Event]
        private void txt_OrderIDFollow_Leave(object sender, EventArgs e)
        {
            if (txt_OrderIDFollow.Text.Trim().Length > 0)
            {
                if (txt_OrderIDFollow.Text.Trim().ToUpper() == "CVTG")
                {
                    txt_OrderIDFollow.StateCommon.Back.Color1 = ColorNormal;
                }
                else
                {
                    int Count = Order_Data.CountOrderIDFollow(txt_OrderIDFollow.Text.Trim());
                    if (Count <= 0)
                    {
                        txt_OrderIDFollow.StateCommon.Back.Color1 = Color.FromArgb(199, 21, 112);
                    }
                    else
                    {
                        txt_OrderIDFollow.StateCommon.Back.Color1 = Color.FromArgb(255, 255, 255);
                    }
                }
            }
            else
            {
                txt_OrderIDFollow.StateCommon.Back.Color1 = Color.FromArgb(255, 255, 255);
            }
        }
        private void txt_Search_ProductID_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                LV_Product.Focus();
                if (LV_Product.Items.Count > 0)
                    LV_Product.Items[0].Selected = true;
            }
            else
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (txt_Search_ProductID.Text.Length > 0)
                    {
                        LV_Product.Focus();
                        LV_Product.Visible = true;
                        LV_Product.BringToFront();
                        LoadLVProduct();
                    }
                    else
                        LV_Product.Visible = false;
                }
            }
        }
        private void LV_Product_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            txt_Search_ProductID.Tag = LV_Product.Items[e.ItemIndex].Tag.ToString();
            txt_Search_ProductID.Text = LV_Product.Items[e.ItemIndex].SubItems[1].Text.Trim();
            txt_Search_ProductName.Text = LV_Product.Items[e.ItemIndex].SubItems[2].Text.Trim();
        }
        private void LV_Product_ItemActivate(object sender, EventArgs e)
        {
            txt_Search_ProductID.Tag = LV_Product.SelectedItems[0].Tag;
            txt_Search_ProductID.Text = LV_Product.SelectedItems[0].SubItems[1].Text.Trim();
            txt_Search_ProductName.Text = LV_Product.SelectedItems[0].SubItems[2].Text.Trim();
            txt_Search_ProductName.Focus();
            LV_Product.Visible = false;
        }
        private void LV_Product_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txt_Search_ProductID.Tag = LV_Product.SelectedItems[0].Tag;
                txt_Search_ProductID.Text = LV_Product.SelectedItems[0].SubItems[1].Text.Trim();
                txt_Search_ProductName.Text = LV_Product.SelectedItems[0].SubItems[2].Text.Trim();
                txt_Search_ProductName.Focus();
                LV_Product.Visible = false;
            }
        }
        private void txt_Search_ProductID_Leave(object sender, EventArgs e)
        {
            if (!LV_Product.Focused)
                LV_Product.Visible = false;

            if (txt_Search_ProductID.Text.Trim().Length == 0)
            {
                txt_Search_ProductID.Tag = null;
                txt_Search_ProductName.Clear();
            }
            else
            {
                Product_Info zProduct = new Product_Info();
                zProduct.Get_Product_Info_ID(txt_Search_ProductID.Text.Trim());
                txt_Search_ProductName.Text = zProduct.ProductName;
            }
        }
        private void txt_Search_WorkID_Leave(object sender, EventArgs e)
        {
            if (txt_Search_WorkID.Text.Trim().Length == 0)
            {
                txt_Search_WorkName.Clear();
            }
        }
        private void cbo_TeamID_Search_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbo_TeamID_Search.SelectedIndex >= 0)
            {
                _TeamKey_Search = cbo_TeamID_Search.SelectedValue.ToInt();
            }
        }
        private void cbo_Team_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbo_Team.SelectedIndex > 0)
            {
                this.Cursor = Cursors.WaitCursor;
                if (txt_OrderID.Tag == null)
                {
                    LoadGVEmployee();
                }
                this.Cursor = Cursors.Default;
            }
        }
        private void txt_OrderID_Leave(object sender, EventArgs e)
        {
            if (txt_OrderID.Text.Trim().Length == 0)
            {
                txt_OrderID.StateCommon.Back.Color1 = ColorWarning;
            }
            else
            {
                if (txtHeader.Tag != null && _IsNew == true)
                {
                    string OldID = txtHeader.Tag.ToString();
                    if (txt_OrderID.Text.ToUpper().Trim() == OldID.ToUpper().Trim())
                    {
                        txt_OrderID.StateCommon.Back.Color1 = ColorWarning;
                        Utils.TNMessageBoxOK("Mã đơn hàng không được trùng !.",1);
                    }
                }
            }
        }
        private void txt_WorkID_Leave(object sender, EventArgs e)
        {
            if (txt_WorkID.Text.Trim().Length == 0)
            {
                txt_WorkName.Clear();
                txt_Price.Clear();
            }
            else
            {
                Product_Stages_Info zStage = new Product_Stages_Info();
                zStage.GetStagesID_Info(txt_WorkID.Text.Trim());
                txt_WorkGroup.Text = zStage.GroupID;
                if (zStage.Key == 0)
                {
                    txt_WorkID.StateCommon.Back.Color1 = ColorWarning;
                    txt_WorkID.Tag = null;
                }
                else
                {
                    txt_WorkID.StateCommon.Back.Color1 = ColorNormal;
                }
            }
        }
        private void txt_ProductID_Leave(object sender, EventArgs e)
        {
            if (!LV_Product.Focused)
                LV_Product.Visible = false;

            if (txt_ProductID.Text.Trim().Length == 0)
            {
                txt_ProductID.Tag = null;
                txt_ProductName.Clear();
            }
            else
            {
                Product_Info zProduct = new Product_Info();
                zProduct.Get_Product_Info_ID(txt_ProductID.Text.Trim());
                if (zProduct.ProductKey != "")
                {
                    txt_ProductID.Tag = zProduct.ProductKey;
                    txt_ProductName.Text = zProduct.ProductName;
                }
                else
                {
                    txt_ProductID.Tag = null;
                    txt_ProductName.Clear();
                    Utils.TNMessageBoxOK("Mã sản phẩm không hợp lệ!",1);
                }
            }
        }
        private void txt_Number_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= '0' && e.KeyChar <= '9') || (Keys)e.KeyChar == Keys.Back || e.KeyChar == 46 || e.KeyChar == 44)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
        private void btn_Search_Work_Click(object sender, EventArgs e)
        {
            Frm_Setup_Work Frm = new Frm_Setup_Work();
            Frm.TeamKey = int.Parse(cbo_TeamID_Search.SelectedValue.ToString());
            Frm.SearchStyle = true;
            Frm.ShowDialog();

            Product_Stages_Info zInfo = Frm.Product_Stages;
            if (zInfo != null)
            {
                txt_Search_WorkID.Tag = zInfo.Key;
                txt_Search_WorkID.Text = zInfo.StagesID;
                txt_Search_WorkName.Text = zInfo.StageName;
            }
        }
        private void btn_Search_Product_Click(object sender, EventArgs e)
        {

        }
        private void btn_OpenSearch_Click(object sender, EventArgs e)
        {
            if (!Panel_Search.Visible)
            {
                btn_OpenSearch.Text = "Ẩn -";
                Panel_Search.Visible = true;
            }
            else
            {
                btn_OpenSearch.Text = "Mở -";
                Panel_Search.Visible = false;
            }
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            using (Frm_Loading frm = new Frm_Loading(LoadData)) { frm.ShowDialog(this); }
            Cursor = Cursors.Default;
        }

        private void btn_SearchInput_Work_Click(object sender, EventArgs e)
        {
            Frm_Setup_Work Frm = new Frm_Setup_Work();
            Frm.TeamKey = int.Parse(cbo_Team.SelectedValue.ToString());
            Frm.SearchStyle = true;
            Frm.ShowDialog();

            Product_Stages_Info zInfo = Frm.Product_Stages;
            if (zInfo != null)
            {
                txt_WorkID.Tag = zInfo.Key;
                txt_WorkID.Text = zInfo.StagesID;
                txt_WorkName.Text = zInfo.StageName;
                txt_Price.Text = zInfo.Price.Ton2String();

                txt_WorkID.StateCommon.Back.Color1 = ColorNormal;
            }
        }
        private void btn_SearchInput_Document_Click(object sender, EventArgs e)
        {
            Frm_Search_Order frm = new Frm_Search_Order();
            frm.ShowDialog();
            if (frm.ProductKey.Length > 0)
            {
                txt_ProductID.Tag = frm.ProductKey;
                txt_OrderIDFollow.Text = frm.OrderID;
                txt_ProductName.Text = frm.ProductName;
                txt_ProductID.Text = frm.ProductID;
                txt_QuantityDocument.Text = frm.Quantity_Document.ToString();
                txt_Unit.Tag = frm.UnitKey;
                txt_Unit.Text = frm.UnitName;

                txt_ProductID.StateCommon.Back.Color1 = ColorNormal;
            }
        }
        private void btn_NewOrder_Click(object sender, EventArgs e)
        {
            _IsNew = true;
            _Order = new Order_Object();

            GVEmployee.Rows.Clear();
            GVEmployee.GetSum(false);
            GVOrder.ClearSelection();
            GVRate.Rows.Clear();

            txt_OrderIDFollow.StateCommon.Back.Color1 = ColorNormal;
            txt_WorkID.StateCommon.Back.Color1 = ColorNormal;
            txt_ProductID.StateCommon.Back.Color1 = ColorNormal;
            txt_OrderID.StateCommon.Back.Color1 = ColorNormal;
            dte_OrderDate.BackColor = ColorNormal;
            cbo_Team.StateCommon.DropBack.Color1 = ColorNormal;
            cbo_Status.StateCommon.DropBack.Color1 = ColorNormal;

            txt_Unit.StateCommon.Back.Color1 = ColorNormal;
            txt_QuantityDocument.Text = "0";
            txt_QuantityLose.Text = "0";
            txt_QuantityReality.Text = "0";
            txt_Percent.Text = "0";

            txt_Note.Clear();
            txt_OrderID.Clear();
            txt_OrderID.Tag = null;
            txt_OrderIDCompare.Clear();
            txt_OrderIDFollow.Clear();
            txt_ProductName.Clear();
            txt_ProductID.Clear();
            txt_ProductID.Tag = null;
            txt_Unit.Clear();
            txt_Unit.Tag = null;
            txt_Price.Clear();
            txt_WorkID.Clear();
            txt_WorkID.Tag = null;
            txt_WorkName.Clear();

            dte_OrderDate.Value = SessionUser.Date_Work;
            cbo_Status.SelectedIndex = 1;
            cbo_Team.SelectedIndex = 0;
            cbo_Team_SelectedIndexChanged(null, null);
            Panel_Message.Visible = false;

            btn_CopyOrder.Enabled = true;
            btn_SaveOrder.Enabled = true;
            btn_DeleteOrder.Enabled = false;
        }
        private void btn_CopyOrder_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            if (!CheckOrder())
            {
                //SaveOrder(); không cần lưu lại vì sẽ bị mất dữ liệu đã tính đơn hàng cũ 
                //if (lbl_Message.Text.Length <= 0)
                //{

                    CopyOrder();
                    Utils.TNMessageBoxOK("Đã cập nhật và sao chép thành công !.",3);
                //}
            }
            Cursor = Cursors.Default;
        }
        private void btn_SaveOrder_Click(object sender, EventArgs e)
        {
            if (SessionUser.Date_Lock >= dte_OrderDate.Value)
            {
                Utils.TNMessageBoxOK("Thời gian này đã khóa sổ.!", 2);
                return;
            }
            Cursor = Cursors.WaitCursor;
            if (txt_OrderID.Tag != null)
            {
                if (Utils.TNMessageBox("Bạn có muốn thay đổi thông tin đơn hàng này? \n Sau khi thay đổi, Đơn hàng cần được thực hiện tính toán lại theo đúng trình tự!",2) == "Y")
                {
                    if (!CheckOrder())
                    {
                        SaveOrder();
                        if (lbl_Message.Text.Length <= 0)
                        {
                            Utils.TNMessageBoxOK("Đã cập nhật thành công !.",3);
                            btn_NewOrder.PerformClick();
                        }
                    }
                }
            }
            else
            {
                if (!CheckOrder())
                {
                    SaveOrder();
                    if (lbl_Message.Text.Length <= 0)
                    {
                        Utils.TNMessageBoxOK("Đã cập nhật thành công !.",3);
                        btn_NewOrder.PerformClick();
                    }
                }
            }
            Cursor = Cursors.Default;
        }
        private void btn_DeleteOrder_Click(object sender, EventArgs e)
        {
            if (GVOrder.SelectedRows.Count > 0 &&
                GVOrder.CurrentRow.Tag != null)
            {
                if (Utils.TNMessageBox("Bạn có xóa hết thông tin đơn hàng này ?", 2) == "Y")
                {
                    bool IsDeleted = false;
                    string Message = "";
                    _OrderKey = GVOrder.CurrentRow.Tag.ToString();
                    Order_Info zOrder = new Order_Info(_OrderKey);
                    Order_Money_Info zInfo = new Order_Money_Info();
                    if (SessionUser.Date_Lock >= zOrder.OrderDate)
                    {
                        Utils.TNMessageBoxOK("Thời gian này đã khóa sổ.!", 2);
                        return;
                    }
                    zInfo.OrderKey = _OrderKey;
                    zInfo.Delete_Order();

                    if (zInfo.Message.Length == 0)
                    {
                        //* Đông bổ sung xóa chia lại lần 1, lần 2 ngày 23/07/2020 
                        Order_Adjusted_Info zAd = new Order_Adjusted_Info();
                        zAd.DeleteAll(_OrderKey);
                        //*
                        _Order = new Order_Object();
                        _Order.Key = _OrderKey;
                        _Order.Delete_Object();

                        if (_Order.Message.Substring(0, 2) == "30")
                        {
                            _OrderKey = string.Empty;
                            IsDeleted = true;
                            GVOrder.ClearSelection();
                            GVOrder.Rows.Remove(GVOrder.CurrentRow);
                        }
                        else
                        {
                            Message += _Order.Message;
                        }
                    }
                    else
                    {
                        Message += zInfo.Message;
                    }

                    if (!IsDeleted)
                    {
                        Utils.TNMessageBoxOK(Message,4);
                    }
                }
            }
        }

        private void btn_DeleteAllOrder_Click(object sender, EventArgs e) //Xóa tất cả trên lưới
        {
            if (SessionUser.Date_Lock >= dte_ToDate.Value)
            {
                Utils.TNMessageBoxOK("Thời gian trên tìm kiếm đã khóa sổ.!", 2);
                return;
            }

            if (Utils.TNMessageBox("Bạn có muốn xóa hết tất cả đơn hàng trên lưới này ?", 2) == "Y")
            {
                string zErr = "";
                for (int i = 0; i < GVOrder.Rows.Count;i++)
                {
                    bool IsDeleted = false;
                    string Message = "";
                    _OrderKey = GVOrder.Rows[i].Tag.ToString();
                    Order_Money_Info zInfo = new Order_Money_Info();
                    zInfo.OrderKey = _OrderKey;
                    zInfo.Delete_Order();

                    if (zInfo.Message.Length == 0)
                    {
                        //* Đông bổ sung xóa chia lại lần 1, lần 2 ngày 23/07/2020 
                        Order_Adjusted_Info zAd = new Order_Adjusted_Info();
                        zAd.DeleteAll(_OrderKey);
                        //*
                        _Order = new Order_Object();
                        _Order.Key = _OrderKey;
                        _Order.Delete_Object();

                        if (_Order.Message.Substring(0, 2) == "30")
                        {
                            _OrderKey = string.Empty;
                            IsDeleted = true;
                        }
                        else
                        {
                            Message += _Order.Message;
                            zErr += Message;
                        }
                    }
                    else
                    {
                        Message += zInfo.Message;
                        zErr += Message;
                    }

                    //if (!IsDeleted)
                    //{
                    //    MessageBox.Show(Message, "Có lỗi khi xóa số liệu vui lòng thông báo cho IT !.");
                    //}

                }
                if (zErr == string.Empty)
                    Utils.TNMessageBoxOK("Xóa xong!",3);
                else
                    Utils.TNMessageBoxOK(zErr,4);
            }

        }



        #endregion
        private void LoadData()
        {
            DateTime FromDate = dte_FromDate.Value;
            DateTime ToDate = dte_ToDate.Value;
            string ProductID = txt_Search_ProductID.Text.Trim();
            string WorkID = txt_Search_WorkID.Text.Trim();
            string OrderID = txt_Search_IDOrder.Text.Trim();
            string OrderIDOriginal = txt_Search_IDOrder_Original.Text.Trim();

            DataTable zTable = Order_Data.List_Search(FromDate, ToDate, ProductID, WorkID, OrderIDOriginal, OrderID, _TeamKey_Search);
            this.Invoke(new MethodInvoker(delegate ()
            {
                LoadGVOrder(zTable);
            }));
        }
        private void ShowOrderInserted()
        {
            int Index = GVOrder.RowCount;
            GVOrder.Rows.Add();
            GVOrder.Rows[Index].Tag = _Order.Key;
            GVOrder.Rows[Index].Cells["No"].Value = (Index).ToString();
            GVOrder.Rows[Index].Cells["OrderID_Compare"].Value = _Order.OrderID_Compare;
            GVOrder.Rows[Index].Cells["OrderID"].Value = _Order.OrderID;
            GVOrder.Rows[Index].Cells["OrderDate"].Value = _Order.OrderDate.ToString("dd/MM/yyyy");

            GVOrder.Rows[Index].Cells["TeamID"].Tag = _Order.TeamKey.ToString();
            GVOrder.Rows[Index].Cells["TeamID"].Value = _Order.TeamID;
            GVOrder.Rows[Index].Cells["TeamName"].Value = _Order.TeamName;

            GVOrder.Rows[Index].Cells["ProductID"].Tag = _Order.ProductKey;
            GVOrder.Rows[Index].Cells["ProductID"].Value = _Order.ProductID;
            GVOrder.Rows[Index].Cells["ProductName"].Value = _Order.ProductName;

            GVOrder.Rows[Index].Cells["StageID"].Tag = _Order.StageKey.ToString();
            GVOrder.Rows[Index].Cells["StageID"].Value = _Order.StagesID;
            GVOrder.Rows[Index].Cells["StageName"].Value = _Order.StageName;
            GVOrder.Rows[Index].Cells["StagePrice"].Value = _Order.StagePrice.Ton2String();

            GVOrder.Rows[Index].Cells["OrderIDFollow"].Value = _Order.OrderIDFollow.ToString();
            GVOrder.Rows[Index].Cells["QuantityDocument"].Value = _Order.QuantityDocument.Ton2String();
            GVOrder.Rows[Index].Cells["QuantityReality"].Value = _Order.QuantityReality.Ton2String();
            GVOrder.Rows[Index].Cells["QuantityLose"].Value = _Order.QuantityLoss.Ton2String();
            GVOrder.Rows[Index].Cells["Unit"].Tag = _Order.UnitKey;
            GVOrder.Rows[Index].Cells["Unit"].Value = _Order.UnitName;
            if (_Order.WorkStatus == 1)
                GVOrder.Rows[Index].Cells["WorkStatus"].Value = "ĐÃ THỰC HIỆN";
            else
                GVOrder.Rows[Index].Cells["WorkStatus"].Value = "";

            GVOrder.Rows[Index].Cells["Percent"].Value = _Order.PercentFinish.ToString();
            GVOrder.Rows[Index].Cells["Note"].Value = _Order.Note.ToString();
        }
        private void ShowOrderUpdate()
        {
            int Index = GVOrder.CurrentRow.Index;
            GVOrder.Rows[Index].Tag = _Order.Key;
            GVOrder.Rows[Index].Cells["No"].Value = (Index).ToString();
            GVOrder.Rows[Index].Cells["OrderID_Compare"].Value = _Order.OrderID_Compare;
            GVOrder.Rows[Index].Cells["OrderID"].Value = _Order.OrderID;
            GVOrder.Rows[Index].Cells["OrderDate"].Value = _Order.OrderDate.ToString("dd/MM/yyyy");

            GVOrder.Rows[Index].Cells["TeamID"].Tag = _Order.TeamKey.ToString();
            GVOrder.Rows[Index].Cells["TeamID"].Value = _Order.TeamID;
            GVOrder.Rows[Index].Cells["TeamName"].Value = _Order.TeamName;

            GVOrder.Rows[Index].Cells["ProductID"].Tag = _Order.ProductKey;
            GVOrder.Rows[Index].Cells["ProductID"].Value = _Order.ProductID;
            GVOrder.Rows[Index].Cells["ProductName"].Value = _Order.ProductName;

            GVOrder.Rows[Index].Cells["StageID"].Tag = _Order.StageKey.ToString();
            GVOrder.Rows[Index].Cells["StageID"].Value = _Order.StagesID;
            GVOrder.Rows[Index].Cells["StageName"].Value = _Order.StageName;
            GVOrder.Rows[Index].Cells["StagePrice"].Value = _Order.StagePrice.Ton2String();

            GVOrder.Rows[Index].Cells["OrderIDFollow"].Value = _Order.OrderIDFollow.ToString();
            GVOrder.Rows[Index].Cells["QuantityDocument"].Value = _Order.QuantityDocument.Ton2String();
            GVOrder.Rows[Index].Cells["QuantityReality"].Value = _Order.QuantityReality.Ton2String();
            GVOrder.Rows[Index].Cells["QuantityLose"].Value = _Order.QuantityLoss.Ton2String();
            GVOrder.Rows[Index].Cells["Unit"].Tag = _Order.UnitKey;
            GVOrder.Rows[Index].Cells["Unit"].Value = _Order.UnitName;
            if (_Order.WorkStatus == 1)
                GVOrder.Rows[Index].Cells["WorkStatus"].Value = "ĐÃ THỰC HIỆN";
            else
                GVOrder.Rows[Index].Cells["WorkStatus"].Value = "";

            GVOrder.Rows[Index].Cells["Percent"].Value = _Order.PercentFinish.ToString();
            GVOrder.Rows[Index].Cells["Note"].Value = _Order.Note.ToString();
        }
        private void SaveOrder()
        {
            if (txt_OrderID.Tag == null)
                _Order = new Order_Object();
            else
                _Order = new Order_Object(txt_OrderID.Tag.ToString());

            _Order.OrderID_Compare = txt_OrderIDCompare.Text.Trim();
            _Order.OrderID = txt_OrderID.Text.Trim();
            if (cbo_Team.SelectedValue != null)
            {
                _Order.TeamKey = cbo_Team.SelectedValue.ToInt();
            }
            if (cbo_Status.SelectedValue != null)
            {
                _Order.WorkStatus = int.Parse(cbo_Status.SelectedValue.ToString());
            }
            if (txt_ProductID.Tag != null)
            {
                _Order.ProductKey = txt_ProductID.Tag.ToString();
                _Order.ProductID = txt_ProductID.Text.Trim();
                _Order.ProductName = txt_ProductName.Text.Trim();
            }
            if (txt_WorkID.Tag != null)
            {
                _Order.Category_Stage = txt_WorkID.Tag.ToInt();
            }
            DateTime zDate = dte_OrderDate.Value;
            _Order.OrderDate = new DateTime(zDate.Year, zDate.Month, zDate.Day, 0, 0, 0);
            _Order.UnitKey = txt_Unit.Tag.ToInt();
            _Order.UnitName = txt_Unit.Text;
            _Order.OrderIDFollow = txt_OrderIDFollow.Text.Trim();
            _Order.PercentFinish = float.Parse(txt_Percent.Text.Trim());
            _Order.QuantityDocument = float.Parse(txt_QuantityDocument.Text.Trim());
            _Order.QuantityReality = float.Parse(txt_QuantityReality.Text.Trim());
            _Order.QuantityLoss = float.Parse(txt_QuantityLose.Text.Trim());
            _Order.Note = txt_Note.Text.Trim();

            if (GVRate.Rows.Count > 0)
            {
                List<Order_Rate_Info> zRateList = new List<Order_Rate_Info>();
                for (int i = 0; i < GVRate.Rows.Count; i++)
                {
                    DataGridViewRow zRow = GVRate.Rows[i];
                    if (zRow.Tag != null)
                    {
                        Order_Rate_Info zOrderRate = (Order_Rate_Info)zRow.Tag;
                        float zRate = float.Parse(zRow.Cells["Rate"].Value.ToString());
                        string zRateName = zRow.Cells["RateName"].Value.ToString().Trim();
                        int zRateKey = zRow.Cells["RateID"].Tag.ToInt();
                        zOrderRate.CategoryKey = zRateKey;
                        zOrderRate.RateName = zRateName;
                        zOrderRate.Rate = zRate;
                        zOrderRate.OrderDate = new DateTime(zDate.Year, zDate.Month, zDate.Day, 0, 0, 0);
                        if (zOrderRate.Key == 0)
                            zOrderRate.RecordStatus = 1;
                        else
                            zOrderRate.RecordStatus = 2;

                        zRateList.Add(zOrderRate);
                    }
                }
                _Order.List_Rate = zRateList;
            }
            if (GVEmployee.Rows.Count > 0)
            {
                List<Order_Employee_Info> zEmployeeList = new List<Order_Employee_Info>();
                for (int i = 0; i < GVEmployee.Rows.Count; i++)
                {
                    DataGridViewRow zRow = GVEmployee.Rows[i];
                    if (zRow.Tag != null)
                    {
                        Order_Employee_Info zOrderEmployee = (Order_Employee_Info)zRow.Tag;
                        zOrderEmployee.OrderDate = new DateTime(zDate.Year, zDate.Month, zDate.Day, 0, 0, 0);
                        zOrderEmployee.EmployeeID = zRow.Cells["EmployeeID"].Value.ToString().Trim();
                        zOrderEmployee.EmployeeKey = zRow.Cells["EmployeeID"].Tag.ToString().Trim();
                        zOrderEmployee.FullName = zRow.Cells["FullName"].Value.ToString().Trim();
                        float zBasket = 0;
                        if (zRow.Cells["Basket"].Value == null)
                            zOrderEmployee.Basket = zBasket;
                        else
                        {
                            float.TryParse(zRow.Cells["Basket"].Value.ToString().Trim(), out zBasket);
                            zOrderEmployee.Basket = zBasket;
                        }
                        float zKilogram = 0;
                        if (zRow.Cells["Kg"].Value == null)
                            zOrderEmployee.Kilogram = zKilogram;
                        else
                        {
                            float.TryParse(zRow.Cells["Kg"].Value.ToString().Trim(), out zKilogram);
                            zOrderEmployee.Kilogram = zKilogram;
                        }
                        float zTime = 0;
                        if (zRow.Cells["Time"].Value == null)
                            zOrderEmployee.Time = zTime;
                        else
                        {
                            float.TryParse(zRow.Cells["Time"].Value.ToString().Trim(), out zTime);
                            zOrderEmployee.Time = zTime;
                        }
                        if (zRow.Cells["Borrow"].Value != null)
                        {
                            if (bool.Parse(zRow.Cells["Borrow"].Value.ToString()) == true)
                            {
                                zOrderEmployee.Borrow = 1;
                            }
                            else
                            {
                                zOrderEmployee.Borrow = 0;
                            }
                        }
                        if (zRow.Cells["Private"].Value != null)
                        {
                            if (bool.Parse(zRow.Cells["Private"].Value.ToString()) == true)
                            {
                                zOrderEmployee.Private = 1;
                            }
                            else
                            {
                                zOrderEmployee.Private = 0;
                            }
                        }
                        if (zRow.Cells["General"].Value != null)
                        {
                            if (bool.Parse(zRow.Cells["General"].Value.ToString()) == true)
                            {
                                zOrderEmployee.Share = 1;
                            }
                            else
                            {
                                zOrderEmployee.Share = 0;
                            }
                        }

                        if (zOrderEmployee.RecordStatus != 99)
                        {
                            if (zOrderEmployee.Key == 0)
                                zOrderEmployee.RecordStatus = 1;
                            else
                                zOrderEmployee.RecordStatus = 2;
                        }

                        zEmployeeList.Add(zOrderEmployee);
                    }
                }
                _Order.List_Employee = zEmployeeList;
            }

            _Order.CreatedBy = SessionUser.UserLogin.Key;
            _Order.CreatedName = SessionUser.Personal_Info.FullName;
            _Order.ModifiedBy = SessionUser.UserLogin.Key;
            _Order.ModifiedName = SessionUser.Personal_Info.FullName;

            _Order.Save_Object();
            _OrderKey = _Order.Key;
            if (_Order.Message.Substring(0, 2) != "11" &&
                _Order.Message.Substring(0, 2) != "20")
            {
                Panel_Message.Visible = true;
                lbl_Message.Text = _Order.Message;
                return;
            }
            if (_Order.Message.Substring(0, 2) == "11")
            {
                //* Đông bổ sung xóa  đã chia tiền năng suất, chia lại cho nhóm lại lần 1, lần 2 ngày 23/07/2020 
                Order_Money_Info zInfo = new Order_Money_Info();
                zInfo.OrderKey = _OrderKey;
                zInfo.Delete_Order();
                Order_Adjusted_Info zAd = new Order_Adjusted_Info();
                zAd.DeleteAll(_OrderKey);
                //*
                Panel_Message.Visible = false;
                lbl_Message.Text = "";
                ShowOrderInserted();
                return;
            }
            if (_Order.Message.Substring(0, 2) == "20")
            {
                //* Đông bổ sung xóa  đã chia tiền năng suất, chia lại cho nhóm lại lần 1, lần 2 ngày 23/07/2020 
                Order_Money_Info zInfo = new Order_Money_Info();
                zInfo.OrderKey = _OrderKey;
                zInfo.Delete_Order();
                Order_Adjusted_Info zAd = new Order_Adjusted_Info();
                zAd.DeleteAll(_OrderKey);
                //*
                Panel_Message.Visible = false;
                lbl_Message.Text = "";
                ShowOrderUpdate();
                return;
            }
        }
        private void CopyOrder()
        {
            LoadOrderInfo(_OrderKey, true);
        }
        private bool CheckOrder()
        {
            bool zIsError = false;
            string zStrError = "";

            for (int i = 0; i < GVEmployee.RowCount - 1; i++)
            {
                bool CellBorrow = false;
                bool CellGeneral = false;
                bool CellPrivate = false;

                if (GVEmployee.Rows[i].Cells["Borrow"].Value != null && bool.Parse(GVEmployee.Rows[i].Cells["Borrow"].Value.ToString()) == true)
                {
                    CellBorrow = true;
                }
                if (GVEmployee.Rows[i].Cells["Private"].Value != null && bool.Parse(GVEmployee.Rows[i].Cells["Private"].Value.ToString()) == true)
                {
                    CellPrivate = true;
                }
                if (GVEmployee.Rows[i].Cells["General"].Value != null && bool.Parse(GVEmployee.Rows[i].Cells["General"].Value.ToString()) == true)
                {
                    CellGeneral = true;
                }

                if (CellBorrow == true)
                {
                    if (CellPrivate == false &&
                        CellGeneral == false)
                    {
                        zIsError = true;
                        zStrError += "- Kiểm tra lại tình huống mượn nhân viên \n\r";
                        break;
                    }
                    if (CellPrivate == true &&
                        CellGeneral == true)
                    {
                        zIsError = true;
                        zStrError += "- Kiểm tra lại tình huống mượn nhân viên \n\r";
                        break;
                    }
                }
            }

            if (txt_OrderID.Text.Trim().Length <= 0)
            {
                zIsError = true;
                txt_OrderID.StateCommon.Back.Color1 = ColorWarning;
                zStrError += "- Kiểm tra lại Mã đơn hàng \n\r";
            }
            else
            {
                if (_IsNew)
                {
                    if (Order_Data.FindOrderID(txt_OrderID.Text.Trim()) != 0)
                    {
                        zIsError = false;
                        zStrError += "- Kiểm tra lại Mã đơn hàng đã tồn tại \n\r";
                    }
                }
                else
                    txt_OrderID.StateCommon.Back.Color1 = ColorNormal;
            }
            if (txt_OrderIDFollow.Text.Trim().ToUpper() != "CVTG")
            {
                if (txt_ProductID.Text.Trim().Length <= 0)
                {
                    zIsError = true;
                    txt_ProductID.StateCommon.Back.Color1 = ColorWarning;
                    zStrError += "- Kiểm tra lại sản phẩm \n\r";
                }
                else
                {
                    Product_Info zProduct = new Product_Info();
                    zProduct.Get_Product_Info_ID(txt_ProductID.Text.Trim());
                    if (zProduct.ProductKey == "")
                    {
                        zStrError += "- Kiểm tra lại sản phẩm không tồn tại \n\r";
                        zIsError = false;
                    }
                    else
                        txt_ProductID.StateCommon.Back.Color1 = ColorNormal;
                }
            }
            if (dte_OrderDate.Value == DateTime.MinValue)
            {
                zIsError = true;
                dte_OrderDate.BackColor = ColorWarning;
                zStrError += "- Kiểm tra lại ngày đơn hàng \n\r";
            }
            else
            {
                zIsError = false;
                dte_OrderDate.BackColor = ColorNormal;
            }

            if (txt_WorkID.Text.Trim().Length <= 0)
            {
                zIsError = true;
                txt_WorkID.StateCommon.Back.Color1 = ColorWarning;
                zStrError += "- Kiểm tra lại công việc \n\r";
            }
            else
            {
                Product_Stages_Info zStage = new Product_Stages_Info();
                zStage.GetStagesID_Info(txt_WorkID.Text.Trim());
                if (zStage.Key == 0)
                {
                    zIsError = false;
                    zStrError += "- Kiểm tra lại công việc không tồn tại \n\r";
                }
                else
                    txt_WorkID.StateCommon.Back.Color1 = ColorNormal;
            }
            if (txt_Unit.Text.Trim().Length <= 0)
            {
                txt_Unit.StateCommon.Back.Color1 = ColorWarning;
                zStrError += "- Kiểm tra đơn vị tính \n\r";
            }
            else
            {
                Product_Unit_Info zUnit = new Product_Unit_Info(txt_Unit.Text.Trim());
                if (zUnit.Key == 0)
                {
                    txt_Unit.StateCommon.Back.Color1 = ColorWarning;
                    zStrError += "- Kiểm tra đơn vị tính \n\r";
                }
                else
                {
                    txt_Unit.Tag = zUnit.Key;
                    txt_Unit.StateCommon.Back.Color1 = ColorNormal;
                }
            }
            if (txt_OrderIDFollow.Text.Trim().Length <= 0)
            {
                zIsError = true;
                txt_OrderIDFollow.StateCommon.Back.Color1 = ColorWarning;
                zStrError += "- Kiểm tra số chứng từ \n\r";
            }
            else
            {
                if (txt_OrderIDFollow.Text.Trim().ToUpper() == "CVTG")
                {
                    zIsError = false;
                    txt_OrderIDFollow.StateCommon.Back.Color1 = ColorNormal;
                }
                else
                {
                    int Count = Order_Data.CountOrderIDFollow(txt_OrderIDFollow.Text.Trim());
                    if (Count <= 0)
                    {
                        txt_OrderIDFollow.StateCommon.Back.Color1 = ColorWarning;
                        zStrError += "- Kiểm tra số chứng từ  \n\r";
                        zIsError = false;
                    }
                    else
                        txt_OrderIDFollow.StateCommon.Back.Color1 = ColorNormal;
                }
            }

            if (cbo_Team.SelectedIndex < 0)
            {
                zIsError = true;
                cbo_Team.StateCommon.DropBack.Color1 = ColorWarning;
                zStrError += "- Kiểm tra số nhóm thực hiện \n\r";
            }
            else
            {
                zIsError = false;
                cbo_Team.StateCommon.DropBack.Color1 = ColorNormal;
            }

            if (cbo_Status.SelectedIndex < 0)
            {
                zIsError = true;
                cbo_Status.StateCommon.DropBack.Color1 = ColorWarning;
                zStrError += "- Kiểm tra tình trạng \n\r";
            }
            else
            {
                zIsError = false;
                cbo_Status.StateCommon.DropBack.Color1 = ColorNormal;
            }

            if (zStrError != string.Empty)
            {
                zIsError = true;
                Panel_Message.Visible = true;
                lbl_Message.Text = zStrError;
            }
            else
            {
                zIsError = false;
                Panel_Message.Visible = false;
                lbl_Message.Text = "";
            }
            return zIsError;
        }

        private void btnClose_Panel_Message_Click(object sender, EventArgs e)
        {
            Panel_Message.Visible = false;
        }
        private void Btn_Export_Click(object sender, EventArgs e)
        {
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Danh sách đơn hàng.xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                Path = FDialog.FileName;
                DataTable dt = new DataTable();
                foreach (DataGridViewColumn col in GVOrder.Columns)
                {
                    dt.Columns.Add(col.HeaderText);
                }

                for (int i = 0; i < GVOrder.RowCount; i++)
                {
                    DataGridViewRow row = GVOrder.Rows[i];
                    DataRow dRow = dt.NewRow();

                    int k = 0;
                    foreach (DataGridViewCell cell in row.Cells)
                    {
                       
                            dRow[cell.ColumnIndex] = cell.Value;
                        k++;
                    }
                    dt.Rows.Add(dRow);
                }
                string Message = ExportTableToExcel(dt, Path);
                if (Message != "OK")
                    Utils.TNMessageBoxOK(Message, 4);
                else
                {
                    Message = "Đã tạo tập tin thành công !." + Environment.NewLine + "Bạn có muốn mở thư mục chứa ?.";
                    if (Utils.TNMessageBox(Message, 3) == "Y")
                    {
                        Process.Start(Path);
                    }
                }
            }
        }
        protected virtual bool IsFileLocked(FileInfo file)
        {
            try
            {
                using (FileStream stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    stream.Close();
                }
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }

            //file is not locked
            return false;
        }
        private string ExportTableToExcel(DataTable zTable, string Folder)
        {
            try
            {
                var newFile = new FileInfo(Folder);
                if (newFile.Exists)
                {
                    if (!IsFileLocked(newFile))
                    {
                        newFile.Delete();
                    }
                    else
                    {
                        return "Tập tin đang sử dụng !.";
                    }
                }

                using (var package = new ExcelPackage(newFile))
                {
                    //Tạo Sheet 1 mới với tên Sheet là DonHang
                    ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("PhanBo");
                    worksheet.Cells["A1"].LoadFromDataTable(zTable, true);
                    worksheet.Cells.Style.Font.Name = "Times New Roman";
                    worksheet.Cells.Style.Font.Size = 12;
                    worksheet.Cells.AutoFilter = true;
                    worksheet.Cells.AutoFitColumns();
                    worksheet.View.FreezePanes(2, 4);

                    //
                    for (int i = 2; i <= zTable.Rows.Count + 1; i++)
                    {
                        if(i==11||i==13||i==14|i==15||i==18)
                        worksheet.Column(i).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    }
                    //Rowheader và row tổng
                    worksheet.Row(1).Height = 60;
                    worksheet.Row(1).Style.WrapText = true;
                    worksheet.Row(1).Style.Font.Bold = true;
                    worksheet.Row(1).Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                    worksheet.Row(1).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    //worksheet.Row(zTable.Rows.Count + 1).Style.Font.Bold = true;//dòng tổng
                    //Chỉnh fix độ rộng cột
                    for (int j = 4; j <= zTable.Columns.Count; j++)
                    {
                        worksheet.Column(j).AutoFit();
                    }
                    worksheet.Column(1).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    //worksheet.Column(zTable.Columns.Count).Style.Font.Bold = true;//côt tổng
                    //Canh phải số
                    
                    //worksheet.Column(8).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    //worksheet.Column(9).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    //Lưu file Excel
                    package.SaveAs(newFile);
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
            return "OK";
        }
    }
}