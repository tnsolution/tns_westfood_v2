﻿namespace TNS.WinApp
{
    partial class Frm_Production_Calculator_V3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Production_Calculator_V3));
            this.HeaderControl = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btnMini = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnMax = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnClose = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.Panel_Left = new System.Windows.Forms.Panel();
            this.Panel_Order = new System.Windows.Forms.Panel();
            this.GVOrder = new System.Windows.Forms.DataGridView();
            this.kryptonHeader2 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.Panel_Work = new System.Windows.Forms.Panel();
            this.GVWork = new System.Windows.Forms.DataGridView();
            this.kryptonHeader4 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.Panel_Team = new System.Windows.Forms.Panel();
            this.GVTeam = new System.Windows.Forms.DataGridView();
            this.kryptonHeader1 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.Panel_Search = new System.Windows.Forms.Panel();
            this.btn_Save = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Count = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.label1 = new System.Windows.Forms.Label();
            this.Panel_Employee = new System.Windows.Forms.Panel();
            this.GVEmployee = new System.Windows.Forms.DataGridView();
            this.txt_title = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btnHideInfo = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnShowInfo = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.Panel_Right = new System.Windows.Forms.Panel();
            this.Panel_Order_Rate = new System.Windows.Forms.Panel();
            this.GVRate = new System.Windows.Forms.DataGridView();
            this.kryptonHeader9 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.Panel_Info = new System.Windows.Forms.Panel();
            this.txt_Quantity = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txt_TotalNu = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txt_TotalNam = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txt_Total = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txt_Percent = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txt_Price = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txt_Time = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txt_Basket = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txt_Kilogram = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.kryptonHeader5 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btn_Search = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.dte_Date = new TNS.SYS.TNDateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.Panel_Left.SuspendLayout();
            this.Panel_Order.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GVOrder)).BeginInit();
            this.Panel_Work.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GVWork)).BeginInit();
            this.Panel_Team.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GVTeam)).BeginInit();
            this.Panel_Search.SuspendLayout();
            this.Panel_Employee.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GVEmployee)).BeginInit();
            this.Panel_Right.SuspendLayout();
            this.Panel_Order_Rate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GVRate)).BeginInit();
            this.Panel_Info.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // HeaderControl
            // 
            this.HeaderControl.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnMini,
            this.btnMax,
            this.btnClose});
            this.HeaderControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.HeaderControl.Location = new System.Drawing.Point(0, 0);
            this.HeaderControl.Name = "HeaderControl";
            this.HeaderControl.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.HeaderControl.Size = new System.Drawing.Size(1366, 42);
            this.HeaderControl.TabIndex = 203;
            this.HeaderControl.Values.Description = "";
            this.HeaderControl.Values.Heading = "Đơn hàng đã tính năng xuất";
            this.HeaderControl.Values.Image = ((System.Drawing.Image)(resources.GetObject("HeaderControl.Values.Image")));
            this.HeaderControl.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Frm_Main_MouseDown);
            this.HeaderControl.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Frm_Main_MouseMove);
            this.HeaderControl.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Frm_Main_MouseUp);
            // 
            // btnMini
            // 
            this.btnMini.Image = ((System.Drawing.Image)(resources.GetObject("btnMini.Image")));
            this.btnMini.UniqueName = "F5F06E8241504E72ABB5BEA3F6A9B753";
            this.btnMini.Click += new System.EventHandler(this.btnMini_Click);
            // 
            // btnMax
            // 
            this.btnMax.Image = ((System.Drawing.Image)(resources.GetObject("btnMax.Image")));
            this.btnMax.UniqueName = "035D1A4881E44F58A084C31DE7352A94";
            this.btnMax.Click += new System.EventHandler(this.btnMax_Click);
            // 
            // btnClose
            // 
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.UniqueName = "11B07C6F4E1C4F9D8B91BD924CB0EBE6";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // Panel_Left
            // 
            this.Panel_Left.Controls.Add(this.Panel_Order);
            this.Panel_Left.Controls.Add(this.Panel_Work);
            this.Panel_Left.Controls.Add(this.Panel_Team);
            this.Panel_Left.Dock = System.Windows.Forms.DockStyle.Left;
            this.Panel_Left.Location = new System.Drawing.Point(0, 93);
            this.Panel_Left.Name = "Panel_Left";
            this.Panel_Left.Size = new System.Drawing.Size(450, 620);
            this.Panel_Left.TabIndex = 206;
            // 
            // Panel_Order
            // 
            this.Panel_Order.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Order.Controls.Add(this.GVOrder);
            this.Panel_Order.Controls.Add(this.kryptonHeader2);
            this.Panel_Order.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel_Order.Location = new System.Drawing.Point(0, 440);
            this.Panel_Order.Name = "Panel_Order";
            this.Panel_Order.Size = new System.Drawing.Size(450, 180);
            this.Panel_Order.TabIndex = 214;
            // 
            // GVOrder
            // 
            this.GVOrder.AllowUserToAddRows = false;
            this.GVOrder.AllowUserToDeleteRows = false;
            this.GVOrder.AllowUserToResizeColumns = false;
            this.GVOrder.AllowUserToResizeRows = false;
            this.GVOrder.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.GVOrder.ColumnHeadersHeight = 25;
            this.GVOrder.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.GVOrder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GVOrder.Location = new System.Drawing.Point(0, 30);
            this.GVOrder.Name = "GVOrder";
            this.GVOrder.ReadOnly = true;
            this.GVOrder.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GVOrder.Size = new System.Drawing.Size(450, 150);
            this.GVOrder.TabIndex = 203;
            // 
            // kryptonHeader2
            // 
            this.kryptonHeader2.AutoSize = false;
            this.kryptonHeader2.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader2.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader2.Name = "kryptonHeader2";
            this.kryptonHeader2.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader2.Size = new System.Drawing.Size(450, 30);
            this.kryptonHeader2.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader2.TabIndex = 200;
            this.kryptonHeader2.Values.Description = "";
            this.kryptonHeader2.Values.Heading = "3-Đơn hàng";
            // 
            // Panel_Work
            // 
            this.Panel_Work.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Work.Controls.Add(this.GVWork);
            this.Panel_Work.Controls.Add(this.kryptonHeader4);
            this.Panel_Work.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel_Work.Location = new System.Drawing.Point(0, 220);
            this.Panel_Work.Name = "Panel_Work";
            this.Panel_Work.Size = new System.Drawing.Size(450, 220);
            this.Panel_Work.TabIndex = 213;
            // 
            // GVWork
            // 
            this.GVWork.AllowUserToAddRows = false;
            this.GVWork.AllowUserToDeleteRows = false;
            this.GVWork.AllowUserToResizeColumns = false;
            this.GVWork.AllowUserToResizeRows = false;
            this.GVWork.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.GVWork.ColumnHeadersHeight = 25;
            this.GVWork.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.GVWork.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GVWork.Location = new System.Drawing.Point(0, 30);
            this.GVWork.Name = "GVWork";
            this.GVWork.ReadOnly = true;
            this.GVWork.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GVWork.Size = new System.Drawing.Size(450, 190);
            this.GVWork.TabIndex = 203;
            // 
            // kryptonHeader4
            // 
            this.kryptonHeader4.AutoSize = false;
            this.kryptonHeader4.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader4.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader4.Name = "kryptonHeader4";
            this.kryptonHeader4.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader4.Size = new System.Drawing.Size(450, 30);
            this.kryptonHeader4.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader4.TabIndex = 200;
            this.kryptonHeader4.Values.Description = "";
            this.kryptonHeader4.Values.Heading = "2-Công việc";
            // 
            // Panel_Team
            // 
            this.Panel_Team.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Team.Controls.Add(this.GVTeam);
            this.Panel_Team.Controls.Add(this.kryptonHeader1);
            this.Panel_Team.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel_Team.Location = new System.Drawing.Point(0, 0);
            this.Panel_Team.Name = "Panel_Team";
            this.Panel_Team.Size = new System.Drawing.Size(450, 220);
            this.Panel_Team.TabIndex = 204;
            // 
            // GVTeam
            // 
            this.GVTeam.AllowUserToAddRows = false;
            this.GVTeam.AllowUserToDeleteRows = false;
            this.GVTeam.AllowUserToResizeColumns = false;
            this.GVTeam.AllowUserToResizeRows = false;
            this.GVTeam.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.GVTeam.ColumnHeadersHeight = 25;
            this.GVTeam.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.GVTeam.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GVTeam.Location = new System.Drawing.Point(0, 30);
            this.GVTeam.Name = "GVTeam";
            this.GVTeam.ReadOnly = true;
            this.GVTeam.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GVTeam.Size = new System.Drawing.Size(450, 190);
            this.GVTeam.TabIndex = 203;
            // 
            // kryptonHeader1
            // 
            this.kryptonHeader1.AutoSize = false;
            this.kryptonHeader1.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader1.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader1.Name = "kryptonHeader1";
            this.kryptonHeader1.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader1.Size = new System.Drawing.Size(450, 30);
            this.kryptonHeader1.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader1.TabIndex = 200;
            this.kryptonHeader1.Values.Description = "";
            this.kryptonHeader1.Values.Heading = "1-Tổ/ nhóm";
            // 
            // Panel_Search
            // 
            this.Panel_Search.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Search.Controls.Add(this.btn_Save);
            this.Panel_Search.Controls.Add(this.btn_Count);
            this.Panel_Search.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Panel_Search.Location = new System.Drawing.Point(0, 713);
            this.Panel_Search.Name = "Panel_Search";
            this.Panel_Search.Size = new System.Drawing.Size(1366, 55);
            this.Panel_Search.TabIndex = 207;
            this.Panel_Search.Visible = false;
            // 
            // btn_Save
            // 
            this.btn_Save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Save.Location = new System.Drawing.Point(1243, 6);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Save.Size = new System.Drawing.Size(120, 40);
            this.btn_Save.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Save.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Save.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Save.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Save.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Save.TabIndex = 24;
            this.btn_Save.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Save.Values.Image")));
            this.btn_Save.Values.Text = "Cập nhật";
            this.btn_Save.Click += new System.EventHandler(this.Btn_Save_Click);
            // 
            // btn_Count
            // 
            this.btn_Count.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Count.Location = new System.Drawing.Point(1117, 6);
            this.btn_Count.Name = "btn_Count";
            this.btn_Count.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Count.Size = new System.Drawing.Size(120, 40);
            this.btn_Count.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Count.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Count.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Count.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Count.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Count.TabIndex = 23;
            this.btn_Count.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Count.Values.Image")));
            this.btn_Count.Values.Text = "Tính tiền";
            this.btn_Count.Click += new System.EventHandler(this.Btn_Count_Click);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.DodgerBlue;
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Location = new System.Drawing.Point(450, 93);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(2, 620);
            this.label1.TabIndex = 208;
            // 
            // Panel_Employee
            // 
            this.Panel_Employee.Controls.Add(this.GVEmployee);
            this.Panel_Employee.Controls.Add(this.txt_title);
            this.Panel_Employee.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel_Employee.Location = new System.Drawing.Point(452, 93);
            this.Panel_Employee.Name = "Panel_Employee";
            this.Panel_Employee.Size = new System.Drawing.Size(704, 620);
            this.Panel_Employee.TabIndex = 211;
            // 
            // GVEmployee
            // 
            this.GVEmployee.AllowUserToAddRows = false;
            this.GVEmployee.AllowUserToDeleteRows = false;
            this.GVEmployee.AllowUserToResizeColumns = false;
            this.GVEmployee.AllowUserToResizeRows = false;
            this.GVEmployee.ColumnHeadersHeight = 25;
            this.GVEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.GVEmployee.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GVEmployee.Location = new System.Drawing.Point(0, 30);
            this.GVEmployee.Name = "GVEmployee";
            this.GVEmployee.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GVEmployee.Size = new System.Drawing.Size(704, 590);
            this.GVEmployee.TabIndex = 201;
            // 
            // txt_title
            // 
            this.txt_title.AutoSize = false;
            this.txt_title.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnHideInfo,
            this.btnShowInfo});
            this.txt_title.Dock = System.Windows.Forms.DockStyle.Top;
            this.txt_title.Location = new System.Drawing.Point(0, 0);
            this.txt_title.Name = "txt_title";
            this.txt_title.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_title.Size = new System.Drawing.Size(704, 30);
            this.txt_title.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.txt_title.TabIndex = 203;
            this.txt_title.Values.Description = "";
            this.txt_title.Values.Heading = "....";
            // 
            // btnHideInfo
            // 
            this.btnHideInfo.Image = ((System.Drawing.Image)(resources.GetObject("btnHideInfo.Image")));
            this.btnHideInfo.UniqueName = "0010BF7FD3B9491A00975C9A919D6B8A";
            this.btnHideInfo.Click += new System.EventHandler(this.btnHideInfo_Click);
            // 
            // btnShowInfo
            // 
            this.btnShowInfo.Image = ((System.Drawing.Image)(resources.GetObject("btnShowInfo.Image")));
            this.btnShowInfo.UniqueName = "8D0A4B861A87443FB6855495AB7AD05A";
            this.btnShowInfo.Click += new System.EventHandler(this.btnShowInfo_Click);
            // 
            // Panel_Right
            // 
            this.Panel_Right.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Right.Controls.Add(this.Panel_Order_Rate);
            this.Panel_Right.Controls.Add(this.Panel_Info);
            this.Panel_Right.Controls.Add(this.kryptonHeader5);
            this.Panel_Right.Dock = System.Windows.Forms.DockStyle.Right;
            this.Panel_Right.Location = new System.Drawing.Point(1156, 93);
            this.Panel_Right.Name = "Panel_Right";
            this.Panel_Right.Size = new System.Drawing.Size(210, 620);
            this.Panel_Right.TabIndex = 216;
            // 
            // Panel_Order_Rate
            // 
            this.Panel_Order_Rate.Controls.Add(this.GVRate);
            this.Panel_Order_Rate.Controls.Add(this.kryptonHeader9);
            this.Panel_Order_Rate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel_Order_Rate.Location = new System.Drawing.Point(0, 294);
            this.Panel_Order_Rate.Name = "Panel_Order_Rate";
            this.Panel_Order_Rate.Size = new System.Drawing.Size(210, 326);
            this.Panel_Order_Rate.TabIndex = 211;
            // 
            // GVRate
            // 
            this.GVRate.AllowUserToAddRows = false;
            this.GVRate.AllowUserToDeleteRows = false;
            this.GVRate.AllowUserToResizeColumns = false;
            this.GVRate.AllowUserToResizeRows = false;
            this.GVRate.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.GVRate.ColumnHeadersHeight = 25;
            this.GVRate.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.GVRate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GVRate.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.GVRate.Location = new System.Drawing.Point(0, 30);
            this.GVRate.Name = "GVRate";
            this.GVRate.ReadOnly = true;
            this.GVRate.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GVRate.Size = new System.Drawing.Size(210, 296);
            this.GVRate.TabIndex = 201;
            // 
            // kryptonHeader9
            // 
            this.kryptonHeader9.AutoSize = false;
            this.kryptonHeader9.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader9.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader9.Name = "kryptonHeader9";
            this.kryptonHeader9.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader9.Size = new System.Drawing.Size(210, 30);
            this.kryptonHeader9.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader9.TabIndex = 202;
            this.kryptonHeader9.Values.Description = "";
            this.kryptonHeader9.Values.Heading = "Hệ số đơn hàng";
            // 
            // Panel_Info
            // 
            this.Panel_Info.Controls.Add(this.txt_Quantity);
            this.Panel_Info.Controls.Add(this.txt_TotalNu);
            this.Panel_Info.Controls.Add(this.txt_TotalNam);
            this.Panel_Info.Controls.Add(this.txt_Total);
            this.Panel_Info.Controls.Add(this.txt_Percent);
            this.Panel_Info.Controls.Add(this.txt_Price);
            this.Panel_Info.Controls.Add(this.label15);
            this.Panel_Info.Controls.Add(this.txt_Time);
            this.Panel_Info.Controls.Add(this.label13);
            this.Panel_Info.Controls.Add(this.label8);
            this.Panel_Info.Controls.Add(this.txt_Basket);
            this.Panel_Info.Controls.Add(this.label7);
            this.Panel_Info.Controls.Add(this.label16);
            this.Panel_Info.Controls.Add(this.label6);
            this.Panel_Info.Controls.Add(this.txt_Kilogram);
            this.Panel_Info.Controls.Add(this.label4);
            this.Panel_Info.Controls.Add(this.label5);
            this.Panel_Info.Controls.Add(this.label18);
            this.Panel_Info.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel_Info.Location = new System.Drawing.Point(0, 30);
            this.Panel_Info.Name = "Panel_Info";
            this.Panel_Info.Size = new System.Drawing.Size(210, 264);
            this.Panel_Info.TabIndex = 217;
            // 
            // txt_Quantity
            // 
            this.txt_Quantity.Location = new System.Drawing.Point(108, 7);
            this.txt_Quantity.Name = "txt_Quantity";
            this.txt_Quantity.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_Quantity.ReadOnly = true;
            this.txt_Quantity.Size = new System.Drawing.Size(96, 26);
            this.txt_Quantity.StateCommon.Border.ColorAngle = 1F;
            this.txt_Quantity.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_Quantity.StateCommon.Border.Rounding = 4;
            this.txt_Quantity.StateCommon.Border.Width = 1;
            this.txt_Quantity.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Quantity.TabIndex = 212;
            this.txt_Quantity.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txt_TotalNu
            // 
            this.txt_TotalNu.Location = new System.Drawing.Point(108, 230);
            this.txt_TotalNu.Name = "txt_TotalNu";
            this.txt_TotalNu.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_TotalNu.ReadOnly = true;
            this.txt_TotalNu.Size = new System.Drawing.Size(96, 26);
            this.txt_TotalNu.StateCommon.Border.ColorAngle = 1F;
            this.txt_TotalNu.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_TotalNu.StateCommon.Border.Rounding = 4;
            this.txt_TotalNu.StateCommon.Border.Width = 1;
            this.txt_TotalNu.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_TotalNu.TabIndex = 216;
            this.txt_TotalNu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txt_TotalNam
            // 
            this.txt_TotalNam.Location = new System.Drawing.Point(108, 202);
            this.txt_TotalNam.Name = "txt_TotalNam";
            this.txt_TotalNam.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_TotalNam.ReadOnly = true;
            this.txt_TotalNam.Size = new System.Drawing.Size(96, 26);
            this.txt_TotalNam.StateCommon.Border.ColorAngle = 1F;
            this.txt_TotalNam.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_TotalNam.StateCommon.Border.Rounding = 4;
            this.txt_TotalNam.StateCommon.Border.Width = 1;
            this.txt_TotalNam.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_TotalNam.TabIndex = 216;
            this.txt_TotalNam.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txt_Total
            // 
            this.txt_Total.Location = new System.Drawing.Point(108, 174);
            this.txt_Total.Name = "txt_Total";
            this.txt_Total.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_Total.ReadOnly = true;
            this.txt_Total.Size = new System.Drawing.Size(96, 26);
            this.txt_Total.StateCommon.Border.ColorAngle = 1F;
            this.txt_Total.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_Total.StateCommon.Border.Rounding = 4;
            this.txt_Total.StateCommon.Border.Width = 1;
            this.txt_Total.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Total.TabIndex = 216;
            this.txt_Total.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txt_Percent
            // 
            this.txt_Percent.Location = new System.Drawing.Point(108, 146);
            this.txt_Percent.Name = "txt_Percent";
            this.txt_Percent.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_Percent.ReadOnly = true;
            this.txt_Percent.Size = new System.Drawing.Size(96, 26);
            this.txt_Percent.StateCommon.Border.ColorAngle = 1F;
            this.txt_Percent.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_Percent.StateCommon.Border.Rounding = 4;
            this.txt_Percent.StateCommon.Border.Width = 1;
            this.txt_Percent.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Percent.TabIndex = 216;
            this.txt_Percent.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txt_Price
            // 
            this.txt_Price.Location = new System.Drawing.Point(108, 118);
            this.txt_Price.Name = "txt_Price";
            this.txt_Price.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_Price.ReadOnly = true;
            this.txt_Price.Size = new System.Drawing.Size(96, 26);
            this.txt_Price.StateCommon.Border.ColorAngle = 1F;
            this.txt_Price.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_Price.StateCommon.Border.Rounding = 4;
            this.txt_Price.StateCommon.Border.Width = 1;
            this.txt_Price.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Price.TabIndex = 216;
            this.txt_Price.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label15.Location = new System.Drawing.Point(20, 40);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(88, 15);
            this.label15.TabIndex = 201;
            this.label15.Text = "SL nguyên liệu";
            // 
            // txt_Time
            // 
            this.txt_Time.Location = new System.Drawing.Point(108, 90);
            this.txt_Time.Name = "txt_Time";
            this.txt_Time.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_Time.ReadOnly = true;
            this.txt_Time.Size = new System.Drawing.Size(96, 26);
            this.txt_Time.StateCommon.Border.ColorAngle = 1F;
            this.txt_Time.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_Time.StateCommon.Border.Rounding = 4;
            this.txt_Time.StateCommon.Border.Width = 1;
            this.txt_Time.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Time.TabIndex = 215;
            this.txt_Time.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label13.Location = new System.Drawing.Point(13, 13);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(91, 15);
            this.label13.TabIndex = 203;
            this.label13.Text = "SL thành phẩm";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label8.Location = new System.Drawing.Point(28, 237);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(74, 15);
            this.label8.TabIndex = 210;
            this.label8.Text = "Tổng giờ nữ";
            // 
            // txt_Basket
            // 
            this.txt_Basket.Location = new System.Drawing.Point(108, 63);
            this.txt_Basket.Name = "txt_Basket";
            this.txt_Basket.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_Basket.ReadOnly = true;
            this.txt_Basket.Size = new System.Drawing.Size(96, 26);
            this.txt_Basket.StateCommon.Border.ColorAngle = 1F;
            this.txt_Basket.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_Basket.StateCommon.Border.Rounding = 4;
            this.txt_Basket.StateCommon.Border.Width = 1;
            this.txt_Basket.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Basket.TabIndex = 214;
            this.txt_Basket.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label7.Location = new System.Drawing.Point(20, 209);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(84, 15);
            this.label7.TabIndex = 210;
            this.label7.Text = "Tổng giờ nam";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label16.Location = new System.Drawing.Point(67, 69);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(41, 15);
            this.label16.TabIndex = 205;
            this.label16.Text = "Số Rổ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label6.Location = new System.Drawing.Point(46, 181);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 15);
            this.label6.TabIndex = 210;
            this.label6.Text = "Tổng tiền";
            // 
            // txt_Kilogram
            // 
            this.txt_Kilogram.Location = new System.Drawing.Point(108, 35);
            this.txt_Kilogram.Name = "txt_Kilogram";
            this.txt_Kilogram.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_Kilogram.ReadOnly = true;
            this.txt_Kilogram.Size = new System.Drawing.Size(96, 26);
            this.txt_Kilogram.StateCommon.Border.ColorAngle = 1F;
            this.txt_Kilogram.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_Kilogram.StateCommon.Border.Rounding = 4;
            this.txt_Kilogram.StateCommon.Border.Width = 1;
            this.txt_Kilogram.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Kilogram.TabIndex = 213;
            this.txt_Kilogram.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label4.Location = new System.Drawing.Point(55, 151);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 15);
            this.label4.TabIndex = 210;
            this.label4.Text = "Tỷ lệ %";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label5.Location = new System.Drawing.Point(49, 96);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 15);
            this.label5.TabIndex = 208;
            this.label5.Text = "Thời gian";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label18.Location = new System.Drawing.Point(55, 123);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(53, 15);
            this.label18.TabIndex = 210;
            this.label18.Text = "Đơn Giá";
            // 
            // kryptonHeader5
            // 
            this.kryptonHeader5.AutoSize = false;
            this.kryptonHeader5.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader5.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader5.Name = "kryptonHeader5";
            this.kryptonHeader5.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader5.Size = new System.Drawing.Size(210, 30);
            this.kryptonHeader5.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader5.TabIndex = 200;
            this.kryptonHeader5.Values.Description = "";
            this.kryptonHeader5.Values.Heading = "Thông tin đơn hàng";
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.DodgerBlue;
            this.label2.Dock = System.Windows.Forms.DockStyle.Right;
            this.label2.Location = new System.Drawing.Point(1154, 93);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(2, 620);
            this.label2.TabIndex = 217;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panel2.Controls.Add(this.btn_Search);
            this.panel2.Controls.Add(this.dte_Date);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 42);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1366, 51);
            this.panel2.TabIndex = 218;
            // 
            // btn_Search
            // 
            this.btn_Search.Location = new System.Drawing.Point(198, 5);
            this.btn_Search.Name = "btn_Search";
            this.btn_Search.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Search.Size = new System.Drawing.Size(106, 40);
            this.btn_Search.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Search.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Search.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Search.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Search.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Search.TabIndex = 281;
            this.btn_Search.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Search.Values.Image")));
            this.btn_Search.Values.Text = "Tìm kiếm";
            // 
            // dte_Date
            // 
            this.dte_Date.CustomFormat = "dd/MM/yyyy";
            this.dte_Date.Location = new System.Drawing.Point(81, 14);
            this.dte_Date.Name = "dte_Date";
            this.dte_Date.Size = new System.Drawing.Size(102, 23);
            this.dte_Date.TabIndex = 123;
            this.dte_Date.Value = new System.DateTime(((long)(0)));
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(16, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 15);
            this.label3.TabIndex = 122;
            this.label3.Text = "Thời gian";
            // 
            // Frm_Production_Calculator_V3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1366, 768);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Panel_Employee);
            this.Controls.Add(this.Panel_Right);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Panel_Left);
            this.Controls.Add(this.Panel_Search);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.HeaderControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_Production_Calculator_V3";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Đơn hàng đã tính năng xuất";
            this.Load += new System.EventHandler(this.Frm_Production_Calculator_V3_Load);
            this.Panel_Left.ResumeLayout(false);
            this.Panel_Order.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GVOrder)).EndInit();
            this.Panel_Work.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GVWork)).EndInit();
            this.Panel_Team.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GVTeam)).EndInit();
            this.Panel_Search.ResumeLayout(false);
            this.Panel_Employee.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GVEmployee)).EndInit();
            this.Panel_Right.ResumeLayout(false);
            this.Panel_Order_Rate.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GVRate)).EndInit();
            this.Panel_Info.ResumeLayout(false);
            this.Panel_Info.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ComponentFactory.Krypton.Toolkit.KryptonHeader HeaderControl;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMini;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMax;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnClose;
        private System.Windows.Forms.Panel Panel_Left;
        private System.Windows.Forms.Panel Panel_Team;
        private System.Windows.Forms.DataGridView GVTeam;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader1;
        private System.Windows.Forms.Panel Panel_Search;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel Panel_Employee;
        private System.Windows.Forms.DataGridView GVEmployee;
        private System.Windows.Forms.Panel Panel_Work;
        private System.Windows.Forms.DataGridView GVWork;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader4;
        private System.Windows.Forms.Panel Panel_Right;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader5;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label18;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader txt_title;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnHideInfo;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnShowInfo;
        private System.Windows.Forms.Label label2;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_Quantity;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_Price;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_Time;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_Basket;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_Kilogram;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Count;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Save;
        private System.Windows.Forms.Panel Panel_Order;
        private System.Windows.Forms.DataGridView GVOrder;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel Panel_Order_Rate;
        private System.Windows.Forms.DataGridView GVRate;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader9;
        private System.Windows.Forms.Panel Panel_Info;
        private System.Windows.Forms.Label label3;
        private SYS.TNDateTimePicker dte_Date;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_Percent;
        private System.Windows.Forms.Label label4;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_Total;
        private System.Windows.Forms.Label label6;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_TotalNu;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_TotalNam;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Search;
    }
}