﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using TN.Connect;
using TN.Toolbox;
using TNS.CORE;
using TNS.HRM;
using TNS.IVT;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Production_Compare : Form
    {
        private string _OrderCompare = "";
        string _txtLog = "";
        int _TotalErr = 0;
        bool _IsSaved = false;//kiem tra đã lưu hay chưa
        FileInfo FileInfo;
        DataTable _TableOrder;
        DataTable _TableEmployee;
        List<Extend_Object> _ListOrder = new List<Extend_Object>();
        public Frm_Production_Compare()
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            btnClose.Click += btnClose_Click;
            btnMax.Click += btnMax_Click;
            btnMini.Click += btnMini_Click;

            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            GV_Compare.Click += GV_Compare_Click;
            GV_Order.Click += GV_Order_Click;
            btn_Search.Click += Btn_Search_Click;
            btn_Import.Click += Btn_Import_Click;
            btn_SaveAll.Click += btn_SaveAll_Click;
            btn_Log.Click += Btn_Log_Click;

            SetupLayoutGV_Compare();
            SetupLayoutGV_Order();
            SetupLayoutGV_Employee();
            dte_FromDate.Value = SessionUser.Date_Work;
            dte_ToDate.Value = SessionUser.Date_Work;
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
        }

        

        private void Frm_Production_Compare_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);

            Panel_Done.Visible = false;
            //Lấy tất cả nhóm trực tiếp sx  và khác bộ phận hỗ trợ sx
            string zSQL = @"SELECT TeamKey,TeamID +'-'+ TeamName AS TeamName  FROM SYS_Team 
                            WHERE RecordStatus <> 99 AND BranchKey=4 AND  DepartmentKey != 26
                            ORDER BY Rank";
            LoadDataToToolbox.KryptonComboBox(cbo_Team, zSQL, "---Chọn---");

        }
        private void Btn_Import_Click(object sender, EventArgs e)
        {
            OpenFileDialog zOpf = new OpenFileDialog
            {
                InitialDirectory = @"C:\",
                DefaultExt = "xlsx",
                Title = "Chọn tập tin Excel",
                Filter = "All Files|*.*",
                FilterIndex = 2,
                ReadOnlyChecked = true,
                CheckFileExists = true,
                CheckPathExists = true,
                RestoreDirectory = true,
                ShowReadOnly = true
            };

            if (zOpf.ShowDialog() == DialogResult.OK)
            {
                //PBar.Maximum = 0;
                _TableEmployee = new DataTable();
                _TableOrder = new DataTable();
                _ListOrder = new List<Extend_Object>();

                FileInfo = new FileInfo(zOpf.FileName);
                bool zcheck = Data_Access.CheckFileStatus(FileInfo);
                if (zcheck == true)
                {
                    Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                }
                else
                {
                    try
                    {
                        //lblFileName.Text = FileInfo.Name;
                        Cursor.Current = Cursors.WaitCursor;
                        using (Frm_Loading frm = new Frm_Loading(ProcessData)) { frm.ShowDialog(this); }
                        Cursor.Current = Cursors.Default;
                        if(_TotalErr!=0)
                        {
                            Utils.TNMessageBoxOK(_TotalErr + " đơn hàng phân tích bị lỗi!",1);
                            txtLog.Text = _txtLog.ToString();
                            Panel_Done.Visible = true;
                            btn_Log.Tag = 1;
                        }
                        else
                        {
                            btn_SaveAll_Click(null, null);
                        }
                    }
                    catch (Exception ex)
                    {
                        Utils.TNMessageBoxOK(ex.ToString(),4);
                    }
                }
            }
        }
        void ProcessData()
        {
            //-----------------Read Excel
            ExcelPackage package = new ExcelPackage(FileInfo);
            _TableOrder = Data_Access.GetTable(FileInfo.FullName, package.Workbook.Worksheets[1].Name);
            _TableEmployee = Data_Access.GetTable(FileInfo.FullName, package.Workbook.Worksheets[2].Name);
            package.Dispose();
            //-----------------Read Data Excel
            ReadData();
           
        }
        void ReadData()
        {
            _TotalErr = 0;
            for (int i = 0; i < _TableOrder.Rows.Count; i++)
            {
                
                DataRow zRow = _TableOrder.Rows[i];
                if (zRow[20]!=null && zRow[20].ToString()!= "" && zRow[20].ToString() != "0")
                    CreateOrder(zRow);
            }
        }
        void CreateOrder(DataRow RowExcel)
        {

            string Key = RowExcel[0].ToString().Trim();
            string IDCompare = RowExcel[1].ToString().Trim();
            string ID = RowExcel[2].ToString().Trim();
            // DateTime OrderDate = DateTime.Parse(RowExcel[3].ToString().Trim());
            DateTime OrderDate = new DateTime();
            if (!DateTime.TryParse(RowExcel[3].ToString().Trim(), out OrderDate))
            {
                OrderDate = DateTime.MinValue;
            }

            Extend_Object zOrder = new Extend_Object();
            zOrder.OrderID = ID;
            zOrder.OrderID_Compare = IDCompare;
            zOrder.OrderDate = OrderDate;

            string zTeamID = RowExcel[4].ToString().Trim();
            Team_Info zTeam = new Team_Info();
            zTeam.Get_Team_ID(zTeamID);
            zOrder.TeamKey = zTeam.Key;
            zOrder.TeamID = zTeamID;
            zOrder.TeamName = zTeam.TeamName;

            string zProductID = RowExcel[6].ToString().Trim();
            if (zProductID.Length > 0)
            {
                Product_Info zProduct = new Product_Info();
                zProduct.Get_Product_Info_ID(zProductID);
                zOrder.ProductKey = zProduct.ProductKey;
                zOrder.ProductID = zProduct.ProductID;
                zOrder.ProductName = zProduct.ProductName;
            }

            string zStageID = RowExcel[8].ToString().Trim();
            Product_Stages_Info zStage = new Product_Stages_Info();
            zStage.GetStagesID_Info(zStageID);
            if (zStage.Key > 0)
            {
                zOrder.WorkKey = zStage.Key;
                zOrder.WorkID = zStage.StagesID;
                zOrder.WorkName = zStage.StageName;
                zOrder.GroupKey = zStage.GroupKey;
                zOrder.GroupID = zStage.GroupID;
            }

            float zOrderTime = 0;
            float.TryParse(RowExcel[20].ToString().Trim(), out zOrderTime);
            zOrder.OrderTime = zOrderTime;
            float zOrderKg = 0;
            float.TryParse(RowExcel[21].ToString().Trim(), out zOrderKg);
            zOrder.OrderKg = zOrderKg;
            float Worktime = 0;
            float.TryParse(RowExcel[22].ToString().Trim(), out Worktime);
            zOrder.WorkTime = Worktime;
            zOrder.OrderKey = Order_Data.FindOrderKey(ID);

            zOrder.CreatedBy = SessionUser.UserLogin.Key;
            zOrder.CreatedName = SessionUser.UserLogin.EmployeeName;
            zOrder.ModifiedBy = SessionUser.UserLogin.Key;
            zOrder.ModifiedName = SessionUser.UserLogin.EmployeeName;
            //Check Record
            zOrder.Message += CheckOrder(RowExcel);

            string zSelectEmployee = "[" + _TableEmployee.Columns[9].ColumnName + "]='" + RowExcel[2].ToString().Trim() + "'";
            DataRow[] zEmployee = _TableEmployee.Select(zSelectEmployee);

           
            if (zEmployee.Length > 0)
            {
                List<Order_Extend_Employee_Info> zEmployeeList = new List<Order_Extend_Employee_Info>();
                foreach (DataRow r in zEmployee)
                {
                    string Message = CheckEmployee(r);
                    zOrder.Message += Message;

                    Order_Extend_Employee_Info zOrderEmployee = new Order_Extend_Employee_Info();
                    zOrderEmployee.OrderDate = OrderDate;
                    string zEmployeeID = r[1].ToString().Trim();
                    Employee_Info zEmployeeInfo = new Employee_Info();
                    zEmployeeInfo.GetEmployeeID(zEmployeeID);
                    zOrderEmployee.EmployeeID = zEmployeeID;
                    zOrderEmployee.EmployeeKey = zEmployeeInfo.Key;
                    zOrderEmployee.EmployeeName = zEmployeeInfo.FullName;

                    zOrderEmployee.TeamKey = zTeam.Key;
                    zOrderEmployee.TeamID = zTeamID;
                    zOrderEmployee.TeamName = zTeam.TeamName;


                    zOrderEmployee.OrderID = ID;
                    zOrderEmployee.OrderID_Compare = IDCompare;
                    zOrderEmployee.OrderDate = OrderDate;
                    float zThoiGianGNS = 0;
                    float.TryParse(r[05].ToString().Trim(), out zThoiGianGNS);
                    zOrderEmployee.ThoiGianGNS = zThoiGianGNS;

                    float zTimeTeam = 0;
                    float.TryParse(r[11].ToString().Trim(), out zTimeTeam);
                    zOrderEmployee.TimeTeam = zTimeTeam;

                    float zThoiGianTNS = 0;
                    float.TryParse(r[12].ToString().Trim(), out zThoiGianTNS);
                    zOrderEmployee.ThoiGianTNS = zThoiGianTNS;

                    zOrderEmployee.OrderKey = Order_Data.FindOrderKey(ID);
                    zOrderEmployee.RecordStatus = 1;

                    zOrderEmployee.CreatedBy = SessionUser.UserLogin.Key;
                    zOrderEmployee.CreatedName = SessionUser.UserLogin.EmployeeName;
                    zOrderEmployee.ModifiedBy = SessionUser.UserLogin.Key;
                    zOrderEmployee.ModifiedName = SessionUser.UserLogin.EmployeeName;
                    zEmployeeList.Add(zOrderEmployee);
                }
                zOrder.List_Employee = zEmployeeList;
            }

            zOrder.CreatedName = SessionUser.UserLogin.EmployeeName;
            zOrder.CreatedBy = SessionUser.UserLogin.Key;
            if(zOrder.Message!=string.Empty)
            {
                _TotalErr++;
                _txtLog += "/---/" + Environment.NewLine;
                _txtLog += "// Đơn hàng: [ " + ID + " ] lỗi.Chi tiết://" + Environment.NewLine;
                _txtLog += "//" + zOrder.Message + "//" + Environment.NewLine;
            }

            _ListOrder.Add(zOrder);
        }

        #region [Check Value Record Excel]
        string CheckOrder(DataRow RowExcel)
        {
            bool isError = false;
            string zMessage = "Vui lòng kiểm tra: " + Environment.NewLine;

            //---kiểm tra mã đơn hàng tham chiếu gốc-----------------------------------
            //kiểm tra order id gốc có giá trị
            string zOrderID = RowExcel[1].ToString();
            if (zOrderID.Trim() == "")
            {
                zMessage += "Mã đơn hàng tham chiếu bị rỗng" + Environment.NewLine;
                isError = true;
            }
            else
            {
                //kiểm tra order key gốc đã tồn tại chưa
                int zOrderKey = Order_Data.FindOrderID_Compare(zOrderID);
                if (zOrderKey == 0)
                {
                    zMessage += "Mã đơn hàng tham chiếu [" + zOrderID + "] chưa tồn tại" + Environment.NewLine;
                    isError = true;
                }
            }

            //---kiểm tra mã đơn hàng của pm----------------------------------------------
            //kiểm tra order id có giá trị
            string zOrderIDPM = RowExcel[2].ToString();
            if (zOrderIDPM.Trim() == "")
            {
                zMessage += "Mã đơn hàng bị rỗng" + Environment.NewLine;
                isError = true;
            }
            else
            {
                //kiểm tra order key đã tồn tại chưa
                int zOrderKeyPM = Order_Data.FindOrderID(zOrderIDPM);
                if (zOrderKeyPM == 0)
                {
                    zMessage += "Mã đơn hàng [" + zOrderIDPM + "] chưa import" + Environment.NewLine;
                    isError = true;
                }
            }

            DateTime OrderDate = new DateTime();
            if (!DateTime.TryParse(RowExcel[3].ToString().Trim(), out OrderDate))
            {
                zMessage += "Ngày tháng không đúng" + Environment.NewLine;
                isError = true;
            }
            else
            {
                if (SessionUser.Date_Lock >= OrderDate)
                {
                    zMessage += "Thời gian này đã khóa sổ.!" + Environment.NewLine;
                    isError = true;
                }
            }    

            //--- kiểm tra nhóm đã có cài đặt chưa -------------------------------------------------------
            string zTeamID = RowExcel[4].ToString();
            Team_Info zTeam = new Team_Info();
            zTeam.Get_Team_ID(zTeamID);
            if (zTeamID == "")
            {
                zMessage += "Mã tổ nhóm bị rỗng" + Environment.NewLine;
                isError = true;
            }
            else
            {
                if (zTeam.Key == 0)
                {
                    zMessage += "Tổ nhóm: [" + zTeamID + "] không tồn tại" + Environment.NewLine;
                    isError = true;
                }
            }

            //--- kiểm tra product đã có cài đặt chưa -----------------------------------------------------
            string zProductID = RowExcel[6].ToString().Trim();
            if (zProductID == "")
            {
                zMessage += "Mã sản phẩm bị rỗng" + Environment.NewLine;
                isError = true;
            }
            else
            {
                Product_Info zProduct = new Product_Info();
                zProduct.Get_Product_Info_ID(zProductID);
                if (zProduct.ProductKey == "")
                {
                    zMessage += "Sản phẩm: [" + zProductID + "] không tồn tại" + Environment.NewLine;
                    isError = true;
                }
            }

            //--- kiểm tra công đoạn đã có cài đặt chưa ---------------------------------------------------
            string zStageID = RowExcel[8].ToString().Trim();
            Product_Stages_Info zStage = new Product_Stages_Info();
            zStage.GetStagesID_Info(zStageID);
            if (zStageID == "")
            {
                zMessage += "Mã công việc bị rỗng" + Environment.NewLine;
                isError = true;
            }
            else
            {
                if (zStage.Key == 0)
                {
                    zMessage += "Công việc: [" + zStageID + "] không tồn tại" + Environment.NewLine;
                    isError = true;
                }
            }

            if (isError)
                return zMessage;
            else
                return string.Empty;
        }
        string CheckEmployee(DataRow RowExcel)
        {
            bool isError = false;
            string zMessage = "Vui lòng kiểm tra: " + Environment.NewLine;

            //---kiểm tra mã thẻ nhân viên----------------------------------------------
            string zEmployeeID = RowExcel[1].ToString();
            if (zEmployeeID.Trim() == "")
            {
                zMessage += "Mã thẻ công nhân bị rỗng" + Environment.NewLine;
                isError = true;
            }
            else
            {
                Employee_Info zEmployee = new Employee_Info();
                zEmployee.GetEmployeeID(zEmployeeID);

                if (zEmployee.Key == "")
                {
                    zMessage += "Số thẻ:" + zEmployeeID + " không tồn tại" + Environment.NewLine;
                    isError = true;
                }
                else
                {
                    if (zEmployee.TeamKey == 0)
                    {
                        zMessage += "Số thẻ:" + zEmployeeID + " chưa chọn tổ nhóm" + Environment.NewLine;
                        isError = true;
                    }
                }
            }

            if (RowExcel[6].ToString() != "")
            {
                if (RowExcel[7].ToString() == "" &&
                    RowExcel[8].ToString() == "")
                {
                    zMessage += "Khi mượn người phải chọn ăn chung hay ăn riêng" + Environment.NewLine;
                    isError = true;
                }

                if (RowExcel[7].ToString() != "" &&
                    RowExcel[8].ToString() != "")
                {
                    zMessage += "Khi mượn người không được vừa ăn chung hay ăn riêng" + Environment.NewLine;
                    isError = true;
                }
            }

            if (isError)
                return zMessage;
            else
                return string.Empty;
        }
        #endregion

        string _OrderID = "";//Lấy ID để nếu có lỗi biết đơn hàng nào lỗi
        void InsertData()
        {
            if(_ListOrder.Count>0)
            CreateInserCommand();
        }
        void CreateInserCommand()
        {
            foreach (Extend_Object zOrder in _ListOrder)
            {
                _OrderID = "";
                StringBuilder zSb = new StringBuilder();
                Guid zNewID = Guid.NewGuid();
                string zExtendKey = zNewID.ToString();
                if (zOrder.Message == string.Empty)
                {
                    #region [Order SQL]
                    //Xóa bảng cha bảng con 1 đơn hàng
                    zSb.AppendLine(@" 
DELETE FROM [dbo].[FTR_Order_Extend] WHERE OrderID= ");
                    zSb.Append("'"+zOrder.OrderID+"' ");
                    zSb.AppendLine(@" 
DELETE FROM [dbo].[FTR_Order_Extend_Employee] WHERE OrderID= ");
                    zSb.Append("'"+zOrder.OrderID+"' ");

                    //Thêm bảng cha
                    zSb.AppendLine(@"
INSERT INTO [dbo].[FTR_Order_Extend](
ExtendKey,OrderKey,OrderID,OrderID_Compare,OrderDate,OrderTime,
OrderKg,WorkTime,ProductKey,ProductID,ProductName,TeamKey,
TeamID,TeamName,WorkKey,WorkID,WorkName,GroupKey,GroupID,
RecordStatus ,CreatedOn,CreatedBy,CreatedName,ModifiedOn,ModifiedBy,ModifiedName
 )");
                    zSb.AppendLine("VALUES");
                    zSb.AppendLine("(");
                    zSb.Append("CAST('"+ zExtendKey+ "'AS UNIQUEIDENTIFIER), ");
                    zSb.Append("CAST('"+zOrder.OrderKey+ "'AS UNIQUEIDENTIFIER),");
                    zSb.Append("'" + zOrder.OrderID + "',");
                    zSb.Append("'" + zOrder.OrderID_Compare + "',");
                    zSb.Append("'" + zOrder.OrderDate.ToString("yyyy-MM-dd 00:00:01") + "',");
                    zSb.Append("" + zOrder.OrderTime + ",");
                    zSb.Append("" + zOrder.OrderKg + ",");
                    zSb.Append("" + zOrder.WorkTime + ",");
                    zSb.Append("'" + zOrder.ProductKey + "',");
                    zSb.Append("'" + zOrder.ProductID + "',");
                    zSb.Append("N'" + zOrder.ProductName + "',");
                    zSb.Append("" + zOrder.TeamKey.ToString() + ",");
                    zSb.Append("'" + zOrder.TeamID.ToString() + "',");
                    zSb.Append("N'" + zOrder.TeamName.ToString() + "',");
                    zSb.Append("" + zOrder.WorkKey.ToString() + ",");
                    zSb.Append("'" + zOrder.WorkID.ToString() + "',");
                    zSb.Append("N'" + zOrder.WorkName.ToString() + "',");
                    zSb.Append("" + zOrder.GroupKey.ToString() + ",");
                    zSb.Append("'" + zOrder.GroupID.ToString() + "',");
                    zSb.Append("0,");
                    zSb.Append("GETDATE(),");
                    zSb.Append("'" + zOrder.CreatedBy + "',");
                    zSb.Append("N'" + zOrder.CreatedName + "',");
                    zSb.Append("GETDATE(),");
                    zSb.Append("'" + zOrder.ModifiedBy + "',");
                    zSb.Append("N'" + zOrder.ModifiedName + "'");
                    zSb.AppendLine(")");
                    _OrderID = zOrder.OrderID;

                    #endregion
                    foreach (Order_Extend_Employee_Info zEmployee in zOrder.List_Employee)
                    {
                        zSb.AppendLine(@"
INSERT INTO [dbo].[FTR_Order_Extend_Employee](
ExtendKey,TeamKey,TeamID,TeamName,EmployeeKey,EmployeeID,EmployeeName,
OrderKey,OrderID,OrderID_Compare,OrderDate,TimeTeam,ThoiGianGNS,ThoiGianTNS,
RecordStatus,CreatedOn,CreatedBy,CreatedName,ModifiedOn,ModifiedBy,ModifiedName
)");
                        zSb.AppendLine("VALUES");
                        zSb.AppendLine("(");
                        zSb.Append("CAST('" + zExtendKey + "'AS UNIQUEIDENTIFIER), ");
                        zSb.Append("" + zEmployee.TeamKey.ToString() + ",");
                        zSb.Append("'" + zEmployee.TeamID.ToString() + "',");
                        zSb.Append("N'" + zEmployee.TeamName.ToString() + "',");
                        zSb.Append("'" + zEmployee.EmployeeKey.ToString() + "',");
                        zSb.Append("'" + zEmployee.EmployeeID.ToString() + "',");
                        zSb.Append("N'" + zEmployee.EmployeeName.ToString() + "',");
                        zSb.Append("CAST('" + zEmployee.OrderKey.ToString() + "'AS UNIQUEIDENTIFIER),");
                        zSb.Append("'" + zEmployee.OrderID.ToString() + "',");
                        zSb.Append("'" + zEmployee.OrderID_Compare.ToString() + "',");
                        zSb.Append("'" + zEmployee.OrderDate.ToString("yyyy-MM-dd 00:00:01") + "',");
                        zSb.Append("" + zEmployee.TimeTeam.ToString() + ",");
                        zSb.Append("" + zEmployee.ThoiGianGNS.ToString() + ",");
                        zSb.Append("" + zEmployee.ThoiGianTNS.ToString() + ",");
                        zSb.Append("0,");
                        zSb.Append("GETDATE(),");
                        zSb.Append("'" + zEmployee.CreatedBy.ToString() + "',");
                        zSb.Append("N'" + zEmployee.CreatedName.ToString() + "',");
                        zSb.Append("GETDATE(),");
                        zSb.Append("'" + zEmployee.ModifiedBy.ToString() + "',");
                        zSb.Append("N'" + zEmployee.ModifiedName.ToString() + "'");
                        zSb.AppendLine(")");
                    }

                    string zString = InsertQuery(zSb.ToString());
                    if (zString != string.Empty)
                    {
                        _txtLog += "/---/" + Environment.NewLine;
                        _txtLog += "// Đơn hàng: [ " + _OrderID + " ] lỗi//" + Environment.NewLine;
                        _txtLog += "// Vui lòng liên hệ IT!.Chi tiết://" + Environment.NewLine;
                        _txtLog += "//" + zString + "//" + Environment.NewLine;
                        _TotalErr++;

                    }
                }
            }
        }
        public string InsertQuery(string zSQL)
        {
            string zResult = "";
            string zMessage = "";
            //---------- String SQL Access Database ---------------
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                zMessage = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zMessage;
        }
        private void btn_SaveAll_Click(object sender, EventArgs e)
        {
            if (SessionUser.Date_Lock >= dte_ToDate.Value)
            {
                Utils.TNMessageBoxOK("Thời gian này đã khóa sổ.!", 2);
                return;
            }
            txtLog.Text = "";
            _TotalErr = 0;
            using (Frm_Loading frm = new Frm_Loading(InsertData)) { frm.ShowDialog(this); }

            if (_TotalErr > 0)
            {
                Utils.TNMessageBoxOK("Còn " + _TotalErr + " Đơn hàng chưa lưu !.",2);
                txtLog.Text = _txtLog.ToString();
                Panel_Done.Visible = true;
                btn_Log.Tag = 1;
            }
            else
            {
                _IsSaved = true;
                Utils.TNMessageBoxOK("Cập nhật thành công !.",3);
            }
        }
        private void Btn_Log_Click(object sender, EventArgs e)
        {
            if (btn_Log.Tag.ToInt() == 0)
            {
                Panel_Done.Visible = true;
                btn_Log.Tag = 1;
            }
            else
            {
                Panel_Done.Visible = false;
                btn_Log.Tag = 0;
            }
        }

        #region [GV Compare]
        private void SetupLayoutGV_Compare()
        {
            GV_Column zCol;
            GV_Compare.ColumnAdd("No", "Stt", 30);
            GV_Compare.ColumnAdd("OrderDate", "Ngày", 80);
            GV_Compare.ColumnAdd("OrderCompare", "Đơn hàng", 130);

            zCol = new GV_Column();
            zCol.IsSum = true;
            zCol.CheckEmpty = true;
            zCol.ColumnName = "OrderKg";
            zCol.HeaderText = "Thành phẩm";
            zCol.Width = 100;
            zCol.Style = "Number";
            GV_Compare.ColumnAdd(zCol);

            zCol = new GV_Column();
            zCol.IsSum = true;
            zCol.CheckEmpty = true;
            zCol.ColumnName = "OrderTime";
            zCol.HeaderText = "Giờ GNS";
            zCol.Width = 100;
            zCol.Style = "Number";
            GV_Compare.ColumnAdd(zCol);

            zCol = new GV_Column();
            zCol.IsSum = true;
            zCol.CheckEmpty = true;
            zCol.ColumnName = "WorkTime";
            zCol.HeaderText = "Giờ phân bổ";
            zCol.Width = 100;
            zCol.Style = "Number";
            GV_Compare.ColumnAdd(zCol);

            GV_Compare.ColumnAdd("WorkName", "Công việc", 250);
            //GV_Team.FillLastColumn = true;
            GV_Compare.IsSum = true;
            GV_Compare.AllowAddRow = false;
            GV_Compare.Init();
            GV_Compare.GetSum(false);
        }
        private void InitData_GVCompare(DataGridView GV, DataTable Table)
        {
            GV.Rows.Clear();
            if (Table != null)
            {
                int i = 0;
                foreach (DataRow r in Table.Rows)
                {

                    GV.Rows.Add();
                    DataGridViewRow GvRow = GV.Rows[i];
                    GvRow.Cells["No"].Value = (i + 1).ToString();
                    GvRow.Cells["OrderCompare"].Tag = r["OrderID_Compare"].ToString();
                    if(r["OrderDate"]!=null)
                    {
                        DateTime zDatetime = DateTime.Parse( r["OrderDate"].ToString());
                        GvRow.Cells["OrderDate"].Value = zDatetime.ToString("dd/MM/yyyy");
                    }

                    GvRow.Cells["OrderCompare"].Value = r["OrderID_Compare"].ToString();
                    GvRow.Cells["WorkName"].Value = r["WorkName"].ToString();
                    float zOrderKg = 0;
                    if (r["OrderKg"] != null)
                    {
                        zOrderKg = float.Parse(r["OrderKg"].ToString());
                    }
                    GvRow.Cells["OrderKg"].Value = zOrderKg.ToString("n2");

                    float zOrderTime = 0;
                    if (r["OrderTime"] != null)
                    {
                        zOrderTime = float.Parse(r["OrderTime"].ToString());
                    }
                    GvRow.Cells["OrderTime"].Value = zOrderTime.ToString("n2");

                    float zWorkTime = 0;
                    if (r["WorkTime"] != null)
                    {
                        zWorkTime = float.Parse(r["WorkTime"].ToString());
                    }
                    GvRow.Cells["WorkTime"].Value = zWorkTime.ToString("n2");
                    i++;
                }
                GV_Compare.GetSum(false);
                //GV.ClearSelection();
            }
        }
        private void GV_Compare_Click(object sender, EventArgs e)
        {
            if (GV_Compare.CurrentRow.Cells["OrderCompare"].Tag != null)
            {
                this.Cursor = Cursors.WaitCursor;
                _OrderCompare = GV_Compare.CurrentRow.Cells["OrderCompare"].Tag.ToString();

                DataTable zTableOrder = Order_Data.List_Order(_OrderCompare);
                InitData_GVOrder(GV_Order, zTableOrder);
                txt_Realy.Text = GV_Compare.CurrentRow.Cells["OrderKg"].Value.ToString();
                txt_Time.Text = GV_Compare.CurrentRow.Cells["WorkTime"].Value.ToString();
                txt_WorkName.Text = GV_Compare.CurrentRow.Cells["WorkName"].Value.ToString();
                txt_OrderKg.Text = "";
                txt_OrderTime.Text = "";
                txt_ProductName.Text = "";
                GV_Employee.Rows.Clear();
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region [GV Order]
        private void SetupLayoutGV_Order()
        {
            GV_Column zCol;
            GV_Order.ColumnAdd("No", "Stt", 30);
            GV_Order.ColumnAdd("OrderDate", "Ngày", 80);
            GV_Order.ColumnAdd("OrderID", "Đơn hàng", 130);

            zCol = new GV_Column();
            zCol.IsSum = true;
            zCol.CheckEmpty = true;
            zCol.ColumnName = "OrderKg";
            zCol.HeaderText = "Thành phẩm";
            zCol.Width = 100;
            zCol.Style = "Number";
            GV_Order.ColumnAdd(zCol);

            zCol = new GV_Column();
            zCol.IsSum = true;
            zCol.CheckEmpty = true;
            zCol.ColumnName = "OrderTime";
            zCol.HeaderText = "Giờ GNS";
            zCol.Width = 80;
            zCol.Style = "Number";
            GV_Order.ColumnAdd(zCol);

            zCol = new GV_Column();
            zCol.IsSum = true;
            zCol.CheckEmpty = true;
            zCol.ColumnName = "WorkTime";
            zCol.HeaderText = "Giờ Phân bổ";
            zCol.Width = 80;
            zCol.Style = "Number";
            GV_Order.ColumnAdd(zCol);

            GV_Order.ColumnAdd("ProductID", "Mã TP", 100);
            GV_Order.ColumnAdd("ProductName", "Têm thành phẩm", 250);
            //GV_Team.FillLastColumn = true;
            GV_Order.IsSum = true;
            GV_Order.AllowAddRow = false;
            GV_Order.Init();
            GV_Order.GetSum(false);
        }
        private void InitData_GVOrder(DataGridView GV, DataTable Table)
        {
            GV.Rows.Clear();
            if (Table != null)
            {
                int i = 0;
                foreach (DataRow r in Table.Rows)
                {

                    GV.Rows.Add();
                    DataGridViewRow GvRow = GV.Rows[i];
                    GvRow.Cells["No"].Value = (i + 1).ToString();
                    GvRow.Cells["No"].Tag = r["OrderKey"].ToString();
                    if (r["OrderDate"] != null)
                    {
                        DateTime zDatetime = DateTime.Parse(r["OrderDate"].ToString());
                        GvRow.Cells["OrderDate"].Value = zDatetime.ToString("dd/MM/yyyy");
                    }

                    GvRow.Cells["OrderID"].Value = r["OrderID"].ToString();
                    float zOrderKg = 0;
                    if (r["OrderKg"] != null)
                    {
                        zOrderKg = float.Parse(r["OrderKg"].ToString());
                    }
                    GvRow.Cells["OrderKg"].Value = zOrderKg.ToString("n2");
                    float zOrderTime = 0;
                    if (r["OrderTime"] != null)
                    {
                        zOrderTime = float.Parse(r["OrderTime"].ToString());
                    }
                    GvRow.Cells["OrderTime"].Value = zOrderTime.ToString("n2");

                    float zWorkTime = 0;
                    if (r["WorkTime"] != null)
                    {
                        zWorkTime = float.Parse(r["WorkTime"].ToString());
                    }
                    GvRow.Cells["WorkTime"].Value = zWorkTime.ToString("n2");

                    GvRow.Cells["ProductID"].Value = r["ProductID"].ToString();
                    GvRow.Cells["ProductName"].Value = r["ProductName"].ToString();
                    i++;
                }
                GV_Order.GetSum(false);
                //GV.ClearSelection();
            }
        }
        private void GV_Order_Click(object sender, EventArgs e)
        {
            if (GV_Order.CurrentRow.Cells["No"].Tag != null)
            {
                this.Cursor = Cursors.WaitCursor;
                string OrderKey = GV_Order.CurrentRow.Cells["No"].Tag.ToString();

                DataTable zTableOrder = Order_Data.List_ExtentEmployee(OrderKey);
                InitData_Employee(GV_Employee, zTableOrder);
                txt_OrderKg.Text = GV_Order.CurrentRow.Cells["OrderKg"].Value.ToString();
                txt_OrderTime.Text = GV_Order.CurrentRow.Cells["OrderTime"].Value.ToString();
                txt_ProductName.Text = GV_Order.CurrentRow.Cells["ProductID"].Value.ToString()
                +" - "+GV_Order.CurrentRow.Cells["ProductName"].Value.ToString();
                this.Cursor = Cursors.Default;
            }
        }
        #endregion

        #region [GV Employee]
        private void SetupLayoutGV_Employee()
        {
            GV_Column zCol;
            GV_Employee.ColumnAdd("No", "Stt", 30);
            GV_Employee.ColumnAdd("TeamID", "Mã nhóm", 100);
            GV_Employee.ColumnAdd("EmployeeID", "Mã thẻ", 100);
            GV_Employee.ColumnAdd("EmployeeName", "Họ & tên", 250);

            zCol = new GV_Column();
            zCol.IsSum = true;
            zCol.CheckEmpty = true;
            zCol.ColumnName = "ThoiGianGNS";
            zCol.HeaderText = "Giờ GNS";
            zCol.Width = 110;
            zCol.Style = "Number";

            GV_Employee.ColumnAdd(zCol);
            zCol = new GV_Column();
            zCol.IsSum = true;
            zCol.CheckEmpty = true;
            zCol.ColumnName = "ThoiGianTNS";
            zCol.HeaderText = "Giờ phân bổ";
            zCol.Width = 110;
            zCol.Style = "Number";

            GV_Employee.ColumnAdd(zCol);

            //GV_Team.FillLastColumn = true;
            GV_Employee.IsSum = true;
            GV_Employee.AllowAddRow = false;
            GV_Employee.Init();
            GV_Employee.GetSum(false);
        }
        private void InitData_Employee(DataGridView GV, DataTable Table)
        {
            GV.Rows.Clear();
            float zToTal = 0;
            if (Table != null)
            {
                int i = 0;
                foreach (DataRow r in Table.Rows)
                {

                    GV.Rows.Add();
                    DataGridViewRow GvRow = GV.Rows[i];
                    GvRow.Cells["No"].Value = (i + 1).ToString();
                    GvRow.Cells["No"].Tag = r["EmployeeKey"].ToString();
                    GvRow.Cells["TeamID"].Value = r["TeamID"].ToString();
                    GvRow.Cells["EmployeeID"].Value = r["EmployeeID"].ToString();
                    GvRow.Cells["EmployeeName"].Value = r["EmployeeName"].ToString();
                    GvRow.Cells["ThoiGianGNS"].Value = r["ThoiGianGNS"].ToString();
                    float zThoiGianGNS = 0;
                    if(float.TryParse(r["ThoiGianGNS"].ToString(),out zThoiGianGNS))
                    {

                    }
                    zToTal += zThoiGianGNS;
                    float zTNS = 0;
                    if (r["ThoiGianTNS"] != null)
                    {
                        if (float.TryParse(r["ThoiGianTNS"].ToString(), out zTNS))
                        {

                        }
                    }
                    GvRow.Cells["ThoiGianTNS"].Value = zTNS.Ton2String();
                    i++;
                }
                GV_Employee.GetSum(false);
                GV.ClearSelection();
            }
        }
        #endregion

        private void Btn_Search_Click(object sender, EventArgs e)
        {
            DataTable ztable = Order_Data.List_OrderCompare(dte_FromDate.Value, dte_ToDate.Value,txt_OrderIDCompare.Text.Trim(),cbo_Team.SelectedValue.ToInt());
            InitData_GVCompare(GV_Compare, ztable);
        }
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion
    }
}
