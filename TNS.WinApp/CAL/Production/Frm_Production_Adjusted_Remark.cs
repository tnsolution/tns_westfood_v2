﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using TNS.CORE;
using TNS.HRM;
using TNS.Misc;

namespace TNS.WinApp
{
    public partial class Frm_Production_Adjusted_Remark : Form
    {
        public DataTable _TableRemark;
        private int _TeamKey = 0;
        private DateTime _Date;
        private string _OrderKey = ""; //order nhóm gốc
        private int _Status = 0;
        private DataTable _Table;
        private int _StageKey = 0;
        private string _StageName = "";
        private double _Price = 0;
        private int _Rowdefault = 0; //số dòng gidview hiện lên đầu tiên
        private Order_Money_Info zOrder; //order nhóm vòng lặp

        #region[Properties]
        public int TeamKey
        {
            get
            {
                return _TeamKey;
            }

            set
            {
                _TeamKey = value;
            }
        }

        public DateTime Date
        {
            get
            {
                return _Date;
            }

            set
            {
                _Date = value;
            }
        }

        public string OrderKey
        {
            get
            {
                return _OrderKey;
            }

            set
            {
                _OrderKey = value;
            }
        }

        public int Status
        {
            get
            {
                return _Status;
            }

            set
            {
                _Status = value;
            }
        }

        public DataTable Table
        {
            get
            {
                return _Table;
            }

            set
            {
                _Table = value;
            }
        }

        public int StageKey
        {
            get
            {
                return _StageKey;
            }

            set
            {
                _StageKey = value;
            }
        }

        public string StageName
        {
            get
            {
                return _StageName;
            }

            set
            {
                _StageName = value;
            }
        }

        public double Price
        {
            get
            {
                return _Price;
            }

            set
            {
                _Price = value;
            }
        }
        #endregion

        public Frm_Production_Adjusted_Remark()
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            Utils.DoubleBuffered(GV_Default, true);
            Utils.DrawGVStyle(ref GV_Default);
            Utils.DrawLVStyle(ref LV_Order);

            LV_Team(LV_Order);
            GV_Default_Layout();
            GV_Default.CellEndEdit += GV_Default_CellEndEdit;
            GV_Default.KeyDown += GV_Default_KeyDown;

            Utils.SizeLastColumn_LV(LV_Order);
        }

        private void Btn_Save_Click(object sender, EventArgs e)
        {
            string Mees = Apply();
            if (Mees != string.Empty)
                MessageBox.Show(Mees);
            else
            {
                MessageBox.Show("Đã áp dụng xong !.");
                this.Close();
            }
        }

        private void Frm_Production_Adjusted_Remark_Load(object sender, EventArgs e)
        {
            LV_LoadData();
            GV_Default_LoadData();
            txt_Stage.Tag = _StageKey;
            txt_Stage.Text = _StageName;
            txt_Price.Text = _Price.ToString();

        }
        private void LV_Team(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 50;
            colHead.TextAlign = HorizontalAlignment.Center;

            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Đơn Hàng";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tình Trạng";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);
        }
        private void LV_LoadData()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LV_Order;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            DataTable In_Table = new DataTable();
            DateTime _FromDate = new DateTime(Date.Year, Date.Month, Date.Day, 0, 0, 0);
            DateTime _ToDate = new DateTime(Date.Year, Date.Month, Date.Day, 23, 59, 59);
            In_Table = HoTroSanXuat.DonHangThucHienChiaLai(_TeamKey, _FromDate, _ToDate); //Order_Data.List_General(_TeamKey, _Date);

            LV.Items.Clear();
            int n = In_Table.Rows.Count;
            int m = 0;
            for (int i = 0; i < n; i++)
            {

                DataRow nRow = In_Table.Rows[i];
                if (nRow["Status"].ToString().Trim() == "")
                {
                    lvi = new ListViewItem();
                    lvi.Text = (m + 1).ToString();
                    lvi.ForeColor = Color.Navy;
                    lvi.Tag = nRow["OrderKey"].ToString();
                    lvi.BackColor = Color.White;

                    lvi.ImageIndex = 0;

                    lvsi = new ListViewItem.ListViewSubItem();
                    lvsi.Text = nRow["OrderID"].ToString().Trim();
                    lvi.SubItems.Add(lvsi);

                    if (nRow["Status"].ToString().Trim() == "")
                    {
                        lvsi = new ListViewItem.ListViewSubItem();
                        lvsi.Text = "Chưa tính";
                        lvi.SubItems.Add(lvsi);
                    }

                    //if (nRow["Status"].ToString().Trim() == "1")
                    //{
                    //    lvsi = new ListViewItem.ListViewSubItem();
                    //    lvsi.Text = "Đã tính";
                    //    lvi.SubItems.Add(lvsi);
                    //}

                    lvsi = new ListViewItem.ListViewSubItem();
                    lvsi.Text = nRow["StageKey"].ToString().Trim();
                    lvi.SubItems.Add(lvsi);

                    lvsi = new ListViewItem.ListViewSubItem();
                    lvsi.Text = nRow["StageName"].ToString().Trim();
                    lvi.SubItems.Add(lvsi);

                    double zPrice = double.Parse(nRow["Price"].ToString().Trim());
                    lvsi = new ListViewItem.ListViewSubItem();
                    lvsi.Text = zPrice.ToString();
                    lvi.SubItems.Add(lvsi);

                    LV.Items.Add(lvi);
                    m++;
                }
            }

            this.Cursor = Cursors.Default;
        }

        //--------------GV_Default--------------
        private void GV_Default_Layout()
        {
            // Setup Column 
            GV_Default.Columns.Add("No", "STT");
            GV_Default.Columns.Add("EmployeeID", "Mã NV");
            GV_Default.Columns.Add("EmployeeName", "Tên Nhân Viên");
            GV_Default.Columns.Add("Time", "Thời Gian");

            GV_Default.Columns["No"].Width = 40;
            GV_Default.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV_Default.Columns["EmployeeID"].Width = 100;
            GV_Default.Columns["EmployeeID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV_Default.Columns["EmployeeName"].Width = 180;
            GV_Default.Columns["EmployeeName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV_Default.Columns["EmployeeName"].Frozen = true;

            GV_Default.Columns["Time"].Width = 100;
            GV_Default.Columns["Time"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        }
        private void GV_Default_LoadData()
        {
            DateTime _FromDate = new DateTime(Date.Year, Date.Month, Date.Day, 0, 0, 0);
            DateTime _ToDate = new DateTime(Date.Year, Date.Month, Date.Day, 23, 59, 59);

            int i = 0;
            GV_Default.Rows.Clear();
            _Table = HoTroSanXuat.CongNhanChuaChiLai(_OrderKey, _TeamKey, _FromDate, _ToDate); //Caculater_Productivity_Data.ListEmployeeGeneral(_OrderKey, _TeamKey, _Date, 0);
            _Rowdefault = _Table.Rows.Count;
            foreach (DataRow nRow in _Table.Rows)
            {
                GV_Default.Rows.Add();
                DataGridViewRow nRowView = GV_Default.Rows[i];
                nRowView.Cells["No"].Value = (i + 1).ToString();
                nRowView.Cells["EmployeeID"].Tag = nRow["EmployeeKey"].ToString().Trim();
                nRowView.Cells["EmployeeID"].Value = nRow["EmployeeID"].ToString().Trim();
                nRowView.Cells["EmployeeName"].Value = nRow["EmployeeName"].ToString().Trim();
                float zTime = 0;
                if (nRow["Time"].ToString().Trim() != "")
                {
                    zTime = nRow["Time"].ToFloat();
                    nRowView.Cells["Time"].Value = zTime.ToString("n1");
                }
                else
                {
                    zTime = 0;
                    nRowView.Cells["Time"].Value = zTime.ToString("n1");
                }
                nRowView.Cells["Time"].Tag = 1;
                i++;
            }
        }
        private void GV_Default_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            int i = 0;
            Order_Employee_Info zOrder_Employee;
            if (GV_Default.Rows[e.RowIndex].Tag == null)
            {
                zOrder_Employee = new Order_Employee_Info();
                GV_Default.Rows[e.RowIndex].Tag = zOrder_Employee;
            }
            else
            {
                zOrder_Employee = (Order_Employee_Info)GV_Default.Rows[e.RowIndex].Tag;
            }
            if (GV_Default.CurrentCell.ColumnIndex == 1)
            {
                if (GV_Default.CurrentCell.Value != null)
                {
                    string EmployeeID = GV_Default.CurrentCell.Value.ToString();
                    Employee_Info zEm = new Employee_Info();
                    zEm.GetEmployee(EmployeeID);
                    if (zOrder_Employee.Key == 0)
                    {
                        if (zEm.Key == "")
                        {
                            MessageBox.Show("Không có tên nhân viên này", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            GV_Default.CurrentRow.Cells[1].Value = "";
                            GV_Default.CurrentRow.Cells[2].Value = "";
                            //GV_Default.CurrentRow.Cells[6].Value = "";
                        }
                        else
                        {
                            int nRowCount = Employee_Data.Count_CardID(EmployeeID, _TeamKey);
                            if (nRowCount > 0)
                            {
                                for (int y = 0; y < GV_Default.Rows.Count - 2; y++)
                                {
                                    if (EmployeeID == GV_Default.Rows[y].Cells["EmployeeID"].Value.ToString())
                                    {
                                        i++;
                                    }
                                }
                                if (i > 0)
                                {
                                    MessageBox.Show("Đã có trong danh sách", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    GV_Default.CurrentRow.Cells[1].Value = "";
                                    GV_Default.CurrentRow.Cells[2].Value = "";
                                    //GV_Default.CurrentRow.Cells[6].Value = "";
                                }
                                if (i == 0)
                                {
                                    nRowCount = Employee_Data.Count(_TeamKey, EmployeeID, _OrderKey);
                                    if (nRowCount > 0)
                                    {
                                        MessageBox.Show("Nhân viên này đã có rồi!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    }
                                    else
                                    {
                                        nRowCount = Employee_Data.Count_CardID(EmployeeID, _TeamKey);
                                        if (nRowCount > 0)
                                        {
                                            ShowDefaultInGridView(e.RowIndex);
                                            GV_Default.CurrentRow.Cells[1].Tag = zEm.Key;
                                            GV_Default.CurrentRow.Cells[2].Value = zEm.FullName;
                                        }
                                        else
                                        {
                                            ShowDefaultInGridView(e.RowIndex);
                                            GV_Default.CurrentRow.Cells[1].Tag = zEm.Key;
                                            GV_Default.CurrentRow.Cells[2].Value = zEm.FullName;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                ShowDefaultInGridView(e.RowIndex);
                                GV_Default.CurrentRow.Cells[1].Tag = zEm.Key;
                                GV_Default.CurrentRow.Cells[2].Value = zEm.FullName;
                            }
                        }
                        zOrder_Employee.RecordStatus = 1;
                        GV_Default.CurrentRow.Cells[3].Tag = 1;

                    }
                }
            }
        }
        private void ShowDefaultInGridView(int RowIndex)
        {
            DataGridViewRow zRowView = GV_Default.Rows[RowIndex];
            zRowView.Cells["No"].Value = (RowIndex + 1).ToString();
        }
        private void GV_Default_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                DialogResult dlr = MessageBox.Show("Bạn có xóa thông tin này ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlr == DialogResult.Yes)
                {
                    for (int i = 0; i < GV_Default.Rows.Count; i++)
                    {
                        if (GV_Default.Rows[i].Selected)
                        {
                            if (GV_Default.Rows[i].Cells["EmployeeID"].Tag != null)
                            {
                                GV_Default.Rows[i].DefaultCellStyle.BackColor = Color.Red; ;
                                GV_Default.Rows[i].Cells["Time"].Tag = 99;
                                GV_Default.Rows[i].ReadOnly = true;
                            }
                        }
                    }
                }
            }

        }
        string Apply()
        {
            int zcheck = 0;
            for (int i = 0; i < LV_Order.Items.Count; i++)
            {
                if (LV_Order.Items[i].Checked == true)
                {
                    zcheck++;

                }
            }
            if (zcheck > 0)
            {
                _TableRemark = new DataTable();
                _TableRemark.Columns.Add("OrderKey");
                _TableRemark.Columns.Add("TeamKey");
                _TableRemark.Columns.Add("StageKey");
                _TableRemark.Columns.Add("EmployeeKey");
                _TableRemark.Columns.Add("EmployeeID");
                _TableRemark.Columns.Add("EmployeeName");
                _TableRemark.Columns.Add("Eat");
                _TableRemark.Columns.Add("Time");
                _TableRemark.Columns.Add("KgEat");
                _TableRemark.Columns.Add("MoneyEat");
                _TableRemark.Columns.Add("Edit");

                for (int i = 0; i < LV_Order.Items.Count; i++)
                {
                    if (LV_Order.Items[i].Checked == true)
                    {
                        for (int j = 0; j < GV_Default.Rows.Count - 1; j++)
                        {
                            DataRow r = _TableRemark.NewRow();
                            if (GV_Default.Rows[j].Cells["EmployeeID"].Value.ToString() != "")
                            {
                                r["OrderKey"] = LV_Order.Items[i].Tag.ToString();
                                r["TeamKey"] = _TeamKey;
                                r["StageKey"] = txt_Stage.Tag.ToString();
                                r["EmployeeKey"] = GV_Default.Rows[j].Cells["EmployeeID"].Tag.ToString();
                                r["EmployeeID"] = GV_Default.Rows[j].Cells["EmployeeID"].Value.ToString();
                                r["EmployeeName"] = GV_Default.Rows[j].Cells["EmployeeName"].Value.ToString();
                                if (GV_Default.Rows[j].Cells["Time"].Value == null)
                                    return "Vui lòng kiểm tra lại thời gian !.";
                                r["Time"] = GV_Default.Rows[j].Cells["Time"].Value.ToString();
                                r["Eat"] = 1;
                                if (GV_Default.Rows[j].Cells["Time"].Tag.ToString() == "99")
                                {
                                    r["Edit"] = 99;//đã xóa
                                }
                                else
                                {
                                    if (j >= _Rowdefault)
                                        r["Edit"] = 0;//tạo mới
                                    else
                                        r["Edit"] = GV_Default.Rows[j].Cells["Time"].Tag;//chỉnh sửa
                                }
                                _TableRemark.Rows.Add(r);
                            }
                        }

                    }

                }
            }
            else
            {
                return "Bạn phải chọn đơn hàng !.";
            }

            if (_TableRemark.Rows.Count > 0)
            {
                foreach (DataRow r in _TableRemark.Rows)
                {
                    if (r["Time"].ToString() == string.Empty)
                        return "Vui lòng kiểm tra lại thời gian !.";
                }
            }

            return string.Empty;
        }

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}
