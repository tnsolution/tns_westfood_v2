﻿using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using TNS.CORE;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;
//Nếu có tổng số nguyên liệu ưu tiên nguyên liệu
//    Ngược lại ưu tiên số rổ nếu có tổng số rổ
//        Ngược lại lấy số giờ
namespace TNS.WinApp
{
    public partial class Frm_Production_Calculator_V3 : Form
    {
        #region[Khai báo hằng số]



        //float _OMoney = HoTroSanXuat.LayTienNgoaiGio(SessionUser.Date_Work);        //tiền dư giờ
        const int _TeamSCCD = 88;
        const int _TeamSCDH = 89;
        int _DayOfWeek = 0;
        #endregion

        private string _TeamName = "";
        private int _TeamKey = 0;
        private string _WorkName = "";
        private int _WorkKey = 0;
        private string _OrderID = "";
        private string _OrderKey = "";

        DateTime _FromDate = new DateTime();
        DateTime _ToDate = new DateTime();
        private DataTable _BangSoSanh;

        public Frm_Production_Calculator_V3()
        {
            InitializeComponent();

            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            btn_Search.Click += Btn_Search_Click;
            InitLayout_GVTeam(GVTeam);
            InitLayout_GVWork(GVWork);
            InitLayout_GVOrder(GVOrder);
            InitLayout_GVRate(GVRate);
            InitLayout_GVData(GVEmployee);

        }



        private void Frm_Production_Calculator_V3_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);

            this.Bounds = Screen.PrimaryScreen.WorkingArea;

            this.DoubleBuffered = true;
            Utils.DoubleBuffered(GVEmployee, true);
            Utils.DoubleBuffered(GVTeam, true);
            Utils.DoubleBuffered(GVWork, true);
            Utils.DoubleBuffered(GVRate, true);
            Utils.DoubleBuffered(GVOrder, true);

            Utils.DrawGVStyle(ref GVEmployee);
            Utils.DrawGVStyle(ref GVTeam);
            Utils.DrawGVStyle(ref GVWork);
            Utils.DrawGVStyle(ref GVRate);
            Utils.DrawGVStyle(ref GVOrder);

            Panel_Right.Visible = true;
            btnHideInfo.Visible = true;
            btnShowInfo.Visible = false;

            InitData_GVTeam(GVTeam, Team_Data.List_Team());
            InitData_GVOrder(GVOrder, null);

            GVTeam.SelectionChanged += GVTeam_Click;
            GVWork.SelectionChanged += GVWork_Click;
            GVOrder.SelectionChanged += GVOrder_Click;

            btn_Count.Click += Btn_Count_Click;
            btn_Save.Click += Btn_Save_Click;

            #region [Datetime Process]
            DateTime DateWork = SessionUser.Date_Work;
            dte_Date.Value = DateWork;
            dte_Date.Validated += (o, s) =>
            {
                DateWork = dte_Date.Value;

                _FromDate = new DateTime(DateWork.Year, DateWork.Month, DateWork.Day, 0, 0, 0);
                _ToDate = new DateTime(DateWork.Year, DateWork.Month, DateWork.Day, 23, 59, 59);


            };
            _FromDate = new DateTime(DateWork.Year, DateWork.Month, DateWork.Day, 0, 0, 0);
            _ToDate = new DateTime(DateWork.Year, DateWork.Month, DateWork.Day, 23, 59, 59);
            #endregion
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            InitData_GVTeam(GVTeam, Team_Data.List_Team());
            InitData_GVWork(GVWork, null);
            InitData_GVOrder(GVOrder, null);
            InitData_GVData(GVEmployee, null);
            InitData_GVRate(GVRate, null);

        }
        private void GVTeam_Click(object sender, EventArgs e)
        {
            if (GVTeam.SelectedRows.Count > 0 &&
                GVTeam.CurrentRow.Cells["TeamName"].Tag != null)
            {
                //Clear các bảng con khi chọn lại
                _WorkKey = 0;
                _OrderKey = "";
                _TeamKey = int.Parse(GVTeam.CurrentRow.Cells["TeamName"].Tag.ToString());
                _TeamName = GVTeam.CurrentRow.Cells["TeamName"].Value.ToString();

                txt_title.Text = _TeamName;
                DateTime zFromDate = new DateTime(_FromDate.Year, _FromDate.Month, _FromDate.Day, 0, 0, 0);
                DateTime zToDate = new DateTime(_ToDate.Year, _ToDate.Month, _ToDate.Day, 23, 59, 59);
                _DayOfWeek = HoTroSanXuat.LayHeSoChuNhat(dte_Date.Value);    //hệ số chủ nhật

                DataTable zTableWork = HoTroSanXuat.CongViecNhom(_TeamKey, zFromDate, zToDate);
                InitData_GVWork(GVWork, zTableWork);
                InitData_GVOrder(GVOrder, null);
                InitData_GVData(GVEmployee, null);
            }
        }
        private void GVWork_Click(object sender, EventArgs e)
        {
            if (GVWork.SelectedRows.Count > 0 &&
                GVWork.CurrentRow.Cells["StageName"].Tag != null)
            {
                //Clear các bảng con khi chọn lại
                _OrderKey = "";
                _WorkKey = int.Parse(GVWork.CurrentRow.Cells["StageName"].Tag.ToString());
                _WorkName = GVWork.CurrentRow.Cells["StageName"].Value.ToString();

                txt_title.Text = _TeamName + " > " + _WorkName;
                DataTable zTableOrder = HoTroSanXuat.DonHangThucHienV3(_TeamKey, _WorkKey, _FromDate, _ToDate);
                InitData_GVOrder(GVOrder, zTableOrder);
                InitData_GVData(GVEmployee, null);

                //--------auto select 
                if (GVOrder.Rows.Count > 0)
                {
                    LoadInfo_Order(GVOrder.Rows[0]);
                }
            }
        }
        private void GVOrder_Click(object sender, EventArgs e)
        {
            if (GVOrder.SelectedRows.Count > 0 &&
                GVOrder.CurrentRow.Cells["OrderID"].Tag != null)
            {
                Cursor = Cursors.WaitCursor;
                LoadInfo_Order(GVOrder.CurrentRow);
                Cursor = Cursors.Default;
            }
        }

        //Load chi tiết đơn hàng
        private void LoadInfo_Order(DataGridViewRow nRow)
        {
            string Name = GVOrder.CurrentRow.Cells["OrderID"].Value.ToString();
            txt_title.Text = _TeamName + " > " + _WorkName + " > " + Name;

            _OrderKey = nRow.Cells["OrderID"].Tag.ToString();
            DateTime zOrderDate = Convert.ToDateTime(nRow.Cells["OrderDate"].Value.ToString());
            string zOrderID = nRow.Cells["OrderID"].Value.ToString();

            float zQuantity = 0;
            if (float.TryParse(nRow.Cells["Quantity"].Value.ToString(), out zQuantity))
            {

            }
            float zPrice = 0;
            if (float.TryParse(nRow.Cells["Price"].Value.ToString(), out zPrice))
            {

            }
            float zTotalBasket = 0;
            if (float.TryParse(nRow.Cells["TotalBasket"].Value.ToString(), out zTotalBasket))
            {

            }
            float zTotalKg = 0;
            if (float.TryParse(nRow.Cells["TotalKg"].Value.ToString(), out zTotalKg))
            {

            }
            float zTotalTime = 0;
            if (float.TryParse(nRow.Cells["TotalTime"].Value.ToString(), out zTotalTime))
            {

            }
            float zRateOrder = 0;
            if (float.TryParse(nRow.Cells["RateOrder"].Value.ToString(), out zRateOrder))
            {

            }
            float zRateDay = 0;
            if (float.TryParse(nRow.Cells["RateDay"].Value.ToString(), out zRateDay))
            {

            }
            float zPercent = 0;
            if (float.TryParse(nRow.Cells["PercentOrder"].Value.ToString(), out zPercent))
            {

            }
            float zGioNam = 0;
            if (float.TryParse(nRow.Cells["GioNam"].Value.ToString(), out zGioNam))
            {
                // dùng nếu không có đơn giá
            }
            float zGioNu = 0;
            if (float.TryParse(nRow.Cells["GioNu"].Value.ToString(), out zGioNu))
            {
                // dùng nếu không có đơn giá
            }

            txt_Price.Text = zPrice.Ton2String();
            txt_Quantity.Text = zQuantity.Ton2String();
            txt_Basket.Text = zTotalBasket.Ton2String();
            txt_Kilogram.Text = zTotalKg.Ton2String();
            txt_Time.Text = zTotalTime.Ton2String();
            txt_Percent.Text = zPercent.ToString();
            txt_TotalNam.Text = zGioNam.Ton2String();
            txt_TotalNu.Text = zGioNu.Ton2String();
            txt_Total.Text = HamTinhTienTongDongHang(zOrderDate, zQuantity, zPrice,
            zRateOrder, zRateDay, zPercent, zGioNam, zGioNu).Ton0String();
            try
            {
                using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }
            }
            catch (Exception ex)
            {
                Utils.TNMessageBoxOK(ex.ToString(), 4);
            }
        }
        private void DisplayData()
        {
            DataTable zTableRate = Order_Rate_Data.List(_OrderKey);
            DataTable zTableEmployee = HoTroSanXuat.CacNhanVienThucHien(_OrderKey);
            this.Invoke(new MethodInvoker(delegate ()
            {
                InitData_GVRate(GVRate, zTableRate);
                InitData_GVData(GVEmployee, zTableEmployee);
                InitData_GVSum(GVEmployee);
            }));
        }

        #region[HÀM LAYOUT SƯ DUNG CHUNG]
        //Lay out Listview List nhóm
        private void InitLayout_GVTeam(DataGridView GV)
        {
            // Setup Column 
            GV.Columns.Add("No", "STT");
            GV.Columns.Add("TeamID", "Mã");
            GV.Columns.Add("TeamName", "Tên nhóm");

            GV.Columns["No"].Width = 40;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns["TeamID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["TeamName"].Width = 200;
            GV.Columns["TeamName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["TeamName"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }
        private void InitData_GVTeam(DataGridView GV, DataTable Table)
        {
            GV.Rows.Clear();
            if (Table != null)
            {
                for (int i = 0; i < Table.Rows.Count; i++)
                {
                    DataRow r = Table.Rows[i];
                    GV.Rows.Add();
                    DataGridViewRow GvRow = GV.Rows[i];
                    GvRow.Cells["No"].Value = (i + 1).ToString();
                    GvRow.Cells["TeamName"].Tag = r["TeamKey"].ToString();
                    GvRow.Cells["TeamName"].Value = r["TeamName"].ToString();
                    GvRow.Cells["TeamID"].Value = r["TeamID"].ToString();
                }
            }
            GV.ClearSelection();
        }
        //Lay out Listview List công việc
        private void InitLayout_GVWork(DataGridView GV)
        {
            // Setup Column 
            GV.Columns.Add("No", "STT");
            GV.Columns.Add("StageID", "Mã");
            GV.Columns.Add("StageName", "Tên công việc");
            GV.Columns.Add("StagePrice", "Đơn giá");

            GV.Columns["No"].Width = 40;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns["StageID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["StageName"].Width = 200;
            GV.Columns["StageName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["StageName"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.Columns["StagePrice"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV.Columns["StagePrice"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }
        private void InitData_GVWork(DataGridView GV, DataTable Table)
        {
            GV.Rows.Clear();
            if (Table != null)
            {
                for (int i = 0; i < Table.Rows.Count; i++)
                {
                    DataRow r = Table.Rows[i];
                    GV.Rows.Add();
                    DataGridViewRow GvRow = GV.Rows[i];
                    GvRow.Cells["No"].Value = (i + 1).ToString();
                    GvRow.Cells["StageName"].Tag = r["StageKey"].ToString();
                    GvRow.Cells["StageName"].Value = r["StageName"].ToString();
                    GvRow.Cells["StagePrice"].Value = r["StagePrice"].Ton1String();
                    GvRow.Cells["StageID"].Value = r["StageID"].ToString();
                }
            }
            GV.ClearSelection();
        }
        //Lay out Listview List Đơn hang
        private void InitLayout_GVOrder(DataGridView GV)
        {
            GV.Columns.Clear();
            // Setup Column 
            GV.Columns.Add("No", "STT");
            GV.Columns.Add("OrderIDCompare", "Mã tham chiếu");
            GV.Columns.Add("OrderID", "Mã đơn hàng");
            GV.Columns.Add("WorkStatus", "Tính trạng");
            GV.Columns.Add("OrderDate", "Ngày đơn hàng");
            GV.Columns.Add("Quantity", "Số lượng");
            GV.Columns.Add("Price", "Đơn giá");
            GV.Columns.Add("TotalKg", "Tổng kí");
            GV.Columns.Add("TotalBasket", "Tổng rổ");
            GV.Columns.Add("TotalTime", "Tổng giờ");
            GV.Columns.Add("RateOrder", "Tổng hệ số");
            GV.Columns.Add("RateDay", "Hệ số lễ");
            GV.Columns.Add("PercentOrder", "Tỷ lệ hoàn thành");
            GV.Columns.Add("GioNam", "Giờ Nam");
            GV.Columns.Add("GioNu", "Giờ Nu");

            GV.Columns["No"].Width = 40;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns["OrderIDCompare"].Width = 150;
            GV.Columns["OrderIDCompare"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["OrderID"].Width = 150;
            GV.Columns["OrderID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["WorkStatus"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["WorkStatus"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }
        private void InitData_GVOrder(DataGridView GV, DataTable Table)
        {
            GV.Rows.Clear();
            if (Table != null)
            {
                for (int i = 0; i < Table.Rows.Count; i++)
                {
                    DataRow r = Table.Rows[i];
                    GV.Rows.Add();
                    DataGridViewRow GvRow = GV.Rows[i];
                    GvRow.Cells["No"].Value = (i + 1).ToString();
                    GvRow.Cells["OrderID"].Tag = r["OrderKey"].ToString();
                    GvRow.Cells["OrderID"].Value = r["OrderID"].ToString();
                    GvRow.Cells["OrderIDCompare"].Value = r["OrderID_Compare"].ToString();
                    if (r["WorkStatus"].ToInt() == 1)
                    {
                        GvRow.Cells["WorkStatus"].Value = "Đã làm xong";
                    }
                    else if (r["WorkStatus"].ToInt() == 0)
                    {
                        GvRow.Cells["WorkStatus"].Value = "Đang thực hiện";
                    }
                    else
                    {
                        GvRow.Cells["WorkStatus"].Value = "Lỗi";
                    }
                    GvRow.Cells["OrderDate"].Value = r["OrderDate"].ToString();
                    GvRow.Cells["Quantity"].Value = r["Quantity"].ToString();
                    GvRow.Cells["Price"].Value = r["Price"].ToString();
                    GvRow.Cells["TotalBasket"].Value = r["TotalBasket"].ToString();
                    GvRow.Cells["TotalKg"].Value = r["TotalKg"].ToString();
                    GvRow.Cells["TotalTime"].Value = r["TotalTime"].ToString();
                    GvRow.Cells["RateOrder"].Value = r["RateOrder"].ToString();
                    GvRow.Cells["RateDay"].Value = r["RateDay"].ToString();
                    GvRow.Cells["PercentOrder"].Value = r["PercentOrder"].ToString();
                    GvRow.Cells["GioNam"].Value = r["GioNam"].ToString();
                    GvRow.Cells["GioNu"].Value = r["GioNu"].ToString();
                }
            }
            GV.Columns["OrderDate"].Visible = false;
            GV.Columns["Quantity"].Visible = false;
            GV.Columns["Price"].Visible = false;
            GV.Columns["TotalBasket"].Visible = false;
            GV.Columns["TotalKg"].Visible = false;
            GV.Columns["TotalTime"].Visible = false;
            GV.Columns["RateOrder"].Visible = false;
            GV.Columns["RateDay"].Visible = false;
            GV.Columns["PercentOrder"].Visible = false;
            GV.Columns["GioNam"].Visible = false;
            GV.Columns["GioNu"].Visible = false;
            GV.ClearSelection();
        }
        //Layout Gidview chi tiết hệ số
        private void InitLayout_GVRate(DataGridView GV)
        {
            // Setup Column 
            GV.Columns.Add("No", "#");
            GV.Columns.Add("CategoryName", "Tên Hệ Số");
            GV.Columns.Add("Rate", "Hệ Số");

            GV.Columns["No"].Width = 40;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV.Columns["CategoryName"].Width = 100;
            GV.Columns["CategoryName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["CategoryName"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GV.Columns["Rate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV.Columns["Rate"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }
        private void InitData_GVRate(DataGridView GV, DataTable Table)
        {
            GV.Rows.Clear();
            if (Table != null)
            {
                for (int i = 0; i < Table.Rows.Count; i++)
                {
                    DataRow r = Table.Rows[i];
                    string RateName = r["RateName"].ToString();
                    float RateValue = 0;
                    if (float.TryParse(r["Rate"].ToString(), out RateValue))
                    {

                    }

                    GV.Rows.Add();
                    DataGridViewRow GvRow = GV.Rows[i];
                    GvRow.Cells["No"].Value = (i + 1).ToString();
                    GvRow.Cells["CategoryName"].Value = RateName;
                    GvRow.Cells["Rate"].Value = RateValue.Ton2String();
                    //GvRow.Cells["Rate"].Value = RateValue.ToString("0.00");
                }
            }
            GV.ClearSelection();
        }
        //Layout Gidview chi tiết nhân viên
        private void InitLayout_GVData(DataGridView GV)
        {
            DataGridViewCheckBoxColumn zColumn;
            // Setup Column 
            GV.Columns.Add("No", "STT");
            GV.Columns.Add("EmployeeID", "Mã NV");
            GV.Columns.Add("EmployeeName", "Tên Nhân Viên");
            GV.Columns.Add("Kilogram", "SL nguyên Liệu");
            GV.Columns.Add("Basket", "Số Rổ");
            GV.Columns.Add("Time", "Thời Gian");
            GV.Columns.Add("KgProduct", "SL thành Phẩm");
            GV.Columns.Add("Money", "Tiền");

            zColumn = new DataGridViewCheckBoxColumn();
            zColumn.Name = "Borrow";
            zColumn.HeaderText = "Mượn Người";
            GV.Columns.Add(zColumn);

            zColumn = new DataGridViewCheckBoxColumn();
            zColumn.Name = "Private";
            zColumn.HeaderText = "Ăn Riêng";
            GV.Columns.Add(zColumn);

            zColumn = new DataGridViewCheckBoxColumn();
            zColumn.Name = "Share";
            zColumn.HeaderText = "Ăn Chung";
            GV.Columns.Add(zColumn);

            GV.Columns.Add("Gender", "Nam/Nữ");
            GV.Columns.Add("Message", "Thông Báo");

            GV.Columns["No"].Width = 40;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns["EmployeeID"].Width = 70;
            GV.Columns["EmployeeID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["EmployeeID"].ReadOnly = true;

            GV.Columns["EmployeeName"].Width = 170;
            GV.Columns["EmployeeName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["EmployeeName"].Frozen = true;
            GV.Columns["EmployeeName"].ReadOnly = true;

            GV.Columns["Basket"].Width = 80;
            GV.Columns["Basket"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV.Columns["Basket"].ReadOnly = true;

            GV.Columns["Kilogram"].Width = 120;
            GV.Columns["Kilogram"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV.Columns["Kilogram"].ReadOnly = true;

            GV.Columns["KgProduct"].Width = 120;
            GV.Columns["KgProduct"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV.Columns["KgProduct"].ReadOnly = true;


            GV.Columns["Time"].Width = 80;
            GV.Columns["Time"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV.Columns["Time"].ReadOnly = true;

            GV.Columns["Money"].Width = 120;
            GV.Columns["Money"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV.Columns["Money"].ReadOnly = true;

            GV.Columns["Borrow"].Width = 90;
            GV.Columns["Borrow"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV.Columns["Borrow"].ReadOnly = true;

            GV.Columns["Private"].Width = 80;
            GV.Columns["Private"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV.Columns["Private"].ReadOnly = true;

            GV.Columns["Share"].Width = 80;
            GV.Columns["Share"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV.Columns["Share"].ReadOnly = true;

            GV.Columns["Gender"].Visible = false;
            GV.Columns["Gender"].Width = 0;
            GV.Columns["Gender"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV.Columns["Gender"].ReadOnly = true;

            GV.Columns["Message"].Width = 120;
            GV.Columns["Message"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV.Columns["Message"].ReadOnly = true;

        }
        private void InitData_GVData(DataGridView GV, DataTable Table)
        {
            GV.Rows.Clear();
            if (Table != null)
            {
                for (int i = 0; i < Table.Rows.Count; i++)
                {
                    GV.Rows.Add();
                    DataGridViewRow nRowView = GV.Rows[i];

                    DataRow r = Table.Rows[i];
                    nRowView.Cells["No"].Value = (i + 1).ToString();
                    nRowView.Cells["EmployeeID"].Tag = r["EmployeeKey"].ToString().Trim();
                    nRowView.Cells["EmployeeID"].Value = r["EmployeeID"].ToString().Trim();
                    nRowView.Cells["EmployeeName"].Value = r["EmployeeName"].ToString().Trim();
                    nRowView.Cells["Basket"].Value = r["Basket"].Ton2String();
                    nRowView.Cells["Kilogram"].Value = r["Kilogram"].Ton2String();
                    float zKgProduct = 0;
                    if (float.TryParse(r["KgProduct"].ToString(), out zKgProduct))
                    {

                    }
                    nRowView.Cells["KgProduct"].Value = zKgProduct.ToString("n4");
                    nRowView.Cells["Time"].Value = r["Time"].Ton2String();
                    nRowView.Cells["Money"].Value = r["Money"].Ton0String();
                    if (r["Borrow"].ToInt() == 1)
                    {
                        nRowView.DefaultCellStyle.BackColor = Color.LightBlue;
                        nRowView.Cells["Borrow"].Value = true;
                    }
                    if (r["Private"].ToInt() == 1)
                    {
                        nRowView.Cells["Private"].Value = true;
                    }
                    if (r["Share"].ToInt() == 1)
                    {
                        nRowView.Cells["Share"].Value = true;
                    }
                    nRowView.Cells["Gender"].Value = r["Gender"].ToString();
                }

                GV.Rows.Add();
            }
        }
        private void InitData_GVSum(DataGridView GV)
        {
            float totalBasket = 0;
            float totalKilogram = 0;
            float totaltime = 0;
            float totalkgProduct = 0;
            float totalmoney = 0;

            int no = GV.Rows.Count - 1;
            GV.Rows[no].Cells["No"].Value = "";
            GV.Rows[no].Cells["EmployeeID"].Value = "";
            GV.Rows[no].Cells["EmployeeName"].Value = "TỔNG";

            for (int k = 0; k < GV.Rows.Count - 1; k++)
            {
                float zBasket = 0;
                float zKilogram = 0;
                float ztime = 0;
                float zkgProduct = 0;
                float zmoney = 0;

                if (float.TryParse(GV.Rows[k].Cells["Basket"].Value.ToString(), out zBasket))
                {

                }
                totalBasket += zBasket;
                if (float.TryParse(GV.Rows[k].Cells["Kilogram"].Value.ToString(), out zKilogram))
                {

                }
                totalKilogram += zKilogram;
                if (float.TryParse(GV.Rows[k].Cells["Time"].Value.ToString(), out ztime))
                {

                }
                totaltime += ztime;
                if (float.TryParse(GV.Rows[k].Cells["KgProduct"].Value.ToString(), out zkgProduct))
                {

                }
                totalkgProduct += zkgProduct;
                if (float.TryParse(GV.Rows[k].Cells["Money"].Value.ToString(), out zmoney))
                {

                }
                totalmoney += zmoney;
            }

            GV.Rows[no].Cells["Basket"].Value = totalBasket.Ton1String();
            GV.Rows[no].Cells["Kilogram"].Value = totalKilogram.Ton2String();
            GV.Rows[no].Cells["Time"].Value = totaltime.Ton2String();
            GV.Rows[no].Cells["KgProduct"].Value = totalkgProduct.ToString("n4");
            GV.Rows[no].Cells["Money"].Value = totalmoney.Ton0String();
            GV.Rows[no].DefaultCellStyle.Font = new Font("Tahoma", 9, FontStyle.Bold);
        }
        #endregion

        #region[Năng suất tại tổ]
        private void Calculator_Order(DataGridViewRow nRow) //tính toán số liệu đơn hàng, công nhân
        {
            string zOrderID = nRow.Cells["OrderID"].Value.ToString();
            DateTime zOrderDate = Convert.ToDateTime(nRow.Cells["OrderDate"].Value.ToString());
            float zQuantity = 0;
            if (float.TryParse(nRow.Cells["Quantity"].Value.ToString(), out zQuantity))
            {

            }
            float zPrice = 0;
            if (float.TryParse(nRow.Cells["Price"].Value.ToString(), out zPrice))
            {

            }
            float zTotalBasket = 0;
            if (float.TryParse(nRow.Cells["TotalBasket"].Value.ToString(), out zTotalBasket))
            {

            }
            float zTotalKg = 0;
            if (float.TryParse(nRow.Cells["TotalKg"].Value.ToString(), out zTotalKg))
            {

            }
            float zTotalTime = 0;
            if (float.TryParse(nRow.Cells["TotalTime"].Value.ToString(), out zTotalTime))
            {

            }
            float zRateOrder = 0;
            if (float.TryParse(nRow.Cells["RateOrder"].Value.ToString(), out zRateOrder))
            {

            }
            float zRateDay = 0;
            if (float.TryParse(nRow.Cells["RateDay"].Value.ToString(), out zRateDay))
            {

            }
            float zPercentFinish = 0;
            if (float.TryParse(nRow.Cells["PercentOrder"].Value.ToString(), out zPercentFinish))
            {

            }
            float zGioNam = 0;
            if (float.TryParse(nRow.Cells["GioNam"].Value.ToString(), out zGioNam))
            {

            }
            float zGioNu = 0;
            if (float.TryParse(nRow.Cells["GioNu"].Value.ToString(), out zGioNu))
            {

            }



            #region [---------   Chuẩn bị các tham số   -----------] 

            float zTempMoney = 0;   //code không rõ có sử dụng hay không
            float zTempBasket = 0;
            float zTempTime = 0;
            float zTempKg = 0;

            // muốn xem công thức cũ qua V2 xem
            #region Cách tính năng xuất ngày 02/10/2020 gom gọn tính thông số
            if (zTotalKg > 0)
            {
                //TH 1.1 Nếu Ngày hiện tại là ngày thường
                zTempMoney = (zQuantity / zTotalKg) * zPrice;
                zTempKg = zQuantity / zTotalKg;
                //TH 1.2 Nếu Ngày hiện tại là ngày lễ
                if (zRateDay > 0)
                {
                    zTempMoney *= zRateDay;
                }
                //TH 1.3 Nếu Ngày hiện tại là ngày chủ nhât
                if (zOrderDate.DayOfWeek == DayOfWeek.Sunday && zRateDay == 0)
                {
                    zTempMoney *= _DayOfWeek;
                }

            }
            else if (zTotalBasket > 0)
            {
                //TH 1.1:Nếu Ngày hiện tại là ngày thường 
                zTempMoney = (zQuantity / zTotalBasket) * zPrice;
                zTempBasket = zQuantity / zTotalBasket;
                //TH 1.2 Nếu Ngày hiện tại là ngày lễ
                if (zRateDay > 0)
                {
                    zTempMoney *= zRateDay;
                }
                //TH 1.3:Nếu Ngày hiện tại là ngày chủ nhật
                if (zOrderDate.DayOfWeek == DayOfWeek.Sunday && zRateDay == 0)
                {
                    zTempMoney *= _DayOfWeek;
                }
            }
            else if (zTotalTime > 0)
            {
                //TH 2.2:Nếu Ngày hiện tại là ngày thường
                zTempMoney = (zQuantity / zTotalTime) * zPrice;
                zTempTime = zQuantity / zTotalTime;
                //TH 2.1 Nếu Ngày hiện tại là ngày lễ
                if (zRateDay > 0)
                {
                    zTempMoney *= zRateDay;
                }
                //TH 2.2:Nếu Ngày hiện tại  là ngày chủ nhật
                if (zOrderDate.DayOfWeek == DayOfWeek.Sunday && zRateDay == 0)
                {
                    zTempMoney *= _DayOfWeek;
                }
            }
            #endregion

            #endregion

            KhoiTaoColumnBangSoSanh();

            for (int i = 0; i < GVEmployee.Rows.Count - 1; i++)
            {
                _BangSoSanh.Rows.Add();
                DataRow row = _BangSoSanh.Rows[i];

                DataGridViewRow r = GVEmployee.Rows[i];
                int Gender = r.Cells["Gender"].Value.ToInt(); //Nam nu
                float Time = 0;
                if (float.TryParse(r.Cells["Time"].Value.ToString(), out Time))
                {
                    //Số giờ 
                }
                float Kilogram = 0;
                if (float.TryParse(r.Cells["Kilogram"].Value.ToString(), out Kilogram))
                {
                    //Số kí 
                }
                float Basket = 0;
                if (float.TryParse(r.Cells["Basket"].Value.ToString(), out Basket))
                {
                    //Số rỗ 
                }
                float ResultKg = 0;     //ket quả số kg
                float ResultMoney = 0;// kết quả số tiền

                if (zPrice > 0)
                {
                    //TH 1.1 Số giờ >0 và số rổ=0 và số kí=0
                    if (Time > 0 && Basket == 0 && Kilogram == 0)
                    {
                        //TH 1.1.1  Ngày hiện tại đơn hàng  là ngày thường
                        ResultKg = 0;
                        if (float.TryParse((float.Parse(zTempTime.ToString("n4")) * Time).ToString(), out ResultKg))
                        {

                        }
                        ResultMoney = (float.Parse(ResultKg.ToString("n4")) * zPrice * zRateOrder);

                        //TH 1.1.2  Ngày hiện tại đơn hàng là ngày lễ
                        if (zRateDay > 0)
                        {
                            ResultMoney *= zRateDay;
                        }

                        //TH 1.1.3  Ngày hiện tại đơn hàng  là ngày chủ nhật
                        if (zOrderDate.DayOfWeek == DayOfWeek.Sunday && zRateDay == 0)
                        {

                            ResultMoney *= _DayOfWeek;
                        }
                    }
                    //TH 1.2 (Số giờ >0 và số rổ=0 ) hoặc (Số giờ ==0 và số kí>0)
                    if (Time > 0 && Basket > 0 || Time == 0 && Basket > 0)
                    {
                        //TH 1.2.1  Ngày hiện tại đơn hàng là ngày thường
                        ResultKg = 0;
                        if (float.TryParse((float.Parse(zTempBasket.ToString("n4")) * Basket).ToString(), out ResultKg))
                        {

                        }
                        ResultMoney = (float.Parse(ResultKg.ToString("n4")) * zPrice * zRateOrder);
                        //TH 1.2.2  Ngày hiện tại đơn hàng là ngày lễ
                        if (zRateDay > 0)
                        {

                            ResultMoney *= zRateDay;
                        }
                        //TH 1.2.3  Ngày hiện tại đơn hàng không là ngày lễ và là ngày chủ nhật
                        if (zOrderDate.DayOfWeek == DayOfWeek.Sunday && zRateDay == 0)
                        {
                            ResultMoney *= _DayOfWeek;
                        }
                    }
                    //TH 1.3 (Số giờ >0 và Số kí >0)
                    if (Time > 0 && Kilogram > 0)
                    {

                        //TH 1.3.1  Ngày hiện tại đơn hàng là ngày thường
                        if (float.TryParse((float.Parse(zTempKg.ToString("n4")) * Kilogram).ToString(), out ResultKg))
                        {

                        }
                        ResultMoney = (float.Parse(ResultKg.ToString("n4")) * zPrice * zRateOrder);
                        //TH 1.3.2  Ngày hiện tại đơn hàng là ngày lễ
                        if (zRateDay > 0)
                        {

                            ResultMoney *= zRateDay;
                        }
                        //TH 1.3.3  Ngày hiện tại đơn hàng không là ngày lễ và là ngày chủ nhật
                        if (zOrderDate.DayOfWeek == DayOfWeek.Sunday && zRateDay == 0)
                        {

                            ResultMoney *= _DayOfWeek;
                        }
                    }
                }
                else
                {
                    int _Hour = HoTroSanXuat.LaySoGioLamHanhChanh(dte_Date.Value);     //thời gian làm việc hành chính
                    float _MMoney = HoTroSanXuat.LayDonGiaNam(dte_Date.Value);      //tiền danh cho công nhân nam
                    float _FMoney = HoTroSanXuat.LayDonGiaNu(dte_Date.Value);       //tiền danh cho công nhân nữ
                    float MoneyTime = 0;
                    if (Gender == 0)
                    {
                        MoneyTime = (Time / _Hour) * _MMoney;
                    }
                    else
                    {
                        MoneyTime = (Time / _Hour) * _FMoney;
                    }
                    ResultMoney = MoneyTime;
                }
                ResultMoney = (ResultMoney * zPercentFinish) / 100;
                //Lưu bảng so sánh dữ liệu trước khi hiện ra
                row["EmployeeKey"] = r.Cells["EmployeeID"].Tag.ToString();
                row["EmployeeID"] = r.Cells["EmployeeID"].Value.ToString();
                row["EmployeeName"] = r.Cells["EmployeeName"].Value.ToString();
                row["Kilogram"] = r.Cells["Kilogram"].Value.ToString();
                row["Basket"] = r.Cells["Basket"].Value.ToString();
                row["Time"] = r.Cells["Time"].Value.ToString();
                row["KgProduct"] = float.Parse(ResultKg.ToString("n4"));
                row["Money"] = float.Parse(ResultMoney.ToString("n0"));
                if (r.Cells["Borrow"].Value == null || bool.Parse(r.Cells["Borrow"].Value.ToString()) == false)
                    row["Borrow"] = 0;
                else
                    row["Borrow"] = 1;
                if (r.Cells["Private"].Value == null || bool.Parse(r.Cells["Private"].Value.ToString()) == false)
                    row["Private"] = 0;
                else
                    row["Private"] = 1;

                if (r.Cells["Share"].Value == null || bool.Parse(r.Cells["Share"].Value.ToString()) == false)
                    row["Share"] = 0;
                else
                    row["Share"] = 1;
                row["Gender"] = r.Cells["Gender"].Value.ToString();
            }
            ProcessCount();


            #region[cu]
            //-----lọc dữ liệu có số tiền thấp nhất, lấy mã nhân viên có số tiền thấp nhất, nếu bằng nhau hết lấy người đầu tiên----
            //DataView dv = _BangSoSanh.DefaultView;
            //dv.RowFilter = "(KgProduct > '0') ";
            //dv.Sort = "KgProduct ASC";
            //DataTable zTemp = dv.ToTable();
            //string EmployeeKey = zTemp.Rows[0]["EmployeeKey"].ToString();

            // lấy tổng số tiền chừa người thấp nhất
            //float sumkgptoduct = 0;
            //float summoney = 0;
            //for (int i = 0; i < _BangSoSanh.Rows.Count; i++)
            //{
            //    if (_BangSoSanh.Rows[i]["EmployeeKey"].ToString() != EmployeeKey)
            //    {
            //        sumkgptoduct += float.Parse(_BangSoSanh.Rows[i]["KgProduct"].ToString());
            //        summoney += float.Parse(_BangSoSanh.Rows[i]["Money"].ToString());
            //    }
            //}
            // chia tiền lại cho người thấp nhất
            //for (int i = 0; i < _BangSoSanh.Rows.Count; i++)
            //{
            //    if (_BangSoSanh.Rows[i]["EmployeeKey"].ToString() == EmployeeKey)
            //    {
            //        _BangSoSanh.Rows[i]["KgProduct"] = (float.Parse(txt_Quantity.Text.ToString()) - sumkgptoduct).ToString("n4");
            //        _BangSoSanh.Rows[i]["Money"] = (float.Parse(txt_Total.Text.ToString())-summoney).Ton0String();
            //        break;
            //    }

            //}
            #endregion
            InitData_GVData(GVEmployee, _BangSoSanh);
            InitData_GVSum(GVEmployee);
        }
        private void ProcessCount()
        {
            string EmployeeKey = "";
            float sumkgptoduct = 0;
            float summoney = 0;
            float money = 0;//Số lệch 
            //-----lọc dữ liệu có số tiền thấp nhất, lấy mã nhân viên có số tiền thấp nhất, nếu bằng nhau hết lấy người đầu tiên----

            DataRow dr = _BangSoSanh.AsEnumerable().OrderBy(r => r["KgProduct"]).Where(r => ((float)r["KgProduct"] > 0)).First(); // getting the row to edit , change it as you need
            // lấy tổng số tiền chừa người thấp nhất
            EmployeeKey = dr["EmployeeKey"].ToString();
            if (float.TryParse(_BangSoSanh.Compute("Sum(KgProduct)", "EmployeeKey NOT IN ('" + EmployeeKey + "')").ToString(), out sumkgptoduct))
            {

            }
            if (float.TryParse(_BangSoSanh.Compute("Sum(Money)", "EmployeeKey NOT IN ('" + EmployeeKey + "')").ToString(), out summoney))
            {

            }
            money = (float.Parse(txt_Total.Text.ToString()) - summoney);
            if (money >= 0)
            {
                // chia tiền lại cho người thấp nhất
                dr["KgProduct"] = (float.Parse(txt_Quantity.Text.ToString()) - sumkgptoduct).ToString("n4");
                dr["Money"] = (float.Parse(txt_Total.Text.ToString()) - summoney).Ton0String();
            }
            else
            {// số tiền âm
             // lấy người số tiền chừa người cao nhất
                dr = dr = _BangSoSanh.AsEnumerable().OrderBy(r => r["KgProduct"]).Where(r => ((float)r["KgProduct"] > 0)).Last(); ; // getting the row to edit , change it as you need
                                                                                                                             // lấy tổng số tiền chừa người cao nhất
                EmployeeKey = dr["EmployeeKey"].ToString();
                if (float.TryParse(_BangSoSanh.Compute("Sum(KgProduct)", "EmployeeKey NOT IN ('" + EmployeeKey + "')").ToString(), out sumkgptoduct))
                {

                }
                if (float.TryParse(_BangSoSanh.Compute("Sum(Money)", "EmployeeKey NOT IN ('" + EmployeeKey + "')").ToString(), out summoney))
                {

                }
                dr["KgProduct"] = (float.Parse(txt_Quantity.Text.ToString()) - sumkgptoduct).ToString("n4");
                dr["Money"] = (float.Parse(txt_Total.Text.ToString()) - summoney).Ton0String();
            }    
        }
        //lưu thông tin đã tính toán
        private void SaveResult_Order()
        {
            DateTime zOrderDate = Convert.ToDateTime(GVOrder.CurrentRow.Cells["OrderDate"].Value.ToString());

            for (int i = 0; i < GVEmployee.Rows.Count - 1; i++)
            {
                DataGridViewRow r = GVEmployee.Rows[i];
                int Borrow = 0;   //mượn
                int Private = 0;  //ăn riêng
                int Share = 0; //ăn chung
                if (r.Cells["Borrow"].Value != null)
                    Borrow = 1;
                if (r.Cells["Private"].Value != null)
                    Private = 1;
                if (r.Cells["Share"].Value != null)
                    Share = 1;
                float Time = 0;
                if (float.TryParse(r.Cells["Time"].Value.ToString(), out Time))
                {
                    //Số giờ 
                }
                float Basket = 0;
                if (float.TryParse(r.Cells["Basket"].Value.ToString(), out Basket))
                {
                    //Số rỗ 
                }
                float ResultKg = 0;
                if (float.TryParse(r.Cells["KgProduct"].Value.ToString(), out ResultKg))
                {
                    //ket quả số kg
                }
                float ResultMoney = 0;
                if (float.TryParse(r.Cells["Money"].Value.ToString(), out ResultMoney))
                {
                    // kết quả số tiền
                }
                string EmployeeKey = r.Cells["EmployeeID"].Tag.ToString();
                string EmployeeID = r.Cells["EmployeeID"].Value.ToString();
                string EmployeeName = r.Cells["EmployeeName"].Value.ToString();


                Order_Money_Model _OrderMoney = new Order_Money_Model();
                _OrderMoney.OrderDate = zOrderDate;
                _OrderMoney.EmployeeKey = EmployeeKey;      //**KEY UDPATE**
                _OrderMoney.EmployeeID = EmployeeID;
                _OrderMoney.EmployeeName = EmployeeName;
                _OrderMoney.StageKey = _WorkKey;
                _OrderMoney.TeamKey = _TeamKey;
                _OrderMoney.OrderKey = _OrderKey;               //**KEY UDPATE**

                //-----BÌNH THƯỜNG ĂN CHUNG (TÌNH HUỐNG MẶT ĐỊNH)
                if ((Borrow == 0 && Private == 0 && Share == 0) ||
                   (Borrow == 0 && Private == 0 && Share == 1))
                {
                    _OrderMoney.Time = Time;
                    _OrderMoney.Kg = ResultKg;
                    _OrderMoney.Money = ResultMoney;
                    _OrderMoney.Basket = Basket;

                    _OrderMoney.Category = 1;//**Lưu ý**
                }
                //-----BÌNH THƯỜNG ĂN RIÊNG
                if (Borrow == 0 && Private == 1 && Share == 0)
                {
                    _OrderMoney.Basket_Private = Basket;
                    _OrderMoney.Time_Private = Time;
                    _OrderMoney.Kg_Private = ResultKg;
                    _OrderMoney.Money_Private = ResultMoney;

                    _OrderMoney.Category_Private = 1;//**Lưu ý**
                }
                //-----MƯỢN MÀ ĂN RIÊNG
                if (Borrow == 1 && Private == 1 && Share == 0)
                {
                    _OrderMoney.Basket_Borrow = Basket;
                    _OrderMoney.Time_Borrow = Time;
                    _OrderMoney.Kg_Borrow = ResultKg;
                    _OrderMoney.Money_Borrow = ResultMoney;

                    _OrderMoney.Category_Borrow = 1;//**Lưu ý**
                }
                //-----MƯỢN MÀ ĂN CHUNG
                if (Borrow == 1 && Private == 0 && Share == 1)
                {
                    _OrderMoney.Money_Eat = ResultMoney;
                    _OrderMoney.Time_Eat = Time;
                    _OrderMoney.Kg_Eat = ResultKg;
                    _OrderMoney.Basket_Eat = Basket;

                    _OrderMoney.Category_Eat = 1; //**Lưu ý**
                }
                //----- LƯU DB
                Order_Exe OrderExe = new Order_Exe();
                OrderExe.OrderMoney = _OrderMoney;
                OrderExe.CreatedBy = SessionUser.UserLogin.Key;
                OrderExe.ModifiedBy = SessionUser.UserLogin.Key;
                OrderExe.CreatedName = SessionUser.UserLogin.EmployeeName;
                OrderExe.ModifiedName = SessionUser.UserLogin.EmployeeName;
                OrderExe.Save();

                if (OrderExe.Message != "11" &&
                    OrderExe.Message != "20")
                    r.Cells["Message"].Value = OrderExe.Message;
                else
                    r.Cells["Message"].Value = "Đã cập nhật";


                #region[Đông đóng 31/01/2021]
                ////----- 
                //Order_Employee_Info zEmployee = new Order_Employee_Info();
                //zEmployee.EmployeeKey = EmployeeKey;
                //zEmployee.OrderKey = _OrderKey;
                //zEmployee.Money = ResultMoney;
                //zEmployee.UpdateMoney();

                ////SCCĐ và SCĐH
                //if (_TeamKey == _TeamSCCD ||
                //    _TeamKey == _TeamSCDH)
                //{
                //    int zCount = OrderExe.TimeExits();
                //    Money_TimeKeep_Info zInfo = new Money_TimeKeep_Info();

                //    zInfo.OrderKey = _OrderKey;
                //    zInfo.DateTimeKeep = _OrderMoney.OrderDate;
                //    zInfo.EmployeeKey = _OrderMoney.EmployeeKey;
                //    zInfo.EmployeeID = _OrderMoney.EmployeeID;
                //    zInfo.EmployeeName = _OrderMoney.EmployeeName;
                //    zInfo.MoneyTimeKeep = ResultMoney;

                //    if (zCount == 0)
                //    {
                //        zInfo.CreatedBy = SessionUser.UserLogin.Key;
                //        zInfo.CreatedName = SessionUser.UserLogin.EmployeeName;
                //        zInfo.Create();
                //    }
                //    else
                //    {
                //        zInfo.ModifiedBy = SessionUser.UserLogin.Key;
                //        zInfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
                //        zInfo.Update();
                //    }
                //}
                ////----- 
                #endregion
            }
        }
        #endregion

        void KhoiTaoColumnBangSoSanh()
        {
            _BangSoSanh = new DataTable();
            _BangSoSanh.Columns.Add("EmployeeKey");
            _BangSoSanh.Columns.Add("EmployeeID");
            _BangSoSanh.Columns.Add("EmployeeName");
            _BangSoSanh.Columns.Add("Kilogram");
            _BangSoSanh.Columns.Add("Basket");
            _BangSoSanh.Columns.Add("Time");
            _BangSoSanh.Columns.Add("KgProduct");
            _BangSoSanh.Columns["KgProduct"].DataType = typeof(float);
            _BangSoSanh.Columns.Add("Money");
            _BangSoSanh.Columns["Money"].DataType = typeof(float);
            _BangSoSanh.Columns.Add("Borrow");
            _BangSoSanh.Columns.Add("Private");
            _BangSoSanh.Columns.Add("Share");
            _BangSoSanh.Columns.Add("Gender");
        }

        private float HamTinhTienTongDongHang(DateTime OrderDate, float Quantity, float Price, float RateOrder,
           float RateDay, float PercentOrder, float TimeMale, float TimeFeMale)
        {
            float zTongTien = 0;

            if (Price > 0)
            {
                zTongTien = Quantity * Price * RateOrder; // số lượng thực tế * đơn giá * hệ số
                //TH 1.2 Nếu Ngày hiện tại là ngày lễ
                if (RateDay > 0)
                {
                    zTongTien *= RateDay;
                }
                //TH 1.3 Nếu Ngày hiện tại là ngày chủ nhât
                if (OrderDate.DayOfWeek == DayOfWeek.Sunday && RateDay == 0)
                {
                    zTongTien *= _DayOfWeek;
                }
                zTongTien *= (PercentOrder / 100);

                return zTongTien;
            }
            else
            {
                float _MMoney = HoTroSanXuat.LayDonGiaNam(OrderDate);      //tiền danh cho công nhân nam
                float _FMoney = HoTroSanXuat.LayDonGiaNu(OrderDate);       //tiền danh cho công nhân nữ
                //Số tiền  nam kiếm được
                float MoneyMale = 0;
                MoneyMale = TimeFeMale * _MMoney;
                float MoneyFeMale = 0;
                MoneyFeMale = TimeFeMale * _FMoney;

                zTongTien = MoneyMale + MoneyFeMale;
                zTongTien *= (PercentOrder / 100);
                return zTongTien;
            }

        }
        //private float HamTinhTienTongDongHang(DateTime OrderDate, float Quantity, float Price,
        //    float TotalBasket, float TotalKg, float TotalTime, float RateOrder,
        //    float RateDay, float PercentOrder, float TimeMale, float TimeFeMale)
        //{
        //    float zTongTien = 0;

        //    if (Price > 0)
        //    {
        //        if (TotalKg > 0)
        //        {
        //            //TH 1.1 Nếu Ngày hiện tại là ngày thường
        //            zTongTien = Quantity * Price * RateOrder; // số lượng thực tế * đơn giá * hệ số
        //            //TH 1.2 Nếu Ngày hiện tại là ngày lễ
        //            if (RateDay > 0)
        //            {
        //                zTongTien *= RateDay;
        //            }
        //            //TH 1.3 Nếu Ngày hiện tại là ngày chủ nhât
        //            if (OrderDate.DayOfWeek == DayOfWeek.Sunday && RateDay == 0)
        //            {
        //                zTongTien *= _DayOfWeek;
        //            }

        //        }
        //        else if (TotalBasket > 0)
        //        {
        //            //TH 1.1:Nếu Ngày hiện tại là ngày thường 
        //            zTongTien = Quantity * Price * RateOrder;
        //            //TH 1.2 Nếu Ngày hiện tại là ngày lễ
        //            if (RateDay > 0)
        //            {
        //                zTongTien *= RateDay;
        //            }
        //            //TH 1.3:Nếu Ngày hiện tại là ngày chủ nhật
        //            if (OrderDate.DayOfWeek == DayOfWeek.Sunday && RateDay == 0)
        //            {
        //                zTongTien *= _DayOfWeek;
        //            }
        //        }
        //        else if (TotalTime > 0)
        //        { 
        //            //TH 2.2:Nếu Ngày hiện tại là ngày thường
        //            zTongTien = Quantity * Price * RateOrder;
        //            //TH 2.1 Nếu Ngày hiện tại là ngày lễ
        //            if (RateDay > 0)
        //            {
        //                zTongTien *= RateDay;
        //            }
        //            //TH 2.2:Nếu Ngày hiện tại  là ngày chủ nhật
        //            if (OrderDate.DayOfWeek == DayOfWeek.Sunday && RateDay == 0)
        //            {
        //                zTongTien *= _DayOfWeek;
        //            }
        //        }
        //        zTongTien *= (PercentOrder / 100);

        //        return zTongTien;
        //    }
        //    else
        //    {
        //        float _MMoney = HoTroSanXuat.LayDonGiaNam(dte_Date.Value);      //tiền danh cho công nhân nam
        //        float _FMoney = HoTroSanXuat.LayDonGiaNu(dte_Date.Value);       //tiền danh cho công nhân nữ
        //        //Số tiền  nam kiếm được
        //        float MoneyMale = 0;
        //        MoneyMale = TimeFeMale * _MMoney;
        //        float MoneyFeMale = 0;
        //        MoneyFeMale = TimeFeMale * _FMoney;

        //        zTongTien = MoneyMale + MoneyFeMale;
        //        zTongTien *= (PercentOrder / 100);
        //        return zTongTien;
        //    }

        //}

        private void Btn_Count_Click(object sender, EventArgs e)
        {
            if (_OrderKey != "")
            {
                Calculator_Order(GVOrder.CurrentRow);
                InitData_GVSum(GVEmployee);
            }
            else
            {
                Utils.TNMessageBoxOK("Bạn phải chọn 1 đơn hàng !.", 1);
            }

        }
        private void Btn_Save_Click(object sender, EventArgs e)
        {
            if (SessionUser.Date_Lock >= dte_Date.Value)
            {
                Utils.TNMessageBoxOK("Thời gian này đã khóa sổ.!", 2);
                return;
            }
            Cursor = Cursors.WaitCursor;
            if (_OrderKey != "")
            {
                SaveResult_Order();
            }
            else
            {
                Utils.TNMessageBoxOK("Bạn phải chọn 1 đơn hàng !.", 1);
            }
            Cursor = Cursors.Default;
        }

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                //this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region [Show Hide]      
        private void btnShowInfo_Click(object sender, EventArgs e)
        {
            Panel_Right.Visible = true;
            btnHideInfo.Visible = true;
            btnShowInfo.Visible = false;
        }

        private void btnHideInfo_Click(object sender, EventArgs e)
        {
            Panel_Right.Visible = false;
            btnHideInfo.Visible = false;
            btnShowInfo.Visible = true;
        }
        #endregion
    }
}