﻿using OfficeOpenXml;
using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using TN.Library.System;
using TNS.HRM;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Human_Time : Form
    {
        string _FileName = "";
        int _CheckErr = 0;
        int _Key = 0;

        DateTime _DateTime;
        DataTable _tblSheet = new DataTable();
        DataTable _TableDefines = new DataTable();
        public Frm_Human_Time()
        {
            InitializeComponent();
            this.DoubleBuffered = true;

            Utils.DoubleBuffered(GVData, true);
            Utils.DrawGVStyle(ref GVData);
            Utils.DrawLVStyle(ref LVFile);

            btnClose.Click += btnClose_Click;
            btnMax.Click += btnMax_Click;
            btnMini.Click += btnMini_Click;

            btn_Save.Click += Btn_Save_Click;
            btn_Del.Click += Btn_Del_Click;
            btn_Search.Click += Btn_Search_Click;

            LVFile.SelectedIndexChanged += LVFile_Click;
            GVData.CellEndEdit += GVData_CellEndEdit;

            InitLayout_LV(LVFile);
            InitLayout_GV();
            Utils.SizeLastColumn_LV(LVFile);
        }

        private void Frm_Human_Time_Load(object sender, EventArgs e)
        {
            dte_FromDate.Value = SessionUser.Date_Work;
            dte_ToDate.Value = SessionUser.Date_Work;
            LoadTableDefines();
            InitData_LV();
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
        }

        #region[Listview]
        private void InitLayout_LV(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "No";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ngày";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên file";
            colHead.Width = 200;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

        }
        private void InitData_LV()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LVFile;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            LV.Items.Clear();

            DateTime FromDate = new DateTime(dte_FromDate.Value.Year, dte_FromDate.Value.Month, dte_FromDate.Value.Day, 0, 0, 0);
            DateTime ToDate = new DateTime(dte_ToDate.Value.Year, dte_ToDate.Value.Month, dte_ToDate.Value.Day, 23, 59, 59);
            DataTable ztb = Temp_Excel_Data.ListFile(FromDate, ToDate, string.Empty);
            for (int i = 0; i < ztb.Rows.Count; i++)
            {
                DataRow nRow = ztb.Rows[i];

                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.Tag = nRow["ExcelKey"];
                lvi.ForeColor = Color.Navy;

                lvi.BackColor = Color.White;
                lvi.ImageIndex = 0;

                lvsi = new ListViewItem.ListViewSubItem();
                DateTime zDateImport = DateTime.Parse(nRow["DateImport"].ToString().Trim());
                lvsi.Text = zDateImport.ToString("dd/MM/yyyy");
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["File"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }

            this.Cursor = Cursors.Default;

        }
        private void LVFile_Click(object sender, EventArgs e)
        {
            if (LVFile.SelectedItems.Count > 0)
            {
                _Key = int.Parse(LVFile.SelectedItems[0].Tag.ToString());
                _DateTime = DateTime.Parse(LVFile.SelectedItems[0].SubItems[1].Text.Trim());
                DataTable In_Table = Temp_Excel_Detail_Data.LoadExcelData(_Key);
                InitData_GV_FromSQL(In_Table);

                GVTitle.Text = "Chi tiết tập tin " + _FileName;
            }
            else
            {
                _Key = 0;
                GVData.Rows.Clear();
            }
        }
        #endregion

        #region [GV_Employee]
        private void InitLayout_GV()
        {
            // Setup Column 
            GVData.Columns.Add("No", "STT");
            GVData.Columns.Add("EmployeeName", "Tên Nhân Viên");
            GVData.Columns.Add("EmployeeID", "Mã NV");
            GVData.Columns.Add("TeamID", "Bộ phận");
            GVData.Columns.Add("BeginTime", "Giờ bắt đầu");
            GVData.Columns.Add("EndTime", "Giờ kết thúc");
            GVData.Columns.Add("OffTime", "Số giờ nghỉ trưa");
            GVData.Columns.Add("TotalTime", "Tổng giờ");
            GVData.Columns.Add("CodeName", "Mã công");
            GVData.Columns.Add("RiceName", "Mã cơm");
            GVData.Columns.Add("Money_Rice", "Tiền cơm");
            GVData.Columns.Add("OverTimeBegin", "Qua đêm từ ngày");
            GVData.Columns.Add("OverTimeEnd", "Qua đêm đến ngày");
            GVData.Columns.Add("Message", "Thông báo");


            GVData.Columns["No"].Width = 40;
            GVData.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GVData.Columns["No"].ReadOnly = true;
            GVData.Columns["No"].Frozen = true;

            GVData.Columns["EmployeeName"].Width = 160;
            GVData.Columns["EmployeeName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GVData.Columns["EmployeeName"].ReadOnly = true;
            GVData.Columns["EmployeeName"].Frozen = true;

            GVData.Columns["EmployeeID"].Width = 100;
            GVData.Columns["EmployeeID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GVData.Columns["EmployeeID"].Frozen = true;

            GVData.Columns["TeamID"].Width = 120;
            GVData.Columns["TeamID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GVData.Columns["TeamID"].Frozen = true;

            GVData.Columns["BeginTime"].Width = 140;
            GVData.Columns["BeginTime"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GVData.Columns["EndTime"].Width = 140;
            GVData.Columns["EndTime"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GVData.Columns["OffTime"].Width = 80;
            GVData.Columns["OffTime"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GVData.Columns["TotalTime"].Width = 80;
            GVData.Columns["TotalTime"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GVData.Columns["TotalTime"].ReadOnly = true;

            GVData.Columns["CodeName"].Width = 80;
            GVData.Columns["CodeName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GVData.Columns["RiceName"].Width = 80;
            GVData.Columns["RiceName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GVData.Columns["Money_Rice"].Width = 80;
            GVData.Columns["Money_Rice"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GVData.Columns["Money_Rice"].ReadOnly = true;

            GVData.Columns["OverTimeBegin"].Width = 100;
            GVData.Columns["OverTimeBegin"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GVData.Columns["OverTimeBegin"].ReadOnly = true;

            GVData.Columns["OverTimeEnd"].Width = 100;
            GVData.Columns["OverTimeEnd"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GVData.Columns["OverTimeEnd"].ReadOnly = true;

            GVData.Columns["Message"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            GVData.ColumnHeadersHeight = 40;
        }
        private void InitData_GV_FromExcel(DataTable In_Table)
        {
            GVData.Rows.Clear();
            for (int i = 0; i < In_Table.Rows.Count - 1; i++)
            {
                DataRow zRow = In_Table.Rows[i + 1];
                GVData.Rows.Add();
                GVData.Rows[i].Cells["No"].Value = (i + 1).ToString();
                GVData.Rows[i].Cells["EmployeeName"].Value = zRow[1].ToString();
                GVData.Rows[i].Cells["EmployeeID"].Value = zRow[2].ToString();
                GVData.Rows[i].Cells["TeamID"].Value = zRow[3].ToString();
                if (zRow[4].ToString() == "")
                {
                    GVData.Rows[i].Cells["BeginTime"].Value = 0;
                }
                else
                {
                    GVData.Rows[i].Cells["BeginTime"].Value = GetTime(zRow[4].ToString());
                }
                if (zRow[5].ToString() == "")
                {
                    GVData.Rows[i].Cells["EndTime"].Value = 0;
                }
                else
                {
                    GVData.Rows[i].Cells["EndTime"].Value = GetTime(zRow[5].ToString());
                }
                if (zRow[6].ToString() == "")
                {
                    GVData.Rows[i].Cells["OffTime"].Value = 0;
                }
                else
                {
                    GVData.Rows[i].Cells["OffTime"].Value = GetTime(zRow[6].ToString());
                }
                if (zRow[7].ToString() == "")
                {
                    GVData.Rows[i].Cells["TotalTime"].Value = 0;
                }
                else
                {
                    GVData.Rows[i].Cells["TotalTime"].Value = GetTime(zRow[7].ToString());
                }
                GVData.Rows[i].Cells["CodeName"].Value = zRow[8].ToString();
                GVData.Rows[i].Cells["RiceName"].Value = zRow[9].ToString();
                Time_Rice_Info zRice_Info = new Time_Rice_Info();
                zRice_Info.Get_Info(zRow[9].ToString().Trim());
                GVData.Rows[i].Cells["Money_Rice"].Value = zRice_Info.Money.Ton1String();
                if (zRow[10].ToString().Trim() != "")
                    GVData.Rows[i].Cells["OverTimeBegin"].Value = zRow[10].ToString().Trim().ToDateString();
                else
                    GVData.Rows[i].Cells["OverTimeBegin"].Value = "";
                if (zRow[11].ToString().Trim() != "")
                    GVData.Rows[i].Cells["OverTimeEnd"].Value = zRow[11].ToString().Trim().ToDateString();
                else
                    GVData.Rows[i].Cells["OverTimeEnd"].Value = "";
            }
        }
        private void InitData_GV_FromSQL(DataTable In_Table)
        {
            GVData.Rows.Clear();
            for (int i = 0; i < In_Table.Rows.Count; i++)
            {
                DataRow zRow = In_Table.Rows[i];
                GVData.Rows.Add();
                GVData.Rows[i].Cells["No"].Value = (i + 1).ToString();
                GVData.Rows[i].Cells["No"].Tag = zRow["AutoKey"].ToString();
                GVData.Rows[i].Cells["EmployeeName"].Value = zRow["EmployeeName"].ToString();
                GVData.Rows[i].Cells["EmployeeID"].Value = zRow["EmployeeID"].ToString();
                GVData.Rows[i].Cells["TeamID"].Value = zRow["TeamID"].ToString();
                if (zRow["BeginTime"].ToString() == "")
                {
                    GVData.Rows[i].Cells["BeginTime"].Value = 0;
                }
                else
                {
                    GVData.Rows[i].Cells["BeginTime"].Value = zRow["BeginTime"].ToString();
                }
                if (zRow["EndTime"].ToString() == "")
                {
                    GVData.Rows[i].Cells["EndTime"].Value = 0;
                }
                else
                {
                    GVData.Rows[i].Cells["EndTime"].Value = zRow["EndTime"].ToString();
                }
                if (zRow["OffTime"].ToString() == "")
                {
                    GVData.Rows[i].Cells["OffTime"].Value = 0;
                }
                else
                {
                    GVData.Rows[i].Cells["OffTime"].Value = zRow["OffTime"].ToString();
                }
                if (zRow["TotalTime"].ToString() == "")
                {
                    GVData.Rows[i].Cells["TotalTime"].Value = 0;
                }
                else
                {
                    GVData.Rows[i].Cells["TotalTime"].Value = zRow["TotalTime"].ToString();
                }
                GVData.Rows[i].Cells["CodeName"].Value = zRow["CodeName"].ToString();
                GVData.Rows[i].Cells["RiceName"].Value = zRow["RiceName"].ToString();
                GVData.Rows[i].Cells["Money_Rice"].Value = zRow["Money_Rice"].Ton1String();
                GVData.Rows[i].Cells["OverTimeBegin"].Value = zRow["OverTimeBegin"].ToString();
                GVData.Rows[i].Cells["OverTimeEnd"].Value = zRow["OverTimeEnd"].ToString();
                GVData.Rows[i].Cells["Message"].Tag = 1;
            }
        }
        private void GVData_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            DataGridViewTextBoxEditingControl te = (DataGridViewTextBoxEditingControl)e.Control;
            te.AutoCompleteMode = AutoCompleteMode.None;
            switch (GVData.CurrentCell.ColumnIndex)
            {

                case 4:
                    e.Control.KeyPress += new KeyPressEventHandler(ControlNumber_KeyPress);
                    break;
                case 5:
                    e.Control.KeyPress += new KeyPressEventHandler(ControlNumber_KeyPress);
                    break;
                case 6:
                    e.Control.KeyPress += new KeyPressEventHandler(ControlNumber_KeyPress);
                    break;
                case 7:
                    e.Control.KeyPress += new KeyPressEventHandler(ControlNumber_KeyPress);
                    break;

                default:
                    e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);

                    break;
            }
        }
        private void GVData_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow zRowEdit = GVData.Rows[e.RowIndex];
            switch (e.ColumnIndex)
            {

                case 4: // giờ bđ
                    if (zRowEdit.Cells[e.ColumnIndex].Value != null)
                    {
                        zRowEdit.Cells[e.ColumnIndex].Value = GetTime(zRowEdit.Cells[e.ColumnIndex].Value.ToString());
                    }
                    else
                    {
                        zRowEdit.Cells[e.ColumnIndex].Value = 0;
                    }
                    break;
                case 5: // giờ kt
                    if (zRowEdit.Cells[e.ColumnIndex].Value != null)
                    {
                        zRowEdit.Cells[e.ColumnIndex].Value = GetTime(zRowEdit.Cells[e.ColumnIndex].Value.ToString());
                    }
                    else
                    {
                        zRowEdit.Cells[e.ColumnIndex].Value = 0;
                    }
                    break;
                case 6: // giờ nghi
                    if (zRowEdit.Cells[e.ColumnIndex].Value != null)
                    {
                        zRowEdit.Cells[e.ColumnIndex].Value = GetTime(zRowEdit.Cells[e.ColumnIndex].Value.ToString());
                    }
                    else
                    {
                        zRowEdit.Cells[e.ColumnIndex].Value = 0;
                    }
                    break;
                case 7: // tong giờ
                    if (zRowEdit.Cells[e.ColumnIndex].Value != null)
                    {
                        zRowEdit.Cells[e.ColumnIndex].Value = GetTime(zRowEdit.Cells[e.ColumnIndex].Value.ToString());
                    }
                    else
                    {
                        zRowEdit.Cells[e.ColumnIndex].Value = 0;
                    }
                    break;

                case 8:
                case 9:
                    if (zRowEdit.Cells[e.ColumnIndex].Value != null)
                        zRowEdit.Cells[e.ColumnIndex].Value = zRowEdit.Cells[e.ColumnIndex].Value.ToString().ToUpper();
                    break;
                case 10:
                    if (zRowEdit.Cells[e.ColumnIndex].Value == null)
                        zRowEdit.Cells[e.ColumnIndex].Value = 0;
                    break;
            }
            CheckRow(GVData.Rows[e.RowIndex]);
        }
        private void ControlNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }

            if (e.KeyChar == '.')
            {
                e.Handled = false;
            }
            if (e.KeyChar == ',')
            {
                e.Handled = false;
            }
        }
        private void Control_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = false;
        }
        #endregion

        #region[Progess_Event]
        private void CheckBeforeSave()
        {
            _CheckErr = 0;
            string zMessage = "";
            for (int i = 0; i < GVData.Rows.Count - 1; i++)
            {
                if (GVData.Rows[i].Cells["Message"].Tag == null)
                {
                    zMessage += CheckRow(GVData.Rows[i]);
                }
            }
            if (zMessage != "")
            {
                MessageBox.Show(zMessage, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _CheckErr++;
                this.Cursor = Cursors.Default;
            }
        }
        private string CheckRow(DataGridViewRow zRow)
        {
            string zMessage = "";
            zRow.DefaultCellStyle.ForeColor = Color.Navy;

            string zEmployeeName = "";
            if (zRow.Cells["EmployeeName"].Value != null)
            {
                zEmployeeName = zRow.Cells["EmployeeName"].Value.ToString();
            }
            string zEmployeeID = "";
            if (zRow.Cells["EmployeeID"].Value != null)
            {
                zEmployeeID = zRow.Cells["EmployeeID"].Value.ToString();
            }
            string zTeamID = "";
            if (zRow.Cells["TeamID"].Value != null)
            {
                zTeamID = zRow.Cells["TeamID"].Value.ToString();
            }
            string zCodeName = "";
            if (zRow.Cells["CodeName"].Value != null)
            {
                zCodeName = zRow.Cells["CodeName"].Value.ToString();
            }
            string zRiceName = "";
            if (zRow.Cells["RiceName"].Value != null)
            {
                zRiceName = zRow.Cells["RiceName"].Value.ToString();
            }
            string zOverTimeBegin = "";
            if (zRow.Cells["OverTimeBegin"].Value != null)
            {
                zOverTimeBegin = zRow.Cells["OverTimeBegin"].Value.ToString();
            }
            string zOverTimeEnd = "";
            if (zRow.Cells["OverTimeEnd"].Value != null)
            {
                zOverTimeBegin = zRow.Cells["OverTimeEnd"].Value.ToString();
            }

            Employee_Info zEmployee = new Employee_Info();
            zEmployee.GetEmployeeID(zEmployeeID);
            if (zEmployeeID == "")
            {
                zMessage += "Stt:" + (zRow.Cells["No"].Value) + "; Chưa nhập mã thẻ" + Environment.NewLine;
            }
            else if (zEmployee.Key == "")
            {
                zMessage += "Stt: " + (zRow.Cells["No"].Value) + "; Mã thẻ: " + zEmployeeID + " không tồn tại" + Environment.NewLine;
            }
            else
            {
                if (zEmployee.TeamKey == 0)
                {
                    zRow.DefaultCellStyle.BackColor = Color.OrangeRed;
                    zMessage += "Stt: " + (zRow.Cells["No"].Value) + "; Mã thẻ:" + zEmployeeID + " chưa thuộc nhóm nào" + Environment.NewLine;
                }
            }

            Team_Info zTeam = new Team_Info();
            zTeam.Get_Team_ID(zTeamID);
            if (zTeam.Key == 0)
            {
                zMessage += "Stt: " + (zRow.Cells["No"].Value) + "; Mã nhóm: " + zTeamID + " không tồn tại" + Environment.NewLine;
            }
            Time_Defines_Info ztime = new Time_Defines_Info();
            ztime.Get_Info(zCodeName);
            if (ztime.Key == 0)
            {
                zMessage += "Stt: " + (zRow.Cells["No"].Value) + "; Mã công: " + zCodeName + " không tồn tại" + Environment.NewLine;
            }

            Time_Rice_Info zRice_Info = new Time_Rice_Info();
            zRice_Info.Get_Info(zRiceName);
            if (zRice_Info.Key == 0)
            {
                zMessage += "Stt: " + (zRow.Cells["No"].Value) + "; Mã cơm: " + zRiceName + " không tồn tại" + Environment.NewLine;
            }
            if (zOverTimeBegin != "")
            {
                DateTime OverTimeBegin; ;
                if (!DateTime.TryParse(zOverTimeBegin, out OverTimeBegin))
                {
                    zMessage += "Stt: " + (zRow.Cells["No"].Value) + "; Ngày qua đêm từ: " + zOverTimeBegin + " không đúng định dạng" + Environment.NewLine;
                }
            }
            if (zOverTimeEnd != "")
            {
                DateTime OverTimeEnd;
                if (!DateTime.TryParse(zOverTimeEnd, out OverTimeEnd))
                {

                    zMessage += "Stt:" + (zRow.Cells["No"].Value) + "; Ngày qua đêm đến: " + zOverTimeBegin + " không đúng định dạng" + Environment.NewLine;
                }
            }
            if (zMessage == "")
            {
                zRow.Cells["Message"].Tag = 1;
                zRow.Cells["Message"].Value = "";
            }
            else
            {
                zRow.DefaultCellStyle.ForeColor = Color.Maroon;
                zRow.Cells["Message"].Tag = null;
                zRow.Cells["Message"].Value = zMessage;
            }
            return zMessage;
        }
        private void Save()
        {
            _CheckErr = 0;

            Temp_Excel_Info ztemp = new Temp_Excel_Info(_Key);
            ztemp.DateImport = _DateTime;
            ztemp.File = _FileName;
            ztemp.CreatedBy = SessionUser.UserLogin.EmployeeKey;
            ztemp.CreatedName = SessionUser.UserLogin.EmployeeName;
            ztemp.ModifiedBy = SessionUser.UserLogin.EmployeeKey;
            ztemp.ModifiedName = SessionUser.UserLogin.EmployeeName;
            ztemp.Save();
            _Key = ztemp.Key;
            for (int i = 0; i < GVData.Rows.Count; i++)
            {
                DataGridViewRow zRow = GVData.Rows[i];
                zRow.DefaultCellStyle.ForeColor = Color.Navy;
                if (zRow.Cells["Message"].Tag != null && zRow.Cells["Message"].Tag.ToInt() == 1)
                {
                    string Message = CheckRow(zRow);
                    if (Message == string.Empty)
                    {
                        Message = AddRecord(zRow);
                        if (Message == string.Empty)
                            DeleteRecord(zRow);
                    }

                    //string Message = AddRecord(zRow);
                    //if (Message == string.Empty)
                    //    GVData.Rows.Remove(zRow);
                    //else
                    //{
                    //    zRow.Cells["Message"].Value = Message;
                    //    zRow.Cells["Message"].Tag = null;
                    //    zRow.DefaultCellStyle.ForeColor = Color.Red;
                    //    _CheckErr++;
                    //}
                }
            }

        }

        private string AddRecord(DataGridViewRow zRow)
        {
            Temp_Excel_Detail_Info zDetail;
            if (zRow.Cells["No"].Tag == null)
            {
                zDetail = new Temp_Excel_Detail_Info();
            }
            else
            {
                zDetail = new Temp_Excel_Detail_Info(zRow.Cells["No"].Tag.ToInt());
            }
            zDetail.SheetKey = 1;
            zDetail.DateImport = _DateTime;
            zDetail.ExcelKey = _Key;

            string zEmployeeID = zRow.Cells["EmployeeID"].Value.ToString().Trim();
            Employee_Info zEmployee = new Employee_Info();
            zEmployee.GetEmployeeID(zEmployeeID);
            zDetail.EmployeeKey = zEmployee.Key;
            zDetail.EmployeeID = zEmployeeID;
            zDetail.EmployeeName = zRow.Cells["EmployeeName"].Value.ToString().Trim();

            string zTeamID = zRow.Cells["TeamID"].Value.ToString();
            Team_Info zTeam = new Team_Info();
            zTeam.Get_Team_ID(zTeamID);
            zDetail.TeamKey = zTeam.Key;
            zDetail.TeamID = zTeamID;
            zDetail.TeamName = zTeam.TeamName;

            zDetail.BeginTime = zRow.Cells["BeginTime"].Value.ToString().Trim();
            zDetail.EndTime = zRow.Cells["EndTime"].Value.ToString();
            zDetail.OffTime = zRow.Cells["OffTime"].Value.ToString();
            zDetail.TotalTime = zRow.Cells["TotalTime"].Value.ToString();

            string zCodeName = zRow.Cells["CodeName"].Value.ToString();
            Time_Defines_Info ztime = new Time_Defines_Info();
            ztime.Get_Info(zCodeName);
            zDetail.CodeKey = ztime.Key;
            zDetail.CodeName = ztime.CodeName;

            string zRiceName = zRow.Cells["RiceName"].Value.ToString().Trim();
            Time_Rice_Info zRice_Info = new Time_Rice_Info();
            zRice_Info.Get_Info(zRiceName);
            zDetail.RiceKey = zRice_Info.Key;
            zDetail.RiceName = zRice_Info.RiceName;
            zDetail.Money_Rice = zRice_Info.Money;

            if (zRow.Cells["OverTimeBegin"].Value.ToString().Trim() == "")
            {
                zDetail.OverTimeBegin = DateTime.MinValue;
            }
            else
            {
                zDetail.OverTimeBegin = DateTime.Parse(zRow.Cells["OverTimeBegin"].Value.ToString().Trim());
            }
            if (zRow.Cells["OverTimeEnd"].Value.ToString().Trim() == "")
            {
                zDetail.OverTimeEnd = DateTime.MinValue;
            }
            else
            {
                zDetail.OverTimeEnd = DateTime.Parse(zRow.Cells["OverTimeEnd"].Value.ToString().Trim());
            }

            if (zDetail.OverTimeBegin != DateTime.MaxValue && zDetail.OverTimeEnd != DateTime.MaxValue)
            {
                zDetail.CheckOverNight = 1;
            }
            zDetail.Save();
            if (zDetail.Message.Substring(0, 2) == "11" ||
               zDetail.Message.Substring(0, 2) == "20")
            {

                return string.Empty;
            }
            else
            {
                return zDetail.Message;
            }

            //    if (zDetail.Message.Substring(0, 2) == "11" || 
            //    zDetail.Message.Substring(0, 2) == "20")
            //{
            //    zRow.Cells["Message"].Value = "Thành công";
            //    zRow.Cells["Message"].Tag = 0;
            //}
            //else
            //{
            //zRow.Cells["Message"].Value = zDetail.Message;
            //zRow.Cells["Message"].Tag = null;
            //zRow.DefaultCellStyle.ForeColor = Color.Red;
            //_CheckErr++;
            //}
        }
        private void DeleteRecord(DataGridViewRow zRow)
        {
            GVData.Rows.Remove(zRow);
        }
        #endregion

        #region[Event]
        private void Btn_New_Click(object sender, EventArgs e)
        {
            OpenFileDialog zOpf = new OpenFileDialog();

            zOpf.InitialDirectory = @"C:\";
            zOpf.Title = "Browse Excel Files";

            zOpf.CheckFileExists = true;
            zOpf.CheckPathExists = true;

            zOpf.DefaultExt = "txt";
            zOpf.Filter = "All Files|*.*";
            zOpf.FilterIndex = 2;
            zOpf.RestoreDirectory = true;

            zOpf.ReadOnlyChecked = true;
            zOpf.ShowReadOnly = true;

            if (zOpf.ShowDialog() == DialogResult.OK)
            {
                FileInfo FileInfo = new FileInfo(zOpf.FileName);
                bool zcheck = true;
                zcheck = Data_Access.CheckFileStatus(FileInfo);
                if (zcheck == true)
                {
                    MessageBox.Show("File đang được sử dụng ở chương trình khác.Vui lòng đóng file để tải lên!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    try
                    {
                        _Key = 0;
                        _FileName = FileInfo.Name;
                        _DateTime = SessionUser.Date_Work;
                        ExcelPackage package = new ExcelPackage(FileInfo);
                        DataTable _tblSheet = Data_Access.GetTable(FileInfo.FullName, package.Workbook.Worksheets[1].Name); package.Dispose();
                        InitData_GV_FromExcel(_tblSheet);

                        btn_Save.Enabled = true;
                        Panel_Left.Visible = false;
                        GVTitle.Text = "Chi tiết tập tin [" + _FileName + "]";
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                        btn_Save.Enabled = false;
                    }
                }
            }
        }
        private void Btn_Save_Click(object sender, EventArgs e)
        {
            if (GVData.Rows.Count > 0)
            {
                this.Cursor = Cursors.WaitCursor;
                //CheckBeforeSave();
                //if (_CheckErr == 0)
                //{
                Save();
                if (_CheckErr == 0)
                {
                    _CheckErr = 0;
                    MessageBox.Show("Cập nhật dữ liêu thành công", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Cursor = Cursors.Default;
                }
                else
                {
                    MessageBox.Show("Đã cập nhật còn " + _CheckErr + " Lỗi", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Cursor = Cursors.Default;
                }
                //}
                this.Cursor = Cursors.Default;
            }
        }
        private void Btn_Del_Click(object sender, EventArgs e)
        {
            if (LVFile.SelectedItems.Count <= 0 && _Key == 0)
            {
                MessageBox.Show("Chưa chọn thông tin!");
            }
            else
            {
                if (MessageBox.Show("Bạn có chắc xóa thông tin này ?.", "Thông báo", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    Temp_Excel_Info zinfo = new Temp_Excel_Info(_Key);
                    zinfo.DeleteAll();
                    if (zinfo.Message == string.Empty)
                    {
                        _Key = 0;
                        int Index = LVFile.SelectedItems[0].Index;
                        LVFile.Items.RemoveAt(Index);
                        MessageBox.Show("Đã xóa !");
                        GVData.Rows.Clear();
                    }
                    else
                    {
                        MessageBox.Show(zinfo.Message);
                    }
                }
            }
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            InitData_LV();
        }
        #endregion

        #region[PROCESS]

        void LoadTableDefines()
        {
            string File = Application.StartupPath + "\\wfTime.xlsx";
            _TableDefines = Data_Access.GetTable(File, "1");
        }

        string GetTime(string Time)
        {
            string zResult = "";
            string Minute = "0.0";
            string[] s = Time.Split('.');
            string Hour = s[0];
            if (s.Length > 1)
            {
                Minute = "0." + s[1];
            }
            float temp = float.Parse(Minute);

            foreach (DataRow r in _TableDefines.Rows)
            {
                float from = r[0].ToFloat();
                float to = r[1].ToFloat();
                float min = r[2].ToFloat();
                if (temp >= from && temp <= to)
                {
                    if (temp < (float)0.95)
                    {
                        zResult = Hour + ":" + min;
                    }
                    else
                    {
                        int zHour = Hour.ToInt() + 1;
                        zResult = zHour.ToString() + ":0";
                    }
                    break;
                }
            }
            return zResult;
        }
        #endregion

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}