﻿using OfficeOpenXml;
using System;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using TNS.CORE;
using TNS.HRM;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    // 22/01/2021 Bổ sung cột Tổng giờ làm tại tổ
    public partial class Frm_Production_Time_V2 : Form
    {
        float _Sotien = 0;
        int _CheckErr = 0;
        string _txtLog = "";
        FileInfo _FileInfo;
        DataTable _TableErr;
        DataTable _TableTime = new DataTable();
        DateTime _ExcelDate = SessionUser.Date_Work;

        public Frm_Production_Time_V2()
        {
            InitializeComponent();

            this.DoubleBuffered = true;

            Utils.DoubleBuffered(GVData, true);
            Utils.DrawGVStyle(ref GVData);
            Utils.DrawLVStyle(ref LVFile);

            btnClose.Click += btnClose_Click;
            btnMax.Click += btnMax_Click;
            btnMini.Click += btnMini_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            btn_Log.Click += Btn_Log_Click;

            SetupLayoutLV(LVFile);
            SetupLayoutGV(GVData);
        }

        private void Frm_Production_Time_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);

            _Sotien = HoTroSanXuat.LayTienNgoaiGio(_ExcelDate);
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            MonthCalendar.SetDate(SessionUser.Date_Work);
            LoadTableTime();
            LoadDataLV();
        }
        private void Btn_Log_Click(object sender, EventArgs e)
        {
            if (btn_Log.Tag.ToInt() == 0)
            {
                Panel_Done.Visible = true;
                btn_Log.Tag = 1;
            }
            else
            {
                Panel_Done.Visible = false;
                btn_Log.Tag = 0;
            }
        }
        void CreateTableError()
        {
            _TableErr = new DataTable();
            _TableErr.Columns.Add("No");
            _TableErr.Columns.Add("HoVaTen");
            _TableErr.Columns.Add("MaThe");
            _TableErr.Columns.Add("MaNhom");
            _TableErr.Columns.Add("GioBD");
            _TableErr.Columns.Add("GioKT");
            _TableErr.Columns.Add("GioNghi");
            _TableErr.Columns.Add("GioCVKhac");
            _TableErr.Columns.Add("TongGio");
            _TableErr.Columns.Add("GioDu");
            _TableErr.Columns.Add("NgayQuaDemTu");
            _TableErr.Columns.Add("NgayQuaDemDen");
            _TableErr.Columns.Add("GioTaiTo");
            _TableErr.Columns.Add("ThongBao");
        }

        #region[Listview]
        private void SetupLayoutLV(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "No";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên";
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            Utils.SizeLastColumn_LV(LV);
            LV.Click += LV_Click;
        }
        private void LoadDataLV()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LVFile;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            LV.Items.Clear();

            DataTable zTable = Teams_Data.List();

            for (int i = 0; i < zTable.Rows.Count; i++)
            {
                DataRow nRow = zTable.Rows[i];

                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.Tag = nRow["TeamKey"];
                lvi.ForeColor = Color.Navy;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["TeamID"].ToString();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["TeamName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }
            Utils.SizeLastColumn_LV(LVFile);
            this.Cursor = Cursors.Default;
        }
        private void LV_Click(object sender, EventArgs e)
        {
            if (LVFile.SelectedItems.Count > 0)
            {
                _ExcelDate = MonthCalendar.SelectionStart.Date;
                ShowDataGV(LVFile.SelectedItems[0].Tag.ToInt(), _ExcelDate);
            }
        }
        #endregion

        #region[GV_Data]
        private void SetupLayoutGV(DataGridView GV)
        {
            // Setup Column 
            GV.Columns.Add("No", "STT");
            GV.Columns.Add("EmployeeID", "Mã NV");
            GV.Columns.Add("EmployeeName", "Tên Nhân Viên");
            GV.Columns.Add("TeamID", "Bộ phận");

            GV.Columns.Add("BeginTime", "Giờ bắt đầu");
            GV.Columns.Add("EndTime", "Giờ kết thúc");
            GV.Columns.Add("OffTime", "Số giờ nghỉ trưa");
            GV.Columns.Add("DifferentTime", "Giờ CV khác");
            GV.Columns.Add("TotalTime", "Tổng giờ");
            GV.Columns.Add("OverTime", "Giờ dư");
            GV.Columns.Add("OverTimeBegin", "Qua đêm từ ngày");
            GV.Columns.Add("OverTimeEnd", "Qua đêm đến ngày");

            GV.Columns.Add("Amount", "Đơn giá giờ dư");
            GV.Columns.Add("Money", "Thành tiền");
            GV.Columns.Add("Time_TeamMain", "Giờ tại tổ");
            GV.Columns.Add("Message", "Thông báo");

            GV.Columns["No"].Width = 40;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV.Columns["No"].ReadOnly = true;
            GV.Columns["No"].Frozen = true;

            GV.Columns["EmployeeID"].Width = 90;
            GV.Columns["EmployeeID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["EmployeeID"].Frozen = true;

            GV.Columns["EmployeeName"].Width = 160;
            GV.Columns["EmployeeName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["EmployeeName"].ReadOnly = true;
            GV.Columns["EmployeeName"].Frozen = true;

            GV.Columns["TeamID"].Width = 90;
            GV.Columns["TeamID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["TeamID"].ReadOnly = true;
            GV.Columns["TeamID"].Frozen = true;

            GV.Columns["BeginTime"].Width = 80;
            GV.Columns["BeginTime"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns["EndTime"].Width = 80;
            GV.Columns["EndTime"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns["OffTime"].Width = 80;
            GV.Columns["OffTime"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns["TotalTime"].Width = 80;
            GV.Columns["TotalTime"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns["DifferentTime"].Width = 100;
            GV.Columns["DifferentTime"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns["OverTime"].Width = 80;
            GV.Columns["OverTime"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV.Columns["OverTime"].ReadOnly = true;

            GV.Columns["OverTimeBegin"].Visible = false;
            GV.Columns["OverTimeEnd"].Visible = false;

            GV.Columns["Money"].Width = 80;
            GV.Columns["Money"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV.Columns["Money"].ReadOnly = true;

            GV.Columns["Amount"].Width = 80;
            GV.Columns["Amount"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV.Columns["Amount"].ReadOnly = true;

            GV.Columns["Time_TeamMain"].Width = 80;
            GV.Columns["Time_TeamMain"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns["Message"].Width = 150;
            GV.Columns["Message"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.SelectionMode = DataGridViewSelectionMode.CellSelect;
            GV.AllowUserToAddRows = true;
            GV.ColumnHeadersHeight = 40;

            GV.EditingControlShowing += GV_EditingControlShowing;
            GV.CellEndEdit += GV_CellEndEdit;
            GV.RowsAdded += GV_RowsAdded;
            GV.RowLeave += GV_RowLeave;
            GV.RowEnter += GV_RowEnter;
            GV.CellClick += GV_CellClick;
            GV.KeyUp += GV_KeyUp;
        }

        private void GV_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {

        }

        private void GV_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            GVData.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LemonChiffon;
        }

        private void GV_RowLeave(object sender, DataGridViewCellEventArgs e)
        {
            GVData.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.White;
        }

        private void GV_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //GVData.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LemonChiffon;
        }

        private void GV_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                if (Utils.TNMessageBox("Bạn muốn xóa dòng này !.", 2) == "Y")
                {
                    int no = GVData.CurrentRow.Index;
                    if (GVData.Rows[no].Cells["No"].Tag == null)
                    {
                        DeleteGridviewRecord(GVData.Rows[no]);
                    }
                    else
                    {
                        Temp_Import_Detail_Info zDetail = new Temp_Import_Detail_Info();
                        zDetail.Key = GVData.Rows[no].Cells["No"].Tag.ToInt();
                        zDetail.RecordStatus = 99;
                        zDetail.Delete();

                        DeleteGridviewRecord(GVData.Rows[no]);
                    }
                }
            }
        }

        private void LoadDataGV(DataTable InTable)
        {
            GVData.Rows.Clear();
            for (int i = 0; i < InTable.Rows.Count; i++)
            {
                DataRow zRow = InTable.Rows[i];
                GVData.Rows.Add();
                GVData.Rows[i].Cells["No"].Value = (i + 1).ToString();
                GVData.Rows[i].Cells["No"].Tag = zRow["AutoKey"].ToString();
                GVData.Rows[i].Cells["EmployeeName"].Value = zRow["EmployeeName"].ToString();
                GVData.Rows[i].Cells["EmployeeID"].Value = zRow["EmployeeID"].ToString();
                GVData.Rows[i].Cells["TeamID"].Value = zRow["TeamID"].ToString();
                if (zRow["BeginTime"].ToString() == "")
                {
                    GVData.Rows[i].Cells["BeginTime"].Value = 0;
                    GVData.Rows[i].Cells["BeginTime"].Tag = 0;
                }
                else
                {
                    GVData.Rows[i].Cells["BeginTime"].Tag = zRow["NBeginTime"].ToString();
                    GVData.Rows[i].Cells["BeginTime"].Value = zRow["BeginTime"].ToString();
                }
                if (zRow["EndTime"].ToString() == "")
                {
                    GVData.Rows[i].Cells["EndTime"].Tag = 0;
                    GVData.Rows[i].Cells["EndTime"].Value = 0;
                }
                else
                {
                    GVData.Rows[i].Cells["EndTime"].Tag = zRow["NEndTime"].ToString();
                    GVData.Rows[i].Cells["EndTime"].Value = zRow["EndTime"].ToString();
                }
                if (zRow["OffTime"].ToString() == "")
                {
                    GVData.Rows[i].Cells["OffTime"].Tag = 0;
                    GVData.Rows[i].Cells["OffTime"].Value = 0;
                }
                else
                {
                    GVData.Rows[i].Cells["OffTime"].Tag = zRow["NOffTime"].ToString();
                    GVData.Rows[i].Cells["OffTime"].Value = zRow["OffTime"].ToString();
                }
                if (zRow["DifferentTime"].ToString() == "")
                {
                    GVData.Rows[i].Cells["DifferentTime"].Tag = 0;
                    GVData.Rows[i].Cells["DifferentTime"].Value = 0;
                }
                else
                {
                    GVData.Rows[i].Cells["DifferentTime"].Tag = zRow["NDifferentTime"].ToString();
                    GVData.Rows[i].Cells["DifferentTime"].Value = zRow["DifferentTime"].ToString();
                }
                if (zRow["TotalTime"].ToString() == "")
                {
                    GVData.Rows[i].Cells["TotalTime"].Tag = 0;
                    GVData.Rows[i].Cells["TotalTime"].Value = 0;
                }
                else
                {
                    GVData.Rows[i].Cells["TotalTime"].Tag = zRow["NTotalTime"].ToString();
                    GVData.Rows[i].Cells["TotalTime"].Value = zRow["TotalTime"].ToString();
                }
                if (zRow["OverTime"].ToString() == "")
                {
                    GVData.Rows[i].Cells["OverTime"].Tag = 0;
                    GVData.Rows[i].Cells["OverTime"].Value = 0;
                }
                else
                {
                    GVData.Rows[i].Cells["OverTime"].Tag = zRow["NOverTime"].ToString();
                    GVData.Rows[i].Cells["OverTime"].Value = zRow["OverTime"].ToString();
                }
                GVData.Rows[i].Cells["OverTimeBegin"].Value = zRow["OverTimeBegin"].ToString();
                GVData.Rows[i].Cells["OverTimeEnd"].Value = zRow["OverTimeEnd"].ToString();
                float zAmount = 0;
                if (float.TryParse(zRow["Amount"].ToString(), out zAmount))
                {

                }
                GVData.Rows[i].Cells["Amount"].Value = zAmount.ToString("n0");
                float zMoney = 0;
                if (float.TryParse(zRow["Money"].ToString(), out zMoney))
                {

                }
                GVData.Rows[i].Cells["Money"].Value = zMoney.ToString("n0");
                float zTime_TeamMain = 0;
                if (float.TryParse(zRow["Time_TeamMain"].ToString(), out zTime_TeamMain))
                {

                }
                if (zTime_TeamMain == 0)
                    GVData.Rows[i].Cells["Time_TeamMain"].Value = "0";
                else
                    GVData.Rows[i].Cells["Time_TeamMain"].Value = zTime_TeamMain.ToString("n1");
                GVData.Rows[i].Cells["Message"].Tag = 0;
            }

        }
        private void GV_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            DataGridViewTextBoxEditingControl te = (DataGridViewTextBoxEditingControl)e.Control;
            te.AutoCompleteMode = AutoCompleteMode.None;
            switch (GVData.CurrentCell.ColumnIndex)
            {
                case 1: // autocomplete for product
                    if (LVFile.SelectedItems.Count > 0)
                    {
                        int TeamKey = LVFile.SelectedItems[0].Tag.ToInt();
                        LoadDataToToolbox.AutoCompleteTextBox(te, "SELECT EmployeeID FROM HRM_Employee WHERE TeamKey = " + TeamKey + " AND RecordStatus <> 99  ");
                    }
                    else
                    {
                        Utils.TNMessageBoxOK("Bạn phải chọn tổ nhóm", 1);
                    }
                    e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);
                    break;
                case 4:
                    e.Control.KeyPress += new KeyPressEventHandler(ControlNumber_KeyPress);
                    break;
                case 5:
                    e.Control.KeyPress += new KeyPressEventHandler(ControlNumber_KeyPress);
                    break;
                case 6:
                    e.Control.KeyPress += new KeyPressEventHandler(ControlNumber_KeyPress);
                    break;
                case 7:
                    e.Control.KeyPress += new KeyPressEventHandler(ControlNumber_KeyPress);
                    break;
                case 14: // giờ tại tổ
                    e.Control.KeyPress += new KeyPressEventHandler(ControlNumber_KeyPress);
                    break;

                default:
                    e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);

                    break;
            }
        }
        private void GV_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow zRowEdit = GVData.Rows[e.RowIndex];
            switch (e.ColumnIndex)
            {
                case 1:
                    if (zRowEdit.Cells[e.ColumnIndex].Value != null)
                    {
                        string zEmployeeID = zRowEdit.Cells["EmployeeID"].Value.ToString().Trim();
                        Employee_Info zEmployee = new Employee_Info();
                        zEmployee.GetEmployeeID(zEmployeeID);
                        if (zEmployee.EmployeeID.Trim().Length > 0)
                        {
                            zRowEdit.Cells["EmployeeName"].Value = zEmployee.FullName;
                            zRowEdit.Cells["TeamID"].Value = LVFile.SelectedItems[0].SubItems[1].Text;
                            zRowEdit.Cells["Amount"].Value = _Sotien.ToString("n0");
                        }
                        else
                        {
                            Utils.TNMessageBoxOK("Chưa có mã công nhân này !.", 1);
                        }
                    }
                    break;
                case 4: // giờ bđ
                    if (zRowEdit.Cells[e.ColumnIndex].Value != null)
                    {
                        zRowEdit.Cells[e.ColumnIndex].Tag = zRowEdit.Cells[e.ColumnIndex].Value.ToString();// he so gio
                    }
                    else
                    {
                        zRowEdit.Cells[e.ColumnIndex].Tag = "0";
                    }
                    break;
                case 5: // giờ kt
                    if (zRowEdit.Cells[e.ColumnIndex].Value != null)
                    {
                        zRowEdit.Cells[e.ColumnIndex].Tag = zRowEdit.Cells[e.ColumnIndex].Value.ToString();// he so gio
                    }
                    else
                    {
                        zRowEdit.Cells[e.ColumnIndex].Tag = "0";
                    }
                    break;
                case 6: // giờ nghi
                    if (zRowEdit.Cells[e.ColumnIndex].Value != null)
                    {
                        zRowEdit.Cells[e.ColumnIndex].Tag = zRowEdit.Cells[e.ColumnIndex].Value.ToString();// he so gio
                    }
                    else
                    {
                        zRowEdit.Cells[e.ColumnIndex].Tag = "0";
                    }
                    break;
                case 7: // giờ khác
                    if (zRowEdit.Cells[e.ColumnIndex].Value != null)
                    {
                        zRowEdit.Cells[e.ColumnIndex].Tag = zRowEdit.Cells[e.ColumnIndex].Value.ToString();// he so gio
                    }
                    else
                    {
                        zRowEdit.Cells[e.ColumnIndex].Tag = "0";
                    }
                    break;
                case 8: // tong giờ
                    if (zRowEdit.Cells[e.ColumnIndex].Value != null)
                    {
                        zRowEdit.Cells[e.ColumnIndex].Tag = zRowEdit.Cells[e.ColumnIndex].Value.ToString();// he so gio
                    }
                    else
                    {
                        zRowEdit.Cells[e.ColumnIndex].Tag = "0";
                    }
                    break;
                case 9: // tong giờ
                    if (zRowEdit.Cells[e.ColumnIndex].Value != null)
                    {
                        zRowEdit.Cells[e.ColumnIndex].Tag = zRowEdit.Cells[e.ColumnIndex].Value.ToString();// he so gio                                                                                                          
                    }
                    else
                    {
                        zRowEdit.Cells[e.ColumnIndex].Tag = "0";
                    }
                    break;
                case 14: // Giờ tại tổ
                    if (zRowEdit.Cells[e.ColumnIndex].Value != null)
                    {
                        zRowEdit.Cells[e.ColumnIndex].Tag = zRowEdit.Cells[e.ColumnIndex].Value.ToString();// he so gio
                        float zTemp = 0;
                        if (float.TryParse(zRowEdit.Cells[e.ColumnIndex].Value.ToString(), out zTemp))
                        {

                        }
                        if (zTemp == 0)
                            zRowEdit.Cells[e.ColumnIndex].Value = "0";
                        else
                            zRowEdit.Cells[e.ColumnIndex].Value = zTemp.ToString("n1");
                    }
                    else
                    {
                        zRowEdit.Cells[e.ColumnIndex].Tag = "0";
                    }
                    break;
            }
            if (CheckGridviewRow(GVData.Rows[e.RowIndex]).Length <= 0)
            {
                Calculator(zRowEdit);
            }
        }
        private void ControlNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }

            if (e.KeyChar == '.')
            {
                e.Handled = false;
            }
            if (e.KeyChar == ',')
            {
                e.Handled = false;
            }
        }
        private void Control_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = false;
        }
        #endregion

        #region[Event]
        private void MonthCalendar_DateSelected(object sender, DateRangeEventArgs e)
        {
            if (LVFile.SelectedItems.Count > 0)
            {
                _ExcelDate = MonthCalendar.SelectionStart.Date;
                txtSubTitle.Text = "Chi tiết thời gian nhóm công nhân làm việc trong ngày: " + _ExcelDate.ToString("dd/MM/yyyy");
                ShowDataGV(LVFile.SelectedItems[0].Tag.ToInt(), _ExcelDate);
            }
        }
        private void Btn_New_Click(object sender, EventArgs e)
        {
            OpenFileDialog zOpf = new OpenFileDialog();

            zOpf.InitialDirectory = @"C:\";
            zOpf.DefaultExt = "txt";
            zOpf.Title = "Browse Excel Files";
            zOpf.Filter = "All Files|*.*";
            zOpf.FilterIndex = 2;
            zOpf.CheckFileExists = true;
            zOpf.CheckPathExists = true;
            zOpf.RestoreDirectory = true;
            zOpf.ReadOnlyChecked = true;
            zOpf.ShowReadOnly = true;

            if (zOpf.ShowDialog() == DialogResult.OK)
            {
                _FileInfo = new FileInfo(zOpf.FileName);
                bool zcheck = Data_Access.CheckFileStatus(_FileInfo);
                if (zcheck == true)
                {
                    Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác. Vui lòng đóng file để tải lên!", 2);
                }
                else
                {
                    try
                    {
                        using (Frm_Loading frm = new Frm_Loading(ProcessData)) { frm.ShowDialog(this); }

                        string Path = @"C:\Loi_Excel_Chua_Import\";
                        if (Directory.Exists(Path))
                        {
                            Utils.TNMessageBoxOK("Có lỗi tập tin Excel, click OK để mở thư mục chứa !.", 2);
                            Process.Start(Path);
                        }
                    }
                    catch (Exception ex)
                    {
                        Utils.TNMessageBoxOK(ex.ToString(), 4);
                        btn_Save.Enabled = false;
                        _txtLog += "Lỗi.Xử lý Import" + Environment.NewLine;
                    }
                    if (_txtLog != "")
                    {
                        txtLog.Text = _txtLog;
                        txtLog.ScrollToCaret();
                        Panel_Done.Visible = true;
                        btn_Log.Tag = 1;
                    }
                }
            }
        }
        private void btn_Save_Click(object sender, EventArgs e)
        {
            if (GVData.Rows.Count > 0)
            {
                if (SessionUser.Date_Lock >= _ExcelDate)
                {
                    Utils.TNMessageBoxOK("Thời gian này đã khóa sổ.!", 2);
                    return;
                }

                _CheckErr = 0;
                this.Cursor = Cursors.WaitCursor;
                SaveDataGridView();
                if (_CheckErr == 0)
                {
                    Utils.TNMessageBoxOK("Cập nhật dữ liêu thành công", 3);
                    _CheckErr = 0;
                }
                else
                {
                    Utils.TNMessageBoxOK("Đã cập nhật còn " + _CheckErr + " thông tin cần kiểm tra lại !.", 3);
                }

                this.Cursor = Cursors.Default;
            }
        }
        #endregion

        #region[Process Time]
        private string GetTime(string Time)
        {
            float zTime = 0;
            string zResult = "";
            float Hour = 0;
            float Minute = 0;
            if (float.TryParse(Time, out zTime))
            {

            }
            Minute = zTime % 1;
            Hour = zTime - Minute;
            float temp = float.Parse(Minute.ToString("n2"));

            foreach (DataRow r in _TableTime.Rows)
            {
                float from = 0;
                if (float.TryParse(r[0].ToString(), out from))
                {

                }
                float to = 0;
                if (float.TryParse(r[1].ToString(), out to))
                {

                }
                float min = 0;
                if (float.TryParse(r[2].ToString(), out min))
                {

                }
                if (temp >= from && temp <= to)
                {
                    float ztam = ((float)95 / (float)100); //lấy số 0.95 đúng định dạng
                    if (temp < ztam)
                    {
                        zResult = Hour.ToString().PadLeft(2, '0') + ":" + min.ToString().PadLeft(2, '0');
                    }
                    else
                    {
                        int zHour = Hour.ToInt() + 1;
                        zResult = zHour.ToString().PadLeft(2, '0') + ":00";
                    }
                    break;
                }
            }

            return zResult;
        }
        private void LoadTableTime()
        {
            string File = Application.StartupPath + "\\wfTime.xlsx";
            _TableTime = Data_Access.GetTable(File, "1");
        }
        #endregion

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                //this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        //-------------------------------------Help function process excel file
        void AddRowError(DataRow zRow, string Message)
        {
            _CheckErr++;
            DataRow r = _TableErr.NewRow();
            r["No"] = zRow[0].ToString().Trim();
            r["HoVaTen"] = zRow[1].ToString().Trim();
            r["MaThe"] = zRow[2].ToString().Trim();
            r["MaNhom"] = zRow[3].ToString().Trim();
            r["GioBD"] = zRow[4].ToString().Trim();
            r["GioKT"] = zRow[5].ToString().Trim();
            r["GioNghi"] = zRow[6].ToString().Trim();
            r["GioCVKhac"] = zRow[7].ToString().Trim();
            r["TongGio"] = zRow[8].ToString().Trim();
            r["GioDu"] = zRow[9].ToString().Trim();
            r["NgayQuaDemTu"] = zRow[10].ToString().Trim();
            r["NgayQuaDemDen"] = zRow[11].ToString().Trim();
            r["GioTaiTo"] = zRow[12].ToString().Trim();
            r["ThongBao"] = Message;
            _TableErr.Rows.Add(r);
        }
        string CheckExcelRow(DataRow zRow,DateTime zDateWrite)
        {
            string zMessage = "";

            string zEmployeeName = zRow[1].ToString().Trim();
            string zEmployeeID = "";

            zEmployeeID = zRow[2].ToString().Trim();
            string zTeamID = "";

            zTeamID = zRow[3].ToString().Trim();

            string zOverTimeBegin = "";
            zOverTimeBegin = zRow[10].ToString().Trim();
            string zOverTimeEnd = "";
            zOverTimeBegin = zRow[11].ToString().Trim();

            if (SessionUser.Date_Lock >= zDateWrite)
            {
                zMessage += "\n Stt:" + (zRow[0].ToString().Trim()) + ";Thời gian này đã khóa sổ.!";
            }

            Employee_Info zEmployee = new Employee_Info();
            zEmployee.GetEmployeeID(zEmployeeID);
            if (zEmployeeID == "")
            {
                zMessage += "\n Stt:" + (zRow[0].ToString().Trim()) + ";Lỗi!Chưa nhập mã thẻ";
            }
            else if (zEmployee.Key == "")
            {
                zMessage += "\n Stt:" + (zRow[0].ToString().Trim()) + ";Lỗi!Mã thẻ:" + zEmployeeID + "không tồn tại";
            }
            else
            {
                if (zEmployee.TeamKey == 0)
                    zMessage += "\n Stt:" + (zRow[0].ToString().Trim()) + ";Lỗi!Mã thẻ:" + zEmployeeID + "chưa thuộc nhóm nào";
            }

            Team_Info zTeam = new Team_Info();
            zTeam.Get_Team_ID(zTeamID);
            if (zTeam.Key == 0)
            {
                zMessage += "\n Stt:" + (zRow[0].ToString().Trim()) + ";Lỗi!Mã nhóm:" + zTeamID + "không tồn tại";
            }
            if (zEmployee.Key == "" && zTeam.Key == 0)
            {
                zMessage += "\n Stt:" + (zRow[0].ToString().Trim()) + ";Dòng trống!Không import chỉ cảnh báo";
            }
            if (zOverTimeBegin != "")
            {
                DateTime OverTimeBegin; ;
                if (!DateTime.TryParse(zOverTimeBegin, out OverTimeBegin))
                {
                    zMessage += "\n Stt:" + (zRow[0].ToString().Trim()) + ";Lỗi!Ngày qua đêm từ:" + zOverTimeBegin + "không đúng định dạng";
                }
            }
            if (zOverTimeEnd != "")
            {
                DateTime OverTimeEnd;
                if (!DateTime.TryParse(zOverTimeEnd, out OverTimeEnd))
                {
                    zMessage += "\n Stt:" + (zRow[0].ToString().Trim()) + ";Lỗi!Ngày qua đêm đến:" + zOverTimeBegin + "không đúng định dạng";
                }
            }

            return zMessage;
        }
        string ExportTableToExcel(DataTable zTable, string Folder)
        {
            try
            {
                var newFile = new FileInfo(Folder);
                if (newFile.Exists)
                {
                    if (!IsFileLocked(newFile))
                    {
                        newFile.Delete();
                    }
                    else
                    {
                        return "Tập tin đang sử dụng !.";
                    }
                }
                using (var package = new ExcelPackage(newFile))
                {
                    //Tạo Sheet 1 mới với tên Sheet là DonHang
                    ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Sheet1");
                    worksheet.Cells["A1"].LoadFromDataTable(zTable, true);
                    worksheet.Cells.Style.Font.Name = "Times New Roman";
                    worksheet.Cells.Style.Font.Size = 12;
                    // worksheet.Cells.AutoFilter = true;
                    worksheet.Cells.AutoFitColumns();
                    // worksheet.View.FreezePanes(2, 4);

                    //Rowheader và row tổng
                    worksheet.Row(1).Height = 30;
                    worksheet.Row(1).Style.WrapText = true;
                    worksheet.Row(1).Style.Font.Bold = true;
                    worksheet.Row(1).Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                    worksheet.Row(1).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    //worksheet.Row(zTable.Rows.Count + 1).Style.Font.Bold = true;//dòng tổng
                    //Chỉnh fix độ rộng cột
                    //for (int j = 4; j <= zTable.Columns.Count; j++)
                    //{
                    //    worksheet.Column(j).Width = 15;
                    //}
                    //worksheet.Column(1).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    //worksheet.Column(1).Width = 5;
                    //worksheet.Column(2).Width = 30;
                    //worksheet.Column(3).Width = 15;
                    //worksheet.Column(3).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    //worksheet.Column(zTable.Columns.Count).Style.Font.Bold = true;//côt tổng
                    //Canh phải số
                    //for (int i = 2; i <= zTable.Rows.Count + 1; i++)
                    //{
                    //    for (int j = 4; j <= zTable.Columns.Count; j++)
                    //    {
                    //        worksheet.Cells[i, j].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    //    }
                    //}
                    //Lưu file Excel
                    package.SaveAs(newFile);
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
            return "OK";
        }
        protected virtual bool IsFileLocked(FileInfo file)
        {
            try
            {
                using (FileStream stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    stream.Close();
                }
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }

            //file is not locked
            return false;
        }

        //-------------------------------------Main process excel data file
        void ProcessData()
        {
            _txtLog = "";
            _txtLog += "---Bắt đầu : " + DateTime.Now.ToString("HH:mm") + " --//" + Environment.NewLine;
            ReadingExcel();
            this.Invoke(new MethodInvoker(delegate ()
            {
                ShowDataGV(0, _ExcelDate);
            }));
            _txtLog += "---Đã xong: " + DateTime.Now.ToString("HH:mm") + " --//" + Environment.NewLine;

        }
        void ReadingExcel()
        {
            DataTable TableSheet = Data_Access.GetSheet(_FileInfo.FullName);
            for (int i = 0; i < TableSheet.Rows.Count; i++)
            {
                string SheetNo = TableSheet.Rows[i][0].ToString();
                //kiem tra nếu sheet là số thì thực hiện xử lý dữ liệu
                int isNumber = 0;
                if (int.TryParse(SheetNo, out isNumber))
                    ReadingSheet(SheetNo);
            }
        }
        void ReadingSheet(string SheetNo)
        {
            _txtLog += "//-------Đang đọc sheet " + SheetNo + " --------//" + Environment.NewLine;
            ExcelPackage package = new ExcelPackage(_FileInfo);
            DataTable TableData = Data_Access.GetTable(_FileInfo.FullName, SheetNo);
            package.Dispose();
            if (TableData.Rows.Count > 0)
                SaveDataSheet(TableData, SheetNo);
            else
                _txtLog += "Sheet " + SheetNo + " không tìm có dữ liệu." + Environment.NewLine;
        }
        void SaveDataSheet(DataTable InTable, string SheetNo)
        {
            if (SheetNo.Length == 2)
            {
                string[] temp = _FileInfo.Name.Split('_');
                string year = temp[0].ToString();
                string month = temp[1].ToString();
                string day = SheetNo.ToString();

                DateTime zDate;
                if (DateTime.TryParse(new DateTime(year.ToInt(), month.ToInt(), day.ToInt(), 0, 0, 0).ToString(), out zDate))
                {
                    try
                    {
                        _txtLog += "Tổng dòng :" + (InTable.Rows.Count - 1) + " " + Environment.NewLine;
                        CreateTableError();
                        int zCountErr = 0;
                        int zCountSuccess = 0;
                        for (int i = 1; i < InTable.Rows.Count; i++)
                        {
                            DataRow zRow = InTable.Rows[i];
                            string MessageExcel = "";
                            MessageExcel = CheckExcelRow(zRow, zDate);
                            if (MessageExcel == string.Empty)
                            {
                                string MessageSQL = InsertToSQL(zRow, zDate);
                                if (MessageSQL != string.Empty)
                                {
                                    AddRowError(zRow, MessageSQL);
                                    zCountErr++;
                                }
                                else
                                {
                                    zCountSuccess++;
                                }
                            }
                            else
                            {
                                AddRowError(zRow, MessageExcel);
                                zCountErr++;
                            }
                        }
                        _txtLog += "Thành công : " + zCountSuccess.ToString() + " " + Environment.NewLine;
                        _txtLog += "Bị lỗi : " + zCountErr + " " + Environment.NewLine;
                        ExportTableError(zDate);
                    }
                    catch (Exception Ex)
                    {
                        _txtLog += "Lỗi.Vui lòng liên hệ IT." + Environment.NewLine;
                    }
                }
                else
                {
                    _txtLog += "Ngày [ " + day + " ]không có trong tháng tải lên!" + Environment.NewLine;
                }
            }
            else
            {
                _txtLog += "Sheet " + SheetNo + "> 2 kí tự không tải lên " + Environment.NewLine;
            }
        }
        string InsertToSQL(DataRow zRow, DateTime SheetDate)
        {
            Temp_Import_Detail_Info zDetail;

            string zEmployeeID = zRow[2].ToString().Trim();
            zDetail = new Temp_Import_Detail_Info(zEmployeeID.ToUpper(), SheetDate);
            zDetail.DateImport = SheetDate;

            Employee_Info zEmployee = new Employee_Info();
            zEmployee.GetEmployeeID(zEmployeeID);
            zDetail.EmployeeKey = zEmployee.Key;
            zDetail.EmployeeID = zEmployeeID.ToUpper();
            zDetail.EmployeeName = zEmployee.FullName;

            //---
            int zTeamKey = Team_Data.GetTeamKeyCurrent(zEmployee.Key, SheetDate);
            Team_Info zTeam = new Team_Info(zTeamKey);
            //int zTeamKeyOption = Team_Employee_Month.OptionTeamKey(zEmployee.Key, SheetDate);
            //Team_Info zTeam = new Team_Info(zTeamKeyOption);
            zDetail.TeamKey = zTeam.Key;
            zDetail.TeamID = zTeam.TeamID;
            zDetail.TeamName = zTeam.TeamName;
            //---

            //string zTeamID = zRow[3].ToString().Trim();
            //Team_Info zTeam = new Team_Info();
            //zTeam.Get_Team_ID(zTeamID);
            //zDetail.TeamKey = zTeam.Key;
            //zDetail.TeamID = zTeamID;
            //zDetail.TeamName = zTeam.TeamName;


            if (zRow[4].ToString().Trim() != "")
            {
                float zNBeginTime = 0;
                if (float.TryParse(zRow[4].ToString().Trim(), out zNBeginTime))
                {

                }
                zDetail.NBeginTime = zNBeginTime;
                zDetail.BeginTime = GetTime(zRow[4].ToString().Trim());
            }

            if (zRow[5].ToString().Trim() != "")
            {
                float zNEndTime = 0;
                if (float.TryParse(zRow[5].ToString().Trim(), out zNEndTime))
                {

                }
                zDetail.NEndTime = zNEndTime;
                zDetail.EndTime = GetTime(zRow[5].ToString().Trim());
            }
            if (zRow[6].ToString().Trim() != "")
            {
                float zNOffTime = 0;
                if (float.TryParse(zRow[6].ToString().Trim(), out zNOffTime))
                {

                }
                zDetail.NOffTime = zNOffTime;
                zDetail.OffTime = GetTime(zRow[6].ToString().Trim());
            }
            if (zRow[7].ToString().Trim() != "")
            {
                float zNDifferentTime = 0;
                if (float.TryParse(zRow[7].ToString().Trim(), out zNDifferentTime))
                {

                }
                zDetail.NDifferentTime = zNDifferentTime;
                zDetail.DifferentTime = GetTime(zRow[7].ToString().Trim());
            }
            if (zRow[8].ToString().Trim() != "")
            {
                float zNTotalTime = 0;
                if (float.TryParse(zRow[8].ToString().Trim(), out zNTotalTime))
                {

                }
                zDetail.NTotalTime = zNTotalTime;
                zDetail.TotalTime = GetTime(zRow[8].ToString().Trim());
            }
            if (zRow[9].ToString().Trim() != "")
            {
                float zNOverTime = 0;
                if (float.TryParse(zRow[9].ToString().Trim(), out zNOverTime))
                {

                }
                zDetail.NOverTime = zNOverTime;
                zDetail.OverTime = GetTime(zRow[9].ToString().Trim());
            }

            if (zRow[10].ToString().Trim() == "")
            {
                zDetail.OverTimeBegin = DateTime.MinValue;
            }
            else
            {
                zDetail.OverTimeBegin = DateTime.Parse(zRow[10].ToString().Trim());
            }
            if (zRow[11].ToString().Trim() == "")
            {
                zDetail.OverTimeEnd = DateTime.MinValue;
            }
            else
            {
                zDetail.OverTimeEnd = DateTime.Parse(zRow[11].ToString().Trim());
            }
            if (zRow[12].ToString().Trim() != "")
            {
                float zTime_TeamMain = 0;
                if (float.TryParse(zRow[12].ToString().Trim(), out zTime_TeamMain))
                {

                }
                zDetail.Time_TeamMain = zTime_TeamMain;
            }
            if (zDetail.OverTimeBegin != DateTime.MinValue && zDetail.OverTimeEnd != DateTime.MinValue)
            {
                zDetail.Slug = 1;
            }
            else
            {
                zDetail.Slug = 0;
            }
            float zSotien = HoTroSanXuat.LayTienNgoaiGio(SheetDate);
            zDetail.Amount = zSotien;
            float zSunDay = HoTroSanXuat.LayHeSoChuNhat(SheetDate);    //hệ số chủ nhật
            float zHeSoLe = HoTroSanXuat.LayHeSoNgayLe(SheetDate); //Lấy hệ số lễ
                                                                   // tính tiền giờ dư
            float GioDu = 0;
            if (float.TryParse(zRow[9].ToString().Trim(), out GioDu))
            {

            }
            GioDu = float.Parse(GioDu.ToString("n2"));
            //string[] s = GioDu.ToString("n2").Split('.');

            float Hour = 0;
            float Minute = 0;
            Minute = GioDu % 1;
            Hour = GioDu - Minute;
            Minute = Minute * 100;

            double TienDu = 0;
            if (GioDu > 0 && Minute < 95)
            {
                TienDu = zSotien * GioDu;
                zDetail.Rate = 1;
                zDetail.Class = "TH";
            }
            else if (GioDu > 0 && Minute >= 95) // Nếu giờ dư được làm tròn
            {
                string zGioDuLamTron = GioDu.ToString("n0");

                TienDu = zSotien * float.Parse(zGioDuLamTron);
                zDetail.Rate = 1;
                zDetail.Class = "TH";
            }
            if (SheetDate.DayOfWeek == DayOfWeek.Sunday) //chủ nhật
            {
                TienDu = TienDu * zSunDay;

                zDetail.Rate = zSunDay;
                zDetail.Class = "CN";
            }
            if (zHeSoLe > 0) //lễ
            {
                TienDu = TienDu * zHeSoLe;

                zDetail.Rate = zHeSoLe;
                zDetail.Class = "LE";
            }
            zDetail.Money = TienDu;

            zDetail.CreatedBy = SessionUser.UserLogin.Key;
            zDetail.CreatedName = SessionUser.UserLogin.EmployeeName;
            zDetail.ModifiedBy = SessionUser.UserLogin.Key;
            zDetail.ModifiedName = SessionUser.UserLogin.EmployeeName;
            zDetail.Save();
            if (zDetail.Message.Substring(0, 2) == "11" ||
                zDetail.Message.Substring(0, 2) == "20")
            {
                return string.Empty;
            }
            else
            {
                return zDetail.Message;
            }

        }
        void ShowDataGV(int TeamKey, DateTime DateView)
        {
            DataTable zTable = Temp_Import_Detail_Data.ListDetail(TeamKey, DateView);
            LoadDataGV(zTable);
        }
        void ExportTableError(DateTime SheetDate)
        {
            string Path = @"C:\Loi_Excel_Chua_Import\";
            string FileName = SheetDate.ToString("yyyy_MM_dd") + "_Chưa_Import.xlsx";
            if (_TableErr.Rows.Count > 0)
            {

                if (!Directory.Exists(Path))
                {
                    Directory.CreateDirectory(Path);
                }
                Path += FileName;
                ExportTableToExcel(_TableErr, Path);
                _txtLog += "Xuất file Excel ngày [ " + SheetDate.ToString("dd") + " ] - " + _TableErr.Rows.Count + " lỗi" + Environment.NewLine;
            }
            else
            {
                var newFile = new FileInfo(Path + FileName);
                //if (Directory.Exists(Path+FileName))
                //{
                //Directory.Delete(Path+FileName);
                //}

                if (newFile.Exists)
                {
                    if (!IsFileLocked(newFile))
                    {
                        newFile.Delete();
                    }
                    else
                    {
                        _txtLog += "Tệp tin " + FileName + " đang sử dụng không thể xóa! " + Environment.NewLine;
                    }
                }
            }
        }

        //-------------------------------------Main process SQL data
        void SaveDataGridView()
        {
            for (int i = 0; i < GVData.Rows.Count; i++)
            {
                DataGridViewRow zRow = GVData.Rows[i];
                zRow.DefaultCellStyle.ForeColor = Color.Navy;
                if (zRow.Cells["Message"].Tag != null &&
                    zRow.Cells["Message"].Tag.ToInt() == 1)
                {
                    string Message = CheckGridviewRow(zRow);
                    if (Message == string.Empty)
                    {
                        Message = UpdateGridviewRecord(zRow);
                        //if (Message == string.Empty)
                        //    DeleteGridviewRecord(zRow);
                    }
                }
            }
        }
        string CheckGridviewRow(DataGridViewRow zRow)
        {
            string zMessage = "";

            zRow.DefaultCellStyle.ForeColor = Color.Navy;
            //string zEmployeeName = zRow.Cells["EmployeeName"].Value.ToString();
            string zEmployeeID = "";
            if (zRow.Cells["EmployeeID"].Value != null)
            {
                zEmployeeID = zRow.Cells["EmployeeID"].Value.ToString();
            }
            string zTeamID = "";
            if (zRow.Cells["TeamID"].Value != null)
            {
                zTeamID = zRow.Cells["TeamID"].Value.ToString();
            }

            string zOverTimeBegin = "";
            if (zRow.Cells["OverTimeBegin"].Value != null)
            {
                zOverTimeBegin = zRow.Cells["OverTimeBegin"].Value.ToString();
            }
            string zOverTimeEnd = "";
            if (zRow.Cells["OverTimeEnd"].Value != null)
            {
                zOverTimeBegin = zRow.Cells["OverTimeEnd"].Value.ToString();
            }

            Employee_Info zEmployee = new Employee_Info();
            zEmployee.GetEmployeeID(zEmployeeID);
            if (zEmployeeID == "")
            {
                zMessage += "\n Stt:" + (zRow.Cells["No"].Value) + ";Lỗi!Chưa nhập mã thẻ";
            }
            else if (zEmployee.Key == "")
            {
                zMessage += "\n Stt:" + (zRow.Cells["No"].Value) + ";Lỗi!Mã thẻ:" + zEmployeeID + "không tồn tại";
            }
            else
            {
                if (zEmployee.TeamKey == 0)
                    zMessage += "\n Stt:" + (zRow.Cells["No"].Value) + ";Lỗi!Mã thẻ:" + zEmployeeID + "chưa thuộc nhóm nào";
            }

            Team_Info zTeam = new Team_Info();
            zTeam.Get_Team_ID(zTeamID);
            if (zTeam.Key == 0)
            {
                zMessage += "\n Stt:" + (zRow.Cells["No"].Value) + ";Lỗi!Mã nhóm:" + zTeamID + "không tồn tại";
            }

            if (zOverTimeBegin != "")
            {
                DateTime OverTimeBegin; ;
                if (!DateTime.TryParse(zOverTimeBegin, out OverTimeBegin))
                {
                    zMessage += "\n Stt:" + (zRow.Cells["No"].Value) + ";Lỗi!Ngày qua đêm từ:" + zOverTimeBegin + "không đúng định dạng";
                }
            }
            if (zOverTimeEnd != "")
            {
                DateTime OverTimeEnd;
                if (!DateTime.TryParse(zOverTimeEnd, out OverTimeEnd))
                {
                    zMessage += "\n Stt:" + (zRow.Cells["No"].Value) + ";Lỗi!Ngày qua đêm đến:" + zOverTimeBegin + "không đúng định dạng";
                }
            }
            if (zMessage == "")
            {
                zRow.Cells["Message"].Tag = 1;
                zRow.Cells["Message"].Value = "";
            }
            else
            {
                zRow.Cells["Message"].Tag = null;
                zRow.DefaultCellStyle.ForeColor = Color.Red;
                zRow.Cells["Message"].Value = zMessage;
            }
            return zMessage;
        }
        string UpdateGridviewRecord(DataGridViewRow zRow)
        {
            Temp_Import_Detail_Info zDetail = new Temp_Import_Detail_Info(zRow.Cells["No"].Tag.ToInt());

            zDetail.DateImport = _ExcelDate;
            string zEmployeeID = zRow.Cells["EmployeeID"].Value.ToString().Trim();
            Employee_Info zEmployee = new Employee_Info();
            zEmployee.GetEmployeeID(zEmployeeID);
            zDetail.EmployeeKey = zEmployee.Key;
            zDetail.EmployeeID = zEmployeeID;
            zDetail.EmployeeName = zRow.Cells["EmployeeName"].Value.ToString().Trim();

            string zTeamID = zRow.Cells["TeamID"].Value.ToString();
            Team_Info zTeam = new Team_Info();
            zTeam.Get_Team_ID(zTeamID);
            zDetail.TeamKey = zTeam.Key;
            zDetail.TeamID = zTeamID;
            zDetail.TeamName = zTeam.TeamName;

            if (zRow.Cells["BeginTime"].Value == null || zRow.Cells["BeginTime"].Value.ToString() == "")
            {
                zDetail.NBeginTime = 0;
                zDetail.BeginTime = "00:00";
            }
            else
            {
                zDetail.NBeginTime = float.Parse(zRow.Cells["BeginTime"].Tag.ToString());
                zDetail.BeginTime = zRow.Cells["BeginTime"].Value.ToString().Trim();
            }
            if (zRow.Cells["EndTime"].Value == null || zRow.Cells["EndTime"].Value.ToString() == "")
            {
                zDetail.NEndTime = 0;
                zDetail.EndTime = "00:00";
            }
            else
            {
                zDetail.NEndTime = float.Parse(zRow.Cells["EndTime"].Tag.ToString());
                zDetail.EndTime = zRow.Cells["EndTime"].Value.ToString();
            }
            if (zRow.Cells["OffTime"].Value == null || zRow.Cells["OffTime"].Value.ToString() == "")
            {
                zDetail.NOffTime = 0;
                zDetail.OffTime = "00:00";
            }
            else
            {
                zDetail.NOffTime = float.Parse(zRow.Cells["OffTime"].Tag.ToString());
                zDetail.OffTime = zRow.Cells["OffTime"].Value.ToString();
            }
            if (zRow.Cells["DifferentTime"].Value == null || zRow.Cells["DifferentTime"].Value.ToString() == "")
            {
                zDetail.NDifferentTime = 0;
                zDetail.DifferentTime = "00:00";
            }
            else
            {
                zDetail.NDifferentTime = float.Parse(zRow.Cells["DifferentTime"].Tag.ToString());
                zDetail.DifferentTime = zRow.Cells["DifferentTime"].Value.ToString();
            }
            if (zRow.Cells["TotalTime"].Value == null || zRow.Cells["TotalTime"].Value.ToString() == "")
            {
                zDetail.NTotalTime = 0;
                zDetail.TotalTime = "00:00";
            }
            else
            {
                zDetail.NTotalTime = float.Parse(zRow.Cells["TotalTime"].Tag.ToString());
                zDetail.TotalTime = zRow.Cells["TotalTime"].Value.ToString();
            }
            if (zRow.Cells["OverTime"].Value == null || zRow.Cells["OverTime"].Value.ToString() == "")
            {
                zDetail.NOverTime = 0;
                zDetail.OverTime = "00:00";
            }
            else
            {
                zDetail.NOverTime = float.Parse(zRow.Cells["OverTime"].Tag.ToString());
                zDetail.OverTime = zRow.Cells["OverTime"].Value.ToString();
            }

            if (zRow.Cells["OverTimeBegin"].Value == null || zRow.Cells["OverTimeBegin"].Value.ToString().Trim() == "")
            {
                zDetail.OverTimeBegin = DateTime.MinValue;
            }
            else
            {
                zDetail.OverTimeBegin = DateTime.Parse(zRow.Cells["OverTimeBegin"].Value.ToString().Trim());
            }
            if (zRow.Cells["OverTimeEnd"].Value == null || zRow.Cells["OverTimeEnd"].Value.ToString().Trim() == "")
            {
                zDetail.OverTimeEnd = DateTime.MinValue;
            }
            else
            {
                zDetail.OverTimeEnd = DateTime.Parse(zRow.Cells["OverTimeEnd"].Value.ToString().Trim());
            }
            if (zRow.Cells["Time_TeamMain"].Value == null || zRow.Cells["Time_TeamMain"].Value.ToString() == "")
            {
                zDetail.Time_TeamMain = 0;
            }
            else
            {
                zDetail.Time_TeamMain = float.Parse(zRow.Cells["Time_TeamMain"].Value.ToString());
            }
            if (zDetail.OverTimeBegin != DateTime.MinValue && zDetail.OverTimeEnd != DateTime.MinValue)
            {
                zDetail.Slug = 1;
            }
            else
            {
                zDetail.Slug = 0;
            }
            zDetail.Amount = HoTroSanXuat.LayTienNgoaiGio(_ExcelDate);
            if (zRow.Cells["Money"].Value == null || zRow.Cells["Money"].Value.ToString() == "")
                zDetail.Money = 0;
            else
            {
                double zMoney = 0;
                if (double.TryParse(zRow.Cells["Money"].Value.ToString(), out zMoney))
                {

                }
                zDetail.Money = zMoney;
            }
            //lưu hệ số
            float zSunDay = HoTroSanXuat.LayHeSoChuNhat(zDetail.DateImport);    //hệ số chủ nhật
            float zHeSoLe = HoTroSanXuat.LayHeSoNgayLe(zDetail.DateImport); //Lấy hệ số lễ

            zDetail.Rate = 1;
            zDetail.Class = "TH";
            if (zDetail.DateImport.DayOfWeek == DayOfWeek.Sunday) //chủ nhật
            {
                zDetail.Rate = zSunDay;
                zDetail.Class = "CN";
            }
            if (zHeSoLe > 0) //lễ
            {
                zDetail.Rate = zHeSoLe;
                zDetail.Class = "LE";
            }

            zDetail.CreatedBy = SessionUser.UserLogin.Key;
            zDetail.CreatedName = SessionUser.UserLogin.EmployeeName;
            zDetail.ModifiedBy = SessionUser.UserLogin.Key;
            zDetail.ModifiedName = SessionUser.UserLogin.EmployeeName;
            zDetail.Save();
            if (zDetail.Message.Substring(0, 2) == "11" ||
                zDetail.Message.Substring(0, 2) == "20")
            {
                return string.Empty;
            }
            else
            {
                zRow.DefaultCellStyle.ForeColor = Color.Maroon;
                zRow.Cells["Message"].Value = zDetail.Message;
                return zDetail.Message;
            }
        }
        void DeleteGridviewRecord(DataGridViewRow zRow)
        {
            GVData.Rows.Remove(zRow);
        }
        //
        void Calculator(DataGridViewRow Row)
        {
            string Giobatdau = "00:00";
            string Gioketthuc = "00:00";
            string Gionghi = "00:00";
            string Giokhac = "00:00";
            float tonggio = 0;
            float sogiodu = 0;
            _Sotien = HoTroSanXuat.LayTienNgoaiGio(_ExcelDate);

            if (Row.Cells["BeginTime"].Tag != null)
            {
                Giobatdau = Row.Cells["BeginTime"].Tag.ToString();
                Row.Cells["BeginTime"].Value = GetTime(Giobatdau);
            }
            if (Row.Cells["EndTime"].Tag != null)
            {
                Gioketthuc = Row.Cells["EndTime"].Tag.ToString();
                Row.Cells["EndTime"].Value = GetTime(Gioketthuc);
            }
            if (Row.Cells["OffTime"].Tag != null)
            {
                Gionghi = Row.Cells["OffTime"].Tag.ToString();
                Row.Cells["OffTime"].Value = GetTime(Gionghi);
            }
            if (Row.Cells["DifferentTime"].Tag != null)
            {
                Giokhac = Row.Cells["DifferentTime"].Tag.ToString();
                Row.Cells["DifferentTime"].Value = GetTime(Giokhac);
            }
            Row.Cells["Amount"].Value = _Sotien.ToString("n0");
            float zSunDay = HoTroSanXuat.LayHeSoChuNhat(_ExcelDate);    //hệ số chủ nhật
            float zHeSoLe = HoTroSanXuat.LayHeSoNgayLe(_ExcelDate); //Lấy hệ số lễ
            if (Giobatdau != "0" || Gioketthuc != "0")
            {
                tonggio = float.Parse(Gioketthuc) - float.Parse(Giobatdau) - float.Parse(Gionghi) - float.Parse(Giokhac);
                sogiodu = 0;

                Row.Cells["TotalTime"].Tag = tonggio.ToString("n2");
                Row.Cells["TotalTime"].Value = GetTime(tonggio.ToString("n2"));

                //string[] s = tonggio.ToString("n2").Split('.');

                tonggio = float.Parse(tonggio.ToString("n2"));
                //string[] s = GioDu.ToString("n2").Split('.');

                float Hour = 0;
                float Minute = 0;
                Minute = tonggio % 1;
                Hour = tonggio - Minute;
                Minute = Minute * 100;
                if (tonggio > 8 && Minute < 95) //Nếu  giờ dư >=0.95 thì lam tron thanh 1
                {
                    sogiodu = tonggio - 8;
                    Row.Cells["OverTime"].Tag = sogiodu.ToString("n2");
                    Row.Cells["OverTime"].Value = GetTime(sogiodu.ToString());
                    if (_ExcelDate.DayOfWeek == DayOfWeek.Sunday) //chủ nhật
                    {
                        Row.Cells["Money"].Value = (sogiodu * _Sotien * zSunDay).ToString("n0");
                    }
                    else if (zHeSoLe > 0) //lễ
                    {
                        Row.Cells["Money"].Value = (sogiodu * _Sotien * zHeSoLe).ToString("n0");
                    }
                    else //thường
                    {
                        Row.Cells["Money"].Value = (sogiodu * _Sotien).ToString("n0");
                    }

                }
                else if (tonggio > 8 && Minute >= 95)
                {
                    sogiodu = tonggio - 8;
                    Row.Cells["OverTime"].Tag = sogiodu.ToString("n2");
                    Row.Cells["OverTime"].Value = GetTime(sogiodu.ToString("n2"));
                    if (_ExcelDate.DayOfWeek == DayOfWeek.Sunday) //chủ nhật
                    {
                        Row.Cells["Money"].Value = (float.Parse(sogiodu.ToString("n0")) * _Sotien * zSunDay).ToString("n0");
                    }
                    else if (zHeSoLe > 0) //lễ
                    {
                        Row.Cells["Money"].Value = (float.Parse(sogiodu.ToString("n0")) * _Sotien * zHeSoLe).ToString("n0");
                    }
                    else //thường
                    {
                        Row.Cells["Money"].Value = (float.Parse(sogiodu.ToString("n0")) * _Sotien).ToString("n0");
                    }

                }
                else
                {
                    Row.Cells["OverTime"].Tag = 0;
                    Row.Cells["OverTime"].Value = "00:00";
                    Row.Cells["Money"].Value = 0;
                }
            }
            else // trong hợp chỉ nhập tổng giờ
            {
                if (Row.Cells["TotalTime"].Tag != null)
                {
                    tonggio = 0;
                    if (float.TryParse(Row.Cells["TotalTime"].Tag.ToString(), out tonggio))
                    {

                    }
                    Row.Cells["TotalTime"].Value = GetTime(tonggio.ToString("n2"));
                    //string[] s = tonggio.ToString("n2").Split('.');
                    tonggio = float.Parse(tonggio.ToString("n2"));

                    float Hour = 0;
                    float Minute = 0;
                    Minute = tonggio % 1;
                    Hour = tonggio - Minute;
                    Minute = Minute * 100;
                    if (tonggio > 8 && Minute < 95) //Nếu  giờ dư >=0.95 thì lam tron thanh 1
                    {
                        sogiodu = tonggio - 8;
                        Row.Cells["OverTime"].Tag = sogiodu.ToString("n2");
                        Row.Cells["OverTime"].Value = GetTime(sogiodu.ToString());
                        if (_ExcelDate.DayOfWeek == DayOfWeek.Sunday) //chủ nhật
                        {
                            Row.Cells["Money"].Value = (sogiodu * _Sotien * zSunDay).ToString("n0");
                        }
                        else if (zHeSoLe > 0) //lễ
                        {
                            Row.Cells["Money"].Value = (sogiodu * _Sotien * zHeSoLe).ToString("n0");
                        }
                        else //thường
                        {
                            Row.Cells["Money"].Value = (sogiodu * _Sotien).ToString("n0");
                        }
                    }
                    else if (tonggio > 8 && Minute >= 95)
                    {
                        sogiodu = tonggio - 8;
                        Row.Cells["OverTime"].Tag = sogiodu.ToString("n2");
                        Row.Cells["OverTime"].Value = GetTime(sogiodu.ToString("n2"));
                        if (_ExcelDate.DayOfWeek == DayOfWeek.Sunday) //chủ nhật
                        {
                            float zGioDu = 0; //làm tròn
                            if (float.TryParse(sogiodu.ToString("n0"), out zGioDu))
                            {

                            }
                            Row.Cells["Money"].Value = (zGioDu * _Sotien * zSunDay).ToString("n0");
                        }
                        else if (zHeSoLe > 0) //lễ
                        {
                            float zGioDu = 0; //làm tròn
                            if (float.TryParse(sogiodu.ToString("n0"), out zGioDu))
                            {

                            }
                            Row.Cells["Money"].Value = (zGioDu * _Sotien * zHeSoLe).ToString("n0");
                        }
                        else //thường
                        {
                            float zGioDu = 0; //làm tròn
                            if (float.TryParse(sogiodu.ToString("n0"), out zGioDu))
                            {

                            }
                            Row.Cells["Money"].Value = (zGioDu * _Sotien).ToString("n0");
                        }
                    }
                    else
                    {
                        Row.Cells["OverTime"].Tag = 0;
                        Row.Cells["OverTime"].Value = "00:00";
                        Row.Cells["Money"].Value = 0;
                    }
                }
            }
        }
    }
}