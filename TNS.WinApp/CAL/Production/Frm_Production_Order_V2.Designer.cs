﻿namespace TNS.WinApp
{
    partial class Frm_Production_Order_V2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Production_Order_V2));
            this.HeaderControl = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btn_OpenSearch = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.buttonSpecAny6 = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnMini = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnMax = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnClose = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.Panel_Left = new System.Windows.Forms.Panel();
            this.GVOrder = new System.Windows.Forms.DataGridView();
            this.kryptonHeader3 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btn_Show = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btn_Hide = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.Panel_Info = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txt_WorkGroup = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txt_OrderIDFollow = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.btn_SearchInput_Document = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.txt_QuantityDocument = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.txt_QuantityReality = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txt_QuantityLose = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txt_Percent = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txt_Note = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.cbo_Team = new ComponentFactory.Krypton.Toolkit.KryptonComboBox();
            this.txt_WorkID = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.btn_SearchInput_Work = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.txt_ProductID = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.buttonSpecAny1 = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.txt_OrderIDCompare = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txt_OrderID = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txt_Unit = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txt_Price = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.dte_OrderDate = new TNS.SYS.TNDateTimePicker();
            this.cbo_Status = new ComponentFactory.Krypton.Toolkit.KryptonComboBox();
            this.txt_WorkName = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txt_ProductName = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.GVRate = new System.Windows.Forms.DataGridView();
            this.Panel_Employee = new System.Windows.Forms.Panel();
            this.Panel_Message = new System.Windows.Forms.Panel();
            this.lbl_Message = new System.Windows.Forms.Label();
            this.kryptonHeader2 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btnClose_Panel_Message = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.GVEmployee = new TN.Toolbox.TN_GridView();
            this.txt_title = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btn_SaveOrder = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_CopyOrder = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_NewOrder = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_DeleteOrder = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.txtHeader = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.Panel_Search = new System.Windows.Forms.Panel();
            this.btn_Export = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Search = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.cbo_TeamID_Search = new ComponentFactory.Krypton.Toolkit.KryptonComboBox();
            this.txt_Search_WorkName = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txt_Search_WorkID = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.btn_Search_Work = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btn_DeleteAll = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txt_Search_IDOrder = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txt_Search_IDOrder_Original = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txt_Search_ProductName = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txt_Search_ProductID = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.dte_FromDate = new TNS.SYS.TNDateTimePicker();
            this.dte_ToDate = new TNS.SYS.TNDateTimePicker();
            this.LV_Product = new System.Windows.Forms.ListView();
            this.Panel_Left.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GVOrder)).BeginInit();
            this.Panel_Info.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbo_Team)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo_Status)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVRate)).BeginInit();
            this.Panel_Employee.SuspendLayout();
            this.Panel_Message.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GVEmployee)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.Panel_Search.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbo_TeamID_Search)).BeginInit();
            this.SuspendLayout();
            // 
            // HeaderControl
            // 
            this.HeaderControl.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btn_OpenSearch,
            this.buttonSpecAny6,
            this.btnMini,
            this.btnMax,
            this.btnClose});
            this.HeaderControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.HeaderControl.Location = new System.Drawing.Point(0, 0);
            this.HeaderControl.Name = "HeaderControl";
            this.HeaderControl.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.HeaderControl.Size = new System.Drawing.Size(1366, 42);
            this.HeaderControl.TabIndex = 147;
            this.HeaderControl.Values.Description = "";
            this.HeaderControl.Values.Heading = "Thông tin đơn hàng sản xuất";
            this.HeaderControl.Values.Image = ((System.Drawing.Image)(resources.GetObject("HeaderControl.Values.Image")));
            this.HeaderControl.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Frm_Main_MouseDown);
            this.HeaderControl.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Frm_Main_MouseMove);
            this.HeaderControl.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Frm_Main_MouseUp);
            // 
            // btn_OpenSearch
            // 
            this.btn_OpenSearch.ExtraText = "Tìm kiếm";
            this.btn_OpenSearch.Image = ((System.Drawing.Image)(resources.GetObject("btn_OpenSearch.Image")));
            this.btn_OpenSearch.UniqueName = "F5BE53B92E534345508EE985BC35AA72";
            this.btn_OpenSearch.Click += new System.EventHandler(this.btn_OpenSearch_Click);
            // 
            // buttonSpecAny6
            // 
            this.buttonSpecAny6.Enabled = ComponentFactory.Krypton.Toolkit.ButtonEnabled.False;
            this.buttonSpecAny6.Style = ComponentFactory.Krypton.Toolkit.PaletteButtonStyle.Command;
            this.buttonSpecAny6.Text = "|";
            this.buttonSpecAny6.UniqueName = "BCC860D56C2641C57E8D31CB3379EE5D";
            // 
            // btnMini
            // 
            this.btnMini.Image = ((System.Drawing.Image)(resources.GetObject("btnMini.Image")));
            this.btnMini.UniqueName = "F5F06E8241504E72ABB5BEA3F6A9B753";
            this.btnMini.Click += new System.EventHandler(this.btnMini_Click);
            // 
            // btnMax
            // 
            this.btnMax.Image = ((System.Drawing.Image)(resources.GetObject("btnMax.Image")));
            this.btnMax.UniqueName = "035D1A4881E44F58A084C31DE7352A94";
            this.btnMax.Click += new System.EventHandler(this.btnMax_Click);
            // 
            // btnClose
            // 
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.UniqueName = "11B07C6F4E1C4F9D8B91BD924CB0EBE6";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // Panel_Left
            // 
            this.Panel_Left.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Left.Controls.Add(this.GVOrder);
            this.Panel_Left.Controls.Add(this.kryptonHeader3);
            this.Panel_Left.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel_Left.Location = new System.Drawing.Point(0, 0);
            this.Panel_Left.Name = "Panel_Left";
            this.Panel_Left.Size = new System.Drawing.Size(300, 627);
            this.Panel_Left.TabIndex = 222;
            // 
            // GVOrder
            // 
            this.GVOrder.AllowUserToAddRows = false;
            this.GVOrder.AllowUserToDeleteRows = false;
            this.GVOrder.AllowUserToResizeColumns = false;
            this.GVOrder.AllowUserToResizeRows = false;
            this.GVOrder.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.GVOrder.ColumnHeadersHeight = 25;
            this.GVOrder.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.GVOrder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GVOrder.Location = new System.Drawing.Point(0, 30);
            this.GVOrder.Name = "GVOrder";
            this.GVOrder.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GVOrder.Size = new System.Drawing.Size(300, 597);
            this.GVOrder.TabIndex = 220;
            // 
            // kryptonHeader3
            // 
            this.kryptonHeader3.AutoSize = false;
            this.kryptonHeader3.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btn_Show,
            this.btn_Hide});
            this.kryptonHeader3.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader3.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader3.Name = "kryptonHeader3";
            this.kryptonHeader3.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader3.Size = new System.Drawing.Size(300, 30);
            this.kryptonHeader3.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader3.TabIndex = 209;
            this.kryptonHeader3.Values.Description = "";
            this.kryptonHeader3.Values.Heading = "Các đơn hàng";
            // 
            // btn_Show
            // 
            this.btn_Show.Image = ((System.Drawing.Image)(resources.GetObject("btn_Show.Image")));
            this.btn_Show.UniqueName = "ABE0F8FEF4FF4DE3448D0BAE7F1C6F22";
            this.btn_Show.Click += new System.EventHandler(this.btn_Show_Click);
            // 
            // btn_Hide
            // 
            this.btn_Hide.Image = ((System.Drawing.Image)(resources.GetObject("btn_Hide.Image")));
            this.btn_Hide.UniqueName = "D6E0AB28C2CE49FBDD866175ABE4E274";
            this.btn_Hide.Click += new System.EventHandler(this.btn_Hide_Click);
            // 
            // Panel_Info
            // 
            this.Panel_Info.AutoScroll = true;
            this.Panel_Info.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Info.Controls.Add(this.panel1);
            this.Panel_Info.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel_Info.Location = new System.Drawing.Point(0, 0);
            this.Panel_Info.Name = "Panel_Info";
            this.Panel_Info.Size = new System.Drawing.Size(1062, 255);
            this.Panel_Info.TabIndex = 225;
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.txt_WorkGroup);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.txt_Note);
            this.panel1.Controls.Add(this.cbo_Team);
            this.panel1.Controls.Add(this.txt_WorkID);
            this.panel1.Controls.Add(this.txt_ProductID);
            this.panel1.Controls.Add(this.txt_OrderIDCompare);
            this.panel1.Controls.Add(this.txt_OrderID);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.txt_Unit);
            this.panel1.Controls.Add(this.txt_Price);
            this.panel1.Controls.Add(this.dte_OrderDate);
            this.panel1.Controls.Add(this.cbo_Status);
            this.panel1.Controls.Add(this.txt_WorkName);
            this.panel1.Controls.Add(this.txt_ProductName);
            this.panel1.Controls.Add(this.GVRate);
            this.panel1.Location = new System.Drawing.Point(5, 33);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1053, 205);
            this.panel1.TabIndex = 248;
            // 
            // txt_WorkGroup
            // 
            this.txt_WorkGroup.Location = new System.Drawing.Point(84, 140);
            this.txt_WorkGroup.Name = "txt_WorkGroup";
            this.txt_WorkGroup.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_WorkGroup.ReadOnly = true;
            this.txt_WorkGroup.Size = new System.Drawing.Size(133, 26);
            this.txt_WorkGroup.StateCommon.Border.ColorAngle = 1F;
            this.txt_WorkGroup.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_WorkGroup.StateCommon.Border.Rounding = 4;
            this.txt_WorkGroup.StateCommon.Border.Width = 1;
            this.txt_WorkGroup.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_WorkGroup.TabIndex = 322;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txt_OrderIDFollow);
            this.groupBox1.Controls.Add(this.txt_QuantityDocument);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label24);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label23);
            this.groupBox1.Controls.Add(this.txt_QuantityReality);
            this.groupBox1.Controls.Add(this.txt_QuantityLose);
            this.groupBox1.Controls.Add(this.txt_Percent);
            this.groupBox1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.groupBox1.Location = new System.Drawing.Point(571, 8);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 159);
            this.groupBox1.TabIndex = 324;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Chứng từ";
            // 
            // txt_OrderIDFollow
            // 
            this.txt_OrderIDFollow.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btn_SearchInput_Document});
            this.txt_OrderIDFollow.Location = new System.Drawing.Point(7, 16);
            this.txt_OrderIDFollow.Name = "txt_OrderIDFollow";
            this.txt_OrderIDFollow.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_OrderIDFollow.Size = new System.Drawing.Size(187, 26);
            this.txt_OrderIDFollow.StateCommon.Border.ColorAngle = 1F;
            this.txt_OrderIDFollow.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_OrderIDFollow.StateCommon.Border.Rounding = 4;
            this.txt_OrderIDFollow.StateCommon.Border.Width = 1;
            this.txt_OrderIDFollow.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_OrderIDFollow.TabIndex = 268;
            // 
            // btn_SearchInput_Document
            // 
            this.btn_SearchInput_Document.Image = ((System.Drawing.Image)(resources.GetObject("btn_SearchInput_Document.Image")));
            this.btn_SearchInput_Document.UniqueName = "FFABA6C89F274796F78435AB2589EE7D";
            // 
            // txt_QuantityDocument
            // 
            this.txt_QuantityDocument.Location = new System.Drawing.Point(106, 44);
            this.txt_QuantityDocument.Name = "txt_QuantityDocument";
            this.txt_QuantityDocument.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_QuantityDocument.Size = new System.Drawing.Size(88, 26);
            this.txt_QuantityDocument.StateCommon.Border.ColorAngle = 1F;
            this.txt_QuantityDocument.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_QuantityDocument.StateCommon.Border.Rounding = 4;
            this.txt_QuantityDocument.StateCommon.Border.Width = 1;
            this.txt_QuantityDocument.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_QuantityDocument.TabIndex = 271;
            this.txt_QuantityDocument.Text = "0";
            this.txt_QuantityDocument.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label8.Location = new System.Drawing.Point(4, 50);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(100, 15);
            this.label8.TabIndex = 264;
            this.label8.Text = "SL chứng từ (Kg)";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label24.Location = new System.Drawing.Point(12, 107);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(92, 15);
            this.label24.TabIndex = 266;
            this.label24.Text = "SL hao hụt (Kg)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label3.Location = new System.Drawing.Point(11, 135);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 15);
            this.label3.TabIndex = 265;
            this.label3.Text = "Hoàn thành (%)";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label23.Location = new System.Drawing.Point(16, 79);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(88, 15);
            this.label23.TabIndex = 267;
            this.label23.Text = "SL thực tế (Kg)";
            // 
            // txt_QuantityReality
            // 
            this.txt_QuantityReality.Location = new System.Drawing.Point(106, 72);
            this.txt_QuantityReality.Name = "txt_QuantityReality";
            this.txt_QuantityReality.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_QuantityReality.Size = new System.Drawing.Size(88, 26);
            this.txt_QuantityReality.StateCommon.Border.ColorAngle = 1F;
            this.txt_QuantityReality.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_QuantityReality.StateCommon.Border.Rounding = 4;
            this.txt_QuantityReality.StateCommon.Border.Width = 1;
            this.txt_QuantityReality.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_QuantityReality.TabIndex = 274;
            this.txt_QuantityReality.Text = "0";
            this.txt_QuantityReality.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txt_QuantityLose
            // 
            this.txt_QuantityLose.Location = new System.Drawing.Point(106, 100);
            this.txt_QuantityLose.Name = "txt_QuantityLose";
            this.txt_QuantityLose.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_QuantityLose.Size = new System.Drawing.Size(88, 26);
            this.txt_QuantityLose.StateCommon.Border.ColorAngle = 1F;
            this.txt_QuantityLose.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_QuantityLose.StateCommon.Border.Rounding = 4;
            this.txt_QuantityLose.StateCommon.Border.Width = 1;
            this.txt_QuantityLose.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_QuantityLose.TabIndex = 272;
            this.txt_QuantityLose.Text = "0";
            this.txt_QuantityLose.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txt_Percent
            // 
            this.txt_Percent.Location = new System.Drawing.Point(106, 128);
            this.txt_Percent.Name = "txt_Percent";
            this.txt_Percent.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_Percent.Size = new System.Drawing.Size(88, 26);
            this.txt_Percent.StateCommon.Border.ColorAngle = 1F;
            this.txt_Percent.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_Percent.StateCommon.Border.Rounding = 4;
            this.txt_Percent.StateCommon.Border.Width = 1;
            this.txt_Percent.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Percent.TabIndex = 273;
            this.txt_Percent.Text = "100";
            this.txt_Percent.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label17.Location = new System.Drawing.Point(0, 146);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(65, 15);
            this.label17.TabIndex = 323;
            this.label17.Text = "Nhóm việc";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txt_Note
            // 
            this.txt_Note.Location = new System.Drawing.Point(84, 168);
            this.txt_Note.Name = "txt_Note";
            this.txt_Note.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_Note.Size = new System.Drawing.Size(685, 26);
            this.txt_Note.StateCommon.Border.ColorAngle = 1F;
            this.txt_Note.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_Note.StateCommon.Border.Rounding = 4;
            this.txt_Note.StateCommon.Border.Width = 1;
            this.txt_Note.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Note.TabIndex = 312;
            // 
            // cbo_Team
            // 
            this.cbo_Team.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_Team.DropDownWidth = 119;
            this.cbo_Team.Location = new System.Drawing.Point(84, 58);
            this.cbo_Team.Name = "cbo_Team";
            this.cbo_Team.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.cbo_Team.Size = new System.Drawing.Size(316, 22);
            this.cbo_Team.StateCommon.ComboBox.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.cbo_Team.StateCommon.ComboBox.Border.Rounding = 4;
            this.cbo_Team.StateCommon.ComboBox.Border.Width = 1;
            this.cbo_Team.StateCommon.ComboBox.Content.Font = new System.Drawing.Font("Tahoma", 9F);
            this.cbo_Team.StateCommon.Item.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.cbo_Team.StateCommon.Item.Border.Rounding = 4;
            this.cbo_Team.StateCommon.Item.Border.Width = 1;
            this.cbo_Team.StateCommon.Item.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cbo_Team.TabIndex = 320;
            // 
            // txt_WorkID
            // 
            this.txt_WorkID.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btn_SearchInput_Work});
            this.txt_WorkID.Location = new System.Drawing.Point(84, 113);
            this.txt_WorkID.Name = "txt_WorkID";
            this.txt_WorkID.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_WorkID.Size = new System.Drawing.Size(133, 26);
            this.txt_WorkID.StateCommon.Border.ColorAngle = 1F;
            this.txt_WorkID.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_WorkID.StateCommon.Border.Rounding = 4;
            this.txt_WorkID.StateCommon.Border.Width = 1;
            this.txt_WorkID.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_WorkID.TabIndex = 318;
            // 
            // btn_SearchInput_Work
            // 
            this.btn_SearchInput_Work.Image = ((System.Drawing.Image)(resources.GetObject("btn_SearchInput_Work.Image")));
            this.btn_SearchInput_Work.UniqueName = "FFABA6C89F274796F78435AB2589EE7D";
            // 
            // txt_ProductID
            // 
            this.txt_ProductID.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.buttonSpecAny1});
            this.txt_ProductID.Location = new System.Drawing.Point(84, 84);
            this.txt_ProductID.Name = "txt_ProductID";
            this.txt_ProductID.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_ProductID.Size = new System.Drawing.Size(133, 26);
            this.txt_ProductID.StateCommon.Border.ColorAngle = 1F;
            this.txt_ProductID.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_ProductID.StateCommon.Border.Rounding = 4;
            this.txt_ProductID.StateCommon.Border.Width = 1;
            this.txt_ProductID.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_ProductID.TabIndex = 317;
            // 
            // buttonSpecAny1
            // 
            this.buttonSpecAny1.UniqueName = "FFABA6C89F274796F78435AB2589EE7D";
            // 
            // txt_OrderIDCompare
            // 
            this.txt_OrderIDCompare.Location = new System.Drawing.Point(84, 3);
            this.txt_OrderIDCompare.Name = "txt_OrderIDCompare";
            this.txt_OrderIDCompare.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_OrderIDCompare.Size = new System.Drawing.Size(248, 26);
            this.txt_OrderIDCompare.StateCommon.Border.ColorAngle = 1F;
            this.txt_OrderIDCompare.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_OrderIDCompare.StateCommon.Border.Rounding = 4;
            this.txt_OrderIDCompare.StateCommon.Border.Width = 1;
            this.txt_OrderIDCompare.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_OrderIDCompare.TabIndex = 311;
            // 
            // txt_OrderID
            // 
            this.txt_OrderID.Location = new System.Drawing.Point(84, 30);
            this.txt_OrderID.Name = "txt_OrderID";
            this.txt_OrderID.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_OrderID.Size = new System.Drawing.Size(248, 26);
            this.txt_OrderID.StateCommon.Border.ColorAngle = 1F;
            this.txt_OrderID.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_OrderID.StateCommon.Border.Rounding = 4;
            this.txt_OrderID.StateCommon.Border.Width = 1;
            this.txt_OrderID.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_OrderID.TabIndex = 310;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label15.Location = new System.Drawing.Point(0, 63);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(41, 15);
            this.label15.TabIndex = 304;
            this.label15.Text = "Nhóm";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label2.Location = new System.Drawing.Point(338, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 15);
            this.label2.TabIndex = 306;
            this.label2.Text = "Tình trạng";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label4.Location = new System.Drawing.Point(338, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 15);
            this.label4.TabIndex = 306;
            this.label4.Text = "Ngày";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label18.Location = new System.Drawing.Point(-2, 8);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(87, 15);
            this.label18.TabIndex = 305;
            this.label18.Text = "Mã tham chiếu";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label13.Location = new System.Drawing.Point(0, 35);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(79, 15);
            this.label13.TabIndex = 305;
            this.label13.Text = "Mã đơn hàng";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label16.Location = new System.Drawing.Point(1, 90);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(64, 15);
            this.label16.TabIndex = 307;
            this.label16.Text = "Sản phẩm";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.Location = new System.Drawing.Point(-1, 174);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 15);
            this.label1.TabIndex = 309;
            this.label1.Text = "Ghi chú";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label5.Location = new System.Drawing.Point(0, 119);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 15);
            this.label5.TabIndex = 308;
            this.label5.Text = "Công việc";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txt_Unit
            // 
            this.txt_Unit.Location = new System.Drawing.Point(480, 79);
            this.txt_Unit.Name = "txt_Unit";
            this.txt_Unit.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_Unit.Size = new System.Drawing.Size(83, 26);
            this.txt_Unit.StateCommon.Border.ColorAngle = 1F;
            this.txt_Unit.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_Unit.StateCommon.Border.Rounding = 4;
            this.txt_Unit.StateCommon.Border.Width = 1;
            this.txt_Unit.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Unit.TabIndex = 316;
            this.txt_Unit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txt_Price
            // 
            this.txt_Price.Location = new System.Drawing.Point(480, 108);
            this.txt_Price.Name = "txt_Price";
            this.txt_Price.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_Price.ReadOnly = true;
            this.txt_Price.Size = new System.Drawing.Size(83, 26);
            this.txt_Price.StateCommon.Border.ColorAngle = 1F;
            this.txt_Price.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_Price.StateCommon.Border.Rounding = 4;
            this.txt_Price.StateCommon.Border.Width = 1;
            this.txt_Price.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Price.TabIndex = 315;
            this.txt_Price.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // dte_OrderDate
            // 
            this.dte_OrderDate.CustomFormat = "dd/MM/yyyy";
            this.dte_OrderDate.Location = new System.Drawing.Point(411, 3);
            this.dte_OrderDate.Name = "dte_OrderDate";
            this.dte_OrderDate.Size = new System.Drawing.Size(101, 26);
            this.dte_OrderDate.TabIndex = 321;
            this.dte_OrderDate.Value = new System.DateTime(((long)(0)));
            // 
            // cbo_Status
            // 
            this.cbo_Status.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_Status.DropDownWidth = 119;
            this.cbo_Status.Items.AddRange(new object[] {
            "Đang thực hiện",
            "Đã thực hiện"});
            this.cbo_Status.Location = new System.Drawing.Point(411, 34);
            this.cbo_Status.Name = "cbo_Status";
            this.cbo_Status.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.cbo_Status.Size = new System.Drawing.Size(152, 22);
            this.cbo_Status.StateCommon.ComboBox.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.cbo_Status.StateCommon.ComboBox.Border.Rounding = 4;
            this.cbo_Status.StateCommon.ComboBox.Border.Width = 1;
            this.cbo_Status.StateCommon.ComboBox.Content.Font = new System.Drawing.Font("Tahoma", 9F);
            this.cbo_Status.StateCommon.Item.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.cbo_Status.StateCommon.Item.Border.Rounding = 4;
            this.cbo_Status.StateCommon.Item.Border.Width = 1;
            this.cbo_Status.StateCommon.Item.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cbo_Status.TabIndex = 319;
            this.cbo_Status.Text = "Đã thực hiện";
            // 
            // txt_WorkName
            // 
            this.txt_WorkName.Location = new System.Drawing.Point(231, 111);
            this.txt_WorkName.Multiline = true;
            this.txt_WorkName.Name = "txt_WorkName";
            this.txt_WorkName.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_WorkName.ReadOnly = true;
            this.txt_WorkName.Size = new System.Drawing.Size(243, 55);
            this.txt_WorkName.StateCommon.Border.ColorAngle = 1F;
            this.txt_WorkName.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_WorkName.StateCommon.Border.Rounding = 4;
            this.txt_WorkName.StateCommon.Border.Width = 1;
            this.txt_WorkName.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_WorkName.TabIndex = 314;
            // 
            // txt_ProductName
            // 
            this.txt_ProductName.Location = new System.Drawing.Point(231, 82);
            this.txt_ProductName.Name = "txt_ProductName";
            this.txt_ProductName.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_ProductName.ReadOnly = true;
            this.txt_ProductName.Size = new System.Drawing.Size(243, 26);
            this.txt_ProductName.StateCommon.Border.ColorAngle = 1F;
            this.txt_ProductName.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_ProductName.StateCommon.Border.Rounding = 4;
            this.txt_ProductName.StateCommon.Border.Width = 1;
            this.txt_ProductName.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_ProductName.TabIndex = 313;
            // 
            // GVRate
            // 
            this.GVRate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GVRate.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.GVRate.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.GVRate.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GVRate.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.GVRate.Location = new System.Drawing.Point(775, 12);
            this.GVRate.Name = "GVRate";
            this.GVRate.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GVRate.Size = new System.Drawing.Size(257, 182);
            this.GVRate.TabIndex = 303;
            // 
            // Panel_Employee
            // 
            this.Panel_Employee.Controls.Add(this.Panel_Message);
            this.Panel_Employee.Controls.Add(this.GVEmployee);
            this.Panel_Employee.Controls.Add(this.txt_title);
            this.Panel_Employee.Controls.Add(this.panel4);
            this.Panel_Employee.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel_Employee.Location = new System.Drawing.Point(0, 255);
            this.Panel_Employee.Name = "Panel_Employee";
            this.Panel_Employee.Size = new System.Drawing.Size(1062, 372);
            this.Panel_Employee.TabIndex = 226;
            // 
            // Panel_Message
            // 
            this.Panel_Message.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Panel_Message.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel_Message.Controls.Add(this.lbl_Message);
            this.Panel_Message.Controls.Add(this.kryptonHeader2);
            this.Panel_Message.Location = new System.Drawing.Point(760, 168);
            this.Panel_Message.Name = "Panel_Message";
            this.Panel_Message.Size = new System.Drawing.Size(302, 150);
            this.Panel_Message.TabIndex = 210;
            this.Panel_Message.Visible = false;
            // 
            // lbl_Message
            // 
            this.lbl_Message.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_Message.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_Message.ForeColor = System.Drawing.Color.Maroon;
            this.lbl_Message.Location = new System.Drawing.Point(0, 30);
            this.lbl_Message.Name = "lbl_Message";
            this.lbl_Message.Size = new System.Drawing.Size(300, 118);
            this.lbl_Message.TabIndex = 0;
            this.lbl_Message.Text = "....";
            // 
            // kryptonHeader2
            // 
            this.kryptonHeader2.AutoSize = false;
            this.kryptonHeader2.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnClose_Panel_Message});
            this.kryptonHeader2.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader2.HeaderStyle = ComponentFactory.Krypton.Toolkit.HeaderStyle.Secondary;
            this.kryptonHeader2.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader2.Name = "kryptonHeader2";
            this.kryptonHeader2.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader2.Size = new System.Drawing.Size(300, 30);
            this.kryptonHeader2.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.kryptonHeader2.TabIndex = 204;
            this.kryptonHeader2.Values.Description = "";
            this.kryptonHeader2.Values.Heading = "!. Cần kiểm tra";
            this.kryptonHeader2.Values.Image = null;
            // 
            // btnClose_Panel_Message
            // 
            this.btnClose_Panel_Message.Checked = ComponentFactory.Krypton.Toolkit.ButtonCheckState.Checked;
            this.btnClose_Panel_Message.Enabled = ComponentFactory.Krypton.Toolkit.ButtonEnabled.True;
            this.btnClose_Panel_Message.Type = ComponentFactory.Krypton.Toolkit.PaletteButtonSpecStyle.Close;
            this.btnClose_Panel_Message.UniqueName = "BEA2AE54CDCE44F263AB208C74E6907A";
            this.btnClose_Panel_Message.Click += new System.EventHandler(this.btnClose_Panel_Message_Click);
            // 
            // GVEmployee
            // 
            this.GVEmployee.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GVEmployee.Location = new System.Drawing.Point(0, 30);
            this.GVEmployee.Name = "GVEmployee";
            this.GVEmployee.Size = new System.Drawing.Size(1062, 288);
            this.GVEmployee.TabIndex = 209;
            // 
            // txt_title
            // 
            this.txt_title.AutoSize = false;
            this.txt_title.Dock = System.Windows.Forms.DockStyle.Top;
            this.txt_title.Location = new System.Drawing.Point(0, 0);
            this.txt_title.Name = "txt_title";
            this.txt_title.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_title.Size = new System.Drawing.Size(1062, 30);
            this.txt_title.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.txt_title.TabIndex = 203;
            this.txt_title.Values.Description = "";
            this.txt_title.Values.Heading = "Chi tiết năng suất công nhân";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.btn_SaveOrder);
            this.panel4.Controls.Add(this.btn_CopyOrder);
            this.panel4.Controls.Add(this.btn_NewOrder);
            this.panel4.Controls.Add(this.btn_DeleteOrder);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(0, 318);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1062, 54);
            this.panel4.TabIndex = 207;
            // 
            // btn_SaveOrder
            // 
            this.btn_SaveOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_SaveOrder.Location = new System.Drawing.Point(937, 6);
            this.btn_SaveOrder.Name = "btn_SaveOrder";
            this.btn_SaveOrder.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_SaveOrder.Size = new System.Drawing.Size(120, 40);
            this.btn_SaveOrder.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_SaveOrder.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_SaveOrder.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_SaveOrder.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_SaveOrder.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_SaveOrder.TabIndex = 206;
            this.btn_SaveOrder.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_SaveOrder.Values.Image")));
            this.btn_SaveOrder.Values.Text = "Cập nhật";
            this.btn_SaveOrder.Click += new System.EventHandler(this.btn_SaveOrder_Click);
            // 
            // btn_CopyOrder
            // 
            this.btn_CopyOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_CopyOrder.Location = new System.Drawing.Point(811, 6);
            this.btn_CopyOrder.Name = "btn_CopyOrder";
            this.btn_CopyOrder.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_CopyOrder.Size = new System.Drawing.Size(120, 40);
            this.btn_CopyOrder.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_CopyOrder.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_CopyOrder.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_CopyOrder.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_CopyOrder.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_CopyOrder.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_CopyOrder.TabIndex = 16;
            this.btn_CopyOrder.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_CopyOrder.Values.Image")));
            this.btn_CopyOrder.Values.Text = "Sao chép";
            this.btn_CopyOrder.Click += new System.EventHandler(this.btn_CopyOrder_Click);
            // 
            // btn_NewOrder
            // 
            this.btn_NewOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_NewOrder.Location = new System.Drawing.Point(4, 5);
            this.btn_NewOrder.Name = "btn_NewOrder";
            this.btn_NewOrder.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_NewOrder.Size = new System.Drawing.Size(120, 40);
            this.btn_NewOrder.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_NewOrder.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_NewOrder.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_NewOrder.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_NewOrder.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_NewOrder.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_NewOrder.TabIndex = 15;
            this.btn_NewOrder.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_NewOrder.Values.Image")));
            this.btn_NewOrder.Values.Text = "Làm mới";
            this.btn_NewOrder.Click += new System.EventHandler(this.btn_NewOrder_Click);
            // 
            // btn_DeleteOrder
            // 
            this.btn_DeleteOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_DeleteOrder.Location = new System.Drawing.Point(685, 6);
            this.btn_DeleteOrder.Name = "btn_DeleteOrder";
            this.btn_DeleteOrder.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_DeleteOrder.Size = new System.Drawing.Size(120, 40);
            this.btn_DeleteOrder.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_DeleteOrder.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_DeleteOrder.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_DeleteOrder.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_DeleteOrder.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_DeleteOrder.TabIndex = 14;
            this.btn_DeleteOrder.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_DeleteOrder.Values.Image")));
            this.btn_DeleteOrder.Values.Text = "Xóa đơn hàng";
            this.btn_DeleteOrder.Click += new System.EventHandler(this.btn_DeleteOrder_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 122);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.Panel_Left);
            this.splitContainer1.Panel1MinSize = 300;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.splitContainer1.Panel2.Controls.Add(this.txtHeader);
            this.splitContainer1.Panel2.Controls.Add(this.Panel_Employee);
            this.splitContainer1.Panel2.Controls.Add(this.Panel_Info);
            this.splitContainer1.Panel2MinSize = 0;
            this.splitContainer1.Size = new System.Drawing.Size(1366, 627);
            this.splitContainer1.SplitterDistance = 300;
            this.splitContainer1.TabIndex = 227;
            // 
            // txtHeader
            // 
            this.txtHeader.AutoSize = false;
            this.txtHeader.Location = new System.Drawing.Point(0, 0);
            this.txtHeader.Name = "txtHeader";
            this.txtHeader.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txtHeader.Size = new System.Drawing.Size(1062, 30);
            this.txtHeader.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.txtHeader.TabIndex = 248;
            this.txtHeader.Values.Description = "";
            this.txtHeader.Values.Heading = "Chi tiết đơn hàng";
            // 
            // Panel_Search
            // 
            this.Panel_Search.AutoScroll = true;
            this.Panel_Search.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Search.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel_Search.Controls.Add(this.btn_Export);
            this.Panel_Search.Controls.Add(this.btn_Search);
            this.Panel_Search.Controls.Add(this.cbo_TeamID_Search);
            this.Panel_Search.Controls.Add(this.txt_Search_WorkName);
            this.Panel_Search.Controls.Add(this.txt_Search_WorkID);
            this.Panel_Search.Controls.Add(this.btn_DeleteAll);
            this.Panel_Search.Controls.Add(this.label14);
            this.Panel_Search.Controls.Add(this.label12);
            this.Panel_Search.Controls.Add(this.label11);
            this.Panel_Search.Controls.Add(this.txt_Search_IDOrder);
            this.Panel_Search.Controls.Add(this.txt_Search_IDOrder_Original);
            this.Panel_Search.Controls.Add(this.label10);
            this.Panel_Search.Controls.Add(this.label9);
            this.Panel_Search.Controls.Add(this.label7);
            this.Panel_Search.Controls.Add(this.label6);
            this.Panel_Search.Controls.Add(this.txt_Search_ProductName);
            this.Panel_Search.Controls.Add(this.txt_Search_ProductID);
            this.Panel_Search.Controls.Add(this.dte_FromDate);
            this.Panel_Search.Controls.Add(this.dte_ToDate);
            this.Panel_Search.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel_Search.Location = new System.Drawing.Point(0, 42);
            this.Panel_Search.Name = "Panel_Search";
            this.Panel_Search.Size = new System.Drawing.Size(1366, 80);
            this.Panel_Search.TabIndex = 228;
            this.Panel_Search.Visible = false;
            // 
            // btn_Export
            // 
            this.btn_Export.Location = new System.Drawing.Point(1067, 33);
            this.btn_Export.Name = "btn_Export";
            this.btn_Export.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Export.Size = new System.Drawing.Size(113, 40);
            this.btn_Export.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Export.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Export.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Export.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Export.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Export.TabIndex = 357;
            this.btn_Export.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Export.Values.Image")));
            this.btn_Export.Values.Text = "Xuất Excel";
            // 
            // btn_Search
            // 
            this.btn_Search.Location = new System.Drawing.Point(956, 34);
            this.btn_Search.Name = "btn_Search";
            this.btn_Search.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Search.Size = new System.Drawing.Size(105, 40);
            this.btn_Search.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Search.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Search.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Search.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Search.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Search.TabIndex = 356;
            this.btn_Search.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Search.Values.Image")));
            this.btn_Search.Values.Text = "Tìm kiếm";
            // 
            // cbo_TeamID_Search
            // 
            this.cbo_TeamID_Search.DropDownWidth = 119;
            this.cbo_TeamID_Search.Location = new System.Drawing.Point(956, 9);
            this.cbo_TeamID_Search.Name = "cbo_TeamID_Search";
            this.cbo_TeamID_Search.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.cbo_TeamID_Search.Size = new System.Drawing.Size(397, 22);
            this.cbo_TeamID_Search.StateCommon.ComboBox.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.cbo_TeamID_Search.StateCommon.ComboBox.Border.Rounding = 4;
            this.cbo_TeamID_Search.StateCommon.ComboBox.Border.Width = 1;
            this.cbo_TeamID_Search.StateCommon.ComboBox.Content.Font = new System.Drawing.Font("Tahoma", 9F);
            this.cbo_TeamID_Search.StateCommon.Item.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.cbo_TeamID_Search.StateCommon.Item.Border.Rounding = 4;
            this.cbo_TeamID_Search.StateCommon.Item.Border.Width = 1;
            this.cbo_TeamID_Search.StateCommon.Item.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cbo_TeamID_Search.TabIndex = 8;
            // 
            // txt_Search_WorkName
            // 
            this.txt_Search_WorkName.Location = new System.Drawing.Point(609, 40);
            this.txt_Search_WorkName.Name = "txt_Search_WorkName";
            this.txt_Search_WorkName.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_Search_WorkName.ReadOnly = true;
            this.txt_Search_WorkName.Size = new System.Drawing.Size(297, 26);
            this.txt_Search_WorkName.StateCommon.Border.ColorAngle = 1F;
            this.txt_Search_WorkName.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_Search_WorkName.StateCommon.Border.Rounding = 4;
            this.txt_Search_WorkName.StateCommon.Border.Width = 1;
            this.txt_Search_WorkName.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Search_WorkName.TabIndex = 7;
            // 
            // txt_Search_WorkID
            // 
            this.txt_Search_WorkID.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btn_Search_Work});
            this.txt_Search_WorkID.Location = new System.Drawing.Point(495, 40);
            this.txt_Search_WorkID.Name = "txt_Search_WorkID";
            this.txt_Search_WorkID.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_Search_WorkID.Size = new System.Drawing.Size(112, 26);
            this.txt_Search_WorkID.StateCommon.Border.ColorAngle = 1F;
            this.txt_Search_WorkID.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_Search_WorkID.StateCommon.Border.Rounding = 4;
            this.txt_Search_WorkID.StateCommon.Border.Width = 1;
            this.txt_Search_WorkID.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Search_WorkID.TabIndex = 5;
            // 
            // btn_Search_Work
            // 
            this.btn_Search_Work.Image = ((System.Drawing.Image)(resources.GetObject("btn_Search_Work.Image")));
            this.btn_Search_Work.UniqueName = "FFABA6C89F274796F78435AB2589EE7D";
            this.btn_Search_Work.Click += new System.EventHandler(this.btn_Search_Work_Click);
            // 
            // btn_DeleteAll
            // 
            this.btn_DeleteAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_DeleteAll.Location = new System.Drawing.Point(1184, 33);
            this.btn_DeleteAll.Name = "btn_DeleteAll";
            this.btn_DeleteAll.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_DeleteAll.Size = new System.Drawing.Size(142, 40);
            this.btn_DeleteAll.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_DeleteAll.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_DeleteAll.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_DeleteAll.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_DeleteAll.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_DeleteAll.TabIndex = 14;
            this.btn_DeleteAll.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_DeleteAll.Values.Image")));
            this.btn_DeleteAll.Values.Text = "Xóa Hêt Lưới ĐH";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(432, 45);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(60, 14);
            this.label14.TabIndex = 260;
            this.label14.Text = "Công việc";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label12.Location = new System.Drawing.Point(144, 42);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(86, 15);
            this.label12.TabIndex = 259;
            this.label12.Text = "Mã phần mềm";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label11.Location = new System.Drawing.Point(143, 14);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(87, 15);
            this.label11.TabIndex = 258;
            this.label11.Text = "Mã tham chiếu";
            // 
            // txt_Search_IDOrder
            // 
            this.txt_Search_IDOrder.Location = new System.Drawing.Point(232, 36);
            this.txt_Search_IDOrder.Name = "txt_Search_IDOrder";
            this.txt_Search_IDOrder.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_Search_IDOrder.Size = new System.Drawing.Size(194, 26);
            this.txt_Search_IDOrder.StateCommon.Border.ColorAngle = 1F;
            this.txt_Search_IDOrder.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_Search_IDOrder.StateCommon.Border.Rounding = 4;
            this.txt_Search_IDOrder.StateCommon.Border.Width = 1;
            this.txt_Search_IDOrder.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Search_IDOrder.TabIndex = 3;
            // 
            // txt_Search_IDOrder_Original
            // 
            this.txt_Search_IDOrder_Original.Location = new System.Drawing.Point(232, 8);
            this.txt_Search_IDOrder_Original.Name = "txt_Search_IDOrder_Original";
            this.txt_Search_IDOrder_Original.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_Search_IDOrder_Original.Size = new System.Drawing.Size(194, 26);
            this.txt_Search_IDOrder_Original.StateCommon.Border.ColorAngle = 1F;
            this.txt_Search_IDOrder_Original.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_Search_IDOrder_Original.StateCommon.Border.Rounding = 4;
            this.txt_Search_IDOrder_Original.StateCommon.Border.Width = 1;
            this.txt_Search_IDOrder_Original.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Search_IDOrder_Original.TabIndex = 2;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(911, 13);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(39, 14);
            this.label10.TabIndex = 248;
            this.label10.Text = "Nhóm";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(432, 16);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(61, 14);
            this.label9.TabIndex = 248;
            this.label9.Text = "Sản phẩm";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 39);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 14);
            this.label7.TabIndex = 247;
            this.label7.Text = "Đến";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 15);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(23, 14);
            this.label6.TabIndex = 247;
            this.label6.Text = "Từ";
            // 
            // txt_Search_ProductName
            // 
            this.txt_Search_ProductName.Location = new System.Drawing.Point(609, 9);
            this.txt_Search_ProductName.Name = "txt_Search_ProductName";
            this.txt_Search_ProductName.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_Search_ProductName.ReadOnly = true;
            this.txt_Search_ProductName.Size = new System.Drawing.Size(297, 26);
            this.txt_Search_ProductName.StateCommon.Border.ColorAngle = 1F;
            this.txt_Search_ProductName.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_Search_ProductName.StateCommon.Border.Rounding = 4;
            this.txt_Search_ProductName.StateCommon.Border.Width = 1;
            this.txt_Search_ProductName.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Search_ProductName.TabIndex = 6;
            // 
            // txt_Search_ProductID
            // 
            this.txt_Search_ProductID.Location = new System.Drawing.Point(495, 9);
            this.txt_Search_ProductID.Name = "txt_Search_ProductID";
            this.txt_Search_ProductID.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_Search_ProductID.Size = new System.Drawing.Size(112, 26);
            this.txt_Search_ProductID.StateCommon.Border.ColorAngle = 1F;
            this.txt_Search_ProductID.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_Search_ProductID.StateCommon.Border.Rounding = 4;
            this.txt_Search_ProductID.StateCommon.Border.Width = 1;
            this.txt_Search_ProductID.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Search_ProductID.TabIndex = 4;
            // 
            // dte_FromDate
            // 
            this.dte_FromDate.CustomFormat = "dd/MM/yyyy";
            this.dte_FromDate.Location = new System.Drawing.Point(37, 12);
            this.dte_FromDate.Name = "dte_FromDate";
            this.dte_FromDate.Size = new System.Drawing.Size(103, 22);
            this.dte_FromDate.TabIndex = 0;
            this.dte_FromDate.Value = new System.DateTime(((long)(0)));
            // 
            // dte_ToDate
            // 
            this.dte_ToDate.CustomFormat = "dd/MM/yyyy";
            this.dte_ToDate.Location = new System.Drawing.Point(37, 36);
            this.dte_ToDate.Name = "dte_ToDate";
            this.dte_ToDate.Size = new System.Drawing.Size(103, 22);
            this.dte_ToDate.TabIndex = 1;
            this.dte_ToDate.Value = new System.DateTime(((long)(0)));
            // 
            // LV_Product
            // 
            this.LV_Product.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LV_Product.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LV_Product.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.LV_Product.FullRowSelect = true;
            this.LV_Product.GridLines = true;
            this.LV_Product.HideSelection = false;
            this.LV_Product.Location = new System.Drawing.Point(459, 77);
            this.LV_Product.Name = "LV_Product";
            this.LV_Product.Size = new System.Drawing.Size(483, 250);
            this.LV_Product.TabIndex = 278;
            this.LV_Product.UseCompatibleStateImageBehavior = false;
            this.LV_Product.View = System.Windows.Forms.View.Details;
            this.LV_Product.Visible = false;
            // 
            // Frm_Production_Order_V2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1366, 749);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.Panel_Search);
            this.Controls.Add(this.HeaderControl);
            this.Controls.Add(this.LV_Product);
            this.Font = new System.Drawing.Font("Tahoma", 9F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_Production_Order_V2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Thông tin đơn hàng sản xuất";
            this.Load += new System.EventHandler(this.Frm_Import_Order_V2_Load);
            this.Panel_Left.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GVOrder)).EndInit();
            this.Panel_Info.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbo_Team)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo_Status)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVRate)).EndInit();
            this.Panel_Employee.ResumeLayout(false);
            this.Panel_Message.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GVEmployee)).EndInit();
            this.panel4.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.Panel_Search.ResumeLayout(false);
            this.Panel_Search.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbo_TeamID_Search)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ComponentFactory.Krypton.Toolkit.KryptonHeader HeaderControl;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMini;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMax;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnClose;
        private System.Windows.Forms.Panel Panel_Left;
        private System.Windows.Forms.Panel Panel_Info;
        private System.Windows.Forms.Panel Panel_Employee;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader txt_title;
        private System.Windows.Forms.Panel panel4;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader3;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btn_Show;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btn_Hide;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridView GVOrder;
        private TN.Toolbox.TN_GridView GVEmployee;
        private System.Windows.Forms.Panel Panel_Message;
        private System.Windows.Forms.Label lbl_Message;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader2;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btn_OpenSearch;
        private System.Windows.Forms.Panel Panel_Search;
        private SYS.TNDateTimePicker dte_FromDate;
        private SYS.TNDateTimePicker dte_ToDate;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_Search_ProductName;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_Search_ProductID;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny buttonSpecAny6;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_Search_IDOrder;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_Search_IDOrder_Original;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_Search_WorkName;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_Search_WorkID;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btn_Search_Work;
        private System.Windows.Forms.Label label14;
        private ComponentFactory.Krypton.Toolkit.KryptonComboBox cbo_TeamID_Search;
        private System.Windows.Forms.ListView LV_Product;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_DeleteOrder;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_NewOrder;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_CopyOrder;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_SaveOrder;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnClose_Panel_Message;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_WorkGroup;
        private System.Windows.Forms.GroupBox groupBox1;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_OrderIDFollow;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btn_SearchInput_Document;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_QuantityDocument;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label23;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_QuantityReality;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_QuantityLose;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_Percent;
        private System.Windows.Forms.Label label17;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_Note;
        private ComponentFactory.Krypton.Toolkit.KryptonComboBox cbo_Team;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_WorkID;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btn_SearchInput_Work;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_ProductID;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny buttonSpecAny1;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_OrderIDCompare;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_OrderID;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_Unit;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_Price;
        private SYS.TNDateTimePicker dte_OrderDate;
        private ComponentFactory.Krypton.Toolkit.KryptonComboBox cbo_Status;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_WorkName;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_ProductName;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView GVRate;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader txtHeader;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_DeleteAll;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label18;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Export;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Search;
    }
}