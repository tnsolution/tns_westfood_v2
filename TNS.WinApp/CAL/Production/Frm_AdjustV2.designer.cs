﻿namespace TNS.WinApp
{
    partial class Frm_AdjustV2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_AdjustV2));
            this.HeaderControl = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btnMini = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnMax = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnClose = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_New = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Save = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_SaveAll = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.Panel_Left = new System.Windows.Forms.Panel();
            this.Panel_Order = new System.Windows.Forms.Panel();
            this.GV_Order = new TN.Toolbox.TN_GridView();
            this.kryptonHeader2 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.Panel_Team = new System.Windows.Forms.Panel();
            this.GV_Team = new TN.Toolbox.TN_GridView();
            this.kryptonHeader1 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.Panel_Search = new System.Windows.Forms.Panel();
            this.btn_Search = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.dte_Date = new TNS.SYS.TNDateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Panel_Right = new System.Windows.Forms.Panel();
            this.Panel_Data = new System.Windows.Forms.Panel();
            this.GV_Employee = new TN.Toolbox.TN_GridView();
            this.txt_title = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.Panel_Info = new System.Windows.Forms.Panel();
            this.GV_EmployeeView = new TN.Toolbox.TN_GridView();
            this.kryptonHeader3 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.panel1.SuspendLayout();
            this.Panel_Left.SuspendLayout();
            this.Panel_Order.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GV_Order)).BeginInit();
            this.Panel_Team.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GV_Team)).BeginInit();
            this.Panel_Search.SuspendLayout();
            this.Panel_Right.SuspendLayout();
            this.Panel_Data.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GV_Employee)).BeginInit();
            this.Panel_Info.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GV_EmployeeView)).BeginInit();
            this.SuspendLayout();
            // 
            // HeaderControl
            // 
            this.HeaderControl.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnMini,
            this.btnMax,
            this.btnClose});
            this.HeaderControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.HeaderControl.Location = new System.Drawing.Point(0, 0);
            this.HeaderControl.Name = "HeaderControl";
            this.HeaderControl.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.HeaderControl.Size = new System.Drawing.Size(1350, 42);
            this.HeaderControl.TabIndex = 198;
            this.HeaderControl.Values.Description = "";
            this.HeaderControl.Values.Heading = "TÍNH NĂNG SUẤT CHIA LẠI CHO NHÓM LẦN 1";
            this.HeaderControl.Values.Image = ((System.Drawing.Image)(resources.GetObject("HeaderControl.Values.Image")));
            // 
            // btnMini
            // 
            this.btnMini.Image = ((System.Drawing.Image)(resources.GetObject("btnMini.Image")));
            this.btnMini.UniqueName = "F5F06E8241504E72ABB5BEA3F6A9B753";
            // 
            // btnMax
            // 
            this.btnMax.Image = ((System.Drawing.Image)(resources.GetObject("btnMax.Image")));
            this.btnMax.UniqueName = "035D1A4881E44F58A084C31DE7352A94";
            // 
            // btnClose
            // 
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.UniqueName = "11B07C6F4E1C4F9D8B91BD924CB0EBE6";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panel1.Controls.Add(this.btn_New);
            this.panel1.Controls.Add(this.btn_Save);
            this.panel1.Controls.Add(this.btn_SaveAll);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 675);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1350, 54);
            this.panel1.TabIndex = 205;
            // 
            // btn_New
            // 
            this.btn_New.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_New.Location = new System.Drawing.Point(921, 7);
            this.btn_New.Name = "btn_New";
            this.btn_New.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_New.Size = new System.Drawing.Size(120, 40);
            this.btn_New.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_New.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_New.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_New.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_New.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_New.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_New.TabIndex = 221;
            this.btn_New.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_New.Values.Image")));
            this.btn_New.Values.Text = "Chỉnh sửa";
            // 
            // btn_Save
            // 
            this.btn_Save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Save.Location = new System.Drawing.Point(1047, 7);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Save.Size = new System.Drawing.Size(120, 40);
            this.btn_Save.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Save.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Save.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Save.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Save.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Save.TabIndex = 209;
            this.btn_Save.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Save.Values.Image")));
            this.btn_Save.Values.Text = "Tính && Lưu ";
            // 
            // btn_SaveAll
            // 
            this.btn_SaveAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_SaveAll.Location = new System.Drawing.Point(1173, 7);
            this.btn_SaveAll.Name = "btn_SaveAll";
            this.btn_SaveAll.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_SaveAll.Size = new System.Drawing.Size(165, 40);
            this.btn_SaveAll.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_SaveAll.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_SaveAll.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_SaveAll.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_SaveAll.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_SaveAll.TabIndex = 210;
            this.btn_SaveAll.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_SaveAll.Values.Image")));
            this.btn_SaveAll.Values.Text = "Tính && lưu tất cả";
            // 
            // Panel_Left
            // 
            this.Panel_Left.Controls.Add(this.Panel_Order);
            this.Panel_Left.Controls.Add(this.Panel_Team);
            this.Panel_Left.Controls.Add(this.Panel_Search);
            this.Panel_Left.Dock = System.Windows.Forms.DockStyle.Left;
            this.Panel_Left.Location = new System.Drawing.Point(0, 42);
            this.Panel_Left.Name = "Panel_Left";
            this.Panel_Left.Size = new System.Drawing.Size(450, 633);
            this.Panel_Left.TabIndex = 206;
            // 
            // Panel_Order
            // 
            this.Panel_Order.Controls.Add(this.GV_Order);
            this.Panel_Order.Controls.Add(this.kryptonHeader2);
            this.Panel_Order.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel_Order.Location = new System.Drawing.Point(0, 280);
            this.Panel_Order.Name = "Panel_Order";
            this.Panel_Order.Size = new System.Drawing.Size(450, 353);
            this.Panel_Order.TabIndex = 206;
            // 
            // GV_Order
            // 
            this.GV_Order.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GV_Order.Location = new System.Drawing.Point(0, 30);
            this.GV_Order.Name = "GV_Order";
            this.GV_Order.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GV_Order.Size = new System.Drawing.Size(450, 323);
            this.GV_Order.TabIndex = 213;
            // 
            // kryptonHeader2
            // 
            this.kryptonHeader2.AutoSize = false;
            this.kryptonHeader2.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader2.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader2.Name = "kryptonHeader2";
            this.kryptonHeader2.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader2.Size = new System.Drawing.Size(450, 30);
            this.kryptonHeader2.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kryptonHeader2.TabIndex = 200;
            this.kryptonHeader2.Values.Description = "";
            this.kryptonHeader2.Values.Heading = "Chọn đơn hàng";
            // 
            // Panel_Team
            // 
            this.Panel_Team.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Team.Controls.Add(this.GV_Team);
            this.Panel_Team.Controls.Add(this.kryptonHeader1);
            this.Panel_Team.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel_Team.Location = new System.Drawing.Point(0, 55);
            this.Panel_Team.Name = "Panel_Team";
            this.Panel_Team.Size = new System.Drawing.Size(450, 225);
            this.Panel_Team.TabIndex = 205;
            // 
            // GV_Team
            // 
            this.GV_Team.AllowUserToAddRows = false;
            this.GV_Team.AllowUserToDeleteRows = false;
            this.GV_Team.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GV_Team.Location = new System.Drawing.Point(0, 30);
            this.GV_Team.Name = "GV_Team";
            this.GV_Team.Size = new System.Drawing.Size(450, 195);
            this.GV_Team.TabIndex = 215;
            // 
            // kryptonHeader1
            // 
            this.kryptonHeader1.AutoSize = false;
            this.kryptonHeader1.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader1.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader1.Name = "kryptonHeader1";
            this.kryptonHeader1.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader1.Size = new System.Drawing.Size(450, 30);
            this.kryptonHeader1.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kryptonHeader1.TabIndex = 201;
            this.kryptonHeader1.Values.Description = "";
            this.kryptonHeader1.Values.Heading = "Tổ nhóm";
            // 
            // Panel_Search
            // 
            this.Panel_Search.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Search.Controls.Add(this.btn_Search);
            this.Panel_Search.Controls.Add(this.dte_Date);
            this.Panel_Search.Controls.Add(this.label8);
            this.Panel_Search.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel_Search.Location = new System.Drawing.Point(0, 0);
            this.Panel_Search.Name = "Panel_Search";
            this.Panel_Search.Size = new System.Drawing.Size(450, 55);
            this.Panel_Search.TabIndex = 203;
            // 
            // btn_Search
            // 
            this.btn_Search.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Search.Location = new System.Drawing.Point(188, 8);
            this.btn_Search.Name = "btn_Search";
            this.btn_Search.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Search.Size = new System.Drawing.Size(102, 40);
            this.btn_Search.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Search.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Search.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Search.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Search.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Search.TabIndex = 207;
            this.btn_Search.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Search.Values.Image")));
            this.btn_Search.Values.Text = "Tìm kiếm";
            // 
            // dte_Date
            // 
            this.dte_Date.CustomFormat = "dd/MM/yyyy";
            this.dte_Date.Location = new System.Drawing.Point(76, 17);
            this.dte_Date.Name = "dte_Date";
            this.dte_Date.Size = new System.Drawing.Size(106, 20);
            this.dte_Date.TabIndex = 0;
            this.dte_Date.Value = new System.DateTime(((long)(0)));
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label8.Location = new System.Drawing.Point(39, 20);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 15);
            this.label8.TabIndex = 149;
            this.label8.Text = "Ngày";
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.DodgerBlue;
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Location = new System.Drawing.Point(450, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(4, 633);
            this.label1.TabIndex = 207;
            // 
            // Panel_Right
            // 
            this.Panel_Right.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(178)))), ((int)(((byte)(229)))));
            this.Panel_Right.Controls.Add(this.Panel_Data);
            this.Panel_Right.Controls.Add(this.Panel_Info);
            this.Panel_Right.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel_Right.Location = new System.Drawing.Point(454, 42);
            this.Panel_Right.Name = "Panel_Right";
            this.Panel_Right.Size = new System.Drawing.Size(896, 633);
            this.Panel_Right.TabIndex = 208;
            // 
            // Panel_Data
            // 
            this.Panel_Data.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Data.Controls.Add(this.GV_Employee);
            this.Panel_Data.Controls.Add(this.txt_title);
            this.Panel_Data.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel_Data.Location = new System.Drawing.Point(0, 280);
            this.Panel_Data.Name = "Panel_Data";
            this.Panel_Data.Size = new System.Drawing.Size(896, 353);
            this.Panel_Data.TabIndex = 204;
            // 
            // GV_Employee
            // 
            this.GV_Employee.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GV_Employee.Location = new System.Drawing.Point(0, 30);
            this.GV_Employee.Name = "GV_Employee";
            this.GV_Employee.Size = new System.Drawing.Size(896, 323);
            this.GV_Employee.TabIndex = 214;
            // 
            // txt_title
            // 
            this.txt_title.AutoSize = false;
            this.txt_title.Dock = System.Windows.Forms.DockStyle.Top;
            this.txt_title.Location = new System.Drawing.Point(0, 0);
            this.txt_title.Name = "txt_title";
            this.txt_title.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_title.Size = new System.Drawing.Size(896, 30);
            this.txt_title.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.txt_title.TabIndex = 200;
            this.txt_title.Values.Description = "";
            this.txt_title.Values.Heading = "Thông tin đơn hàng mẫu (TÍNH CHIA TIỀN)";
            // 
            // Panel_Info
            // 
            this.Panel_Info.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Info.Controls.Add(this.GV_EmployeeView);
            this.Panel_Info.Controls.Add(this.kryptonHeader3);
            this.Panel_Info.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel_Info.Location = new System.Drawing.Point(0, 0);
            this.Panel_Info.Name = "Panel_Info";
            this.Panel_Info.Size = new System.Drawing.Size(896, 280);
            this.Panel_Info.TabIndex = 205;
            // 
            // GV_EmployeeView
            // 
            this.GV_EmployeeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GV_EmployeeView.Location = new System.Drawing.Point(0, 30);
            this.GV_EmployeeView.Name = "GV_EmployeeView";
            this.GV_EmployeeView.ReadOnly = true;
            this.GV_EmployeeView.Size = new System.Drawing.Size(896, 250);
            this.GV_EmployeeView.TabIndex = 215;
            // 
            // kryptonHeader3
            // 
            this.kryptonHeader3.AutoSize = false;
            this.kryptonHeader3.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader3.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader3.Name = "kryptonHeader3";
            this.kryptonHeader3.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader3.Size = new System.Drawing.Size(896, 30);
            this.kryptonHeader3.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kryptonHeader3.TabIndex = 202;
            this.kryptonHeader3.Values.Description = "";
            this.kryptonHeader3.Values.Heading = "Thông tin đơn hàng đã tính (CHỈ XEM)";
            // 
            // Frm_AdjustV2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1350, 729);
            this.Controls.Add(this.Panel_Right);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Panel_Left);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.HeaderControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_AdjustV2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TÍNH NĂNG SUẤT CHIA LẠI CHO NHÓM LẦN 1";
            this.Load += new System.EventHandler(this.Frm_Adjust_Load);
            this.panel1.ResumeLayout(false);
            this.Panel_Left.ResumeLayout(false);
            this.Panel_Order.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GV_Order)).EndInit();
            this.Panel_Team.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GV_Team)).EndInit();
            this.Panel_Search.ResumeLayout(false);
            this.Panel_Search.PerformLayout();
            this.Panel_Right.ResumeLayout(false);
            this.Panel_Data.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GV_Employee)).EndInit();
            this.Panel_Info.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GV_EmployeeView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ComponentFactory.Krypton.Toolkit.KryptonHeader HeaderControl;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMini;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMax;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnClose;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel Panel_Left;
        private System.Windows.Forms.Panel Panel_Order;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader2;
        private System.Windows.Forms.Panel Panel_Team;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader1;
        private System.Windows.Forms.Panel Panel_Search;
        private SYS.TNDateTimePicker dte_Date;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel Panel_Right;
        private System.Windows.Forms.Panel Panel_Data;
        private TN.Toolbox.TN_GridView GV_Employee;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader txt_title;
        private System.Windows.Forms.Panel Panel_Info;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Search;
        private TN.Toolbox.TN_GridView GV_Team;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Save;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_SaveAll;
        private TN.Toolbox.TN_GridView GV_EmployeeView;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader3;
        private TN.Toolbox.TN_GridView GV_Order;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_New;
    }
}