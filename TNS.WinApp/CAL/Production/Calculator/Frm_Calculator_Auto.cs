﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using TNS.CORE;
using TNS.Misc;
using TNS.SYS;

/*
    Lưu ý đang fix cứng
    hệ số ngày chủ nhật x2
    tiền danh cho công nhân nam
    tiền danh cho công nhân nữ
    TeamKey cho SCCĐ và SCĐH 88-89

    KHÔNG TÍNH DƯ GIỜ VÌ ĐÃ CÓ PHẦN TÍNH RIÊNG > CODE TÍNH DƯ GIỜ HIỆN ĐANG COMMENT

    Nếu có tổng số nguyên liệu ưu tiên nguyên liệu
    Ngược lại ưu tiên số rổ nếu có tổng số rổ
    Ngược lại lấy số giờ
 */

namespace TNS.WinApp
{
    public partial class Frm_Calculator_Auto : Form
    {
        #region [Var]

        public int AutoKey = 0;         //Key theo dõi tình trang trong ATU_Run;
        public int TeamTracking = 0;    //key team cần chạy riêng để kiểm tra
        public string OrderTracking = string.Empty; //mã đơn hàng chạy riêng để kiểm tra




        // float _OMoney = HoTroSanXuat.LayTienNgoaiGio(SessionUser.Date_Work); //tiền dư giờ
        const int _TeamSCCD = 88;
        const int _TeamSCDH = 89;

        bool _WorkDif = false;

        DataTable _TableTeam;   //nhóm
        DataTable _TableWork;  //công việc tại chỗ
        DataTable _TableOrder; //đơn hàng tại chỗ
        DataTable _TableEmployee;  //Các nhân viên trong đơn hàng

        int _TeamKey = 0;
        int _TotalTeam = 0;
        int _IndexTeam = 0;
        string _TeamName = "";

        int _TotalWork = 0;
        int _IndexWork = 0;
        int _WorkKey = 0;
        float _WorkPrice = 0;//giá công việc
        string _WorkName = "";

        int _TotalOrder = 0;
        int _IndexOrder = 0;
        string _OrderKey = "";

        int _TotalEmp = 0;
        int _IndexEmp = 0;
        string _EmployeeKey = "";

        Order_Model _OrderModel;
        Order_Money_Model _OrderMoney;

        public bool IsAuto;
        public DateTime FromDate;
        public DateTime ToDate;
        #endregion

        public Frm_Calculator_Auto()
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            Utils.DoubleBuffered(GVTeam, true);
            Utils.DrawGVStyle(ref GVTeam);
            InitGVTeam_Layout(GVTeam);

            timerTeam.Enabled = true;
            timerTeam.Stop();

            timerWork.Enabled = true;
            timerWork.Stop();

            timerOrder.Enabled = true;
            timerOrder.Stop();

            timerEmployee.Enabled = true;
            timerEmployee.Stop();

            PicLoading.Visible = false;
            Panel_Right.Visible = false;
            btnHideLog.Visible = false;
            btnShowLog.Visible = true;

            //dte_FromDate.Validated += (o, s) =>
            //{
            //    int LastDay = DateTime.DaysInMonth(dte_FromDate.Value.Year, dte_FromDate.Value.Month);
            //    DateTime nDate = new DateTime(dte_FromDate.Value.Year, dte_FromDate.Value.Month, LastDay, 23, 59, 59);
            //    dte_ToDate.Value = nDate;
            //};
        }
        private void Frm_Calculator_Auto_Load(object sender, EventArgs e)
        {
            if (IsAuto)
            {
                dte_FromDate.Value = FromDate;
                dte_ToDate.Value = ToDate;

                string title = "Tính năng xuất ngày " + FromDate.ToString("dd/MM/yyyy");
                ShowLog(title);
                HeaderControl.Text = title;

                btn_Run.Visible = false;
                dte_FromDate.Enabled = false;
                dte_ToDate.Enabled = false;

                btn_Run_Click(null, null);
            }
            else
            {
                DateTime ViewDate = SessionUser.Date_Work;
                FromDate = new DateTime(ViewDate.Year, ViewDate.Month, 1, 0, 0, 0);
                ToDate = FromDate.AddMonths(1).AddDays(-1);
                ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
                dte_FromDate.Value = FromDate;
                dte_ToDate.Value = ToDate;
                               
                _TableOrder = HoTroSanXuat.DonHangThucHien(OrderTracking);
                _TotalOrder = _TableOrder.Rows.Count;
                _IndexOrder = 0;
                timerOrder.Start();
            }
        }
        void ShowGVTeam(DataRow nRow, DataGridView GV)
        {
            GV.Rows.Add();
            DataGridViewRow nRowView = GV.Rows[_IndexTeam];
            nRowView.Tag = nRow["TeamKey"];
            nRowView.Cells["No"].Value = (_IndexTeam + 1).ToString();
            nRowView.Cells["TeamName"].Value = nRow["TeamName"].ToString().Trim();
            nRowView.Cells["TeamID"].Value = nRow["TeamID"].ToString().Trim();
        }
        void InitGVTeam_Layout(DataGridView GV)
        {
            GV.Columns.Add("No", "#");
            GV.Columns.Add("TeamID", "Mã nhóm");
            GV.Columns.Add("TeamName", "Tên nhóm");

            GV.Columns["No"].Width = 45;
            GV.Columns["TeamID"].Width = 100;
            GV.Columns["TeamName"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.ColumnHeadersHeight = 25;
            GV.FirstDisplayedScrollingRowIndex = GV.RowCount - 1;
        }

        //----------------------------------------------------------------------------
        void LoadTeam()
        {
            if (TeamTracking != 0)
                _TableTeam = HoTroSanXuat.DanhSachToNhom(TeamTracking);
            else
                _TableTeam = HoTroSanXuat.DanhSachToNhom();

            _TotalTeam = _TableTeam.Rows.Count;
        }
        void LoadWork(int TeamKey)
        {
            _TableWork = HoTroSanXuat.CongViecNhom(TeamKey, dte_FromDate.Value, dte_ToDate.Value);
            _TotalWork = _TableWork.Rows.Count;
        }
        void LoadOrder(int TeamKey, int WorkKey)
        {
            //if (OrderTracking != string.Empty)
            //    _TableOrder = HoTroSanXuat.DonHangThucHien(TeamKey, WorkKey, OrderTracking, dte_FromDate.Value, dte_ToDate.Value);
            //else
                _TableOrder = HoTroSanXuat.DonHangThucHien(TeamKey, WorkKey, dte_FromDate.Value, dte_ToDate.Value);

            _TotalOrder = _TableOrder.Rows.Count;
        }
        void LoadEmployee(string OrderKey)
        {
            //lay danh sach nhan vien trong order
            _TableEmployee = HoTroSanXuat.CacNhanVienThucHien(OrderKey);
            _TotalEmp = _TableEmployee.Rows.Count;
        }
        void ProcessEmployee(DataRow r)
        {
            int _DayOfWeek = HoTroSanXuat.LayHeSoChuNhat(dte_ToDate.Value);    //hệ số chủ nhật
            if (_WorkDif == false)
            {
                #region //VIỆC TẠI TỔ

                #region [---------   Chuẩn bị các tham số   -----------]                                //MAP VỚI COUNT

                float zTempMoney = 0;
                float zTempBasket = 0;
                float zTempTime = 0;
                float zTempKg = 0;

                DateTime OrderDate = _OrderModel.OrderDate; // ngày đơn hàng
                float zPrice = _OrderModel.Price;         //giá công việc đơn hàng
                float zTotalBasket = _OrderModel.TotalBasket;  //số rỗ làm
                float zTotalTime = _OrderModel.TotalTime;  //thời gian làm
                float zTotalKg = _OrderModel.TotalKg;          //số ký
                float zRateOrder = _OrderModel.RateOrder; //he số đơn hàng
                float zRateDay = _OrderModel.RateDay;   //hệ số ngày đơn hàng
                float zQuantity = _OrderModel.Quantity; // số lượng 
                float zPercentFinish = _OrderModel.PercentFinish; //tỉ lệ hoàn thành đơn hàng

                #region Cách tính năng xuất ngày 21/02/2020 3 trường hợp FIx hệ số chiều 21/02/2020
                if (zTotalKg > 0)
                {
                    if (zRateDay > 0)
                    {
                        zTempMoney = (zQuantity / zTotalKg) * zPrice * zRateDay;
                        zTempKg = zQuantity / zTotalKg;
                    }
                    if (OrderDate.DayOfWeek != DayOfWeek.Sunday && zRateDay == 0)
                    {
                        zTempMoney = (zQuantity / zTotalKg) * zPrice;
                        zTempKg = zQuantity / zTotalKg;
                    }
                    if (OrderDate.DayOfWeek == DayOfWeek.Sunday)
                    {
                        zTempMoney = (zQuantity / zTotalKg) * zPrice * _DayOfWeek;
                        zTempKg = zQuantity / zTotalKg;
                    }
                }
                else if (zTotalBasket > 0)
                {
                    //TH 1.1 Nếu Ngày hiện tại là ngày lễ
                    if (zRateDay > 0)
                    {
                        zTempMoney = (zQuantity / zTotalBasket) * zPrice * zRateDay;
                        zTempBasket = zQuantity / zTotalBasket;
                    }
                    //TH 1.2:Nếu Ngày hiện tại là ngày thường và không phải là ngày chủ nhật
                    if (OrderDate.DayOfWeek != DayOfWeek.Sunday && zRateDay == 0)
                    {
                        zTempMoney = (zQuantity / zTotalBasket) * zPrice;
                        zTempBasket = zQuantity / zTotalBasket;
                    }
                    //TH 1.3:Nếu Ngày hiện tại là ngày chủ nhật
                    if (OrderDate.DayOfWeek == DayOfWeek.Sunday)
                    {
                        zTempMoney = (zQuantity / zTotalBasket) * zPrice * _DayOfWeek;
                        zTempBasket = zQuantity / zTotalBasket;
                    }
                }
                else if (zTotalTime > 0)
                {
                    //TH 2.1 Nếu Ngày hiện tại là ngày lễ
                    if (zRateDay > 0)
                    {
                        zTempMoney = (zQuantity / zTotalTime) * zPrice * zRateDay;
                        zTempTime = zQuantity / zTotalTime;
                    }
                    //TH 2.2:Nếu Ngày hiện tại là ngày thường và không phải là ngày chủ nhật
                    if (OrderDate.DayOfWeek != DayOfWeek.Sunday && zRateDay == 0)
                    {
                        zTempMoney = (zQuantity / zTotalTime) * zPrice;
                        zTempTime = zQuantity / zTotalTime;
                    }
                    if (OrderDate.DayOfWeek == DayOfWeek.Sunday)
                    {
                        zTempMoney = (zQuantity / zTotalTime) * zPrice * _DayOfWeek;
                        zTempTime = zQuantity / zTotalTime;
                    }
                }
                #endregion

                #region CLOSE
                #region Cách tính năng xuất ngày 21/02/2020 3 trường hợp
                //if (zTotalKg > 0)
                //{
                //    if (zRateDay > 0)
                //    {
                //        zTempMoney = (zQuantity / zTotalKg) * zPrice * zRateOrder * zRateDay;
                //        zTempKg = zQuantity / zTotalKg;
                //    }
                //    if (OrderDate.DayOfWeek != DayOfWeek.Sunday && zRateDay == 0)
                //    {
                //        zTempMoney = (zQuantity / zTotalKg) * zPrice * zRateOrder;
                //        zTempKg = zQuantity / zTotalKg;
                //    }
                //    if (OrderDate.DayOfWeek == DayOfWeek.Sunday)
                //    {
                //        zTempMoney = (zQuantity / zTotalKg) * zPrice * zRateOrder * 2;
                //        zTempKg = zQuantity / zTotalKg;
                //    }
                //}
                //if (zTotalBasket > 0)
                //{
                //    //TH 1.1 Nếu Ngày hiện tại là ngày lễ
                //    if (zRateDay > 0)
                //    {
                //        zTempMoney = (zQuantity / zTotalBasket) * zPrice * zRateOrder * zRateDay;
                //        zTempBasket = zQuantity / zTotalBasket;
                //    }
                //    //TH 1.2:Nếu Ngày hiện tại là ngày thường và không phải là ngày chủ nhật
                //    if (OrderDate.DayOfWeek != DayOfWeek.Sunday && zRateDay == 0)
                //    {
                //        zTempMoney = (zQuantity / zTotalBasket) * zPrice * zRateOrder;
                //        zTempBasket = zQuantity / zTotalBasket;
                //    }
                //    //TH 1.3:Nếu Ngày hiện tại là ngày chủ nhật
                //    if (OrderDate.DayOfWeek == DayOfWeek.Sunday)
                //    {
                //        zTempMoney = (zQuantity / zTotalBasket) * zPrice * zRateOrder * 2;
                //        zTempBasket = zQuantity / zTotalBasket;
                //    }
                //}
                //if (zTotalTime > 0)
                //{
                //    //TH 2.1 Nếu Ngày hiện tại là ngày lễ
                //    if (zRateDay > 0)
                //    {
                //        zTempMoney = (zQuantity / zTotalTime) * zPrice * zRateOrder * zRateDay;
                //        zTempTime = zQuantity / zTotalTime;
                //    }
                //    //TH 2.2:Nếu Ngày hiện tại là ngày thường và không phải là ngày chủ nhật
                //    if (OrderDate.DayOfWeek != DayOfWeek.Sunday && zRateDay == 0)
                //    {
                //        zTempMoney = (zQuantity / zTotalTime) * zPrice * zRateOrder;
                //        zTempTime = zQuantity / zTotalTime;
                //    }
                //    if (OrderDate.DayOfWeek == DayOfWeek.Sunday)
                //    {
                //        zTempMoney = (zQuantity / zTotalTime) * zPrice * zRateOrder * 2;
                //        zTempTime = zQuantity / zTotalTime;
                //    }
                //}
                #endregion
                /*
                   #region  //	Trường hợp 1: Chỉ có tổng số rổ, tổng số kí=0, tổng thời gian=0
                if (zTotalBasket > 0 &&
                    zTotalKg == 0 &&
                    zTotalTime == 0)
                {
                    //TH 1.1 Nếu Ngày hiện tại là ngày lễ
                    if (zRateDay > 0)
                    {
                        zTempMoney = (zQuantity / zTotalBasket) * zPrice * zRateOrder * zRateDay;
                        zTempBasket = zQuantity / zTotalBasket;
                    }
                    //TH 1.2:Nếu Ngày hiện tại là ngày thường và không phải là ngày chủ nhật
                    if (OrderDate.DayOfWeek != DayOfWeek.Sunday && zRateDay == 0)
                    {
                        zTempMoney = (zQuantity / zTotalBasket) * zPrice * zRateOrder;
                        zTempBasket = zQuantity / zTotalBasket;
                    }
                    //TH 1.3:Nếu Ngày hiện tại là ngày chủ nhật
                    if (OrderDate.DayOfWeek == DayOfWeek.Sunday)
                    {
                        zTempMoney = (zQuantity / zTotalBasket) * zPrice * zRateOrder * 2;
                        zTempBasket = zQuantity / zTotalBasket;
                    }
                }
                #endregion
                #region  //	Trường hợp 2: Chỉ tổng thời gian, tổng số kí=0, tổng số rổ =0
                if (zTotalBasket == 0 &&
                    zTotalKg == 0 &&
                    zTotalTime > 0)
                {
                    //TH 2.1 Nếu Ngày hiện tại là ngày lễ
                    if (zRateDay > 0)
                    {
                        zTempMoney = (zQuantity / zTotalTime) * zPrice * zRateOrder * zRateDay;
                        zTempTime = zQuantity / zTotalTime;
                    }
                    //TH 2.2:Nếu Ngày hiện tại là ngày thường và không phải là ngày chủ nhật
                    if (OrderDate.DayOfWeek != DayOfWeek.Sunday && zRateDay == 0)
                    {
                        zTempMoney = (zQuantity / zTotalTime) * zPrice * zRateOrder;
                        zTempTime = zQuantity / zTotalTime;

                    }
                    if (OrderDate.DayOfWeek == DayOfWeek.Sunday)
                    {
                        zTempMoney = (zQuantity / zTotalTime) * zPrice * zRateOrder * 2;
                        zTempTime = zQuantity / zTotalTime;

                    }
                }
                #endregion
                #region  //	Trường hợp 3: Chỉ tổng số kí, tổng thời gian = 0 ,tổng số rổ = 0
                if (zTotalBasket == 0 &&
                   zTotalKg > 0 &&
                   zTotalTime == 0)
                {
                    if (zRateDay > 0)
                    {
                        zTempMoney = (zQuantity / zTotalTime) * zPrice * zRateOrder * zRateDay;
                        zTempKg = zQuantity / zTotalTime;

                    }
                    if (OrderDate.DayOfWeek != DayOfWeek.Sunday && zRateDay == 0)
                    {
                        zTempMoney = (zQuantity / zTotalTime) * zPrice * zRateOrder;
                        zTempKg = zQuantity / zTotalTime;

                    }
                    if (OrderDate.DayOfWeek == DayOfWeek.Sunday)
                    {
                        zTempMoney = (zQuantity / zTotalTime) * zPrice * zRateOrder * 2;
                        zTempKg = zQuantity / zTotalTime;

                    }
                }
                #endregion
                #region   //	Trường hợp 4: tổng số kí >0, tổng thời gian >0 ĐÃ EDIT NGÀY 19/02/2020 HỌP TẠI VP WESTFOOD, EDIT NGƯỢC LẠI BAN ĐẦU NGÀY 21/02/2020
                //if (zTotalKg > 0 && zTotalTime > 0)
                //{
                //    if (zRateDay > 0)
                //    {
                //        zTempMoney = (zTotalKg / zQuantity) * zPrice * zRateOrder * zRateDay;
                //        zTempKg = zTotalKg / zQuantity;
                //    }
                //    if (OrderDate.DayOfWeek != DayOfWeek.Sunday && zRateDay == 0)
                //    {
                //        zTempMoney = (zTotalKg / zQuantity) * zPrice * zRateOrder;
                //        zTempKg = zTotalKg / zQuantity;
                //    }
                //    if (OrderDate.DayOfWeek == DayOfWeek.Sunday)
                //    {
                //        zTempMoney = (zTotalKg / zQuantity) * zPrice * zRateOrder * 2;
                //        zTempKg = zTotalKg / zQuantity;
                //    }
                //}

                if (zTotalKg > 0 && zTotalTime > 0)
                {
                    if (zRateDay > 0)
                    {
                        zTempMoney = (zQuantity / zTotalKg) * zPrice * zRateOrder * zRateDay;
                        zTempKg = (zQuantity / zTotalKg);
                    }
                    if (OrderDate.DayOfWeek != DayOfWeek.Sunday && zRateDay == 0)
                    {
                        zTempMoney = (zQuantity / zTotalKg) * zPrice * zRateOrder;
                        zTempKg = (zQuantity / zTotalKg);
                    }
                    if (OrderDate.DayOfWeek == DayOfWeek.Sunday)
                    {
                        zTempMoney = (zQuantity / zTotalKg) * zPrice * zRateOrder * 2;
                        zTempKg = (zQuantity / zTotalKg);
                    }
                }
                #endregion
                #region //	Trường hợp 5:  tổng số rổ >0, tổng thời gian >0 
                if (zTotalBasket > 0 && zTotalTime > 0)
                {
                    if (zRateDay > 0)
                    {
                        zTempMoney = (zQuantity / zTotalBasket) * zPrice * zRateOrder * zRateDay;
                        zTempBasket = zQuantity / zTotalBasket;
                    }
                    if (OrderDate.DayOfWeek != DayOfWeek.Sunday && zRateDay == 0)
                    {
                        zTempMoney = (zQuantity / zTotalBasket) * zPrice * zRateOrder;
                        zTempBasket = zQuantity / zTotalBasket;
                    }
                    if (OrderDate.DayOfWeek == DayOfWeek.Sunday)
                    {
                        zTempMoney = (zQuantity / zTotalBasket) * zPrice * zRateOrder * 2;
                        zTempBasket = zQuantity / zTotalBasket;
                    }
                }
                #endregion
                 
                 
                 */
                #endregion

                #endregion

                _OrderMoney = new Order_Money_Model();

                int Gender = r["Gender"].ToInt(); //Nam nu
                float Time = 0;
                if (float.TryParse(r["Time"].ToString(), out Time))
                {
                    //Số giờ 
                }
                float Kilogram = 0;
                if (float.TryParse(r["Kilogram"].ToString(), out Kilogram))
                {
                    //Số kí 
                }
                float Basket = 0;
                if (float.TryParse(r["Basket"].ToString(), out Basket))
                {
                    //Số rỗ 
                }
                float ResultKg = 0;     //ket quả số kg
                float ResultMoney = 0;// kết quả số tiền
                #region Tính toán số liệu                                                           //MAP VỚI COUNT_PRODUCTIVITY// Cách tính năng xuất ngày 28/02/2020 FIX NHÂN HỆ SỐ ĐƠN HÀNG
                //Đơn giá > 0
                if (zPrice > 0)
                {
                    //TH 1.1 Số giờ >0 và số rổ=0 và số kí=0
                    if (Time > 0 && Basket == 0 && Kilogram == 0)
                    {
                        //TH 1.1.1  Ngày hiện tại đơn hàng là ngày lễ
                        if (zRateDay > 0)
                        {
                            ResultKg = 0;
                            if (float.TryParse((zTempTime * Time).ToString(), out ResultKg))
                            {
                                
                            }
                            ResultMoney = (ResultKg * zPrice * zRateDay * zRateOrder);
                        }
                        //TH 1.1.2  Ngày hiện tại đơn hàng không là ngày lễ và không phải là ngày chủ nhật
                        if (OrderDate.DayOfWeek != DayOfWeek.Sunday && zRateDay == 0)
                        {
                            ResultKg = 0;
                            if (float.TryParse((zTempTime * Time).ToString(), out ResultKg))
                            {

                            }
                            ResultMoney = (ResultKg * zPrice * zRateOrder);
                        }
                        //TH 1.1.3  Ngày hiện tại đơn hàng không là ngày lễ và là ngày chủ nhật
                        if (OrderDate.DayOfWeek == DayOfWeek.Sunday)
                        {
                            ResultKg = 0;
                            if (float.TryParse((zTempTime * Time).ToString(), out ResultKg))
                            {

                            }
                            ResultMoney = (ResultKg * zPrice * zRateOrder * _DayOfWeek);
                        }
                    }
                    //TH 1.2 (Số giờ >0 và số rổ=0 ) hoặc (Số giờ ==0 và số kí>0)
                    if (Time > 0 && Basket > 0 || Time == 0 && Basket > 0)
                    {
                        //TH 1.2.1  Ngày hiện tại đơn hàng là ngày lễ
                        if (zRateDay > 0)
                        {
                            ResultKg = 0;
                            if (float.TryParse((zTempBasket * Basket).ToString(), out ResultKg))
                            {

                            }
                            ResultMoney = 0;
                            if (float.TryParse((ResultKg * zPrice * zRateOrder * zRateDay).ToString(), out ResultMoney))
                            {

                            }

                            //ResultMoney = (ResultKg * zPrice * zRateOrder * zRateDay).ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider).ToFloat();
                        }
                        //TH 1.2.2  Ngày hiện tại đơn hàng không là ngày lễ và không phải là ngày chủ nhật
                        if (OrderDate.DayOfWeek != DayOfWeek.Sunday && zRateDay == 0)
                        {
                            ResultKg = 0;
                            if (float.TryParse((zTempBasket * Basket).ToString(), out ResultKg))
                            {

                            }
                            ResultMoney = 0;
                            if (float.TryParse((ResultKg * zPrice * zRateOrder).ToString(), out ResultMoney))
                            {

                            }
                            //ResultMoney = (ResultKg * zPrice * zRateOrder).ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider).ToFloat();
                        }
                        //TH 1.2.3  Ngày hiện tại đơn hàng không là ngày lễ và là ngày chủ nhật
                        if (OrderDate.DayOfWeek == DayOfWeek.Sunday)
                        {
                            ResultKg = 0;
                            if (float.TryParse((zTempBasket * Basket).ToString(), out ResultKg))
                            {

                            }
                            ResultMoney = 0;
                            if (float.TryParse((ResultKg * zPrice * _DayOfWeek * zRateOrder).ToString(), out ResultMoney))
                            {

                            }
                            //ResultMoney = (ResultKg * zPrice * _DayOfWeek * zRateOrder).ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider).ToFloat();
                        }
                    }
                    //TH 1.3 (Số giờ >0 và Số kí >0)
                    if (Time > 0 && Kilogram > 0)
                    {
                        //TH 1.3.1  Ngày hiện tại đơn hàng là ngày lễ
                        if (zRateDay > 0)
                        {
                            ResultKg = 0;
                            if (float.TryParse((zTempKg * Kilogram).ToString(), out ResultKg))
                            {

                            }
                            ResultMoney = (ResultKg * zPrice * zRateDay * zRateOrder);
                        }
                        //TH 1.3.2  Ngày hiện tại đơn hàng không là ngày lễ và không phải là ngày chủ nhật
                        if (OrderDate.DayOfWeek != DayOfWeek.Sunday && zRateDay == 0)
                        {
                            ResultKg = 0;
                            if (float.TryParse((zTempKg * Kilogram).ToString(), out ResultKg))
                            {

                            }
                            ResultMoney = (ResultKg * zPrice * zRateOrder);
                        }
                        //TH 1.3.3  Ngày hiện tại đơn hàng không là ngày lễ và là ngày chủ nhật
                        if (OrderDate.DayOfWeek == DayOfWeek.Sunday)
                        {
                            ResultKg = 0;
                            if (float.TryParse((zTempKg * Kilogram).ToString(), out ResultKg))
                            {

                            }
                            ResultMoney = (ResultKg * zPrice * _DayOfWeek * zRateOrder);
                        }
                    }
                }
                //Đơn giá = 0
                else
                {
                    int _Hour = HoTroSanXuat.LaySoGioLamHanhChanh(dte_ToDate.Value);     //thời gian làm việc hành chính
                    float _MMoney = HoTroSanXuat.LayDonGiaNam(dte_ToDate.Value);      //tiền danh cho công nhân nam
                    float _FMoney = HoTroSanXuat.LayDonGiaNu(dte_ToDate.Value);       //tiền danh cho công nhân nữ

                    float MoneyTime = 0;
                    if (Gender == 0)
                    {
                        MoneyTime = (Time / _Hour) * _MMoney;
                    }
                    else
                    {
                        MoneyTime = (Time / _Hour) * _FMoney;
                    }

                    ResultMoney = MoneyTime;
                }
                #endregion

                ///bổ sung thêm nhân % hoàn thành
                ResultMoney = (ResultMoney * zPercentFinish) / 100;

                int Borrow = r["Borrow"].ToInt();   //mượn
                int Private = r["Private"].ToInt();     //ăn riêng
                int Share = r["Share"].ToInt();    //ăn chung             

                _OrderMoney.OrderDate = OrderDate;
                _OrderMoney.EmployeeKey = r["EmployeeKey"].ToString();          //**KEY UDPATE**
                _OrderMoney.EmployeeID = r["EmployeeID"].ToString();
                _OrderMoney.EmployeeName = r["EmployeeName"].ToString();
                _OrderMoney.StageKey = _WorkKey;
                _OrderMoney.TeamKey = _TeamKey;
                _OrderMoney.OrderKey = _OrderKey;                                           //**KEY UDPATE**

                //-----BÌNH THƯỜNG ĂN CHUNG (TÌNH HUỐNG MẶT ĐỊNH)
                if ((Borrow == 0 && Private == 0 && Share == 0) ||
                   (Borrow == 0 && Private == 0 && Share == 1))
                {
                    _OrderMoney.Time = Time;
                    _OrderMoney.Kg = ResultKg;
                    _OrderMoney.Money = ResultMoney;
                    _OrderMoney.Basket = Basket;

                    _OrderMoney.Time_Private = 0;
                    _OrderMoney.Kg_Private = 0;
                    _OrderMoney.Money_Private = 0;
                    _OrderMoney.Basket_Private = 0;

                    _OrderMoney.Time_Borrow = 0;
                    _OrderMoney.Kg_Borrow = 0;
                    _OrderMoney.Money_Borrow = 0;
                    _OrderMoney.Basket_Borrow = 0;

                    _OrderMoney.Time_Eat = 0;
                    _OrderMoney.Kg_Eat = 0;
                    _OrderMoney.Money_Eat = 0;
                    _OrderMoney.Basket_Eat = 0;

                    _OrderMoney.Category = 1;//**Lưu ý**
                    _OrderMoney.Category_Borrow = 0;
                    _OrderMoney.Category_Eat = 0;
                    _OrderMoney.Category_Private = 0;
                }
                //-----BÌNH THƯỜNG ĂN RIÊNG
                if (Borrow == 0 && Private == 1 && Share == 0)
                {
                    _OrderMoney.Time = 0;
                    _OrderMoney.Money = 0;
                    _OrderMoney.Kg = 0;
                    _OrderMoney.Basket = 0;

                    _OrderMoney.Basket_Private = Basket;
                    _OrderMoney.Time_Private = Time;
                    _OrderMoney.Kg_Private = ResultKg;
                    _OrderMoney.Money_Private = ResultMoney;

                    _OrderMoney.Basket_Borrow = 0;
                    _OrderMoney.Time_Borrow = 0;
                    _OrderMoney.Kg_Borrow = 0;
                    _OrderMoney.Money_Borrow = 0;

                    _OrderMoney.Money_Eat = 0;
                    _OrderMoney.Time_Eat = 0;
                    _OrderMoney.Kg_Eat = 0;
                    _OrderMoney.Basket_Eat = 0;

                    _OrderMoney.Category = 0;
                    _OrderMoney.Category_Borrow = 0;
                    _OrderMoney.Category_Eat = 0;
                    _OrderMoney.Category_Private = 1;//**Lưu ý**
                }
                //-----MƯỢN MÀ ĂN RIÊNG
                if (Borrow == 1 && Private == 1 && Share == 0)
                {
                    _OrderMoney.Time = 0;
                    _OrderMoney.Money = 0;
                    _OrderMoney.Kg = 0;
                    _OrderMoney.Basket = 0;

                    _OrderMoney.Basket_Private = 0;
                    _OrderMoney.Time_Private = 0;
                    _OrderMoney.Kg_Private = 0;
                    _OrderMoney.Money_Private = 0;

                    _OrderMoney.Basket_Borrow = Basket;
                    _OrderMoney.Time_Borrow = Time;
                    _OrderMoney.Kg_Borrow = ResultKg;
                    _OrderMoney.Money_Borrow = ResultMoney;

                    _OrderMoney.Money_Eat = 0;
                    _OrderMoney.Time_Eat = 0;
                    _OrderMoney.Kg_Eat = 0;
                    _OrderMoney.Basket_Eat = 0;

                    _OrderMoney.Category = 0;
                    _OrderMoney.Category_Borrow = 1;//**Lưu ý**
                    _OrderMoney.Category_Eat = 0;
                    _OrderMoney.Category_Private = 0;
                }
                //-----MƯỢN MÀ ĂN CHUNG
                if (Borrow == 1 && Private == 0 && Share == 1)
                {
                    _OrderMoney.Time = 0;
                    _OrderMoney.Money = 0;
                    _OrderMoney.Kg = 0;
                    _OrderMoney.Basket = 0;

                    _OrderMoney.Basket_Private = 0;
                    _OrderMoney.Time_Private = 0;
                    _OrderMoney.Kg_Private = 0;
                    _OrderMoney.Money_Private = 0;

                    _OrderMoney.Basket_Borrow = 0;
                    _OrderMoney.Time_Borrow = 0;
                    _OrderMoney.Kg_Borrow = 0;
                    _OrderMoney.Money_Borrow = 0;

                    _OrderMoney.Money_Eat = ResultMoney;
                    _OrderMoney.Time_Eat = Time;
                    _OrderMoney.Kg_Eat = ResultKg;
                    _OrderMoney.Basket_Eat = Basket;

                    _OrderMoney.Category = 0;
                    _OrderMoney.Category_Borrow = 0;
                    _OrderMoney.Category_Eat = 1; //**Lưu ý**
                    _OrderMoney.Category_Private = 0;
                }

                //----- LƯU DB FTR_Order_Money (các khoản tính toán lại)
                Order_Exe OrderExe = new Order_Exe();
                OrderExe.OrderMoney = _OrderMoney;
                OrderExe.CreatedBy = SessionUser.UserLogin.Key;
                OrderExe.ModifiedBy = SessionUser.UserLogin.Key;
                OrderExe.CreatedName = SessionUser.UserLogin.EmployeeName;
                OrderExe.ModifiedName = SessionUser.UserLogin.EmployeeName;
                OrderExe.Save();

                if (OrderExe.Message != "11" &&
                    OrderExe.Message != "20")
                    ShowLog(OrderExe.Message);

                //----- lưu bảng FTR_Order_Employee
                Order_Employee_Info zEmployee = new Order_Employee_Info();
                zEmployee.EmployeeKey = r["EmployeeKey"].ToString();
                zEmployee.OrderKey = _OrderKey;
                zEmployee.OrderDate = OrderDate;
                zEmployee.Money = ResultMoney;
                zEmployee.UpdateMoney();

                //SCCĐ và SCĐH !.
                //Lưu thời gian riêng biệt cho 2 nhóm này !.
                if (_TeamKey == _TeamSCCD ||
                    _TeamKey == _TeamSCDH)
                {
                    int zCount = OrderExe.TimeExits();
                    Money_TimeKeep_Info zInfo = new Money_TimeKeep_Info();

                    zInfo.OrderKey = _OrderKey;
                    zInfo.DateTimeKeep = _OrderMoney.OrderDate;
                    zInfo.EmployeeKey = _OrderMoney.EmployeeKey;
                    zInfo.EmployeeID = _OrderMoney.EmployeeID;
                    zInfo.EmployeeName = _OrderMoney.EmployeeName;
                    zInfo.MoneyTimeKeep = ResultMoney;

                    if (zCount == 0)
                    {
                        zInfo.CreatedBy = SessionUser.UserLogin.Key;
                        zInfo.CreatedName = SessionUser.UserLogin.EmployeeName;
                        zInfo.Create();
                    }
                    else
                    {
                        zInfo.ModifiedBy = SessionUser.UserLogin.Key;
                        zInfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
                        zInfo.Update();
                    }
                }
                //----- 
                #endregion
            }
            else
            {
                //đông 17/09/2020 không thấy có trường hợp này nên chưa sữa hàm parse
                #region //VIỆC KHÁC

                #region [---------   Chuẩn bị các tham số   -----------]                        MAP VỚI [Count_Dif]
                float zTempKg = 0;
                float zTempBasket = 0;
                float zTempMoney = 0;

                DateTime OrderDate = _OrderModel.OrderDate; // ngày đơn hàng
                float zPrice = _OrderModel.Price;                       //giá công việc đơn hàng
                float zTotalBasket = _OrderModel.TotalBasket;  //số rỗ làm
                float zTotalTime = _OrderModel.TotalTime;       //thời gian làm
                float zTotalKg = _OrderModel.TotalKg;               //số ký
                float zRateOrder = _OrderModel.RateOrder;   //he số đơn hàng
                float zRateDay = _OrderModel.RateDay;           //hệ số ngày đơn hàng
                float zQuantity = _OrderModel.Quantity;         // số lượng 

                if (zTotalBasket > 0)
                {
                    if (zRateDay > 0)
                    {
                        zTempKg = (zQuantity / zTotalBasket) * zPrice * zRateOrder * zRateDay;
                        zTempBasket = zQuantity / zTotalBasket;
                        zTempMoney = zTempKg;
                    }
                    if (OrderDate.DayOfWeek != DayOfWeek.Sunday && zRateDay == 0)
                    {
                        zTempKg = (zQuantity / zTotalBasket) * zPrice * zRateOrder;
                        zTempBasket = zQuantity / zTotalBasket;
                        zTempMoney = zTempKg;
                    }
                    if (OrderDate.DayOfWeek == DayOfWeek.Sunday)
                    {
                        zTempKg = (zQuantity / zTotalBasket) * zPrice * _DayOfWeek;
                        zTempBasket = zQuantity / zTotalBasket;
                        zTempMoney = zTempKg;
                    }
                }
                else if (zTotalTime > 0)
                {
                    if (zRateDay > 0)
                    {
                        zTempKg = (zQuantity / zTotalTime) * zPrice * zRateOrder * zRateDay;
                        zTempBasket = zQuantity / zTotalTime;
                        zTempMoney = zTempKg;
                    }
                    if (OrderDate.DayOfWeek != DayOfWeek.Sunday && zRateDay == 0)
                    {
                        zTempKg = (zQuantity / zTotalTime) * zPrice * zRateOrder;
                        zTempBasket = zQuantity / zTotalTime;
                        zTempMoney = zTempKg;
                    }
                    if (OrderDate.DayOfWeek == DayOfWeek.Sunday)
                    {
                        zTempKg = (zQuantity / zTotalTime) * zPrice * _DayOfWeek;
                        zTempBasket = zQuantity / zTotalTime;
                        zTempMoney = zTempKg;
                    }
                }
                #endregion

                _OrderMoney = new Order_Money_Model();

                int Gender = r["Gender"].ToInt(); //Nam nu
                float Time = 0;
                if (float.TryParse(r["Time"].ToString(), out Time))
                {
                    //Số giờ 
                }
                float Kilogram = 0;
                if (float.TryParse(r["Kilogram"].ToString(), out Kilogram))
                {
                    //Số kí 
                }
                float Basket = 0;
                if (float.TryParse(r["Basket"].ToString(), out Basket))
                {
                    //Số rỗ 
                }
                float ResultKg = 0;     //ket quả số kg
                float ResultMoney = 0;// kết quả số tiền

                #region Tính toán số liệu >                                                             MAP VỚI [Count_Productivity_Dif]
                if (Kilogram > 0)
                {
                    ResultMoney = Kilogram * zTempMoney;
                }
                if (Basket > 0)
                {
                    ResultKg = Basket * zTempBasket;
                    ResultMoney = Basket * zTempMoney;
                }
                if (Time > 0)
                {
                    ResultMoney = Time * zTempMoney;
                }
                #endregion
                int Borrow = r["Borrow"].ToInt();   //mượn
                int Private = r["Private"].ToInt();     //ăn riêng
                int Share = r["Share"].ToInt();    //ăn chung

                _OrderMoney.OrderDate = OrderDate;
                _OrderMoney.EmployeeKey = r["EmployeeKey"].ToString();          //**KEY UDPATE**
                _OrderMoney.EmployeeID = r["EmployeeID"].ToString();
                _OrderMoney.EmployeeName = r["EmployeeName"].ToString();
                _OrderMoney.StageKey = _WorkKey;
                _OrderMoney.TeamKey = _TeamKey;
                _OrderMoney.OrderKey = _OrderKey;                                           //**KEY UDPATE**

                if (Borrow == 1 && Private == 1)
                {
                    if (zRateDay > 0)
                    {
                        _OrderMoney.Money_Dif_Holiday = ResultMoney;
                    }
                    if (OrderDate.DayOfWeek != DayOfWeek.Sunday && zRateDay == 0)
                    {
                        _OrderMoney.Money_Dif = ResultMoney;
                    }
                    if (OrderDate.DayOfWeek == DayOfWeek.Sunday)
                    {
                        _OrderMoney.Money_Dif_Sunday = ResultMoney;
                    }
                }
                if (Borrow == 1 && Share == 1)
                {
                    if (zRateDay > 0)
                    {
                        _OrderMoney.Money_Dif_Holiday_General = ResultMoney;
                    }
                    if (OrderDate.DayOfWeek != DayOfWeek.Sunday && zRateDay == 0)
                    {
                        _OrderMoney.Money_Dif_General = ResultMoney;
                    }
                    if (OrderDate.DayOfWeek == DayOfWeek.Sunday)
                    {
                        _OrderMoney.Money_Dif_Sunday_General = ResultMoney;
                    }
                }

                string InfoEmployee = r["EmployeeName"].ToString();
                if (Borrow == 1 && Private == 1)
                    ShowLog(InfoEmployee + " MƯỢN MÀ ĂN RIÊNG ");
                if (Borrow == 1 && Share == 1)
                    ShowLog(InfoEmployee + " MƯỢN MÀ ĂN CHUNG ");

                //----- LƯU DB
                Order_Exe OrderExe = new Order_Exe();
                OrderExe.OrderMoney = _OrderMoney;
                OrderExe.CreatedBy = SessionUser.UserLogin.Key;
                OrderExe.ModifiedBy = SessionUser.UserLogin.Key;
                OrderExe.CreatedName = SessionUser.UserLogin.EmployeeName;
                OrderExe.ModifiedName = SessionUser.UserLogin.EmployeeName;
                OrderExe.Save();

                if (OrderExe.Message != "11" && OrderExe.Message != "20")
                    ShowLog(OrderExe.Message);
                #endregion               
            }
        }
        //----------------------------------------------------------------------------

        private void btn_Run_Click(object sender, EventArgs e)
        {
            HeaderControl.Text = HeaderControl.Text + "[Đang xử lý, vui lòng chờ !.]";

            _IndexEmp = 0;
            _IndexOrder = 0;
            _IndexTeam = 0;
            _IndexWork = 0;

            LoadTeam();
            timerTeam.Start();
        }

        //Tick
        private void timerTeam_Tick(object sender, EventArgs e)
        {
            //tạm dừng vòng team
            timerTeam.Stop();
            PicLoading.Visible = true;
            btn_Run.Enabled = false;
            dte_FromDate.Enabled = false;
            dte_ToDate.Enabled = false;

            if (_IndexTeam < _TotalTeam)
            {
                if (_TableTeam.Rows.Count > 0)
                {
                    DataRow r = _TableTeam.Rows[_IndexTeam];
                    _TeamName = r["TeamName"].ToString();
                    _TeamKey = r["TeamKey"].ToInt();


                    //add nhóm đang xử lý vào view
                    ShowGVTeam(r, GVTeam);

                    //Phân loại công việc
                    if (_TeamKey == 98)
                    {
                        _WorkDif = true;
                    }
                    else
                    {
                        _WorkDif = false;
                    }

                    //xử lý tiếp data thông tin công việc của nhóm
                    LoadWork(_TeamKey);

                    _IndexWork = 0;
                    timerWork.Start();
                }
                else
                {
                    timerTeam.Stop();
                    PicLoading.Visible = false;
                    HeaderControl.Text = "Tính năng xuất tại tổ !.";
                    btn_Run.Enabled = true;
                    dte_FromDate.Enabled = true;
                    dte_ToDate.Enabled = true;

                    LogTracking("Đã thực hiện xong !.");
                    //this.Close();
                }
            }
            else
            {
                //tạm dừng vòng team
                timerTeam.Stop();
                PicLoading.Visible = false;
                HeaderControl.Text = "Tính năng xuất tại tổ !.";
                btn_Run.Enabled = true;
                dte_FromDate.Enabled = true;
                dte_ToDate.Enabled = true;

                LogTracking("Đã thực hiện xong !.");
                this.Close();
            }
        }
        private void timerWork_Tick(object sender, EventArgs e)
        {
            //tạm dừng vòng cong viec
            timerWork.Stop();

            if (_IndexWork < _TotalWork)
            {
                if (_TableWork.Rows.Count > 0)
                {
                    DataRow r = _TableWork.Rows[_IndexWork];
                    _WorkName = r["StageName"].ToString();  //tên cong viec
                    _WorkKey = r["StageKey"].ToInt();           //key cong viec
                    _WorkPrice = 0;
                    if(float.TryParse(r["StagePrice"].ToString(), out _WorkPrice))
                    {
                        //gia cong viec
                    } 

                    //xử lý tiếp data thông tin đơn hàng của công việc
                    LoadOrder(_TeamKey, _WorkKey);

                    //chay tiếp vòng đơn hàng
                    _IndexOrder = 0;
                    timerOrder.Start();
                }
                else
                {
                    //kết thúc vòng cùa con work của (team)
                    //quay lại vòng cha trước đó (team)
                    _IndexTeam++;
                    timerTeam.Start();
                }
            }
            else
            {
                //kết thúc vòng cùa con work của (team)
                //quay lại vòng cha trước đó (team)
                _IndexTeam++;
                timerTeam.Start();
            }
        }
        private void timerOrder_Tick(object sender, EventArgs e)
        {
            //tam dừng vòng oder
            timerOrder.Stop();

            if (_IndexOrder < _TotalOrder)
            {
                if (_TableOrder.Rows.Count > 0)
                {
                    DataRow r = _TableOrder.Rows[_IndexOrder];
                    _OrderKey = r["OrderKey"].ToString();
                    if (!IsAuto)
                    {
                        _TeamKey = r["TeamKey"].ToInt();
                        _WorkKey= r["Category_Stage"].ToInt();
                    }
                    _OrderModel = new Order_Model();
                    _OrderModel.OrderKey = r["OrderKey"].ToString();
                    _OrderModel.OrderID = r["OrderID"].ToString();
                    _OrderModel.OrderDate = Convert.ToDateTime(r["OrderDate"]);

                    float zQuantity = 0;
                    if (float.TryParse(r["Quantity"].ToString(), out zQuantity))
                    {
                        
                    }
                    _OrderModel.Quantity = zQuantity;
                    float zTotalKg = 0;
                    if (float.TryParse(r["TotalKg"].ToString(), out zTotalKg))
                    {

                    }
                    _OrderModel.TotalKg =zTotalKg;
                    float zTotalBasket = 0;
                    if (float.TryParse(r["TotalBasket"].ToString(), out zTotalBasket))
                    {

                    }
                    _OrderModel.TotalBasket = zTotalBasket;
                    float zTotalTime = 0;
                    if (float.TryParse(r["TotalTime"].ToString(), out zTotalTime))
                    {

                    }
                    _OrderModel.TotalTime = zTotalTime;
                    float zRateOrder = 0;
                    if (float.TryParse(r["RateOrder"].ToString(), out zRateOrder))
                    {

                    }
                    _OrderModel.RateOrder = zRateOrder;
                    float zRateDay = 0;
                    if (float.TryParse(r["RateDay"].ToString(), out zRateDay))
                    {

                    }
                    _OrderModel.RateDay = zRateDay;
                    //_OrderModel.Price =_WorkPrice; //r["Price"].ToFloat();  
                    float zPrice = 0;
                    if (float.TryParse(r["Price"].ToString(), out zPrice))
                    {

                    }
                    _OrderModel.Price = zPrice;
                    float zPercentOrder = 0;
                    if (float.TryParse(r["PercentOrder"].ToString(), out zPercentOrder))
                    {

                    }
                    _OrderModel.PercentFinish = zPercentOrder;

                    LoadEmployee(_OrderKey);

                    //chạy tiếp vòng emp         
                    _IndexEmp = 0;
                    timerEmployee.Start();
                }
                else
                {
                    //kết thúc vòng cùa con order của (work)
                    //quay lại vòng cha trước đó (work)
                    _IndexWork++;
                    timerWork.Start();
                }
            }
            else
            {
                if (!IsAuto)
                {
                    LogTracking("Đã thực hiện xong !.");
                    this.Close();
                }

                //kết thúc vòng cùa con order của (work)
                //quay lại vòng cha trước đó (work)
                _IndexWork++;
                timerWork.Start();
            }
        }
        private void timerEmployee_Tick(object sender, EventArgs e)
        {
            timerEmployee.Stop();
            if (_IndexEmp < _TotalEmp)
            {
                if (_TableEmployee.Rows.Count > 0)
                {
                    DataRow r = _TableEmployee.Rows[_IndexEmp];
                    ProcessEmployee(r);

                    _IndexEmp++;
                    timerEmployee.Start();
                }
                else
                {
                    //kết thúc vòng cùa con employee của (order)
                    //quay lại vòng cha trước đó (order)
                    _IndexOrder++;
                    timerOrder.Start();
                }
            }
            else
            {
                //kết thúc vòng cùa con employee của (order)
                //quay lại vòng cha trước đó (order)
                _IndexOrder++;
                timerOrder.Start();
            }
        }

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region [Log]
        private void LogTracking(string Message)
        {
            AUT_Run zAuto = new AUT_Run();
            zAuto.RecordStatus = 1;
            zAuto.AutoKey = AutoKey;
            if (Message != string.Empty)
            {
                zAuto.Error = Message;
            }
            zAuto.Update();
        }
        private void btnHideLog_Click(object sender, EventArgs e)
        {
            Panel_Right.Visible = false;
            txtLog.Visible = false;
            btnShowLog.Visible = true;
            btnHideLog.Visible = false;
        }
        private void btnShowLog_Click(object sender, EventArgs e)
        {
            Panel_Right.Visible = true;
            txtLog.Visible = true;
            btnShowLog.Visible = false;
            btnHideLog.Visible = true;
        }
        void ShowLog(string Text)
        {
            txtLog.AppendText(Text + Environment.NewLine);
            txtLog.ScrollToCaret();
        }
        #endregion

        private void Frm_Calculator_Auto_FormClosing(object sender, FormClosingEventArgs e)
        {
            LogTracking("Đóng");
        }
    }
}