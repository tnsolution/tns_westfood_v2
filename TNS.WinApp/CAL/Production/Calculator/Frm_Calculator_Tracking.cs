﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using TNS.CORE;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Calculator_Tracking : Form
    {
        public Frm_Calculator_Tracking()
        {
            InitializeComponent();

            InitGV_Layout(GVData);
            Utils.DrawGVStyle(ref GVData);
            Utils.DoubleBuffered(GVData, true);
        }
        private void Frm_Main_Running_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);

            DateTime ViewDate = SessionUser.Date_Work;
            DateTime FromDate = new DateTime(ViewDate.Year, ViewDate.Month, 1, 0, 0, 0);
            DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            dte_FromDate.Value = FromDate;
            dte_ToDate.Value = ToDate;
            //Lấy tất cả nhóm trực tiếp sx  và khác bộ phận hỗ trợ sx
            string zSQL = @"SELECT TeamKey,TeamID +'-'+ TeamName AS TeamName  FROM SYS_Team 
                            WHERE RecordStatus <> 99 AND BranchKey=4 AND  DepartmentKey != 26
                            ORDER BY Rank";
            LoadDataToToolbox.KryptonComboBox(cboTeam, zSQL, "--Tất cả nhóm--");
        }
        private void InitGV_Layout(DataGridView GV)
        {
            GV.Columns.Add("No", "#");
            GV.Columns.Add("Title", "Tiêu đề");
            GV.Columns.Add("TimeBegin", "Bắt đầu");
            GV.Columns.Add("TimeEnd", "Kết thúc");
            GV.Columns.Add("TimeSpen", "Xử lý");
            GV.Columns.Add("Status", "Tình trạng");

            GV.Columns["No"].Width = 45;
            GV.Columns["Title"].Width = 300;
            GV.ColumnHeadersHeight = 25;
            GV.FirstDisplayedScrollingRowIndex = GV.RowCount - 1;
        }
        private void InitGV_Data(DataGridView GV, DataTable Table)
        {
            GV.Rows.Clear();
            if (Table.Rows.Count > 0)
            {
                for (int i = 0; i < Table.Rows.Count; i++)
                {
                    DataRow r = Table.Rows[i];

                    GV.Rows.Add();
                    DataGridViewRow nRowView = GV.Rows[i];
                    nRowView.Cells["No"].Value = (i + 1);
                    nRowView.Cells["Title"].Value = r["Title"].ToString();


                    if (r["TimeBegin"] != null &&
                        r["TimeBegin"].ToString() != string.Empty)
                        nRowView.Cells["TimeBegin"].Value = Convert.ToDateTime(r["TimeBegin"]).ToString("hh:mm:ss");

                    if (r["TimeEnd"] != null &&
                        r["TimeEnd"].ToString() != string.Empty)
                        nRowView.Cells["TimeEnd"].Value = Convert.ToDateTime(r["TimeEnd"]).ToString("hh:mm:ss");

                    if (r["RecordStatus"].ToInt() != 1)
                        nRowView.Cells["Status"].Value = "Đang tính toán";
                    else
                    {
                        nRowView.Cells["Status"].Value = "Đã hoàn thành";
                        nRowView.Cells["TimeSpen"].Value = Utils.TimeAgo(Convert.ToDateTime(r["TimeBegin"]), Convert.ToDateTime(r["TimeEnd"]));
                    }
                }

                int Finished = Table.Select("RecordStatus = 0").Length;
                if (Finished > 0)
                {
                    PicLoading.Visible = true;
                    btn_Run.Enabled = false;

                    dte_FromDate.Enabled = false;
                    dte_ToDate.Enabled = false;
                    btnClose.Visible = false;
                }
                else
                {
                    PicLoading.Visible = false;
                    btn_Run.Enabled = true;

                    dte_FromDate.Enabled = true;
                    dte_ToDate.Enabled = true;
                    btnClose.Visible = true;
                }
            }
        }
        private void btn_Run_Click(object sender, EventArgs e)
        {
            if (rdoTeam.Checked)
            {
                if (cboTeam.SelectedIndex <= 0)
                {
                    Utils.TNMessageBoxOK("Vui lòng kiểm tra lại thông tin nhóm !.",1);
                    return;
                }
                if (SessionUser.Date_Lock >= dte_ToDate.Value)
                {
                    Utils.TNMessageBoxOK("Thời gian này đã khóa sổ.!", 2);
                    return;
                }
                if (dte_FromDate.Value.Month != dte_ToDate.Value.Month)
                {
                    Utils.TNMessageBoxOK("Vui lòng chỉ chọn trong 1 tháng !", 1);
                    return;
                }
                if (dte_FromDate.Value.Day > dte_ToDate.Value.Day)
                {
                    Utils.TNMessageBoxOK("Ngày bắt đầu không được phép lớn hơn ngày kết thúc !", 1);
                    return;
                }
                //
                btnClose.Visible = false;
                btn_Run.Enabled = false;
                dte_FromDate.Enabled = false;
                dte_ToDate.Enabled = false;
                PicLoading.Visible = true;
                GVData.Rows.Clear();
                AUT_Run.Delete(SessionUser.UserLogin.Name);
                

                int stt = 0;
                int end = dte_FromDate.Value.Day;
                int no = txt_FormNo.Text.ToInt();
                int Year = dte_ToDate.Value.Year;
                int Month = dte_ToDate.Value.Month;

                string title = string.Empty;
                string month = string.Empty;
                string error = string.Empty;

                DateTime DateStart;
                DateTime DateEnd;

                while (end < dte_ToDate.Value.Day)
                {
                    //kiem tra ngày kết thúc khi tính toán có vượt quá 31 ngày
                    int atempStart = end;
                    int atempEnd = end + no - 1;
                    if (atempEnd > 31)
                    {
                        atempStart = end;
                        atempEnd = end + 1;

                        DateStart = new DateTime(Year, Month, atempStart, 0, 0, 0);
                        DateEnd = new DateTime(Year, Month, atempEnd, 23, 59, 59);
                    }
                    else
                    {
                        DateStart = new DateTime(Year, Month, end, 0, 0, 0);
                        DateEnd = new DateTime(Year, Month, end + no - 1, 23, 59, 59);
                    }

                    end = end + no;

                    title = "Thực hiện tính dữ liệu năng xuất ngày " + DateStart.ToString("dd/MM/yyyy");
                    month = DateEnd.ToString("/MM/yyyy");
                    error = string.Empty;

                    //ShowGV(DateStart.ToString("dd/MM/yyyy") + "-" + DateEnd.ToString("dd/MM/yyyy"), GVData, stt);
                    int Key = TrackAuto(month, title, error);

                    OpenForm(DateStart, DateEnd, Key);

                    stt++;
                }

                if (end >= dte_ToDate.Value.Day)
                {
                    end = dte_ToDate.Value.Day;
                    DateStart = new DateTime(Year, Month, end, 0, 0, 0);
                    DateEnd = new DateTime(Year, Month, end, 23, 59, 59);

                    title = "Thực hiện tính dữ liệu năng xuất ngày " + DateStart.ToString("dd/MM/yyyy");
                    month = DateEnd.ToString("/MM/yyyy");
                    error = string.Empty;

                    //ShowGV(title, GVData, stt);
                    int Key = TrackAuto(month, title, error);
                    OpenForm(DateStart, DateEnd, Key);
                }
                //
            }

            if (rdoOrder.Checked)
            {
                if (txt_OrderID.Text.Trim() == string.Empty)
                {
                    Utils.TNMessageBoxOK("Vui lòng kiểm tra lại thông tin đơn hàng !.",1);
                    return;
                }
                else
                {
                    if (Order_Data.FindOrderID(txt_OrderID.Text.Trim()) == 0)
                    {
                        Utils.TNMessageBoxOK("Vui lòng kiểm tra lại thông tin đơn hàng không tồn tại !.",1);
                        return;
                    }
                }

                string title = "Thực hiện tính dữ liệu năng xuất đơn hàng "+ txt_OrderID.Text.Trim() +" lúc "+ DateTime.Now.ToString("dd/MM/yyyy HH:mm");
                string month = "";
                string error = string.Empty;

                int Key = TrackAuto(month, title, error);
                OpenForm(DateTime.Now, DateTime.Now, Key);
            }            

            DataTable zTable = HoTroSanXuat.TheoDoi(SessionUser.UserLogin.Name);
            InitGV_Data(GVData, zTable);

            this.BringToFront();
        }

        private void OpenForm(DateTime DateStart, DateTime DateEnd, int Key)
        {
            Frm_Calculator_Auto_V2 Frm = new Frm_Calculator_Auto_V2();

            if (rdoTeam.Checked && Key != 0)
            {
                if (cboTeam.SelectedIndex > 0)
                {
                    Frm.TeamTracking = cboTeam.SelectedValue.ToInt();
                    Frm.IsAuto = true;
                }
            }
            if (txt_OrderID.Text.Trim() != string.Empty)//&& Key == 0
            {
                Frm.OrderTracking = txt_OrderID.Text.Trim();
                Frm.IsAuto = false;
            }

            Frm.FormClosed += Frm_FormClosed;
            Frm.FromDate = DateStart;
            Frm.ToDate = DateEnd;
            Frm.AutoKey = Key;
           
            Frm.ShowInTaskbar = true;
            Frm.ControlBox = true;
            Frm.Text = string.Empty;
            Frm.WindowState = FormWindowState.Minimized;
            Frm.Show();
            Frm.Hide();
        }

        private void Frm_FormClosed(object sender, FormClosedEventArgs e)
        {
            ((Form)sender).FormClosed -= Frm_FormClosed;
            DataTable zTable = HoTroSanXuat.TheoDoi(SessionUser.UserLogin.Name);
            InitGV_Data(GVData, zTable);
        }

        /// <summary>
        /// Lưu bảng auto run và lấy key để theo dõi update tình trạng ở các form tự động chạy
        /// </summary>
        /// <param name="Month"></param>
        /// <param name="Title"></param>
        /// <param name="Error"></param>
        /// <returns></returns>
        int TrackAuto(string Month, string Title, string Error)
        {
            AUT_Run zAuto = new AUT_Run();
            zAuto.Month = Month;
            zAuto.Title = Title;
            zAuto.Error = Error;
            zAuto.CreatedBy = SessionUser.UserLogin.Name;
            zAuto.Insert();

            return zAuto.AutoKey;
        }

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        private void rdoTeam_CheckedChanged(object sender, EventArgs e)
        {
            txt_OrderID.Clear();
            txt_OrderID.Enabled = false;
            cboTeam.Enabled = true;
            dte_FromDate.Enabled = true;
            dte_ToDate.Enabled = true;
        }

        private void rdoOrder_CheckedChanged(object sender, EventArgs e)
        {
            cboTeam.SelectedIndex = -1;
            cboTeam.Enabled = false;
            txt_OrderID.Enabled = true;
            dte_FromDate.Enabled = false;
            dte_ToDate.Enabled = false;
        }

        private void txt_OrderID_Leave(object sender, EventArgs e)
        {
            if (Order_Data.FindOrderID(txt_OrderID.Text.Trim()) == 0)
            {
                Utils.TNMessageBoxOK("Vui lòng kiểm tra lại thông tin đơn hàng không tồn tại !.",1);
                return;
            }
        }
    }
}