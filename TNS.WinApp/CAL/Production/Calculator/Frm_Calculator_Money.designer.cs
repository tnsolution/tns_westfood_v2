﻿namespace TNS.WinApp
{
    partial class Frm_Calculator_Money
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Calculator_Money));
            this.Panel_Search = new System.Windows.Forms.Panel();
            this.cboTeam = new ComponentFactory.Krypton.Toolkit.KryptonComboBox();
            this.PicLoading = new System.Windows.Forms.PictureBox();
            this.btn_Run = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.txtTitle = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btnMini = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnMax = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnClose = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.dte_ToDate = new TNS.SYS.TNDateTimePicker();
            this.dte_FromDate = new TNS.SYS.TNDateTimePicker();
            this.timerTeam = new System.Windows.Forms.Timer(this.components);
            this.Panel_Right = new System.Windows.Forms.Panel();
            this.txtLog = new System.Windows.Forms.TextBox();
            this.kryptonHeader2 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.Panel_Team = new System.Windows.Forms.Panel();
            this.GVTeam = new System.Windows.Forms.DataGridView();
            this.kryptonHeader1 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btnShowLog = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnHideLog = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.timerDate = new System.Windows.Forms.Timer(this.components);
            this.timerEmployee = new System.Windows.Forms.Timer(this.components);
            this.Panel_Search.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboTeam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicLoading)).BeginInit();
            this.Panel_Right.SuspendLayout();
            this.Panel_Team.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GVTeam)).BeginInit();
            this.SuspendLayout();
            // 
            // Panel_Search
            // 
            this.Panel_Search.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Search.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel_Search.Controls.Add(this.cboTeam);
            this.Panel_Search.Controls.Add(this.PicLoading);
            this.Panel_Search.Controls.Add(this.btn_Run);
            this.Panel_Search.Controls.Add(this.txtTitle);
            this.Panel_Search.Controls.Add(this.dte_ToDate);
            this.Panel_Search.Controls.Add(this.dte_FromDate);
            this.Panel_Search.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel_Search.Location = new System.Drawing.Point(0, 0);
            this.Panel_Search.Name = "Panel_Search";
            this.Panel_Search.Size = new System.Drawing.Size(1000, 106);
            this.Panel_Search.TabIndex = 205;
            // 
            // cboTeam
            // 
            this.cboTeam.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTeam.DropDownWidth = 119;
            this.cboTeam.Location = new System.Drawing.Point(589, 49);
            this.cboTeam.Name = "cboTeam";
            this.cboTeam.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.cboTeam.Size = new System.Drawing.Size(337, 24);
            this.cboTeam.StateCommon.ComboBox.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.cboTeam.StateCommon.ComboBox.Border.Rounding = 4;
            this.cboTeam.StateCommon.ComboBox.Border.Width = 1;
            this.cboTeam.StateCommon.ComboBox.Content.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cboTeam.StateCommon.Item.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.cboTeam.StateCommon.Item.Border.Rounding = 4;
            this.cboTeam.StateCommon.Item.Border.Width = 1;
            this.cboTeam.StateCommon.Item.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cboTeam.TabIndex = 208;
            // 
            // PicLoading
            // 
            this.PicLoading.BackColor = System.Drawing.Color.Transparent;
            this.PicLoading.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.PicLoading.Dock = System.Windows.Forms.DockStyle.Right;
            this.PicLoading.Image = ((System.Drawing.Image)(resources.GetObject("PicLoading.Image")));
            this.PicLoading.Location = new System.Drawing.Point(930, 42);
            this.PicLoading.Name = "PicLoading";
            this.PicLoading.Size = new System.Drawing.Size(68, 62);
            this.PicLoading.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.PicLoading.TabIndex = 207;
            this.PicLoading.TabStop = false;
            this.PicLoading.Visible = false;
            // 
            // btn_Run
            // 
            this.btn_Run.Location = new System.Drawing.Point(167, 49);
            this.btn_Run.Name = "btn_Run";
            this.btn_Run.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Run.Size = new System.Drawing.Size(102, 40);
            this.btn_Run.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Run.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Run.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Run.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Run.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Run.TabIndex = 206;
            this.btn_Run.Tag = "0";
            this.btn_Run.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Run.Values.Image")));
            this.btn_Run.Values.Text = "Khởi động";
            this.btn_Run.Click += new System.EventHandler(this.btn_Run_Click);
            // 
            // txtTitle
            // 
            this.txtTitle.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnMini,
            this.btnMax,
            this.btnClose});
            this.txtTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtTitle.Location = new System.Drawing.Point(0, 0);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txtTitle.Size = new System.Drawing.Size(998, 42);
            this.txtTitle.TabIndex = 205;
            this.txtTitle.Values.Description = "";
            this.txtTitle.Values.Heading = "Lương năng suất";
            this.txtTitle.Values.Image = ((System.Drawing.Image)(resources.GetObject("txtTitle.Values.Image")));
            this.txtTitle.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Frm_Main_MouseDown);
            this.txtTitle.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Frm_Main_MouseMove);
            this.txtTitle.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Frm_Main_MouseUp);
            // 
            // btnMini
            // 
            this.btnMini.Image = ((System.Drawing.Image)(resources.GetObject("btnMini.Image")));
            this.btnMini.UniqueName = "F5F06E8241504E72ABB5BEA3F6A9B753";
            // 
            // btnMax
            // 
            this.btnMax.Image = ((System.Drawing.Image)(resources.GetObject("btnMax.Image")));
            this.btnMax.UniqueName = "035D1A4881E44F58A084C31DE7352A94";
            this.btnMax.Visible = false;
            // 
            // btnClose
            // 
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.UniqueName = "11B07C6F4E1C4F9D8B91BD924CB0EBE6";
            // 
            // dte_ToDate
            // 
            this.dte_ToDate.CustomFormat = "dd/MM/yyyy";
            this.dte_ToDate.Location = new System.Drawing.Point(12, 74);
            this.dte_ToDate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dte_ToDate.Name = "dte_ToDate";
            this.dte_ToDate.Size = new System.Drawing.Size(149, 28);
            this.dte_ToDate.TabIndex = 0;
            this.dte_ToDate.Value = new System.DateTime(((long)(0)));
            // 
            // dte_FromDate
            // 
            this.dte_FromDate.CustomFormat = "dd/MM/yyyy";
            this.dte_FromDate.Location = new System.Drawing.Point(12, 49);
            this.dte_FromDate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dte_FromDate.Name = "dte_FromDate";
            this.dte_FromDate.Size = new System.Drawing.Size(149, 28);
            this.dte_FromDate.TabIndex = 0;
            this.dte_FromDate.Value = new System.DateTime(((long)(0)));
            // 
            // timerTeam
            // 
            this.timerTeam.Enabled = true;
            this.timerTeam.Tick += new System.EventHandler(this.timerTeam_Tick);
            // 
            // Panel_Right
            // 
            this.Panel_Right.Controls.Add(this.txtLog);
            this.Panel_Right.Controls.Add(this.kryptonHeader2);
            this.Panel_Right.Dock = System.Windows.Forms.DockStyle.Right;
            this.Panel_Right.Location = new System.Drawing.Point(650, 106);
            this.Panel_Right.Name = "Panel_Right";
            this.Panel_Right.Size = new System.Drawing.Size(350, 494);
            this.Panel_Right.TabIndex = 211;
            // 
            // txtLog
            // 
            this.txtLog.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLog.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLog.Location = new System.Drawing.Point(0, 30);
            this.txtLog.Multiline = true;
            this.txtLog.Name = "txtLog";
            this.txtLog.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtLog.Size = new System.Drawing.Size(350, 464);
            this.txtLog.TabIndex = 210;
            this.txtLog.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtLog.Visible = false;
            // 
            // kryptonHeader2
            // 
            this.kryptonHeader2.AutoSize = false;
            this.kryptonHeader2.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader2.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader2.Name = "kryptonHeader2";
            this.kryptonHeader2.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader2.Size = new System.Drawing.Size(350, 30);
            this.kryptonHeader2.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader2.TabIndex = 205;
            this.kryptonHeader2.Values.Description = "";
            this.kryptonHeader2.Values.Heading = "Theo dõi";
            // 
            // Panel_Team
            // 
            this.Panel_Team.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Team.Controls.Add(this.GVTeam);
            this.Panel_Team.Controls.Add(this.kryptonHeader1);
            this.Panel_Team.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel_Team.Location = new System.Drawing.Point(0, 106);
            this.Panel_Team.Name = "Panel_Team";
            this.Panel_Team.Size = new System.Drawing.Size(650, 494);
            this.Panel_Team.TabIndex = 212;
            // 
            // GVTeam
            // 
            this.GVTeam.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.GVTeam.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.GVTeam.ColumnHeadersHeight = 25;
            this.GVTeam.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.GVTeam.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GVTeam.Location = new System.Drawing.Point(0, 30);
            this.GVTeam.Name = "GVTeam";
            this.GVTeam.ReadOnly = true;
            this.GVTeam.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GVTeam.Size = new System.Drawing.Size(650, 464);
            this.GVTeam.TabIndex = 203;
            // 
            // kryptonHeader1
            // 
            this.kryptonHeader1.AutoSize = false;
            this.kryptonHeader1.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnShowLog,
            this.btnHideLog});
            this.kryptonHeader1.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader1.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader1.Name = "kryptonHeader1";
            this.kryptonHeader1.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader1.Size = new System.Drawing.Size(650, 30);
            this.kryptonHeader1.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader1.TabIndex = 200;
            this.kryptonHeader1.Values.Description = "";
            this.kryptonHeader1.Values.Heading = "1 - Tổ/ nhóm";
            // 
            // btnShowLog
            // 
            this.btnShowLog.Image = ((System.Drawing.Image)(resources.GetObject("btnShowLog.Image")));
            this.btnShowLog.UniqueName = "ABE0F8FEF4FF4DE3448D0BAE7F1C6F22";
            this.btnShowLog.Click += new System.EventHandler(this.btnShowLog_Click);
            // 
            // btnHideLog
            // 
            this.btnHideLog.Image = ((System.Drawing.Image)(resources.GetObject("btnHideLog.Image")));
            this.btnHideLog.UniqueName = "D6E0AB28C2CE49FBDD866175ABE4E274";
            this.btnHideLog.Click += new System.EventHandler(this.btnHideLog_Click);
            // 
            // timerDate
            // 
            this.timerDate.Enabled = true;
            this.timerDate.Tick += new System.EventHandler(this.timerDate_Tick);
            // 
            // timerEmployee
            // 
            this.timerEmployee.Enabled = true;
            this.timerEmployee.Tick += new System.EventHandler(this.timerEmployee_Tick);
            // 
            // Frm_Calculator_Money
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1000, 600);
            this.Controls.Add(this.Panel_Team);
            this.Controls.Add(this.Panel_Right);
            this.Controls.Add(this.Panel_Search);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_Calculator_Money";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Lương năng xuất";
            this.Load += new System.EventHandler(this.Frm_Production_Money_View_Load);
            this.Panel_Search.ResumeLayout(false);
            this.Panel_Search.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboTeam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicLoading)).EndInit();
            this.Panel_Right.ResumeLayout(false);
            this.Panel_Right.PerformLayout();
            this.Panel_Team.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GVTeam)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel Panel_Search;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader txtTitle;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMini;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMax;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnClose;
        private SYS.TNDateTimePicker dte_ToDate;
        private SYS.TNDateTimePicker dte_FromDate;
        private System.Windows.Forms.Timer timerTeam;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Run;
        private System.Windows.Forms.PictureBox PicLoading;
        private System.Windows.Forms.Panel Panel_Right;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader2;
        private System.Windows.Forms.TextBox txtLog;
        private System.Windows.Forms.Panel Panel_Team;
        private System.Windows.Forms.DataGridView GVTeam;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader1;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnShowLog;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnHideLog;
        private System.Windows.Forms.Timer timerDate;
        private System.Windows.Forms.Timer timerEmployee;
        private ComponentFactory.Krypton.Toolkit.KryptonComboBox cboTeam;
    }
}