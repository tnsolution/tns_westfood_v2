﻿namespace TNS.WinApp
{
    partial class Frm_Production_Calculator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Production_Calculator));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.GVData1 = new System.Windows.Forms.DataGridView();
            this.kryptonHeader5 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.Panel_Right = new System.Windows.Forms.Panel();
            this.Panel_Order_Rate = new System.Windows.Forms.Panel();
            this.GVRate1 = new System.Windows.Forms.DataGridView();
            this.kryptonHeader4 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btnHide1 = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnUnhide1 = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.Panel_Temp1 = new System.Windows.Forms.Panel();
            this.kryptonHeader11 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.txt_KGTB = new System.Windows.Forms.TextBox();
            this.txt_Kg_Bas = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_Money_TB = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.txt_TB_Time = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.Panel_Order = new System.Windows.Forms.Panel();
            this.label26 = new System.Windows.Forms.Label();
            this.kryptonHeader3 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.txt_Quantity = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_Kilogram = new System.Windows.Forms.TextBox();
            this.txt_Stage = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.txt_Time = new System.Windows.Forms.TextBox();
            this.txt_Price = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.txt_Basket = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.Panel_Left = new System.Windows.Forms.Panel();
            this.LVData1 = new System.Windows.Forms.ListView();
            this.kryptonHeader1 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.Panel_Info = new System.Windows.Forms.Panel();
            this.dte_Date1 = new TNS.SYS.TNDateTimePicker();
            this.cbo_Stage = new System.Windows.Forms.ComboBox();
            this.kryptonHeader2 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.label34 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.cbo_Team = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.btnRun1 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.txt_OrderID = new System.Windows.Forms.TextBox();
            this.btn_Count = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Save = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.GVData2 = new System.Windows.Forms.DataGridView();
            this.kryptonHeader10 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.Panel_Right_2 = new System.Windows.Forms.Panel();
            this.Panel_Order_Rate_2 = new System.Windows.Forms.Panel();
            this.GVRate2 = new System.Windows.Forms.DataGridView();
            this.kryptonHeader9 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btnHide2 = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnUnhide2 = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.kryptonHeader8 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.txt_Quantity_Dif = new System.Windows.Forms.TextBox();
            this.txt_Stage_Dif = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txt_Kilogram_Dif = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.txt_Time_Dif = new System.Windows.Forms.TextBox();
            this.txt_Basket_Dif = new System.Windows.Forms.TextBox();
            this.txt_Price_Dif = new System.Windows.Forms.TextBox();
            this.Panel_Temp2 = new System.Windows.Forms.Panel();
            this.kryptonHeader12 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.txt_Kg_Bas_Dif = new System.Windows.Forms.TextBox();
            this.txt_Money_TB_Dif = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.txt_TB_Time_Dif = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.Panel_Left_2 = new System.Windows.Forms.Panel();
            this.LVData2 = new System.Windows.Forms.ListView();
            this.kryptonHeader7 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.Panel_Info_2 = new System.Windows.Forms.Panel();
            this.dte_Date2 = new TNS.SYS.TNDateTimePicker();
            this.cbo_Stage_Dif = new System.Windows.Forms.ComboBox();
            this.kryptonHeader6 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.label6 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.cbo_Team_Dif = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.btn_Timer_Dif = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Count_Dif = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Save_Dif = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.txt_OrderID_Dif = new System.Windows.Forms.TextBox();
            this.timerTeam = new System.Windows.Forms.Timer(this.components);
            this.timerWork = new System.Windows.Forms.Timer(this.components);
            this.Timer_Dif = new System.Windows.Forms.Timer(this.components);
            this.Timer_Combobox_Dif = new System.Windows.Forms.Timer(this.components);
            this.txtTitle = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btnMini = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnMax = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnClose = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GVData1)).BeginInit();
            this.Panel_Right.SuspendLayout();
            this.Panel_Order_Rate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GVRate1)).BeginInit();
            this.Panel_Temp1.SuspendLayout();
            this.Panel_Order.SuspendLayout();
            this.Panel_Left.SuspendLayout();
            this.Panel_Info.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GVData2)).BeginInit();
            this.Panel_Right_2.SuspendLayout();
            this.Panel_Order_Rate_2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GVRate2)).BeginInit();
            this.panel1.SuspendLayout();
            this.Panel_Temp2.SuspendLayout();
            this.Panel_Left_2.SuspendLayout();
            this.Panel_Info_2.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.tabControl1.Location = new System.Drawing.Point(0, 42);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1386, 726);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabControl1_Selected);
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(178)))), ((int)(((byte)(229)))));
            this.tabPage1.Controls.Add(this.GVData1);
            this.tabPage1.Controls.Add(this.kryptonHeader5);
            this.tabPage1.Controls.Add(this.Panel_Right);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.Panel_Left);
            this.tabPage1.Controls.Add(this.groupBox6);
            this.tabPage1.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tabPage1.Location = new System.Drawing.Point(4, 23);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(1378, 699);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "TẠI TỔ";
            // 
            // GVData1
            // 
            this.GVData1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GVData1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GVData1.Location = new System.Drawing.Point(291, 177);
            this.GVData1.Name = "GVData1";
            this.GVData1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GVData1.Size = new System.Drawing.Size(1087, 467);
            this.GVData1.TabIndex = 186;
            // 
            // kryptonHeader5
            // 
            this.kryptonHeader5.AutoSize = false;
            this.kryptonHeader5.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader5.Location = new System.Drawing.Point(291, 147);
            this.kryptonHeader5.Name = "kryptonHeader5";
            this.kryptonHeader5.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader5.Size = new System.Drawing.Size(1087, 30);
            this.kryptonHeader5.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader5.TabIndex = 185;
            this.kryptonHeader5.Values.Description = "";
            this.kryptonHeader5.Values.Heading = "Thông tin công nhân";
            // 
            // Panel_Right
            // 
            this.Panel_Right.Controls.Add(this.Panel_Order_Rate);
            this.Panel_Right.Controls.Add(this.Panel_Temp1);
            this.Panel_Right.Controls.Add(this.Panel_Order);
            this.Panel_Right.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel_Right.Location = new System.Drawing.Point(291, 0);
            this.Panel_Right.Name = "Panel_Right";
            this.Panel_Right.Size = new System.Drawing.Size(1087, 147);
            this.Panel_Right.TabIndex = 134;
            // 
            // Panel_Order_Rate
            // 
            this.Panel_Order_Rate.Controls.Add(this.GVRate1);
            this.Panel_Order_Rate.Controls.Add(this.kryptonHeader4);
            this.Panel_Order_Rate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel_Order_Rate.Location = new System.Drawing.Point(559, 0);
            this.Panel_Order_Rate.Name = "Panel_Order_Rate";
            this.Panel_Order_Rate.Size = new System.Drawing.Size(374, 147);
            this.Panel_Order_Rate.TabIndex = 204;
            // 
            // GVRate1
            // 
            this.GVRate1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.GVRate1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GVRate1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GVRate1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.GVRate1.Location = new System.Drawing.Point(0, 30);
            this.GVRate1.Name = "GVRate1";
            this.GVRate1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GVRate1.Size = new System.Drawing.Size(374, 117);
            this.GVRate1.TabIndex = 151;
            // 
            // kryptonHeader4
            // 
            this.kryptonHeader4.AutoSize = false;
            this.kryptonHeader4.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnHide1,
            this.btnUnhide1});
            this.kryptonHeader4.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader4.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader4.Name = "kryptonHeader4";
            this.kryptonHeader4.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader4.Size = new System.Drawing.Size(374, 30);
            this.kryptonHeader4.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader4.TabIndex = 200;
            this.kryptonHeader4.Values.Description = "";
            this.kryptonHeader4.Values.Heading = "Hệ số";
            // 
            // btnHide1
            // 
            this.btnHide1.Image = ((System.Drawing.Image)(resources.GetObject("btnHide1.Image")));
            this.btnHide1.UniqueName = "0010BF7FD3B9491A00975C9A919D6B8A";
            this.btnHide1.Click += new System.EventHandler(this.btnHide1_Click);
            // 
            // btnUnhide1
            // 
            this.btnUnhide1.Image = ((System.Drawing.Image)(resources.GetObject("btnUnhide1.Image")));
            this.btnUnhide1.UniqueName = "8D0A4B861A87443FB6855495AB7AD05A";
            this.btnUnhide1.Visible = false;
            this.btnUnhide1.Click += new System.EventHandler(this.btnUnhide1_Click);
            // 
            // Panel_Temp1
            // 
            this.Panel_Temp1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Temp1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel_Temp1.Controls.Add(this.kryptonHeader11);
            this.Panel_Temp1.Controls.Add(this.txt_KGTB);
            this.Panel_Temp1.Controls.Add(this.txt_Kg_Bas);
            this.Panel_Temp1.Controls.Add(this.label29);
            this.Panel_Temp1.Controls.Add(this.label1);
            this.Panel_Temp1.Controls.Add(this.txt_Money_TB);
            this.Panel_Temp1.Controls.Add(this.label31);
            this.Panel_Temp1.Controls.Add(this.txt_TB_Time);
            this.Panel_Temp1.Controls.Add(this.label33);
            this.Panel_Temp1.Dock = System.Windows.Forms.DockStyle.Right;
            this.Panel_Temp1.Location = new System.Drawing.Point(933, 0);
            this.Panel_Temp1.Name = "Panel_Temp1";
            this.Panel_Temp1.Size = new System.Drawing.Size(154, 147);
            this.Panel_Temp1.TabIndex = 205;
            this.Panel_Temp1.Visible = false;
            // 
            // kryptonHeader11
            // 
            this.kryptonHeader11.AutoSize = false;
            this.kryptonHeader11.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader11.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader11.Name = "kryptonHeader11";
            this.kryptonHeader11.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader11.Size = new System.Drawing.Size(152, 30);
            this.kryptonHeader11.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader11.TabIndex = 202;
            this.kryptonHeader11.Values.Description = "";
            this.kryptonHeader11.Values.Heading = "Số liệu tính toán";
            // 
            // txt_KGTB
            // 
            this.txt_KGTB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_KGTB.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_KGTB.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_KGTB.Location = new System.Drawing.Point(67, 63);
            this.txt_KGTB.Name = "txt_KGTB";
            this.txt_KGTB.ReadOnly = true;
            this.txt_KGTB.Size = new System.Drawing.Size(80, 21);
            this.txt_KGTB.TabIndex = 161;
            // 
            // txt_Kg_Bas
            // 
            this.txt_Kg_Bas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_Kg_Bas.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_Kg_Bas.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Kg_Bas.Location = new System.Drawing.Point(67, 37);
            this.txt_Kg_Bas.Name = "txt_Kg_Bas";
            this.txt_Kg_Bas.ReadOnly = true;
            this.txt_Kg_Bas.Size = new System.Drawing.Size(80, 21);
            this.txt_Kg_Bas.TabIndex = 4;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label29.Location = new System.Drawing.Point(16, 117);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(49, 15);
            this.label29.TabIndex = 146;
            this.label29.Text = "Tiền TB";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.Location = new System.Drawing.Point(10, 67);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 15);
            this.label1.TabIndex = 162;
            this.label1.Text = "Số KgTB";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txt_Money_TB
            // 
            this.txt_Money_TB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_Money_TB.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_Money_TB.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Money_TB.Location = new System.Drawing.Point(67, 114);
            this.txt_Money_TB.Name = "txt_Money_TB";
            this.txt_Money_TB.ReadOnly = true;
            this.txt_Money_TB.Size = new System.Drawing.Size(80, 21);
            this.txt_Money_TB.TabIndex = 6;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label31.Location = new System.Drawing.Point(6, 40);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(59, 15);
            this.label31.TabIndex = 150;
            this.label31.Text = "Số Kg/Rổ";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txt_TB_Time
            // 
            this.txt_TB_Time.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_TB_Time.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_TB_Time.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_TB_Time.Location = new System.Drawing.Point(67, 89);
            this.txt_TB_Time.Name = "txt_TB_Time";
            this.txt_TB_Time.ReadOnly = true;
            this.txt_TB_Time.Size = new System.Drawing.Size(80, 21);
            this.txt_TB_Time.TabIndex = 5;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label33.Location = new System.Drawing.Point(6, 92);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(59, 15);
            this.label33.TabIndex = 152;
            this.label33.Text = "Số Kg/TG";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Panel_Order
            // 
            this.Panel_Order.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Order.Controls.Add(this.label26);
            this.Panel_Order.Controls.Add(this.kryptonHeader3);
            this.Panel_Order.Controls.Add(this.txt_Quantity);
            this.Panel_Order.Controls.Add(this.label3);
            this.Panel_Order.Controls.Add(this.txt_Kilogram);
            this.Panel_Order.Controls.Add(this.txt_Stage);
            this.Panel_Order.Controls.Add(this.label32);
            this.Panel_Order.Controls.Add(this.label2);
            this.Panel_Order.Controls.Add(this.label28);
            this.Panel_Order.Controls.Add(this.txt_Time);
            this.Panel_Order.Controls.Add(this.txt_Price);
            this.Panel_Order.Controls.Add(this.label30);
            this.Panel_Order.Controls.Add(this.txt_Basket);
            this.Panel_Order.Dock = System.Windows.Forms.DockStyle.Left;
            this.Panel_Order.Location = new System.Drawing.Point(0, 0);
            this.Panel_Order.Name = "Panel_Order";
            this.Panel_Order.Size = new System.Drawing.Size(559, 147);
            this.Panel_Order.TabIndex = 203;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label26.Location = new System.Drawing.Point(7, 64);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(106, 15);
            this.label26.TabIndex = 144;
            this.label26.Text = "Số Kg nguyên liệu";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // kryptonHeader3
            // 
            this.kryptonHeader3.AutoSize = false;
            this.kryptonHeader3.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader3.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader3.Name = "kryptonHeader3";
            this.kryptonHeader3.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader3.Size = new System.Drawing.Size(559, 30);
            this.kryptonHeader3.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader3.TabIndex = 200;
            this.kryptonHeader3.Values.Description = "";
            this.kryptonHeader3.Values.Heading = "Chi tiết đơn hàng";
            // 
            // txt_Quantity
            // 
            this.txt_Quantity.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_Quantity.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_Quantity.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Quantity.Location = new System.Drawing.Point(115, 35);
            this.txt_Quantity.Name = "txt_Quantity";
            this.txt_Quantity.ReadOnly = true;
            this.txt_Quantity.Size = new System.Drawing.Size(80, 21);
            this.txt_Quantity.TabIndex = 0;
            this.txt_Quantity.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label3.Location = new System.Drawing.Point(51, 38);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 15);
            this.label3.TabIndex = 160;
            this.label3.Text = "Số Lượng";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txt_Kilogram
            // 
            this.txt_Kilogram.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_Kilogram.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_Kilogram.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Kilogram.Location = new System.Drawing.Point(115, 61);
            this.txt_Kilogram.Name = "txt_Kilogram";
            this.txt_Kilogram.ReadOnly = true;
            this.txt_Kilogram.Size = new System.Drawing.Size(80, 21);
            this.txt_Kilogram.TabIndex = 1;
            this.txt_Kilogram.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txt_Stage
            // 
            this.txt_Stage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_Stage.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_Stage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Stage.Location = new System.Drawing.Point(115, 88);
            this.txt_Stage.Multiline = true;
            this.txt_Stage.Name = "txt_Stage";
            this.txt_Stage.ReadOnly = true;
            this.txt_Stage.Size = new System.Drawing.Size(440, 50);
            this.txt_Stage.TabIndex = 7;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label32.Location = new System.Drawing.Point(202, 63);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(78, 15);
            this.label32.TabIndex = 151;
            this.label32.Text = "Tổng TG làm";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label2.Location = new System.Drawing.Point(43, 91);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 15);
            this.label2.TabIndex = 158;
            this.label2.Text = "Công Đoạn";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label28.Location = new System.Drawing.Point(239, 37);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(41, 15);
            this.label28.TabIndex = 145;
            this.label28.Text = "Số Rổ";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txt_Time
            // 
            this.txt_Time.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_Time.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_Time.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Time.Location = new System.Drawing.Point(282, 60);
            this.txt_Time.Name = "txt_Time";
            this.txt_Time.ReadOnly = true;
            this.txt_Time.Size = new System.Drawing.Size(80, 21);
            this.txt_Time.TabIndex = 3;
            this.txt_Time.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txt_Price
            // 
            this.txt_Price.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_Price.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_Price.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Price.Location = new System.Drawing.Point(472, 35);
            this.txt_Price.Name = "txt_Price";
            this.txt_Price.ReadOnly = true;
            this.txt_Price.Size = new System.Drawing.Size(80, 21);
            this.txt_Price.TabIndex = 8;
            this.txt_Price.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label30.Location = new System.Drawing.Point(417, 38);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(53, 15);
            this.label30.TabIndex = 157;
            this.label30.Text = "Đơn Giá";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txt_Basket
            // 
            this.txt_Basket.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_Basket.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_Basket.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Basket.Location = new System.Drawing.Point(282, 35);
            this.txt_Basket.Name = "txt_Basket";
            this.txt_Basket.ReadOnly = true;
            this.txt_Basket.Size = new System.Drawing.Size(80, 21);
            this.txt_Basket.TabIndex = 2;
            this.txt_Basket.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.DodgerBlue;
            this.label4.Dock = System.Windows.Forms.DockStyle.Left;
            this.label4.Location = new System.Drawing.Point(289, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(2, 644);
            this.label4.TabIndex = 205;
            // 
            // Panel_Left
            // 
            this.Panel_Left.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel_Left.Controls.Add(this.LVData1);
            this.Panel_Left.Controls.Add(this.kryptonHeader1);
            this.Panel_Left.Controls.Add(this.Panel_Info);
            this.Panel_Left.Dock = System.Windows.Forms.DockStyle.Left;
            this.Panel_Left.Location = new System.Drawing.Point(0, 0);
            this.Panel_Left.Name = "Panel_Left";
            this.Panel_Left.Size = new System.Drawing.Size(289, 644);
            this.Panel_Left.TabIndex = 135;
            // 
            // LVData1
            // 
            this.LVData1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.LVData1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LVData1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LVData1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.LVData1.FullRowSelect = true;
            this.LVData1.GridLines = true;
            this.LVData1.HideSelection = false;
            this.LVData1.Location = new System.Drawing.Point(0, 176);
            this.LVData1.Name = "LVData1";
            this.LVData1.Size = new System.Drawing.Size(287, 466);
            this.LVData1.TabIndex = 205;
            this.LVData1.UseCompatibleStateImageBehavior = false;
            this.LVData1.View = System.Windows.Forms.View.Details;
            // 
            // kryptonHeader1
            // 
            this.kryptonHeader1.AutoSize = false;
            this.kryptonHeader1.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader1.Location = new System.Drawing.Point(0, 146);
            this.kryptonHeader1.Name = "kryptonHeader1";
            this.kryptonHeader1.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader1.Size = new System.Drawing.Size(287, 30);
            this.kryptonHeader1.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader1.TabIndex = 204;
            this.kryptonHeader1.Values.Description = "";
            this.kryptonHeader1.Values.Heading = "Đơn hàng";
            // 
            // Panel_Info
            // 
            this.Panel_Info.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Info.Controls.Add(this.dte_Date1);
            this.Panel_Info.Controls.Add(this.cbo_Stage);
            this.Panel_Info.Controls.Add(this.kryptonHeader2);
            this.Panel_Info.Controls.Add(this.label34);
            this.Panel_Info.Controls.Add(this.label8);
            this.Panel_Info.Controls.Add(this.cbo_Team);
            this.Panel_Info.Controls.Add(this.label21);
            this.Panel_Info.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel_Info.Location = new System.Drawing.Point(0, 0);
            this.Panel_Info.Name = "Panel_Info";
            this.Panel_Info.Size = new System.Drawing.Size(287, 146);
            this.Panel_Info.TabIndex = 203;
            // 
            // dte_Date1
            // 
            this.dte_Date1.CustomFormat = "dd/MM/yyyy";
            this.dte_Date1.Location = new System.Drawing.Point(83, 39);
            this.dte_Date1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dte_Date1.Name = "dte_Date1";
            this.dte_Date1.Size = new System.Drawing.Size(115, 25);
            this.dte_Date1.TabIndex = 0;
            this.dte_Date1.Value = new System.DateTime(((long)(0)));
            // 
            // cbo_Stage
            // 
            this.cbo_Stage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbo_Stage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.cbo_Stage.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.cbo_Stage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbo_Stage.FormattingEnabled = true;
            this.cbo_Stage.Location = new System.Drawing.Point(83, 93);
            this.cbo_Stage.Name = "cbo_Stage";
            this.cbo_Stage.Size = new System.Drawing.Size(195, 23);
            this.cbo_Stage.TabIndex = 2;
            // 
            // kryptonHeader2
            // 
            this.kryptonHeader2.AutoSize = false;
            this.kryptonHeader2.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader2.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader2.Name = "kryptonHeader2";
            this.kryptonHeader2.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader2.Size = new System.Drawing.Size(287, 30);
            this.kryptonHeader2.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader2.TabIndex = 200;
            this.kryptonHeader2.Values.Description = "";
            this.kryptonHeader2.Values.Heading = "Thông tin nhóm";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label34.Location = new System.Drawing.Point(11, 97);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(70, 15);
            this.label34.TabIndex = 152;
            this.label34.Text = "Công Đoạn";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label8.Location = new System.Drawing.Point(46, 43);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 15);
            this.label8.TabIndex = 149;
            this.label8.Text = "Ngày";
            // 
            // cbo_Team
            // 
            this.cbo_Team.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbo_Team.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.cbo_Team.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.cbo_Team.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbo_Team.FormattingEnabled = true;
            this.cbo_Team.Location = new System.Drawing.Point(83, 67);
            this.cbo_Team.Name = "cbo_Team";
            this.cbo_Team.Size = new System.Drawing.Size(195, 23);
            this.cbo_Team.TabIndex = 1;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label21.Location = new System.Drawing.Point(7, 71);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(74, 15);
            this.label21.TabIndex = 150;
            this.label21.Text = "Chọn Nhóm";
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.groupBox6.Controls.Add(this.btnRun1);
            this.groupBox6.Controls.Add(this.txt_OrderID);
            this.groupBox6.Controls.Add(this.btn_Count);
            this.groupBox6.Controls.Add(this.btn_Save);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox6.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.ForeColor = System.Drawing.Color.Maroon;
            this.groupBox6.Location = new System.Drawing.Point(0, 644);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(1378, 55);
            this.groupBox6.TabIndex = 1;
            this.groupBox6.TabStop = false;
            // 
            // btnRun1
            // 
            this.btnRun1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnRun1.Location = new System.Drawing.Point(6, 8);
            this.btnRun1.Name = "btnRun1";
            this.btnRun1.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btnRun1.Size = new System.Drawing.Size(120, 40);
            this.btnRun1.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btnRun1.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btnRun1.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btnRun1.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRun1.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btnRun1.TabIndex = 7;
            this.btnRun1.Values.Image = ((System.Drawing.Image)(resources.GetObject("btnRun1.Values.Image")));
            this.btnRun1.Values.Text = "Tính tự động";
            this.btnRun1.Visible = false;
            this.btnRun1.Click += new System.EventHandler(this.btnRun1_Click);
            // 
            // txt_OrderID
            // 
            this.txt_OrderID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_OrderID.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_OrderID.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_OrderID.Location = new System.Drawing.Point(301, 20);
            this.txt_OrderID.Name = "txt_OrderID";
            this.txt_OrderID.ReadOnly = true;
            this.txt_OrderID.Size = new System.Drawing.Size(289, 21);
            this.txt_OrderID.TabIndex = 3;
            this.txt_OrderID.Visible = false;
            // 
            // btn_Count
            // 
            this.btn_Count.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Count.Location = new System.Drawing.Point(1255, 8);
            this.btn_Count.Name = "btn_Count";
            this.btn_Count.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Count.Size = new System.Drawing.Size(120, 40);
            this.btn_Count.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Count.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Count.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Count.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Count.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Count.TabIndex = 7;
            this.btn_Count.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Count.Values.Image")));
            this.btn_Count.Values.Text = "Tính tiền";
            // 
            // btn_Save
            // 
            this.btn_Save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Save.Location = new System.Drawing.Point(1255, 8);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Save.Size = new System.Drawing.Size(120, 40);
            this.btn_Save.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Save.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Save.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Save.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Save.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Save.TabIndex = 8;
            this.btn_Save.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Save.Values.Image")));
            this.btn_Save.Values.Text = "Duyệt";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(178)))), ((int)(((byte)(229)))));
            this.tabPage2.Controls.Add(this.GVData2);
            this.tabPage2.Controls.Add(this.kryptonHeader10);
            this.tabPage2.Controls.Add(this.Panel_Right_2);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Controls.Add(this.Panel_Left_2);
            this.tabPage2.Controls.Add(this.groupBox11);
            this.tabPage2.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tabPage2.Location = new System.Drawing.Point(4, 23);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(1378, 699);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "CÔNG VIỆC KHÁC";
            // 
            // GVData2
            // 
            this.GVData2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GVData2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GVData2.Location = new System.Drawing.Point(291, 177);
            this.GVData2.Name = "GVData2";
            this.GVData2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GVData2.Size = new System.Drawing.Size(1087, 467);
            this.GVData2.TabIndex = 203;
            // 
            // kryptonHeader10
            // 
            this.kryptonHeader10.AutoSize = false;
            this.kryptonHeader10.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader10.Location = new System.Drawing.Point(291, 147);
            this.kryptonHeader10.Name = "kryptonHeader10";
            this.kryptonHeader10.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader10.Size = new System.Drawing.Size(1087, 30);
            this.kryptonHeader10.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.kryptonHeader10.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader10.TabIndex = 202;
            this.kryptonHeader10.Values.Description = "";
            this.kryptonHeader10.Values.Heading = "Thông tin công nhân";
            // 
            // Panel_Right_2
            // 
            this.Panel_Right_2.Controls.Add(this.Panel_Order_Rate_2);
            this.Panel_Right_2.Controls.Add(this.panel1);
            this.Panel_Right_2.Controls.Add(this.Panel_Temp2);
            this.Panel_Right_2.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel_Right_2.Location = new System.Drawing.Point(291, 0);
            this.Panel_Right_2.Name = "Panel_Right_2";
            this.Panel_Right_2.Size = new System.Drawing.Size(1087, 147);
            this.Panel_Right_2.TabIndex = 135;
            // 
            // Panel_Order_Rate_2
            // 
            this.Panel_Order_Rate_2.Controls.Add(this.GVRate2);
            this.Panel_Order_Rate_2.Controls.Add(this.kryptonHeader9);
            this.Panel_Order_Rate_2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel_Order_Rate_2.Location = new System.Drawing.Point(559, 0);
            this.Panel_Order_Rate_2.Name = "Panel_Order_Rate_2";
            this.Panel_Order_Rate_2.Size = new System.Drawing.Size(373, 147);
            this.Panel_Order_Rate_2.TabIndex = 204;
            // 
            // GVRate2
            // 
            this.GVRate2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.GVRate2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GVRate2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GVRate2.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.GVRate2.Location = new System.Drawing.Point(0, 30);
            this.GVRate2.Name = "GVRate2";
            this.GVRate2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GVRate2.Size = new System.Drawing.Size(373, 117);
            this.GVRate2.TabIndex = 201;
            // 
            // kryptonHeader9
            // 
            this.kryptonHeader9.AutoSize = false;
            this.kryptonHeader9.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnHide2,
            this.btnUnhide2});
            this.kryptonHeader9.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader9.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader9.Name = "kryptonHeader9";
            this.kryptonHeader9.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader9.Size = new System.Drawing.Size(373, 30);
            this.kryptonHeader9.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader9.TabIndex = 202;
            this.kryptonHeader9.Values.Description = "";
            this.kryptonHeader9.Values.Heading = "Hệ số";
            // 
            // btnHide2
            // 
            this.btnHide2.Image = ((System.Drawing.Image)(resources.GetObject("btnHide2.Image")));
            this.btnHide2.UniqueName = "0010BF7FD3B9491A00975C9A919D6B8A";
            this.btnHide2.Click += new System.EventHandler(this.btnHide2_Click);
            // 
            // btnUnhide2
            // 
            this.btnUnhide2.Image = ((System.Drawing.Image)(resources.GetObject("btnUnhide2.Image")));
            this.btnUnhide2.UniqueName = "8D0A4B861A87443FB6855495AB7AD05A";
            this.btnUnhide2.Visible = false;
            this.btnUnhide2.Click += new System.EventHandler(this.btnUnhide2_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.kryptonHeader8);
            this.panel1.Controls.Add(this.txt_Quantity_Dif);
            this.panel1.Controls.Add(this.txt_Stage_Dif);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.txt_Kilogram_Dif);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.txt_Time_Dif);
            this.panel1.Controls.Add(this.txt_Basket_Dif);
            this.panel1.Controls.Add(this.txt_Price_Dif);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(559, 147);
            this.panel1.TabIndex = 203;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label5.Location = new System.Drawing.Point(202, 63);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(78, 15);
            this.label5.TabIndex = 201;
            this.label5.Text = "Tổng TG làm";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label13.Location = new System.Drawing.Point(51, 38);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(62, 15);
            this.label13.TabIndex = 160;
            this.label13.Text = "Số Lượng";
            // 
            // kryptonHeader8
            // 
            this.kryptonHeader8.AutoSize = false;
            this.kryptonHeader8.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader8.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader8.Name = "kryptonHeader8";
            this.kryptonHeader8.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader8.Size = new System.Drawing.Size(559, 30);
            this.kryptonHeader8.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.kryptonHeader8.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader8.TabIndex = 200;
            this.kryptonHeader8.Values.Description = "";
            this.kryptonHeader8.Values.Heading = "Chi tiết đơn hàng";
            // 
            // txt_Quantity_Dif
            // 
            this.txt_Quantity_Dif.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_Quantity_Dif.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_Quantity_Dif.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Quantity_Dif.Location = new System.Drawing.Point(115, 35);
            this.txt_Quantity_Dif.Name = "txt_Quantity_Dif";
            this.txt_Quantity_Dif.Size = new System.Drawing.Size(80, 21);
            this.txt_Quantity_Dif.TabIndex = 161;
            this.txt_Quantity_Dif.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txt_Stage_Dif
            // 
            this.txt_Stage_Dif.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_Stage_Dif.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_Stage_Dif.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Stage_Dif.Location = new System.Drawing.Point(115, 88);
            this.txt_Stage_Dif.Multiline = true;
            this.txt_Stage_Dif.Name = "txt_Stage_Dif";
            this.txt_Stage_Dif.ReadOnly = true;
            this.txt_Stage_Dif.Size = new System.Drawing.Size(440, 50);
            this.txt_Stage_Dif.TabIndex = 159;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label14.Location = new System.Drawing.Point(43, 91);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(70, 15);
            this.label14.TabIndex = 158;
            this.label14.Text = "Công Đoạn";
            // 
            // txt_Kilogram_Dif
            // 
            this.txt_Kilogram_Dif.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_Kilogram_Dif.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_Kilogram_Dif.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Kilogram_Dif.Location = new System.Drawing.Point(115, 61);
            this.txt_Kilogram_Dif.Name = "txt_Kilogram_Dif";
            this.txt_Kilogram_Dif.Size = new System.Drawing.Size(80, 21);
            this.txt_Kilogram_Dif.TabIndex = 153;
            this.txt_Kilogram_Dif.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label15.Location = new System.Drawing.Point(7, 64);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(106, 15);
            this.label15.TabIndex = 144;
            this.label15.Text = "Số Kg nguyên liệu";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label16.Location = new System.Drawing.Point(239, 37);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(41, 15);
            this.label16.TabIndex = 145;
            this.label16.Text = "Số Rổ";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label18.Location = new System.Drawing.Point(417, 38);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(53, 15);
            this.label18.TabIndex = 157;
            this.label18.Text = "Đơn Giá";
            // 
            // txt_Time_Dif
            // 
            this.txt_Time_Dif.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_Time_Dif.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_Time_Dif.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Time_Dif.Location = new System.Drawing.Point(282, 60);
            this.txt_Time_Dif.Name = "txt_Time_Dif";
            this.txt_Time_Dif.Size = new System.Drawing.Size(80, 21);
            this.txt_Time_Dif.TabIndex = 149;
            this.txt_Time_Dif.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txt_Basket_Dif
            // 
            this.txt_Basket_Dif.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_Basket_Dif.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_Basket_Dif.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Basket_Dif.Location = new System.Drawing.Point(282, 35);
            this.txt_Basket_Dif.Name = "txt_Basket_Dif";
            this.txt_Basket_Dif.Size = new System.Drawing.Size(80, 21);
            this.txt_Basket_Dif.TabIndex = 148;
            this.txt_Basket_Dif.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txt_Price_Dif
            // 
            this.txt_Price_Dif.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_Price_Dif.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_Price_Dif.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Price_Dif.Location = new System.Drawing.Point(472, 35);
            this.txt_Price_Dif.Name = "txt_Price_Dif";
            this.txt_Price_Dif.Size = new System.Drawing.Size(80, 21);
            this.txt_Price_Dif.TabIndex = 156;
            this.txt_Price_Dif.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // Panel_Temp2
            // 
            this.Panel_Temp2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Temp2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel_Temp2.Controls.Add(this.kryptonHeader12);
            this.Panel_Temp2.Controls.Add(this.txt_Kg_Bas_Dif);
            this.Panel_Temp2.Controls.Add(this.txt_Money_TB_Dif);
            this.Panel_Temp2.Controls.Add(this.label17);
            this.Panel_Temp2.Controls.Add(this.label22);
            this.Panel_Temp2.Controls.Add(this.label19);
            this.Panel_Temp2.Controls.Add(this.txt_TB_Time_Dif);
            this.Panel_Temp2.Dock = System.Windows.Forms.DockStyle.Right;
            this.Panel_Temp2.Location = new System.Drawing.Point(932, 0);
            this.Panel_Temp2.Name = "Panel_Temp2";
            this.Panel_Temp2.Size = new System.Drawing.Size(155, 147);
            this.Panel_Temp2.TabIndex = 206;
            this.Panel_Temp2.Visible = false;
            // 
            // kryptonHeader12
            // 
            this.kryptonHeader12.AutoSize = false;
            this.kryptonHeader12.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader12.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader12.Name = "kryptonHeader12";
            this.kryptonHeader12.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader12.Size = new System.Drawing.Size(153, 30);
            this.kryptonHeader12.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader12.TabIndex = 202;
            this.kryptonHeader12.Values.Description = "";
            this.kryptonHeader12.Values.Heading = "Số liệu tính toán";
            // 
            // txt_Kg_Bas_Dif
            // 
            this.txt_Kg_Bas_Dif.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_Kg_Bas_Dif.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_Kg_Bas_Dif.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Kg_Bas_Dif.Location = new System.Drawing.Point(67, 43);
            this.txt_Kg_Bas_Dif.Name = "txt_Kg_Bas_Dif";
            this.txt_Kg_Bas_Dif.Size = new System.Drawing.Size(80, 21);
            this.txt_Kg_Bas_Dif.TabIndex = 147;
            // 
            // txt_Money_TB_Dif
            // 
            this.txt_Money_TB_Dif.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_Money_TB_Dif.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_Money_TB_Dif.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Money_TB_Dif.Location = new System.Drawing.Point(67, 93);
            this.txt_Money_TB_Dif.Name = "txt_Money_TB_Dif";
            this.txt_Money_TB_Dif.Size = new System.Drawing.Size(80, 21);
            this.txt_Money_TB_Dif.TabIndex = 155;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label17.Location = new System.Drawing.Point(16, 96);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(49, 15);
            this.label17.TabIndex = 146;
            this.label17.Text = "Tiền TB";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label22.Location = new System.Drawing.Point(6, 71);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(59, 15);
            this.label22.TabIndex = 152;
            this.label22.Text = "Số Kg/TG";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label19.Location = new System.Drawing.Point(6, 46);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(59, 15);
            this.label19.TabIndex = 150;
            this.label19.Text = "Số Kg/Rổ";
            // 
            // txt_TB_Time_Dif
            // 
            this.txt_TB_Time_Dif.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_TB_Time_Dif.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_TB_Time_Dif.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_TB_Time_Dif.Location = new System.Drawing.Point(67, 68);
            this.txt_TB_Time_Dif.Name = "txt_TB_Time_Dif";
            this.txt_TB_Time_Dif.Size = new System.Drawing.Size(80, 21);
            this.txt_TB_Time_Dif.TabIndex = 154;
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.DodgerBlue;
            this.label9.Dock = System.Windows.Forms.DockStyle.Left;
            this.label9.Location = new System.Drawing.Point(289, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(2, 644);
            this.label9.TabIndex = 204;
            // 
            // Panel_Left_2
            // 
            this.Panel_Left_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel_Left_2.Controls.Add(this.LVData2);
            this.Panel_Left_2.Controls.Add(this.kryptonHeader7);
            this.Panel_Left_2.Controls.Add(this.Panel_Info_2);
            this.Panel_Left_2.Dock = System.Windows.Forms.DockStyle.Left;
            this.Panel_Left_2.Location = new System.Drawing.Point(0, 0);
            this.Panel_Left_2.Name = "Panel_Left_2";
            this.Panel_Left_2.Size = new System.Drawing.Size(289, 644);
            this.Panel_Left_2.TabIndex = 136;
            // 
            // LVData2
            // 
            this.LVData2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.LVData2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LVData2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LVData2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.LVData2.FullRowSelect = true;
            this.LVData2.GridLines = true;
            this.LVData2.HideSelection = false;
            this.LVData2.Location = new System.Drawing.Point(0, 176);
            this.LVData2.Name = "LVData2";
            this.LVData2.Size = new System.Drawing.Size(287, 466);
            this.LVData2.TabIndex = 149;
            this.LVData2.UseCompatibleStateImageBehavior = false;
            this.LVData2.View = System.Windows.Forms.View.Details;
            // 
            // kryptonHeader7
            // 
            this.kryptonHeader7.AutoSize = false;
            this.kryptonHeader7.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader7.Location = new System.Drawing.Point(0, 146);
            this.kryptonHeader7.Name = "kryptonHeader7";
            this.kryptonHeader7.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader7.Size = new System.Drawing.Size(287, 30);
            this.kryptonHeader7.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.kryptonHeader7.StateCommon.Border.Rounding = 0;
            this.kryptonHeader7.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader7.TabIndex = 205;
            this.kryptonHeader7.Values.Description = "";
            this.kryptonHeader7.Values.Heading = "Đơn hàng";
            // 
            // Panel_Info_2
            // 
            this.Panel_Info_2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Info_2.Controls.Add(this.dte_Date2);
            this.Panel_Info_2.Controls.Add(this.cbo_Stage_Dif);
            this.Panel_Info_2.Controls.Add(this.kryptonHeader6);
            this.Panel_Info_2.Controls.Add(this.label6);
            this.Panel_Info_2.Controls.Add(this.label10);
            this.Panel_Info_2.Controls.Add(this.cbo_Team_Dif);
            this.Panel_Info_2.Controls.Add(this.label7);
            this.Panel_Info_2.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel_Info_2.Location = new System.Drawing.Point(0, 0);
            this.Panel_Info_2.Name = "Panel_Info_2";
            this.Panel_Info_2.Size = new System.Drawing.Size(287, 146);
            this.Panel_Info_2.TabIndex = 203;
            // 
            // dte_Date2
            // 
            this.dte_Date2.CustomFormat = "dd/MM/yyyy";
            this.dte_Date2.Location = new System.Drawing.Point(83, 39);
            this.dte_Date2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dte_Date2.Name = "dte_Date2";
            this.dte_Date2.Size = new System.Drawing.Size(115, 25);
            this.dte_Date2.TabIndex = 156;
            this.dte_Date2.Value = new System.DateTime(((long)(0)));
            // 
            // cbo_Stage_Dif
            // 
            this.cbo_Stage_Dif.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbo_Stage_Dif.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.cbo_Stage_Dif.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.cbo_Stage_Dif.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbo_Stage_Dif.FormattingEnabled = true;
            this.cbo_Stage_Dif.Location = new System.Drawing.Point(83, 93);
            this.cbo_Stage_Dif.Name = "cbo_Stage_Dif";
            this.cbo_Stage_Dif.Size = new System.Drawing.Size(195, 23);
            this.cbo_Stage_Dif.TabIndex = 153;
            // 
            // kryptonHeader6
            // 
            this.kryptonHeader6.AutoSize = false;
            this.kryptonHeader6.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader6.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader6.Name = "kryptonHeader6";
            this.kryptonHeader6.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader6.Size = new System.Drawing.Size(287, 30);
            this.kryptonHeader6.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.kryptonHeader6.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader6.TabIndex = 200;
            this.kryptonHeader6.Values.Description = "";
            this.kryptonHeader6.Values.Heading = "Thông tin nhóm";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label6.Location = new System.Drawing.Point(11, 97);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 15);
            this.label6.TabIndex = 152;
            this.label6.Text = "Công Đoạn";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label10.Location = new System.Drawing.Point(46, 43);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(35, 15);
            this.label10.TabIndex = 149;
            this.label10.Text = "Ngày";
            // 
            // cbo_Team_Dif
            // 
            this.cbo_Team_Dif.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbo_Team_Dif.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.cbo_Team_Dif.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.cbo_Team_Dif.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbo_Team_Dif.FormattingEnabled = true;
            this.cbo_Team_Dif.Location = new System.Drawing.Point(83, 67);
            this.cbo_Team_Dif.Name = "cbo_Team_Dif";
            this.cbo_Team_Dif.Size = new System.Drawing.Size(195, 23);
            this.cbo_Team_Dif.TabIndex = 151;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label7.Location = new System.Drawing.Point(7, 71);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(74, 15);
            this.label7.TabIndex = 150;
            this.label7.Text = "Chọn Nhóm";
            // 
            // groupBox11
            // 
            this.groupBox11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.groupBox11.Controls.Add(this.btn_Timer_Dif);
            this.groupBox11.Controls.Add(this.btn_Count_Dif);
            this.groupBox11.Controls.Add(this.btn_Save_Dif);
            this.groupBox11.Controls.Add(this.txt_OrderID_Dif);
            this.groupBox11.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox11.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox11.ForeColor = System.Drawing.Color.Maroon;
            this.groupBox11.Location = new System.Drawing.Point(0, 644);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(1378, 55);
            this.groupBox11.TabIndex = 200;
            this.groupBox11.TabStop = false;
            // 
            // btn_Timer_Dif
            // 
            this.btn_Timer_Dif.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_Timer_Dif.Location = new System.Drawing.Point(293, 8);
            this.btn_Timer_Dif.Name = "btn_Timer_Dif";
            this.btn_Timer_Dif.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Timer_Dif.Size = new System.Drawing.Size(120, 40);
            this.btn_Timer_Dif.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Timer_Dif.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Timer_Dif.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Timer_Dif.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Timer_Dif.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Timer_Dif.TabIndex = 24;
            this.btn_Timer_Dif.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Timer_Dif.Values.Image")));
            this.btn_Timer_Dif.Values.Text = "Tính tự động";
            this.btn_Timer_Dif.Visible = false;
            this.btn_Timer_Dif.Click += new System.EventHandler(this.btn_Timer_Dif_Click);
            // 
            // btn_Count_Dif
            // 
            this.btn_Count_Dif.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Count_Dif.Location = new System.Drawing.Point(1250, 12);
            this.btn_Count_Dif.Name = "btn_Count_Dif";
            this.btn_Count_Dif.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Count_Dif.Size = new System.Drawing.Size(120, 40);
            this.btn_Count_Dif.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Count_Dif.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Count_Dif.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Count_Dif.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Count_Dif.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Count_Dif.TabIndex = 22;
            this.btn_Count_Dif.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Count_Dif.Values.Image")));
            this.btn_Count_Dif.Values.Text = "Tính tiền";
            // 
            // btn_Save_Dif
            // 
            this.btn_Save_Dif.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Save_Dif.Location = new System.Drawing.Point(1251, 12);
            this.btn_Save_Dif.Name = "btn_Save_Dif";
            this.btn_Save_Dif.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Save_Dif.Size = new System.Drawing.Size(120, 40);
            this.btn_Save_Dif.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Save_Dif.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Save_Dif.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Save_Dif.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Save_Dif.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Save_Dif.TabIndex = 23;
            this.btn_Save_Dif.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Save_Dif.Values.Image")));
            this.btn_Save_Dif.Values.Text = "Duyệt";
            // 
            // txt_OrderID_Dif
            // 
            this.txt_OrderID_Dif.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_OrderID_Dif.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_OrderID_Dif.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_OrderID_Dif.Location = new System.Drawing.Point(0, 18);
            this.txt_OrderID_Dif.Name = "txt_OrderID_Dif";
            this.txt_OrderID_Dif.Size = new System.Drawing.Size(289, 21);
            this.txt_OrderID_Dif.TabIndex = 21;
            this.txt_OrderID_Dif.Visible = false;
            // 
            // timerTeam
            // 
            this.timerTeam.Interval = 2000;
            this.timerTeam.Tick += new System.EventHandler(this.timerTeam_Tick);
            // 
            // timerWork
            // 
            this.timerWork.Interval = 2000;
            this.timerWork.Tick += new System.EventHandler(this.timerWork_Tick);
            // 
            // Timer_Dif
            // 
            this.Timer_Dif.Interval = 2000;
            this.Timer_Dif.Tick += new System.EventHandler(this.Timer_Dif_Tick);
            // 
            // Timer_Combobox_Dif
            // 
            this.Timer_Combobox_Dif.Tick += new System.EventHandler(this.Timer_Combobox_Dif_Tick);
            // 
            // txtTitle
            // 
            this.txtTitle.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnMini,
            this.btnMax,
            this.btnClose});
            this.txtTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtTitle.Location = new System.Drawing.Point(0, 0);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txtTitle.Size = new System.Drawing.Size(1386, 42);
            this.txtTitle.TabIndex = 202;
            this.txtTitle.Values.Description = "";
            this.txtTitle.Values.Heading = "Tính năng xuất";
            this.txtTitle.Values.Image = ((System.Drawing.Image)(resources.GetObject("txtTitle.Values.Image")));
            this.txtTitle.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Frm_Main_MouseDown);
            this.txtTitle.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Frm_Main_MouseMove);
            this.txtTitle.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Frm_Main_MouseUp);
            // 
            // btnMini
            // 
            this.btnMini.Image = ((System.Drawing.Image)(resources.GetObject("btnMini.Image")));
            this.btnMini.UniqueName = "F5F06E8241504E72ABB5BEA3F6A9B753";
            this.btnMini.Click += new System.EventHandler(this.btnMini_Click);
            // 
            // btnMax
            // 
            this.btnMax.Image = ((System.Drawing.Image)(resources.GetObject("btnMax.Image")));
            this.btnMax.UniqueName = "035D1A4881E44F58A084C31DE7352A94";
            this.btnMax.Click += new System.EventHandler(this.btnMax_Click);
            // 
            // btnClose
            // 
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.UniqueName = "11B07C6F4E1C4F9D8B91BD924CB0EBE6";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // Frm_Production_Calculator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1386, 768);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.txtTitle);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_Production_Calculator";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tính Năng Suất Công Nhân";
            this.Load += new System.EventHandler(this.Frm_Calculation_Productivity_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GVData1)).EndInit();
            this.Panel_Right.ResumeLayout(false);
            this.Panel_Order_Rate.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GVRate1)).EndInit();
            this.Panel_Temp1.ResumeLayout(false);
            this.Panel_Temp1.PerformLayout();
            this.Panel_Order.ResumeLayout(false);
            this.Panel_Order.PerformLayout();
            this.Panel_Left.ResumeLayout(false);
            this.Panel_Info.ResumeLayout(false);
            this.Panel_Info.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GVData2)).EndInit();
            this.Panel_Right_2.ResumeLayout(false);
            this.Panel_Order_Rate_2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GVRate2)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.Panel_Temp2.ResumeLayout(false);
            this.Panel_Temp2.PerformLayout();
            this.Panel_Left_2.ResumeLayout(false);
            this.Panel_Info_2.ResumeLayout(false);
            this.Panel_Info_2.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Panel Panel_Right;
        private System.Windows.Forms.TextBox txt_Kg_Bas;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox txt_Basket;
        private System.Windows.Forms.TextBox txt_Price;
        private System.Windows.Forms.TextBox txt_Time;
        private System.Windows.Forms.TextBox txt_Money_TB;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox txt_TB_Time;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox txt_Kilogram;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Panel Panel_Left;
        private System.Windows.Forms.ComboBox cbo_Stage;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.ComboBox cbo_Team;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox txt_Stage;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_Quantity;
        private System.Windows.Forms.Panel Panel_Left_2;
        private System.Windows.Forms.ListView LVData2;
        private System.Windows.Forms.Panel Panel_Right_2;
        private System.Windows.Forms.ComboBox cbo_Stage_Dif;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbo_Team_Dif;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txt_Quantity_Dif;
        private System.Windows.Forms.TextBox txt_Stage_Dif;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txt_Kg_Bas_Dif;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txt_Basket_Dif;
        private System.Windows.Forms.TextBox txt_Price_Dif;
        private System.Windows.Forms.TextBox txt_Time_Dif;
        private System.Windows.Forms.TextBox txt_Money_TB_Dif;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txt_TB_Time_Dif;
        private System.Windows.Forms.TextBox txt_Kilogram_Dif;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.GroupBox groupBox11;
        private TNS.SYS.TNDateTimePicker dte_Date1;
        private TNS.SYS.TNDateTimePicker dte_Date2;
        private System.Windows.Forms.DataGridView GVRate1;
        private System.Windows.Forms.TextBox txt_KGTB;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer timerTeam;
        private System.Windows.Forms.TextBox txt_OrderID;
        private System.Windows.Forms.Timer timerWork;
        private System.Windows.Forms.TextBox txt_OrderID_Dif;
        private System.Windows.Forms.Timer Timer_Dif;
        private System.Windows.Forms.Timer Timer_Combobox_Dif;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader txtTitle;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMini;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMax;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnClose;
        private System.Windows.Forms.Panel Panel_Info;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader2;
        private System.Windows.Forms.ListView LVData1;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader1;
        private System.Windows.Forms.Panel Panel_Order;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader3;
        private System.Windows.Forms.Panel Panel_Order_Rate;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader4;
        private System.Windows.Forms.DataGridView GVData1;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader5;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Count;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnRun1;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Save;
        private System.Windows.Forms.Panel Panel_Info_2;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader6;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader7;
        private System.Windows.Forms.Panel panel1;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader8;
        private System.Windows.Forms.DataGridView GVData2;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader10;
        private System.Windows.Forms.Panel Panel_Order_Rate_2;
        private System.Windows.Forms.DataGridView GVRate2;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Timer_Dif;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Count_Dif;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Save_Dif;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel Panel_Temp1;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader11;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnHide1;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnUnhide1;
        private System.Windows.Forms.Panel Panel_Temp2;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader12;
        private System.Windows.Forms.Label label5;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader9;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnHide2;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnUnhide2;
    }
}