﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using TN.Library.HumanResource;
using TN.Library.System;
using TNS.HRM;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp

{
    public partial class Frm_Temp_Excel_View : Form
    {
        public Frm_Temp_Excel_View()
        {
            InitializeComponent();
            btn_New.Click += btn_New_Click;
            btn_Del.Click += btn_Del_Click;
            LV_File.ItemActivate += LV_File_ItemActivate;
            btn_Search.Click += btn_Search_Click;
            btn_Hide.Click += btn_Hide_Click;
            btn_Done.Click += btn_Done_Click;
            btn_Save.Click += btn_Save_Click;
            GV_Employee.EditingControlShowing += GV_Employee_EditingControlShowing;
            GV_Employee.KeyDown += GV_Employee_KeyDown;
            //btn_Employee_Time.Click += btn_Employee_Time_Click;
            GV_Employee_Layout();
            LV_Layout(LV_File);
        }
        private int _ExcelKey = 0;
        public int ExcelKey
        {
            get
            {
                return _ExcelKey;
            }

            set
            {
                _ExcelKey = value;
            }
        }
        private List<Temp_Excel_Detail_Info> _List;
        public List<Temp_Excel_Detail_Info> List
        {
            get
            {
                return _List;
            }

            set
            {
                _List = value;
            }
        }
        public DateTime _Date;
        DataTable ztb = new DataTable();

        private void Frm_Temp_Excel_View_Load(object sender, EventArgs e)
        {
            dte_FromDate.Value = SessionUser.Date_Work;
            dte_ToDate.Value = SessionUser.Date_Work;
            ztb = Temp_Excel_Detail_Data.LoadExcelData(ExcelKey);
            GV_Employee_LoadData(ztb);
            LV_LoadData();
            #region[Ẩn log]
            GB_Log.Visible = false;
            btn_Hide.Tag = "1";
            GB_Detail.Width = this.groupBox3.Width;
            btn_Hide.Text = "<<";
            #endregion
        }

        #region[ListView]
        private void LV_Layout(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "No";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ngày";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên file";
            colHead.Width = 200;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

        }

        private void LV_LoadData()
        {
            lb_Log.Items.Add("Đang tìm!");
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LV_File;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            LV.Items.Clear();

            DateTime FromDate = new DateTime(dte_FromDate.Value.Year, dte_FromDate.Value.Month, dte_FromDate.Value.Day, 0, 0, 0);
            DateTime ToDate = new DateTime(dte_ToDate.Value.Year, dte_ToDate.Value.Month, dte_ToDate.Value.Day, 23, 59, 59);
            DataTable ztb = Temp_Excel_Data.ListFile(FromDate, ToDate, txt_Search.Text);
            for (int i = 0; i < ztb.Rows.Count; i++)
            {
                DataRow nRow = ztb.Rows[i];

                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.Tag = nRow["ExcelKey"];
                lvi.ForeColor = Color.Navy;

                lvi.BackColor = Color.White;
                lvi.ImageIndex = 0;

                lvsi = new ListViewItem.ListViewSubItem();
                DateTime zDateImport = DateTime.Parse(nRow["DateImport"].ToString().Trim());
                lvsi.Text = zDateImport.ToString("dd/MM/yyyy");
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["File"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }
            lb_Log.Items.Add("Tìm được " + ztb.Rows.Count + " kết quả!");
            lb_Log.Items.Add("Tải dữ liệu thành công!");

            this.Cursor = Cursors.Default;

        }
        private void btn_New_Click(object sender, EventArgs e)
        {
            //Frm_Import_Temp_Excel frm = new Frm_Import_Temp_Excel();
            //frm.Show();
        }
        private void btn_Del_Click(object sender, EventArgs e)
        {

            lb_Log.Items.Add("Đang xóa dữ liệu!");
            if (_ExcelKey == 0)
            {
                MessageBox.Show("Chưa chọn thông tin!");
            }
            else
            {
                if (MessageBox.Show("Bạn có chắc xóa thông tin này ?.", "Thông báo", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    Temp_Excel_Info zinfo = new Temp_Excel_Info(_ExcelKey);
                    zinfo.Delete();
                    if (zinfo.Message == string.Empty)
                    {
                        MessageBox.Show("Đã xóa !");
                        lb_Log.Items.Add("Đã xóa dữ liệu thành công!");
                        LV_LoadData();
                        _ExcelKey = 0;
                        ztb = Temp_Excel_Detail_Data.LoadExcelData(ExcelKey);
                        GV_Employee_LoadData(ztb);
                    }
                    else
                    {
                        MessageBox.Show(zinfo.Message);
                    }

                }
            }

        }
        private void LV_File_ItemActivate(object sender, EventArgs e)
        {
            for (int i = 0; i < LV_File.Items.Count; i++)
            {
                if (LV_File.Items[i].Selected == true)
                {
                    LV_File.Items[i].BackColor = Color.LightBlue; // highlighted item
                }
                else
                {
                    LV_File.Items[i].BackColor = SystemColors.Window; // normal item
                }
            }
            _ExcelKey = int.Parse(LV_File.SelectedItems[0].Tag.ToString());
            DateTime _CheckTime = DateTime.Parse(LV_File.SelectedItems[0].SubItems[1].Text);
            ztb = Temp_Excel_Detail_Data.LoadExcelData(ExcelKey);
            GV_Employee_LoadData(ztb);
        }
        private void btn_Search_Click(object sender, EventArgs e)
        {
            LV_LoadData();
        }
        #endregion

        #region [GV_Employee]
        private void GV_Employee_Layout()
        {
            // Setup Column 
            GV_Employee.Columns.Add("No", "STT");
            GV_Employee.Columns.Add("EmployeeName", "Tên Nhân Viên");
            GV_Employee.Columns.Add("EmployeeID", "Mã NV");
            GV_Employee.Columns.Add("BeginTime", "Giờ bắt đầu");
            GV_Employee.Columns.Add("EndTime", "Giờ kết thúc");
            GV_Employee.Columns.Add("OffTime", "Số giờ nghỉ trưa");
            GV_Employee.Columns.Add("TotalTime", "Tổng giờ");
            GV_Employee.Columns.Add("DifferentTime", "Giờ CV khác");
            GV_Employee.Columns.Add("OverTime", "Giờ dư");
            GV_Employee.Columns.Add("Value", "Đơn giá ngoài giờ");
            GV_Employee.Columns.Add("Money", "Tiền ngoài giờ");
            GV_Employee.Columns.Add("Code", "Loại công");
            GV_Employee.Columns.Add("Money_Rice", "Tiền cơm");

            GV_Employee.Columns["No"].Width = 40;
            GV_Employee.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV_Employee.Columns["No"].ReadOnly = true;
            GV_Employee.Columns["No"].Frozen = true;

            GV_Employee.Columns["EmployeeName"].Width = 160;
            GV_Employee.Columns["EmployeeName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV_Employee.Columns["EmployeeName"].ReadOnly = true;
            GV_Employee.Columns["EmployeeName"].Frozen = true;

            GV_Employee.Columns["EmployeeID"].Width = 100;
            GV_Employee.Columns["EmployeeID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV_Employee.Columns["EmployeeID"].Frozen = true;

            GV_Employee.Columns["BeginTime"].Width = 140;
            GV_Employee.Columns["BeginTime"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV_Employee.Columns["EndTime"].Width = 140;
            GV_Employee.Columns["EndTime"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV_Employee.Columns["OffTime"].Width = 80;
            GV_Employee.Columns["OffTime"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV_Employee.Columns["TotalTime"].Width = 80;
            GV_Employee.Columns["TotalTime"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV_Employee.Columns["TotalTime"].ReadOnly = true;

            GV_Employee.Columns["DifferentTime"].Width = 100;
            GV_Employee.Columns["DifferentTime"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV_Employee.Columns["OverTime"].Width = 80;
            GV_Employee.Columns["OverTime"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV_Employee.Columns["OverTime"].ReadOnly = true;

            GV_Employee.Columns["Value"].Width = 80;
            GV_Employee.Columns["Value"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Employee.Columns["Value"].ReadOnly = true;

            GV_Employee.Columns["Money"].Width = 80;
            GV_Employee.Columns["Money"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Employee.Columns["Money"].ReadOnly = true;

            GV_Employee.Columns["Code"].Width = 80;
            GV_Employee.Columns["Code"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV_Employee.Columns["Money_Rice"].Width = 80;
            GV_Employee.Columns["Money_Rice"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Employee.Columns["Money_Rice"].ReadOnly = true;
            // setup style view

            GV_Employee.BackgroundColor = Color.White;
            GV_Employee.GridColor = Color.FromArgb(227, 239, 255);
            GV_Employee.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV_Employee.DefaultCellStyle.ForeColor = Color.FromArgb(43, 43, 43);
            GV_Employee.DefaultCellStyle.Font = new Font("Arial", 9, FontStyle.Bold);
            GV_Employee.AutoResizeColumns();

            GV_Employee.AllowUserToResizeRows = false;
            GV_Employee.AllowUserToResizeColumns = true;

            GV_Employee.RowHeadersVisible = false;
            GV_Employee.AllowUserToDeleteRows = false;
            //// setup Height Header
            //GV_Employee.ColumnHeadersHeight = GV_Employee.ColumnHeadersHeight * 3 / 2;
            GV_Employee.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            GV_Employee.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;
            GV_Employee.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 9, FontStyle.Bold);

            for (int i = 1; i <= 8; i++)
            {
                GV_Employee.Rows.Add();
            }

        }
        private void GV_Employee_LoadData(DataTable InTable)
        {
            lb_Log.Items.Add("Đang tải dữ liệu!");

            _List = new List<Temp_Excel_Detail_Info>();
            foreach (DataRow nRow in InTable.Rows)
            {
                Temp_Excel_Detail_Info zinfo = new Temp_Excel_Detail_Info(nRow);
                _List.Add(zinfo);
            }

            GV_Employee.Rows.Clear();
            for (int i = 1; i <= _List.Count; i++)
            {
                GV_Employee.Rows.Add();
            }
            for (int i = 0; i < _List.Count; i++)
            {
                Temp_Excel_Detail_Info zinfo = (Temp_Excel_Detail_Info)_List[i];
                //if (zinfo.Hand == 1) // 0 import Excel,1 tạo tay, 2 sửa tay
                //    GV_Employee.Rows[i].DefaultCellStyle.BackColor = Color.GreenYellow;
                //if (zinfo.Hand == 2) // 0 import Excel,1 tạo tay, 2 sửa tay
                //    GV_Employee.Rows[i].DefaultCellStyle.BackColor = Color.LightBlue;

                GV_Employee.Rows[i].Tag = zinfo;
                GV_Employee.Rows[i].Cells["No"].Tag = zinfo.Key;
                GV_Employee.Rows[i].Cells["No"].Value = (i + 1).ToString();
                GV_Employee.Rows[i].Cells["EmployeeID"].Value = zinfo.EmployeeID;
                GV_Employee.Rows[i].Cells["EmployeeName"].Value = zinfo.EmployeeName;
                DateTime zBeginTime = zinfo.BeginTime;
                GV_Employee.Rows[i].Cells["BeginTime"].Value = zBeginTime.ToString("dd/MM/yyyy HH:mm");
                DateTime zEndTime = zinfo.EndTime;
                GV_Employee.Rows[i].Cells["EndTime"].Value = zEndTime.ToString("dd/MM/yyyy HH:mm");
                float zOffTime = zinfo.OffTime;
                GV_Employee.Rows[i].Cells["OffTime"].Value = Math.Round(zOffTime, 2).ToString();
                float zDifferentTime = zinfo.DifferentTime;
                GV_Employee.Rows[i].Cells["DifferentTime"].Value = Math.Round(zDifferentTime, 2).ToString();
                float zTotalTime = zinfo.TotalTime;
                if (zinfo.TotalTime < 0)
                {
                    GV_Employee.Rows[i].Cells["TotalTime"].Style.BackColor = Color.Pink;
                    GV_Employee.Rows[i].DefaultCellStyle.BackColor = Color.Tomato;
                    GV_Employee.Rows[i].Cells["BeginTime"].Style.BackColor = Color.Pink;
                    GV_Employee.Rows[i].Cells["EndTime"].Style.BackColor = Color.Pink;
                    GV_Employee.Rows[i].Cells["TotalTime"].Value = Math.Round(zTotalTime, 2).ToString();
                }
                else
                {
                    GV_Employee.Rows[i].Cells["TotalTime"].Value = Math.Round(zTotalTime, 2).ToString();
                }

                float zOverTime = zinfo.OverTime;
                if (zinfo.OverTime < 0)
                {
                    GV_Employee.Rows[i].Cells["OverTime"].Style.BackColor = Color.Pink;
                    GV_Employee.Rows[i].DefaultCellStyle.BackColor = Color.Tomato;
                    GV_Employee.Rows[i].Cells["OverTime"].Value = Math.Round(zOverTime, 2).ToString();
                }
                else
                {
                    GV_Employee.Rows[i].Cells["OverTime"].Value = Math.Round(zOverTime, 2).ToString();
                }

                float zValue = zinfo.Value;
                if (zValue == 0)
                {
                    GV_Employee.Rows[i].Cells["Value"].Value = "0";
                }
                else
                {

                    GV_Employee.Rows[i].Cells["Value"].Value = zValue.ToString("###,###,###").Trim();
                }
                double zMoney = zinfo.Money;
                if (zMoney < 0)
                {
                    GV_Employee.Rows[i].Cells["Money"].Style.BackColor = Color.Pink;
                    GV_Employee.Rows[i].DefaultCellStyle.BackColor = Color.Tomato;
                    GV_Employee.Rows[i].Cells["Money"].Value = zMoney.ToString("###,###,###").Trim();
                }
                else if (zMoney == 0)
                {
                    GV_Employee.Rows[i].Cells["Money"].Value = "0";
                }
                else
                {
                    GV_Employee.Rows[i].Cells["Money"].Value = zMoney.ToString("###,###,###").Trim();
                }

                if (zinfo.RiceKey == 0)
                {
                    GV_Employee.Rows[i].Cells["Code"].Style.BackColor = Color.Pink;
                    GV_Employee.Rows[i].DefaultCellStyle.BackColor = Color.Tomato;
                }

                if (zinfo.CodeKey == 0)
                {
                    GV_Employee.Rows[i].Cells["Code"].Style.BackColor = Color.Pink;
                    GV_Employee.Rows[i].DefaultCellStyle.BackColor = Color.Tomato;
                }

                GV_Employee.Rows[i].Cells["Code"].Value = zinfo.Code;
                double zMoney_Rice = zinfo.Money_Rice;
                if (zMoney_Rice == 0)
                {
                    GV_Employee.Rows[i].Cells["Money_Rice"].Value = "0";
                }
                else
                {
                    GV_Employee.Rows[i].Cells["Money_Rice"].Value = zMoney_Rice.ToString("###,###,###").Trim();
                }
            }


            lb_Log.Items.Add("Tìm được " + InTable.Rows.Count + " kết quả!");
            lb_Log.Items.Add("Tải dữ liệu thành công!");

        }
        private void GV_Employee_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                DialogResult dlr = MessageBox.Show("Bạn có xóa thông tin này ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlr == DialogResult.Yes)
                {
                    for (int i = 0; i < GV_Employee.Rows.Count; i++)
                    {
                        if (GV_Employee.Rows[i].Selected)
                        {
                            if (GV_Employee.Rows[i].Tag != null)
                            {
                                Temp_Excel_Detail_Info zinfo = (Temp_Excel_Detail_Info)GV_Employee.Rows[i].Tag;
                                GV_Employee.Rows[i].Visible = false;
                                zinfo.RecordStatus = 99;
                            }
                        }
                    }
                  
                }
            }
        }
        private void GV_Employee_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            switch (GV_Employee.CurrentCell.ColumnIndex)
            {
                case 2: // autocomplete for product
                    DataGridViewTextBoxEditingControl te = (DataGridViewTextBoxEditingControl)e.Control;
                    te.AutoCompleteMode = AutoCompleteMode.None;
                    LoadDataToToolbox.AutoCompleteTextBox(te, "SELECT EmployeeID  FROM HRM_Employee WHERE RecordStatus < 99");
                    e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);
                    break;
                case 3:
                    te = (DataGridViewTextBoxEditingControl)e.Control;
                    te.AutoCompleteMode = AutoCompleteMode.None;
                    break;
                case 4:
                    te = (DataGridViewTextBoxEditingControl)e.Control;
                    te.AutoCompleteMode = AutoCompleteMode.None;
                    break;
                case 5:
                    te = (DataGridViewTextBoxEditingControl)e.Control;
                    te.AutoCompleteMode = AutoCompleteMode.None;
                    break;
                case 7:
                    te = (DataGridViewTextBoxEditingControl)e.Control;
                    te.AutoCompleteMode = AutoCompleteMode.None;
                    break;
                case 11:
                    te = (DataGridViewTextBoxEditingControl)e.Control;
                    te.AutoCompleteMode = AutoCompleteMode.None;
                    break;

            }
        }
        private void Control_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = false;
        }
        private void GV_Employee_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow zRowEdit = GV_Employee.Rows[e.RowIndex];
            Temp_Excel_Detail_Info zInfo;
            if (GV_Employee.Rows[e.RowIndex].Tag == null)
            {
                zInfo = new Temp_Excel_Detail_Info();
                GV_Employee.Rows[e.RowIndex].Tag = zInfo;
            }
            else
            {
                zInfo = (Temp_Excel_Detail_Info)GV_Employee.Rows[e.RowIndex].Tag;
            }
            bool zIsChangeValued = false;
            switch (e.ColumnIndex)
            {
                case 2:

                    if (zRowEdit.Cells[e.ColumnIndex].Value == null)
                    {
                        zRowEdit.Cells["EmployeeID"].ErrorText = "Sai định dạng!";
                        zIsChangeValued = false;

                    }
                    else
                    {
                        zRowEdit.Cells["EmployeeID"].ErrorText = "";
                        if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() != zInfo.EmployeeID.ToString())
                        {
                            string EmployeeID = GV_Employee.CurrentCell.Value.ToString().Trim();
                            Employee_Info zEm = new Employee_Info();
                            zEm.GetEmployee(EmployeeID);
                            if (zEm.Key == "")
                            {
                                MessageBox.Show("Không có tên nhân viên này", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                for (int o = 1; o <= 11; o++)
                                {
                                    GV_Employee.CurrentRow.Cells[o].Value = "";
                                }

                            }
                            else
                            {
                                int t = 0;
                                for (int i = 0; i < GV_Employee.Rows.Count; i++)
                                {
                                    if (GV_Employee.Rows[i].Cells["EmployeeID"].Value == EmployeeID && GV_Employee.Rows[i].Cells["EmployeeID"].Value != zRowEdit.Cells[e.ColumnIndex].Value)
                                        t++;
                                }
                                if (t != 0)
                                {
                                    MessageBox.Show("Đã có trong danh sách!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                                else
                                {
                                    zInfo.EmployeeKey = zEm.Key;
                                    zInfo.EmployeeID = zEm.EmployeeID;
                                    zInfo.EmployeeName = zEm.FullName;
                                    zIsChangeValued = true;
                                }

                            }

                        }
                    }
                    break;
                case 3: // thong tin ve K
                    if (zRowEdit.Cells[e.ColumnIndex].Value == null)
                    {
                        zRowEdit.Cells["BeginTime"].ErrorText = "Sai định dạng!";
                        zIsChangeValued = false;
                    }
                    else
                    {
                        if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() != zInfo.BeginTime.ToString())
                        {
                            if (zRowEdit.Cells["BeginTime"].Value.ToString().Trim().Length == 16)
                            {
                                zInfo.BeginTime = DateTime.Parse(zRowEdit.Cells[e.ColumnIndex].Value.ToString()); //;  
                                zRowEdit.Cells["BeginTime"].ErrorText = "";
                                zIsChangeValued = true;
                            }
                            else
                            {
                                zRowEdit.Cells["BeginTime"].ErrorText = "Sai định dạng!";
                                zIsChangeValued = false;
                            }

                        }
                    }
                    break;
                case 4: // thong tin ve K
                    if (zRowEdit.Cells[e.ColumnIndex].Value == null)
                    {
                        zRowEdit.Cells["EndTime"].ErrorText = "Sai định dạng!";
                        zIsChangeValued = true;
                    }
                    else
                    {
                        if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() != zInfo.EndTime.ToString())
                        {
                            if (zRowEdit.Cells["EndTime"].Value.ToString().Trim().Length == 16)
                            {
                                zInfo.EndTime = DateTime.Parse(zRowEdit.Cells[e.ColumnIndex].Value.ToString()); //;  
                                zRowEdit.Cells["EndTime"].ErrorText = "";
                                zIsChangeValued = true;
                            }
                            else
                            {
                                zRowEdit.Cells["EndTime"].ErrorText = "Sai định dạng!";
                                zIsChangeValued = false;
                            }
                        }
                    }
                    break;
                case 5: // thong tin ve K
                    if (zRowEdit.Cells[e.ColumnIndex].Value == null)
                    {
                        zRowEdit.Cells["OffTime"].ErrorText = "Sai định dạng!";
                        zIsChangeValued = true;
                    }
                    else
                    {
                        if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() != zInfo.OffTime.ToString())
                        {

                            zInfo.OffTime = float.Parse(zRowEdit.Cells[e.ColumnIndex].Value.ToString()); //;  
                            zRowEdit.Cells["OffTime"].ErrorText = "";
                            zIsChangeValued = true;
                        }
                    }
                    break;
                case 7: // thong tin ve K
                    if (zRowEdit.Cells[e.ColumnIndex].Value == null)
                    {
                        zRowEdit.Cells["DifferentTime"].ErrorText = "Sai định dạng!";
                        zIsChangeValued = true;
                    }
                    else
                    {
                        if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() != zInfo.DifferentTime.ToString())
                        {

                            zInfo.DifferentTime = float.Parse(zRowEdit.Cells[e.ColumnIndex].Value.ToString()); //;  
                            zRowEdit.Cells["DifferentTime"].ErrorText = "";
                            zIsChangeValued = true;
                        }
                    }
                    break;
                
                case 11: // thong tin KP
                    if (zRowEdit.Cells[e.ColumnIndex].Value == null)
                    {
                        zRowEdit.Cells["Code"].ErrorText = "Sai định dạng!";
                        zInfo.CodeName = "";
                        zIsChangeValued = true;
                    }
                    else
                    {
                        if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() != zInfo.CodeName.ToString())
                        {

                            if (zRowEdit.Cells["Code"].Value.ToString().Trim().Length > 0 && zRowEdit.Cells["Code"].Value.ToString().Trim().Length ==5)
                            {
                                string zCodeName = zRowEdit.Cells["Code"].Value.ToString().Trim();
                                string zCodeRice = zCodeName.Substring(3, 2);
                                int zCountRice = Temp_Excel_Detail_Data.Count_Key_Rice(zCodeRice);
                                float zMoney_Rice = Temp_Excel_Detail_Data.Get_Money_Rice(zCodeRice);
                                if(zCountRice >0 || zMoney_Rice>0)
                                {
                                    zInfo.Code = zRowEdit.Cells[e.ColumnIndex].Value.ToString(); //; 
                                    zRowEdit.Cells["Code"].ErrorText = "";
                                } 
                                else
                                {
                                    zRowEdit.Cells["Code"].ErrorText = "Sai định dạng!";
                                }

                            }
                            else
                            {
                                zRowEdit.Cells["Code"].ErrorText = "Sai định dạng!";
                            }
                            zIsChangeValued = true;
                        }
                    }

                    break;

            }
            if (zIsChangeValued)
            {
                zInfo.RecordStatus = 3; 
                CaculateAmount(e.RowIndex);
                ShowProductInGridView(e.RowIndex);
            }
            if (zInfo.Key != 0)
            {
                zInfo.RecordStatus = 2;//2 la cap nhat thay dôi
                zInfo.Hand = 2;//cập nhật tay
            }
            else
            {
                zInfo.RecordStatus = 3;//3 la tao moi
                zInfo.Hand = 1;//tạo tay
            }
        }

        private void CaculateAmount(int RowIndex)
        {
            Temp_Excel_Detail_Info zProduct = (Temp_Excel_Detail_Info)GV_Employee.Rows[RowIndex].Tag;
            DateTime zBeginTime = zProduct.BeginTime;
            DateTime zEndTime = zProduct.EndTime;
            float zOffTime = zProduct.OffTime;
            float zDifferentTime = zProduct.DifferentTime;

            // Tổng thời gian làm
            TimeSpan zBeginEnd = zEndTime - zBeginTime;
            //Tổng thời gian chưa trừ thời gian nghỉ trưa /60 phút
            float zTotal_Temp = float.Parse((zBeginEnd.Hours).ToString()) + (zBeginEnd.Minutes).ToFloat() / 60;
            //Tổng thời gian đã trừ thời gian nghỉ trưa
            float zTotal = zTotal_Temp - zOffTime;
            zProduct.TotalTime = Math.Round(zTotal, 2).ToFloat();
            float zOverTime = 0;

            if (zTotal <= 8)
            {
                zOverTime = 0;
                zProduct.OverTime = zOverTime;
            }
            else
            {
                zOverTime = Math.Round(zTotal, 2).ToFloat() - zDifferentTime - 8;
                zProduct.OverTime = zOverTime;
            }


            float zValue = Default_Parameter_Data.Get_Money_OverTime();
            float zMoney = Math.Round(zOverTime, 2).ToFloat() * zValue;
            zProduct.Value = zValue;
            zProduct.Money = zMoney;

            string zCode = "";
            string zDefine = "";
            string zRiceName = "";
            Time_Defines_Info ztime = new Time_Defines_Info();
            Time_Rice_Info zRice_Info = new Time_Rice_Info();
            if (zProduct.Code.Trim().Length > 0 && zProduct.Code.Trim().Length == 5 && zProduct.Code != null)
            {
                zCode = zProduct.Code;
                zDefine = zCode.Substring(0, 3);
                zRiceName = zCode.Substring(3, 2);
            }
            ztime.Get_Info(zDefine);
            zRice_Info.Get_Info(zRiceName);

            zProduct.CodeKey = ztime.Key;
            zProduct.CodeName = ztime.CodeName;
            zProduct.RiceKey = zRice_Info.Key;
            zProduct.RiceName = zRice_Info.RiceName;
            zProduct.Money_Rice = zRice_Info.Money;

        }
        private void ShowProductInGridView(int RowIndex)
        {
            DataGridViewRow zRowView = GV_Employee.Rows[RowIndex];
            Temp_Excel_Detail_Info zProduct = (Temp_Excel_Detail_Info)zRowView.Tag;

            zRowView.Cells["No"].Value = (RowIndex + 1).ToString();
            zRowView.Cells["No"].Tag = zProduct.Key;
            zRowView.Cells["EmployeeID"].Value = zProduct.EmployeeID;
            zRowView.Cells["EmployeeName"].Value = zProduct.EmployeeName;
            DateTime zBeginTime = zProduct.BeginTime;
            zRowView.Cells["BeginTime"].Value = zBeginTime.ToString("dd/MM/yyyy HH:mm");
            DateTime zEndTime = zProduct.EndTime;
            zRowView.Cells["EndTime"].Value = zEndTime.ToString("dd/MM/yyyy HH:mm");
            float zOffTime = zProduct.OffTime;
            zRowView.Cells["OffTime"].Value = Math.Round(zOffTime, 2).ToString();
            float zDifferentTime = zProduct.DifferentTime;
            zRowView.Cells["DifferentTime"].Value = Math.Round(zDifferentTime, 2).ToString();
            float zTotalTime = zProduct.TotalTime;
            zRowView.Cells["TotalTime"].Value = Math.Round(zTotalTime, 2).ToString();
            float zOverTime = zProduct.OverTime;
            zRowView.Cells["OverTime"].Value = Math.Round(zOverTime, 2).ToString();
            float zValue = zProduct.Value;
            if (zValue == 0)
            {
                zRowView.Cells["Value"].Value = "0";
            }
            else
            {

                zRowView.Cells["Value"].Value = zValue.ToString("###,###,###").Trim();
            }
            double zMoney = zProduct.Money;
            if (zMoney == 0)
            {
                zRowView.Cells["Money"].Value = "0";
            }
            else
            {
                zRowView.Cells["Money"].Value = zMoney.ToString("###,###,###").Trim();
            }

            zRowView.Cells["Code"].Value = zProduct.Code;

            double zMoney_Rice = zProduct.Money_Rice;
            if (zMoney_Rice == 0)
            {
                zRowView.Cells["Money_Rice"].Value = "0";
            }
            else
            {
                zRowView.Cells["Money_Rice"].Value = zMoney_Rice.ToString("###,###,###").Trim();
            }
        }
        #endregion

        #region[Event Button chốt]
        int _indexttable = 0;
        int _Count = 0;
        private void btn_Done_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            lb_Log.Items.Add("Đang kiểm tra dữ liệu!");
            if (CheckBeforeSave() != "")
            {
                MessageBox.Show("Vui lòng kiểm tra lại thông tin \n" + zError, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                ButonShow();
                this.Cursor = Cursors.Default;
                lb_Log.Items.Add("Vui lòng kiểm tra lại thông tin");
                lb_Log.Items.Add("--------------------------");
            }
            else
            {
                Temp_Excel_Detail_Info zinfo = new Temp_Excel_Detail_Info();
                zinfo.Delete_Working_Time_Rice(_ExcelKey);
                //string tem = Temp_Excel_Detail_Data.Delete_WK_Time_Permit(_ExcelKey);
                if (zinfo.Message == string.Empty)
                {
                    ButonHide();
                    _indexttable = 0;
                    timer1.Start();
                    lb_Log.Items.Add("Đang phân tích dữ liệu!");
                    btn_Done.Enabled=false;
                }
                else
                {
                    MessageBox.Show(zinfo.Message);
                    this.Cursor = Cursors.Default;
                    btn_Done.Enabled=true;
                }

            }

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            // int _indexttable = 0;
            timer1.Stop();
            int zCodeKey = int.Parse(ztb.Rows[_indexttable]["CodeKey"].ToString().Trim());
            int zRiceKey = int.Parse(ztb.Rows[_indexttable]["RiceKey"].ToString().Trim());
            string zEmployeeKey = ztb.Rows[_indexttable]["EmployeeKey"].ToString().Trim();
            DateTime zBeginTime = DateTime.Parse(ztb.Rows[_indexttable]["BeginTime"].ToString().Trim());
            _Date = DateTime.Parse(ztb.Rows[_indexttable]["BeginTime"].ToString().Trim());
            DateTime zEndTime = DateTime.Parse(ztb.Rows[_indexttable]["EndTime"].ToString().Trim());
            lb_Log.Items.Add("Đang xử lý " + (_indexttable + 1) + " - " + "Mã NV" + ":" + ztb.Rows[_indexttable]["EmployeeID"].ToString());
            lb_Log.Items.Add("Đang phân tích mã: " + ztb.Rows[_indexttable]["Code"].ToString());
            // Time_Defines_Info zcode = new Time_Defines_Info(zCodeKey);
            //if (zcode.Slug == 1)
            //{
            Time_Working_Info zinfo = new Time_Working_Info(zEmployeeKey, zBeginTime, zEndTime);
            zinfo.EmployeeKey = zEmployeeKey;
            zinfo.WorkingID = ztb.Rows[_indexttable]["ExcelKey"].ToString().Trim();
            zinfo.BeginTime = zBeginTime;
            zinfo.EndTime = zEndTime;
            zinfo.OffTime = float.Parse(ztb.Rows[_indexttable]["OffTime"].ToString().Trim());
            zinfo.CodeKey = zCodeKey;
            zinfo.Save();
            if (zinfo.Message == string.Empty)
                lb_Log.Items.Add("Mã: " + ztb.Rows[_indexttable]["CodeName"] + " .Chấm công thành công!");
            else
                lb_Log.Items.Add("Mã: " + ztb.Rows[_indexttable]["CodeName"] + " .Châm công thất bại!");
            // Chấm công giờ dư
            Employee_Time_Info zTime = new Employee_Time_Info(zEmployeeKey, zBeginTime, zEndTime);
            zTime.EmployeeKey = zEmployeeKey;
            zTime.ExcelKey = int.Parse(ztb.Rows[_indexttable]["ExcelKey"].ToString().Trim());
            zTime.BeginTime = zBeginTime;
            zTime.EndTime = zEndTime;
            zTime.OffTime = float.Parse(ztb.Rows[_indexttable]["OffTime"].ToString().Trim());

            zTime.DifferentTime = float.Parse(ztb.Rows[_indexttable]["DifferentTime"].ToString().Trim());
            zTime.TotalTime = float.Parse(ztb.Rows[_indexttable]["TotalTime"].ToString().Trim());
            zTime.OverTime = float.Parse(ztb.Rows[_indexttable]["OverTime"].ToString().Trim());
            zTime.Value = float.Parse(ztb.Rows[_indexttable]["Value"].ToString().Trim());
            zTime.Money = float.Parse(ztb.Rows[_indexttable]["Money"].ToString().Trim());

            zTime.CheckTime = _Date;
            zTime.Save();
            if (zTime.Message == string.Empty)
                lb_Log.Items.Add("Mã: " + ztb.Rows[_indexttable]["CodeName"] + " .Giờ dư thành công!");
            else
                lb_Log.Items.Add("Mã: " + ztb.Rows[_indexttable]["CodeName"] + " .Giờ dư thất bại!");

            // Tiền cơm
            Employee_Rice_Info zRice = new Employee_Rice_Info(zEmployeeKey, zBeginTime, zEndTime);
            zRice.EmployeeKey = zEmployeeKey;
            zRice.ExcelKey = int.Parse(ztb.Rows[_indexttable]["ExcelKey"].ToString().Trim()); ;
            zRice.Money = float.Parse(ztb.Rows[_indexttable]["Money_Rice"].ToString().Trim());
            zRice.RiceKey = int.Parse(ztb.Rows[_indexttable]["RiceKey"].ToString().Trim());
            zRice.BeginTime = zBeginTime;
            zRice.EndTime = zEndTime;
            zRice.CheckTime = _Date;
            zRice.Save();
            if (zRice.Message == string.Empty)
                lb_Log.Items.Add("Mã: " + ztb.Rows[_indexttable]["RiceName"] + " .Tiền cơm thành công!");
            else
                lb_Log.Items.Add("Mã: " + ztb.Rows[_indexttable]["RiceName"] + " .Tiền cơm thất bại!");

            lb_Log.Items.Add("Phân tích thành công!");
            lb_Log.Items.Add("---------------------");

            _indexttable++;
            if (_indexttable < ztb.Rows.Count)
                timer1.Start();
            else
            {
                MessageBox.Show("Chốt dữ liệu hoàn thành!","Thông báo",MessageBoxButtons.OK,MessageBoxIcon.Information);
                lb_Log.Items.Add("Chốt dữ liệu hoàn thành!");
                ButonShow();
                this.Cursor = Cursors.Default;
                btn_Done.Enabled = true;
            }

        }

        #endregion

        private void btn_Hide_Click(object sender, EventArgs e)
        {
            if (btn_Hide.Tag == "0")
            {
                GB_Log.Visible = false;
                btn_Hide.Tag = "1";
                GB_Detail.Width = this.groupBox3.Width;
                btn_Hide.Text = "<<";
            }
            else
            {
                GB_Log.Visible = true;
                btn_Hide.Tag = "0";
                GB_Detail.Width = this.groupBox3.Width - 205;
                btn_Hide.Text = ">>";
            }

        }
        private void ButonShow()
        {
            btn_Save.Enabled = true;
            btn_New.Enabled = true;
            btn_Del.Enabled = true;
        }
        private void ButonHide()
        {
            btn_Save.Enabled = false;
            btn_New.Enabled = false;
            btn_Del.Enabled = false;
        }
        private void btn_Save_Click(object sender, EventArgs e)
        {
            DialogResult dlr = MessageBox.Show("Bạn có chắc thay đổi số liệu không ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dlr == DialogResult.Yes)
            {
                lb_Log.Items.Add("Đang xử lý dữ liệu!");
                ButonHide();
                this.Cursor = Cursors.WaitCursor;
                if (CheckBeforeSave()!="")
                {
                    MessageBox.Show("Vui lòng kiểm tra lại thông tin \n"+ zError,"Thông báo",MessageBoxButtons.OK,MessageBoxIcon.Information);
                    ButonShow();
                    this.Cursor = Cursors.Default;
                }
                else
                {
                    _List.Clear();
                    _List = new List<Temp_Excel_Detail_Info>();
                    Temp_Excel_Detail_Info zInfo;
                    for (int i = 0; i < GV_Employee.Rows.Count; i++)
                    {
                        if (GV_Employee.Rows[i].Tag != null)
                        {
                            zInfo = (Temp_Excel_Detail_Info)GV_Employee.Rows[i].Tag;
                            if(zInfo.Key ==0)
                            {
                                zInfo.ExcelKey = _ExcelKey;
                            }
                            _List.Add(zInfo);
                        }
                    }
                    string zMessage = "";
                    for (int i = 0; i < _List.Count; i++)
                    {
                        Temp_Excel_Detail_Info zProduct = (Temp_Excel_Detail_Info)_List[i];
                        zProduct.ModifiedBy = SessionUser.UserLogin.EmployeeKey;
                        zProduct.ModifiedName = SessionUser.UserLogin.EmployeeName;
                        switch (zProduct.RecordStatus)
                        {
                            case 99:
                                zProduct.Delete();
                                break;
                            case 3:
                                zProduct.Create();
                                break;
                            case 2:
                                zProduct.Update();
                                break;
                        }
                         zMessage = TN_Message.Show(zProduct.Message);
                    }
                    if (zMessage.Length == 0)
                    {
                        ztb = Temp_Excel_Detail_Data.LoadExcelData(ExcelKey);
                        GV_Employee_LoadData(ztb);
                        MessageBox.Show("Câp nhật thành công", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lb_Log.Items.Add("Cập nhật dữ liệu thành công!");
                    }
                    else
                    {
                        MessageBox.Show(zMessage, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        lb_Log.Items.Add("Cập nhật dữ liệu không thành công!");
                    }
                    ButonShow();
                    this.Cursor = Cursors.Default;
                }
               
            }

        }
        string zError = "";
        private string CheckBeforeSave()
        {
            zError = "";
            int zEmployeeID = 0;
            int zOffTime = 0;
            int zDifferent = 0;
            int zTotaltime =0;
            int zCode = 0;
            int zOvertime = 0;
            for (int i = 0; i < GV_Employee.Rows.Count; i++)
            {
                GV_Employee.Rows[i].DefaultCellStyle.BackColor = Color.White;
                if (GV_Employee.Rows[i].Cells["EmployeeID"].Value.ToString() == "" && GV_Employee.Rows[i].Visible == true)
                {
                    zEmployeeID++;
                    GV_Employee.Rows[i].DefaultCellStyle.BackColor = Color.Tomato;
                }
                if ((GV_Employee.Rows[i].Cells["OffTime"].Value.ToString() == "" || GV_Employee.Rows[i].Cells["OffTime"].Value.ToFloat() < 0) && GV_Employee.Rows[i].Visible == true)
                {
                    zOffTime++;
                    GV_Employee.Rows[i].DefaultCellStyle.BackColor = Color.Tomato;
                }
                if ((GV_Employee.Rows[i].Cells["OverTime"].Value.ToString() == "" || GV_Employee.Rows[i].Cells["OverTime"].Value.ToFloat() < 0) && GV_Employee.Rows[i].Visible == true)
                {
                    zOvertime++;
                    GV_Employee.Rows[i].DefaultCellStyle.BackColor = Color.Tomato;
                }
                if ((GV_Employee.Rows[i].Cells["DifferentTime"].Value.ToString() == "" || GV_Employee.Rows[i].Cells["DifferentTime"].Value.ToFloat() < 0) && GV_Employee.Rows[i].Visible == true)
                {
                    zDifferent++;
                    GV_Employee.Rows[i].DefaultCellStyle.BackColor = Color.Tomato;
                }
                if ((GV_Employee.Rows[i].Cells["TotalTime"].Value.ToString() == "" || GV_Employee.Rows[i].Cells["TotalTime"].Value.ToFloat() < 0) && GV_Employee.Rows[i].Visible == true)
                {
                    zTotaltime++;
                    GV_Employee.Rows[i].DefaultCellStyle.BackColor = Color.Tomato;
                }
                if (GV_Employee.Rows[i].Cells["Code"].Value.ToString() == ""  && GV_Employee.Rows[i].Visible == true)
                {
                    zCode++;
                    GV_Employee.Rows[i].DefaultCellStyle.BackColor = Color.Tomato;
                }
            }
            if(zEmployeeID >0)
                zError += "Mã nhân viên\n";
            if (zOffTime > 0)
                zError += "Thời gian nghỉ \n";
            if (zDifferent > 0)
                zError += "Thời gian gian khác\n";
            if (zTotaltime > 0)
                zError += "Tổng thời gian \n";
            if (zOvertime > 0)
                zError += "Số giờ dư \n";
            if (zCode > 0)
                zError += "Mã nhân chấm công- mã cơm\n";
            return zError;

        }

    }
}
