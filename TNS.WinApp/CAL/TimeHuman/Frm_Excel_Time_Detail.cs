﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using TNS.HRM;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Excel_Time_Detail : Form
    {
        public int ExcelKey = 0;
        public string Title = "";
        DataTable _TableDefines = new DataTable();
        List<Temp_Import_Detail_Info> _List = new List<Temp_Import_Detail_Info>();
        public Frm_Excel_Time_Detail()
        {
            InitializeComponent();
            LoadTableDefines();
            btn_Save.Click += Btn_Save_Click;
            LVData.Click += LV_List_ItemActivate;

            GVData.CellEndEdit += GV_Employee_CellEndEdit;
            GVData.EditingControlShowing += GV_Employee_EditingControlShowing;
        }
        private void Frm_Import_TimeWorking_Load(object sender, EventArgs e)
        {
            this.DoubleBuffered = true;
            Utils.DoubleBuffered(GVData, true);
            Utils.DrawLVStyle(ref LVData);
            Utils.DrawGVStyle(ref GVData);

            InitLayout_GV();
            InitLayout_LV(LVData);
            InitData_LV(ExcelKey);

            txtTitle.Text = "Thời gian công nhân làm việc [" + Title + "]";
            this.Bounds = Screen.PrimaryScreen.WorkingArea;

            Utils.SizeLastColumn_LV(LVData);
        }

        #region [ListView]
        private void InitLayout_LV(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "Stt";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ngày ";
            colHead.Width = 300;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);
        }

        private void InitData_LV(int ExcelKey)
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LVData;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            DataTable In_Table = Temp_Import_Sheet_Data.ListSheet(ExcelKey);
            if (In_Table.Rows.Count > 0)
            {
                LV.Items.Clear();
                int n = In_Table.Rows.Count;
                for (int i = 0; i < n; i++)
                {
                    lvi = new ListViewItem();
                    lvi.Text = "";
                    lvi.ForeColor = Color.Navy;
                    DataRow nRow = In_Table.Rows[i];
                    lvi.Tag = nRow["SheetKey"].ToString();
                    lvi.BackColor = Color.White;

                    lvi.ImageIndex = i;
                    lvsi = new ListViewItem.ListViewSubItem();
                    lvsi.Text = "";// nRow["SheetName"].ToString().Trim();
                    lvi.SubItems.Add(lvsi);

                    LV.Items.Add(lvi);
                }
            }
            this.Cursor = Cursors.Default;

        }
        private void LV_List_ItemActivate(object sender, EventArgs e)
        {
            if (LVData.SelectedItems.Count > 0)
            {
                int _SheetKey = LVData.SelectedItems[0].Tag.ToInt();
                txtSubTitle.Text = "Thời gian nhóm công nhân làm việc trong ngày [" + _SheetKey + "]";
                DataTable zTable = Temp_Import_Detail_Data.ListDetail(_SheetKey);
                GV_Employee_LoadData(zTable);
            }
        }
        #endregion

        #region[EVENT]
        private void Btn_Save_Click(object sender, EventArgs e)
        {
            _List.Clear();
            _List = new List<Temp_Import_Detail_Info>();
            Temp_Import_Detail_Info zInfo;
            for (int i = 0; i < GVData.Rows.Count; i++)
            {
                if (GVData.Rows[i].Tag != null)
                {
                    zInfo = (Temp_Import_Detail_Info)GVData.Rows[i].Tag;
                    _List.Add(zInfo);
                }
            }
            string zMessage = "";
            for (int i = 0; i < _List.Count; i++)
            {
                zInfo = _List[i];
                zInfo.ModifiedBy = SessionUser.UserLogin.EmployeeKey;
                zInfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
                switch (zInfo.RecordStatus)
                {
                    case 2:
                        zInfo.Update();
                        break;
                }
                zMessage = TN_Message.Show(zInfo.Message);
            }
            if (zMessage.Length == 0)
            {
                MessageBox.Show("Câp nhật thành công", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show(zMessage, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        #region [GV_Employee]
        private void InitLayout_GV()
        {
            // Setup Column 
            GVData.Columns.Add("No", "STT");
            GVData.Columns.Add("EmployeeName", "Tên Nhân Viên");
            GVData.Columns.Add("EmployeeID", "Mã NV");
            GVData.Columns.Add("TeamID", "Bộ phận");

            GVData.Columns.Add("BeginTime", "Giờ bắt đầu");
            GVData.Columns.Add("EndTime", "Giờ kết thúc");
            GVData.Columns.Add("OffTime", "Số giờ nghỉ trưa");
            GVData.Columns.Add("DifferentTime", "Giờ CV khác");
            GVData.Columns.Add("TotalTime", "Tổng giờ");
            GVData.Columns.Add("OverTime", "Giờ dư");
            GVData.Columns.Add("OverTimeBegin", "Qua đêm từ");
            GVData.Columns.Add("OverTimeEnd", "Qua đêm đến");

            GVData.Columns.Add("Amount", "Tiền dư/1h");
            GVData.Columns.Add("Money", "Thành tiền");

            GVData.Columns["No"].Width = 40;
            GVData.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GVData.Columns["No"].ReadOnly = true;
            GVData.Columns["No"].Frozen = true;

            GVData.Columns["EmployeeName"].Width = 160;
            GVData.Columns["EmployeeName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GVData.Columns["EmployeeName"].ReadOnly = true;
            GVData.Columns["EmployeeName"].Frozen = true;

            GVData.Columns["EmployeeID"].Width = 90;
            GVData.Columns["EmployeeID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GVData.Columns["EmployeeID"].ReadOnly = true;
            GVData.Columns["EmployeeID"].Frozen = true;

            GVData.Columns["TeamID"].Width = 90;
            GVData.Columns["TeamID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GVData.Columns["TeamID"].ReadOnly = true;
            GVData.Columns["TeamID"].Frozen = true;

            GVData.Columns["BeginTime"].Width = 80;
            GVData.Columns["BeginTime"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GVData.Columns["EndTime"].Width = 80;
            GVData.Columns["EndTime"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GVData.Columns["OffTime"].Width = 80;
            GVData.Columns["OffTime"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GVData.Columns["TotalTime"].Width = 80;
            GVData.Columns["TotalTime"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GVData.Columns["DifferentTime"].Width = 100;
            GVData.Columns["DifferentTime"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GVData.Columns["OverTime"].Width = 80;
            GVData.Columns["OverTime"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GVData.Columns["Money"].Width = 80;
            GVData.Columns["Money"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GVData.Columns["Amount"].Width = 80;
            GVData.Columns["Amount"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GVData.ColumnHeadersHeight = 40;
        }
        private void GV_Employee_LoadData(DataTable InTable)
        {
            _List = new List<Temp_Import_Detail_Info>();
            foreach (DataRow nRow in InTable.Rows)
            {
                Temp_Import_Detail_Info zinfo = new Temp_Import_Detail_Info(nRow);
                _List.Add(zinfo);
            }
            GVData.Rows.Clear();
            for (int i = 1; i <= _List.Count; i++)
            {
                GVData.Rows.Add();
            }
            for (int i = 0; i < _List.Count; i++)
            {
                Temp_Import_Detail_Info zinfo = _List[i];
                GVData.Rows[i].Tag = zinfo;
                GVData.Rows[i].Cells["No"].Tag = zinfo.Key;
                GVData.Rows[i].Cells["No"].Value = (i + 1).ToString();
                GVData.Rows[i].Cells["EmployeeName"].Value = zinfo.EmployeeName;
                GVData.Rows[i].Cells["EmployeeID"].Value = zinfo.EmployeeID;
                GVData.Rows[i].Cells["TeamID"].Value = zinfo.TeamID;
                GVData.Rows[i].Cells["BeginTime"].Value = zinfo.BeginTime;
                GVData.Rows[i].Cells["EndTime"].Value = zinfo.EndTime;
                GVData.Rows[i].Cells["OffTime"].Value = zinfo.OffTime;
                GVData.Rows[i].Cells["DifferentTime"].Value = zinfo.DifferentTime;
                GVData.Rows[i].Cells["TotalTime"].Value = zinfo.TotalTime;
                GVData.Rows[i].Cells["OverTime"].Value = zinfo.OverTime;
                if (zinfo.OverTimeBegin == DateTime.MinValue)
                    GVData.Rows[i].Cells["OverTimeBegin"].Value = "";
                else
                    GVData.Rows[i].Cells["OverTimeBegin"].Value = zinfo.OverTimeBegin.ToString("dd/MM/yyyy");
                if (zinfo.OverTimeEnd == DateTime.MinValue)
                    GVData.Rows[i].Cells["OverTimeEnd"].Value = "";
                else
                    GVData.Rows[i].Cells["OverTimeEnd"].Value = zinfo.OverTimeEnd.ToString("dd/MM/yyyy");

                GVData.Rows[i].Cells["Amount"].Value = zinfo.Amount.ToString("n0");
                GVData.Rows[i].Cells["Money"].Value = zinfo.Money.ToString("n0");
            }
        }
        private void GV_Employee_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            DataGridViewTextBoxEditingControl te = (DataGridViewTextBoxEditingControl)e.Control;
            te.AutoCompleteMode = AutoCompleteMode.None;
            switch (GVData.CurrentCell.ColumnIndex)
            {

                case 4:
                    e.Control.KeyPress += new KeyPressEventHandler(ControlNumber_KeyPress);
                    break;
                case 5:
                    e.Control.KeyPress += new KeyPressEventHandler(ControlNumber_KeyPress);
                    break;
                case 6:
                    e.Control.KeyPress += new KeyPressEventHandler(ControlNumber_KeyPress);
                    break;
                case 7:
                    e.Control.KeyPress += new KeyPressEventHandler(ControlNumber_KeyPress);
                    break;
                case 8:
                    e.Control.KeyPress += new KeyPressEventHandler(ControlNumber_KeyPress);
                    break;
                case 9:
                    e.Control.KeyPress += new KeyPressEventHandler(ControlNumber_KeyPress);
                    break;
                case 12:
                    e.Control.KeyPress += new KeyPressEventHandler(ControlNumber_KeyPress);
                    break;
                case 13:
                    e.Control.KeyPress += new KeyPressEventHandler(ControlNumber_KeyPress);
                    break;
                default:
                    e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);

                    break;
            }
        }
        private void GV_Employee_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow zRowEdit = GVData.Rows[e.RowIndex];
            Temp_Import_Detail_Info zInfo;
            if (GVData.Rows[e.RowIndex].Tag == null)
            {
                zInfo = new Temp_Import_Detail_Info();
                GVData.Rows[e.RowIndex].Tag = zInfo;
            }
            else
            {
                zInfo = (Temp_Import_Detail_Info)GVData.Rows[e.RowIndex].Tag;
            }
            bool zIsChangeValued = false;
            switch (e.ColumnIndex)
            {

                case 4:
                    if (zRowEdit.Cells[e.ColumnIndex].Value == null)
                    {
                        zInfo.BeginTime = "0.0";
                        zIsChangeValued = true;
                    }
                    else
                    {
                        zRowEdit.Cells["BeginTime"].ErrorText = "";
                        if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() != zInfo.BeginTime.ToString())
                        {
                            string zBeginTime = "0.0";
                            if (zRowEdit.Cells[e.ColumnIndex].Value.ToString().Trim() != "")
                            {
                                zBeginTime = zRowEdit.Cells[e.ColumnIndex].Value.ToString().Trim();
                            }
                            zInfo.BeginTime = GetTime(zBeginTime);
                            zIsChangeValued = true;
                        }
                    }
                    break;
                case 5:
                    if (zRowEdit.Cells[e.ColumnIndex].Value == null)
                    {
                        zInfo.EndTime = "0.0";
                        zIsChangeValued = true;
                    }
                    else
                    {
                        zRowEdit.Cells["EndTime"].ErrorText = "";
                        if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() != zInfo.EndTime.ToString())
                        {
                            string zEndTime = "0.0";
                            if (zRowEdit.Cells[e.ColumnIndex].Value.ToString().Trim() != "")
                            {
                                zEndTime = zRowEdit.Cells[e.ColumnIndex].Value.ToString().Trim();
                            }
                            zInfo.EndTime = GetTime(zEndTime);

                            zIsChangeValued = true;
                        }
                    }
                    break;
                case 6:
                    if (zRowEdit.Cells[e.ColumnIndex].Value == null)
                    {
                        zInfo.OffTime = "0.0";
                        zIsChangeValued = true;
                    }
                    else
                    {
                        zRowEdit.Cells["OffTime"].ErrorText = "";
                        if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() != zInfo.OffTime.ToString())
                        {
                            string zOffTime = "0.0";
                            if (zRowEdit.Cells[e.ColumnIndex].Value.ToString().Trim() != "")
                            {
                                zOffTime = zRowEdit.Cells[e.ColumnIndex].Value.ToString().Trim();
                            }
                            zInfo.OffTime = GetTime(zOffTime);
                            zIsChangeValued = true;
                        }
                    }
                    break;
                case 7:
                    if (zRowEdit.Cells[e.ColumnIndex].Value == null)
                    {
                        zInfo.DifferentTime = "0.0";
                        zIsChangeValued = true;
                    }
                    else
                    {
                        zRowEdit.Cells["DifferentTime"].ErrorText = "";
                        if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() != zInfo.DifferentTime.ToString())
                        {
                            string zDifferentTime = "0.0";
                            if (zRowEdit.Cells[e.ColumnIndex].Value.ToString().Trim() != "")
                            {
                                zDifferentTime = zRowEdit.Cells[e.ColumnIndex].Value.ToString().Trim();
                            }
                            zInfo.DifferentTime = GetTime(zDifferentTime);
                            zIsChangeValued = true;
                        }
                    }
                    break;
                case 8:
                    if (zRowEdit.Cells[e.ColumnIndex].Value == null)
                    {
                        zInfo.TotalTime = "0.0";
                        zIsChangeValued = true;
                    }
                    else
                    {
                        zRowEdit.Cells["TotalTime"].ErrorText = "";
                        if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() != zInfo.TotalTime.ToString())
                        {
                            string zTotalTime = "0.0";
                            if (zRowEdit.Cells[e.ColumnIndex].Value.ToString().Trim() != "")
                            {
                                zTotalTime = zRowEdit.Cells[e.ColumnIndex].Value.ToString().Trim();
                            }
                            zInfo.TotalTime = GetTime(zTotalTime);
                            zIsChangeValued = true;
                        }
                    }
                    break;
                case 9:
                    if (zRowEdit.Cells[e.ColumnIndex].Value == null)
                    {
                        zInfo.OverTime = "0.0";
                        zIsChangeValued = true;
                    }
                    else
                    {
                        zRowEdit.Cells["OverTime"].ErrorText = "";
                        if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() != zInfo.OverTime.ToString())
                        {
                            string zOverTime = "0.0";
                            if (zRowEdit.Cells[e.ColumnIndex].Value.ToString().Trim() != "")
                            {
                                zOverTime = zRowEdit.Cells[e.ColumnIndex].Value.ToString().Trim();
                            }
                            zInfo.OverTime = GetTime(zOverTime);
                            zIsChangeValued = true;
                        }
                    }
                    break;

                case 10:
                    if (zRowEdit.Cells[e.ColumnIndex].Value == null)
                    {
                        zInfo.OverTimeBegin = DateTime.MinValue;
                        zIsChangeValued = false;
                    }
                    else
                    {
                        if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() != zInfo.OverTimeBegin.ToString())
                        {
                            DateTime zOverTimeBegin = DateTime.MinValue;
                            if (zRowEdit.Cells[e.ColumnIndex].Value.ToString().Trim() != "")
                            {
                                if (!DateTime.TryParse(zRowEdit.Cells[e.ColumnIndex].Value.ToString().Trim(), out zOverTimeBegin))
                                {
                                    zInfo.OverTimeBegin = DateTime.MinValue;
                                }
                                else
                                {
                                    zInfo.OverTimeBegin = zOverTimeBegin;
                                }
                            }
                            else
                            {
                                zInfo.OverTimeBegin = DateTime.MinValue;
                            }
                            zIsChangeValued = true;
                        }

                    }
                    break;

                case 11:
                    if (zRowEdit.Cells[e.ColumnIndex].Value == null)
                    {
                        zInfo.OverTimeEnd = DateTime.MinValue;
                        zIsChangeValued = true;
                    }
                    else
                    {
                        if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() != zInfo.OverTimeEnd.ToString())
                        {
                            DateTime zOverTimeEnd = DateTime.MinValue;
                            if (zRowEdit.Cells[e.ColumnIndex].Value.ToString().Trim() != "")
                            {
                                if (!DateTime.TryParse(zRowEdit.Cells[e.ColumnIndex].Value.ToString().Trim(), out zOverTimeEnd))
                                {
                                    zInfo.OverTimeEnd = DateTime.MinValue;
                                }
                                else
                                {
                                    zInfo.OverTimeEnd = zOverTimeEnd;
                                }
                            }
                            zIsChangeValued = true;
                        }

                    }
                    break;
            }
            if (zIsChangeValued)
            {
                zInfo.RecordStatus = 2;
                ShowInGridView(e.RowIndex);
            }

        }
        private void ShowInGridView(int RowIndex)
        {
            DataGridViewRow zRowView = GVData.Rows[RowIndex];
            Temp_Import_Detail_Info zInfo = (Temp_Import_Detail_Info)zRowView.Tag;
            zRowView.Cells["No"].Value = (RowIndex + 1).ToString();
            zRowView.Cells["No"].Tag = zInfo.Key;
            zRowView.Cells["EmployeeID"].Value = zInfo.EmployeeID;
            zRowView.Cells["EmployeeName"].Value = zInfo.EmployeeName;
            zRowView.Cells["TeamID"].Value = zInfo.TeamID;
            zRowView.Cells["BeginTime"].Value = zInfo.BeginTime;
            zRowView.Cells["EndTime"].Value = zInfo.EndTime;
            zRowView.Cells["OffTime"].Value = zInfo.OffTime;
            zRowView.Cells["DifferentTime"].Value = zInfo.DifferentTime;
            zRowView.Cells["TotalTime"].Value = zInfo.TotalTime;
            zRowView.Cells["OverTime"].Value = zInfo.OverTime;
            if (zInfo.OverTimeBegin == DateTime.MinValue)
                zRowView.Cells["OverTimeBegin"].Value = "";
            else
                zRowView.Cells["OverTimeBegin"].Value = zInfo.OverTimeBegin.ToString("dd/MM/yyyy");
            if (zInfo.OverTimeEnd == DateTime.MinValue)
                zRowView.Cells["OverTimeEnd"].Value = "";
            else
                zRowView.Cells["OverTimeEnd"].Value = zInfo.OverTimeEnd.ToString("dd/MM/yyyy");

            zRowView.Cells["Amount"].Value = zInfo.Amount;
            zRowView.Cells["Money"].Value = zInfo.Money;
        }

        private void ControlNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }

            if (e.KeyChar == '.')
            {
                e.Handled = false;
            }
            if (e.KeyChar == ',')
            {
                e.Handled = false;
            }
        }
        private void Control_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = false;
        }
        #endregion

        #region[PROCESS]

        void LoadTableDefines()
        {
            string File = Application.StartupPath + "\\wfTime.xlsx";
            _TableDefines = Data_Access.GetTable(File, "1");
        }

        string GetTime(string Time)
        {
            string zResult = "";
            string Minute = "0.0";
            string[] s = Time.Split('.');
            string Hour = s[0];
            if (s.Length > 1)
            {
                Minute = "0." + s[1];
            }
            float temp = float.Parse(Minute);

            foreach (DataRow r in _TableDefines.Rows)
            {
                float from = r[0].ToFloat();
                float to = r[1].ToFloat();
                float min = r[2].ToFloat();
                if (temp >= from && temp <= to)
                {
                    if (temp < (float)0.95)
                    {
                        zResult = Hour + ":" + min;
                    }
                    else
                    {
                        int zHour = Hour.ToInt() + 1;
                        zResult = zHour.ToString() + ":0";
                    }
                    break;
                }
            }
            return zResult;
        }
        #endregion

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}