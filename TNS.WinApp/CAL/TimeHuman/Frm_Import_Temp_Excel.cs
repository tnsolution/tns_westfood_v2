﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using TN.Library.HumanResources;
using TN.Library.Miscellaneous;
using TN.Library.System;
using TN.Library.SystemManagement;

namespace TN_WinApp
{
    public partial class Frm_Import_Temp_Excel : Form
    {
        public Frm_Import_Temp_Excel()
        {
            InitializeComponent();
            btn_Import.Click += btn_Excel_Click;
            btn_Save.Click += btn_Save_Click;
            LV_List.ItemActivate += LV_List_ItemActivate;
            GV_Employee_Layout();
        }
        private string _TeamName = "";
        DateTime _Date;
        private int _Row = 0;
        private Thread _Thread;
        private int _test = 0;
        private int _ExcelKey = 0;

        private string FileName;
        private int _From = 0;
        private bool _IsStop = false;

        private void Frm_Import_TimeKeeping_Load(object sender, EventArgs e)
        {
            LV_Sheet_Layout(LV_List);
            btn_Save.Enabled = false;
        }
        #region [ListView_TeamWorking]

        private void LV_Sheet_Layout(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "No";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên sheet ";
            colHead.Width = 300;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);
        }
        OpenFileDialog of = new OpenFileDialog();
        private void LV_Sheet_LoadSheet()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LV_List;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            DataTable In_Table = Data_Access.GetSheet(of.FileName);
            if (In_Table.Rows.Count <= 0)
            {
                MessageBox.Show("Vui lòng đóng File trước khi upload  !");
                //this.Close();
                //LV.Items.Clear();
                this.Cursor = Cursors.Default;
            }
            else
            {
                LV.Items.Clear();
                int n = In_Table.Rows.Count;
                for (int i = 0; i < n; i++)
                {
                    lvi = new ListViewItem();
                    lvi.Text = (i + 1).ToString();
                    lvi.ForeColor = Color.Navy;
                    DataRow nRow = In_Table.Rows[i];
                    //lvi.Tag = nRow["TeamKey"].ToString();
                    lvi.BackColor = Color.White;

                    lvi.ImageIndex = 0;
                    lvsi = new ListViewItem.ListViewSubItem();
                    lvsi.Text = nRow["sheetname"].ToString().Trim();
                    lvi.SubItems.Add(lvsi);

                    LV.Items.Add(lvi);
                }

                this.Cursor = Cursors.Default;
            }


        }

        private void LV_List_ItemActivate(object sender, EventArgs e)
        {
            _TeamName = LV_List.SelectedItems[0].SubItems[1].Text;
            DataTable zTable = Data_Access.GetTable(of.FileName, _TeamName);

            DataTable zGvTable = new DataTable();
            zGvTable.Columns.Add("AutoKey");
            zGvTable.Columns.Add("EmployeeName");
            zGvTable.Columns.Add("EmployeeID");
            zGvTable.Columns.Add("BeginTime"); // số ngày công tính lương
            zGvTable.Columns.Add("EndTime");// tỉ lệ % hoàn thành trong tháng
            zGvTable.Columns.Add("OffTime");
            zGvTable.Columns.Add("DifferentTime");
            zGvTable.Columns.Add("CodeName");
            zGvTable.Columns.Add();
            foreach (DataRow r in zTable.Rows)
            {
                DataRow rN = zGvTable.NewRow();
                rN["AutoKey"] = 0;
                rN["EmployeeName"] = r[1];
                rN["EmployeeID"] = r[2];
                rN["BeginTime"] = r[3];
                rN["EndTime"] = r[4];
                rN["OffTime"] = r[5];
                rN["DifferentTime"] = r[6];
                rN["CodeName"] = r[7];
                zGvTable.Rows.Add(rN);
            }

            GV_Employee_LoadData(zGvTable);

            //   label3.Text = "THÔNG TIN - " + _TeamName;
        }
        #endregion

        #region [GV_Employee]
        private void GV_Employee_Layout()
        {
            // Setup Column 
            GV_Employee.Columns.Add("No", "STT");
            GV_Employee.Columns.Add("EmployeeName", "Tên Nhân Viên");
            GV_Employee.Columns.Add("EmployeeID", "Mã NV");

            GV_Employee.Columns.Add("BeginTime", "Giờ bắt đầu");
            GV_Employee.Columns.Add("EndTime", "Giờ kết thúc");
            GV_Employee.Columns.Add("OffTime", "Số giờ nghỉ trưa");
            GV_Employee.Columns.Add("TotalTime", "Tổng giờ");
            GV_Employee.Columns.Add("DifferentTime", "Giờ CV khác");
            GV_Employee.Columns.Add("OverTime", "Giờ dư");
            GV_Employee.Columns.Add("Value", "Đơn giá Ngoài giờ");
            GV_Employee.Columns.Add("Money", "Số tiền Ngoài giờ");
            GV_Employee.Columns.Add("CodeName", "Loại công");
            GV_Employee.Columns.Add("Money_Rice", "Tiền cơm");


            GV_Employee.Columns["No"].Width = 40;
            GV_Employee.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV_Employee.Columns["No"].ReadOnly = true;
            GV_Employee.Columns["No"].Frozen = true;

            GV_Employee.Columns["EmployeeName"].Width = 160;
            GV_Employee.Columns["EmployeeName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV_Employee.Columns["EmployeeName"].ReadOnly = true;
            GV_Employee.Columns["EmployeeName"].Frozen = true;

            GV_Employee.Columns["EmployeeID"].Width = 100;
            GV_Employee.Columns["EmployeeID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV_Employee.Columns["EmployeeID"].ReadOnly = true;
            GV_Employee.Columns["EmployeeID"].Frozen = true;

            GV_Employee.Columns["BeginTime"].Width = 140;
            GV_Employee.Columns["BeginTime"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV_Employee.Columns["BeginTime"].ReadOnly = true;

            GV_Employee.Columns["EndTime"].Width = 140;
            GV_Employee.Columns["EndTime"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV_Employee.Columns["EndTime"].ReadOnly = true;

            GV_Employee.Columns["OffTime"].Width = 80;
            GV_Employee.Columns["OffTime"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Employee.Columns["OffTime"].ReadOnly = true;

            GV_Employee.Columns["TotalTime"].Width = 80;
            GV_Employee.Columns["TotalTime"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Employee.Columns["TotalTime"].ReadOnly = true;

            GV_Employee.Columns["DifferentTime"].Width = 100;
            GV_Employee.Columns["DifferentTime"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Employee.Columns["DifferentTime"].ReadOnly = true;

            GV_Employee.Columns["OverTime"].Width = 80;
            GV_Employee.Columns["OverTime"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Employee.Columns["OverTime"].ReadOnly = true;

            GV_Employee.Columns["Value"].Width = 110;
            GV_Employee.Columns["Value"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Employee.Columns["Value"].ReadOnly = true;

            GV_Employee.Columns["Money"].Width = 110;
            GV_Employee.Columns["Money"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Employee.Columns["Money"].ReadOnly = true;

            GV_Employee.Columns["CodeName"].Width = 80;
            GV_Employee.Columns["CodeName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV_Employee.Columns["CodeName"].ReadOnly = true;

            GV_Employee.Columns["Money_Rice"].Width = 80;
            GV_Employee.Columns["Money_Rice"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Employee.Columns["Money_Rice"].ReadOnly = true;

            // setup style view

            GV_Employee.BackgroundColor = Color.White;
            GV_Employee.GridColor = Color.FromArgb(227, 239, 255);
            GV_Employee.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV_Employee.DefaultCellStyle.ForeColor = Color.FromArgb(43, 43, 43);
            GV_Employee.DefaultCellStyle.Font = new Font("Arial", 9, FontStyle.Bold);

            GV_Employee.AllowUserToResizeRows = false;
            GV_Employee.AllowUserToResizeColumns = true;

            GV_Employee.RowHeadersVisible = false;

            //// setup Height Header
            //GV_Employee.ColumnHeadersHeight = GV_Employee.ColumnHeadersHeight * 3 / 2;
            GV_Employee.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 9, FontStyle.Bold);
            GV_Employee.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            GV_Employee.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;

            for (int i = 1; i <= 8; i++)
            {
                GV_Employee.Rows.Add();
            }

        }
        private void GV_Employee_LoadData(DataTable InTable)
        {
            GV_Employee.Rows.Clear();

            int i = 0;
            foreach (DataRow nRow in InTable.Rows)
            {
                GV_Employee.Rows.Add();
                DataGridViewRow nRowView = GV_Employee.Rows[i];
                nRowView.Tag = nRow["EmployeeID"].ToString().Trim();
                nRowView.Cells["No"].Value = (i + 1).ToString();
                nRowView.Cells["EmployeeName"].Value = nRow["EmployeeName"].ToString().Trim();
                nRowView.Cells["EmployeeID"].Value = nRow["EmployeeID"].ToString().Trim();
                DateTime zBeginTime = DateTime.Parse(nRow["BeginTime"].ToString().Trim());
                _Date = zBeginTime;
                nRowView.Cells["BeginTime"].Value = zBeginTime.ToString("dd/MM/yyyy HH:mm");
                DateTime zEndTime = DateTime.Parse(nRow["EndTime"].ToString().Trim());
                nRowView.Cells["EndTime"].Value = zEndTime.ToString("dd/MM/yyyy HH:mm");
                float zOffTime = float.Parse(nRow["OffTime"].ToString().Trim());
                nRowView.Cells["OffTime"].Value = Math.Round(zOffTime,2).ToString();
                float zDifferentTime = 0;
                if (nRow["DifferentTime"].ToString() != "")
                {
                    zDifferentTime = float.Parse(nRow["DifferentTime"].ToString().Trim());
                    nRowView.Cells["DifferentTime"].Value = Math.Round(zDifferentTime, 2).ToString();
                }
                else
                {
                    zDifferentTime = 0;
                    nRowView.Cells["DifferentTime"].Value = Math.Round(zDifferentTime, 2).ToString();
                }
                // Tổng thời gian chưa trừ thời gian nghỉ trưa
                TimeSpan zBeginEnd = zEndTime - zBeginTime;
                //Tổng thời gian chưa trừ thời gian nghỉ trưa /60 phút
                float zTotal_Temp = float.Parse((zBeginEnd.Hours).ToString()) + (zBeginEnd.Minutes).ToFloat() / 60;
                //Tổng thời gian đã trừ thời gian nghỉ trưa
                float zTotal = zTotal_Temp - zOffTime;

                //Giờ dư = Tổng thời gian trừ đi giờ trả tiền ở công việc khác
                float zOverTime = 0;
                if (zTotal < 0)
                {
                    nRowView.Cells["BeginTime"].Style.BackColor = Color.Tomato;
                    nRowView.Cells["EndTime"].Style.BackColor = Color.Tomato;

                    nRowView.Cells["TotalTime"].Style.BackColor = Color.Tomato;
                    nRowView.Cells["OverTime"].Style.BackColor = Color.Tomato;
                    nRowView.Cells["Money"].Style.BackColor = Color.Tomato;
                    _test++;
                }
                if (zTotal <= 8)
                {
                    nRowView.Cells["TotalTime"].Value = Math.Round(zTotal, 2);
                    zOverTime = 0;
                    nRowView.Cells["OverTime"].Value = Math.Round(zOverTime,2);
                }
                else
                {
                    nRowView.Cells["TotalTime"].Value = Math.Round(zTotal, 2);
                    zOverTime = Math.Round(zTotal, 2).ToFloat() - zDifferentTime - 8;
                    nRowView.Cells["OverTime"].Value = Math.Round(zOverTime, 2);
                }



                float zValue = Default_Parameter_Data.Get_Money_OverTime();
                if (zValue == 0)
                {
                    nRowView.Cells["Value"].Value = "0";
                }
                else
                {

                    nRowView.Cells["Value"].Value = zValue.ToString("###,###,###").Trim();
                }
                float zMoney = Math.Round(zOverTime, 2).ToFloat() * zValue;
                if (zMoney == 0)
                {
                    nRowView.Cells["Money"].Value = "0";
                }
                else
                {

                    nRowView.Cells["Money"].Value = zMoney.ToString("###,###,###").Trim();
                }


                if (nRow["CodeName"].ToString().Trim().Length == 0 || nRow["CodeName"].ToString().Trim().Length != 5)
                {
                    nRowView.Cells["CodeName"].Style.BackColor = Color.Tomato;
                    nRowView.Cells["CodeName"].Value = nRow["CodeName"].ToString().Trim();
                    nRowView.Cells["Money_Rice"].Value = "0";
                    _test++;
                }
                else
                {
                    nRowView.Cells["CodeName"].Value = nRow["CodeName"].ToString().Trim();
                    string zCodeName = nRow["CodeName"].ToString().Trim();
                    string zCodeRice = zCodeName.Substring(3, 2);
                    int zCountRice = Temp_Excel_Detail_Data.Count_Key_Rice(zCodeRice);
                    if (zCountRice == 0)
                    {
                        nRowView.Cells["CodeName"].Style.BackColor = Color.Tomato;
                        nRowView.Cells["Money_Rice"].Value = "0";
                        _test++;
                    }
                    else
                    {
                        float zMoney_Rice = Temp_Excel_Detail_Data.Get_Money_Rice(zCodeRice);
                        if (zMoney == 0)
                        {
                            nRowView.Cells["Money_Rice"].Value = "0";
                        }
                        else
                        {
                            nRowView.Cells["Money_Rice"].Value = zMoney_Rice.ToString("###,###,###").Trim();
                        }
                    }

                }

                i++;
            }
            if (_test > 0)
            {
                MessageBox.Show("Dữ liệu không hợp lệ.Vui lòng kiểm tra lại!");
            }
            _Row = i;
            btn_Save.Enabled = true;
        }
        #endregion

        #region[Progess]
        private void Save()
        {

            if (_test > 0)
            {
                DialogResult dlr = MessageBox.Show("Dữ liệu không hợp lệ.Bạn có muốn tiếp tục ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlr == DialogResult.Yes)
                {
                    Save_Detail();
                }
                else
                {
                    btn_Save.Enabled = true;
                }

            }
            else
            {
                Save_Detail();
            }
        }
        private void Save_Detail()
        {
            // chuyển file excel thành kiểu byte
            FileStream fs = new FileStream(FileName, FileMode.Open, FileAccess.Read);
            BinaryReader br = new BinaryReader(fs);
            Byte[] bytes = br.ReadBytes((Int32)fs.Length);
            br.Close();
            fs.Close();

            //Lấy tên file
            string strFileName = Path.GetFileName(FileName).Trim();

            int _Data = 0;
            DateTime FromDate;
            DateTime ToDate;
            FromDate = new DateTime(_Date.Year, _Date.Month, _Date.Day, 0, 0, 0);
            ToDate = new DateTime(_Date.Year, _Date.Month, _Date.Day, 23, 59, 59);

            Temp_Excel_Info ztemp = new Temp_Excel_Info();
            ztemp.DateImport = _Date;
            ztemp.File = strFileName;
            ztemp.Save();
            _ExcelKey = ztemp.ExcelKey;

            Temp_Excel_File_Info zFileExcel = new Temp_Excel_File_Info();
            zFileExcel.ExcelKey = _ExcelKey;
            zFileExcel.FileAttach = bytes;
            zFileExcel.Save();

            foreach (DataGridViewRow nRow in GV_Employee.Rows)
            {
                if (nRow.Tag != null)
                {

                    string zEmployeeID = nRow.Tag.ToString();
                    Employee_Info zEmployee = new Employee_Info();
                    zEmployee.GetEmployee(zEmployeeID);
                    string zCode = "";
                    string zDefine = "";
                    string zRiceName = "";
                    Time_Defines_Info ztime = new Time_Defines_Info();
                    Time_Rice_Info zRice_Info = new Time_Rice_Info();
                    if (nRow.Cells["CodeName"].Value.ToString().Trim().Length > 0 && nRow.Cells["CodeName"].Value.ToString().Trim().Length == 5
                        && nRow.Cells["CodeName"].Value.ToString() != DBNull.Value.ToString())
                    {
                        zCode = nRow.Cells["CodeName"].Value.ToString().Trim();
                        zDefine = zCode.Substring(0, 3);
                        zRiceName = zCode.Substring(3, 2);
                    }
                    ztime.Get_Info(zDefine);
                    zRice_Info.Get_Info(zRiceName);

                    Temp_Excel_Detail_Info zinfo = new Temp_Excel_Detail_Info(zEmployeeID, FromDate, ToDate);
                    zinfo.ExcelKey = _ExcelKey;
                    zinfo.EmployeeKey = zEmployee.Key;
                    zinfo.EmployeeID = zEmployeeID;
                    zinfo.EmployeeName = nRow.Cells["EmployeeName"].Value.ToString().Trim();
                    zinfo.BeginTime = DateTime.Parse(nRow.Cells["BeginTime"].Value.ToString().Trim());
                    zinfo.EndTime = DateTime.Parse(nRow.Cells["EndTime"].Value.ToString().Trim());
                    zinfo.OffTime = float.Parse(nRow.Cells["OffTime"].Value.ToString().Trim());
                    zinfo.TotalTime = float.Parse(nRow.Cells["TotalTime"].Value.ToString().Trim());
                    zinfo.DifferentTime = float.Parse(nRow.Cells["DifferentTime"].Value.ToString().Trim());
                    zinfo.OverTime = float.Parse(nRow.Cells["OverTime"].Value.ToString().Trim());
                    zinfo.Value = float.Parse(nRow.Cells["Value"].Value.ToString().Trim());
                    zinfo.Money = float.Parse(nRow.Cells["Money"].Value.ToString().Trim());
                    zinfo.CodeKey = ztime.Key;
                    zinfo.CodeName = zDefine;
                    zinfo.RiceKey = zRice_Info.Key;
                    zinfo.RiceName = zRiceName;
                    zinfo.Code = nRow.Cells["CodeName"].Value.ToString().Trim();
                    zinfo.Money_Rice = float.Parse(nRow.Cells["Money_Rice"].Value.ToString().Trim());
                    zinfo.Save();
                    _From++;
                }
            }
            _IsStop = true;
            _Thread.Abort();
        }
        #endregion

        #region[Event]
        private void btn_Excel_Click(object sender, EventArgs e)
        {
            of.Filter = "Excel Files|*.xls;*.xlsx;*.xlsm";
            of.Multiselect = false;
            if (DialogResult.OK == of.ShowDialog())
            {
                FileName = of.FileName;
                LV_Sheet_LoadSheet();
            }
        }
        private void btn_Save_Click(object sender, EventArgs e)
        {
            btn_Save.Enabled = false;
            _Thread = new Thread(new ThreadStart(Save));
            _Thread.Start();

            timer1.Start();
            Bar_Progess.Maximum = _Row;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (Bar_Progess.Value <= _Row)
            {
                double t = _From * 100 / _Row;
                lbl_Count.Text = t.ToString() + " %";
                Bar_Progess.Value = _From;

            }

            if (_IsStop)
            {
                Bar_Progess.Value = _Row;
                timer1.Stop();
                MessageBox.Show("Cập nhật thành công", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
                _Row = 0;
                _From = 0;
                btn_Save.Enabled = true;

                Frm_Temp_Excel_View frm = new Frm_Temp_Excel_View();
                frm.ExcelKey = _ExcelKey;
                frm.Show();
            }
        }


        #endregion
    }
}
