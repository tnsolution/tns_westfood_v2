﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TNS.CORE;
using TNS.Misc;

namespace TNS.WinApp
{
    public partial class Frm_Production_Auto_Money : Form
    {
        DataTable _TableTeam;   //nhóm
        DataTable _TableEmployee;  //Các nhân viên trong đơn hàng

        int _TotalTeam = 0;
        int _IndexTeam = 0;
        int _TeamKey = 0;
        string _TeamName = "";

        int _TotalEmp = 0;
        int _IndexEmp = 0;
        string _EmployeeKey = "";

        public Frm_Production_Auto_Money()
        {
            InitializeComponent();

            InitGVTeam_Layout(GVTeam);
            Utils.DrawGVStyle(ref GVTeam);
            Utils.DoubleBuffered(GVTeam, true);

            timerTeam.Enabled = true;
            timerTeam.Stop();

            timerEmployee.Enabled = true;
            timerEmployee.Stop();
        }

        private void InitGVTeam_Layout(DataGridView GV)
        {
            GV.Columns.Add("No", "Stt");
            GV.Columns.Add("TeamID", "Mã nhóm");
            GV.Columns.Add("TeamName", "Tên nhóm");

            GV.Columns["No"].Width = 45;
            GV.Columns["TeamID"].Width = 100;
            GV.Columns["TeamName"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.ColumnHeadersHeight = 25;
            GV.FirstDisplayedScrollingRowIndex = GV.RowCount - 1;
        }
        void ShowGVTeam(DataRow nRow, DataGridView GV)
        {
            GV.Rows.Add();
            DataGridViewRow nRowView = GV.Rows[_IndexTeam];
            nRowView.Tag = nRow["TeamKey"];
            nRowView.Cells["No"].Value = (_IndexTeam + 1).ToString();
            nRowView.Cells["TeamName"].Value = nRow["TeamName"].ToString().Trim();
            nRowView.Cells["TeamID"].Value = nRow["TeamID"].ToString().Trim();
        }
        private void btn_Run_Click(object sender, EventArgs e)
        {
            HeaderControl.Text = HeaderControl.Text + "[Đang xử lý, vui lòng chờ !.]";
                      
            _IndexTeam = 0;
            _IndexEmp = 0;

            LoadTeam();
            timerTeam.Start();
        }

        void LoadTeam()
        {
            _TableTeam = HoTroSanXuat.DanhSachToNhom();
            _TotalTeam = _TableTeam.Rows.Count;
        }

        void LoadEmployee(int TeamKey)
        {
            _TableEmployee = HoTroSanXuat.TinhTienDonHangCuaNhom(TeamKey, dte_FromDate.Value, dte_ToDate.Value);
            _TotalEmp = _TableEmployee.Rows.Count;
        }

        void ProcessEmployee(DataRow r)
        { 
        
        }

        private void timerTeam_Tick(object sender, EventArgs e)
        {
            //tạm dừng vòng team
            timerTeam.Stop();
            PicLoading.Visible = true;
            btn_Run.Enabled = false;
            dte_FromDate.Enabled = false;
            dte_ToDate.Enabled = false;

            if (_IndexTeam < _TotalTeam)
            {
                if (_TableTeam.Rows.Count > 0)
                {
                    DataRow r = _TableTeam.Rows[_IndexTeam];
                    _TeamName = r["TeamName"].ToString();
                    _TeamKey = r["TeamKey"].ToInt();
                    
                    //add nhóm đang xử lý vào view
                    ShowGVTeam(r, GVTeam);
                    //xử lý tiếp data thông tin nhân viên của nhóm
                    LoadEmployee(_TeamKey);


                    _IndexEmp = 0;
                    timerEmployee.Start();
                }
            }
            else
            {
                //tạm dừng vòng team
                timerTeam.Stop();
                PicLoading.Visible = false;
                HeaderControl.Text = "Tính lương năng xuất !.";
                btn_Run.Enabled = true;
                dte_FromDate.Enabled = true;
                dte_ToDate.Enabled = true;
            }
        }

        private void timerEmployee_Tick(object sender, EventArgs e)
        {
            timerEmployee.Stop();
            if (_IndexEmp < _TotalEmp)
            {
                if (_TableEmployee.Rows.Count > 0)
                {
                    DataRow r = _TableEmployee.Rows[_IndexEmp];
                    ProcessEmployee(r);

                    _IndexEmp++;
                    timerEmployee.Start();
                }
                else
                {                  
                    _IndexTeam++;
                    timerTeam.Start();
                }
            }
            else
            {
                _IndexTeam++;
                timerTeam.Start();
            }
        }
    }
}
