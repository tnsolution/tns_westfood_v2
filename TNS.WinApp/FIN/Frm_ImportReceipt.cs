﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TN.Library.System;
using TNS.FIN;
using TNS.HRM;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_ImportReceipt : Form
    {
        bool _IsSaved = false;//kiem tra đã lưu hay chưa
        FileInfo FileInfo;
        DataTable _GVEmployee;
        int zError = 0;
        int zSuccess = 0;
        int _IndexGV = 0;
        int _TotalGV = 0;
        public Frm_ImportReceipt()
        {
            InitializeComponent();
            btnClose.Click += btnClose_Click;
            btnMax.Click += btnMax_Click;
            btnMini.Click += btnMini_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            btn_Open.Click += Btn_Open_Click;
            btn_SaveAll.Click += Btn_SaveAll_Click;
            GV_Employee.CellEndEdit += GV_Employee_CellEndEdit;
            timer1.Tick += Timer1_Tick;
            timer2.Tick += Timer2_Tick;
            timer3.Tick += Timer3_Tick;
        }

        private void Frm_Import_Receipt_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);

            PicLoading.Visible = false;
            this.DoubleBuffered = true;
            Utils.DoubleBuffered(GV_Employee, true);
            Utils.DrawGVStyle(ref GV_Employee);
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            lbl_EmployeeName.Text = "Nhân viên : " + SessionUser.Personal_Info.FullName;
            dte_WorkDate.Value = SessionUser.Date_Work;
            GV_Employee_Layout();
        }
        private void Btn_SaveAll_Click(object sender, EventArgs e)
        {
            if (GV_Employee.Rows.Count <= 0)
            {
                Utils.TNMessageBoxOK("Không tìm thấy dữ liệu!", 1);
                return;
            }
            PicLoading.Visible = true;
            zError = 0;
            zSuccess = 0;
            _IndexGV = 0;
            _TotalGV = GV_Employee.Rows.Count;
            txt_Sql.Text = "0";
            timer1.Start();

        }

        private void Btn_Open_Click(object sender, EventArgs e)
        {
            OpenFileDialog zOpf = new OpenFileDialog
            {
                InitialDirectory = @"C:\",
                DefaultExt = "xlsx",
                Title = "Chọn tập tin Excel",
                Filter = "All Files|*.*",
                FilterIndex = 2,
                ReadOnlyChecked = true,
                CheckFileExists = true,
                CheckPathExists = true,
                RestoreDirectory = true,
                ShowReadOnly = true
            };

            if (zOpf.ShowDialog() == DialogResult.OK)
            {
                GV_Employee.Rows.Clear();

                FileInfo = new FileInfo(zOpf.FileName);
                bool zcheck = Data_Access.CheckFileStatus(FileInfo);
                if (zcheck == true)
                {
                    Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file để tải lên!",2);
                }
                else
                {
                    try
                    {
                        lblFileName.Text = FileInfo.Name;
                        //Cursor.Current = Cursors.WaitCursor;
                        ExcelPackage package = new ExcelPackage(FileInfo);

                        _GVEmployee = new DataTable();
                        _GVEmployee = Data_Access.GetTable(FileInfo.FullName, package.Workbook.Worksheets[1].Name);
                        GV_Employee_LoadData();
                        _IsSaved = false;
                        //  Cursor.Current = Cursors.Default;

                    }
                    catch (Exception ex)
                    {
                        Utils.TNMessageBoxOK(ex.ToString(),4);
                    }
                }
            }
        }


        #region [GV_Employee]
        private void GV_Employee_Layout()
        {
            // Setup Column 
            GV_Employee.Columns.Add("No", "STT");
            GV_Employee.Columns.Add("EmployeeID", "Mã NV");
            GV_Employee.Columns.Add("EmployeeName", "Tên Nhân Viên");
            GV_Employee.Columns.Add("Department", "Mã Bộ phận");
            GV_Employee.Columns.Add("Date", "Ngày nhập");
            GV_Employee.Columns.Add("ID", "Mã phiếu");
            GV_Employee.Columns.Add("CategoryID", "Mã phí");
            GV_Employee.Columns.Add("CategoryName", "Tên phí");
            GV_Employee.Columns.Add("Description", "Nội dung chi");
            GV_Employee.Columns.Add("Amount", "Số tiền");
            GV_Employee.Columns.Add("Slug", "Tính lương");
            GV_Employee.Columns.Add("Note", "Ghi chú");
            GV_Employee.Columns.Add("Message", "Thông báo");

            GV_Employee.Columns["No"].Width = 40;
            GV_Employee.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV_Employee.Columns["No"].ReadOnly = true;
            GV_Employee.Columns["No"].Frozen = true;

            GV_Employee.Columns["EmployeeName"].Width = 180;
            GV_Employee.Columns["EmployeeName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV_Employee.Columns["EmployeeName"].ReadOnly = true;
            GV_Employee.Columns["EmployeeName"].Frozen = true;

            GV_Employee.Columns["EmployeeID"].Width = 100;
            GV_Employee.Columns["EmployeeID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Employee.Columns["EmployeeID"].Frozen = true;

            GV_Employee.Columns["Date"].Width = 100;
            GV_Employee.Columns["Date"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV_Employee.Columns["ID"].Width = 100;
            GV_Employee.Columns["ID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV_Employee.Columns["CategoryID"].Width = 100;
            GV_Employee.Columns["CategoryID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV_Employee.Columns["CategoryName"].Width = 250;
            GV_Employee.Columns["CategoryName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV_Employee.Columns["Description"].Width = 250;
            GV_Employee.Columns["Description"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV_Employee.Columns["Amount"].Width = 120;
            GV_Employee.Columns["Amount"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee.Columns["Note"].Width = 150;
            GV_Employee.Columns["Note"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV_Employee.Columns["Message"].Width = 150;
            GV_Employee.Columns["Message"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;


            GV_Employee.ColumnHeadersHeight = 45;
            GV_Employee.Rows.Clear();

        }
        private void GV_Employee_LoadData()
        {
            PicLoading.Visible = true;
            GV_Employee.Rows.Clear();
            _IndexGV = 0;
            _TotalGV = _GVEmployee.Rows.Count;
            timer3.Start();

        }

        private void GV_Employee_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            //DataGridViewRow zRowEdit = GVData.Rows[e.RowIndex];
            //if (GVData.Rows[e.RowIndex].Tag != null && e.ColumnIndex == 4)
            //{
            //    zRowEdit.Cells["Message"].Tag = 1;
            //    if (zRowEdit.Cells["Value"].Value == null)
            //    {
            //        zRowEdit.Cells["Value"].Value = 0;
            //    }

            //}
            //if (GVData.Rows[e.RowIndex].Tag != null && e.ColumnIndex == 5)
            //{
            //    zRowEdit.Cells["Message"].Tag = 1;
            //    if (zRowEdit.Cells["Description"].Value == null)
            //    {
            //        zRowEdit.Cells["Description"].Value = 0;
            //    }

            //}
        }


        #endregion

        private string CheckRow(DataGridViewRow zRow)
        {
            string Message = "";
            if (zRow.Cells["EmployeeName"].Value == null || zRow.Cells["EmployeeName"].Value.ToString() == "")
            {
                Message += "Tên nhân viên không bỏ trống /";
            }
            if (zRow.Cells["EmployeeID"].Value == null || zRow.Cells["EmployeeID"].Value.ToString() == "")
            {
                Message += "Mã nhân viên không bỏ trống /";
            }
            else
            {

                Employee_Info zinfo = new Employee_Info();
                zinfo.GetEmployeeID(zRow.Cells["EmployeeID"].Value.ToString());
                if (zinfo.Key == "")
                {
                    Message += "Mã nhân viên không tồn tại /";
                }
            }
            if (zRow.Cells["ID"].Value == null || zRow.Cells["ID"].Value.ToString() == "")
            {
                Message += "Mã Phiếu không bỏ trống /";
            }
            else
            {
                int zCount = Receipt_Data.CheckReceiptID(zRow.Cells["ID"].Value.ToString().Trim());
                if (zCount > 0)
                {
                    Message += "Mã phiếu đã được sử dụng /";
                }
            }
            if (zRow.Cells["CategoryID"].Value == null || zRow.Cells["CategoryID"].Value.ToString() == "")
            {
                Message += "Mã phí không bỏ trống /";
            }
            else
            {
                int zCount = Fee_Category_Data.CountReceiptCategoryID(zRow.Cells["CategoryID"].Value.ToString().Trim());
                if (zCount == 0)
                {
                    Message += "Mã phí không tồn tại  /";
                }
            }

            //if (zRow.Cells["Department"].Value == null || zRow.Cells["Department"].Value.ToString() == "")
            //{
            //    Message += "Phòng ban không hợp lệ /";
            //}
            //else
            //{
            //    Department_Info zinfo = new Department_Info();
            //    zinfo.Get_Department_ID((zRow.Cells["Department"].Value.ToString().Trim()));
            //    if (zinfo.Key == 0)
            //    {
            //        Message += "Phòng ban không hợp lệ /";
            //    }
            //}

            if (zRow.Cells["Date"].Value == null || zRow.Cells["Date"].Value.ToString() == "")
            {
                Message += "Ngày nhập không hợp lệ /";
            }
            else if (zRow.Cells["Date"].Value.ToString() != "")
            {
                DateTime zFromDate = DateTime.MinValue;
                if (!DateTime.TryParse(zRow.Cells["Date"].Value.ToString(), out zFromDate))
                {
                    Message += "Ngày nhập không hợp lệ /";
                }
                else
                {
                    if (SessionUser.Date_Lock >= zFromDate)
                    {
                        Message += "Thời gian này đã khóa sổ.! /";
                    }
                }
            }
            if (zRow.Cells["Description"].Value == null)
            {
                zRow.Cells["Description"].Value = "";
            }
            if (zRow.Cells["Slug"].Value == null)
            {
                zRow.Cells["Slug"].Value = "";
            }
            else
            {
                if (zRow.Cells["Slug"].Value.ToString().Trim() != "CO")
                {
                    Message += "Tính lương không đúng định dạng/";
                }
            }
            if (zRow.Cells["Amount"].Value == null || zRow.Cells["Amount"].Value.ToString() == "")
            {
                zRow.Cells["Amount"].Value = 0;
            }
            else
            {
                float zAmount = 0;
                //if (!float.TryParse(zRow.Cells["Amount"].Value.ToString().Trim().Replace(",", ""), out zAmount))
                if (!float.TryParse(zRow.Cells["Amount"].Value.ToString().Trim(), out zAmount))
                {
                    Message += "Số tiền chưa đúng định dạng/";
                }
            }
            if (zRow.Cells["Note"].Value == null)
            {
                zRow.Cells["Note"].Value = "";
            }
            if (Message != "")
            {
                zRow.Cells["Message"].Value = Message;
                zRow.DefaultCellStyle.BackColor = Color.Tomato;
                zRow.Tag = null;
            }
            else
            {
                zRow.Cells["Message"].Value = "";
                zRow.DefaultCellStyle.BackColor = Color.White;
                zRow.Tag = 0;
            }
            return Message;
        }

        private void Save()
        {
            zError = 0;
            zSuccess = 0;
            _IndexGV = 0;
            txt_Sql.Text = zSuccess.ToString();
            txt_Error.Text = zError.ToString();

            _TotalGV = GV_Employee.Rows.Count;
            timer2.Start();
        }

        private string SaveRow(DataGridViewRow zRow)
        {
            string zMessage = "";
            string zCategoryID = zRow.Cells["CategoryID"].Value.ToString().Trim();

            Receipt_Info zReceipt = new Receipt_Info();
            zReceipt.ReceiptID = zRow.Cells["ID"].Value.ToString().Trim();
            zReceipt.ReceiptDate = DateTime.Parse(zRow.Cells["Date"].Value.ToString().Trim());
            Fee_Category_Info zCate = new Fee_Category_Info();
            zCate.GetID(zRow.Cells["CategoryID"].Value.ToString().Trim());
            zReceipt.CategoryKey = zCate.Key;
            Employee_Info zEmp = new Employee_Info();
            zEmp.GetEmployeeID(zRow.Cells["EmployeeID"].Value.ToString().Trim());
            zReceipt.ParentKey = zEmp.Key;
            zReceipt.ParentTable = "HRM_Employee";
            zReceipt.AmountOrderMain = double.Parse(zRow.Cells["Amount"].Value.ToString().Trim());
            zReceipt.ReceiptDescription = zRow.Cells["Description"].Value.ToString().Trim();
            if (zRow.Cells["Slug"].Value.ToString().Trim() == "CO")
            {
                zReceipt.Slug = 1;
            }
            else
            {
                zReceipt.Slug = 0;
            }
            zReceipt.Note = zRow.Cells["Note"].Value.ToString().Trim();
            zReceipt.CreatedBy = SessionUser.UserLogin.Key;
            zReceipt.CreatedName = SessionUser.UserLogin.EmployeeName;
            zReceipt.ModifiedBy = SessionUser.UserLogin.Key;
            zReceipt.ModifiedName = SessionUser.UserLogin.EmployeeName;
            zReceipt.Save();
            zMessage = TN_Message.Show(zReceipt.Message);
            if (zMessage != "")
            {
                zRow.Cells["Message"].Value = zMessage;
                zRow.DefaultCellStyle.BackColor = Color.Tomato;
                zRow.Tag = null;
            }
            else
            {
                zRow.Tag = 1;
                zRow.Cells["Message"].Value = "Thành công";
            }
            return zMessage;
        }



        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            if (_IsSaved == true)
            {
                this.Close();
            }
            else
            {
                if (Utils.TNMessageBox("Dữ liệu bạn chưa được cập nhật !.", 2) == "Y")
                {
                    this.Close();
                }
            }
        }
        #endregion

        private void Timer1_Tick(object sender, EventArgs e)
        {
            // check befor save
            timer1.Stop();
            if (_IndexGV < _TotalGV)
            {
                GV_Employee.Rows[_IndexGV].DefaultCellStyle.BackColor = Color.White;

                if (GV_Employee.Rows[_IndexGV].Visible == true && GV_Employee.Rows[_IndexGV].Tag == null)
                {
                    string zMesage = CheckRow(GV_Employee.Rows[_IndexGV]);
                    if (zMesage != "")
                    {
                        zError++;
                        txt_Error.Text = zError.ToString();
                    }
                    else
                    {
                        zSuccess++;
                        //txt_Sql.Text = zSuccess.ToString();
                    }
                }
                _IndexGV++;
                timer1.Start();
            }
            else
            {
                timer1.Stop();
                if (zError > 0)
                {
                    timer2.Stop();
                    PicLoading.Visible = false;
                    Utils.TNMessageBoxOK("Còn " + zError + " dòng không hợp lệ vui lòng kiểm tra lại!", 2);
                }
                else

                {
                    _IndexGV = 0;
                    zError = 0;
                    zSuccess = 0;
                    lbl_Sql.Text = "Số lượng đã lưu";
                    txt_Sql.Text = "0";
                    timer2.Start();
                }
            }
        }

        private void Timer2_Tick(object sender, EventArgs e)
        {
            //Save

            timer2.Stop();
            if (_IndexGV < _TotalGV)
            {
                GV_Employee.Rows[_IndexGV].DefaultCellStyle.BackColor = Color.White;
                if (GV_Employee.Rows[_IndexGV].Cells["EmployeeID"].Value != null && GV_Employee.Rows[_IndexGV].Visible == true && GV_Employee.Rows[_IndexGV].Tag.ToString() == "0")
                {
                    string zMessage = SaveRow(GV_Employee.Rows[_IndexGV]);
                    if (zMessage != "")
                    {
                        zError++;
                    }
                    else
                    {
                        zSuccess++;
                    }

                }
                txt_Sql.Text = zSuccess.ToString();
                txt_Error.Text = zError.ToString();
                _IndexGV++;
                timer2.Start();
            }
            else
            {
                timer2.Stop();
                PicLoading.Visible = false;
                _IsSaved = true;
                if (zError > 0)
                {
                    string Status = "Import Excel form " + HeaderControl.Text + " > lỗi > SL Excel:" + txt_Amount_Excel.Text + " > SL Lỗi:" + txt_Error.Text + " > SL SQL:" + txt_Sql.Text;
                     Application_Data.Save_LogApplication(SessionUser.UserLogin.Key,SessionUser.UserLogin.EmployeeKey, Status, 3);

                    Utils.TNMessageBoxOK("Còn " + zError + " dòng chưa lưu !", 2);
                }
                else
                {
                    string Status = "Import Excel form " + HeaderControl.Text + " > thành công > SL Excel:" + txt_Amount_Excel.Text + " > SL Lỗi:" + txt_Error.Text + " > SL SQL:" + txt_Sql.Text;
                     Application_Data.Save_LogApplication(SessionUser.UserLogin.Key,SessionUser.UserLogin.EmployeeKey, Status, 3);

                    Utils.TNMessageBoxOK("Đã lưu thành công!", 3);
                }
            }
        }

        private void Timer3_Tick(object sender, EventArgs e)
        {
            timer3.Stop();
            if (_IndexGV < _TotalGV)
            {
                int i = _IndexGV;
                DataRow zRow = _GVEmployee.Rows[i];
                GV_Employee.Rows.Add();
                GV_Employee.Rows[i].Cells["No"].Value = (i + 1).ToString();
                GV_Employee.Rows[i].Cells["EmployeeID"].Value = zRow[1].ToString().Trim();
                GV_Employee.Rows[i].Cells["EmployeeName"].Value = zRow[2].ToString().Trim();
                GV_Employee.Rows[i].Cells["Department"].Value = zRow[3].ToString().Trim();
                GV_Employee.Rows[i].Cells["Date"].Value = zRow[4].ToString().Trim();
                GV_Employee.Rows[i].Cells["ID"].Value = zRow[5].ToString().Trim();
                GV_Employee.Rows[i].Cells["CategoryID"].Value = zRow[6].ToString().Trim();
                GV_Employee.Rows[i].Cells["CategoryName"].Value = zRow[7].ToString().Trim();
                GV_Employee.Rows[i].Cells["Description"].Value = zRow[8].ToString().Trim();
                GV_Employee.Rows[i].Cells["Amount"].Value = zRow[9].ToString().Trim();
                GV_Employee.Rows[i].Cells["Slug"].Value = zRow[10].ToString().Trim();
                GV_Employee.Rows[i].Cells["Note"].Value = zRow[11].ToString().Trim();
                txt_Amount_Excel.Text = (i + 1).ToString();
                _IndexGV++;
                timer3.Start();
            }
            else
            {
                PicLoading.Visible = false;
                timer3.Stop();
            }
        }

    }
}
