﻿using C1.Win.C1FlexGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TN.SLR;
using TNS.CORE;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_TinhThuongKD : Form
    {
        private int _BranchKey = 0;
        private int _DepartmentKey = 0;
        private int _TeamKey = 0;
        private double _Persent = 1;
        public Frm_TinhThuongKD()
        {
            InitializeComponent();
            btnClose.Click += btnClose_Click;
            btnMax.Click += btnMax_Click;
            btnMini.Click += btnMini_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            btn_Search.Click += Btn_Search_Click;
            btn_Export.Click += Btn_Export_Click;
            btn_Done.Click += Btn_Done_Click;
            btn_View.Click += Btn_View_Click;

            cbo_Branch.SelectedIndexChanged += Cbo_Branch_SelectedIndexChanged;
            cbo_Department.SelectedIndexChanged += Cbo_Department_SelectedIndexChanged;

            DateTime ViewDate = SessionUser.Date_Work;
            DateTime FromDate = new DateTime(ViewDate.Year, 1, 1, 0, 0, 0);
            DateTime ToDate = new DateTime(ViewDate.Year, ViewDate.Month, ViewDate.Day, 23, 59, 59);
            dte_FromDate.Value = FromDate;
            dte_ToDate.Value = ToDate;
            dte_DateOff.Value = ToDate;
        }

        

        private void Frm_TinhThuongKD_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            LoadDataToToolbox.KryptonComboBox(cbo_Department, "SELECT DepartmentKey, DepartmentName FROM[dbo].[SYS_Department] WHERE DepartmentKey != 98  AND RecordStatus< 99", "---- Chọn----");
            LoadDataToToolbox.KryptonComboBox(cbo_Branch, "SELECT BranchKey,BranchName FROM SYS_Branch WHERE BranchKey != 98  AND RecordStatus < 99", "---- Tất cả----");
            LoadDataToToolbox.KryptonComboBox(cbo_Team, "SELECT TeamKey,TeamName FROM SYS_Team WHERE TeamKey != 98  AND RecordStatus < 99", "---- Chọn ----");
            Check_RoleForm();
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
        }
        private void Cbo_Branch_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDataToToolbox.KryptonComboBox(cbo_Department, "SELECT DepartmentKey, DepartmentName FROM[dbo].[SYS_Department] WHERE BranchKey = " + cbo_Branch.SelectedValue.ToInt() + "  AND RecordStatus< 99", "---- Chọn tất cả----");

        }
        private void Cbo_Department_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDataToToolbox.KryptonComboBox(cbo_Team, "SELECT TeamKey,TeamName FROM SYS_Team WHERE TeamKey != 98 AND DepartmentKey = " + cbo_Department.SelectedValue.ToInt() + "  AND RecordStatus < 99", "---- Chọn tất cả ----");
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            _BranchKey = cbo_Branch.SelectedValue.ToInt();
            _DepartmentKey = cbo_Department.SelectedValue.ToInt();
            _TeamKey = cbo_Team.SelectedValue.ToInt();
            if(double.TryParse(txt_Persent.Text.Trim(),out _Persent))
            {
                if(_Persent>0)
                _Persent /= 100;
            }
            string Status = "Tìm DL form " + HeaderControl.Text + " > từ: " + dte_FromDate.Value.ToString("dd/MM/yyyy") + " > đến:" + dte_ToDate.Value.ToString("dd/MM/yyyy"); ;
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);
            
            if ( dte_ToDate.Value.Year != dte_FromDate.Value.Year)
            {
                Utils.TNMessageBoxOK( "Vui lòng chỉ chọn trong 1 năm !", 1);
                return;
            }
            else
            {
                try
                {
                    using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }
                }
                catch (Exception ex)
                {
                    Utils.TNMessageBoxOK( ex.ToString(), 4);
                }
            }
        }
        private void DisplayData()
        {
            DataTable _Intable;
            if (rdo_Default.Checked)
            {
                _Intable = Salary_Business_Data.TinhThuongKinhDoanh_NhomMacDinh(dte_FromDate.Value, dte_ToDate.Value, _BranchKey, _DepartmentKey, _TeamKey, _Persent, dte_DateOff.Value);
            }
            else
            {
                _Intable = Salary_Business_Data.TinhThuongKinhDoanh_NhomPhanBo(dte_FromDate.Value, dte_ToDate.Value, _BranchKey, _DepartmentKey, _TeamKey, _Persent, dte_DateOff.Value);
            }
            if (_Intable.Rows.Count > 0)
            {
                this.Invoke(new MethodInvoker(delegate ()
                {
                    InitGV_Layout(_Intable);

                    int NoGroup = 2;
                    int RowTam = NoGroup;
                    Row zGvRow;
                    int TeamKey = _Intable.Rows[0]["TeamKey"].ToInt();
                    string TeamName = _Intable.Rows[0]["TeamName"].ToString();
                    DataRow[] Array = _Intable.Select("TeamKey=" + TeamKey);
                    if (Array.Length > 0)
                    {
                        DataTable zGroup = Array.CopyToDataTable();
                        GVData.Rows.Add();
                        zGvRow = GVData.Rows[1];
                        HeaderRow(zGvRow, zGroup, TeamKey, TeamName, 1);
                    }
                    for (int i = 0; i < _Intable.Rows.Count; i++)
                    {
                        DataRow r = _Intable.Rows[i];
                        if (TeamKey != r["TeamKey"].ToInt())
                        {
                            #region [GROUP]
                            TeamKey = r["TeamKey"].ToInt();
                            TeamName = r["TeamName"].ToString();
                            Array = _Intable.Select("TeamKey=" + TeamKey);
                            if (Array.Length > 0)
                            {
                                DataTable zGroup = Array.CopyToDataTable();
                                GVData.Rows.Add();
                                zGvRow = GVData.Rows[i + NoGroup];
                                HeaderRow(zGvRow, zGroup, TeamKey, TeamName, i + NoGroup);
                            }
                            #endregion
                            NoGroup++;
                        }
                        GVData.Rows.Add();
                        zGvRow = GVData.Rows[i + NoGroup];
                        DetailRow(zGvRow, r, i + NoGroup - 1);
                        RowTam = i + NoGroup;
                    }
                    GVData.Rows.Add();
                    zGvRow = GVData.Rows[RowTam + 1];
                    TotalRow(zGvRow, _Intable, RowTam + 1);
                }));
            }
        }
        private void HeaderRow(Row RowView, DataTable Table, int TeamKey, string TeamName, int No)
        {
            RowView[0] = (No - 1).ToString();
            RowView[1] = TeamName;
            RowView[2] = "Số nhân sự " + Table.Select("TeamKey='" + TeamKey + "'").Length;
            int nCol = 3;
            float zTotal = 0;
            for (int i = 6; i < Table.Columns.Count; i++)
            {
                RowView[nCol] = Table.Compute("SUM([" + Table.Columns[i].ColumnName + "])", "TeamKey='" + TeamKey + "'").Toe0String();
                float zTemp = 0;
                if (float.TryParse(RowView[nCol].ToString(), out zTemp))
                {

                }
                zTotal += zTemp;
                nCol++;
            }
            RowView.StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
        }
        private void DetailRow(Row RowView, DataRow rDetail, int No)
        {
            RowView[0] = (No).ToString();
            RowView[1] = rDetail["EmployeeName"].ToString().Trim();
            RowView[2] = rDetail["EmployeeID"].ToString().Trim();
            int nCol = 3;
            for (int i = 6; i < rDetail.ItemArray.Length; i++)
            {
                RowView[nCol] = rDetail[i].Toe0String();
                nCol++;
            }
            double zTotalRow = 0;
            for (int i = 6; i < rDetail.ItemArray.Length; i++)
            {
                float zTemp = 0;
                if (float.TryParse(rDetail[i].ToString(), out zTemp))
                {

                }
                zTotalRow += zTemp;
            }
            //RowView[nCol] = zTotalRow.Toe0String();
            //RowView[nCol + 1] = (zTotalRow / (rDetail.ItemArray.Length - 6)).Toe0String();
            //RowView[nCol + 2] = (zTotalRow / (rDetail.ItemArray.Length - 6) *_Persent).Toe0String();
        }
        private void TotalRow(Row RowView, DataTable Table, int No)
        {

            RowView[0] = (No - 1).ToString();
            RowView[1] = "Tổng ";
            RowView[2] = "Số nhân sự " + Table.Rows.Count;

            float zTotal = 0;
            int nCol = 3;
            for (int i = 6; i < Table.Columns.Count; i++)
            {
                RowView[nCol] = Table.Compute("SUM([" + Table.Columns[i].ColumnName + "])", "").Toe0String();
                float zTemp = 0;
                if (float.TryParse(RowView[nCol].ToString(), out zTemp))
                {

                }
                zTotal += zTemp;
                nCol++;
            }
            //RowView[nCol] = zTotal.Toe0String();
            //RowView[nCol + 1] = (zTotal / (Table.Columns.Count - 6)).Toe0String();
            //RowView[nCol + 2] = (zTotal / (Table.Columns.Count - 6)*_Persent).Toe0String();
            RowView.StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
        }

        void InitGV_Layout(DataTable Table)
        {

            GVData.Clear();

            //int TotalRow = 1 ;
            int ToTalCol = Table.Columns.Count-3 ;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(1);
            //Row Header          

            GVData.Rows[0][0] = "STT";
            GVData.Rows[0][1] = "HỌ VÀ TÊN";
            GVData.Rows[0][2] = "SỐ THẺ";
            int nCol = 6;
            for (int i = 3; i < ToTalCol; i++)
            {
                DataColumn Col = Table.Columns[nCol];
                string strCaption = "";
                if (Col.ColumnName.ToString().Length <= 2)
                {
                    strCaption = "Tháng " + Col.ColumnName.ToString();
                }
                else
                {
                    strCaption = Col.ColumnName.ToString();
                }
                GVData.Rows[0][i] = strCaption.ToUpper();
                GVData.Cols[i].Width = 110;
                nCol++;
            }

            GVData.Rows.Fixed = 1;
            GVData.Cols.Frozen = 3;
            GVData.Cols[0].StyleNew.BackColor = Color.Empty;
            GVData.Cols[1].StyleNew.BackColor = Color.Empty;
            GVData.Cols[2].StyleNew.BackColor = Color.Empty;



            //Style
            //GVData.AutoSizeCols();
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.Custom;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.LeftCenter;
            GVData.Rows[0].Height = 60;
            GVData.Rows[0].StyleNew.WordWrap = true;
            GVData.Rows[0].TextAlign = TextAlignEnum.CenterCenter;

            ////
            GVData.Cols[0].Width = 40;
            GVData.Cols[1].Width = 200;
            GVData.Cols[2].Width = 120;

            for (int i = 3; i < ToTalCol; i++)
            {
                GVData.Cols[i].TextAlign = TextAlignEnum.RightCenter;
            }
        }
        private void Btn_Done_Click(object sender, EventArgs e)
        {
            if (dte_ToDate.Value.Year != dte_FromDate.Value.Year)
            {
                Utils.TNMessageBox( "Vui lòng chỉ chọn trong 1 năm !", 2);
                return;
            }
            if (double.TryParse(txt_Persent.Text.Trim(), out _Persent))
            {
                if (_Persent > 0)
                    _Persent /= 100;
            }
            if (SessionUser.Date_Lock >= dte_FromDate.Value)
            {
                Utils.TNMessageBoxOK("Thời gian này đã khóa sổ.!", 2);
                return;
            }
            if (Utils.TNMessageBox("Bạn có chắc chốt thanh toán đợt này ?.", 2) == "Y")
            {
                if (txt_LanUng.Text.Trim() == "0")
                {
                    Utils.TNMessageBoxOK("Vui lòng nhập đợt chi thường lớn hơn 0",1);
                    return;
                }
                if (txt_Persent.Text.Trim() == "0")
                {
                    Utils.TNMessageBoxOK("Vui lòng nhập mức chi lớn hơn 0",1);
                    return;
                }
                else
                {
                    string Mesage = Salary_Business_Data.LuuThuongKinhDoanh(dte_FromDate.Value, dte_ToDate.Value, _Persent, txt_LanUng.Text.ToInt(),dte_DateOff.Value);
                    if (Mesage == string.Empty)
                    {
                        Utils.TNMessageBoxOK("Lưu thông tin thưởng kinh doanh đợt " + txt_LanUng.Text.ToInt() + " thành công.",3);
                    }
                    else
                    {
                        Utils.TNMessageBoxOK("Lỗi.Vui lòng liên hệ IT "+Mesage,4);
                    }
                }
            }
        }

        private void Btn_Export_Click(object sender, EventArgs e)
        {
            string zTeamName = "";
            if (cbo_Team.SelectedValue.ToInt() != 0)
            {
                string[] s = cbo_Team.Text.Trim().Split('-');
                zTeamName = "_Nhóm_" + s[0];
            }
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Thanh toán thưởng kinh doanh" + zTeamName + "_Từ_" + dte_FromDate.Value.ToString("dd_MM_yyyy") + "_Đến_" + dte_ToDate.Value.ToString("dd_MM_yyyy") + ".xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }

                    string Status = "Xuất Excel form " + HeaderControl.Text + " > đường dẫn và tên file xuất:" + FDialog.FileName;
                    Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);

                    GVData.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }
        }
        private void Btn_View_Click(object sender, EventArgs e)
        {
            Frm_BaoCaoThuongKinhDoanh frm = new Frm_BaoCaoThuongKinhDoanh();
            frm.Show();
        }
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Normal)
            {
                this.WindowState = FormWindowState.Maximized;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
                this.StartPosition = FormStartPosition.CenterScreen;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
                btn_Done.Enabled = false;
                btn_View.Enabled = false;
            }
            if (Role.RoleAdd == 0 && Role.RoleEdit == 0)
            {
                btn_Done.Enabled = false;
            }
        }
        #endregion
    }
}
