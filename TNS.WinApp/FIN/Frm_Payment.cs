﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.HRM;
using TNS.FIN;
using TNS.Misc;
using TNS.SYS;
using TNS.LOG;
using C1.Win.C1FlexGrid;
using System.IO;
using System.Diagnostics;

namespace TNS.WinApp
{
    public partial class Frm_Payment : Form
    {
        #region[Private]
        private string _Key = "";
        #endregion
        public Frm_Payment()
        {
            InitializeComponent();
            btn_Save.Click += Btn_Save_Click;
            btn_New.Click += Btn_New_Click;
            btn_Del.Click += Btn_Del_Click;
            btn_Search.Click += Btn_Search_Click;
            txt_Amount.KeyPress += txt_Amount_KeyPress;
            txt_Amount.KeyUp += Txt_Amount_KeyUp;
            GVData.Click += GVData_Click;
            btn_Import.Click += Btn_Import_Click;
            txt_PaymentID.Leave += Txt_PaymentID_Leave;
            txt_PaymentID.Enter += Txt_PaymentID_Enter;
            txt_EmployeeID.Enter += Txt_EmployeeID_Enter;
            txt_EmployeeID.Leave += Txt_EmployeeID_Leave;
            btn_Export.Click += Btn_Export_Click;

            btnClose.Click += btnClose_Click;
            btnMax.Click += btnMax_Click;
            btnMini.Click += btnMini_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;
            cbo_Branch.SelectedIndexChanged += Cbo_Branch_SelectedIndexChanged;
            cbo_Department.SelectedIndexChanged += Cbo_Department_SelectedIndexChanged;
            this.Bounds = Screen.PrimaryScreen.WorkingArea;

        }

        private void Txt_Amount_KeyUp(object sender, KeyEventArgs e)
        {
            double zAmount = 0;
            double.TryParse(txt_Amount.Text.Trim(), out zAmount);
            txt_Amount.Text = zAmount.Ton0String();
            txt_Amount.Focus();
            txt_Amount.SelectionStart = txt_Amount.Text.Length;
        }
        private void txt_Amount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= '0' && e.KeyChar <= '9') || (Keys)e.KeyChar == Keys.Back || e.KeyChar == 46 || e.KeyChar == 44)
            {

                e.Handled = false;

            }
            else
            {
                e.Handled = true;
            }
            
        }
        #region[event]
        private void Txt_PaymentID_Enter(object sender, EventArgs e)
        {
            if (txt_PaymentID.Text.Trim() == "Nhập 6 kí tự đầu") 
            {
                txt_PaymentID.Text = "";
                txt_PaymentID.ForeColor = Color.Black;
            }
        }
        private void Txt_PaymentID_Leave(object sender, EventArgs e)
        {
            if (_Key == "")
            {
                if (txt_PaymentID.Text.Trim().Length != 6 && txt_PaymentID.Text.Trim().Length != 11)
                {
                    txt_PaymentID.Text = "Nhập 6 kí tự đầu";
                    txt_PaymentID.ForeColor = Color.White;
                }
                if (txt_PaymentID.Text.Trim().Length == 6)
                {
                    string zTrama = Payment_Data.TramaID(txt_PaymentID.Text.Trim());
                    txt_PaymentID.Text = zTrama.ToUpper();
                }
               
            }
        }
        private void Txt_EmployeeID_Enter(object sender, EventArgs e)
        {

            if (txt_EmployeeID.Text.Trim() == "Nhập mã thẻ")
            {
                txt_EmployeeID.Text = "";
                txt_EmployeeID.ForeColor = Color.Brown;
            }
        }
        private void Txt_EmployeeID_Leave(object sender, EventArgs e)
        {
            if (txt_EmployeeID.Text.Trim().Length < 1)
            {
                txt_EmployeeID.Text = "Nhập mã thẻ";
                txt_EmployeeID.ForeColor = Color.White;
            }
            else
            {
                Employee_Info zinfo = new Employee_Info();
                zinfo.GetEmployee(txt_EmployeeID.Text.Trim());
                if (zinfo.Key == "")
                {
                    txt_EmployeeID.Text = "Nhập mã thẻ";
                    txt_EmployeeID.ForeColor = Color.White;
                    txt_EmployeeID.Tag = null;
                }
                else
                {
                    txt_EmployeeID.Text = zinfo.EmployeeID;
                    txt_EmployeeName.Text = zinfo.FullName;
                    txt_EmployeeID.Tag = zinfo.Key;
                    txt_EmployeeID.BackColor = Color.Black;
                }
            }
        }
        #endregion
        private void Btn_Import_Click(object sender, EventArgs e)
        {
            Frm_ImportPayment frm = new Frm_ImportPayment();
            frm.Show();
        }

        private void Frm_Payment_Detail_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            Check_RoleForm();

            DateTime ViewDate = SessionUser.Date_Work;
            DateTime FromDate = new DateTime(ViewDate.Year, ViewDate.Month, 1, 0, 0, 0);
            DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            dte_FromDate.Value = FromDate;
            dte_ToDate.Value = ToDate;

            dte_PaymentDate.Value = SessionUser.Date_Work;
            LoadDataToToolbox.KryptonComboBox(cbo_Branch, "SELECT BranchKey,BranchName FROM SYS_Branch WHERE BranchKey != 98  AND RecordStatus < 99", "---- Tất cả----");
            LoadDataToToolbox.KryptonComboBox(cbo_Category, "SELECT CategoryKey, (CategoryID+'-'+ CategoryName) AS Category FROM FNC_Fee_Category WHERE Slug = 2 AND RecordStatus <> 99 ORDER BY [Rank] ASC", "--Chọn--");
            LoadDataToToolbox.KryptonComboBox(cbo_CategorySearch, "SELECT CategoryKey,  (CategoryID+'-'+ CategoryName) AS Category FROM FNC_Fee_Category WHERE Slug = 2 AND RecordStatus <> 99 ORDER BY [Rank] ASC", "--Tất cả--");
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
        }
        private void Cbo_Branch_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDataToToolbox.KryptonComboBox(cbo_Department, "SELECT DepartmentKey, DepartmentName FROM[dbo].[SYS_Department] WHERE BranchKey = " + cbo_Branch.SelectedValue.ToInt() + "  AND RecordStatus< 99", "---- Chọn tất cả----");

        }
        private void Cbo_Department_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDataToToolbox.KryptonComboBox(cbo_Team, "SELECT TeamKey,TeamName FROM SYS_Team WHERE TeamKey != 98 AND DepartmentKey = " + cbo_Department.SelectedValue.ToInt() + "  AND RecordStatus < 99", "---- Chọn tất cả ----");
        }

        #region[Progess]
        private void LoadOrderInfo()
        {
            Payment_Info zPayment = new Payment_Info();
            zPayment.Payment_Employee(_Key);
            txt_EmployeeID.Tag = zPayment.ParentKey;
            if(zPayment.Key!="")
            {
                txt_PaymentID.ReadOnly = true;
                txt_EmployeeID.ReadOnly = true;
            }
            txt_PaymentID.Text = zPayment.PaymentID;
            if (zPayment.PaymentDate != DateTime.MinValue)
            {
                dte_PaymentDate.Value = zPayment.PaymentDate;
            }
            if (zPayment.Slug == 0)
            {
                ck_Salary.Checked = false;
            }
            else
            {
                ck_Salary.Checked = true;
            }
            cbo_Category.SelectedValue = zPayment.CategoryKey;
            Employee_Info Employee = new Employee_Info(zPayment.ParentKey);
            txt_EmployeeName.Text = Employee.FullName;
            txt_EmployeeID.Text = Employee.EmployeeID;
            txt_Note.Text = zPayment.Note;
            txt_Amount.Text = zPayment.AmountOrderMain.Ton0String();
            txt_PaymentDescription.Text = zPayment.PaymentDescription;
            lbl_Created.Text = "Tạo bởi:[" + zPayment.CreatedName + "][" + zPayment.CreatedOn + "]";
            lbl_Modified.Text = "Chỉnh sửa:[" + zPayment.ModifiedName + "][" + zPayment.ModifiedOn + "]";

        }
        private void Load_Refresh()
        {
            _Key = "";
            dte_PaymentDate.Value = SessionUser.Date_Work;
            txt_PaymentID.Text = "Nhập 6 kí tự đầu";
            txt_EmployeeID.Tag = null;
            txt_EmployeeID.Text = "Nhập mã thẻ";
            txt_EmployeeName.Text = "";
            txt_Amount.Text = "0";
            txt_Note.Text = "";
            cbo_Category.SelectedIndex = 0;
            txt_PaymentDescription.Text = "";
            txt_PaymentID.ReadOnly = false;
            txt_EmployeeID.ReadOnly = false;
            lbl_Created.Text = "Tạo bởi:";
            lbl_Modified.Text = "Chỉnh sửa:";
        }
       
        #endregion

        #region[Event]
        private void Btn_Save_Click(object sender, EventArgs e)
        {
            if (SessionUser.Date_Lock >= dte_PaymentDate.Value)
            {
                Utils.TNMessageBoxOK("Thời gian này đã khóa sổ.!", 2);
                return;
            }
            if (txt_EmployeeID.Tag == null || txt_EmployeeID.Text.Trim() == "")
            {
                Utils.TNMessageBoxOK("Chưa chọn nhân viên!",1);
            }
            else if (cbo_Category.SelectedValue.ToInt() == 0)
            {
                Utils.TNMessageBoxOK("Chưa chọn danh mục phí!",1);
            }
            else if(txt_PaymentID.Text.Trim().Length!=11)
            {
                Utils.TNMessageBoxOK("Mã phiếu không hợp lệ!",1);
            }
            else
            {
                Payment_Info zPayment = new Payment_Info(_Key);
                zPayment.PaymentID = txt_PaymentID.Text;
                zPayment.PaymentDate = dte_PaymentDate.Value;
                zPayment.CategoryKey = (int)cbo_Category.SelectedValue;
                zPayment.ParentKey = txt_EmployeeID.Tag.ToString();
                zPayment.ParentTable = "HRM_Employee";
                zPayment.AmountOrderMain = double.Parse(txt_Amount.Text);
                zPayment.PaymentDescription = txt_PaymentDescription.Text;
                if (ck_Salary.CheckState == CheckState.Checked)
                {
                    zPayment.Slug = 1;
                }
                else
                {
                    zPayment.Slug = 0;
                }
                zPayment.CreatedBy = SessionUser.UserLogin.Key;
                zPayment.CreatedName = SessionUser.UserLogin.EmployeeName;
                zPayment.ModifiedBy = SessionUser.UserLogin.Key;
                zPayment.ModifiedName = SessionUser.UserLogin.EmployeeName;
                zPayment.Save();
                string zMessage = TN_Message.Show(zPayment.Message);
                if (zMessage.Length == 0)
                {
                    Utils.TNMessageBoxOK("Câp nhật thành công", 3);
                    _Key = zPayment.Key;
                    LoadOrderInfo();
                    DisplayData();
                }
                else
                {
                    Utils.TNMessageBoxOK(zPayment.Message,4);
                }
            }


        }
        private void Btn_Del_Click(object sender, EventArgs e)
        {
            if (_Key != "")
            {
                Payment_Info zpayment = new Payment_Info(_Key);
                if (SessionUser.Date_Lock >= zpayment.PaymentDate)
                {
                    Utils.TNMessageBoxOK("Thời gian này đã khóa sổ.!", 2);
                    return;
                }
                if (Utils.TNMessageBox("Bạn có muốn xóa thông tin này?",2)=="Y")
                {
                    zpayment.ModifiedBy = SessionUser.UserLogin.Key;
                    zpayment.ModifiedName = SessionUser.UserLogin.EmployeeName;
                    zpayment.Delete();
                    string zMessage = TN_Message.Show(zpayment.Message);
                    if (zMessage.Length == 0)
                    {
                        Utils.TNMessageBoxOK("Xóa thành công",3);
                        Load_Refresh();
                        DisplayData();
                    }
                    else
                    {
                        Utils.TNMessageBoxOK(zpayment.Message, 4);
                    }

                }
            }
            else
            {
                Utils.TNMessageBoxOK("Vui lòng chọn phiếu !.",1);
            }

        }
        private void Btn_New_Click(object sender, EventArgs e)
        {
            Load_Refresh();
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            //if(cbo_CategorySearch.SelectedValue.ToInt()==0)
            //{
                //MessageBox.Show("Vui lòng chọn 1 danh mục phí");
               // return;
            //}
            DisplayData();
        }
        #endregion


        //private void GVData_KeyDown(object sender, KeyEventArgs e)
        //{
        //    string zMessage = "";
        //    if (e.KeyData == Keys.Delete)
        //    {
        //        DialogResult dlr = MessageBox.Show("Bạn có muốn xóa các phiếu đã chọn ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        //        if (dlr == DialogResult.Yes)
        //        {
        //            for (int i = 0; i < GVData.Rows.Count; i++)
        //            {
        //                if (GVData.Rows[i].Tag != null)
        //                {
        //                    if (GVData.Rows[i].Selected)
        //                    {
        //                        _Key = GVData.Rows[i].Tag.ToString();
        //                        Payment_Info zpayment = new Payment_Info(_Key);

        //                        zpayment.ModifiedBy = SessionUser.UserLogin.Key;
        //                        zpayment.ModifiedName = SessionUser.UserLogin.EmployeeName;
        //                        zpayment.Delete();
        //                        zMessage += TN_Message.Show(zpayment.Message);
        //                    }
        //                }
        //            }
        //            if (zMessage.Length == 0)
        //            {
        //                MessageBox.Show("Xóa Thành Công", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
        //                Load_Refresh();
        //                LoadData();
        //            }
        //            else
        //            {
        //                MessageBox.Show("Vui lòng kiểm tra lại", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //            }
        //        }

        //    }
        //}
        private void GVData_Click(object sender, EventArgs e)
        {
            if (GVData.Rows.Count >0 &&  GVData.Rows[GVData.RowSel][10].ToString() != "")
            {
                _Key = GVData.Rows[GVData.RowSel][10].ToString();
                LoadOrderInfo();
            }
        }
        private void DisplayData()
        {
            DataTable _Intable = Payment_Data.ListPayment(cbo_Branch.SelectedValue.ToInt(), cbo_Department.SelectedValue.ToInt(), cbo_Team.SelectedValue.ToInt(),cbo_CategorySearch.SelectedValue.ToInt() , txt_Search.Text.Trim(), dte_FromDate.Value,dte_ToDate.Value);
            InitGV_Layout(_Intable);
        }

        void InitGV_Layout(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            //int TotalRow = TableView.Rows.Count + 1;
            int ToTalCol = 11;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(1);

            //Row Header
            GVData.Rows[0][0] = "Stt";
            GVData.Rows[0][1] = "Ngày chi";
            GVData.Rows[0][2] = "Mã phiếu";
            GVData.Rows[0][3] = "Họ và tên";
            GVData.Rows[0][4] = "Số thẻ";
            GVData.Rows[0][5] = "Tổ nhóm";
            GVData.Rows[0][6] = "Danh mục chi";
            GVData.Rows[0][7] = "Nội dung chi";
            GVData.Rows[0][8] = "Số tiền";
            GVData.Rows[0][9] = "Ghi chú";

            GVData.Rows[0][10] = ""; // ẩn cột này

            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                GVData.Rows.Add();
                DataRow rData = TableView.Rows[rIndex];

                GVData.Rows[rIndex + 1][0] = rIndex + 1;
                GVData.Rows[rIndex + 1][1] = rData[1];
                GVData.Rows[rIndex + 1][2] = rData[2];
                GVData.Rows[rIndex + 1][3] = rData[3];
                GVData.Rows[rIndex + 1][4] = rData[4];
                GVData.Rows[rIndex + 1][5] = rData[5];
                GVData.Rows[rIndex + 1][6] = rData[6];
                GVData.Rows[rIndex + 1][7] = rData[7];
                GVData.Rows[rIndex + 1][8] = rData[8].Toe0String();
                GVData.Rows[rIndex + 1][9] = rData[9];
                GVData.Rows[rIndex + 1][10] = rData[0];
            }

            //Style         
            GVData.AllowFreezing = AllowFreezingEnum.Both;
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.FixedOnly;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.LeftCenter;
            GVData.Styles.Normal.WordWrap = true;

            //Freeze Row and Column                              
            GVData.Rows.Fixed = 1;
            GVData.Cols.Frozen = 6;

            GVData.Cols[0].StyleNew.BackColor = Color.Empty;
            GVData.Cols[1].StyleNew.BackColor = Color.Empty;
            GVData.Cols[2].StyleNew.BackColor = Color.Empty;
            GVData.Cols[3].StyleNew.BackColor = Color.Empty;
            GVData.Cols[4].StyleNew.BackColor = Color.Empty;
            GVData.Cols[5].StyleNew.BackColor = Color.Empty;

            GVData.Rows[0].TextAlign = TextAlignEnum.CenterCenter;

            GVData.AutoSizeCols();
            GVData.Rows[0].Height = 40;

            GVData.Cols[0].Width = 35;
            GVData.Cols[1].Width = 80;
            GVData.Cols[2].Width = 90;
            GVData.Cols[3].Width = 180;
            GVData.Cols[4].Width = 80;
            GVData.Cols[5].Width = 80;
            GVData.Cols[8].TextAlign = TextAlignEnum.RightCenter;
            GVData.Cols[10].Visible = false;
        }

        private void Btn_Export_Click(object sender, EventArgs e)
        {

            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Danh_Sach_Phieu_Chi.xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }

                    string Status = "Xuất Excel form " + HeaderControl.Text + " > đường dẫn và tên file xuất:" + FDialog.FileName;
                    Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);

                    GVData.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }

        }
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
               
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                //this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion
        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
                btn_Export.Enabled = false;
            }
            if (Role.RoleAdd == 0 && Role.RoleEdit == 0)
            {
                btn_New.Enabled = false;
                btn_Save.Enabled = false;
                btn_Import.Enabled = false;
            }
            if (Role.RoleAdd == 0 && Role.RoleEdit == 1)
            {
                btn_New.Enabled = false;
                btn_Save.Enabled = true;
            }
            if (Role.RoleDel == 0)
            {
                btn_Del.Enabled = false;
            }
        }
        #endregion

    }
}
