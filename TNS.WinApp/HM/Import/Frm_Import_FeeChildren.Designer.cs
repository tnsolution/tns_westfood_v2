﻿namespace TNS.WinApp
{
    partial class Frm_Import_FeeChildren
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Import_FeeChildren));
            this.HeaderControl = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btnMini = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnMax = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnClose = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.panel1 = new System.Windows.Forms.Panel();
            this.PicLoading = new System.Windows.Forms.PictureBox();
            this.txt_Sql = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txt_Error = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txt_Amount_Excel = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lbl_Sql = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblFileName = new System.Windows.Forms.Label();
            this.btn_Open = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.timer3 = new System.Windows.Forms.Timer(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.panel3 = new System.Windows.Forms.Panel();
            this.btn_SaveAll = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.GV_Employee = new System.Windows.Forms.DataGridView();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicLoading)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GV_Employee)).BeginInit();
            this.SuspendLayout();
            // 
            // HeaderControl
            // 
            this.HeaderControl.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnMini,
            this.btnMax,
            this.btnClose});
            this.HeaderControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.HeaderControl.Location = new System.Drawing.Point(0, 0);
            this.HeaderControl.Name = "HeaderControl";
            this.HeaderControl.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.HeaderControl.Size = new System.Drawing.Size(1318, 42);
            this.HeaderControl.TabIndex = 223;
            this.HeaderControl.Values.Description = "";
            this.HeaderControl.Values.Heading = "Tập tin Excel cài đặt phí Hỗ trợ lao động nữ có con nhỏ (từ 1-6 tuổi)";
            this.HeaderControl.Values.Image = ((System.Drawing.Image)(resources.GetObject("txtTitle.Values.Image")));
            // 
            // btnMini
            // 
            this.btnMini.Image = ((System.Drawing.Image)(resources.GetObject("btnMini.Image")));
            this.btnMini.UniqueName = "F5F06E8241504E72ABB5BEA3F6A9B753";
            // 
            // btnMax
            // 
            this.btnMax.Image = ((System.Drawing.Image)(resources.GetObject("btnMax.Image")));
            this.btnMax.UniqueName = "035D1A4881E44F58A084C31DE7352A94";
            // 
            // btnClose
            // 
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.UniqueName = "11B07C6F4E1C4F9D8B91BD924CB0EBE6";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.PicLoading);
            this.panel1.Controls.Add(this.txt_Sql);
            this.panel1.Controls.Add(this.txt_Error);
            this.panel1.Controls.Add(this.txt_Amount_Excel);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.lbl_Sql);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.lblFileName);
            this.panel1.Controls.Add(this.btn_Open);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 42);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1318, 54);
            this.panel1.TabIndex = 225;
            // 
            // PicLoading
            // 
            this.PicLoading.BackColor = System.Drawing.Color.Transparent;
            this.PicLoading.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.PicLoading.Dock = System.Windows.Forms.DockStyle.Right;
            this.PicLoading.Image = ((System.Drawing.Image)(resources.GetObject("PicLoading.Image")));
            this.PicLoading.Location = new System.Drawing.Point(1248, 0);
            this.PicLoading.Name = "PicLoading";
            this.PicLoading.Size = new System.Drawing.Size(68, 52);
            this.PicLoading.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.PicLoading.TabIndex = 208;
            this.PicLoading.TabStop = false;
            this.PicLoading.Visible = false;
            // 
            // txt_Sql
            // 
            this.txt_Sql.Location = new System.Drawing.Point(642, 20);
            this.txt_Sql.Name = "txt_Sql";
            this.txt_Sql.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_Sql.ReadOnly = true;
            this.txt_Sql.Size = new System.Drawing.Size(52, 26);
            this.txt_Sql.StateCommon.Border.ColorAngle = 1F;
            this.txt_Sql.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_Sql.StateCommon.Border.Rounding = 4;
            this.txt_Sql.StateCommon.Border.Width = 1;
            this.txt_Sql.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Sql.TabIndex = 111;
            this.txt_Sql.Text = "0";
            this.txt_Sql.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txt_Error
            // 
            this.txt_Error.Location = new System.Drawing.Point(519, 21);
            this.txt_Error.Name = "txt_Error";
            this.txt_Error.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_Error.ReadOnly = true;
            this.txt_Error.Size = new System.Drawing.Size(52, 26);
            this.txt_Error.StateCommon.Border.ColorAngle = 1F;
            this.txt_Error.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_Error.StateCommon.Border.Rounding = 4;
            this.txt_Error.StateCommon.Border.Width = 1;
            this.txt_Error.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Error.TabIndex = 111;
            this.txt_Error.Text = "0";
            this.txt_Error.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txt_Amount_Excel
            // 
            this.txt_Amount_Excel.Location = new System.Drawing.Point(393, 21);
            this.txt_Amount_Excel.Name = "txt_Amount_Excel";
            this.txt_Amount_Excel.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_Amount_Excel.ReadOnly = true;
            this.txt_Amount_Excel.Size = new System.Drawing.Size(52, 26);
            this.txt_Amount_Excel.StateCommon.Border.ColorAngle = 1F;
            this.txt_Amount_Excel.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_Amount_Excel.StateCommon.Border.Rounding = 4;
            this.txt_Amount_Excel.StateCommon.Border.Width = 1;
            this.txt_Amount_Excel.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Amount_Excel.TabIndex = 111;
            this.txt_Amount_Excel.Text = "0";
            this.txt_Amount_Excel.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label1.Location = new System.Drawing.Point(498, 2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 15);
            this.label1.TabIndex = 12;
            this.label1.Text = "Số Lượng Lỗi";
            // 
            // lbl_Sql
            // 
            this.lbl_Sql.AutoSize = true;
            this.lbl_Sql.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Sql.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.lbl_Sql.Location = new System.Drawing.Point(616, 2);
            this.lbl_Sql.Name = "lbl_Sql";
            this.lbl_Sql.Size = new System.Drawing.Size(99, 15);
            this.lbl_Sql.TabIndex = 13;
            this.lbl_Sql.Text = "Số Lượng SQL";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label3.Location = new System.Drawing.Point(369, 2);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(107, 15);
            this.label3.TabIndex = 14;
            this.label3.Text = "Số Lượng Excel";
            // 
            // lblFileName
            // 
            this.lblFileName.AutoEllipsis = true;
            this.lblFileName.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFileName.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblFileName.Location = new System.Drawing.Point(3, 11);
            this.lblFileName.Margin = new System.Windows.Forms.Padding(0);
            this.lblFileName.Name = "lblFileName";
            this.lblFileName.Padding = new System.Windows.Forms.Padding(5);
            this.lblFileName.Size = new System.Drawing.Size(275, 30);
            this.lblFileName.TabIndex = 11;
            this.lblFileName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btn_Open
            // 
            this.btn_Open.Location = new System.Drawing.Point(281, 11);
            this.btn_Open.Name = "btn_Open";
            this.btn_Open.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Open.Size = new System.Drawing.Size(70, 30);
            this.btn_Open.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Open.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Open.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Open.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Open.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Open.TabIndex = 10;
            this.btn_Open.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Open.Values.Image")));
            this.btn_Open.Values.Text = "Chọn";
            // 
            // timer3
            // 
            this.timer3.Interval = 20;
            // 
            // timer1
            // 
            this.timer1.Interval = 50;
            // 
            // timer2
            // 
            this.timer2.Interval = 20;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.btn_SaveAll);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 578);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1318, 54);
            this.panel3.TabIndex = 226;
            // 
            // btn_SaveAll
            // 
            this.btn_SaveAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_SaveAll.Location = new System.Drawing.Point(1185, 5);
            this.btn_SaveAll.Name = "btn_SaveAll";
            this.btn_SaveAll.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_SaveAll.Size = new System.Drawing.Size(120, 40);
            this.btn_SaveAll.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_SaveAll.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_SaveAll.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_SaveAll.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_SaveAll.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_SaveAll.TabIndex = 10;
            this.btn_SaveAll.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_SaveAll.Values.Image")));
            this.btn_SaveAll.Values.Text = "Cập nhật";
            // 
            // GV_Employee
            // 
            this.GV_Employee.AllowUserToAddRows = false;
            this.GV_Employee.AllowUserToDeleteRows = false;
            this.GV_Employee.BackgroundColor = System.Drawing.Color.White;
            this.GV_Employee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GV_Employee.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GV_Employee.Location = new System.Drawing.Point(0, 96);
            this.GV_Employee.Name = "GV_Employee";
            this.GV_Employee.RowHeadersWidth = 51;
            this.GV_Employee.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GV_Employee.Size = new System.Drawing.Size(1318, 482);
            this.GV_Employee.TabIndex = 227;
            // 
            // Frm_Import_FeeChildren
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1318, 632);
            this.Controls.Add(this.GV_Employee);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.HeaderControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_Import_FeeChildren";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Frm_Import_FeeChildren";
            this.Load += new System.EventHandler(this.Frm_Import_FeeChildren_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicLoading)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GV_Employee)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ComponentFactory.Krypton.Toolkit.KryptonHeader HeaderControl;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMini;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMax;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnClose;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox PicLoading;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_Sql;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_Error;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_Amount_Excel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbl_Sql;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblFileName;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Open;
        private System.Windows.Forms.Timer timer3;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DataGridView GV_Employee;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_SaveAll;
    }
}