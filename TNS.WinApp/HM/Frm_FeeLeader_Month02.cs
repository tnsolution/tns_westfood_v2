﻿using C1.Win.C1FlexGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.HRM;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_FeeLeader_Month02 : Form
    {
        private int _Key = 0;
        private int _BranchKey = 0;
        private int _DepartmentKey = 0;
        private int _TeamKey = 0;
        private int _Category = 0;
        private string _Search = "";
        private DateTime _DateView;
        public Frm_FeeLeader_Month02()
        {
            InitializeComponent();
            btnClose.Click += btnClose_Click;
            btnMax.Click += btnMax_Click;
            btnMini.Click += btnMini_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            btn_Search.Click += Btn_Search_Click;
            btn_Export.Click += Btn_Export_Click;
            btn_Import.Click += Btn_Import_Click;
            btn_AddCategory.Click += Btn_AddCategory_Click;
            btn_New.Click += Btn_New_Click;
            btn_Save.Click += Btn_Save_Click;
            btn_Del.Click += Btn_Del_Click;

            cbo_Branch.SelectedIndexChanged += Cbo_Branch_SelectedIndexChanged;
            cbo_Department.SelectedIndexChanged += Cbo_Department_SelectedIndexChanged;
            GVData.Click += GVData_Click;

            txt_EmployeeID.Enter += Txt_EmployeeID_Enter;
            txt_EmployeeID.Leave += Txt_EmployeeID_Leave;
        }

       

        private void Frm_FeeLeader_Month02_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            Check_RoleForm();

            LoadDataToToolbox.KryptonComboBox(cbo_FeeSearch, "SELECT AutoKey,ID+'-'+Name FROM HRM_Fee_Category WHERE RecordStatus <> 99 AND Slug =3  ORDER BY [Rank]", "--Tất cả--");
            LoadDataToToolbox.KryptonComboBox(cbo_Department, "SELECT DepartmentKey, DepartmentName FROM[dbo].[SYS_Department] WHERE DepartmentKey != 98  AND RecordStatus< 99", "---- Chọn----");
            LoadDataToToolbox.KryptonComboBox(cbo_Branch, "SELECT BranchKey,BranchName FROM SYS_Branch WHERE BranchKey != 98  AND RecordStatus < 99", "---- Tất cả----");
            LoadDataToToolbox.KryptonComboBox(cbo_Team, "SELECT TeamKey,TeamName FROM SYS_Team WHERE TeamKey != 98  AND RecordStatus < 99", "---- Chọn ----");
            LoadDataToToolbox.KryptonComboBox(cbo_Salary, "SELECT AutoKey,ID+'-'+Name FROM HRM_Fee_Category WHERE RecordStatus <> 99 AND Slug =3  ORDER BY [Rank]", "--Tất cả--");
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            dteDate.Value = SessionUser.Date_Work;
            SetDefault();
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
        }
        private void GVData_Click(object sender, EventArgs e)
        {
            if (GVData.Rows.Count > 0 && GVData.Rows[GVData.RowSel][9].ToString() != "")
            {
                _Key = GVData.Rows[GVData.RowSel][9].ToInt();
                LoadData();
            }
        }


        private void Cbo_Branch_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDataToToolbox.KryptonComboBox(cbo_Department, "SELECT DepartmentKey, DepartmentName FROM[dbo].[SYS_Department] WHERE BranchKey = " + cbo_Branch.SelectedValue.ToInt() + "  AND RecordStatus< 99", "---- Chọn tất cả----");

        }
        private void Cbo_Department_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDataToToolbox.KryptonComboBox(cbo_Team, "SELECT TeamKey,TeamName FROM SYS_Team WHERE TeamKey != 98 AND DepartmentKey = " + cbo_Department.SelectedValue.ToInt() + "  AND RecordStatus < 99", "---- Chọn tất cả ----");
        }

        private int _STT = 1;
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            _BranchKey = cbo_Branch.SelectedValue.ToInt();
            _DepartmentKey = cbo_Department.SelectedValue.ToInt();
            _TeamKey = cbo_Team.SelectedValue.ToInt();
            _Category = cbo_FeeSearch.SelectedValue.ToInt();
            _Search = txt_Search.Text.Trim();
            _DateView = dteDate.Value;
            try
            {
                using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }

            }
            catch (Exception ex)
            {
                Utils.TNMessageBoxOK(ex.ToString(), 4);
            }

        }
        private void DisplayData()
        {
            DataTable _Intable = FeeLeaderMonth_Data.ListV2(_DateView, _BranchKey, _DepartmentKey, _TeamKey, _Category, _Search);
            if (_Intable.Rows.Count > 0)
            {
                this.Invoke(new MethodInvoker(delegate ()
                {
                    InitGV_Layout(_Intable);
                    _STT = 1;
                    int NoGroup = 2;
                    int RowTam = NoGroup;
                    Row zGvRow;
                    int TeamKey = _Intable.Rows[0]["TeamKey"].ToInt();
                    string TeamName = _Intable.Rows[0]["TeamName"].ToString();
                    DataRow[] Array = _Intable.Select("TeamKey=" + TeamKey);
                    if (Array.Length > 0)
                    {
                        DataTable zGroup = Array.CopyToDataTable();
                        GVData.Rows.Add();
                        zGvRow = GVData.Rows[1];
                        HeaderRow(zGvRow, zGroup, TeamKey, TeamName, 0);
                    }
                    for (int i = 0; i < _Intable.Rows.Count; i++)
                    {
                        DataRow r = _Intable.Rows[i];
                        if (TeamKey != r["TeamKey"].ToInt())
                        {
                            #region [GROUP]
                            TeamKey = r["TeamKey"].ToInt();
                            TeamName = r["TeamName"].ToString();
                            Array = _Intable.Select("TeamKey=" + TeamKey);
                            _STT = 1;
                            if (Array.Length > 0)
                            {
                                DataTable zGroup = Array.CopyToDataTable();
                                GVData.Rows.Add();
                                zGvRow = GVData.Rows[i + NoGroup];
                                HeaderRow(zGvRow, zGroup, TeamKey, TeamName, _STT);
                            }
                            #endregion
                            NoGroup++;
                        }
                        GVData.Rows.Add();
                        zGvRow = GVData.Rows[i + NoGroup];
                        DetailRow(zGvRow, r, _STT);
                        RowTam = i + NoGroup;
                        _STT++;
                    }
                    GVData.Rows.Add();
                    zGvRow = GVData.Rows[RowTam + 1];
                    TotalRow(zGvRow, _Intable, RowTam + 1);

                }));
            }
        }
        void InitGV_Layout(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            //int TotalRow = TableView.Rows.Count + 1;
            int ToTalCol = 10;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(1);

            //Row Header
            GVData.Rows[0][0] = "Stt";
            GVData.Rows[0][1] = "Ngày";
            GVData.Rows[0][2] = "Họ và tên";
            GVData.Rows[0][3] = "Số thẻ";
            GVData.Rows[0][4] = "Mã phí hỗ trợ";
            GVData.Rows[0][5] = "Tên phí hỗ trợ";
            GVData.Rows[0][6] = "Giá trị";
            GVData.Rows[0][7] = "Đơn vị";
            GVData.Rows[0][8] = "Ghi chú";

            GVData.Rows[0][9] = ""; // ẩn cột này

            //for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            //{
            //    DataRow rData = TableView.Rows[rIndex];

            //    GVData.Rows[rIndex + 1][0] = rIndex + 1;
            //    GVData.Rows[rIndex + 1][1] = rData[1];
            //    GVData.Rows[rIndex + 1][2] = rData[2];
            //    GVData.Rows[rIndex + 1][3] = rData[3];
            //    GVData.Rows[rIndex + 1][4] = rData[4].Ton0String();
            //    GVData.Rows[rIndex + 1][5] = rData[5];
            //    GVData.Rows[rIndex + 1][6] = rData[6];
            //    GVData.Rows[rIndex + 1][7] = rData[7];
            //    GVData.Rows[rIndex + 1][8] = rData[0];
            //}

            //Style         
            GVData.AllowFreezing = AllowFreezingEnum.Both;
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.FixedOnly;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.LeftCenter;
            GVData.Styles.Normal.WordWrap = true;

            //Freeze Row and Column                              
            GVData.Rows.Fixed = 1;
            GVData.Cols.Frozen = 4;

            GVData.Cols[0].StyleNew.BackColor = Color.Empty;
            GVData.Cols[1].StyleNew.BackColor = Color.Empty;
            GVData.Cols[2].StyleNew.BackColor = Color.Empty;
            GVData.Cols[3].StyleNew.BackColor = Color.Empty;
            GVData.Rows[0].TextAlign = TextAlignEnum.CenterCenter;

            GVData.Rows[0].Height = 60;

            GVData.Cols[0].Width = 35;
            GVData.Cols[1].Width = 80;
            GVData.Cols[2].Width = 170;
            GVData.Cols[3].Width = 100;
            GVData.Cols[4].Width = 100;
            GVData.Cols[5].Width = 200;
            GVData.Cols[6].Width = 100;
            GVData.Cols[6].TextAlign = TextAlignEnum.RightCenter;
            GVData.Cols[7].Width = 50;
            GVData.Cols[8].Width = 120;
            GVData.Cols[9].Visible = false;
        }
        private void HeaderRow(Row RowView, DataTable Table, int TeamKey, string TeamName, int No)
        {
            RowView[0] = "";
            RowView[2] = TeamName;
            RowView[3] = "";/// "Số nhân sự " + Table.Select("TeamKey=" + TeamKey).Length;
            RowView[6] = "";// Table.Compute("SUM([" + Table.Columns[9].ColumnName + "])", "TeamKey=" + TeamKey).Toe1String();
            RowView[9] = "";
            RowView.StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
        }
        private void DetailRow(Row RowView, DataRow rDetail, int No)
        {
            RowView[0] = (No).ToString();
            DateTime zFromDate;
            if (rDetail[1] != null && rDetail[1].ToString() != "")
            {
                zFromDate = DateTime.Parse(rDetail[1].ToString());
                RowView[1] = zFromDate.ToString("dd/MM/yyyy");
            }
            else
            {
                RowView[1] = "";
            }
            RowView[2] = rDetail["EmployeeName"].ToString().Trim();
            RowView[3] = rDetail["EmployeeID"].ToString().Trim();
            RowView[4] = rDetail["CategoryID"].ToString().Trim();
            RowView[5] = rDetail["CategoryName"].ToString().Trim();
            RowView[6] = rDetail["Value"].Toe1String().Trim();
            RowView[7] = rDetail["Ext"].ToString().Trim();
            RowView[8] = rDetail["Description"].ToString().Trim();
            RowView[9] = rDetail[0];//autokey
        }
        private void TotalRow(Row RowView, DataTable Table, int No)
        {
            RowView[0] = "";
            RowView[1] = "TỔNG CỘNG ";
            RowView[3] = "";// "Số nhân sự " + Table.Rows.Count;
            RowView[6] = "";// Table.Compute("SUM([" + Table.Columns[9].ColumnName + "])", "").Toe1String();
            RowView[9] = "";
            RowView.StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);

        }
        private void Btn_Export_Click(object sender, EventArgs e)
        {
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Các_Phí_Hỗ_Trợ_Theo_Thang_" + cbo_FeeSearch.Text + ".xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }

                    string Status = "Xuất Excel form " + HeaderControl.Text + " > đường dẫn và tên file xuất:" + FDialog.FileName;
                    Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);

                    GVData.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }

        }
        private void Btn_Import_Click(object sender, EventArgs e)
        {
            Frm_Import_FeeLeaderMonth frm = new Frm_Import_FeeLeaderMonth();
            frm.Show();
        }
        private void Btn_AddCategory_Click(object sender, EventArgs e)
        {
            Frm_Fee_Category frm = new Frm_Fee_Category();
            frm.Slug = 3;
            frm.ShowDialog();
            LoadDataToToolbox.KryptonComboBox(cbo_FeeSearch, "SELECT AutoKey,ID+'-'+Name FROM HRM_Fee_Category WHERE RecordStatus <> 99 AND Slug =3  ORDER BY [Rank]", "--Tất cả--");
        }

        #region[ update]
        private void Btn_New_Click(object sender, EventArgs e)
        {
            SetDefault();
        }
        private void SetDefault()
        {
            _Key = 0;
            txt_EmployeeID.Text = "Nhập mã thẻ";
            txt_EmployeeID.Tag = null;
            txt_EmployeeName.Text = "";
            txt_Amount.Text = "0";
            txt_Description.Text = "";
            cbo_Salary.SelectedIndex = 1;
            dte_DateWrite.Value = SessionUser.Date_Work;
            cbo_Ext.SelectedIndex = 1;
            btn_Del.Enabled = false;
            lbl_Created.Text = "Tạo bởi:";
            lbl_Modified.Text = "Chỉnh sửa:";
            txt_EmployeeID.Focus();
        }
        private void LoadData()
        {
            FeeLeaderMonth_Info zinfo = new FeeLeaderMonth_Info(_Key);
            txt_EmployeeID.Tag = zinfo.EmployeeKey;
            txt_EmployeeID.Text = zinfo.EmployeeID;
            txt_EmployeeName.Text = zinfo.EmployeeName;
            float zValue = float.Parse(zinfo.Value.ToString());
            txt_Amount.Text = zValue.Ton1String();
            txt_Description.Text = zinfo.Description;
            if (zinfo.Ext == "VND")
                cbo_Ext.SelectedIndex = 0;
            if (zinfo.Ext == "NGAY")
                cbo_Ext.SelectedIndex = 1;
            if (zinfo.Ext == "%")
                cbo_Ext.SelectedIndex = 2;

            dte_DateWrite.Value = zinfo.DateWrite;
            cbo_Salary.SelectedValue = zinfo.CategoryKey;
            btn_Del.Enabled = true;
            lbl_Created.Text = "Tạo bởi:[" + zinfo.CreatedName + "][" + zinfo.ModifiedOn + "]";
            lbl_Modified.Text = "Chỉnh sửa:[" + zinfo.ModifiedName + "][" + zinfo.ModifiedOn + "]";
        }
        private void Btn_Save_Click(object sender, EventArgs e)
        {
            if (SessionUser.Date_Lock >= dte_DateWrite.Value)
            {
                Utils.TNMessageBoxOK("Thời gian này đã khóa sổ.!", 2);
                return;
            }
            if (txt_EmployeeID.Tag==null)
            {
                Utils.TNMessageBoxOK("Chưa nhập mã nhân viên!",1);
                return;
            }
            if (cbo_Salary.SelectedValue.ToInt() == 0)
            {
                Utils.TNMessageBoxOK("Chọn danh mục!",1);
                return;
            }
            float Value = 0;
            if (!float.TryParse(txt_Amount.Text.Trim(), out Value))
            {
                Utils.TNMessageBoxOK("Giá trị/ số tiền không đúng!",1);
                return;
            }
            else
            {
                FeeLeaderMonth_Info zinfo = new FeeLeaderMonth_Info(_Key);
                zinfo.EmployeeKey = txt_EmployeeID.Tag.ToString();
                zinfo.EmployeeID = txt_EmployeeID.Text.Trim().ToUpper();
                zinfo.EmployeeName = txt_EmployeeName.Text.Trim().ToString();
                zinfo.Value = float.Parse(txt_Amount.Text.Trim());
                zinfo.Description = txt_Description.Text;
                zinfo.DateWrite = dte_DateWrite.Value;
                zinfo.Ext = cbo_Ext.Text;

                zinfo.CategoryKey = cbo_Salary.SelectedValue.ToInt();
                string[] s = cbo_Salary.Text.Split('-');
                zinfo.CategoryID = s[0];
                zinfo.CategoryName = s[1];
                zinfo.CreatedBy = SessionUser.UserLogin.Key;
                zinfo.CreatedName = SessionUser.UserLogin.EmployeeName;
                zinfo.ModifiedBy = SessionUser.UserLogin.Key;
                zinfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
                zinfo.Save();
                if (zinfo.Message == "11" || zinfo.Message == "20")
                {
                    Utils.TNMessageBoxOK("Cập nhật thành công!",3);
                    _Key = zinfo.Key;
                    LoadData();
                }
                else
                {
                    Utils.TNMessageBoxOK(zinfo.Message,4);
                }
            }


        }
        private void Btn_Del_Click(object sender, EventArgs e)
        {
            if (_Key == 0)
            {
                Utils.TNMessageBoxOK("Chưa chọn thông tin!",1);
            }
            else
            {
                FeeLeaderMonth_Info zinfo = new FeeLeaderMonth_Info(_Key);
                if (SessionUser.Date_Lock >= zinfo.DateWrite)
                {
                    Utils.TNMessageBoxOK("Thời gian này đã khóa sổ.!", 2);
                    return;
                }
                if (Utils.TNMessageBox("Bạn có chắc xóa thông tin này ?.",2) == "Y")
                {
                  
                    zinfo.ModifiedBy = SessionUser.UserLogin.Key;
                    zinfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
                    zinfo.Delete();
                    if (zinfo.Message == "30")
                    {
                        Utils.TNMessageBoxOK("Đã xóa thành công !",3);
                        SetDefault();
                    }
                    else
                    {
                        Utils.TNMessageBoxOK(zinfo.Message,4);
                    }

                }
            }

        }
        private void Txt_EmployeeID_Leave(object sender, EventArgs e)
        {
            if (txt_EmployeeID.Text.Trim().Length < 1)
            {
                txt_EmployeeID.Text = "Nhập mã thẻ";
                txt_EmployeeID.ForeColor = Color.White;
            }
            else
            {
                Employee_Info zinfo = new Employee_Info();
                zinfo.GetEmployee(txt_EmployeeID.Text.Trim());
                if (zinfo.Key == "")
                {
                    txt_EmployeeID.Text = "Nhập mã thẻ";
                    txt_EmployeeID.ForeColor = Color.White;
                    txt_EmployeeID.Tag = null;
                }
                else
                {
                    txt_EmployeeID.Text = zinfo.EmployeeID;
                    txt_EmployeeName.Text = zinfo.FullName;
                    txt_EmployeeID.Tag = zinfo.Key;
                    txt_EmployeeID.BackColor = Color.Black;
                }
            }
        }
        private void Txt_EmployeeID_Enter(object sender, EventArgs e)
        {
            if (txt_EmployeeID.Text.Trim() == "Nhập mã thẻ")
            {
                txt_EmployeeID.Text = "";
                txt_EmployeeID.ForeColor = Color.Brown;
            }
        }
        #endregion
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;

                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                //this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion
        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
            }
            if (Role.RoleAdd == 0 && Role.RoleEdit == 0)
            {
                btn_New.Enabled = false;
                btn_Save.Enabled = false;
            }
            if (Role.RoleAdd == 0 && Role.RoleEdit == 1)
            {
                btn_New.Enabled = false;
                btn_Save.Enabled = true;
            }
            if (Role.RoleDel == 0)
            {
                btn_Del.Enabled = false;
            }
        }
        #endregion
    }
}
