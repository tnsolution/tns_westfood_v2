﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.HRM;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_ScoreStockBegin_Detail : Form
    {
        private int _Key = 0;
        public int Key
        {
            get
            {
                return _Key;
            }

            set
            {
                _Key = value;
            }
        }
        public Frm_ScoreStockBegin_Detail()
        {
            InitializeComponent();
            btn_Save.Click += Btn_Save_Click;
            btn_New.Click += Btn_New_Click;
            btn_Del.Click += Btn_Del_Click;

            btnMini.Click += btnMini_Click;
            btnClose.Click += btnClose_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;


            txt_EmployeeID.Enter += Txt_EmployeeID_Enter;
            txt_EmployeeID.Leave += Txt_EmployeeID_Leave;
            txt_ScoreBorrow.Leave += Txt_ScoreBorrow_Leave;
            txt_ScoreStock.Leave += Txt_ScoreStock_Leave;
        }

       
        private void Frm_Salary_Default_Detail_Load(object sender, EventArgs e)
        {
           
            if (Key==0)
                Load_Refresh();
            else
                LoadOrderInfo();
        }
        private void Txt_ScoreStock_Leave(object sender, EventArgs e)
        {
            double zAmount = 0;
            if (double.TryParse(txt_ScoreStock.Text.Trim(), out zAmount))
            {
                txt_ScoreStock.Text = zAmount.Ton1String();
               // txt_ScoreStock.Focus();
                txt_ScoreStock.SelectionStart = txt_ScoreStock.Text.Length;
            }
            else
            {
                MessageBox.Show("Lỗi!.Định dạng số.");
            }
        }

        private void Txt_ScoreBorrow_Leave(object sender, EventArgs e)
        {
            double zAmount = 0;
            if (double.TryParse(txt_ScoreBorrow.Text.Trim(), out zAmount))
            {
                txt_ScoreBorrow.Text = zAmount.Ton1String();
               // txt_ScoreBorrow.Focus();
                txt_ScoreBorrow.SelectionStart = txt_ScoreBorrow.Text.Length;
            }
            else
            {
                MessageBox.Show("Lỗi!.Định dạng số.");
            }
        }

        private void LoadOrderInfo()
        {
            Score_Stock_Info zInfo = new Score_Stock_Info(_Key);
            txt_EmployeeID.Tag = zInfo.EmployeeKey;
            if (zInfo.Key != 0)
            {
                txt_EmployeeID.ReadOnly = true;
            }
            txt_EmployeeName.Text = zInfo.EmployeeName;
            txt_EmployeeID.Text = zInfo.EmployeeID;

            if (zInfo.DateWrite != DateTime.MinValue)
            {
                dte_DateWrite.Value = zInfo.DateWrite;
            }
            txt_Description.Text = zInfo.Description;
            txt_ScoreStock.Text = zInfo.ScoreStock.Ton1String();
            txt_ScoreBorrow.Text = zInfo.ScoreBorrow.Ton1String();
            btn_Del.Enabled = true;

            lbl_Created.Text = "Tạo bởi:[" + zInfo.CreatedName + "][" + zInfo.ModifiedOn + "]";
            lbl_Modified.Text = "Chỉnh sửa:[" + zInfo.ModifiedName + "][" + zInfo.ModifiedOn + "]";

        }
        private void Load_Refresh()
        {
            _Key = 0;
            dte_DateWrite.Value = SessionUser.Date_Work;
            txt_EmployeeID.Tag = null;
            txt_EmployeeID.Text = "";
            txt_EmployeeName.Text = "";
            txt_ScoreStock.Text = "0";
            txt_ScoreBorrow.Text = "0";
            txt_Description.Text = "";
            txt_EmployeeID.ReadOnly = false;
            txt_EmployeeID.Focus();
            lbl_Created.Text = "Tạo bởi:";
            lbl_Modified.Text = "Chỉnh sửa:";
            btn_Del.Enabled = false;
        }

        private void Btn_Save_Click(object sender, EventArgs e)
        {
            float zScoreStock = 0;
            float zScoreBorrow = 0;

            if (txt_EmployeeID.Tag == null || txt_EmployeeID.Text.Trim() == "")
            {
                MessageBox.Show("Chưa chọn nhân viên!");
            }
            //else if (Score_Stock_Data.Count(txt_EmployeeID.Tag.ToString(), dte_DateWrite.Value) > 0)
            //{
            //    MessageBox.Show("Nhân viên tháng này đã nhập!");
            //}
            else if (!float.TryParse(txt_ScoreStock.Text.Trim(), out zScoreStock))
            {
                MessageBox.Show("Công tích lũy không hợp lệ!");
            }
            else if (!float.TryParse(txt_ScoreBorrow.Text.Trim(), out zScoreBorrow))
            {
                MessageBox.Show("Cộng mượn không hợp lệ!");
            }
            else
            {
                Score_Stock_Info zInfo = new Score_Stock_Info(_Key);
                zInfo.DateWrite = dte_DateWrite.Value;
                zInfo.EmployeeKey = txt_EmployeeID.Tag.ToString();
                zInfo.EmployeeID = txt_EmployeeID.Text.ToString().Trim();
                zInfo.EmployeeName = txt_EmployeeName.Text.ToString();
                Employee_Info zEm = new Employee_Info(zInfo.EmployeeKey);
                zInfo.TeamKey = zEm.TeamKey;
                zInfo.DepartmentKey = zEm.DepartmentKey;
                zInfo.BranchKey = zEm.BranchKey;
                zInfo.ScoreStock = float.Parse(txt_ScoreStock.Text);
                zInfo.ScoreBorrow = float.Parse(txt_ScoreBorrow.Text);
                zInfo.Description = txt_Description.Text;

                zInfo.CreatedBy = SessionUser.UserLogin.Key;
                zInfo.CreatedName = SessionUser.UserLogin.EmployeeName;
                zInfo.ModifiedBy = SessionUser.UserLogin.Key;
                zInfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
                zInfo.Save();
                string zMessage = TN_Message.Show(zInfo.Message);
                if (zMessage.Length == 0)
                {
                    MessageBox.Show("Câp nhật thành công", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    _Key = zInfo.Key;
                    LoadOrderInfo();
                }
                else
                {
                    MessageBox.Show("Vui lòng kiểm tra lại", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }


        }
        private void Btn_Del_Click(object sender, EventArgs e)
        {
            if (_Key != 0)
            {
                DialogResult dlr = MessageBox.Show("Bạn có xóa thông tin này ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlr == DialogResult.Yes)
                {
                    Score_Stock_Info zInfo = new Score_Stock_Info(_Key);
                    zInfo.ModifiedBy = SessionUser.UserLogin.Key;
                    zInfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
                    zInfo.Delete();
                    if (zInfo.Message.Length == 0)
                    {
                        MessageBox.Show("Câp nhật thành công", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Load_Refresh();
                    }
                    else
                    {
                        MessageBox.Show("Vui lòng kiểm tra lại", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                }
            }
            else
            {
                MessageBox.Show("Vui lòng chọn phiếu !.", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }
        private void Btn_New_Click(object sender, EventArgs e)
        {
            Load_Refresh();
        }


        #region[event]
        private void Txt_EmployeeID_Enter(object sender, EventArgs e)
        {

            if (txt_EmployeeID.Text.Trim() == "Nhập mã thẻ")
            {
                txt_EmployeeID.Text = "";
                txt_EmployeeID.ForeColor = Color.Brown;
            }
        }
        private void Txt_EmployeeID_Leave(object sender, EventArgs e)
        {
            if (txt_EmployeeID.Text.Trim().Length < 1)
            {
                txt_EmployeeID.Text = "Nhập mã thẻ";
                txt_EmployeeID.ForeColor = Color.White;
            }
            else
            {
                Employee_Info zinfo = new Employee_Info();
                zinfo.GetEmployee(txt_EmployeeID.Text.Trim());
                if (zinfo.Key == "")
                {
                    txt_EmployeeID.Text = "Nhập mã thẻ";
                    txt_EmployeeID.ForeColor = Color.White;
                    txt_EmployeeID.Tag = null;
                }
                else
                {
                    txt_EmployeeID.Text = zinfo.EmployeeID;
                    txt_EmployeeName.Text = zinfo.FullName;
                    txt_EmployeeID.Tag = zinfo.Key;
                    txt_EmployeeID.BackColor = Color.Black;
                }
            }
        }
        #endregion
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}
