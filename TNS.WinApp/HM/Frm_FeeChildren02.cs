﻿using C1.Win.C1FlexGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.HRM;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_FeeChildren02 : Form
    {
        DataTable ztbType = new DataTable();
        private int _BranchKey = 0;
        private int _DepartmentKey = 0;
        private int _TeamKey = 0;
        private int _Category = 0;
        private int _Type = 0;
        private string _Search = "";
        private int _Key = 0;
        private DateTime _LeavingDate;
        public Frm_FeeChildren02()
        {
            InitializeComponent();
            btnClose.Click += btnClose_Click;
            btnMax.Click += btnMax_Click;
            btnMini.Click += btnMini_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            btn_Search.Click += Btn_Search_Click;
            btn_Export.Click += Btn_Export_Click;
            btn_Import.Click += Btn_Import_Click;
            btn_AddCategory.Click += Btn_AddCategory_Click;
            btn_New.Click += Btn_New_Click;
            btn_Save.Click += Btn_Save_Click;
            btn_Del.Click += Btn_Del_Click;
            btn_Copy.Click += Btn_Copy_Click;

            cbo_Branch.SelectedIndexChanged += Cbo_Branch_SelectedIndexChanged;
            cbo_Department.SelectedIndexChanged += Cbo_Department_SelectedIndexChanged;
            GVData.Click += GVData_Click;
            txt_EmployeeID.Enter += Txt_EmployeeID_Enter;
            txt_EmployeeID.Leave += Txt_EmployeeID_Leave;
            txt_Amount.KeyUp += Txt_Amount_KeyUp;
        }



        private void Frm_FeeChildren02_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            Check_RoleForm();

            LoadTableType();
            LoadDataToToolbox.KryptonComboBox(cbo_SalarySearch, "SELECT AutoKey,ID+'-'+Name FROM HRM_Fee_Category WHERE RecordStatus <> 99 AND Slug =2  ORDER BY [Rank]", "--Tất cả--");
            if (Role.AccessBranch == "2,4")
                LoadDataToToolbox.KryptonComboBox(cbo_Branch, "SELECT BranchKey,BranchName FROM SYS_Branch WHERE BranchKey IN (" + Role.AccessBranch + ")  AND RecordStatus < 99", "--Chọn tất cả--");
            else
            {
                LoadDataToToolbox.KryptonComboBox(cbo_Branch, "SELECT BranchKey,BranchName FROM SYS_Branch WHERE BranchKey IN (" + Role.AccessBranch + ")  AND RecordStatus < 99", "");
                Cbo_Branch_SelectedIndexChanged(null, null);
            }
            LoadDataToToolbox.ComboBoxData(cbo_Type.ComboBox, ztbType, false, 0, 1);

            LoadDataToToolbox.KryptonComboBox(cbo_Salary, "SELECT AutoKey,ID+'-'+Name FROM HRM_Fee_Category WHERE RecordStatus <> 99 AND Slug=2  ORDER BY [Rank]", "");
            SetDefault();
            DateTime FromDate = SessionUser.Date_Work;
            dte_Statistic.Value = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
        }
        private void GVData_Click(object sender, EventArgs e)
        {
            if (GVData.Rows.Count > 0 && GVData.Rows[GVData.RowSel][10] !=null && GVData.Rows[GVData.RowSel][10].ToString() != "")
            {
                _Key = GVData.Rows[GVData.RowSel][10].ToString().ToInt();
                LoadData();
            }
        }


        private void Btn_New_Click(object sender, EventArgs e)
        {
            SetDefault();
        }

        private void Cbo_Branch_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDataToToolbox.KryptonComboBox(cbo_Department, "SELECT DepartmentKey, DepartmentName FROM[dbo].[SYS_Department] WHERE BranchKey = " + cbo_Branch.SelectedValue.ToInt() + "  AND RecordStatus< 99", "---- Chọn tất cả----");

        }
        private void Cbo_Department_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDataToToolbox.KryptonComboBox(cbo_Team, "SELECT TeamKey,TeamName FROM SYS_Team WHERE TeamKey != 98 AND DepartmentKey = " + cbo_Department.SelectedValue.ToInt() + "  AND RecordStatus < 99", "---- Chọn tất cả ----");
        }
        void LoadTableType()
        {
            ztbType = new DataTable();
            ztbType.Columns.Add("Value");
            ztbType.Columns.Add("Name");
            ztbType.Rows.Add("0", "--Tất cả--");
            ztbType.Rows.Add("1", "Đang áp dụng");
            ztbType.Rows.Add("2", "Đã hết thời hạn");
        }
        private int _STT = 1;
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            _BranchKey = cbo_Branch.SelectedValue.ToInt();
            _DepartmentKey = cbo_Department.SelectedValue.ToInt();
            _TeamKey = cbo_Team.SelectedValue.ToInt();
            _Category = cbo_SalarySearch.SelectedValue.ToInt();
            _Type = cbo_Type.SelectedValue.ToInt();
            _Search = txt_Search.Text.Trim();
            _LeavingDate = dte_Statistic.Value;
            try
            {
                using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }

            }
            catch (Exception ex)
            {
                Utils.TNMessageBoxOK(ex.ToString(), 4);
            }

        }
        private void DisplayData()
        {
            DataTable _Intable = Employee_FeeChildren_Data.List01_V2(_BranchKey, _DepartmentKey, _TeamKey, _Category, _LeavingDate, _Search);
            if (_Intable.Rows.Count > 0)
            {
                this.Invoke(new MethodInvoker(delegate ()
                {
                    InitGV_Layout(_Intable);
                    _STT = 1;
                    int NoGroup = 2;
                    int RowTam = NoGroup;
                    Row zGvRow;
                    int TeamKey = _Intable.Rows[0]["TeamKey"].ToInt();
                    string TeamName = _Intable.Rows[0]["TeamName"].ToString();
                    DataRow[] Array = _Intable.Select("TeamKey=" + TeamKey);
                    if (Array.Length > 0)
                    {
                        DataTable zGroup = Array.CopyToDataTable();
                        GVData.Rows.Add();
                        zGvRow = GVData.Rows[1];
                        HeaderRow(zGvRow, zGroup, TeamKey, TeamName, 0);
                    }
                    for (int i = 0; i < _Intable.Rows.Count; i++)
                    {
                        DataRow r = _Intable.Rows[i];
                        if (TeamKey != r["TeamKey"].ToInt())
                        {
                            #region [GROUP]
                            TeamKey = r["TeamKey"].ToInt();
                            TeamName = r["TeamName"].ToString();
                            Array = _Intable.Select("TeamKey=" + TeamKey);
                            _STT = 1;
                            if (Array.Length > 0)
                            {
                                DataTable zGroup = Array.CopyToDataTable();
                                GVData.Rows.Add();
                                zGvRow = GVData.Rows[i + NoGroup];
                                HeaderRow(zGvRow, zGroup, TeamKey, TeamName, _STT);
                            }
                            #endregion
                            NoGroup++;
                        }
                        GVData.Rows.Add();
                        zGvRow = GVData.Rows[i + NoGroup];
                        DetailRow(zGvRow, r, _STT);
                        RowTam = i + NoGroup;
                        _STT++;
                    }
                    GVData.Rows.Add();
                    zGvRow = GVData.Rows[RowTam + 1];
                    TotalRow(zGvRow, _Intable, RowTam + 1);

                }));
            }
        }

        void InitGV_Layout(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            //int TotalRow = TableView.Rows.Count + 1;
            int ToTalCol = 11;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(1);

            //Row Header
            GVData.Rows[0][0] = "Stt";
            GVData.Rows[0][1] = "Họ và tên";
            GVData.Rows[0][2] = "Số thẻ";
            GVData.Rows[0][3] = "Danh mục";
            GVData.Rows[0][4] = "Số con";
            GVData.Rows[0][5] = "Ngày sinh";
            GVData.Rows[0][6] = "Từ ngày";
            GVData.Rows[0][7] = "Đến ngày";
            GVData.Rows[0][8] = "Ngày nghỉ việc";
            GVData.Rows[0][9] = "Ghi chú";

            GVData.Rows[0][10] = ""; // ẩn cột này

            //for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            //{
            //    DataRow rData = TableView.Rows[rIndex];

            //    GVData.Rows[rIndex + 1][0] = rIndex + 1;
            //    GVData.Rows[rIndex + 1][1] = rData[1];
            //    GVData.Rows[rIndex + 1][2] = rData[2];
            //    GVData.Rows[rIndex + 1][3] = rData[3];
            //    GVData.Rows[rIndex + 1][4] = rData[4].Ton0String();
            //    GVData.Rows[rIndex + 1][5] = rData[5];
            //    GVData.Rows[rIndex + 1][6] = rData[6];
            //    GVData.Rows[rIndex + 1][7] = rData[7];
            //    GVData.Rows[rIndex + 1][8] = rData[0];
            //}

            //Style         
            GVData.AllowFreezing = AllowFreezingEnum.Both;
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.FixedOnly;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.LeftCenter;
            GVData.Styles.Normal.WordWrap = true;

            //Freeze Row and Column                              
            GVData.Rows.Fixed = 1;
            GVData.Cols.Frozen = 3;

            GVData.Cols[0].StyleNew.BackColor = Color.Empty;
            GVData.Cols[1].StyleNew.BackColor = Color.Empty;
            GVData.Cols[2].StyleNew.BackColor = Color.Empty;

            GVData.Rows[0].TextAlign = TextAlignEnum.CenterCenter;

            GVData.Rows[0].Height = 40;

            GVData.Cols[0].Width = 40;
            GVData.Cols[1].Width = 200;
            GVData.Cols[2].Width = 100;
            GVData.Cols[3].Width = 100;
            GVData.Cols[4].Width = 80;
            GVData.Cols[4].TextAlign = TextAlignEnum.RightCenter;
            GVData.Cols[5].Width = 100;
            GVData.Cols[6].Width = 100;
            GVData.Cols[7].Width = 100;
            GVData.Cols[8].Width = 100;
            GVData.Cols[9].Width = 100;
            GVData.Cols[10].Visible = false;
        }
        private void HeaderRow(Row RowView, DataTable Table, int TeamKey, string TeamName, int No)
        {
            RowView[0] = "";
            RowView[1] = TeamName;
            RowView[2] = "Số nhân sự " + Table.Select("TeamKey=" + TeamKey).Length;
            RowView[3] = "";
            RowView[4] = Table.Compute("SUM([" + Table.Columns[7].ColumnName + "])", "TeamKey=" + TeamKey).Toe0String();
            RowView[8] = "";
            RowView.StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
        }
        private void DetailRow(Row RowView, DataRow rDetail, int No)
        {
            RowView[0] = (No).ToString();
            RowView[1] = rDetail["EmployeeName"].ToString().Trim();
            RowView[2] = rDetail["EmployeeID"].ToString().Trim();
            RowView[3] = rDetail["CategoryID"].ToString().Trim();
            RowView[4] = rDetail[7].Ton1String();
            DateTime zBirthDay;
            if (rDetail[8] != null && rDetail[8].ToString() != "")
            {
                zBirthDay = DateTime.Parse(rDetail[8].ToString());
                RowView[5] = zBirthDay.ToString("dd/MM/yyyy");
            }
            else
            {
                RowView[5] = "";
            }

            DateTime zFromDate;
            if (rDetail[9] != null && rDetail[9].ToString() != "")
            {
                zFromDate = DateTime.Parse(rDetail[9].ToString());
                RowView[6] = zFromDate.ToString("dd/MM/yyyy");
            }
            else
            {
                RowView[6] = "";
            }
            DateTime zToDate;
            if (rDetail[10] != null && rDetail[10].ToString() != "")
            {
                zToDate = DateTime.Parse(rDetail[10].ToString());
                RowView[7] = zToDate.ToString("dd/MM/yyyy");
                //RowView.StyleNew.BackColor = Color.LightGray;
            }
            else
            {
                RowView[7] = "";
            }
            DateTime zLeavingDate;
            if (rDetail[11] != null && rDetail[11].ToString() != "")
            {
                zLeavingDate = DateTime.Parse(rDetail[11].ToString());
                RowView[8] = zLeavingDate.ToString("dd/MM/yyyy");
                //RowView.StyleNew.BackColor = Color.LightGray;
            }
            else
            {
                RowView[8] = "";
            }
            RowView[9] = rDetail[12].ToString();
            RowView[10] = rDetail[0];//autokey
        }
        private void TotalRow(Row RowView, DataTable Table, int No)
        {
            RowView[0] = "";
            RowView[1] = "TỔNG CỘNG ";
            RowView[2] = "Số nhân sự " + Table.Rows.Count;
            RowView[3] = "";
            RowView[4] = Table.Compute("SUM([" + Table.Columns[7].ColumnName + "])", "").Toe0String();
            RowView[8] = "";
            RowView.StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);

        }
        private void Btn_Export_Click(object sender, EventArgs e)
        {
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Các_Phí_Hỗ_Trợ_Có_Con_Nhỏ_" + cbo_Type.Text + ".xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }

                    string Status = "Xuất Excel form " + HeaderControl.Text + " > đường dẫn và tên file xuất:" + FDialog.FileName;
                    Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);

                    GVData.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }

        }
        private void Btn_Import_Click(object sender, EventArgs e)
        {
            Frm_Import_FeeChildren frm = new Frm_Import_FeeChildren();
            frm.ShowDialog();
        }
        private void Btn_AddCategory_Click(object sender, EventArgs e)
        {
            Frm_Fee_Category frm = new Frm_Fee_Category();
            frm.Slug = 2;
            frm.ShowDialog();
            LoadDataToToolbox.KryptonComboBox(cbo_SalarySearch, "SELECT AutoKey,ID+'-'+Name FROM HRM_Fee_Category WHERE RecordStatus <> 99 AND Slug =2  ORDER BY [Rank]", "--Tất cả--");
        }

        #region[Create Info]
        private void Txt_Amount_KeyUp(object sender, KeyEventArgs e)
        {
            double zAmount = 0;
            double.TryParse(txt_Amount.Text.Trim(), out zAmount);
            txt_Amount.Text = zAmount.Ton0String();
            txt_Amount.Focus();
            txt_Amount.SelectionStart = txt_Amount.Text.Length;
        }
        private void SetDefault()
        {
            _Key = 0;
            txt_EmployeeID.Text = "Nhập mã thẻ";
            txt_EmployeeID.Tag = null;
            txt_EmployeeName.Text = "";
            txt_Amount.Text = "0";
            txt_Description.Text = "";
            dte_BrirthDay.Value = SessionUser.Date_Work;
            dte_FromDate.Value = SessionUser.Date_Work;
            dte_ToDate.Value = SessionUser.Date_Work;
            dte_LeavingDate.Value = DateTime.MinValue;
            btn_Copy.Enabled = false;
            btn_Del.Enabled = false;
            lbl_Created.Text = "Tạo bởi:";
            lbl_Modified.Text = "Chỉnh sửa:";
            txt_EmployeeID.Focus();
        }
        private void LoadData()
        {
            Employee_FeeChildren_Info zinfo = new Employee_FeeChildren_Info(_Key);
            txt_EmployeeID.Tag = zinfo.EmployeeKey;
            txt_EmployeeID.Text = zinfo.EmployeeID;
            txt_EmployeeName.Text = zinfo.EmployeeName;
            float zNumber = float.Parse(zinfo.Number.ToString());
            txt_Amount.Text = zNumber.Ton0String();
            txt_Description.Text = zinfo.Description;
            if (zinfo.BirthDay == null)
            {
                dte_BrirthDay.Value = DateTime.MinValue;
                //btn_Copy.Enabled = false;
            }
            else
            {
                dte_BrirthDay.Value = zinfo.BirthDay;
                //btn_Copy.Enabled = true;
            }
            dte_FromDate.Value = zinfo.FromDate;

            if (zinfo.ToDate == null)
            {
                dte_ToDate.Value = DateTime.MinValue;
                //btn_Copy.Enabled = false;
            }
            else
            {
                dte_ToDate.Value = zinfo.ToDate;
                //btn_Copy.Enabled = true;
            }
            if (zinfo.LeavingDate == null)
            {
                dte_LeavingDate.Value = DateTime.MinValue;
            }
            else
            {
                dte_LeavingDate.Value = zinfo.LeavingDate;
            }
            cbo_Salary.SelectedValue = zinfo.CategoryKey;
            btn_Del.Enabled = true;

            lbl_Created.Text = "Tạo bởi:[" + zinfo.CreatedName + "][" + zinfo.CreatedOn + "]";
            lbl_Modified.Text = "Chỉnh sửa:[" + zinfo.ModifiedName + "][" + zinfo.ModifiedOn + "]";
        }
        private void Btn_Save_Click(object sender, EventArgs e)
        {
            if (txt_EmployeeID.Tag == null)
            {
                Utils.TNMessageBoxOK("Chưa nhập mã nhân viên!", 1);
                return;
            }
            if (cbo_Salary.SelectedValue.ToInt() == 0)
            {
                Utils.TNMessageBoxOK("Chọn danh mục!", 1);
                return;
            }
            float Value = 0;
            if (!float.TryParse(txt_Amount.Text.Trim(), out Value))
            {
                Utils.TNMessageBoxOK("Số con không đúng!", 1);
                return;
            }
            else
            {
                Employee_FeeChildren_Info zinfo = new Employee_FeeChildren_Info(_Key);
                zinfo.EmployeeKey = txt_EmployeeID.Tag.ToString();
                zinfo.EmployeeID = txt_EmployeeID.Text.Trim().ToUpper();
                zinfo.EmployeeName = txt_EmployeeName.Text.Trim().ToString();
                zinfo.Number = float.Parse(txt_Amount.Text.Trim());
                zinfo.Description = txt_Description.Text;
                DateTime zBirthDay = DateTime.MinValue;
                if (dte_BrirthDay.Value != DateTime.MinValue)
                {
                    zBirthDay = new DateTime(dte_BrirthDay.Value.Year, dte_BrirthDay.Value.Month, dte_BrirthDay.Value.Day, 0, 0, 0);
                }
                DateTime zFromDate = new DateTime(dte_FromDate.Value.Year, dte_FromDate.Value.Month, dte_FromDate.Value.Day, 0, 0, 0);
                DateTime zToDate = DateTime.MinValue;
                if (dte_ToDate.Value != DateTime.MinValue)
                {
                    zToDate = new DateTime(dte_ToDate.Value.Year, dte_ToDate.Value.Month, dte_ToDate.Value.Day, 23, 59, 59);
                }
                DateTime zLeavingDate = DateTime.MinValue;
                if (dte_LeavingDate.Value != DateTime.MinValue)
                {
                    zLeavingDate = new DateTime(dte_LeavingDate.Value.Year, dte_LeavingDate.Value.Month, dte_LeavingDate.Value.Day, 23, 59, 59);
                }
                zinfo.BirthDay = zBirthDay;
                zinfo.FromDate = zFromDate;
                zinfo.ToDate = zToDate;
                zinfo.LeavingDate = zLeavingDate;

                zinfo.CategoryKey = cbo_Salary.SelectedValue.ToInt();
                string[] s = cbo_Salary.Text.Split('-');
                zinfo.CategoryID = s[0];
                zinfo.CategoryName = s[1];
                zinfo.CreatedBy = SessionUser.UserLogin.Key;
                zinfo.CreatedName = SessionUser.UserLogin.EmployeeName;
                zinfo.ModifiedBy = SessionUser.UserLogin.Key;
                zinfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
                zinfo.Save();
                if (zinfo.Message == "11" || zinfo.Message == "20")
                {
                    Utils.TNMessageBoxOK("Cập nhật thành công!", 3);
                    _Key = zinfo.Key;
                    LoadData();
                }
                else
                {
                    Utils.TNMessageBoxOK(zinfo.Message, 4);
                }
            }


        }
        private void Btn_Del_Click(object sender, EventArgs e)
        {
            if (_Key == 0)
            {
                Utils.TNMessageBoxOK("Chưa chọn thông tin!", 1);
            }
            else
            {
                if (Utils.TNMessageBox("Bạn có chắc xóa thông tin này ?.", 2) == "Y")
                {
                    Employee_FeeChildren_Info zinfo = new Employee_FeeChildren_Info(_Key);
                    zinfo.ModifiedBy = SessionUser.UserLogin.Key;
                    zinfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
                    zinfo.Delete();
                    if (zinfo.Message == "30")
                    {
                        Utils.TNMessageBoxOK("Đã xóa thành công !", 3);
                        SetDefault();
                    }
                    else
                    {
                        Utils.TNMessageBoxOK(zinfo.Message, 4);
                    }

                }
            }

        }
        private void Btn_Copy_Click(object sender, EventArgs e)
        {
            Employee_FeeChildren_Info zinfo = new Employee_FeeChildren_Info(_Key);
            txt_EmployeeID.Tag = zinfo.EmployeeKey;
            txt_EmployeeID.Text = zinfo.EmployeeID;
            txt_EmployeeName.Text = zinfo.EmployeeName;
            float zNumber = float.Parse(zinfo.Number.ToString());
            txt_Amount.Text = zNumber.Ton0String();
            txt_Description.Text = zinfo.Description;
            dte_FromDate.Value = zinfo.FromDate;
            if (zinfo.ToDate == null)
                dte_ToDate.Value = DateTime.MinValue;
            else
                dte_ToDate.Value = zinfo.ToDate;
            if (zinfo.LeavingDate == null)
                dte_LeavingDate.Value = DateTime.MinValue;
            cbo_Salary.SelectedValue = zinfo.CategoryKey;
            _Key = 0;
            lbl_Created.Text = "Tạo bởi:";
            lbl_Modified.Text = "Chỉnh sửa:";
            Utils.TNMessageBoxOK("Sao chép thành công!Vui lòng chỉnh sửa thông tin.", 1);
        }
        private void Txt_EmployeeID_Leave(object sender, EventArgs e)
        {
            if (txt_EmployeeID.Text.Trim().Length < 1)
            {
                txt_EmployeeID.Text = "Nhập mã thẻ";
                txt_EmployeeID.ForeColor = Color.White;
            }
            else
            {
                Employee_Info zinfo = new Employee_Info();
                zinfo.GetEmployee(txt_EmployeeID.Text.Trim());
                if (zinfo.Key == "")
                {
                    txt_EmployeeID.Text = "Nhập mã thẻ";
                    txt_EmployeeID.ForeColor = Color.White;
                    txt_EmployeeID.Tag = null;
                }
                else
                {
                    txt_EmployeeID.Text = zinfo.EmployeeID;
                    txt_EmployeeName.Text = zinfo.FullName;
                    txt_EmployeeID.Tag = zinfo.Key;
                    txt_EmployeeID.BackColor = Color.Black;
                }
            }
        }
        private void Txt_EmployeeID_Enter(object sender, EventArgs e)
        {
            if (txt_EmployeeID.Text.Trim() == "Nhập mã thẻ")
            {
                txt_EmployeeID.Text = "";
                txt_EmployeeID.ForeColor = Color.Brown;
            }
        }

        #endregion
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;

                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                //this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion
        #region [ Get Auth-Action-Chg_Lang ]
        Access_Role_Info Role;
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
                btn_Export.Enabled = false;
            }
            if (Role.RoleAdd == 0 && Role.RoleEdit == 0)
            {
                btn_New.Enabled = false;
                btn_Save.Enabled = false;
                btn_Copy.Enabled = false;
                btn_Import.Enabled = false;
            }
            if (Role.RoleAdd == 0 && Role.RoleEdit == 1)
            {
                btn_New.Enabled = false;
                btn_Save.Enabled = true;
            }
            if (Role.RoleDel == 0)
            {
                btn_Del.Enabled = false;
            }
        }
        #endregion
    
        
    }
}
