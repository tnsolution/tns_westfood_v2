﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TNS.HRM;
using TNS.Misc;
using TNS.HRM;

namespace TNS.WinApp
{
    public partial class Frm_Salary_Worker : Form
    {
        private int _TeamKey = 0;
        private DataTable _Table;
        public Frm_Salary_Worker()
        {
            InitializeComponent();
            GV_Product_Layout();
            btn_Hide_Log.Visible = false;
            btn_Show_Log.Visible = true;
            groupBox6.Visible = false;
            btn_Hide_Log.Click += Btn_Hide_Click;
            btn_Show_Log.Click += Btn_Show_Log_Click;
            btn_Hide_Panel.Visible = false;
            btn_Show_Panel.Visible = true;
            panel1.Visible = false;
            btn_Hide_Panel.Click += Btn_Hide_Panel_Click;
            btn_Show_Panel.Click += Btn_Show_Panel_Click;
            LV_Team_Month_Layout(LV_Teams_Month);
            LV_Teams_Month.Click += LV_Teams_Month_Click;
        }
        private void Frm_Salary_Worker_Load(object sender, EventArgs e)
        {
            LV_Team_Month_LoadData();
        }

        #region ----- Panel Left -----

        #region [Desgin Layout ListView]
        private void LV_Team_Month_Layout(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên Nhóm";
            colHead.Width = 220;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);


        }

        public void LV_Team_Month_LoadData()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LV_Teams_Month;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            DataTable In_Table = new DataTable();

            In_Table = Teams_Data.List();

            LV.Items.Clear();
            int n = In_Table.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.ForeColor = Color.Navy;
                DataRow nRow = In_Table.Rows[i];
                lvi.Tag = nRow["TeamKey"].ToString();
                lvi.BackColor = Color.White;

                lvi.ImageIndex = 0;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["TeamName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);

            }

            this.Cursor = Cursors.Default;

        }
        #endregion

        #region [Process Envent]
        private void LV_Teams_Month_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < LV_Teams_Month.Items.Count; i++)
            {
                if (LV_Teams_Month.Items[i].Selected == true)
                {
                    LV_Teams_Month.Items[i].BackColor = Color.LightBlue; // highlighted item
                }
                else
                {
                    LV_Teams_Month.Items[i].BackColor = SystemColors.Window; // normal item
                }
            }
            _TeamKey = LV_Teams_Month.SelectedItems[0].Tag.ToInt();
            Load_Data_GV();
            LB_Log.Items.Clear();
        }
        #endregion

        #endregion

        private void GV_Product_Layout()
        {
            // Setup Column 
            GV_Employee.Columns.Add("No", "STT");
            GV_Employee.Columns.Add("EmployeeName", "Họ và Tên");
            GV_Employee.Columns.Add("Department", "Bộ phận");
            GV_Employee.Columns.Add("EmployeeID", "Số thẻ");
            GV_Employee.Columns.Add("ATM", "Tài Khoản ATM");
            GV_Employee.Columns.Add("SalaryBasic", "Mức lương chính");
            GV_Employee.Columns.Add("SalaryOverTime", "Tiền lương giãn ca (50%)");
            GV_Employee.Columns.Add("SalaryProduct", "Khoán Sản Phẩm(Ngày Thường)");
            GV_Employee.Columns.Add("SalarySunday", "Khoán Sản Phẩm (Chủ Nhật) 200%");
            GV_Employee.Columns.Add("SalaryHoliday", "Khoán Sản Phẩm (Ngày Lễ) 300%");
            GV_Employee.Columns.Add("SalaryDif", "Khoán Sản Phẩm Khác (Ngày Thường)");
            GV_Employee.Columns.Add("SalaryDifSunday", "Khoản Sản Phẩm Khác (Chủ Nhật) 200%");
            GV_Employee.Columns.Add("SalaryDifHoliday", "Khoán Sản Phẩm Khác (Ngày Lễ) 300%");
            GV_Employee.Columns.Add("MoneyTimeKeep", "Tiền khoán tính chuyên cần");
            GV_Employee.Columns.Add("SalaryTimeKeep", "Tiền chuyên cần");
            GV_Employee.Columns.Add("SalarySupport", "Hỗ trợ");
            GV_Employee.Columns.Add("NumberOffHoliday", "Số ngày P.Năm, Hiếu Hỷ, Lễ Tết,...");
            GV_Employee.Columns.Add("SalaryOffHoliday", "Số tiền P.Năm, Hiếu Hỷ, Lễ Tết,...");
            GV_Employee.Columns.Add("SalaryPayment", "Tiền X.Hàng - BX Đường");
            GV_Employee.Columns.Add("SupportNew", "Hỗ trợ công nhân mới");
            GV_Employee.Columns.Add("ToTal", "Tổng cộng");
            GV_Employee.Columns.Add("SupportMonth", "Hỗ trợ tháng thấp điểm");
            GV_Employee.Columns.Add("SalaryChil", "Số tiền hỗ trợ phụ cấp con nhỏ ( 1 - 6 tuổi)");
            GV_Employee.Columns.Add("NumberRice", "Số phần cơm");
            GV_Employee.Columns.Add("SalaryRice", "Số tiền cơm");
            GV_Employee.Columns.Add("ToTalDate", "Tổng ngày công");
            GV_Employee.Columns.Add("AddvancePayment", "Hoàn ứng (Trừ Thu Nhập)");
            GV_Employee.Columns.Add("Receipt", "Tiền X.Hàng - BX Đường (Trừ Thu Nhập)");
            GV_Employee.Columns.Add("BHXH", "BHXH (Trừ Thu Nhập)");
            GV_Employee.Columns.Add("BHYT", "BHYT (Trừ Thu Nhập)");
            GV_Employee.Columns.Add("BHTN", "BHTN (Trừ Thu Nhập)");
            GV_Employee.Columns.Add("CĐ", "Công Đoàn (Trừ Thu Nhập)");
            GV_Employee.Columns.Add("SalaryTexas", "Thu nhập chịu thuế (Trừ Thu Nhập)");
            GV_Employee.Columns.Add("Texas", "Thuế TN (Trừ Thu Nhập)");
            GV_Employee.Columns.Add("TLV", "Tấm Lòng Vàng (Trừ Thu Nhập)");
            GV_Employee.Columns.Add("DayOff", "Ngày nghỉ bị trừ tiền cơm (Trừ Thu Nhập)");
            GV_Employee.Columns.Add("SalaryDayOff", "Số tiền cơm bị trừ (Trừ Thu Nhập)");
            GV_Employee.Columns.Add("RealLeaders", "Thực Lãnh");

            #region
            GV_Employee.Columns["No"].Width = 40;
            GV_Employee.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV_Employee.Columns["No"].ReadOnly = true;

            GV_Employee.Columns["EmployeeName"].Width = 300;
            GV_Employee.Columns["EmployeeName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV_Employee.Columns["Department"].Width = 80;
            GV_Employee.Columns["Department"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV_Employee.Columns["EmployeeID"].Width = 80;
            GV_Employee.Columns["EmployeeID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV_Employee.Columns["EmployeeID"].Frozen = true;

            GV_Employee.Columns["ATM"].Width = 120;
            GV_Employee.Columns["ATM"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV_Employee.Columns["SalaryBasic"].Width = 100;
            GV_Employee.Columns["SalaryBasic"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee.Columns["SalaryOverTime"].Width = 100;
            GV_Employee.Columns["SalaryOverTime"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee.Columns["SalaryProduct"].Width = 140;
            GV_Employee.Columns["SalaryProduct"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee.Columns["SalaryProduct"].Width = 130;
            GV_Employee.Columns["SalaryProduct"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee.Columns["SalarySunday"].Width = 120;
            GV_Employee.Columns["SalarySunday"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee.Columns["SalaryHoliday"].Width = 120;
            GV_Employee.Columns["SalaryHoliday"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee.Columns["SalaryDif"].Width = 140;
            GV_Employee.Columns["SalaryDif"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee.Columns["SalaryDifSunday"].Width = 145;
            GV_Employee.Columns["SalaryDifSunday"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee.Columns["SalaryDifHoliday"].Width = 140;
            GV_Employee.Columns["SalaryDifHoliday"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee.Columns["MoneyTimeKeep"].Width = 140;
            GV_Employee.Columns["MoneyTimeKeep"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee.Columns["SalaryTimeKeep"].Width = 100;
            GV_Employee.Columns["SalaryTimeKeep"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee.Columns["SalarySupport"].Width = 100;
            GV_Employee.Columns["SalarySupport"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee.Columns["NumberOffHoliday"].Width = 200;
            GV_Employee.Columns["NumberOffHoliday"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee.Columns["SalaryOffHoliday"].Width = 200;
            GV_Employee.Columns["SalaryOffHoliday"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee.Columns["SalaryPayment"].Width = 100;
            GV_Employee.Columns["SalaryPayment"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee.Columns["SupportNew"].Width = 100;
            GV_Employee.Columns["SupportNew"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee.Columns["ToTal"].Width = 100;
            GV_Employee.Columns["ToTal"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee.Columns["SupportMonth"].Width = 100;
            GV_Employee.Columns["SupportMonth"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee.Columns["SalaryChil"].Width = 190;
            GV_Employee.Columns["SalaryChil"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee.Columns["NumberRice"].Width = 100;
            GV_Employee.Columns["NumberRice"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee.Columns["SalaryRice"].Width = 100;
            GV_Employee.Columns["SalaryRice"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee.Columns["ToTalDate"].Width = 100;
            GV_Employee.Columns["ToTalDate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee.Columns["AddvancePayment"].Width = 130;
            GV_Employee.Columns["AddvancePayment"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee.Columns["Receipt"].Width = 130;
            GV_Employee.Columns["Receipt"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee.Columns["BHXH"].Width = 130;
            GV_Employee.Columns["BHXH"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee.Columns["BHYT"].Width = 130;
            GV_Employee.Columns["BHYT"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee.Columns["BHTN"].Width = 130;
            GV_Employee.Columns["BHTN"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee.Columns["CĐ"].Width = 130;
            GV_Employee.Columns["CĐ"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee.Columns["SalaryTexas"].Width = 130;
            GV_Employee.Columns["SalaryTexas"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee.Columns["Texas"].Width = 130;
            GV_Employee.Columns["Texas"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee.Columns["TLV"].Width = 130;
            GV_Employee.Columns["TLV"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee.Columns["DayOff"].Width = 130;
            GV_Employee.Columns["DayOff"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee.Columns["SalaryDayOff"].Width = 130;
            GV_Employee.Columns["SalaryDayOff"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee.Columns["RealLeaders"].Width = 150;
            GV_Employee.Columns["RealLeaders"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;



            #endregion

            GV_Employee.BackgroundColor = Color.White;
            GV_Employee.GridColor = Color.FromArgb(227, 239, 255);
            GV_Employee.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV_Employee.DefaultCellStyle.ForeColor = Color.Navy;
            GV_Employee.DefaultCellStyle.Font = new Font("Arial", 9);
            GV_Employee.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(0, 90, 200);
            GV_Employee.ColumnHeadersDefaultCellStyle.ForeColor = Color.FromArgb(206, 246, 255);
            GV_Employee.EnableHeadersVisualStyles = false;
            GV_Employee.ColumnHeadersHeight = 70;

            GV_Employee.AllowUserToResizeRows = false;
            GV_Employee.AllowUserToResizeColumns = true;

            GV_Employee.RowHeadersVisible = false;

            //// setup Height Header
            // GV_Employees.ColumnHeadersHeight = GV_Employees.ColumnHeadersHeight * 3 / 2;
            GV_Employee.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            GV_Employee.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;

            foreach (DataGridViewColumn column in GV_Employee.Columns)
            {
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
                column.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            }

            for (int i = 1; i <= 8; i++)
            {
                GV_Employee.Rows.Add();
            }
        }
        private void Load_Data_GV()
        {
            int i = 0;
            int zFind = 0;
            GV_Employee.Rows.Clear();
            _Table = Salary_Product_Worker_Data.List_Team_Month(_TeamKey, dte_Month.Value);

            foreach (DataRow nRow in _Table.Rows)
            {
                GV_Employee.Rows.Add();
                DataGridViewRow nRowView = GV_Employee.Rows[i];
                nRowView.Cells["No"].Value = (i + 1).ToString();
                nRowView.Cells["EmployeeID"].Tag = nRow["EmployeeKey"].ToString().Trim();
                nRowView.Cells["EmployeeName"].Value = nRow["EmployeeName"].ToString().Trim();
                nRowView.Cells["EmployeeID"].Value = nRow["EmployeeID"].ToString().Trim();
                nRowView.Cells["ATM"].Value = nRow["ATM"].ToString().Trim();

                #region ------ Lương Căn Bản
                double zSalaryBasic = 0;
                if (nRow["Salary_Basic"].ToString() != "")
                {
                    zSalaryBasic = double.Parse(nRow["Salary_Basic"].ToString());
                    nRowView.Cells["SalaryBasic"].Value = zSalaryBasic.ToString("#,###,###");
                }
                else
                {
                    nRowView.Cells["SalaryBasic"].Value = "0";
                }
                #endregion

                double zSalaryOvertime = double.Parse(nRow["SalaryOvertime"].ToString());
                nRowView.Cells["SalaryOvertime"].Value = zSalaryOvertime.ToString("#,###,###");
                double zSalaryProduct = double.Parse(nRow["SalaryProduct"].ToString());
                nRowView.Cells["SalaryProduct"].Value = zSalaryProduct.ToString("#,###,###");
                double zSalarySunday = double.Parse(nRow["SalarySunday"].ToString());
                nRowView.Cells["SalarySunday"].Value = zSalarySunday.ToString("#,###,###");
                double zSalaryHoliday = double.Parse(nRow["SalaryHoliday"].ToString());
                nRowView.Cells["SalaryHoliday"].Value = zSalaryHoliday.ToString("#,###,###");
                double zSalaryDif = double.Parse(nRow["SalaryDif"].ToString());
                nRowView.Cells["SalaryDif"].Value = zSalaryDif.ToString("#,###,###");
                double zSalaryDifSunday = double.Parse(nRow["SalaryDifSunday"].ToString());
                nRowView.Cells["SalaryDifSunday"].Value = zSalaryDifSunday.ToString("#,###,###");
                double zSalaryDifHoliday = double.Parse(nRow["SalaryDifHoliday"].ToString());
                nRowView.Cells["SalaryDifHoliday"].Value = zSalaryDifHoliday.ToString("#,###,###");
                double zMoneyTimeKeep = double.Parse(nRow["MoneyTimeKeep"].ToString());
                nRowView.Cells["MoneyTimeKeep"].Value = zMoneyTimeKeep.ToString("#,###,###");


                #region ------ Lương Chuyên Cần
                double zSalaryTimeKeep = 0;
                int zDateStandard = nRow["NCChuan"].ToInt();
                int zDateWorking = nRow["NCThucTe"].ToInt();
                int zHeSo10 = nRow["HeSo10"].ToInt();
                int zHeSo5 = nRow["HeSo5"].ToInt();
                double zMucTran = nRow["MucTran"].ToInt();
                if (zDateStandard - zDateWorking <= 1)
                {
                    zSalaryTimeKeep = (zHeSo10 * zMoneyTimeKeep) / 100;
                    if (zSalaryTimeKeep > 600000)
                    {
                        nRowView.Cells["SalaryTimeKeep"].Value = zMucTran.ToString("#,###,###");
                    }
                    else
                    {
                        nRowView.Cells["SalaryTimeKeep"].Value = zSalaryTimeKeep.ToString("#,###,###");
                    }
                }
                if (zDateStandard - zDateWorking > 1 && zDateStandard - zDateWorking < 3)
                {
                    zSalaryTimeKeep = (zHeSo5 * zMoneyTimeKeep) / 100;
                    if (zSalaryTimeKeep > 600000)
                    {
                        nRowView.Cells["SalaryTimeKeep"].Value = zMucTran.ToString("#,###,###");
                    }
                    else
                    {
                        nRowView.Cells["SalaryTimeKeep"].Value = zSalaryTimeKeep.ToString("#,###,###");
                    }
                }
                if (zDateStandard - zDateWorking > 3)
                {
                    nRowView.Cells["SalaryTimeKeep"].Value = "0";
                }
                #endregion

                double zSalarySupport = double.Parse(nRow["SalarySupport"].ToString());
                nRowView.Cells["SalarySupport"].Value = zSalarySupport.ToString("#,###,###");
                float zNumberOffHoiday = float.Parse(nRow["NumberOffHoliday"].ToString());
                nRowView.Cells["NumberOffHoliday"].Value = zNumberOffHoiday.ToFloat();
                double zSalaryOffHoliday = double.Parse(nRow["SalaryOffHoliday"].ToString());
                nRowView.Cells["SalaryOffHoliday"].Value = zSalaryOffHoliday.ToString("#,###,###");

                #region ------ Xuất Hàng - Bốc Xếp Đường
                double zSalaryPayment = 0;
                if (nRow["XuatHang"].ToString() != "")
                {
                    zSalaryPayment = double.Parse(nRow["XuatHang"].ToString());
                }
                else
                {
                    zSalaryPayment = 0;
                }
                nRowView.Cells["SalaryPayment"].Value = zSalaryPayment.ToString("#,###,###");
                #endregion

                double zSupportNew = double.Parse(nRow["SupportNew"].ToString());
                nRowView.Cells["SupportNew"].Value = zSupportNew.ToString("#,###,###");
                double zToTal = double.Parse(nRow["ToTal"].ToString());
                nRowView.Cells["ToTal"].Value = zToTal.ToString("#,###,###");
                double zSupportMonth = double.Parse(nRow["SupportMonth"].ToString());
                nRowView.Cells["SupportMonth"].Value = zSupportMonth.ToString("#,###,###");

                #region ------ Hỗ Trợ Con Nhỏ
                double zSalaryChil = 0;
                if (nRow["HoTroConNho"].ToString() != "")
                {
                    zSalaryChil = double.Parse(nRow["HoTroConNho"].ToString());

                }
                else
                {
                    zSalaryChil = 0;
                }
                nRowView.Cells["SalaryChil"].Value = zSalaryChil.ToString("#,###,###");
                #endregion

                int zNumberRice = int.Parse(nRow["NumberRice"].ToString());
                nRowView.Cells["NumberRice"].Value = zNumberRice.ToString();
                double zSalaryRice = double.Parse(nRow["SalaryRice"].ToString());
                nRowView.Cells["SalaryRice"].Value = zSalaryRice.ToString("#,###,###");
                int zToTalDate = int.Parse(nRow["NgayCong"].ToString());
                nRowView.Cells["ToTalDate"].Value = zToTalDate;

                #region ------ Hoàn ứng
                double zAddvancePayment = 0;
                if (nRow["HoanUng"].ToString() != "")
                {
                    zAddvancePayment = double.Parse(nRow["HoanUng"].ToString());
                }
                else
                {
                    zAddvancePayment = 0;
                }
                nRowView.Cells["AddvancePayment"].Value = zAddvancePayment.ToString("#,###,###");
                #endregion

                #region  ------ Xuất Hàng - Bốc Xếp (Trừ Thu Nhập)
                double zReceipt = 0;
                if (nRow["XuatHang"].ToString() != "")
                {
                    zReceipt = double.Parse(nRow["XuatHang"].ToString());
                }
                else
                {
                    zReceipt = 0;
                }
                nRowView.Cells["Receipt"].Value = zReceipt.ToString("#,###,###");
                #endregion

                #region ------ BH Xã Hội
                float zBHXaHoi = float.Parse(nRow["BHXaHoi"].ToString());
                double zBHXH = 0;
                if (zSalaryBasic > 0)
                {
                    zBHXH = (zSalaryBasic * zBHXaHoi) / 100;
                    nRowView.Cells["BHXH"].Value = zBHXH.ToString("#,###,###");
                }
                else
                {
                    zBHXH = 0;
                    nRowView.Cells["BHXH"].Value = zBHXH.ToString("#,###,###");
                }
                #endregion

                #region ------ BH Y Tế
                float zBHYTe = float.Parse(nRow["BHYTe"].ToString());
                double zBHYT = 0;
                if (zSalaryBasic > 0)
                {
                    zBHYT = (zSalaryBasic * zBHYTe) / 100;
                    nRowView.Cells["BHYT"].Value = zBHYT.ToString("#,###,###");
                }
                else
                {
                    zBHYT = 0;
                    nRowView.Cells["BHYT"].Value = zBHYT.ToString("#,###,###");
                }
                #endregion

                #region ------ BH Tai Nạn
                float zBHTNan = float.Parse(nRow["BHTaiNan"].ToString());
                double zBHTN = 0;
                if (zSalaryBasic > 0)
                {
                    zBHTN = (zSalaryBasic * zBHTNan) / 100;
                    nRowView.Cells["BHTN"].Value = zBHTN.ToString("#,###,###");
                }
                else
                {
                    zBHTN = 0;
                    nRowView.Cells["BHTN"].Value = zBHTN.ToString("#,###,###");
                }
                #endregion

                double zCD = double.Parse(nRow["CD"].ToString());
                nRowView.Cells["CĐ"].Value = zCD.ToString();
                double zSalaryTexas = double.Parse(nRow["SalaryTexas"].ToString());
                nRowView.Cells["SalaryTexas"].Value = zSalaryTexas.ToString("#,###,###");
                double zTexas = double.Parse(nRow["Texas"].ToString());
                nRowView.Cells["Texas"].Value = zTexas.ToString("#,###,###");
                double zTLV = double.Parse(nRow["TLV"].ToString());
                nRowView.Cells["TLV"].Value = zTLV.ToString("#,###,###");
                float zDayOff = float.Parse(nRow["DayOFf"].ToString());
                nRowView.Cells["DayOFf"].Value = zDayOff.ToFloat();
                double zSalaryDayOff = double.Parse(nRow["SalaryDayOff"].ToString());
                nRowView.Cells["SalaryDayOff"].Value = zSalaryDayOff.ToString("#,###,###");
                double zRealLeaders = double.Parse(nRow["RealLeader"].ToString());
                nRowView.Cells["RealLeaders"].Value = zRealLeaders.ToString("#,###,###");

                i++;
            }
        }

        #region [Process Event]
        private void Btn_Show_Panel_Click(object sender, EventArgs e)
        {
            panel1.Visible = true;
            btn_Hide_Panel.Visible = true;
            btn_Show_Panel.Visible = false;
        }
        private void Btn_Hide_Panel_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
            btn_Hide_Panel.Visible = false;
            btn_Show_Panel.Visible = true;
        }
        private void Btn_Show_Log_Click(object sender, EventArgs e)
        {
            groupBox6.Visible = true;
            btn_Hide_Log.Visible = true;
            btn_Show_Log.Visible = false;
        }
        private void Btn_Hide_Click(object sender, EventArgs e)
        {
            groupBox6.Visible = false;
            btn_Hide_Log.Visible = false;
            btn_Show_Log.Visible = true;
        }
        #endregion
    }
}
