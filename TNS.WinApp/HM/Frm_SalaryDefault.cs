﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.HRM;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_SalaryDefault : Form
    {
        private int _DepartmentKey = 0;
        private int _TeamKey = 0;
        private string _CategoryID = "";
        DataTable ztbType = new DataTable();
        public Frm_SalaryDefault()
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            Utils.DoubleBuffered(GVCategory, true);
            Utils.DrawGVStyle(ref GVCategory);
            Utils.DoubleBuffered(GVData, true);
            Utils.DrawGVStyle(ref GVData);
            Utils.DoubleBuffered(GV_Department, true);
            Utils.DrawGVStyle(ref GV_Department);
            Utils.DoubleBuffered(GVTeam, true);
            Utils.DrawGVStyle(ref GVTeam);

            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;
            btnClose.Click += btnClose_Click;
            btn_Import.Click += Btn_Import_Click;
            btn_Add.Click += Btn_Save_Click;
            GV_Department.Click += GVDepartment_Click;
            GVTeam.Click += GVTeam_Click;
            GVData.CellEndEdit += GVData_CellEndEdit;
            GVCategory.Click += GVCategory_Click;
            cbo_Type.SelectedIndexChanged += Cbo_Type_SelectedIndexChanged;
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            InitGV_Layout(GVData);
            InitLayout_GVDepartment(GV_Department);
            InitLayout_GVTeam(GVTeam);
            InitLayout_GVCategory(GVCategory);
            btn_Hide.Click += Btn_Hide_Click;
            btn_Done.Click += Btn_Done_Click;
            btnClose_Panel_Message.Click += BtnClose_Panel_Message_Click;
        }

        private void BtnClose_Panel_Message_Click(object sender, EventArgs e)
        {
            Btn_Hide_Click(null, e);
        }

        private void Frm_SalaryDefault_Load(object sender, EventArgs e)
        {
            DataTable zTable = Employee_SalaryDefault_Data.ListDepartment();
            InitData_GVDepartment(GV_Department, zTable);
            DataTable zTableFee = Employee_SalaryDefault_Data.ListCategory();
            InitData_GVCategory(GVCategory, zTableFee);
            LoadTableType();
            LoadDataToToolbox.ComboBoxData(cbo_Type.ComboBox, ztbType, false, 0, 1);
            Panel_Done.Visible = false;
            dte_Exp.Value = SessionUser.Date_Work ;
        }
        void LoadTableType()
        {
            ztbType = new DataTable();
            ztbType.Columns.Add("Value");
            ztbType.Columns.Add("Name");
            ztbType.Rows.Add("0", "--Tất cả--");
            ztbType.Rows.Add("1", "Chưa xác định hết hạn");
            ztbType.Rows.Add("2", "Đã xác định thời hạn");
        }
        private void Cbo_Type_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadData();
        }
        private void InitLayout_GVCategory(DataGridView GV)
        {
            // Setup Column 
            GV.Columns.Add("No", "STT");
            GV.Columns.Add("CategoryID", "Mã");
            GV.Columns.Add("CategoryName", "Tên");
            GV.Columns["No"].Width = 40;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns["CategoryID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["CategoryID"].Width = 100;
            GV.Columns["CategoryName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["CategoryName"].Width = 300;
        }
        private void InitData_GVCategory(DataGridView GV, DataTable Table)
        {
            GV.Rows.Clear();
            if (Table != null)
            {
                for (int i = 0; i < Table.Rows.Count; i++)
                {
                    DataRow r = Table.Rows[i];
                    GV.Rows.Add();
                    DataGridViewRow GvRow = GV.Rows[i];
                    GvRow.Cells["No"].Value = (i + 1).ToString();
                    GvRow.Cells["CategoryID"].Tag = r["CategoryID"].ToString();
                    GvRow.Cells["CategoryID"].Value = r["CategoryID"].ToString();
                    GvRow.Cells["CategoryName"].Value = r["CategoryName"].ToString();
                }
            }
            GV.ClearSelection();
        }
        private void GVCategory_Click(object sender, EventArgs e)
        {
            if (GVCategory.SelectedRows.Count > 0 &&
                GVCategory.CurrentRow.Cells["CategoryID"].Tag != null)
            {
                _CategoryID = GVCategory.CurrentRow.Cells["CategoryID"].Value.ToString();
                LoadData();
            }
        }
        private void LoadData()
        {
            DataTable zTable = Employee_SalaryDefault_Data.List(_TeamKey, _DepartmentKey, _CategoryID, cbo_Type.SelectedValue.ToInt());
            if (zTable.Rows.Count > 0)
                InitGV_Data(GVData, zTable);
            else
                GVData.Rows.Clear();
        }
        private void GVDepartment_Click(object sender, EventArgs e)
        {
            if (GV_Department.SelectedRows.Count > 0 &&
                GV_Department.CurrentRow.Cells["DepartmentID"].Tag != null)
            {
                //Clear các bảng con khi chọn lại
                _DepartmentKey = 0;
                _DepartmentKey = int.Parse(GV_Department.CurrentRow.Cells["DepartmentID"].Tag.ToString());
                _TeamKey = 0;
                DataTable zTable = Employee_FeeDefault_Data.ListTeam(_DepartmentKey);
                InitData_GVTeam(GVTeam, zTable);
                LoadData();
            }
        }
        private void GVTeam_Click(object sender, EventArgs e)
        {
            if (GVTeam.SelectedRows.Count > 0 &&
                GVTeam.CurrentRow.Cells["TeamID"].Tag != null)
            {
                _TeamKey = 0;
                _TeamKey = int.Parse(GVTeam.CurrentRow.Cells["TeamID"].Tag.ToString());
                LoadData();
            }
        }
        private void Cbo_Fee_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadData();
        }
        private void InitLayout_GVDepartment(DataGridView GV)
        {
            // Setup Column 
            GV.Columns.Add("No", "STT");
            GV.Columns.Add("DepartmentID", "Mã");
            GV.Columns.Add("DepartmentName", "Tên");

            GV.Columns["No"].Width = 40;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns["DepartmentID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["DepartmentName"].Width = 200;
            GV.Columns["DepartmentName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["DepartmentName"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }
        private void InitData_GVDepartment(DataGridView GV, DataTable Table)
        {
            GV.Rows.Clear();
            if (Table != null)
            {
                for (int i = 0; i < Table.Rows.Count; i++)
                {
                    DataRow r = Table.Rows[i];
                    GV.Rows.Add();
                    DataGridViewRow GvRow = GV.Rows[i];
                    GvRow.Cells["No"].Value = (i + 1).ToString();
                    GvRow.Cells["DepartmentID"].Tag = r["DepartmentKey"].ToString();
                    GvRow.Cells["DepartmentID"].Value = r["DepartmentID"].ToString();
                    GvRow.Cells["DepartmentName"].Value = r["DepartmentName"].ToString();
                }
            }
            GV.ClearSelection();
        }
        //Lay out Listview List công việc
        private void InitLayout_GVTeam(DataGridView GV)
        {
            // Setup Column 
            GV.Columns.Add("No", "STT");
            GV.Columns.Add("TeamID", "Mã");
            GV.Columns.Add("TeamName", "Tên");

            GV.Columns["No"].Width = 40;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns["TeamID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["TeamName"].Width = 200;
            GV.Columns["TeamName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["TeamName"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }
        private void InitData_GVTeam(DataGridView GV, DataTable Table)
        {
            GV.Rows.Clear();
            if (Table != null)
            {
                for (int i = 0; i < Table.Rows.Count; i++)
                {
                    DataRow r = Table.Rows[i];
                    GV.Rows.Add();
                    DataGridViewRow GvRow = GV.Rows[i];
                    GvRow.Cells["No"].Value = (i + 1).ToString();
                    GvRow.Cells["TeamID"].Tag = r["TeamKey"].ToString();
                    GvRow.Cells["TeamID"].Value = r["TeamID"].ToString();
                    GvRow.Cells["TeamName"].Value = r["TeamName"].ToString();
                }
            }
            GV.ClearSelection();
        }
        private void Btn_Import_Click(object sender, EventArgs e)
        {
            Frm_Import_SalaryDefault frm = new Frm_Import_SalaryDefault();
            frm.ShowDialog();
            DataTable zTableFee = Employee_SalaryDefault_Data.ListCategory();
            InitData_GVCategory(GVCategory, zTableFee);
        }
        void InitGV_Layout(DataGridView GV)
        {
            GVData.Rows.Clear();
            GVData.Columns.Clear();

            // Setup Column 
            GV.Columns.Add("No", "STT");
            GV.Columns.Add("EmployeeName", "HỌ VÀ TÊN");
            GV.Columns.Add("EmployeeID", "SỐ THẺ");
            GV.Columns.Add("Value", "Số tiền");
            GV.Columns.Add("FromDate", "Từ ngày");
            GV.Columns.Add("ToDate", "Đến ngày");
            GV.Columns.Add("Description", "Ghi chú");
            GV.Columns.Add("Message", "Thông báo");

            #region
            GV.Columns["No"].Width = 40;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV.Columns["No"].Frozen = true;
            GV.Columns["No"].ReadOnly = true;

            GV.Columns["EmployeeName"].Width = 200;
            GV.Columns["EmployeeName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["EmployeeName"].Frozen = true;

            GV.Columns["EmployeeID"].Width = 80;
            GV.Columns["EmployeeID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["EmployeeID"].Frozen = true;

            GV.Columns["Value"].Width = 100;
            GV.Columns["Value"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns["FromDate"].Width = 100;
            GV.Columns["FromDate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["ToDate"].Width = 100;
            GV.Columns["ToDate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["Description"].Width = 150;
            GV.Columns["Description"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["Message"].Width = 150;
            GV.Columns["Message"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.ColumnHeadersHeight = 45;
            #endregion
        }
        void InitGV_Data(DataGridView GV, DataTable zTable)
        {
            int no = 0;
            GVData.Rows.Clear();
            foreach (DataRow nRow in zTable.Rows)
            {
                GV.Rows.Add();
                DataGridViewRow nRowView = GV.Rows[no];
                nRowView.Tag = nRow["AutoKey"].ToString().Trim();
                nRowView.Cells["No"].Value = (no + 1).ToString();
                nRowView.Cells["EmployeeName"].Value = nRow["EmployeeName"].ToString().Trim();
                nRowView.Cells["EmployeeID"].Value = nRow["EmployeeID"].ToString().Trim();
                float zValue = 0;
                if (nRow["Value"].ToString().Trim() != "")
                {
                    zValue = float.Parse(nRow["Value"].ToString().Trim());
                }
                nRowView.Cells["Value"].Value = zValue.ToString("###,###,##0");
                if (nRow["FromDate"].ToString() == "")
                {
                    nRowView.Cells["FromDate"].Value = "";
                }
                else
                {
                    DateTime zFromDate = DateTime.Parse(nRow["FromDate"].ToString());
                    nRowView.Cells["FromDate"].Value = zFromDate.ToString("dd/MM/yyyy");
                }

                if (nRow["ToDate"].ToString() == "")
                {
                    nRowView.Cells["ToDate"].Value = "";
                }
                else
                {
                    DateTime zToDate = DateTime.Parse(nRow["ToDate"].ToString());
                    nRowView.Cells["ToDate"].Value = zToDate.ToString("dd/MM/yyyy");
                    nRowView.DefaultCellStyle.BackColor = Color.LightBlue;
                }
                nRowView.Cells["Description"].Value = nRow["Description"].ToString().Trim();
                nRowView.Cells["Message"].Tag = 0; // 0 là không có sửa, 1 là đã có sửa
                no++;
            }
        }
        private void GVData_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow zRowEdit = GVData.Rows[e.RowIndex];
            if (GVData.Rows[e.RowIndex].Tag != null && e.ColumnIndex == 3)
            {
                zRowEdit.Cells["Message"].Tag = 1;
                if (zRowEdit.Cells["Value"].Value == null)
                    zRowEdit.Cells["Value"].Value = "0";
                else
                {
                    float zValue = 0;
                    if (!float.TryParse(zRowEdit.Cells["Value"].Value.ToString(), out zValue))
                    {
                        zRowEdit.Cells["Value"].Value = "0";
                    }
                }
            }
            if (GVData.Rows[e.RowIndex].Tag != null && e.ColumnIndex == 4)
            {
                zRowEdit.Cells["Message"].Tag = 1;
                if (zRowEdit.Cells["FromDate"].Value == null)
                    zRowEdit.Cells["FromDate"].Value = "";
                else
                {
                    DateTime zFromDate = DateTime.MinValue;
                    if (!DateTime.TryParse(zRowEdit.Cells["FromDate"].Value.ToString(), out zFromDate))
                    {
                        zRowEdit.Cells["FromDate"].Value = "";
                    }
                }
            }
            if (GVData.Rows[e.RowIndex].Tag != null && e.ColumnIndex == 5)
            {
                zRowEdit.Cells["Message"].Tag = 1;
                if (zRowEdit.Cells["ToDate"].Value == null)
                    zRowEdit.Cells["ToDate"].Value = "";
                else
                {
                    DateTime zToDate = DateTime.MinValue;
                    if (!DateTime.TryParse(zRowEdit.Cells["ToDate"].Value.ToString(), out zToDate))
                    {
                        zRowEdit.Cells["ToDate"].Value = "";
                    }
                }
            }
            if (GVData.Rows[e.RowIndex].Tag != null && e.ColumnIndex == 6)
            {
                zRowEdit.Cells["Message"].Tag = 1;
                if (zRowEdit.Cells["Description"].Value == null)
                {
                    zRowEdit.Cells["Description"].Value = "";
                }
            }
        }
        private void Btn_Save_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            if (GVData.Rows.Count == 0)
            {
                MessageBox.Show("Không tìm thấy dữ liệu");
                this.Cursor = Cursors.Default;
            }
            else
            {
                Save();
            }

        }
        void Save()
        {
            int zErr = 0;
            for (int i = 0; i < GVData.Rows.Count; i++)
            {
                if (GVData.Rows[i].Tag != null && GVData.Rows[i].Cells["EmployeeID"].Value != null && GVData.Rows[i].Cells["Message"].Tag.ToInt() == 1)
                {
                    Employee_SalaryDefault_Info zinfo = new Employee_SalaryDefault_Info(GVData.Rows[i].Tag.ToInt());
                    zinfo.Value = GVData.Rows[i].Cells["Value"].Value.ToFloat();
                    DateTime zFromDate = DateTime.MinValue;
                    if (GVData.Rows[i].Cells["FromDate"].Value.ToString().Trim() != "")
                    {
                        zFromDate = DateTime.Parse(GVData.Rows[i].Cells["FromDate"].Value.ToString().Trim());
                    }
                    DateTime zToDate = DateTime.MinValue;
                    if (GVData.Rows[i].Cells["ToDate"].Value.ToString().Trim() != "")
                    {
                        zToDate = DateTime.Parse(GVData.Rows[i].Cells["ToDate"].Value.ToString().Trim());
                    }
                    zinfo.FromDate = zFromDate;
                    zinfo.ToDate = zToDate;
                    zinfo.Description = GVData.Rows[i].Cells["Description"].Value.ToString().Trim();
                    zinfo.ModifiedBy = SessionUser.UserLogin.Key;
                    zinfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
                    zinfo.Update();
                    if (zinfo.Message == "20")
                    {
                        GVData.Rows[i].Cells["Message"].Value = "Cập nhật thành công!";
                    }
                    else
                    {
                        GVData.Rows[i].Cells["Message"].Value = "Lỗi.Vui lòng liên hệ IT!";
                        zErr++;
                    }
                }
            }
            if (zErr == 0)
            {
                MessageBox.Show("Cập nhật thành công");
                this.Cursor = Cursors.Default;
            }
            else
            {
                MessageBox.Show("Cập nhật còn " + zErr.ToString() + " lỗi.Vui lòng kiểm tra lại");
                this.Cursor = Cursors.Default;
            }
        }

        //Panel_Done
        private void Btn_Hide_Click(object sender, EventArgs e)
        {
            if (btn_Hide.Tag.ToInt() == 0)
            {
                Panel_Done.Visible = true;
                btn_Hide.Tag = 1;
            }
            else
            {
                Panel_Done.Visible = false;
                btn_Hide.Tag = 0;
            }
        }
        private void Btn_Done_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            if (GVData.Rows.Count == 0)
            {
                MessageBox.Show("Không tìm thấy dữ liệu");
                Btn_Hide_Click(null, e);
                this.Cursor = Cursors.Default;
            }
            else
            {
                Save_Done();
                Btn_Hide_Click(null, e);
            }
        }
        void Save_Done()
        {
            int zErr = 0;
            for (int i = 0; i < GVData.Rows.Count; i++)
            {
                if (GVData.Rows[i].Tag != null && GVData.Rows[i].Cells["EmployeeID"].Value != null && GVData.Rows[i].Cells["ToDate"].ToString() == "")
                {
                    Employee_SalaryDefault_Info zinfo = new Employee_SalaryDefault_Info(GVData.Rows[i].Tag.ToInt());
                    DateTime zToDate = DateTime.MinValue;
                    zToDate = dte_Exp.Value;
                    zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
                    zinfo.ToDate = zToDate;
                    zinfo.ModifiedBy = SessionUser.UserLogin.Key;
                    zinfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
                    zinfo.Update();
                    if (zinfo.Message == "20")
                    {
                        GVData.Rows[i].Cells["Message"].Value = "Cập nhật thành công!";
                    }
                    else
                    {
                        GVData.Rows[i].Cells["Message"].Value = "Lỗi.Vui lòng liên hệ IT!";
                        zErr++;
                    }
                }
            }
            if (zErr == 0)
            {
                MessageBox.Show("Cập nhật thành công");
                this.Cursor = Cursors.Default;
            }
            else
            {
                MessageBox.Show("Cập nhật còn " + zErr.ToString() + " lỗi.Vui lòng kiểm tra lại");
                this.Cursor = Cursors.Default;
            }
        }
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}
