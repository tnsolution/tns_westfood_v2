﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TN.Library.System;
using TNS.CORE;
using TNS.HRM;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;
//Type dùng để phân biệt mã cơm và mã công
//Type=1: định nghia cham cong;
//Type = 2: định nghia cơm;
namespace TNS.WinApp
{
    public partial class Frm_Calculator_TimeKeeping : Form
    {
        #region [Khai Báo Biến]    
        private DataTable _TableTeam;
        private DataTable _ReportCode;
        private int _TotalTeam = 0;
        private int _IndexTeam = 0;
        private int _TeamKey = 0;
        private string _TeamID = "";
        private string _TeamName = "";
        int _Circle = 0;
        private DataTable _TableEmployee;
        private int _TotalEmployee = 0;
        private int _IndexEmployee = 0;
        TimeKeeping_Month_Info _Month_Info;
        List<TimeKeeping_Month_Info> _ListEmployee;
        int _Key = 0;
        string _ID = "";
        string _Name = "";
        float _Number = 0;
        int _Type = 0;
        string _TypeName = "";
        int _CategoryKey = 0;
        string _CategoryName = "";
        int _Rank = 0;

        float _NCQD = 0;
        float _NCTT = 0;
        string _Text = "";
        private string _Log_Detail = "";
        private string _txtLog = "";
        #endregion
        public Frm_Calculator_TimeKeeping()
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            Utils.DoubleBuffered(GVTeam, true);
            Utils.DrawGVStyle(ref GVTeam);

            InitGVTeam_Layout(GVTeam);

            btnClose.Click += btnClose_Click;
            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            btn_Run.Click += btn_Run_Click;
            btnShowLog.Click += btnShowLog_Click;
            btnHideLog.Click += btnHideLog_Click;
            timerTeam.Enabled = true;
            timerTeam.Stop();
            btn_ViewLog.Click += Btn_ViewLog_Click;

            timerEmployee.Enabled = false;
            timerEmployee.Stop();
            dte_FromDate.Value = SessionUser.Date_Work;
            rdo_All.CheckedChanged += Rdo_All_CheckedChanged;

        }



        private void Calculator_TimeKeeping_Load(object sender, EventArgs e)
        {
            if (SessionUser.UserLogin.Key != "4e5a9e53-9241-4c9b-a69f-e86a7ae3507f")
            {
                Utils.TNMessageBoxOK("Trang đang nâng cấp.Vui lòng quay lại sau!", 1);
                this.Close();
            }

            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            Check_RoleForm();

            PicLoading.Visible = false;
            Panel_Right.Visible = true;
            btnHideLog.Visible = true;
            btnShowLog.Visible = false;

            string zSQL = @"SELECT A.TeamKey, A.TeamID +'-'+ A.TeamName AS TeamName  FROM SYS_Team A
LEFT JOIN dbo.SYS_Branch B ON b.BranchKey=A.BranchKey
LEFT JOIN dbo.SYS_Department C ON C.DepartmentKey=A.DepartmentKey
WHERE A.RecordStatus <> 99 
ORDER BY B.Rank,C.Rank,A.Rank";
            LoadDataToToolbox.KryptonComboBox(cboTeam, zSQL, "---- Chọn tất cả ----");
        }
        private void Rdo_All_CheckedChanged(object sender, EventArgs e)
        {
            if (rdo_All.Checked)
            {
                cboTeam.Enabled = true;
            }
            else
            {
                cboTeam.Enabled = false;
            }
        }
        void LoadTeam()
        {
            _Text = "";
            AddLog_Detail("/---Đọc các nhóm cần tính chấm công---/ ");
            if (rdo_All.Checked)
            {
                if (cboTeam.SelectedValue.ToInt() > 0)
                {
                    _TableTeam = TimeKeeping_Month_Data.DanhSachToNhom(cboTeam.SelectedValue.ToInt());
                    _TotalTeam = _TableTeam.Rows.Count;
                    _Text = cboTeam.Text;
                    if (_TotalTeam > 0)
                    {
                        AddLog_Detail("--Key:" + _TableTeam.Rows[0]["TeamKey"] + ">Mã:" + _TableTeam.Rows[0]["TeamID"] + " >Tên: " + _TableTeam.Rows[0]["TeamName"]);
                        AddLog_Detail("/---Kết thúc đọc các nhóm cần tính chấm công---/ ");
                        _Log_Detail += Environment.NewLine;
                    }
                }
                else
                {
                    _TableTeam = TimeKeeping_Month_Data.DanhSachToNhom();
                    _TotalTeam = _TableTeam.Rows.Count;
                    _Text = "Tất cả nhóm";

                    if (_TotalTeam > 0)
                    {
                        for (int i = 0; i < _TableTeam.Rows.Count; i++)
                        {
                            AddLog_Detail("--Key:" + _TableTeam.Rows[i]["TeamKey"] + ">Mã:" + _TableTeam.Rows[i]["TeamID"] + " >Tên: " + _TableTeam.Rows[i]["TeamName"]);
                        }
                        AddLog_Detail("/---Kết thúc đọc các nhóm cần tính chấm công---/ ");
                        _Log_Detail += Environment.NewLine;
                    }
                }
            }
            if (rdo_VanPhong.Checked)
            {
                _TableTeam = TimeKeeping_Month_Data.DanhSachToNhomVanPhong();
                _TotalTeam = _TableTeam.Rows.Count;
                _Text = "Các nhóm bộ phận văn phòng";

                if (_TotalTeam > 0)
                {
                    for (int i = 0; i < _TableTeam.Rows.Count; i++)
                    {
                        AddLog_Detail("--Key:" + _TableTeam.Rows[i]["TeamKey"] + ">Mã:" + _TableTeam.Rows[i]["TeamID"] + " >Tên: " + _TableTeam.Rows[i]["TeamName"]);
                    }
                    AddLog_Detail("/---Kết thúc đọc các nhóm cần tính chấm công---/ ");
                    _Log_Detail += Environment.NewLine;
                }
            }
            if (rdo_HoTro.Checked)
            {
                _TableTeam = TimeKeeping_Month_Data.DanhSachNhomToTro();
                _TotalTeam = _TableTeam.Rows.Count;
                _Text = "Các nhóm bộ hỗ trợ sản xuất";

                if (_TotalTeam > 0)
                {
                    for (int i = 0; i < _TableTeam.Rows.Count; i++)
                    {
                        AddLog_Detail("--Key:" + _TableTeam.Rows[i]["TeamKey"] + ">Mã:" + _TableTeam.Rows[i]["TeamID"] + " >Tên: " + _TableTeam.Rows[i]["TeamName"]);
                    }
                    AddLog_Detail("/---Kết thúc đọc các nhóm cần tính chấm công---/ ");
                    _Log_Detail += Environment.NewLine;
                }
            }
            if (rdo_CongNhan.Checked)
            {
                _TableTeam = TimeKeeping_Month_Data.DanhSachNhomCongNhan();
                _TotalTeam = _TableTeam.Rows.Count;
                _Text = "Các nhóm bộ phận sản xuất";

                if (_TotalTeam > 0)
                {
                    for (int i = 0; i < _TableTeam.Rows.Count; i++)
                    {
                        AddLog_Detail("--Key:" + _TableTeam.Rows[i]["TeamKey"] + ">Mã:" + _TableTeam.Rows[i]["TeamID"] + " >Tên: " + _TableTeam.Rows[i]["TeamName"]);
                    }
                    AddLog_Detail("/---Kết thúc đọc các nhóm cần tính chấm công---/ ");
                    _Log_Detail += Environment.NewLine;
                }
            }
        }
        private void btn_Run_Click(object sender, EventArgs e)
        {
            if (SessionUser.Date_Lock >= dte_FromDate.Value)
            {
                Utils.TNMessageBoxOK("Thời gian này đã khóa sổ.!", 2);
                return;
            }
            _ReportCode = TimeKeeping_Month_Data.ListCodeReport(dte_FromDate.Value);
            if (_ReportCode.Rows.Count == 0)
            {
                Utils.TNMessageBoxOK("Chưa cấu hình mẫu cơm hoặc mẫu tính công", 2);
                return;
            }
            if (btn_Run.Tag.ToInt() == 0)
            {
                #region[Log]
                _Log_Detail = "";
                txtLog.Text = "";
                string Status = "Khởi động tính chấm công nhân sự > tháng:" + dte_FromDate.Value.ToString("MM/yyyy") + " > nhóm:" + _Text;
                Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);

                AddLog_Detail("/--- " + Status + " ---/");
                AddLog_Detail("/---Lúc " + DateTime.Now.ToString("HH: mm") + "---/");
                AddLog_Detail(Environment.NewLine);

                #endregion

                if (_Circle == 0)
                {
                    _IndexTeam = 0;
                    _TotalTeam = 0;

                    _IndexEmployee = 0;
                    _TotalEmployee = 0;

                    _Circle = 1;
                    LoadTeam();
                    timerTeam.Start();
                }
                if (_Circle == 1)
                {
                    timerTeam.Start();
                }
                if (_Circle == 2)
                {
                    timerEmployee.Start();
                }


                ShowLog("---Khởi chạy---");
                ShowLog("---Lúc " + DateTime.Now.ToString("hh:mm") + "---");
                PicLoading.Visible = true;
                btn_Run.Text = "Tạm dừng";
                btn_Run.Tag = 1;
            }
            else
            {
                if (_Circle == 1)
                {
                    timerTeam.Stop();
                }

                if (_Circle == 2)
                {
                    timerEmployee.Stop();
                }

                #region[Log]
                string Status = "Tạm dừng tính chấm công nhân sự > tháng:" + dte_FromDate.Value.ToString("MM/yyyy") + " > nhóm:" + _Text;
                Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);

                AddLog_Detail("/--- " + Status + " ---/");
                AddLog_Detail("/---Lúc " + DateTime.Now.ToString("HH: mm") + "---/");

                #endregion

                ShowLog("---Tạm dừng---");
                ShowLog("---Lúc " + DateTime.Now.ToString("hh:mm") + "---");
                PicLoading.Visible = true;
                btn_Run.Text = "Khởi động";
                btn_Run.Tag = 0;
            }
        }
        void LoadEmployee(int TeamKey, string TeamID, string TeamName)
        {
            _TableEmployee = TimeKeeping_Month_Data.Load_DataV2(TeamKey, dte_FromDate.Value);
            _TotalEmployee = _TableEmployee.Rows.Count;

            ShowLog("\\----------------------------------------------------------------------------\\");
            ShowLog("---[   Xử lý thông tin nhóm  " + TeamName + "  ]---");


            AddLog_Detail("/----------------------------------------------------------------------------/");
            AddLog_Detail("---[   Xử lý danh sách nhân sự nhóm  " + TeamName + "  ]---");

            if (_TableEmployee.Rows.Count > 0)
            {
                _ListEmployee = new List<TimeKeeping_Month_Info>();
                foreach (DataRow r in _TableEmployee.Rows)
                {
                    TimeKeeping_Month_Info zInfo = new TimeKeeping_Month_Info();
                    zInfo.EmployeeKey = r["EmployeeKey"].ToString();
                    zInfo.EmployeeID = r["EmployeeID"].ToString();
                    zInfo.EmployeeName = r["EmployeeName"].ToString();
                    zInfo.TeamKey = TeamKey;
                    zInfo.TeamID = TeamID;
                    zInfo.TeamName = TeamName;
                    zInfo.DateWrite = dte_FromDate.Value;
                    zInfo.CreatedBy = SessionUser.UserLogin.Key;
                    zInfo.CreatedName = SessionUser.UserLogin.EmployeeName;
                    zInfo.ModifiedBy = SessionUser.UserLogin.Key;
                    zInfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
                    _ListEmployee.Add(zInfo);
                }
            }
            else
            {
                ShowLog("Không có công nhân.");
                AddLog_Detail("---Không có công nhân.---");
            }
        }
        private void timerEmployee_Tick(object sender, EventArgs e)
        {
            timerEmployee.Stop();
            if (_IndexEmployee < _TotalEmployee)
            {
                _NCQD = 0;
                _NCTT = 0;
                //------------
                _Month_Info = new TimeKeeping_Month_Info();
                _Month_Info = _ListEmployee[_IndexEmployee]; // truyen qua progessdata xu lý tạo mới
                Process_Data();
                Process_Score();
                _Circle = 2;
                _IndexEmployee++;
                timerEmployee.Start();
            }
            else
            {
                ShowLog("---Đã xử lý xong nhóm ---");

                AddLog_Detail("/---Đã xử lý xong nhóm ---/");
                AddLog_Detail("/-------------------------/");

                _Circle = 1;
                _IndexTeam++;
                _IndexEmployee = 0;
                timerTeam.Start();
            }
        }
        private void timerTeam_Tick(object sender, EventArgs e)
        {
            timerTeam.Stop();

            if (_IndexTeam < _TotalTeam)
            {
                if (_TableTeam.Rows.Count > 0)
                {
                    DataRow r = _TableTeam.Rows[_IndexTeam];
                    _TeamKey = r["TeamKey"].ToInt();
                    _TeamName = r["TeamName"].ToString();
                    _TeamID = r["TeamID"].ToString();

                    AddLog_Detail("/---Bắt đầu tính nhóm : " + _TeamName + "---/");
                    AddLog_Detail("---Xóa thông tin đã tính >nhóm : " + _TeamName + ">ngày:" + dte_FromDate.Value.ToString());

                    _Month_Info = new TimeKeeping_Month_Info();
                    _Month_Info.Delete_Team(_TeamKey, dte_FromDate.Value);

                    if (_Month_Info.Message == "30")
                    {
                        AddLog_Detail("---Xóa thông tin đã tính >nhóm : " + _TeamName + ">ngày:" + dte_FromDate.Value.ToString() + ">Thành công");
                    }
                    else
                    {
                        AddLog_Detail("---Xóa thông tin đã tính >nhóm : " + _TeamName + ">ngày:" + dte_FromDate.Value.ToString() + ">Lỗi:" + _Month_Info.Message);
                    }

                    LoadEmployee(_TeamKey, _TeamID.Trim(), _TeamName);
                    ShowGVTeam(r, GVTeam);
                    timerEmployee.Start();
                }
                else
                {
                    _Circle = 0;
                    _IndexTeam = 0;
                    timerTeam.Stop();
                    btn_Run.Enabled = true;
                    btn_Run.Text = "Khởi động";
                    PicLoading.Visible = false;
                    btn_Run.Tag = 0;
                    ShowLog("---Đã tính xong lúc " + DateTime.Now.ToString("HH:mm") + "---");

                    AddLog_Detail("/--Đã tính xong lúc " + DateTime.Now.ToString("HH:mm") + "---/");
                    //txt_Log_Detail.AppendText(_Log_Detail + Environment.NewLine);
                    string zStatus = "Kết thúc tính chấm công nhân sự > tháng:" + dte_FromDate.Value.ToString("MM/yyyy") + " > nhóm:" + _Text;
                    Save_LogCaculator_Import(zStatus);


                }
            }
            else
            {
                _Circle = 0;
                _IndexTeam = 0;
                timerTeam.Stop();
                btn_Run.Enabled = true;
                PicLoading.Visible = false;
                btn_Run.Text = "Khởi động";
                btn_Run.Tag = 0;
                ShowLog("---Đã tính xong lúc " + DateTime.Now.ToString("HH:mm") + "---");

                AddLog_Detail("/--Đã tính xong lúc " + DateTime.Now.ToString("HH:mm") + "---/");
                //txt_Log_Detail.AppendText(_Log_Detail + Environment.NewLine);
                string zStatus = "Kết thúc tính chấm công nhân sự > tháng:" + dte_FromDate.Value.ToString("MM/yyyy") + " > nhóm:" + _Text;
                Save_LogCaculator_Import(zStatus);
            }
        }
        void Process_Data()
        {
            _Key = 0;
            _ID = "";
            _Name = "";
            _Number = 0;
            _Type = 0;
            _CategoryKey = 0;
            _CategoryName = "";
            _Rank = 0;

            string ztext = "--Bắt đầu tính chấm công nhân sự:[" + _Month_Info.EmployeeID + "-" + _Month_Info.EmployeeName + "]--" + Environment.NewLine;
            ztext += "-Số stt       :[" + _IndexEmployee + "] " + Environment.NewLine;
            ztext += "-Key nhân viên:" + _Month_Info.EmployeeKey + Environment.NewLine;
            ztext += "-Mã thẻ       :" + _Month_Info.EmployeeID + Environment.NewLine;
            ztext += "-Họ và tên    :" + _Month_Info.EmployeeName + Environment.NewLine;
            ztext += "-Key nhóm     :" + _Month_Info.TeamKey + Environment.NewLine;
            ztext += "-Mã nhóm      :" + _Month_Info.TeamID + Environment.NewLine;
            ztext += "-Tên nhóm     :" + _Month_Info.TeamName + Environment.NewLine;
            ztext += "-Ngày         :" + _Month_Info.DateWrite.ToString("dd/MM/yyyy") + Environment.NewLine;
            ztext += "-Key tạo      :" + _Month_Info.CreatedBy + Environment.NewLine;
            ztext += "-Tên người tạo:" + _Month_Info.CreatedName + Environment.NewLine;
            ztext += "-Key sửa      :" + _Month_Info.ModifiedBy + Environment.NewLine;
            ztext += "-Tên người sửa:" + _Month_Info.ModifiedName + Environment.NewLine;
            AddLog_Detail(ztext);

            AddLog_Detail("---Bắt đầu tính bảng chấm công---");

            for (int i = 0; i < _ReportCode.Rows.Count; i++)
            {
                _Key = _ReportCode.Rows[i]["AutoKey"].ToInt();
                _ID = _ReportCode.Rows[i]["ID"].ToString().Trim();
                _Name = _ReportCode.Rows[i]["Name"].ToString().Trim();
                _Type = _ReportCode.Rows[i]["Type"].ToInt();// chua su dung
                _TypeName = _ReportCode.Rows[i]["TypeName"].ToString();// chua su dung
                _Rank = _ReportCode.Rows[i]["Rank"].ToInt();
                _CategoryKey = _ReportCode.Rows[i]["CategoryKey"].ToInt();
                _CategoryName = _ReportCode.Rows[i]["CategoryName"].ToString().Trim();
               
                switch (_ID)
                {
                    case "CDPN"://Số ngày công trả chế độ phép năm
                        _Number = TimeKeeping_Month_Data.CDPN(dte_FromDate.Value);
                        break;
                    case "NCQD"://Số ngày công quy định
                        _Number = TimeKeeping_Month_Data.NCQD(_Month_Info.EmployeeKey, dte_FromDate.Value);
                        _NCQD = _Number;
                        break;
                    case "NNPN"://Số ngày nghỉ phép năm
                        _Number = TimeKeeping_Month_Data.NNPN(_Month_Info.EmployeeKey, dte_FromDate.Value);
                        break;
                    case "NLTT"://Số ngày lễ trong tháng
                        _Number = TimeKeeping_Month_Data.NLTT(_Month_Info.EmployeeKey, dte_FromDate.Value);
                        break;
                    case "NCK0"://Mã công
                    case "NCK2":
                    case "NCK1":
                    case "NCK3":
                    case "NCCP":
                    case "NCPT":
                    case "NCKP":
                    case "NCOM":
                    case "NCCO":
                    case "NCLP":
                    case "NCRN":
                    case "NCVE":
                    case "NCVA":
                    case "NCKL":
                    case "NCTS":
                    case "NCTC":
                    case "NCCH":
                    case "NCL1":
                    case "NCL2":
                    case "NCL3":
                        _Number = TimeKeeping_Month_Data.DemSoMaCong(_Month_Info.EmployeeKey, _ID.Substring(2, 2), dte_FromDate.Value);
                        break;
                    #region[Mã cơm cũ]
                    case "PCC1"://Mã cơm
                    case "PCC2":
                    case "PCC3":
                    case "PCU1":
                    case "PCU2":
                    case "PCU3":
                    case "PTT1":
                    case "PTT2":
                        _Number = TimeKeeping_Month_Data.DemSoMaCom(_Month_Info.EmployeeKey, _ID.Substring(2, 2), dte_FromDate.Value);
                        break;
                    #endregion
                    case "SNCC"://Số ngày chuyên cần
                        _Number = TimeKeeping_Month_Data.SNCC(_Month_Info.EmployeeKey, dte_FromDate.Value);
                        break;
                    case "SCTT"://Số ngày thực tế
                        _Number = TimeKeeping_Month_Data.SCTT(_Month_Info.EmployeeKey, dte_FromDate.Value);
                        break;
                    case "TTNT"://Số công thực tế tính lương
                        _Number = TimeKeeping_Month_Data.TTNT(_Month_Info.EmployeeKey, dte_FromDate.Value);
                        break;
                    case "CBTT"://Số công tích lũy tháng trước
                        _Number = TimeKeeping_Month_Data.CBTT(_Month_Info.EmployeeKey, dte_FromDate.Value);
                        break;
                    case "CMTT"://Số công mượn tháng trước
                        _Number = TimeKeeping_Month_Data.CMTT(_Month_Info.EmployeeKey, dte_FromDate.Value);
                        break;
                    case "CBTN"://Công bù trong tháng này và thâm niên
                        _Number = TimeKeeping_Month_Data.CBTN(_Month_Info.EmployeeKey, dte_FromDate.Value);
                        break;
                    case "HUNC"://hoàn ứng công mượn
                        _Number = TimeKeeping_Month_Data.HUNC(_Month_Info.EmployeeKey, dte_FromDate.Value);
                        break;
                    case "UTNC"://Ứng công
                        _Number = TimeKeeping_Month_Data.UTNC(_Month_Info.EmployeeKey, dte_FromDate.Value);
                        _NCTT = _Number;
                        break;
                    case "CTNB"://Số công thừa nghỉ bù
                        _Number = TimeKeeping_Month_Data.CTNB(_Month_Info.EmployeeKey, dte_FromDate.Value);
                        break;
                    case "CTCT"://Số công thừa cuối tháng hiện tính
                        _Number = TimeKeeping_Month_Data.CTCT(_Month_Info.EmployeeKey, dte_FromDate.Value);
                        break;
                    case "CMCT"://Số công mượn cuối tháng hiện tính
                        _Number = TimeKeeping_Month_Data.CMCT(_Month_Info.EmployeeKey, dte_FromDate.Value);
                        break;
                    case "CHPT"://Cưới hỏi, phép tang
                        _Number = TimeKeeping_Month_Data.CHPT(_Month_Info.EmployeeKey, dte_FromDate.Value);
                        _NCTT = _Number;
                        break;
                    //case "CO16"://Tổng phần cơm 16
                    //    _Number = TimeKeeping_Month_Data.CO16(_Month_Info.EmployeeKey, dte_FromDate.Value);
                    //    _NCTT = _Number;
                    //    break;
                    //case "CO20"://Tổng phần cơm 20
                    //    _Number = TimeKeeping_Month_Data.CO20(_Month_Info.EmployeeKey, dte_FromDate.Value);
                    //    _NCTT = _Number;
                    //    break;
                    //case "COTR"://Tổng phần cơm trừ
                    //    _Number = TimeKeeping_Month_Data.COTR(_Month_Info.EmployeeKey, dte_FromDate.Value);
                    //    _NCTT = _Number;
                    //    break;
                    default:
                        if (_CategoryKey == 1 | _CategoryKey == 2)
                        {
                            //Tổng mã cơm
                            string RiceID = TimeKeeping_Month_Data.GetRiceID(_ID);
                            if (RiceID.Length > 0)
                                _Number = TimeKeeping_Month_Data.NumberRice(_Month_Info.EmployeeKey, RiceID, dte_FromDate.Value);
                        }
                        else if (_CategoryKey == 3 | _CategoryKey == 4)
                        {
                            // Tổng mã cơm thống kê
                            string RiceID = TimeKeeping_Month_Data.GetRiceID(_ID);
                            if (RiceID.Length > 0)
                                _Number = TimeKeeping_Month_Data.NumberRice(_Month_Info.EmployeeKey, RiceID, dte_FromDate.Value);
                        }
                        else
                        {
                            _Number = 0;
                        }
                        break;
                }
                Insert();
            }
            AddLog_Detail("----Kết thúc tính chấm công nhân sự:[" + _Month_Info.EmployeeID + "-" + _Month_Info.EmployeeName + "]---");
            _Log_Detail += Environment.NewLine;
        }
        private void Insert()
        {
            _Month_Info.CodeKey = _Key;
            _Month_Info.CodeID = _ID;
            _Month_Info.CodeName = _Name;
            _Month_Info.Number = _Number;
            _Month_Info.Type = _Type;
            _Month_Info.TypeName = _TypeName;
            _Month_Info.Rank = _Rank;
            _Month_Info.CategoryKey = _CategoryKey;
            _Month_Info.CategoryName = _CategoryName;
            _Month_Info.Create();
            if (_Month_Info.Message == "11")
                AddLog_Detail("-Key:" + _Key.ToString().PadLeft(2, '0') + " > Mã: " + _ID + " > SL: " + _Number.ToString("n1").PadLeft(12, '_') + " > Tên: " + _Name.PadRight(40, ' ') + " > Lưu thành công");
            else
                AddLog_Detail("-Key:" + _Key.ToString().PadLeft(2, '0') + " > Mã: " + _ID + " > SL: " + _Number.ToString("n1").PadLeft(12, '_') + " > Tên: " + _Name.PadRight(40, ' ') + " >> Lỗi. " + _Month_Info.Message);

        }
        void Process_Score()
        {
            int zCount = Score_Data.CountTeam(_Month_Info.TeamKey.ToInt());
            if (zCount > 0)
            {
                Score_Info zScore;
                zScore = new Score_Info(_Month_Info.EmployeeKey, _Month_Info.DateWrite);
                zScore.EmployeeKey = _Month_Info.EmployeeKey;
                zScore.EmployeeID = _Month_Info.EmployeeID;
                zScore.EmployeeName = _Month_Info.EmployeeName;
                zScore.TeamKey = _Month_Info.TeamKey;
                zScore.DateWrite = _Month_Info.DateWrite;
                zScore.NCQD = _NCQD;
                zScore.NCTT = _NCTT;

                // A: Công tính lương tháng này
                // B:Công thực tế tháng này từ chấm công
                // T:Số công thừa tháng trước
                // M:Công mượn tháng này
                // N: Dư công tháng này
                // L:Công thực tế tính tiền
                // CT: Số công còn thiếu để bằng A
                // TAM: Công tạm

                DateTime zOld = _Month_Info.DateWrite.AddMonths(-1);

                float A = _NCQD;
                float B = _NCTT;
                float T = Score_Data.BeginScore(_Month_Info.EmployeeKey, zOld);
                float M = Score_Data.BeginScore(_Month_Info.EmployeeKey, zOld);
                float L = 0;
                float CT = 0;
                float TAM = 0;
                float N = 0;
                if ((B - A) >= 0) //Thừa công hoặc bằng công tháng hiện tại
                {
                    L = A;
                    N = (B - A) + T + M;
                }
                else //Thiếu công tháng hiện tại
                {
                    CT = Math.Abs(B - A);
                    TAM = CT - (T + M);
                    // Khấu trừ công tích lũy
                    if (TAM > 0) //Đã khấu trừ công tích lũy nhưng vẫn không đủ
                    {
                        L = A - TAM;
                        N = 0;
                    }
                    else //Đã khấu trừ công tích lũy mà vẫn còn dư công
                    {
                        L = A;
                        N = Math.Abs(TAM);
                    }
                }




                //float zDuCongThangTruoc = Score_Data.BeginScore(_Month_Info.EmployeeKey, zOld);
                //float zMuonCongThangNay = Score_Data.BeginScore(_Month_Info.EmployeeKey, zOld);
                //float zDuCongThangNay = 0;
                //float zDuTam = _NCQD - _NCTT;
                //float zTam = 0;
                //if (zDuTam <= 0) //đang dư công
                //{
                //    zDuCongThangNay = zDuTam * (-1) + zDuCongThangTruoc;
                //}
                //else
                //{
                //     zTam = zDuCongThangTruoc + _NCTT + zMuonCongThangNay;
                //    if (_NCQD < zTam) //ngày công quy định < dư công thăng trước + Ngày công tt tháng này + ngày mượn
                //        zDuCongThangNay = _NCQD - zTam * (-1);
                //}

                //zScore.BeginScore = zDuCongThangTruoc;
                //zScore.EndScore = zDuCongThangNay;
                //zScore.BorrowScore = zMuonCongThangNay;
                zScore.BeginScore = T;
                zScore.EndScore = N;
                zScore.BorrowScore = M;

                zScore.CreatedBy = SessionUser.UserLogin.Key;
                zScore.CreatedName = SessionUser.UserLogin.EmployeeName;
                zScore.ModifiedBy = SessionUser.UserLogin.Key;
                zScore.ModifiedName = SessionUser.UserLogin.EmployeeName;
                zScore.Save();
            }


        }
        void InitGVTeam_Layout(DataGridView GV)
        {
            GV.Columns.Add("No", "#");
            GV.Columns.Add("TeamID", "Mã nhóm");
            GV.Columns.Add("TeamName", "Tên nhóm");

            GV.Columns["No"].Width = 45;
            GV.Columns["TeamID"].Width = 100;
            GV.Columns["TeamName"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.ColumnHeadersHeight = 25;
            GV.FirstDisplayedScrollingRowIndex = GV.RowCount - 1;
        }
        void ShowGVTeam(DataRow nRow, DataGridView GV)
        {
            GV.Rows.Add();
            DataGridViewRow nRowView = GV.Rows[_IndexTeam];
            nRowView.Tag = nRow["TeamKey"];
            nRowView.Cells["No"].Value = (_IndexTeam + 1).ToString();
            nRowView.Cells["TeamName"].Value = nRow["TeamName"].ToString().Trim();
            nRowView.Cells["TeamID"].Value = nRow["TeamID"].ToString().Trim();
        }
        void ShowLog(string Text)
        {
            Invoke(new MethodInvoker(delegate
            {
                txtLog.Items.Add(Text);
                txtLog.SelectedIndex = txtLog.Items.Count - 1;
            }));

            _txtLog += Text + Environment.NewLine;
        }
        void AddLog_Detail(string Text)
        {
            Invoke(new MethodInvoker(delegate
            {
                txt_Log_Detail.Items.Add(DateTime.Now.ToString("HH:mm:ss.fff") + " : " + Text);
                txt_Log_Detail.SelectedIndex = txt_Log_Detail.Items.Count - 1;
            }));

            _Log_Detail += DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss.fff") + " " + Text + Environment.NewLine;
        }
        void Save_LogCaculator_Import(string ActionName)
        {
            Application_Info zinfo = new Application_Info();
            zinfo.ActionName = ActionName;
            zinfo.DateLog = dte_FromDate.Value;
            zinfo.ActionType = 3;
            zinfo.SubDescription =_txtLog;
            zinfo.Description = _Log_Detail;
            zinfo.Form = 4;//Dành cho tính chấm công.
            zinfo.FormName = "Form " + HeaderControl.Text;
            zinfo.UserKey = SessionUser.UserLogin.Key;
            zinfo.EmployeeKey = SessionUser.UserLogin.EmployeeKey;
            zinfo.CreatedBy = SessionUser.UserLogin.Key;
            zinfo.CreatedName = SessionUser.UserLogin.EmployeeName;
            zinfo.ModifiedBy = SessionUser.UserLogin.Key;
            zinfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
            zinfo.Create();
        }
        private void Btn_ViewLog_Click(object sender, EventArgs e)
        {
            Frm_LogDetail frm = new Frm_LogDetail();
            frm.DateWrite = dte_FromDate.Value;
            frm.Form = 4;
            frm.FormName = HeaderControl.Text;
            frm.ShowDialog();
        }

        #region [Custom Control Box Form]
        private void btnHideLog_Click(object sender, EventArgs e)
        {
            Panel_Right.Visible = false;
            txtLog.Visible = false;
            btnShowLog.Visible = true;
            btnHideLog.Visible = false;
        }
        private void btnShowLog_Click(object sender, EventArgs e)
        {
            Panel_Right.Visible = true;
            txtLog.Visible = true;
            btnShowLog.Visible = false;
            btnHideLog.Visible = true;
        }

        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion        

        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_ViewLog.Enabled = false;
            }
            if (Role.RoleAdd == 0 && Role.RoleEdit == 0)
            {
                btn_Run.Enabled = false;
                btn_ViewLog.Enabled = false;
            }

        }
        #endregion
    }
}
