﻿using C1.Win.C1FlexGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.HRM;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Adjust_NCPN : Form
    {
        #region[Private]
        private int _Key = 0;
        #endregion
        public Frm_Adjust_NCPN()
        {
            InitializeComponent();
            btn_Save.Click += Btn_Save_Click;
            btn_New.Click += Btn_New_Click;
            btn_Del.Click += Btn_Del_Click;
            btn_Search.Click += Btn_Search_Click;
            txt_EmployeeID.Enter += Txt_EmployeeID_Enter;
            txt_EmployeeID.Leave += Txt_EmployeeID_Leave;
            //txt_Number.KeyUp += Txt_Number_KeyUp;
            txt_Number.Leave += Txt_Number_Leave;

            btnClose.Click += btnClose_Click;
            btnMax.Click += btnMax_Click;
            btnMini.Click += btnMini_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;
            GVData.Click += GVData_List_Click;
            btn_Export.Click += Btn_Export_Click;

        }

        private void Txt_Number_Leave(object sender, EventArgs e)
        {
            double zAmount = 0;
            if (double.TryParse(txt_Number.Text.Trim(), out zAmount))
            {
                txt_Number.Text = zAmount.Toe1String();
            }
            else
            {
                Utils.TNMessageBoxOK("Lỗi!.Định dạng số.",1);
            }
        }

        private void Frm_Adjust_NCPN_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            Check_RoleForm();
            this.Bounds = Screen.PrimaryScreen.WorkingArea;

            DateTime ViewDate = SessionUser.Date_Work;
            DateTime FromDate = new DateTime(ViewDate.Year, ViewDate.Month, 1, 0, 0, 0);
            DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            dte_FromDate.Value = FromDate;
            dte_ToDate.Value = ToDate;
            dte_DateWrite.Value = SessionUser.Date_Work;

            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            //DisplayData();
        }
        #region[event]
        private void Txt_EmployeeID_Enter(object sender, EventArgs e)
        {

            if (txt_EmployeeID.Text.Trim() == "Nhập mã thẻ")
            {
                txt_EmployeeID.Text = "";
                txt_EmployeeID.ForeColor = Color.Brown;
            }
        }
        private void Txt_EmployeeID_Leave(object sender, EventArgs e)
        {
            if (txt_EmployeeID.Text.Trim().Length < 1)
            {
                txt_EmployeeID.Text = "Nhập mã thẻ";
                txt_EmployeeID.ForeColor = Color.White;
            }
            else
            {
                Employee_Info zinfo = new Employee_Info();
                zinfo.GetEmployee(txt_EmployeeID.Text.Trim());
                if (zinfo.Key == "")
                {
                    txt_EmployeeID.Text = "Nhập mã thẻ";
                    txt_EmployeeID.ForeColor = Color.White;
                    txt_EmployeeID.Tag = null;
                }
                else
                {
                    txt_EmployeeID.Text = zinfo.EmployeeID;
                    txt_EmployeeName.Text = zinfo.FullName;
                    txt_EmployeeID.Tag = zinfo.Key;
                    txt_EmployeeID.BackColor = Color.Black;
                }
            }
        }
        #endregion
        #region[Progess]
        private void LoadOrderInfo()
        {
            Adjust_NCPN_Info zInfo = new Adjust_NCPN_Info(_Key);
            txt_EmployeeID.Tag = zInfo.EmployeeKey;
            
            txt_AdjustName.Text = zInfo.AdjustName;
            txt_EmployeeName.Text = zInfo.EmployeeName;
            txt_EmployeeID.Text = zInfo.EmployeeID;

            if (zInfo.DateWrite != DateTime.MinValue)
            {
                dte_DateWrite.Value = zInfo.DateWrite;
            }
            txt_Description.Text = zInfo.Description;
            txt_Number.Text = zInfo.Number.Ton0String();

        }
        private void Load_Refresh()
        {
            _Key = 0;
            dte_DateWrite.Value = SessionUser.Date_Work;
            txt_EmployeeID.Tag = null;
            txt_EmployeeID.Text = "";
            txt_EmployeeName.Text = "";
            txt_Number.Text = "0";
            txt_Description.Text = "";
            txt_AdjustName.Text = "";
            txt_EmployeeID.ReadOnly = false;
            txt_EmployeeID.Focus();
        }
       
        #endregion

        #region[Event]
        private void Btn_Save_Click(object sender, EventArgs e)
        {
            float zNumber = 0;
            if (txt_EmployeeID.Tag == null || txt_EmployeeID.Text.Trim() == "")
            {
                Utils.TNMessageBoxOK("Chưa chọn nhân viên!",1);
            }
          
            else if (!float.TryParse(txt_Number.Text.Trim(),out zNumber))
            {
                Utils.TNMessageBoxOK("Giá trị không hợp lệ!",1);
            }
            if (SessionUser.Date_Lock >= dte_DateWrite.Value)
            {
                Utils.TNMessageBoxOK("Thời gian này đã khóa sổ.!", 2);
                return;
            }
            else
            {
                Adjust_NCPN_Info zInfo = new Adjust_NCPN_Info(_Key);
                zInfo.AdjustName = txt_AdjustName.Text.Trim();
                zInfo.DateWrite = dte_DateWrite.Value;
                zInfo.CategoryKey = 0;
                zInfo.CategoryID = "NPTN";
                zInfo.CategoryName = "NGÀY THÂM NIÊN";
                zInfo.EmployeeKey = txt_EmployeeID.Tag.ToString();
                zInfo.EmployeeID = txt_EmployeeID.Text.ToString().Trim();
                zInfo.EmployeeName = txt_EmployeeName.Text.ToString();
                Employee_Info zEm = new Employee_Info(zInfo.EmployeeKey);
                zInfo.TeamKey = zEm.TeamKey;
                zInfo.DepartmentKey = zEm.DepartmentKey;
                zInfo.Number = float.Parse(txt_Number.Text);
                zInfo.Description = txt_Description.Text;
               
                zInfo.CreatedBy = SessionUser.UserLogin.Key;
                zInfo.CreatedName = SessionUser.UserLogin.EmployeeName;
                zInfo.ModifiedBy = SessionUser.UserLogin.Key;
                zInfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
                zInfo.Save();
                string zMessage = TN_Message.Show(zInfo.Message);
                if (zMessage.Length == 0)
                {
                    Utils.TNMessageBoxOK("Câp nhật thành công", 3);
                    _Key = zInfo.Key;
                    LoadOrderInfo();
                    DisplayData();
                }
                else
                {
                    Utils.TNMessageBoxOK(zInfo.Message, 4);
                }
            }


        }
        private void Btn_Del_Click(object sender, EventArgs e)
        {
            if (_Key != 0)
            {
                Adjust_NCPN_Info zInfo = new Adjust_NCPN_Info(_Key);
                if (SessionUser.Date_Lock >= zInfo.DateWrite)
                {
                    Utils.TNMessageBoxOK("Thời gian này đã khóa sổ.!", 2);
                    return;
                }
                if (Utils.TNMessageBox("Bạn có chắc xóa thông tin này",2)=="Y")
                {
                   
                    zInfo.ModifiedBy = SessionUser.UserLogin.Key;
                    zInfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
                    zInfo.Delete();
                    string zMessage = TN_Message.Show(zInfo.Message);
                    if (zMessage.Length == 0)
                    {
                        Utils.TNMessageBoxOK("Câp nhật thành công",3);
                        Load_Refresh();
                        DisplayData();
                    }
                    else
                    {
                        Utils.TNMessageBoxOK(zInfo.Message,4);
                    }

                }
            }
            else
            {
                Utils.TNMessageBoxOK("Vui lòng chọn phiếu !.",1);
            }

        }
        private void Btn_New_Click(object sender, EventArgs e)
        {
            Load_Refresh();
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            DisplayData();
        }
        #endregion

       
        private void txt_Amount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= '0' && e.KeyChar <= '9') || (Keys)e.KeyChar == Keys.Back || e.KeyChar == 46)
            {

                e.Handled = false;

            }
            else
            {
                e.Handled = true;
            }

        }
        //private void Txt_Number_KeyUp(object sender, KeyEventArgs e)
        //{
        //    double zAmount = 0;
        //    if(double.TryParse(txt_Number.Text.Trim(), out zAmount)){
        //        txt_Number.Text = zAmount.Toe1String();
        //        txt_Number.Focus();
        //        txt_Number.SelectionStart = txt_Number.Text.Length - 2;
        //    }
        //    else
        //    {

        //    }

        //}

        #region[ListView]
        private void DisplayData()
        {

            DataTable ztb = Adjust_NCPN_Data.Search_List(dte_FromDate.Value, dte_ToDate.Value, txt_Search.Text.Trim());
            InitGV_Layout(ztb);
        }
        private void GVData_List_Click(object sender, EventArgs e)
        {
            if (GVData.Rows.Count > 0)
            {
                string zKey = GVData.Rows[GVData.RowSel][8].ToString();
                if (zKey != null || zKey != "")
                {
                    _Key = int.Parse(zKey);
                    LoadOrderInfo();
                }
                else
                {
                    _Key = 0;
                    LoadOrderInfo();
                }
            }
        }
        private void Btn_Export_Click(object sender, EventArgs e)
        {
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Danh sách ngày thâm niên.xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }
                    GVData.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }

        }
        void InitGV_Layout(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            int TotalRow = TableView.Rows.Count + 1;
            int ToTalCol = 9;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(TotalRow);

            //Row Header

            GVData.Rows[0][0] = "Stt";
            GVData.Rows[0][1] = "Ngày";
            GVData.Rows[0][2] = "Họ và tên";
            GVData.Rows[0][3] = "Số thẻ";
            GVData.Rows[0][4] = "Tổ nhóm";
            GVData.Rows[0][5] = "Giá trị";
            GVData.Rows[0][6] = "Lý do";
            GVData.Rows[0][7] = "Ghi chú";
            GVData.Rows[0][8] = ""; // ẩn cột này

            //Style         
            //GVData.AllowFreezing = AllowFreezingEnum.Both;
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.Custom;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.LeftCenter;
            GVData.Styles.Normal.WordWrap = true;

            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                DataRow rData = TableView.Rows[rIndex];

                GVData.Rows[rIndex + 1][0] = rIndex + 1;
                GVData.Rows[rIndex + 1][1] = rData[1];
                GVData.Rows[rIndex + 1][2] = rData[2];
                GVData.Rows[rIndex + 1][3] = rData[3];
                GVData.Rows[rIndex + 1][4] = rData[4];
                GVData.Rows[rIndex + 1][5] = rData[5].Toe0String();
                GVData.Rows[rIndex + 1][6] = rData[6];
                GVData.Rows[rIndex + 1][7] = rData[7];
                GVData.Rows[rIndex + 1][8] = rData[0];
            }



            //Freeze Row and Column                              
            GVData.Rows.Fixed = 1;
            GVData.Rows[0].TextAlign = TextAlignEnum.CenterCenter;
            GVData.Rows[0].Height = 50;

            GVData.Cols[0].Width = 30;
            GVData.Cols[1].Width = 100;
            GVData.Cols[2].Width = 200;
            GVData.Cols[3].Width = 100;
            GVData.Cols[4].Width = 100;
            GVData.Cols[5].Width = 100;
            GVData.Cols[6].Width = 200;
            GVData.Cols[7].Width = 50;
            GVData.Cols[8].Visible = false;

            GVData.Cols[5].TextAlign = TextAlignEnum.RightCenter;
        }
        #endregion
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                //this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
                btn_Export.Enabled = false;
            }
            if (Role.RoleAdd == 0 && Role.RoleEdit == 0)
            {
                btn_New.Enabled = false;
                btn_Save.Enabled = false;
            }
            if (Role.RoleAdd == 0 && Role.RoleEdit == 1)
            {
                btn_New.Enabled = false;
                btn_Save.Enabled = true;
            }
            if (Role.RoleDel == 0)
            {
                btn_Del.Enabled = false;
            }
        }
        #endregion
    }
}
