﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.HRM;
using TNS.Misc;
using TN.Library.System;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_TimePermit : Form
    {
        public Frm_TimePermit()
        {
            InitializeComponent();
            btn_Save.Click += Btn_Save_Click;
            btn_New.Click += Btn_New_Click;
            btn_Del.Click += Btn_Del_Click;
            btn_Search.Click += Btn_Search_Click;
        }
        #region[Private]
        private string _Key = "";
        private string _Name = "";
        private string _CardID = "";
        private DateTime _Date;
        private string _AutoKey = "";
        #endregion
        private void Frm_TimeWorking_Load(object sender, EventArgs e)
        {
            LoadDataToToolbox.ComboBoxData(cbo_Code, Time_Defines_Data.ListTable(), false, 0, 1);
            LV_Layout(LV_List);
            LV_Layout_Detail(LV_Detail);
            _Date = dte_Month.Value;
            SetDefault();
        }
        private void LoadData()
        {
            Permit_Info zinfo = new Permit_Info(_AutoKey);
            dte_CheckTime.Value = zinfo.PermitDate;
            dte_FromDay.Value = zinfo.FromDate;
            dte_ToDay.Value = zinfo.ToDate;
            txt_NumberDay.Text = zinfo.NumberOff.ToString();
            txt_Description.Text = zinfo.Description;
            cbo_Code.SelectedValue = zinfo.CodeKey;
            txt_CardID.Text = _CardID;
            txt_EmployeeName.Text = _Name;

        }
        private void SetDefault()
        {
            _AutoKey = "";
            dte_CheckTime.Value = dte_CheckTime.Value;
            dte_FromDay.Value = dte_CheckTime.Value;
            dte_ToDay.Value = dte_CheckTime.Value;
            txt_NumberDay.Text = "0";
            txt_Description.Text = "";
            dte_CheckTime.Focus();
        }
        #region[LV_List]
        private void LV_Layout(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "No";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Họ tên";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã nhân viên";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);
        }
        private void LV_LoadData(DataTable In_Table)
        {
            _AutoKey = "";
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LV_List;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            LV.Items.Clear();
            int n = In_Table.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                DataRow nRow = In_Table.Rows[i];

                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.Tag = nRow["EmployeeKey"];
                lvi.ForeColor = Color.Navy;

                lvi.BackColor = Color.White;
                lvi.ImageIndex = 0;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["FullName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["CardID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }

            this.Cursor = Cursors.Default;

        }
        private void LV_List_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < LV_List.Items.Count; i++)
            {
                if (LV_List.Items[i].Selected == true)
                {
                    LV_List.Items[i].BackColor = Color.LightBlue; // highlighted item
                }
                else
                {
                    LV_List.Items[i].BackColor = SystemColors.Window; // normal item
                }
            }
            _Key = LV_List.SelectedItems[0].Tag.ToString();
            _Name = LV_List.SelectedItems[0].SubItems[1].Text;
            _CardID = LV_List.SelectedItems[0].SubItems[2].Text;
            txt_CardID.Text = _CardID;
            txt_EmployeeName.Text = _Name;
           // SetDefault();
            LV_LoadData_Detail();
        }



        #endregion
        #region[LV_Detail]
        private void LV_Layout_Detail(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "No";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ngày";
            colHead.Width = 110;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Loại phép";
            colHead.Width = 110;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Lý do";
            colHead.Width = 110;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ngày bắt đầu";
            colHead.Width = 110;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ngày kết thúc";
            colHead.Width = 110;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số ngày nghỉ";
            colHead.Width = 110;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);
        }
        private void LV_LoadData_Detail()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LV_Detail;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            LV.Items.Clear();
            DateTime FromDate;
            DateTime ToDate;
            FromDate = new DateTime(_Date.Year, _Date.Month, 1, 0, 0, 0);
            ToDate = FromDate.AddMonths(1).AddDays(-1);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            lbl_TitleDetail.Text = "CHI TIÊT NGHỈ PHÉP THÁNG " + _Date.Month + "/" + _Date.Year;
            DataTable ztb = Permit_Data.EmployeePermitList(_Key, FromDate, ToDate);
            int n = ztb.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                DataRow nRow = ztb.Rows[i];

                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.Tag = nRow["PermitKey"];
                lvi.ForeColor = Color.Navy;

                lvi.BackColor = Color.White;
                lvi.ImageIndex = 0;

                lvsi = new ListViewItem.ListViewSubItem();
                DateTime zPermitDate = (DateTime)nRow["PermitDate"];
                lvsi.Text = zPermitDate.ToString("dd/MM/yyyy");
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["CodeName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["Description"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                DateTime zFromDate = (DateTime)nRow["FromDate"];
                lvsi.Text = zFromDate.ToString("dd/MM/yyyy HH:mm");
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                DateTime zToDate = (DateTime)nRow["ToDate"];
                lvsi.Text = zToDate.ToString("dd/MM/yyyy HH:mm");
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                double zNumberOff = double.Parse(nRow["NumberOff"].ToString().Trim());
                lvsi.Text = Math.Round(zNumberOff, 0).ToString();
                lvi.SubItems.Add(lvsi);


                LV.Items.Add(lvi);
            }

            this.Cursor = Cursors.Default;

        }
        private void LV_Detail_Click(object sender, EventArgs e)
        {
            _AutoKey = LV_Detail.SelectedItems[0].Tag.ToString();
            LoadData();
        }
        #endregion
        private void Btn_Search_Click(object sender, EventArgs e)
        {
                DataTable zTable = Employee_Data.Search(txt_Search.Text.Trim());
                LV_LoadData(zTable);

        }
        private void dte_Month_ValueChanged(object sender, EventArgs e)
        {
            _Date = dte_Month.Value;
        }

        private void Btn_Save_Click(object sender, EventArgs e)
        {
            if(_Key.Length > 0)
            {
                Permit_Info zinfo = new Permit_Info(_AutoKey);
                zinfo.EmployeeKey = _Key;
                zinfo.PermitDate = dte_CheckTime.Value;
                zinfo.FromDate = dte_FromDay.Value;
                zinfo.ToDate = dte_ToDay.Value;
                zinfo.NumberOff = float.Parse(txt_NumberDay.Text);
                zinfo.CodeKey = int.Parse(cbo_Code.SelectedValue.ToString());
                zinfo.Description = txt_Description.Text;
                zinfo.Save();

                if (zinfo.Message == string.Empty)
                {
                    MessageBox.Show("Cập nhật thành công!");
                    LV_LoadData_Detail();
                    SetDefault();
                }
                else
                    MessageBox.Show(zinfo.Message);
            }
            else
            {
                MessageBox.Show("Chưa chọn nhân viên!");
            }
           
        }

        private void dte_ToDay_ValueChanged(object sender, EventArgs e)
        {
            //string zFromDay = "";
            //zFromDay = dte_FromDay.Value.ToString("dd/MM/yyyy");
            //DateTime zFrom = new DateTime();
            //zFrom = DateTime.Parse(zFromDay);
            //string zToDay = "";
            //zToDay = dte_ToDay.Value.ToString("dd/MM/yyyy");
            //DateTime zTo = new DateTime();
            //zTo = DateTime.Parse(zToDay);
            TimeSpan ts = new TimeSpan();

            ts = (TimeSpan)(dte_ToDay.Value - dte_FromDay.Value);
            txt_NumberDay.Text = (ts.Days + 1).ToString();
        }

        private void Btn_Del_Click(object sender, EventArgs e)
        {
            if (_AutoKey.Length == 0)
            {
                MessageBox.Show("Chưa chọn thông tin!");
            }
            else
            {
                if (MessageBox.Show("Bạn có chắc xóa thông tin này ?.", "Thông báo", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    Permit_Info zinfo = new Permit_Info(_AutoKey);
                    zinfo.Delete();
                    if (zinfo.Message == string.Empty)
                    {
                        MessageBox.Show("Đã xóa !");
                        LV_LoadData_Detail();
                        SetDefault();
                    }
                    else
                    {
                        MessageBox.Show(zinfo.Message);
                    }

                }
            }
        }

        private void Btn_New_Click(object sender, EventArgs e)
        {
            SetDefault();
        }
    }
}
