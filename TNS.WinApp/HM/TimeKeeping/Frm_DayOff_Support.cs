﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TNS.HRM;
using TNS.Misc;
using TNS.HRM;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_DayOff_Support : Form
    {
        private DateTime _Date;
        private DateTime _FromDate;
        private DateTime _ToDate;
        #region[Private Tab1]
        DataTable In_Table;
        #endregion
        #region[Private Tab 2]
        int _TeamKey = 0;
        private List<DayOff_Support_Info> _List_DayOff;
        public List<DayOff_Support_Info> List_DayOff
        {
            get
            {
                return _List_DayOff;
            }

            set
            {
                _List_DayOff = value;
            }
        }
        DataTable In_Table_Tab2;
        #endregion
        public Frm_DayOff_Support()
        {
            InitializeComponent();

            dte_Month.CustomFormat = "MM/yyyy";
            dte_Month.Validated += dte_Month_ValueChanged;

            #region[Tab1]
            GV_Employee_Layout();
            btn_Save.Click += btn_Save_Click;
            timer1.Tick += timer1_Tick;
            #endregion

            #region[Tab2]
            GV_Data_Layout();
            btn_Save_Tab2.Click += btn_Save_Tab2_Click;
            LV_List.ItemActivate += LV_List_ItemActivate;
            btn_Done_Tab2.Click += btn_Done_Tab2_Click;
            GV_Data.CellEndEdit += GV_Data_CellEndEdit;
            btn_Hide.Click += btn_Hide_Click;
            #endregion

        }

        private void Frm_DayOff_Support_Load(object sender, EventArgs e)
        {
            dte_Month.Value = SessionUser.Date_Work;
            _Date = dte_Month.Value;
            _FromDate = new DateTime(_Date.Year, _Date.Month, 1, 0, 0, 0);
            _ToDate = _FromDate.AddMonths(1).AddDays(-1);
            _ToDate = new DateTime(_ToDate.Year, _ToDate.Month, _ToDate.Day, 23, 59, 59);
            lbl_Title.Text = "TÍNH NGÀY NGHỈ, HIẾU, HỶ,LỄ, PHÉP NĂM " + dte_Month.Value.ToString("MM/yyyy");

            #region[Tab1]
            In_Table = Employee_Data.Load_Data();
            GV_Employee_LoadData(In_Table);
            #endregion

            #region[Tab2]
            LV_TeamWorking_Layout(LV_List);
            LV_Team_LoadData();

            GB_Log.Visible = false;
            btn_Hide.Tag = "1";
            btn_Hide.Text = "<<";
            panel4.Width = panel5.Width;
            #endregion
        }
        private void dte_Month_ValueChanged(object sender, EventArgs e)
        {
            _Date = dte_Month.Value;
            _FromDate = new DateTime(_Date.Year, _Date.Month, 1, 0, 0, 0);
            _ToDate = _FromDate.AddMonths(1).AddDays(-1);
            _ToDate = new DateTime(_ToDate.Year, _ToDate.Month, _ToDate.Day, 23, 59, 59);
            lbl_Title.Text = "TÍNH NGÀY NGHỈ, HIẾU, HỶ,LỄ, PHÉP NĂM " + dte_Month.Value.ToString("MM/yyyy");
            GV_Data_LoadData();
        }

        #region[Tab1]
        #region [GV_Employee]
        private void GV_Employee_Layout()
        {
            // Setup Column 
            GV_Employee.Columns.Add("No", "STT");
            GV_Employee.Columns.Add("EmployeeID", "Mã NV");
            GV_Employee.Columns.Add("FullName", "Họ và tên");


            GV_Employee.Columns["No"].Width = 40;
            GV_Employee.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV_Employee.Columns["No"].ReadOnly = true;

            GV_Employee.Columns["EmployeeID"].Width = 100;
            GV_Employee.Columns["EmployeeID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV_Employee.Columns["EmployeeID"].ReadOnly = true;


            GV_Employee.Columns["FullName"].Width = 265;
            GV_Employee.Columns["FullName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV_Employee.Columns["FullName"].ReadOnly = true;


            // setup style view

            GV_Employee.BackgroundColor = Color.White;
            GV_Employee.GridColor = Color.FromArgb(227, 239, 255);
            GV_Employee.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV_Employee.DefaultCellStyle.ForeColor = Color.FromArgb(43, 43, 43);
            GV_Employee.DefaultCellStyle.Font = new Font("Arial", 9, FontStyle.Bold);

            GV_Employee.AllowUserToResizeRows = false;
            GV_Employee.AllowUserToResizeColumns = true;

            GV_Employee.RowHeadersVisible = false;

            //// setup Height Header
            //GV_Employee.ColumnHeadersHeight = GV_Employee.ColumnHeadersHeight * 3 / 2;
            GV_Employee.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 9, FontStyle.Bold);
            GV_Employee.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            GV_Employee.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;

            for (int i = 1; i <= 8; i++)
            {
                GV_Employee.Rows.Add();
            }

        }
        private void GV_Employee_LoadData(DataTable InTable)
        {
            GV_Employee.Rows.Clear();

            int i = 0;
            foreach (DataRow nRow in InTable.Rows)
            {
                // int zt = 0;
                GV_Employee.Rows.Add();
                DataGridViewRow nRowView = GV_Employee.Rows[i];
                nRowView.Tag = nRow["EmployeeKey"].ToString().Trim();
                nRowView.Cells["No"].Value = (i + 1).ToString();
                nRowView.Cells["FullName"].Value = nRow["FullName"].ToString().Trim();
                nRowView.Cells["EmployeeID"].Value = nRow["EmployeeID"].ToString().Trim();
                i++;
            }
            txt_Employee.Text = InTable.Rows.Count.ToString();
        }
        #endregion
        int _indexttable = 0;
        private void btn_Save_Click(object sender, EventArgs e)
        {
            _indexttable = 0;
            string t = "";
            t = DayOff_Support_Data.Count_DayOff_Support(_FromDate, _ToDate);
            if (t.ToInt() > 0)
            {
                DialogResult dlr = MessageBox.Show("Chấm công đã tính.Bạn có muốn tính lại không ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlr == DialogResult.Yes)
                {
                    LB_Log.Items.Add("Đang xử xóa dữ liệu " + _Date.ToString("MM/yyyy"));
                    DayOff_Support_Info zinfo = new DayOff_Support_Info();
                    zinfo.Delete(_FromDate, _ToDate);
                    if (zinfo.Message.Substring(0, 2) == "30")
                    {
                        LB_Log.Items.Add("Xóa dữ liệu " + _Date.ToString("MM/yyyy") + " thành công !");
                        timer1.Start();
                        btn_Save.Enabled = false;
                    }
                    else
                    {
                        LB_Log.Items.Add("Xóa dữ liệu " + _Date.ToString("MM/yyyy") + " bị lỗi !");
                    }

                }
            }
            else
            {
                timer1.Start();
                btn_Save.Enabled = false;
            }

        }

        int _Count = 0;

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Stop();
            string zEmployeeKey = In_Table.Rows[_indexttable]["EmployeeKey"].ToString();
            LB_Log.Items.Add("Đang xử lý " + _indexttable.ToString() + " - " + "Mã NV" + ":" + In_Table.Rows[_indexttable]["EmployeeID"].ToString() + " - " + "Tên nhân viên" + ":" + In_Table.Rows[_indexttable]["FullName"].ToString());
            string ztem = DayOff_Support_Data.Insert_DayOff_Month(zEmployeeKey, _FromDate, _ToDate);
            string zteam = "";
            if (ztem == "")
            {
                zteam = "Thất bại !";
            }
            else if (ztem.Substring(0, 2) == "11")
            {
                zteam = "Thêm thành công !";
                _Count++;
            }
            else
            {
                zteam = "Thất bại !";
            }
            LB_Log.Items.Add(zteam);
            _indexttable++;
            if (_indexttable < In_Table.Rows.Count)
                timer1.Start();
            else
            {
                txt_True.Text = _Count.ToString();
                if ((In_Table.Rows.Count - _Count) <= 0)
                    txt_False.Text = "0";
                else
                    txt_False.Text = (In_Table.Rows.Count - _Count).ToString();
                btn_Save.Enabled = true;
                MessageBox.Show("Tính ngày nghỉ lễ, hiếu, hỷ,phép năm hoàn thành!");
            }

        }
        #endregion

        #region[Tab 2]

        #region [Design ListView]
        private void LV_TeamWorking_Layout(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "No";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên Nhóm";
            colHead.Width = 250;
            colHead.TextAlign = HorizontalAlignment.Left;

            LV.Columns.Add(colHead);

        }

        public void LV_Team_LoadData()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LV_List;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            DataTable ztb_Team = new DataTable();

            ztb_Team = Working_Month_Data.List_Team_Product();

            LV.Items.Clear();
            int n = ztb_Team.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.ForeColor = Color.Navy;
                DataRow nRow = ztb_Team.Rows[i];
                lvi.Tag = nRow["TeamKey"].ToString();
                lvi.BackColor = Color.White;

                lvi.ImageIndex = 0;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["TeamName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);

            }
            this.Cursor = Cursors.Default;
            if (n <= 0)
                txt_Team.Text = "0";
            else
                txt_Team.Text = n.ToString();
        }

        #endregion

        private void LV_List_ItemActivate(object sender, EventArgs e)
        {
            for (int i = 0; i < LV_List.Items.Count; i++)
            {
                if (LV_List.Items[i].Selected == true)
                {
                    LV_List.Items[i].BackColor = Color.LightBlue; // highlighted item
                }
                else
                {
                    LV_List.Items[i].BackColor = SystemColors.Window; // normal item
                }
            }
            _TeamKey = int.Parse(LV_List.SelectedItems[0].Tag.ToString());
            lbl_Team.Text = "TÍNH NGÀY NGHỈ NHÓM " + ": " + LV_List.SelectedItems[0].SubItems[1].Text;
            LV_List.Enabled = false;
            GV_Data_LoadData();
        }

        private void GV_Data_Layout()
        {
            // Setup Column 
            GV_Data.Columns.Add("No", "STT");
            GV_Data.Columns.Add("EmployeeID", "Mã NV");
            GV_Data.Columns.Add("EmployeeName", "Họ và tên");
            GV_Data.Columns.Add("SalaryBasic", "Lương cơ bản");
            GV_Data.Columns.Add("Day_Standard", "Ngày tiêu chuẩn ");
            GV_Data.Columns.Add("DayOff", "Số ngày nghỉ");
            GV_Data.Columns.Add("Unit_Price", "Tiền 1 ngày nghỉ");
            GV_Data.Columns.Add("Money", "Số tiền");
            GV_Data.Columns.Add("Description", "Ghi chú");

            GV_Data.Columns["No"].Width = 40;
            GV_Data.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV_Data.Columns["No"].ReadOnly = true;
            GV_Data.Columns["No"].Frozen = true;

            GV_Data.Columns["EmployeeID"].Width = 100;
            GV_Data.Columns["EmployeeID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV_Data.Columns["EmployeeID"].ReadOnly = true;
            GV_Data.Columns["EmployeeID"].Frozen = true;

            GV_Data.Columns["EmployeeName"].Width = 150;
            GV_Data.Columns["EmployeeName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV_Data.Columns["EmployeeName"].ReadOnly = true;
            GV_Data.Columns["EmployeeName"].Frozen = true;

            GV_Data.Columns["SalaryBasic"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Data.Columns["SalaryBasic"].ReadOnly = true;

            GV_Data.Columns["Day_Standard"].Width = 110;
            GV_Data.Columns["Day_Standard"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Data.Columns["DayOff"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Data.Columns["Unit_Price"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Data.Columns["Money"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Data.Columns["Description"].Width = 250;
            GV_Data.Columns["Description"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV_Data.Columns["Description"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            // setup style view

            GV_Data.BackgroundColor = Color.White;
            GV_Data.GridColor = Color.FromArgb(227, 239, 255);
            GV_Data.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV_Data.DefaultCellStyle.ForeColor = Color.FromArgb(43, 43, 43);
            GV_Data.DefaultCellStyle.Font = new Font("Arial", 9, FontStyle.Bold);

            GV_Data.AllowUserToResizeRows = false;
            GV_Data.AllowUserToResizeColumns = true;

            GV_Data.RowHeadersVisible = false;
            GV_Data.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;

            //// setup Height Header
            //GV_Employee.ColumnHeadersHeight = GV_Employee.ColumnHeadersHeight * 3 / 2;
            GV_Data.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 9, FontStyle.Bold);
            GV_Data.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            GV_Data.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;

            for (int i = 1; i <= 8; i++)
            {
                GV_Data.Rows.Add();
            }

        }
        private void GV_Data_LoadData()
        {
            LB_Log2.Items.Add("--------------------------");
            LB_Log2.Items.Add("Đang tải dữ liệu!");
            In_Table_Tab2 = DayOff_Support_Data.List_Data_DayOff_Support_Team(_TeamKey, _FromDate, _ToDate);
            _List_DayOff = new List<DayOff_Support_Info>();
            foreach (DataRow nRow in In_Table_Tab2.Rows)
            {
                DayOff_Support_Info Product = new DayOff_Support_Info(nRow);
                _List_DayOff.Add(Product);
            }

            GV_Data.Rows.Clear();
            for (int i = 1; i < _List_DayOff.Count; i++)
            {
                GV_Data.Rows.Add();
            }
            for (int i = 0; i < _List_DayOff.Count; i++)
            {
                DayOff_Support_Info zWorking = (DayOff_Support_Info)_List_DayOff[i];
                if (zWorking.RecordStatus == 1)
                    GV_Data.Rows[i].DefaultCellStyle.BackColor = Color.LightBlue;
                GV_Data.Rows[i].Tag = zWorking;
                GV_Data.Rows[i].Cells["No"].Value = (i + 1).ToString();
                GV_Data.Rows[i].Cells["EmployeeID"].Value = zWorking.EmployeeID;
                GV_Data.Rows[i].Cells["EmployeeName"].Value = zWorking.EmployeeName;
                GV_Data.Rows[i].Cells["DayOff"].Value = zWorking.DayOff;

                GV_Data.Rows[i].Cells["SalaryBasic"].Value = zWorking.SalaryBasic.ToString("###,###,##0.00");

                GV_Data.Rows[i].Cells["Unit_Price"].Value = zWorking.Unit_Price.ToString("###,###,##0.00");
                GV_Data.Rows[i].Cells["Day_Standard"].Value = zWorking.Day_Standard;

                GV_Data.Rows[i].Cells["Money"].Value = zWorking.Money.ToString("###,###,##0.00");

                GV_Data.Rows[i].Cells["Description"].Value = zWorking.Description;

            }
            LV_List.Enabled = true;
            if (_List_DayOff.Count.ToInt() <= 0)
                txt_Count.Text = "0";
            else
                txt_Count.Text = _List_DayOff.Count.ToString();

            LB_Log2.Items.Add("Tìm được " + _List_DayOff.Count + " kết quả!");
            LB_Log2.Items.Add("Tải dữ liệu thành công!");
        }

        private void GV_Data_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow zRowEdit = GV_Data.Rows[e.RowIndex];
            DayOff_Support_Info zInfo;
            if (GV_Data.Rows[e.RowIndex].Tag == null)
            {
                zInfo = new DayOff_Support_Info();
                GV_Data.Rows[e.RowIndex].Tag = zInfo;
            }
            else
            {
                zInfo = (DayOff_Support_Info)GV_Data.Rows[e.RowIndex].Tag;
            }
            bool zIsChangeValued = false;
            switch (e.ColumnIndex)
            {
                case 4: // thong tin KP
                    if (zRowEdit.Cells[e.ColumnIndex].Value == null)
                    {
                        zInfo.Day_Standard = 0;
                        zIsChangeValued = true;
                    }
                    else
                    {
                        if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() != zInfo.Day_Standard.ToString())
                        {

                            float zDay_Standard;
                            if ((float.TryParse(zRowEdit.Cells["Day_Standard"].Value.ToString(), out zDay_Standard)))
                            {
                                zInfo.Day_Standard = float.Parse(zRowEdit.Cells[e.ColumnIndex].Value.ToString()); //;  
                                zRowEdit.Cells["Day_Standard"].ErrorText = "";
                            }
                            else
                            {
                                zInfo.Day_Standard = 0;
                                zRowEdit.Cells["Day_Standard"].ErrorText = "Sai định dạng!";
                            }
                            zIsChangeValued = true;
                        }
                    }
                    break;
                case 5:
                    if (zRowEdit.Cells[e.ColumnIndex].Value == null)
                    {
                        zInfo.DayOff = 0;
                        zIsChangeValued = true;
                    }
                    else
                    {
                        if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() != zInfo.DayOff.ToString())
                        {

                            float zDayOff;
                            if ((float.TryParse(zRowEdit.Cells["DayOff"].Value.ToString(), out zDayOff)))
                            {
                                zInfo.DayOff = float.Parse(zRowEdit.Cells[e.ColumnIndex].Value.ToString()); //;  
                                zRowEdit.Cells["DayOff"].ErrorText = "";
                            }
                            else
                            {
                                zInfo.DayOff = 0;
                                zRowEdit.Cells["DayOff"].ErrorText = "Sai định dạng!";
                            }
                            zIsChangeValued = true;
                        }
                    }
                    break;
                case 8:
                    if (zRowEdit.Cells[e.ColumnIndex].Value == null)
                    {
                        zInfo.Description = "";
                        zIsChangeValued = true;
                    }
                    else
                    {
                        if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() != zInfo.Description.ToString())
                        {


                            zInfo.Description = zRowEdit.Cells[e.ColumnIndex].Value.ToString(); //;  
                            zRowEdit.Cells["Description"].ErrorText = "";
                            zIsChangeValued = true;
                        }
                    }
                    break;


            }
            if (zIsChangeValued)
            {
                zInfo.RecordStatus = 1;
                CaculateAmount(e.RowIndex);
                ShowProductInGridView(e.RowIndex);
            }
            if (zInfo.Key != 0)
                zInfo.RecordStatus = 2;
            else
                zInfo.RecordStatus = 1;
        }
        private void CaculateAmount(int RowIndex)
        {
            DayOff_Support_Info zInfo = (DayOff_Support_Info)GV_Data.Rows[RowIndex].Tag;


            double zSalaryBasic = zInfo.SalaryBasic;
            double zStandard = zInfo.Day_Standard;
            double zDayOff = zInfo.DayOff;
            double zUnit_Price = 0;
            double zMoney = 0;
            string zDescription = zInfo.Description;

            zUnit_Price = zSalaryBasic / zStandard;
            zInfo.Unit_Price = Math.Round(zUnit_Price, 2);

            zMoney = Math.Round(zUnit_Price, 2) * zDayOff;
            zInfo.Money = Math.Round(zMoney, 0);

        }
        private void ShowProductInGridView(int RowIndex)
        {
            DataGridViewRow zRowView = GV_Data.Rows[RowIndex];
            DayOff_Support_Info zProduct = (DayOff_Support_Info)zRowView.Tag;
            zRowView.Cells["No"].Value = (RowIndex + 1).ToString();
            zRowView.Cells["No"].Tag = zProduct.Key;

            zRowView.Cells["EmployeeID"].Value = zProduct.EmployeeID;
            zRowView.Cells["EmployeeName"].Value = zProduct.EmployeeName;
            if (zProduct.DayOff == 0)
                zRowView.Cells["DayOff"].Value = "0";
            else
                zRowView.Cells["DayOff"].Value = zProduct.DayOff;


            zRowView.Cells["SalaryBasic"].Value = zProduct.SalaryBasic.ToString("###,###,##0.00");

            zRowView.Cells["Unit_Price"].Value = zProduct.Unit_Price.ToString("###,###,##0.00");

            zRowView.Cells["Day_Standard"].Value = zProduct.Day_Standard;

            zRowView.Cells["Money"].Value = zProduct.Money.ToString("###,###,##0.00");

            zRowView.Cells["Description"].Value = zProduct.Description;
        }

        #region[Save_butonSavetab2]
        int _index1 = 0;
        private void btn_Save_Tab2_Click(object sender, EventArgs e)
        {
            if(In_Table_Tab2.Rows.Count==0)
            {
                MessageBox.Show("Không tìm thấy dữ liệu!");     
            }
            else
            {
                DialogResult dlr = MessageBox.Show("Bạn có chắc thay đổi số liệu không ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlr == DialogResult.Yes)
                {
                    ButonHide();
                    LB_Log2.Items.Add("--------------------------");
                    LB_Log2.Items.Add("Đang xử lý dữ liệu!");
                    _List_DayOff = new List<DayOff_Support_Info>();
                    DayOff_Support_Info zInfo;
                    for (int i = 0; i < GV_Data.Rows.Count; i++)
                    {
                        if (GV_Data.Rows[i].Tag != null)
                        {
                            zInfo = (DayOff_Support_Info)GV_Data.Rows[i].Tag;
                            _List_DayOff.Add(zInfo);
                        }
                    }
                    _index1 = 0;
                    timer2.Start();


                }
            }
           

        }
        private void timer2_Tick(object sender, EventArgs e)
        {
            timer2.Stop();
            string zMessage = "";
            DayOff_Support_Info zDayOff = (DayOff_Support_Info)_List_DayOff[_index1];
            zDayOff.ModifiedBy = SessionUser.UserLogin.EmployeeKey;
            zDayOff.ModifiedName = SessionUser.UserLogin.EmployeeName;
            LB_Log2.Items.Add("-------------");
            LB_Log2.Items.Add("Đang xử lý :" + zDayOff.EmployeeID + "!");
            switch (zDayOff.RecordStatus)
            {
                case 2:
                    zDayOff.Update();
                    if (zDayOff.Message == "")
                        LB_Log2.Items.Add("Cập nhật :" + zDayOff.EmployeeID + " lỗi!");
                    else if (zDayOff.Message.Substring(0, 2) == "11" || zDayOff.Message.Substring(0, 2) == "20")
                    {
                        LB_Log2.Items.Add("Cập nhật :" + zDayOff.EmployeeID + " thành công!");
                    }
                    else
                    {
                        LB_Log2.Items.Add("Cập nhật :" + zDayOff.EmployeeID + " lỗi!");
                    }
                    break;
            }
            if(zDayOff.RecordStatus == 1|| zDayOff.RecordStatus == 0)
            LB_Log2.Items.Add("" + zDayOff.EmployeeID + ": Không thay đổi!");

            zMessage += TN_Message.Show(zDayOff.Message);

            _index1++;
            if (_index1 < _List_DayOff.Count)
                timer2.Start();
            else
            {
                if (zMessage.Length == 0)
                {

                    MessageBox.Show("Câp nhật thành công", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    LB_Log2.Items.Add("Cập nhật dữ liệu thành công!");
                    GV_Data_LoadData();
                }
                else
                {
                    MessageBox.Show("Vui lòng kiểm tra lại", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    LB_Log2.Items.Add("Cập nhật dữ liệu thất bại!");
                }
                LB_Log2.Items.Add("--------------------------");
                ButonShow();
            }

        }
        #endregion

        #region[button Done 2]
        private void btn_Done_Tab2_Click(object sender, EventArgs e)
        {
            _index = 0;
            if(In_Table_Tab2.Rows.Count ==0)
            {
                MessageBox.Show("Không tìm thấy dữ liệu!");
            }
            else
            {
                ButonHide();
                LB_Log2.Items.Add("Đang Chốt dữ liệu!");
                timer3.Start();
            }

        }
        int _index = 0;
        private void timer3_Tick(object sender, EventArgs e)
        {

            timer3.Stop();
            string zMessage = "";
            string zEmployeeKey = In_Table_Tab2.Rows[_index]["EmployeeKey"].ToString();
            int zCount = Salary_Product_Worker_Data.Find_Employee(zEmployeeKey, _Date);
            LB_Log2.Items.Add("Đang chốt : " + In_Table_Tab2.Rows[_index]["EmployeeID"].ToString() + "");
            Salary_Product_Worker_Info zSalary = new Salary_Product_Worker_Info();
            if (zCount == 0)
            {
                zSalary.SalaryMonth = _Date;
                zSalary.EmployeeKey = zEmployeeKey;
                zSalary.EmployeeID = In_Table_Tab2.Rows[_index]["EmployeeID"].ToString();
                zSalary.EmployeeName = In_Table_Tab2.Rows[_index]["EmployeeName"].ToString();

                zSalary.NumberOffHoliday = float.Parse(In_Table_Tab2.Rows[_index]["DayOff"].ToString());
                zSalary.SalaryOffHoliday = double.Parse(In_Table_Tab2.Rows[_index]["Money"].ToString());

                zSalary.Create_Holiday();
                zMessage += TN_Message.Show(zSalary.Message);
                LB_Log2.Items.Add("Đã chốt " + In_Table_Tab2.Rows[_index]["EmployeeID"].ToString() + "!");
            }
            if (zCount > 0)
            {
                zSalary.SalaryMonth = _Date;
                zSalary.EmployeeKey = zEmployeeKey;
                zSalary.EmployeeID = In_Table_Tab2.Rows[_index]["EmployeeID"].ToString();
                zSalary.EmployeeName = In_Table_Tab2.Rows[_index]["EmployeeName"].ToString();
                zSalary.NumberOffHoliday = float.Parse(In_Table_Tab2.Rows[_index]["DayOff"].ToString());
                zSalary.SalaryOffHoliday = double.Parse(In_Table_Tab2.Rows[_index]["Money"].ToString());

                zSalary.Update_Holiday();
                zMessage += TN_Message.Show(zSalary.Message);
                LB_Log2.Items.Add("Đã chốt " + In_Table_Tab2.Rows[_index]["EmployeeID"].ToString() + "");
                LB_Log2.Items.Add("-------------------");
            }
            _index++;
            if (_index < In_Table_Tab2.Rows.Count)
                timer3.Start();
            else
            {
                if (zMessage.Length == 0)
                {
                    MessageBox.Show("Câp nhật thành công", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    LB_Log2.Items.Add("Đã chốt thành công!");
                    ButonShow();
                }
                else
                {
                    MessageBox.Show("Vui lòng kiểm tra lại", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    LB_Log2.Items.Add("Chốt không thành công!");
                    ButonShow();
                }
            }
        }
        #endregion

        private void btn_Hide_Click(object sender, EventArgs e)
        {
            if (btn_Hide.Tag == "0")
            {
                GB_Log.Visible = false;
                btn_Hide.Tag = "1";
                btn_Hide.Text = "<<";
                panel4.Width = panel5.Width;
            }
            else
            {
                GB_Log.Visible = true;
                btn_Hide.Tag = "0";
                btn_Hide.Text = ">>";
                panel4.Width = this.panel5.Width - 192;
            }

        }

        private void ButonShow()
        {
            btn_Save_Tab2.Enabled = true;
            btn_Done_Tab2.Enabled = true;
        }
        private void ButonHide()
        {
            btn_Save_Tab2.Enabled = false;
            btn_Done_Tab2.Enabled = false;
        }
        #endregion



    }
}
