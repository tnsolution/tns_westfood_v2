﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.HRM;
using TNS.Misc;
using TNS.HRM;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Caculator_Working : Form
    {
        private DateTime _Date;
        private DateTime _FromDate;
        private DateTime _ToDate;
        #region[Private Tab1]
        DataTable In_Table;
        #endregion
        #region[Private Tab 2]
        int _TeamKey = 0;
        private List<Working_Month_Info> _List_Worker;
        public List<Working_Month_Info> List_Worker
        {
            get
            {
                return _List_Worker;
            }

            set
            {
                _List_Worker = value;
            }
        }
        DataTable In_Table_Tab2;
        #endregion
        #region[Private Tab 3]
        //int _BranchKey = 0;
        //private List<Working_Month_Info> _List_Office;
        //public List<Working_Month_Info> List_Office
        //{
        //    get
        //    {
        //        return _List_Office;
        //    }

        //    set
        //    {
        //        _List_Office = value;
        //    }
        //}
        //DataTable In_Table_Tab3;
        #endregion
        public Frm_Caculator_Working()
        {
            InitializeComponent();
            dte_Month.Validated += dte_Month_ValueChanged;

            #region[Tab1]
            GV_Employee_Layout();
            btn_Save.Click += btn_Save_Click;
            timer1.Tick += timer1_Tick;
            #endregion

            #region[Tab2]
            GV_Data_Layout();
            btn_Save_Tab2.Click += btn_Save_Tab2_Click;
            LV_List.Click += LV_List_Click;
            btn_Done_Tab2.Click += btn_Done_Tab2_Click;
            #endregion
            #region[Tab3]
            //GV_Office_Layout();
            //btn_Save_Tab3.Click += btn_Save_Tab3_Click;
            //LV_Office.ItemActivate += LV_Office_ItemActivate;
            //btn_Done_Tab3.Click += btn_Done_Tab3_Click;
            #endregion

        }

        private void Frm_Caculator_Working_Load(object sender, EventArgs e)
        {
            btn_Save_Tab2.Visible = false;
            dte_Month.Value = SessionUser.Date_Work;
            _Date = dte_Month.Value;
            _FromDate = new DateTime(_Date.Year, _Date.Month, 1, 0, 0, 0);
            _ToDate = _FromDate.AddMonths(1).AddDays(-1);
            _ToDate = new DateTime(_ToDate.Year, _ToDate.Month, _ToDate.Day, 23, 59, 59);
            lbl_Title.Text = "TÍNH CÔNG THÁNG " + dte_Month.Value.ToString("MM/yyyy");

            #region[Tab1]
            In_Table = Employee_Data.Load_Data();
            GV_Employee_LoadData(In_Table);
            #endregion

            #region[Tab2]
            LV_TeamWorking_Layout(LV_List);
            LV_Team_LoadData();
            #endregion
            #region[Tab3]
            //LV_Office_Layout(LV_Office);
            //LV_Office_LoadData();
            #endregion
        }
        private void dte_Month_ValueChanged(object sender, EventArgs e)
        {
            _Date = dte_Month.Value;
            _FromDate = new DateTime(_Date.Year, _Date.Month, 1, 0, 0, 0);
            _ToDate = _FromDate.AddMonths(1).AddDays(-1);
            _ToDate = new DateTime(_ToDate.Year, _ToDate.Month, _ToDate.Day, 23, 59, 59);
            lbl_Title.Text = "TÍNH CÔNG THÁNG " + dte_Month.Value.ToString("MM/yyyy");
            GV_Data_LoadData();
            GV_Office_LoadData();
        }

        #region[Tab1]
        #region [GV_Employee]
        private void GV_Employee_Layout()
        {
            // Setup Column 
            GV_Employee.Columns.Add("No", "STT");
            GV_Employee.Columns.Add("EmployeeID", "Mã NV");
            GV_Employee.Columns.Add("FullName", "Họ và tên");


            GV_Employee.Columns["No"].Width = 40;
            GV_Employee.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV_Employee.Columns["No"].ReadOnly = true;

            GV_Employee.Columns["EmployeeID"].Width = 100;
            GV_Employee.Columns["EmployeeID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV_Employee.Columns["EmployeeID"].ReadOnly = true;


            GV_Employee.Columns["FullName"].Width = 265;
            GV_Employee.Columns["FullName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV_Employee.Columns["FullName"].ReadOnly = true;


            // setup style view

            GV_Employee.BackgroundColor = Color.White;
            GV_Employee.GridColor = Color.FromArgb(227, 239, 255);
            GV_Employee.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV_Employee.DefaultCellStyle.ForeColor = Color.FromArgb(43, 43, 43);
            GV_Employee.DefaultCellStyle.Font = new Font("Arial", 9, FontStyle.Bold);

            GV_Employee.AllowUserToResizeRows = false;
            GV_Employee.AllowUserToResizeColumns = true;

            GV_Employee.RowHeadersVisible = false;

            //// setup Height Header
            //GV_Employee.ColumnHeadersHeight = GV_Employee.ColumnHeadersHeight * 3 / 2;
            GV_Employee.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 9, FontStyle.Bold);
            GV_Employee.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            GV_Employee.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;

            for (int i = 1; i <= 8; i++)
            {
                GV_Employee.Rows.Add();
            }

        }
        private void GV_Employee_LoadData(DataTable InTable)
        {
            GV_Employee.Rows.Clear();

            int i = 0;
            foreach (DataRow nRow in InTable.Rows)
            {
                // int zt = 0;
                GV_Employee.Rows.Add();
                DataGridViewRow nRowView = GV_Employee.Rows[i];
                nRowView.Tag = nRow["EmployeeKey"].ToString().Trim();
                nRowView.Cells["No"].Value = (i + 1).ToString();
                nRowView.Cells["FullName"].Value = nRow["FullName"].ToString().Trim();
                nRowView.Cells["EmployeeID"].Value = nRow["EmployeeID"].ToString().Trim();
                i++;
            }
            txt_Employee.Text = InTable.Rows.Count.ToString();
        }
        #endregion
        int _indexttable = 0;
        private void btn_Save_Click(object sender, EventArgs e)
        {
            _indexttable = 0;
            string t = "";
            t = Working_Month_Data.Count_WorkingMonth(_FromDate, _ToDate);
            if (t.ToInt() > 0)
            {
                DialogResult dlr = MessageBox.Show("Chấm công đã tính.Bạn có muốn tính lại không ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlr == DialogResult.Yes)
                {
                    LB_Log.Items.Add("Đang xử xóa dữ liệu " + _Date.ToString("MM/yyyy"));
                    Working_Month_Info zinfo = new Working_Month_Info();
                    zinfo.Delete(_FromDate, _ToDate);
                    if (zinfo.Message.Substring(0, 2) == "30")
                    {
                        LB_Log.Items.Add("Xóa dữ liệu " + _Date.ToString("MM/yyyy") + " thành công !");
                        timer1.Start();
                        btn_Save.Enabled = false;
                    }
                    else
                    {
                        LB_Log.Items.Add("Xóa dữ liệu " + _Date.ToString("MM/yyyy") + " bị lỗi !");
                    }

                }
            }
            else
            {
                timer1.Start();
                btn_Save.Enabled = false;
            }

        }

        int _Count = 0;


        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Stop();
            string zEmployeeKey = In_Table.Rows[_indexttable]["EmployeeKey"].ToString();
            LB_Log.Items.Add("Đang xử lý " + _indexttable.ToString() + " - " + "Mã NV" + ":" + In_Table.Rows[_indexttable]["EmployeeID"].ToString() + " - " + "Tên nhân viên" + ":" + In_Table.Rows[_indexttable]["FullName"].ToString());
            string ztem = Working_Month_Data.Insert_Working_Month(zEmployeeKey, _FromDate, _ToDate);
            string zteam = "";
            if (ztem.Substring(0, 2) == "11")
            {
                zteam = "Thêm thành công !";
                _Count++;
            }
            else
            {
                zteam = "Thất bại !";
            }
            LB_Log.Items.Add(zteam);
            _indexttable++;
            if (_indexttable < In_Table.Rows.Count)
                timer1.Start();
            else
            {
                txt_True.Text = _Count.ToString();
                if ((In_Table.Rows.Count - _Count) <= 0)
                    txt_False.Text = "0";
                else
                    txt_False.Text = (In_Table.Rows.Count - _Count).ToString();
                btn_Save.Enabled = true;
                MessageBox.Show("Tính công hoàn thành!");
            }

        }
        #endregion

        #region[Tab 2]

        #region [Design ListView]
        private void LV_TeamWorking_Layout(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "No";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên Nhóm";
            colHead.Width = 250;
            colHead.TextAlign = HorizontalAlignment.Left;
            
            LV.Columns.Add(colHead);
            
        }

        public void LV_Team_LoadData()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LV_List;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            DataTable ztb_Team = new DataTable();

            ztb_Team = Working_Month_Data.List_Team_Product();

            LV.Items.Clear();
            int n = ztb_Team.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.ForeColor = Color.Navy;
                DataRow nRow = ztb_Team.Rows[i];
                lvi.Tag = nRow["TeamKey"].ToString();
                lvi.BackColor = Color.White;

                lvi.ImageIndex = 0;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["TeamName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);

            }
            this.Cursor = Cursors.Default;
            if (n <= 0)
                txt_Team.Text = "0";
            else
                txt_Team.Text = n.ToString();
        }

        #endregion

        private void LV_List_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < LV_List.Items.Count; i++)
            {
                if (LV_List.Items[i].Selected == true)
                {
                    LV_List.Items[i].BackColor = Color.LightBlue; // highlighted item
                }
                else
                {
                    LV_List.Items[i].BackColor = SystemColors.Window; // normal item
                }
            }
            _TeamKey = int.Parse(LV_List.SelectedItems[0].Tag.ToString());
            lbl_Team.Text = "TÍNH CÔNG NHÓM " + ": " + LV_List.SelectedItems[0].SubItems[1].Text;
            LV_List.Enabled = false;
            GV_Data_LoadData();
        }

        private void GV_Data_Layout()
        {
            // Setup Column 
            GV_Data.Columns.Add("No", "STT");
            GV_Data.Columns.Add("EmployeeID", "Mã NV");
            GV_Data.Columns.Add("EmployeeName", "Họ và tên");
            GV_Data.Columns.Add("Date_K", "Tính công");
            GV_Data.Columns.Add("Date_TRE", "Đi trể");
            GV_Data.Columns.Add("Date_VE0", "Về sớm");
            GV_Data.Columns.Add("Date_RCG", "Ra cổng ngoài ");
            GV_Data.Columns.Add("Date_P", "Nghỉ phép");
            GV_Data.Columns.Add("Date_KP", "Nghỉ không phép");
            GV_Data.Columns.Add("Date_PT", "Nghỉ phép trừ");
            GV_Data.Columns.Add("Date_LP", "Nghỉ luân phiên");

            GV_Data.Columns.Add("Date_different", "Loại nghỉ khác");
            GV_Data.Columns.Add("Overtime", "Giờ dư");
            GV_Data.Columns.Add("OverMoney", "Tiền ngoài giờ");
            GV_Data.Columns.Add("Number_Rice", "Số phần cơm");
            GV_Data.Columns.Add("Money_Rice", "Tiền cơm");
            GV_Data.Columns.Add("Money_Rice_PTKP", "Tiền PT");
            GV_Data.Columns.Add("Date_Working", "Tổng ngày làm ");
            GV_Data.Columns.Add("Total_Time", "Tổng giờ làm ");
            GV_Data.Columns.Add("Date_Off", "Tổng ngày nghỉ");

            GV_Data.Columns["No"].Width = 40;
            GV_Data.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV_Data.Columns["No"].ReadOnly = true;
            GV_Data.Columns["No"].Frozen = true;

            GV_Data.Columns["EmployeeID"].Width = 100;
            GV_Data.Columns["EmployeeID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV_Data.Columns["EmployeeID"].ReadOnly = true;
            GV_Data.Columns["EmployeeID"].Frozen = true;

            GV_Data.Columns["EmployeeName"].Width = 150;
            GV_Data.Columns["EmployeeName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV_Data.Columns["EmployeeName"].ReadOnly = true;
            GV_Data.Columns["EmployeeName"].Frozen = true;



            GV_Data.Columns["Date_K"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Data.Columns["Date_TRE"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Data.Columns["Date_VE0"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Data.Columns["Date_RCG"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Data.Columns["Date_P"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Data.Columns["Date_KP"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Data.Columns["Date_LP"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Data.Columns["Date_PT"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Data.Columns["Date_different"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Data.Columns["Overtime"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Data.Columns["OverMoney"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Data.Columns["Date_Working"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Data.Columns["Date_Off"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Data.Columns["Number_Rice"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Data.Columns["Money_Rice"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Data.Columns["Money_Rice_PTKP"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Data.Columns["Total_Time"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;


            // setup style view

            GV_Data.BackgroundColor = Color.White;
            GV_Data.GridColor = Color.FromArgb(227, 239, 255);
            GV_Data.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV_Data.DefaultCellStyle.ForeColor = Color.FromArgb(43, 43, 43);
            GV_Data.DefaultCellStyle.Font = new Font("Arial", 9, FontStyle.Bold);

            GV_Data.AllowUserToResizeRows = false;
            GV_Data.AllowUserToResizeColumns = true;

            GV_Data.RowHeadersVisible = false;

            //// setup Height Header
            //GV_Employee.ColumnHeadersHeight = GV_Employee.ColumnHeadersHeight * 3 / 2;
            GV_Data.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 9, FontStyle.Bold);
            GV_Data.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            GV_Data.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;

            for (int i = 1; i <= 8; i++)
            {
                GV_Data.Rows.Add();
            }

        }
        private void GV_Data_LoadData()
        {
            In_Table_Tab2 = Working_Month_Data.List_Data_Worker_Team(_TeamKey, _FromDate, _ToDate);
            _List_Worker = new List<Working_Month_Info>();
            foreach (DataRow nRow in In_Table_Tab2.Rows)
            {
                Working_Month_Info Product = new Working_Month_Info(nRow);
                _List_Worker.Add(Product);
            }

            GV_Data.Rows.Clear();
            for (int i = 1; i < _List_Worker.Count; i++)
            {
                GV_Data.Rows.Add();
            }
            for (int i = 0; i < _List_Worker.Count; i++)
            {
                Working_Month_Info zWorking = (Working_Month_Info)_List_Worker[i];
                if (zWorking.RecordStatus == 1)
                    GV_Data.Rows[i].DefaultCellStyle.BackColor = Color.Khaki;
                GV_Data.Rows[i].Tag = zWorking.Key;
                GV_Data.Rows[i].Cells["No"].Value = (i + 1).ToString();
                GV_Data.Rows[i].Cells["EmployeeID"].Value = zWorking.EmployeeID;
                GV_Data.Rows[i].Cells["EmployeeName"].Value = zWorking.EmployeeName;
                GV_Data.Rows[i].Cells["Date_K"].Value = zWorking.Date_K;
                GV_Data.Rows[i].Cells["Date_TRE"].Value = zWorking.Date_TRE;
                GV_Data.Rows[i].Cells["Date_VE0"].Value = zWorking.Date_VE0;
                GV_Data.Rows[i].Cells["Date_RCG"].Value = zWorking.Date_RCG;
                GV_Data.Rows[i].Cells["Date_P"].Value = zWorking.Date_P;
                GV_Data.Rows[i].Cells["Date_KP"].Value = zWorking.Date_KP;
                GV_Data.Rows[i].Cells["Date_LP"].Value = zWorking.Date_LP;
                GV_Data.Rows[i].Cells["Date_PT"].Value = zWorking.Date_PT;
                GV_Data.Rows[i].Cells["Date_different"].Value = zWorking.Date_different;
                GV_Data.Rows[i].Cells["Overtime"].Value = Math.Round(zWorking.Overtime,2);
                if (zWorking.OverMoney == 0)
                    GV_Data.Rows[i].Cells["OverMoney"].Value = "0";
                else
                GV_Data.Rows[i].Cells["OverMoney"].Value = zWorking.OverMoney.ToString("###,####,###");
                GV_Data.Rows[i].Cells["Date_Working"].Value = zWorking.Date_Working;
                GV_Data.Rows[i].Cells["Date_Off"].Value = zWorking.Date_Off;
                GV_Data.Rows[i].Cells["Number_Rice"].Value = zWorking.Number_Rice;
                if (zWorking.Money_Rice == 0)
                    GV_Data.Rows[i].Cells["Money_Rice"].Value = "0";
                else
                    GV_Data.Rows[i].Cells["Money_Rice"].Value = zWorking.Money_Rice.ToString("###,####,###");
                if (zWorking.Money_Rice_PTKP == 0)
                    GV_Data.Rows[i].Cells["Money_Rice_PTKP"].Value = "0";
                else
                    GV_Data.Rows[i].Cells["Money_Rice_PTKP"].Value = zWorking.Money_Rice_PTKP.ToString("###,####,###");
                GV_Data.Rows[i].Cells["Total_Time"].Value = Math.Round(zWorking.Total_Time, 2);
            }
            LV_List.Enabled = true;
            if (_List_Worker.Count.ToInt() <= 0)
                txt_Count.Text = "0";
            else
                txt_Count.Text = _List_Worker.Count.ToString();
        }

        private void GV_Data_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            //DataGridViewRow zRowEdit = GV_Data.Rows[e.RowIndex];
            //Working_Month_Info zWorker = (Working_Month_Info)GV_Data.Rows[e.RowIndex].Tag;
            //if (GV_Data.Rows[e.RowIndex].Tag == null)
            //{
            //    zWorker = new Working_Month_Info();
            //    GV_Data.Rows[e.RowIndex].Tag = zWorker;
            //}
            //else
            //{
            //    zWorker = (Working_Month_Info)GV_Data.Rows[e.RowIndex].Tag;
            //}
            //bool zIsChangeValued = false;
            //switch (e.ColumnIndex)
            //{
            //    case 3: // thong tin ve K
            //        if (zRowEdit.Cells[e.ColumnIndex].Value == null)
            //        {
            //            zWorker.Date_K = 0;
            //            zIsChangeValued = true;
            //        }
            //        else
            //        {
            //            if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() != zWorker.Date_K.ToString())
            //            {
            //                float zDate_K;
            //                if ((float.TryParse(zRowEdit.Cells["Date_K"].Value.ToString(), out zDate_K)))
            //                {
            //                    zWorker.Date_K = float.Parse(zRowEdit.Cells[e.ColumnIndex].Value.ToString()); //;  
            //                    zRowEdit.Cells["Date_K"].ErrorText = "";
            //                }
            //                else
            //                {
            //                    zWorker.Date_K = 0;
            //                    zRowEdit.Cells["Date_K"].ErrorText = "Sai định dạng!";
            //                }
            //                zIsChangeValued = true;
            //            }
            //        }
            //        break;
            //    case 4: // Thong tin ve D
            //        if (zRowEdit.Cells[e.ColumnIndex].Value == null)
            //        {
            //            zWorker.Date_D = 0;
            //            zIsChangeValued = true;
            //        }
            //        else
            //        {
            //            if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() != zWorker.Date_D.ToString())
            //            {
            //                float zDate_D;
            //                if ((float.TryParse(zRowEdit.Cells["Date_P"].Value.ToString(), out zDate_D)))
            //                {
            //                    zWorker.Date_D = float.Parse(zRowEdit.Cells[e.ColumnIndex].Value.ToString()); //;  
            //                    zRowEdit.Cells["Date_D"].ErrorText = "";
            //                }
            //                else
            //                {
            //                    zWorker.Date_D = 0;
            //                    zRowEdit.Cells["Date_D"].ErrorText = "Sai định dạng!";
            //                }
            //                zIsChangeValued = true;
            //            }
            //        }
            //        break;
            //    case 5: // P
            //        if (zRowEdit.Cells[e.ColumnIndex].Value == null)
            //        {
            //            zWorker.Date_P = 0;
            //            zIsChangeValued = true;
            //        }
            //        else
            //        {
            //            if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() != zWorker.Date_P.ToString())
            //            {
            //                float zDate_P;
            //                if ((float.TryParse(zRowEdit.Cells["Date_P"].Value.ToString(), out zDate_P)))
            //                {
            //                    zWorker.Date_P = float.Parse(zRowEdit.Cells[e.ColumnIndex].Value.ToString()); //;  
            //                    zRowEdit.Cells["Date_P"].ErrorText = "";
            //                }
            //                else
            //                {
            //                    zWorker.Date_P = 0;
            //                    zRowEdit.Cells["Date_P"].ErrorText = "Sai định dạng!";
            //                }
            //                zIsChangeValued = true;
            //            }
            //        }
            //        break;
            //    case 6: // thong tin KP
            //        if (zRowEdit.Cells[e.ColumnIndex].Value == null)
            //        {
            //            zWorker.Date_KP = 0;
            //            zIsChangeValued = true;
            //        }
            //        else
            //        {
            //            if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() != zWorker.Date_KP.ToString())
            //            {

            //                float zDate_KP;
            //                if ((float.TryParse(zRowEdit.Cells["Date_KP"].Value.ToString(), out zDate_KP)))
            //                {
            //                    zWorker.Date_KP = float.Parse(zRowEdit.Cells[e.ColumnIndex].Value.ToString()); //;  
            //                    zRowEdit.Cells["Date_KP"].ErrorText = "";
            //                }
            //                else
            //                {
            //                    zWorker.Date_KP = 0;
            //                    zRowEdit.Cells["Date_KP"].ErrorText = "Sai định dạng!";
            //                }
            //                zIsChangeValued = true;
            //            }
            //        }

            //        break;
            //    case 7: // thong tin LP
            //        if (zRowEdit.Cells[e.ColumnIndex].Value == null)
            //        {
            //            zWorker.Date_LP = 0;
            //            zIsChangeValued = true;
            //        }
            //        else
            //        {
            //            if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() != zWorker.Date_LP.ToString())
            //            {

            //                float zDate_LP;
            //                if ((float.TryParse(zRowEdit.Cells["Date_LP"].Value.ToString(), out zDate_LP)))
            //                {
            //                    zWorker.Date_LP = float.Parse(zRowEdit.Cells[e.ColumnIndex].Value.ToString()); //;  
            //                    zRowEdit.Cells["Date_LP"].ErrorText = "";
            //                }
            //                else
            //                {
            //                    zWorker.Date_LP = 0;
            //                    zRowEdit.Cells["Date_LP"].ErrorText = "Sai định dạng!";
            //                }
            //                zIsChangeValued = true;
            //            }
            //        }

            //        break;
            //    case 8: // thong tin ve ghi chu
            //        if (zRowEdit.Cells[e.ColumnIndex].Value == null)
            //        {
            //            zWorker.Date_PT = 0;
            //            zIsChangeValued = true;
            //        }
            //        else
            //        {
            //            if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() != zWorker.Date_PT.ToString())
            //            {

            //                float zDate_PT;
            //                if ((float.TryParse(zRowEdit.Cells["Date_PT"].Value.ToString(), out zDate_PT)))
            //                {
            //                    zWorker.Date_PT = float.Parse(zRowEdit.Cells[e.ColumnIndex].Value.ToString()); //;  
            //                    zRowEdit.Cells["Date_PT"].ErrorText = "";
            //                }
            //                else
            //                {
            //                    zWorker.Date_PT = 0;
            //                    zRowEdit.Cells["Date_PT"].ErrorText = "Sai định dạng!";
            //                }
            //                zIsChangeValued = true;
            //            }
            //        }
            //        break;
            //    case 12: // thong tin Different
            //        if (zRowEdit.Cells[e.ColumnIndex].Value == null)
            //        {
            //            zWorker.Date_different = 0;
            //            zIsChangeValued = true;
            //        }
            //        else
            //        {
            //            if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() != zWorker.Date_different.ToString())
            //            {

            //                float zOvertime;
            //                if ((float.TryParse(zRowEdit.Cells["Overtime"].Value.ToString(), out zOvertime)))
            //                {
            //                    zWorker.Date_different = float.Parse(zRowEdit.Cells[e.ColumnIndex].Value.ToString());
            //                    zRowEdit.Cells["Overtime"].ErrorText = "";
            //                }
            //                else
            //                {
            //                    zWorker.Date_different = 0;
            //                    zRowEdit.Cells["Overtime"].ErrorText = "Sai định dạng!";
            //                }
            //                zIsChangeValued = true;
            //            }
            //        }
            //        break;
            //    case 13: // thong tin Different
            //        if (zRowEdit.Cells[e.ColumnIndex].Value == null)
            //        {
            //            zWorker.Overtime = 0;
            //            zIsChangeValued = true;
            //        }
            //        else
            //        {
            //            if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() != zWorker.Overtime.ToString())
            //            {

            //                float zOvertime;
            //                if ((float.TryParse(zRowEdit.Cells["Overtime"].Value.ToString(), out zOvertime)))
            //                {
            //                    zWorker.Overtime = float.Parse(zRowEdit.Cells[e.ColumnIndex].Value.ToString());
            //                    zRowEdit.Cells["Overtime"].ErrorText = "";
            //                }
            //                else
            //                {
            //                    zWorker.Overtime = 0;
            //                    zRowEdit.Cells["Overtime"].ErrorText = "Sai định dạng!";
            //                }
            //                zIsChangeValued = true;
            //            }
            //        }
            //        break;
            //    case 14: // thong tin Different
            //        if (zRowEdit.Cells[e.ColumnIndex].Value == null)
            //        {
            //            zWorker.OverMoney = 0;
            //            zIsChangeValued = true;
            //        }
            //        else
            //        {

            //            if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() != zWorker.OverMoney.ToString())
            //            {
            //                float zOverMoney;
            //                if ((float.TryParse(zRowEdit.Cells["OverMoney"].Value.ToString(), out zOverMoney)))
            //                {
            //                    zWorker.OverMoney = float.Parse(zRowEdit.Cells[e.ColumnIndex].Value.ToString()); //;
            //                    zRowEdit.Cells["OverMoney"].ErrorText = "";
            //                }
            //                else
            //                {
            //                    zWorker.OverMoney = 0;
            //                    zRowEdit.Cells["OverMoney"].ErrorText = "Sai định dạng!";
            //                }
            //                zIsChangeValued = true;
            //            }
            //        }
            //        break;
            //    case 15: // thong tin Different
            //        if (zRowEdit.Cells[e.ColumnIndex].Value == null)
            //        {
            //            zWorker.Date_Working = 0;
            //            zIsChangeValued = true;
            //        }
            //        else
            //        {
            //            if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() != zWorker.Date_Working.ToString())
            //            {

            //                float zDate_Working;
            //                if ((float.TryParse(zRowEdit.Cells["OverMoney"].Value.ToString(), out zDate_Working)))
            //                {
            //                    zWorker.Date_Working = float.Parse(zRowEdit.Cells[e.ColumnIndex].Value.ToString()); //;
            //                    zRowEdit.Cells["Date_Working"].ErrorText = "";
            //                }
            //                else
            //                {
            //                    zWorker.Date_Working = 0;
            //                    zRowEdit.Cells["Date_Working"].ErrorText = "Sai định dạng!";
            //                }
            //                zIsChangeValued = true;
            //            }
            //        }
            //        break;
            //    case 16: // thong tin Different
            //        if (zRowEdit.Cells[e.ColumnIndex].Value == null)
            //        {
            //            zWorker.Date_Off = 0;
            //            zIsChangeValued = true;
            //        }
            //        else
            //        {
            //            if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() != zWorker.Date_Off.ToString())
            //            {
            //                float zDate_Off;
            //                if ((float.TryParse(zRowEdit.Cells["Date_Off"].Value.ToString(), out zDate_Off)))
            //                {
            //                    zWorker.Date_Off = float.Parse(zRowEdit.Cells[e.ColumnIndex].Value.ToString()); //;  
            //                    zRowEdit.Cells["Date_Off"].ErrorText = "";
            //                }
            //                else
            //                {
            //                    zWorker.Date_Off = 0;
            //                    zRowEdit.Cells["Date_Off"].ErrorText = "Sai định dạng!";
            //                }
            //                zIsChangeValued = true;
            //            }
            //        }
            //        break;
            //}
            //if (zIsChangeValued)
            //{
            //    zWorker.RecordStatus = 1;

            //}
            //if (zWorker.Key != 0)
            //    zWorker.RecordStatus = 2;
            //else
            //    zWorker.RecordStatus = 1;
        }

        private void btn_Save_Tab2_Click(object sender, EventArgs e)
        {
            //DialogResult dlr = MessageBox.Show("Bạn có chắc thay đổi số liệu không ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            //if (dlr == DialogResult.Yes)
            //{
            //    _List_Worker = new List<Working_Month_Info>();
            //    Working_Month_Info zWorking;
            //    for (int i = 0; i < GV_Data.Rows.Count; i++)
            //    {
            //        if (GV_Data.Rows[i].Tag != null)
            //        {
            //            zWorking = (Working_Month_Info)GV_Data.Rows[i].Tag;
            //            _List_Worker.Add(zWorking);
            //        }
            //    }
            //    string zMessage = "";
            //    for (int i = 0; i < _List_Worker.Count; i++)
            //    {
            //        Working_Month_Info zProduct = (Working_Month_Info)_List_Worker[i];
            //        zProduct.ModifiedBy = SessionUser.UserLogin.EmployeeKey;
            //        zProduct.ModifiedName = SessionUser.UserLogin.EmployeeName;
            //        switch (zProduct.RecordStatus)
            //        {
            //            case 2:
            //                zProduct.Update();
            //                break;
            //        }
            //        TN_Message.Show(zProduct.Message);
            //    }
            //    if (zMessage.Length == 0)
            //    {
            //        GV_Data_LoadData();
            //        MessageBox.Show("Câp nhật thành công", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

            //    }
            //    else
            //    {
            //        MessageBox.Show("Vui lòng kiểm tra lại", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    }
            //}

        }
        private void btn_Done_Tab2_Click(object sender, EventArgs e)
        {
            string zMessage = "";
            for (int i = 0; i < In_Table_Tab2.Rows.Count; i++)
            {
                string zEmployeeKey = In_Table_Tab2.Rows[i]["EmployeeKey"].ToString();
                int zCount = Salary_Product_Worker_Data.Find_Employee(zEmployeeKey, _Date);
                Salary_Product_Worker_Info zSalary = new Salary_Product_Worker_Info();
                if (zCount == 0)
                {
                    zSalary.SalaryMonth = _Date;
                    zSalary.EmployeeKey = zEmployeeKey;
                    zSalary.EmployeeID = In_Table_Tab2.Rows[i]["EmployeeID"].ToString();
                    zSalary.EmployeeName = In_Table_Tab2.Rows[i]["EmployeeName"].ToString();

                    zSalary.SalaryOvertime = float.Parse(In_Table_Tab2.Rows[i]["OverMoney"].ToString());
                    zSalary.NumberRice = float.Parse(In_Table_Tab2.Rows[i]["Number_Rice"].ToString());
                    zSalary.SalaryRice = float.Parse(In_Table_Tab2.Rows[i]["Money_Rice"].ToString());
                    zSalary.TotalDate = int.Parse(In_Table_Tab2.Rows[i]["Date_K"].ToString());
                    float zDayoff = float.Parse(In_Table_Tab2.Rows[i]["Date_KP"].ToString()) + float.Parse(In_Table_Tab2.Rows[i]["Date_PT"].ToString()); ;
                    zSalary.DayOff = zDayoff;
                    zSalary.SalaryDayOff = float.Parse(In_Table_Tab2.Rows[i]["Money_Rice_PTKP"].ToString());
                    zSalary.Create_TimeWorking();
                    zMessage += TN_Message.Show(zSalary.Message);
                }
                if (zCount > 0)
                {
                    zSalary.SalaryMonth = _Date;
                    zSalary.EmployeeKey = zEmployeeKey;
                    zSalary.EmployeeID = In_Table_Tab2.Rows[i]["EmployeeID"].ToString();
                    zSalary.EmployeeName = In_Table_Tab2.Rows[i]["EmployeeName"].ToString();
                    zSalary.SalaryOvertime = float.Parse(In_Table_Tab2.Rows[i]["OverMoney"].ToString());
                    zSalary.NumberRice = float.Parse(In_Table_Tab2.Rows[i]["Number_Rice"].ToString());
                    zSalary.SalaryRice = float.Parse(In_Table_Tab2.Rows[i]["Money_Rice"].ToString());
                    zSalary.TotalDate = int.Parse(In_Table_Tab2.Rows[i]["Date_K"].ToString());
                    float zDayoff = float.Parse(In_Table_Tab2.Rows[i]["Date_KP"].ToString()) + float.Parse(In_Table_Tab2.Rows[i]["Date_PT"].ToString()); ;
                    zSalary.DayOff = zDayoff;
                    zSalary.SalaryDayOff = float.Parse(In_Table_Tab2.Rows[i]["Money_Rice_PTKP"].ToString());
                    zSalary.Update_TimeWorking();
                    zMessage += TN_Message.Show(zSalary.Message);
                }

            }
            if (zMessage.Length == 0)
            {
                MessageBox.Show("Câp nhật thành công", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Vui lòng kiểm tra lại", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        #endregion

        #region[Tab3]
        #region [Design ListView]
        private void LV_Office_Layout(ListView LV)
        {
            //ColumnHeader colHead;
            //colHead = new ColumnHeader();
            //colHead.Text = "No";
            //colHead.Width = 40;
            //colHead.TextAlign = HorizontalAlignment.Center;
            //LV.Columns.Add(colHead);

            //colHead = new ColumnHeader();
            //colHead.Text = "Tên Nhóm";
            //colHead.Width = 250;
            //colHead.TextAlign = HorizontalAlignment.Left;
            //LV.Columns.Add(colHead);
        }

        public void LV_Office_LoadData()
        {
            //this.Cursor = Cursors.WaitCursor;
            //ListView LV = LV_Office;
            //ListViewItem lvi;
            //ListViewItem.ListViewSubItem lvsi;

            //DataTable ztb_Team = new DataTable();

            //ztb_Team = Working_Month_Data.List_Office();

            //LV.Items.Clear();
            //int n = ztb_Team.Rows.Count;
            //for (int i = 0; i < n; i++)
            //{
            //    lvi = new ListViewItem();
            //    lvi.Text = (i + 1).ToString();
            //    lvi.ForeColor = Color.Navy;
            //    DataRow nRow = ztb_Team.Rows[i];
            //    lvi.Tag = nRow["BranchKey"].ToString();
            //    lvi.BackColor = Color.White;

            //    lvi.ImageIndex = 0;

            //    lvsi = new ListViewItem.ListViewSubItem();
            //    lvsi.Text = nRow["BranchName"].ToString().Trim();
            //    lvi.SubItems.Add(lvsi);

            //    LV.Items.Add(lvi);

            //}
            //this.Cursor = Cursors.Default;
            //if (n <= 0)
            //    txt_Branch.Text = "0";
            //else
            //    txt_Branch.Text = n.ToString();
        }
        #endregion
        private void LV_Office_ItemActivate(object sender, EventArgs e)
        {
            //for (int i = 0; i < LV_Office.Items.Count; i++)
            //{
            //    if (LV_Office.Items[i].Selected == true)
            //    {
            //        LV_Office.Items[i].BackColor = Color.LightBlue; // highlighted item
            //    }
            //    else
            //    {
            //        LV_Office.Items[i].BackColor = SystemColors.Window; // normal item
            //    }
            //}
            //_BranchKey = int.Parse(LV_Office.SelectedItems[0].Tag.ToString());
            //lbl_Office.Text = "TÍNH CÔNG KHỐI " + ": " + LV_Office.SelectedItems[0].SubItems[1].Text;
            //LV_Office.Enabled = false;
            //GV_Office_LoadData();
        }

        private void GV_Office_Layout()
        {
            // Setup Column 
            //GV_Office.Columns.Add("No", "STT");
            //GV_Office.Columns.Add("EmployeeID", "Mã NV");
            //GV_Office.Columns.Add("EmployeeName", "Họ và tên");
            //GV_Office.Columns.Add("Date_K", "Tính công");
            //GV_Office.Columns.Add("Date_D", "Không tính công");
            //GV_Office.Columns.Add("Date_P", "Nghỉ phép");
            //GV_Office.Columns.Add("Date_KP", "Ngỉ không phép");
            //GV_Office.Columns.Add("Date_LP", "Nghỉ luân phiên");
            //GV_Office.Columns.Add("Date_PT", "Nghỉ phép trừ");

            //GV_Office.Columns.Add("Date_1", "Tính 1 công");
            //GV_Office.Columns.Add("Date_05", "Tính 1/2 công");
            //GV_Office.Columns.Add("Date_0", "Nghỉ phép");

            //GV_Office.Columns.Add("Date_different", "Loại khác");
            //GV_Office.Columns.Add("Overtime", "Giờ dư");
            //GV_Office.Columns.Add("OverMoney", "Tiền ngoài giờ");
            //GV_Office.Columns.Add("Date_Working", "Tổng ngày đi làm ");
            //GV_Office.Columns.Add("Date_Off", "Tổng ngày nghỉ");

            //GV_Office.Columns["No"].Width = 40;
            //GV_Office.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //GV_Office.Columns["No"].ReadOnly = true;
            //GV_Office.Columns["No"].Frozen = true;

            //GV_Office.Columns["EmployeeID"].Width = 100;
            //GV_Office.Columns["EmployeeID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            //GV_Office.Columns["EmployeeID"].ReadOnly = true;
            //GV_Office.Columns["EmployeeID"].Frozen = true;

            //GV_Office.Columns["EmployeeName"].Width = 150;
            //GV_Office.Columns["EmployeeName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            //GV_Office.Columns["EmployeeName"].ReadOnly = true;
            //GV_Office.Columns["EmployeeName"].Frozen = true;

            //GV_Office.Columns["Date_1"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            //GV_Office.Columns["Date_05"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            //GV_Office.Columns["Date_0"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            //GV_Office.Columns["Date_different"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            //GV_Office.Columns["Overtime"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            //GV_Office.Columns["OverMoney"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            //GV_Office.Columns["Date_Working"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            //GV_Office.Columns["Date_Off"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            //GV_Office.Columns["Date_K"].Visible = false;
            //GV_Office.Columns["Date_D"].Visible = false;
            //GV_Office.Columns["Date_P"].Visible = false;
            //GV_Office.Columns["Date_KP"].Visible = false;
            //GV_Office.Columns["Date_LP"].Visible = false;
            //GV_Office.Columns["Date_PT"].Visible = false;

            //// setup style view

            //GV_Office.BackgroundColor = Color.White;
            //GV_Office.GridColor = Color.FromArgb(227, 239, 255);
            //GV_Office.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            //GV_Office.DefaultCellStyle.ForeColor = Color.FromArgb(43, 43, 43);
            //GV_Office.DefaultCellStyle.Font = new Font("Arial", 9, FontStyle.Bold);

            //GV_Office.AllowUserToResizeRows = false;
            //GV_Office.AllowUserToResizeColumns = true;

            //GV_Office.RowHeadersVisible = false;

            ////// setup Height Header
            ////GV_Employee.ColumnHeadersHeight = GV_Employee.ColumnHeadersHeight * 3 / 2;
            //GV_Office.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 9, FontStyle.Bold);
            //GV_Office.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            //GV_Office.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;

            //for (int i = 1; i <= 8; i++)
            //{
            //    GV_Office.Rows.Add();
            //}

        }
        private void GV_Office_LoadData()
        {
            //In_Table_Tab3 = Working_Month_Data.List_Data_Office_Team(_BranchKey, _FromDate, _ToDate);
            //_List_Office = new List<Working_Month_Info>();
            //foreach (DataRow nRow in In_Table_Tab3.Rows)
            //{
            //    Working_Month_Info Product = new Working_Month_Info(nRow);
            //    _List_Office.Add(Product);
            //}

            //GV_Office.Rows.Clear();
            //for (int i = 1; i <= 100; i++)
            //{
            //    GV_Office.Rows.Add();
            //}
            //for (int i = 0; i < _List_Office.Count; i++)
            //{
            //    Working_Month_Info zWorking = (Working_Month_Info)_List_Office[i];
            //    if (zWorking.RecordStatus == 1)
            //        GV_Office.Rows[i].DefaultCellStyle.BackColor = Color.Khaki;
            //    GV_Office.Rows[i].Tag = zWorking;
            //    GV_Office.Rows[i].Cells["No"].Value = (i + 1).ToString();
            //    GV_Office.Rows[i].Cells["EmployeeID"].Value = zWorking.EmployeeID;
            //    GV_Office.Rows[i].Cells["EmployeeName"].Value = zWorking.EmployeeName;
            //    GV_Office.Rows[i].Cells["Date_K"].Value = zWorking.Date_K;
            //    GV_Office.Rows[i].Cells["Date_D"].Value = zWorking.Date_D;
            //    GV_Office.Rows[i].Cells["Date_P"].Value = zWorking.Date_P;
            //    GV_Office.Rows[i].Cells["Date_KP"].Value = zWorking.Date_KP;
            //    GV_Office.Rows[i].Cells["Date_LP"].Value = zWorking.Date_LP;
            //    GV_Office.Rows[i].Cells["Date_PT"].Value = zWorking.Date_PT;
            //    GV_Office.Rows[i].Cells["Date_1"].Value = zWorking.Date_1;
            //    GV_Office.Rows[i].Cells["Date_05"].Value = zWorking.Date_05;
            //    GV_Office.Rows[i].Cells["Date_0"].Value = zWorking.Date_0;
            //    GV_Office.Rows[i].Cells["Date_different"].Value = zWorking.Date_different;
            //    GV_Office.Rows[i].Cells["Overtime"].Value = zWorking.Overtime;
            //    GV_Office.Rows[i].Cells["OverMoney"].Value = zWorking.OverMoney;
            //    GV_Office.Rows[i].Cells["Date_Working"].Value = zWorking.Date_Working;
            //    GV_Office.Rows[i].Cells["Date_Off"].Value = zWorking.Date_Off;

            //}
            //LV_Office.Enabled = true;
            //if (_List_Office.Count.ToInt() <= 0)
            //    txt_Cout_3.Text = "0";
            //else
            //    txt_Cout_3.Text = _List_Office.Count.ToString();
        }
        private void GV_Office_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            //DataGridViewRow zRowEdit = GV_Office.Rows[e.RowIndex];
            //Working_Month_Info zOffice = (Working_Month_Info)GV_Office.Rows[e.RowIndex].Tag;
            //if (GV_Office.Rows[e.RowIndex].Tag == null)
            //{
            //    zOffice = new Working_Month_Info();
            //    GV_Office.Rows[e.RowIndex].Tag = zOffice;
            //}
            //else
            //{
            //    zOffice = (Working_Month_Info)GV_Office.Rows[e.RowIndex].Tag;
            //}
            //bool zIsChangeValued = false;
            //switch (e.ColumnIndex)
            //{
            //    case 9: // thong tin ve 1
            //        if (zRowEdit.Cells[e.ColumnIndex].Value == null)
            //        {
            //            zOffice.Date_1 = 0;
            //            zIsChangeValued = true;
            //        }
            //        else
            //        {
            //            if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() != zOffice.Date_1.ToString())
            //            {
                           
            //                float zDate_1;
            //                if ((float.TryParse(zRowEdit.Cells["Date_1"].Value.ToString(), out zDate_1)))
            //                {
            //                    zOffice.Date_1 = float.Parse(zRowEdit.Cells[e.ColumnIndex].Value.ToString()); //;   
            //                    zRowEdit.Cells["Date_1"].ErrorText = "";
            //                }
            //                else
            //                {
            //                    zOffice.Date_1 = 0;
            //                    zRowEdit.Cells["Date_1"].ErrorText = "Sai định dạng!";
            //                }
            //                zIsChangeValued = true;
            //            }
            //        }
            //        break;
            //    case 10: // Thong tin ve 0.5
            //        if (zRowEdit.Cells[e.ColumnIndex].Value == null)
            //        {
            //            zOffice.Date_05 = 0;
            //            zIsChangeValued = true;
            //        }
            //        else
            //        {
            //            if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() != zOffice.Date_05.ToString())
            //            {
                           
            //                float zDate_05;
            //                if ((float.TryParse(zRowEdit.Cells["Date_05"].Value.ToString(), out zDate_05)))
            //                {
            //                    zOffice.Date_05 = float.Parse(zRowEdit.Cells[e.ColumnIndex].Value.ToString()); //;     
            //                    zRowEdit.Cells["Date_05"].ErrorText = "";
            //                }
            //                else
            //                {
            //                    zOffice.Date_05 = 0;
            //                    zRowEdit.Cells["Date_05"].ErrorText = "Sai định dạng!";
            //                }
            //                zIsChangeValued = true;
            //            }
            //        }
            //        break;
            //    case 11: // P
            //        if (zRowEdit.Cells[e.ColumnIndex].Value == null)
            //        {
            //            zOffice.Date_0 = 0;
            //            zIsChangeValued = true;
            //        }
            //        else
            //        {
            //            if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() != zOffice.Date_0.ToString())
            //            {

            //                float zDate_0;
            //                if ((float.TryParse(zRowEdit.Cells["Date_0"].Value.ToString(), out zDate_0)))
            //                {
            //                    zOffice.Date_0 = float.Parse(zRowEdit.Cells[e.ColumnIndex].Value.ToString()); //; 
            //                    zRowEdit.Cells["Date_0"].ErrorText = "";
            //                }
            //                else
            //                {
            //                    zOffice.Date_0 = 0;
            //                    zRowEdit.Cells["Date_0"].ErrorText = "Sai định dạng!";
            //                }
            //                zIsChangeValued = true;
            //            }
            //        }
            //        break;
            
            //    case 12: // thong tin Different
            //        if (zRowEdit.Cells[e.ColumnIndex].Value == null)
            //        {
            //            zOffice.Date_different = 0;
            //            zIsChangeValued = true;
            //        }
            //        else
            //        {
            //            if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() != zOffice.Date_different.ToString())
            //            {
            //                float zDate_different;
            //                if ((float.TryParse(zRowEdit.Cells["Date_different"].Value.ToString(), out zDate_different)))
            //                {
            //                    zOffice.Date_different = float.Parse(zRowEdit.Cells[e.ColumnIndex].Value.ToString()); //; 
            //                    zRowEdit.Cells["Date_different"].ErrorText = "";
            //                }
            //                else
            //                {
            //                    zOffice.Date_different = 0;
            //                    zRowEdit.Cells["Date_different"].ErrorText = "Sai định dạng!";
            //                }
            //                zIsChangeValued = true;
            //            }
            //        }
            //        break;
            //    case 13: // thong tin Different
            //        if (zRowEdit.Cells[e.ColumnIndex].Value == null)
            //        {
            //            zOffice.Overtime = 0;
            //            zIsChangeValued = true;
            //        }
            //        else
            //        {
            //            if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() != zOffice.Overtime.ToString())
            //            {
 
            //                float zOvertime;
            //                if ((float.TryParse(zRowEdit.Cells["Overtime"].Value.ToString(), out zOvertime)))
            //                {
            //                    zOffice.Overtime = float.Parse(zRowEdit.Cells[e.ColumnIndex].Value.ToString()); //;  
            //                    zRowEdit.Cells["Overtime"].ErrorText = "";
            //                }
            //                else
            //                {
            //                    zOffice.Overtime = 0;
            //                    zRowEdit.Cells["Overtime"].ErrorText = "Sai định dạng!";
            //                }
            //                zIsChangeValued = true;
            //            }
            //        }
            //        break;
            //    case 14: // thong tin Different
            //        if (zRowEdit.Cells[e.ColumnIndex].Value == null)
            //        {
            //            zOffice.OverMoney = 0;
            //            zIsChangeValued = true;
            //        }
            //        else
            //        {
            //            if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() != zOffice.OverMoney.ToString())
            //            {
                          
            //                float zOverMoney;
            //                if ((float.TryParse(zRowEdit.Cells["OverMoney"].Value.ToString(), out zOverMoney)))
            //                {
            //                    zOffice.OverMoney = float.Parse(zRowEdit.Cells[e.ColumnIndex].Value.ToString()); //;  
            //                    zRowEdit.Cells["OverMoney"].ErrorText = "";
            //                }
            //                else
            //                {
            //                    zOffice.OverMoney = 0;
            //                    zRowEdit.Cells["OverMoney"].ErrorText = "Sai định dạng!";
            //                }
            //                zIsChangeValued = true;
            //            }
            //        }
            //        break;
            //    case 15: // thong tin Different
            //        if (zRowEdit.Cells[e.ColumnIndex].Value == null)
            //        {
            //            zOffice.Date_Working = 0;
            //            zIsChangeValued = true;
            //        }
            //        else
            //        {
            //            if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() != zOffice.Date_Working.ToString())
            //            {
                          
            //                float zDate_Working;
            //                if ((float.TryParse(zRowEdit.Cells["Date_Working"].Value.ToString(), out zDate_Working)))
            //                {
            //                    zOffice.Date_Working = float.Parse(zRowEdit.Cells[e.ColumnIndex].Value.ToString()); //; 
            //                    zRowEdit.Cells["Date_Working"].ErrorText = "";
            //                }
            //                else
            //                {
            //                    zOffice.Date_Working = 0;
            //                    zRowEdit.Cells["Date_Working"].ErrorText = "Sai định dạng!";
            //                }
            //                zIsChangeValued = true;
            //            }
            //        }
            //        break;
            //    case 16: // thong tin Different
            //        if (zRowEdit.Cells[e.ColumnIndex].Value == null)
            //        {
            //            zOffice.Date_Off = 0;
            //            zIsChangeValued = true;
            //        }
            //        else
            //        {
            //            if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() != zOffice.Date_Off.ToString())
            //            {
                            
            //                float zDate_Off;
            //                if ((float.TryParse(zRowEdit.Cells["Date_Off"].Value.ToString(), out zDate_Off)))
            //                {
            //                    zOffice.Date_Off = float.Parse(zRowEdit.Cells[e.ColumnIndex].Value.ToString()); //;  
            //                    zRowEdit.Cells["Date_Off"].ErrorText = "";
            //                }
            //                else
            //                {
            //                    zOffice.Date_Off = 0;
            //                    zRowEdit.Cells["Date_Off"].ErrorText = "Sai định dạng!";
            //                }
            //                zIsChangeValued = true;
            //            }
            //        }
            //        break;
            //}
            //if (zIsChangeValued)
            //{
            //    zOffice.RecordStatus = 1;

            //}
            //if (zOffice.Key != 0)
            //    zOffice.RecordStatus = 2;
            //else
            //    zOffice.RecordStatus = 1;
        }
        private void btn_Save_Tab3_Click(object sender, EventArgs e)
        {
            //DialogResult dlr = MessageBox.Show("Bạn có chắc thay đổi số liệu không ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            //if (dlr == DialogResult.Yes)
            //{
            //    _List_Office = new List<Working_Month_Info>();
            //    Working_Month_Info zWorking;
            //    for (int i = 0; i < GV_Office.Rows.Count; i++)
            //    {
            //        if (GV_Office.Rows[i].Tag != null)
            //        {
            //            zWorking = (Working_Month_Info)GV_Office.Rows[i].Tag;
            //            _List_Office.Add(zWorking);
            //        }
            //    }
            //    string zMessage = "";
            //    for (int i = 0; i < _List_Office.Count; i++)
            //    {
            //        Working_Month_Info zProduct = (Working_Month_Info)_List_Office[i];
            //        zProduct.ModifiedBy = SessionUser.UserLogin.EmployeeKey;
            //        zProduct.ModifiedName = SessionUser.UserLogin.EmployeeName;
            //        switch (zProduct.RecordStatus)
            //        {
            //            case 2:
            //                zProduct.Update();
            //                break;
            //        }
            //        TN_Message.Show(zProduct.Message);
            //    }
            //    if (zMessage.Length == 0)
            //    {
            //        GV_Office_LoadData();
            //        MessageBox.Show("Câp nhật thành công", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

            //    }
            //    else
            //    {
            //        MessageBox.Show("Vui lòng kiểm tra lại", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    }
            //}

        }
        private void btn_Done_Tab3_Click(object sender, EventArgs e)
        {
            //string zMessage = "";
            //for (int i = 0; i < In_Table_Tab3.Rows.Count; i++)
            //{
            //    string zEmployeeKey = In_Table_Tab3.Rows[i]["EmployeeKey"].ToString();
            //    int zCount = Salary_Finish_Data.Find_Employee(zEmployeeKey, _Date);
            //    Salary_Product_Worker_Info zSalary = new Salary_Product_Worker_Info();
            //    if (zCount == 0)
            //    {
            //        zSalary.SalaryMonth = _Date;
            //        zSalary.EmployeeKey = zEmployeeKey;
            //        zSalary.EmployeeID = In_Table_Tab3.Rows[i]["EmployeeID"].ToString();
            //        zSalary.EmployeeName = In_Table_Tab3.Rows[i]["EmployeeName"].ToString();
            //        zSalary.WorkingDate = float.Parse(In_Table_Tab3.Rows[i]["Date_Working"].ToString());
            //        zSalary.DayOff = float.Parse(In_Table_Tab3.Rows[i]["Date_Off"].ToString());
            //        zSalary.OverTime = float.Parse(In_Table_Tab3.Rows[i]["Overtime"].ToString());
            //        zSalary.SalaryOverTime = float.Parse(In_Table_Tab3.Rows[i]["OverMoney"].ToString());
            //        zSalary.Create();
            //        zMessage += TN_Message.Show(zSalary.Message);
            //    }
            //    if (zCount > 0)
            //    {
            //        zSalary.SalaryMonth = _Date;
            //        zSalary.EmployeeKey = zEmployeeKey;
            //        zSalary.EmployeeID = In_Table_Tab3.Rows[i]["EmployeeID"].ToString();
            //        zSalary.EmployeeName = In_Table_Tab3.Rows[i]["EmployeeName"].ToString();
            //        zSalary.WorkingDate = float.Parse(In_Table_Tab3.Rows[i]["Date_Working"].ToString());
            //        zSalary.DayOff = float.Parse(In_Table_Tab3.Rows[i]["Date_Off"].ToString());
            //        zSalary.OverTime = float.Parse(In_Table_Tab3.Rows[i]["Overtime"].ToString());
            //        zSalary.SalaryOverTime = float.Parse(In_Table_Tab3.Rows[i]["OverMoney"].ToString());
            //        zSalary.Update();
            //        zMessage += TN_Message.Show(zSalary.Message);
            //    }

            //}
            //if (zMessage.Length == 0)
            //{
            //    MessageBox.Show("Câp nhật thành công", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //}
            //else
            //{
            //    MessageBox.Show("Vui lòng kiểm tra lại", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
        }
        #endregion


    }
}
