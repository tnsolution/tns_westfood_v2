﻿using C1.Win.C1FlexGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.HRM;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_EmployeeTeamOfficeView02 : Form
    {
        DataTable ztbType = new DataTable();
        private int _DepartmentKey = 0;
        private int _TeamKey = 0;
        private string _Search = "";
        private int _Type = 0;
        private int _Key = 0;
        public Frm_EmployeeTeamOfficeView02()
        {
            InitializeComponent();
            btnClose.Click += btnClose_Click;
            btnMax.Click += btnMax_Click;
            btnMini.Click += btnMini_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            btn_Search.Click += Btn_Search_Click;
            btn_Export.Click += Btn_Export_Click;
            cbo_Department.SelectedIndexChanged += Cbo_Department_SelectedIndexChanged;
            GVData.Click += GVData_Click;
            btn_Save.Click += Btn_Save_Click;
            btn_New.Click += Btn_New_Click;
            btn_Del.Click += Btn_Del_Click;
            txt_EmployeeID.Enter += Txt_EmployeeID_Enter;
            txt_EmployeeID.Leave += Txt_EmployeeID_Leave;
            btn_Copy.Click += Btn_Copy_Click;
        }

        

        private void Frm_EmployeeTeamOfficeView_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            Check_RoleForm();

            LoadTableType();
            LoadDataToToolbox.KryptonComboBox(cbo_Department, "SELECT DepartmentKey, DepartmentName FROM[dbo].[SYS_Department] WHERE DepartmentKey != 98  AND RecordStatus< 99", "---- Chọn tất cả----");
            LoadDataToToolbox.KryptonComboBox(cbo_Team, "SELECT TeamKey,TeamName FROM SYS_Team WHERE TeamKey != 98  AND RecordStatus < 99", "---- Chọn tất cả ----");
            LoadDataToToolbox.ComboBoxData(cbo_Type.ComboBox, ztbType, false, 0, 1);

            LoadDataToToolbox.KryptonComboBox(cbo_TeamPB, "SELECT TeamKey,TeamName FROM SYS_Team WHERE TeamKey != 98 AND BranchKey = 2  AND RecordStatus < 99 ORDER BY Rank", "---- Chọn ----");
            SetDefault();
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
        }
        private void Cbo_Department_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDataToToolbox.KryptonComboBox(cbo_Team, "SELECT TeamKey,TeamName FROM SYS_Team WHERE TeamKey != 98 AND DepartmentKey = " + cbo_Department.SelectedValue.ToInt() + "  AND RecordStatus < 99", "---- Chọn tất cả ----");
        }
        void LoadTableType()
        {
            ztbType = new DataTable();
            ztbType.Columns.Add("Value");
            ztbType.Columns.Add("Name");
            ztbType.Rows.Add("0", "--Tất cả--");
            ztbType.Rows.Add("1", "Đang áp dụng");
            ztbType.Rows.Add("2", "Đã hết thời hạn");
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            _DepartmentKey = cbo_Department.SelectedValue.ToInt();
            _TeamKey = cbo_Team.SelectedValue.ToInt();
            _Search = txt_Search.Text.Trim();
            _Type = cbo_Type.SelectedValue.ToInt();
            try
            {
                using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }

            }
            catch (Exception ex)
            {
                Utils.TNMessageBoxOK( ex.ToString(), 4);
            }
        }
        private void GVData_Click(object sender, EventArgs e)
        {
            if (GVData.Rows.Count >0 &&GVData.Rows[GVData.RowSel][9].ToString() != "")
            {
                _Key = GVData.Rows[GVData.RowSel][9].ToString().ToInt();
                LoadData();
            }
        }
       
        private void Btn_Export_Click(object sender, EventArgs e)
        {
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Hiển_Thị_Tổ_Nhóm_Chi_Phí.xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }

                    string Status = "Xuất Excel form " + HeaderControl.Text + " > đường dẫn và tên file xuất:" + FDialog.FileName;
                     Application_Data.Save_LogApplication(SessionUser.UserLogin.Key,SessionUser.UserLogin.EmployeeKey, Status, 3);

                    GVData.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }

        }
        private void Btn_New_Click(object sender, EventArgs e)
        {
            SetDefault();
        }

        private int _STT = 1;
        private void DisplayData()
        {
            DataTable _Intable = Employee_TeamOfficeView_Data.List02(_DepartmentKey, _TeamKey, _Search, _Type);
            if (_Intable.Rows.Count > 0)
            {
                this.Invoke(new MethodInvoker(delegate ()
                {
                    InitGV_Layout(_Intable);
                }));
            }
        }

        void InitGV_Layout(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            int TotalRow = TableView.Rows.Count + 1;
            int ToTalCol = 10;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(1);

            //Row Header
            GVData.Rows[0][0] = "Stt";
            GVData.Rows[0][1] = "Số thẻ";
            GVData.Rows[0][2] = "Họ và tên";
            GVData.Rows[0][3] = "Mã phòng";
            GVData.Rows[0][4] = "Mã Nhóm";
            GVData.Rows[0][5] = "Tên nhóm";
            GVData.Rows[0][6] = "Từ ngày";
            GVData.Rows[0][7] = "Đến ngày";
            GVData.Rows[0][8] = "Ghi chú";

            GVData.Rows[0][9] = ""; // ẩn cột này

            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                DataRow rData = TableView.Rows[rIndex];
                GVData.Rows.Add();
                GVData.Rows[rIndex + 1][0] = (rIndex + 1);
                GVData.Rows[rIndex + 1][1] = rData[1];
                GVData.Rows[rIndex + 1][2] = rData[2];
                GVData.Rows[rIndex + 1][3] = rData[3];
                GVData.Rows[rIndex + 1][4] = rData[4];
                GVData.Rows[rIndex + 1][5] = rData[5];
                GVData.Rows[rIndex + 1][6] = rData[6];
                GVData.Rows[rIndex + 1][7] = rData[7];
                GVData.Rows[rIndex + 1][8] = rData[8];
                GVData.Rows[rIndex + 1][9] = rData[0];
            }

            //Style         
            GVData.AllowFreezing = AllowFreezingEnum.Both;
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.FixedOnly;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.LeftCenter;
            GVData.Styles.Normal.WordWrap = true;

            //Freeze Row and Column                              
            GVData.Rows.Fixed = 1;
            GVData.Cols.Frozen = 3;

            GVData.Cols[0].StyleNew.BackColor = Color.Empty;
            GVData.Cols[1].StyleNew.BackColor = Color.Empty;
            GVData.Cols[2].StyleNew.BackColor = Color.Empty;

            GVData.Rows[0].TextAlign = TextAlignEnum.CenterCenter;

            GVData.Rows[0].Height = 40;

            GVData.Cols[0].Width = 40;
            GVData.Cols[1].Width = 120;
            GVData.Cols[2].Width = 200;
            GVData.Cols[3].Width = 120;
            GVData.Cols[4].Width = 120;
            GVData.Cols[5].Width = 300;
            GVData.Cols[6].Width = 120;
            GVData.Cols[7].Width = 120;
            GVData.Cols[8].Width = 120;
            GVData.Cols[9].Visible = false;
        }

        #region[Create Info]
        private void SetDefault()
        {
            _Key = 0;
            txt_EmployeeID.Text = "";
            txt_EmployeeName.Text = "";
            txt_EmployeeID.Tag = null;
            txt_Description.Text = "";
            cbo_Team.SelectedValue = 0;
            dte_FromDate.Value = SessionUser.Date_Work;
            dte_ToDate.Value = DateTime.MinValue;
            btn_Del.Enabled = false;
            lbl_Created.Text = "Tạo bởi:";
            lbl_Modified.Text = "Chỉnh sửa:";
            btn_Copy.Enabled = false;
            btn_Del.Enabled = false;
            Txt_EmployeeID_Leave(null, null);
        }
        private void LoadData()
        {
            Employee_TeamOfficeView_Info zinfo = new Employee_TeamOfficeView_Info(_Key);
            txt_EmployeeID.Tag = zinfo.EmployeeKey;
            txt_EmployeeID.Text = zinfo.EmployeeID;
            txt_EmployeeName.Text = zinfo.EmployeeName;
            cbo_Team.SelectedValue = zinfo.TeamKeyView;
            txt_Description.Text = zinfo.Description;
            dte_FromDate.Value = zinfo.FromDate;
            if (zinfo.ToDate == DateTime.MinValue)
            {
                dte_ToDate.Value = DateTime.MinValue;
                btn_Copy.Enabled = false;
            }
            else
            {
                dte_ToDate.Value = zinfo.ToDate;
                btn_Copy.Enabled = true;
            }
            btn_Del.Enabled = true;
            lbl_Created.Text = "Tạo bởi:[" + zinfo.CreatedName + "][" + zinfo.CreatedOn + "]";
            lbl_Modified.Text = "Chỉnh sửa:[" + zinfo.ModifiedName + "][" + zinfo.ModifiedOn + "]";
        }

        private void Btn_Save_Click(object sender, EventArgs e)
        {
            if (txt_EmployeeID.Text.Trim().Length == 0)
            {
                Utils.TNMessageBoxOK("Chưa nhập mã nhân viên!",1);
                return;
            }
            else
            {
                Employee_TeamOfficeView_Info zinfo = new Employee_TeamOfficeView_Info(_Key);
                zinfo.EmployeeKey = txt_EmployeeID.Tag.ToString();
                zinfo.EmployeeID = txt_EmployeeID.Text.Trim().ToUpper();
                zinfo.EmployeeName = txt_EmployeeName.Text.Trim().ToString();
                zinfo.TeamKeyView = int.Parse(cbo_Team.SelectedValue.ToString());
                zinfo.Description = txt_Description.Text.Trim();
                DateTime zFromDate = new DateTime(dte_FromDate.Value.Year, dte_FromDate.Value.Month, dte_FromDate.Value.Day, 0, 0, 0);
                DateTime zToDate = DateTime.MinValue;
                if (dte_ToDate.Value != DateTime.MinValue)
                {
                    zToDate = new DateTime(dte_ToDate.Value.Year, dte_ToDate.Value.Month, dte_ToDate.Value.Day, 23, 59, 59);
                }
                zinfo.FromDate = zFromDate;
                zinfo.ToDate = zToDate;
                zinfo.CreatedBy = SessionUser.UserLogin.Key;
                zinfo.CreatedName = SessionUser.UserLogin.EmployeeName;
                zinfo.ModifiedBy = SessionUser.UserLogin.Key;
                zinfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
                zinfo.Save();
                if (zinfo.Message == "11" || zinfo.Message == "20")
                {
                    Utils.TNMessageBoxOK("Cập nhật thành công!",3);
                    _Key = zinfo.Key;
                    LoadData();
                }
                else
                {
                    Utils.TNMessageBoxOK(zinfo.Message,4);
                }
            }


        }
        private void Btn_Del_Click(object sender, EventArgs e)
        {
            if (_Key == 0)
            {
                Utils.TNMessageBoxOK("Chưa chọn thông tin!",1);
            }
            else
            {
                if (Utils.TNMessageBox("Bạn có chắc xóa thông tin này ?.", 2) == "Y")
                {
                    Employee_TeamOfficeView_Info zinfo = new Employee_TeamOfficeView_Info(_Key);
                    zinfo.ModifiedBy = SessionUser.UserLogin.Key;
                    zinfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
                    zinfo.Delete();
                    if (zinfo.Message == "30")
                    {
                        Utils.TNMessageBoxOK("Đã xóa thành công !",3);
                        SetDefault();
                    }
                    else
                    {
                        Utils.TNMessageBoxOK(zinfo.Message,4);
                    }

                }
            }

        }

        private void Btn_Copy_Click(object sender, EventArgs e)
        {
            Employee_TeamOfficeView_Info zinfo = new Employee_TeamOfficeView_Info(_Key);
            txt_EmployeeID.Tag = zinfo.EmployeeKey;
            txt_EmployeeID.Text = zinfo.EmployeeID;
            txt_EmployeeName.Text = zinfo.EmployeeName;
            cbo_Team.SelectedValue = zinfo.TeamKeyView;
            txt_Description.Text = zinfo.Description;
            dte_FromDate.Value = zinfo.FromDate;
            if (zinfo.ToDate == null)
            {
                dte_ToDate.Value = DateTime.MinValue;
            }
            else
            {
                dte_ToDate.Value = zinfo.ToDate;
            }
            btn_Copy.Enabled = false;
            btn_Del.Enabled = false;
            _Key = 0;

            lbl_Created.Text = "Tạo bởi:";
            lbl_Modified.Text = "Chỉnh sửa:";
            MessageBox.Show("Sao chép thành công!Vui lòng chỉnh sửa thông tin.");
        }

        private void Txt_EmployeeID_Leave(object sender, EventArgs e)
        {
            if (txt_EmployeeID.Text.Trim().Length < 1)
            {
                txt_EmployeeID.Text = "Nhập mã thẻ";
                txt_EmployeeID.ForeColor = Color.White;
            }
            else
            {
                Employee_Info zinfo = new Employee_Info();
                zinfo.GetEmployee(txt_EmployeeID.Text.Trim());
                if (zinfo.Key == "")
                {
                    txt_EmployeeID.Text = "Nhập mã thẻ";
                    txt_EmployeeID.ForeColor = Color.White;
                    txt_EmployeeID.Tag = null;
                }
                else
                {
                    txt_EmployeeID.Text = zinfo.EmployeeID;
                    txt_EmployeeName.Text = zinfo.FullName;
                    txt_EmployeeID.Tag = zinfo.Key;
                    txt_EmployeeID.BackColor = Color.Black;
                }
            }
        }
        private void Txt_EmployeeID_Enter(object sender, EventArgs e)
        {
            if (txt_EmployeeID.Text.Trim() == "Nhập mã thẻ")
            {
                txt_EmployeeID.Text = "";
                txt_EmployeeID.ForeColor = Color.Brown;
            }
        }

        #endregion

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;

                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                //this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion
        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
                btn_Export.Enabled = false;
            }
            if (Role.RoleAdd == 0 && Role.RoleEdit == 0)
            {
                btn_New.Enabled = false;
                btn_Save.Enabled = false;
                btn_Copy.Enabled = false;

            }
            if (Role.RoleAdd == 0 && Role.RoleEdit == 1)
            {
                btn_New.Enabled = false;
                btn_Save.Enabled = true;
            }
            if (Role.RoleDel == 0)
            {
                btn_Del.Enabled = false;
            }
        }
        #endregion
    }
}
