﻿namespace TNS.WinApp
{
    partial class Frm_Employee_Contract
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Employee_Contract));
            this.cbo_Shearch_Branch = new System.Windows.Forms.ComboBox();
            this.cbo_Teams = new System.Windows.Forms.ComboBox();
            this.label42 = new System.Windows.Forms.Label();
            this.lbl_team = new System.Windows.Forms.Label();
            this.txt_Search = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.Panel_Right = new System.Windows.Forms.Panel();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.GV_Chil = new System.Windows.Forms.DataGridView();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.GV_Fee_Minus = new System.Windows.Forms.DataGridView();
            this.kryptonHeader4 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.GV_Fee_Plus = new System.Windows.Forms.DataGridView();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.cbo_CategoryKey = new System.Windows.Forms.ComboBox();
            this.grp_Probation = new System.Windows.Forms.GroupBox();
            this.rdo_First = new System.Windows.Forms.RadioButton();
            this.rdo_Late = new System.Windows.Forms.RadioButton();
            this.lbl_Probation = new System.Windows.Forms.Label();
            this.dte_Todate = new System.Windows.Forms.DateTimePicker();
            this.lbl_Fromday = new System.Windows.Forms.Label();
            this.dte_Fromdate = new System.Windows.Forms.DateTimePicker();
            this.dte_FinishDay = new System.Windows.Forms.DateTimePicker();
            this.lbl_Today = new System.Windows.Forms.Label();
            this.dte_Signday = new System.Windows.Forms.DateTimePicker();
            this.txt_ContractID = new System.Windows.Forms.TextBox();
            this.lbl_FinishDay = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.lbl_signday = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.GV_BasicSalary = new System.Windows.Forms.DataGridView();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.txt_EmployeeNameB = new System.Windows.Forms.TextBox();
            this.txt_HomeAddressB = new System.Windows.Forms.TextBox();
            this.dte_IssueDateB = new System.Windows.Forms.DateTimePicker();
            this.dte_BirthdayB = new System.Windows.Forms.DateTimePicker();
            this.txt_IssuedPlaceB = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.grp_Gender = new System.Windows.Forms.GroupBox();
            this.rdo_MaleB = new System.Windows.Forms.RadioButton();
            this.rdo_FemaleB = new System.Windows.Forms.RadioButton();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.txt_IssueIDB = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.txt_WorkingDescription = new System.Windows.Forms.TextBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.txt_EmployeeNameA = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.txt_PositionA = new System.Windows.Forms.TextBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.cbo_DepartmentContract = new System.Windows.Forms.ComboBox();
            this.cbo_PositionContract = new System.Windows.Forms.ComboBox();
            this.cbo_BranchContract = new System.Windows.Forms.ComboBox();
            this.cbo_TeamContract = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btn_Finish = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Save_New = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Add = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_New = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.Header = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btnMini = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnMax = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnClose = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.Panel_Search = new System.Windows.Forms.Panel();
            this.btn_Search = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.Panel_Left = new System.Windows.Forms.Panel();
            this.Panel_Employee = new System.Windows.Forms.Panel();
            this.LV_Employee = new System.Windows.Forms.ListView();
            this.kryptonHeader5 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btnHide = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnShow = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Panel_Contract = new System.Windows.Forms.Panel();
            this.LV_List_Contract = new System.Windows.Forms.ListView();
            this.kryptonHeader2 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.Panel_Right.SuspendLayout();
            this.groupBox15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GV_Chil)).BeginInit();
            this.groupBox12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GV_Fee_Minus)).BeginInit();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GV_Fee_Plus)).BeginInit();
            this.groupBox6.SuspendLayout();
            this.grp_Probation.SuspendLayout();
            this.groupBox16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GV_BasicSalary)).BeginInit();
            this.groupBox8.SuspendLayout();
            this.grp_Gender.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.panel3.SuspendLayout();
            this.Panel_Search.SuspendLayout();
            this.Panel_Left.SuspendLayout();
            this.Panel_Employee.SuspendLayout();
            this.Panel_Contract.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbo_Shearch_Branch
            // 
            this.cbo_Shearch_Branch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.cbo_Shearch_Branch.Font = new System.Drawing.Font("Arial", 9F);
            this.cbo_Shearch_Branch.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbo_Shearch_Branch.FormattingEnabled = true;
            this.cbo_Shearch_Branch.Location = new System.Drawing.Point(68, 35);
            this.cbo_Shearch_Branch.Name = "cbo_Shearch_Branch";
            this.cbo_Shearch_Branch.Size = new System.Drawing.Size(181, 23);
            this.cbo_Shearch_Branch.TabIndex = 1;
            // 
            // cbo_Teams
            // 
            this.cbo_Teams.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.cbo_Teams.Font = new System.Drawing.Font("Arial", 9F);
            this.cbo_Teams.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbo_Teams.FormattingEnabled = true;
            this.cbo_Teams.Location = new System.Drawing.Point(68, 64);
            this.cbo_Teams.Name = "cbo_Teams";
            this.cbo_Teams.Size = new System.Drawing.Size(181, 23);
            this.cbo_Teams.TabIndex = 2;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label42.Location = new System.Drawing.Point(34, 40);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(32, 15);
            this.label42.TabIndex = 154;
            this.label42.Text = "Khối";
            // 
            // lbl_team
            // 
            this.lbl_team.AutoSize = true;
            this.lbl_team.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_team.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbl_team.Location = new System.Drawing.Point(8, 69);
            this.lbl_team.Name = "lbl_team";
            this.lbl_team.Size = new System.Drawing.Size(58, 15);
            this.lbl_team.TabIndex = 155;
            this.lbl_team.Text = "Tổ Nhóm";
            // 
            // txt_Search
            // 
            this.txt_Search.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_Search.Font = new System.Drawing.Font("Arial", 9F);
            this.txt_Search.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Search.Location = new System.Drawing.Point(68, 8);
            this.txt_Search.Name = "txt_Search";
            this.txt_Search.Size = new System.Drawing.Size(181, 21);
            this.txt_Search.TabIndex = 0;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label20.Location = new System.Drawing.Point(2, 11);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(64, 15);
            this.label20.TabIndex = 152;
            this.label20.Text = "Nhân Viên";
            // 
            // Panel_Right
            // 
            this.Panel_Right.AutoScroll = true;
            this.Panel_Right.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Right.Controls.Add(this.groupBox15);
            this.Panel_Right.Controls.Add(this.groupBox12);
            this.Panel_Right.Controls.Add(this.kryptonHeader4);
            this.Panel_Right.Controls.Add(this.groupBox9);
            this.Panel_Right.Controls.Add(this.groupBox6);
            this.Panel_Right.Controls.Add(this.groupBox16);
            this.Panel_Right.Controls.Add(this.groupBox8);
            this.Panel_Right.Controls.Add(this.groupBox11);
            this.Panel_Right.Controls.Add(this.groupBox7);
            this.Panel_Right.Controls.Add(this.groupBox10);
            this.Panel_Right.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel_Right.Location = new System.Drawing.Point(304, 42);
            this.Panel_Right.Name = "Panel_Right";
            this.Panel_Right.Size = new System.Drawing.Size(746, 667);
            this.Panel_Right.TabIndex = 3;
            // 
            // groupBox15
            // 
            this.groupBox15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.groupBox15.Controls.Add(this.GV_Chil);
            this.groupBox15.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.groupBox15.ForeColor = System.Drawing.Color.Black;
            this.groupBox15.Location = new System.Drawing.Point(6, 992);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(720, 139);
            this.groupBox15.TabIndex = 5;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "Hổ trợ con nhỏ";
            // 
            // GV_Chil
            // 
            this.GV_Chil.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.GV_Chil.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GV_Chil.Location = new System.Drawing.Point(6, 20);
            this.GV_Chil.Name = "GV_Chil";
            this.GV_Chil.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GV_Chil.Size = new System.Drawing.Size(708, 113);
            this.GV_Chil.TabIndex = 2;
            // 
            // groupBox12
            // 
            this.groupBox12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.groupBox12.Controls.Add(this.GV_Fee_Minus);
            this.groupBox12.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.groupBox12.ForeColor = System.Drawing.Color.Black;
            this.groupBox12.Location = new System.Drawing.Point(6, 861);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(720, 125);
            this.groupBox12.TabIndex = 238;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Khoản trừ người lao động";
            // 
            // GV_Fee_Minus
            // 
            this.GV_Fee_Minus.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.GV_Fee_Minus.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GV_Fee_Minus.Location = new System.Drawing.Point(6, 20);
            this.GV_Fee_Minus.Name = "GV_Fee_Minus";
            this.GV_Fee_Minus.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GV_Fee_Minus.Size = new System.Drawing.Size(708, 96);
            this.GV_Fee_Minus.TabIndex = 2;
            // 
            // kryptonHeader4
            // 
            this.kryptonHeader4.AutoSize = false;
            this.kryptonHeader4.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader4.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader4.Name = "kryptonHeader4";
            this.kryptonHeader4.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader4.Size = new System.Drawing.Size(729, 30);
            this.kryptonHeader4.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader4.TabIndex = 203;
            this.kryptonHeader4.Values.Description = "";
            this.kryptonHeader4.Values.Heading = "Chi tiết";
            // 
            // groupBox9
            // 
            this.groupBox9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.groupBox9.Controls.Add(this.GV_Fee_Plus);
            this.groupBox9.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.groupBox9.ForeColor = System.Drawing.Color.Black;
            this.groupBox9.Location = new System.Drawing.Point(6, 728);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(720, 127);
            this.groupBox9.TabIndex = 3;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Khoản phụ cấp hợp đồng";
            // 
            // GV_Fee_Plus
            // 
            this.GV_Fee_Plus.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.GV_Fee_Plus.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GV_Fee_Plus.Location = new System.Drawing.Point(6, 20);
            this.GV_Fee_Plus.Name = "GV_Fee_Plus";
            this.GV_Fee_Plus.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GV_Fee_Plus.Size = new System.Drawing.Size(708, 96);
            this.GV_Fee_Plus.TabIndex = 2;
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.groupBox6.Controls.Add(this.cbo_CategoryKey);
            this.groupBox6.Controls.Add(this.grp_Probation);
            this.groupBox6.Controls.Add(this.lbl_Probation);
            this.groupBox6.Controls.Add(this.dte_Todate);
            this.groupBox6.Controls.Add(this.lbl_Fromday);
            this.groupBox6.Controls.Add(this.dte_Fromdate);
            this.groupBox6.Controls.Add(this.dte_FinishDay);
            this.groupBox6.Controls.Add(this.lbl_Today);
            this.groupBox6.Controls.Add(this.dte_Signday);
            this.groupBox6.Controls.Add(this.txt_ContractID);
            this.groupBox6.Controls.Add(this.lbl_FinishDay);
            this.groupBox6.Controls.Add(this.label18);
            this.groupBox6.Controls.Add(this.lbl_signday);
            this.groupBox6.Controls.Add(this.label28);
            this.groupBox6.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.groupBox6.ForeColor = System.Drawing.Color.Black;
            this.groupBox6.Location = new System.Drawing.Point(6, 36);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(720, 132);
            this.groupBox6.TabIndex = 0;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Thông tin hợp đồng";
            // 
            // cbo_CategoryKey
            // 
            this.cbo_CategoryKey.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.cbo_CategoryKey.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbo_CategoryKey.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbo_CategoryKey.FormattingEnabled = true;
            this.cbo_CategoryKey.Location = new System.Drawing.Point(121, 50);
            this.cbo_CategoryKey.Name = "cbo_CategoryKey";
            this.cbo_CategoryKey.Size = new System.Drawing.Size(212, 23);
            this.cbo_CategoryKey.TabIndex = 1;
            // 
            // grp_Probation
            // 
            this.grp_Probation.Controls.Add(this.rdo_First);
            this.grp_Probation.Controls.Add(this.rdo_Late);
            this.grp_Probation.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Italic);
            this.grp_Probation.Location = new System.Drawing.Point(121, 68);
            this.grp_Probation.Name = "grp_Probation";
            this.grp_Probation.Size = new System.Drawing.Size(212, 30);
            this.grp_Probation.TabIndex = 2;
            this.grp_Probation.TabStop = false;
            // 
            // rdo_First
            // 
            this.rdo_First.AutoSize = true;
            this.rdo_First.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Italic);
            this.rdo_First.Location = new System.Drawing.Point(6, 9);
            this.rdo_First.Name = "rdo_First";
            this.rdo_First.Size = new System.Drawing.Size(93, 19);
            this.rdo_First.TabIndex = 0;
            this.rdo_First.TabStop = true;
            this.rdo_First.Text = "15 ngày đầu";
            this.rdo_First.UseVisualStyleBackColor = true;
            // 
            // rdo_Late
            // 
            this.rdo_Late.AutoSize = true;
            this.rdo_Late.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Italic);
            this.rdo_Late.Location = new System.Drawing.Point(118, 9);
            this.rdo_Late.Name = "rdo_Late";
            this.rdo_Late.Size = new System.Drawing.Size(92, 19);
            this.rdo_Late.TabIndex = 1;
            this.rdo_Late.TabStop = true;
            this.rdo_Late.Text = "15 ngày sau";
            this.rdo_Late.UseVisualStyleBackColor = true;
            // 
            // lbl_Probation
            // 
            this.lbl_Probation.AutoSize = true;
            this.lbl_Probation.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Probation.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Probation.ForeColor = System.Drawing.Color.Black;
            this.lbl_Probation.Location = new System.Drawing.Point(34, 79);
            this.lbl_Probation.Name = "lbl_Probation";
            this.lbl_Probation.Size = new System.Drawing.Size(83, 15);
            this.lbl_Probation.TabIndex = 143;
            this.lbl_Probation.Text = "Ngày thử việc ";
            // 
            // dte_Todate
            // 
            this.dte_Todate.CustomFormat = "dd/MM/yyyy";
            this.dte_Todate.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dte_Todate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dte_Todate.Location = new System.Drawing.Point(484, 99);
            this.dte_Todate.Name = "dte_Todate";
            this.dte_Todate.Size = new System.Drawing.Size(193, 21);
            this.dte_Todate.TabIndex = 5;
            // 
            // lbl_Fromday
            // 
            this.lbl_Fromday.AutoSize = true;
            this.lbl_Fromday.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Fromday.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Fromday.ForeColor = System.Drawing.Color.Black;
            this.lbl_Fromday.Location = new System.Drawing.Point(376, 102);
            this.lbl_Fromday.Name = "lbl_Fromday";
            this.lbl_Fromday.Size = new System.Drawing.Size(106, 15);
            this.lbl_Fromday.TabIndex = 133;
            this.lbl_Fromday.Text = "Ngày cuối thử việc";
            // 
            // dte_Fromdate
            // 
            this.dte_Fromdate.CustomFormat = "dd/MM/yyyy";
            this.dte_Fromdate.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dte_Fromdate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dte_Fromdate.Location = new System.Drawing.Point(484, 72);
            this.dte_Fromdate.Name = "dte_Fromdate";
            this.dte_Fromdate.Size = new System.Drawing.Size(193, 21);
            this.dte_Fromdate.TabIndex = 6;
            // 
            // dte_FinishDay
            // 
            this.dte_FinishDay.CustomFormat = "dd/MM/yyyy";
            this.dte_FinishDay.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dte_FinishDay.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dte_FinishDay.Location = new System.Drawing.Point(484, 48);
            this.dte_FinishDay.Name = "dte_FinishDay";
            this.dte_FinishDay.Size = new System.Drawing.Size(193, 21);
            this.dte_FinishDay.TabIndex = 4;
            // 
            // lbl_Today
            // 
            this.lbl_Today.AutoSize = true;
            this.lbl_Today.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Today.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Today.ForeColor = System.Drawing.Color.Black;
            this.lbl_Today.Location = new System.Drawing.Point(378, 75);
            this.lbl_Today.Name = "lbl_Today";
            this.lbl_Today.Size = new System.Drawing.Size(104, 15);
            this.lbl_Today.TabIndex = 133;
            this.lbl_Today.Text = "Ngày đầu thử việc";
            // 
            // dte_Signday
            // 
            this.dte_Signday.CustomFormat = "dd/MM/yyyy";
            this.dte_Signday.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dte_Signday.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dte_Signday.Location = new System.Drawing.Point(484, 23);
            this.dte_Signday.Name = "dte_Signday";
            this.dte_Signday.Size = new System.Drawing.Size(193, 21);
            this.dte_Signday.TabIndex = 3;
            // 
            // txt_ContractID
            // 
            this.txt_ContractID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_ContractID.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_ContractID.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_ContractID.Location = new System.Drawing.Point(121, 25);
            this.txt_ContractID.Name = "txt_ContractID";
            this.txt_ContractID.Size = new System.Drawing.Size(212, 21);
            this.txt_ContractID.TabIndex = 0;
            // 
            // lbl_FinishDay
            // 
            this.lbl_FinishDay.AutoSize = true;
            this.lbl_FinishDay.BackColor = System.Drawing.Color.Transparent;
            this.lbl_FinishDay.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_FinishDay.ForeColor = System.Drawing.Color.Black;
            this.lbl_FinishDay.Location = new System.Drawing.Point(382, 51);
            this.lbl_FinishDay.Name = "lbl_FinishDay";
            this.lbl_FinishDay.Size = new System.Drawing.Size(100, 15);
            this.lbl_FinishDay.TabIndex = 133;
            this.lbl_FinishDay.Text = "Ngày hết hạn HĐ";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(36, 28);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(83, 15);
            this.label18.TabIndex = 127;
            this.label18.Text = "Mã Hợp Đồng";
            // 
            // lbl_signday
            // 
            this.lbl_signday.AutoSize = true;
            this.lbl_signday.BackColor = System.Drawing.Color.Transparent;
            this.lbl_signday.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_signday.ForeColor = System.Drawing.Color.Black;
            this.lbl_signday.Location = new System.Drawing.Point(377, 26);
            this.lbl_signday.Name = "lbl_signday";
            this.lbl_signday.Size = new System.Drawing.Size(105, 15);
            this.lbl_signday.TabIndex = 133;
            this.lbl_signday.Text = "Ngày ký hợp đồng";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(28, 55);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(91, 15);
            this.label28.TabIndex = 131;
            this.label28.Text = "Loại Hợp Đồng";
            // 
            // groupBox16
            // 
            this.groupBox16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.groupBox16.Controls.Add(this.GV_BasicSalary);
            this.groupBox16.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.groupBox16.ForeColor = System.Drawing.Color.Black;
            this.groupBox16.Location = new System.Drawing.Point(6, 585);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(720, 137);
            this.groupBox16.TabIndex = 2;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "Mức Lương Căn Bản";
            // 
            // GV_BasicSalary
            // 
            this.GV_BasicSalary.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.GV_BasicSalary.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GV_BasicSalary.Location = new System.Drawing.Point(6, 20);
            this.GV_BasicSalary.Name = "GV_BasicSalary";
            this.GV_BasicSalary.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GV_BasicSalary.Size = new System.Drawing.Size(708, 106);
            this.GV_BasicSalary.TabIndex = 0;
            // 
            // groupBox8
            // 
            this.groupBox8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.groupBox8.Controls.Add(this.txt_EmployeeNameB);
            this.groupBox8.Controls.Add(this.txt_HomeAddressB);
            this.groupBox8.Controls.Add(this.dte_IssueDateB);
            this.groupBox8.Controls.Add(this.dte_BirthdayB);
            this.groupBox8.Controls.Add(this.txt_IssuedPlaceB);
            this.groupBox8.Controls.Add(this.label32);
            this.groupBox8.Controls.Add(this.grp_Gender);
            this.groupBox8.Controls.Add(this.label33);
            this.groupBox8.Controls.Add(this.label34);
            this.groupBox8.Controls.Add(this.label35);
            this.groupBox8.Controls.Add(this.txt_IssueIDB);
            this.groupBox8.Controls.Add(this.label36);
            this.groupBox8.Controls.Add(this.label37);
            this.groupBox8.Controls.Add(this.label38);
            this.groupBox8.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.groupBox8.ForeColor = System.Drawing.Color.Black;
            this.groupBox8.Location = new System.Drawing.Point(6, 174);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(721, 133);
            this.groupBox8.TabIndex = 1;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Thông tin ứng tuyển";
            // 
            // txt_EmployeeNameB
            // 
            this.txt_EmployeeNameB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_EmployeeNameB.Font = new System.Drawing.Font("Arial", 9F);
            this.txt_EmployeeNameB.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_EmployeeNameB.Location = new System.Drawing.Point(121, 19);
            this.txt_EmployeeNameB.Name = "txt_EmployeeNameB";
            this.txt_EmployeeNameB.Size = new System.Drawing.Size(212, 21);
            this.txt_EmployeeNameB.TabIndex = 0;
            // 
            // txt_HomeAddressB
            // 
            this.txt_HomeAddressB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_HomeAddressB.Font = new System.Drawing.Font("Arial", 9F);
            this.txt_HomeAddressB.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_HomeAddressB.Location = new System.Drawing.Point(121, 99);
            this.txt_HomeAddressB.Name = "txt_HomeAddressB";
            this.txt_HomeAddressB.Size = new System.Drawing.Size(212, 21);
            this.txt_HomeAddressB.TabIndex = 2;
            // 
            // dte_IssueDateB
            // 
            this.dte_IssueDateB.CustomFormat = "dd/MM/yyyy";
            this.dte_IssueDateB.Font = new System.Drawing.Font("Arial", 9F);
            this.dte_IssueDateB.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dte_IssueDateB.Location = new System.Drawing.Point(483, 72);
            this.dte_IssueDateB.Name = "dte_IssueDateB";
            this.dte_IssueDateB.Size = new System.Drawing.Size(194, 21);
            this.dte_IssueDateB.TabIndex = 5;
            // 
            // dte_BirthdayB
            // 
            this.dte_BirthdayB.CustomFormat = "dd/MM/yyyy";
            this.dte_BirthdayB.Font = new System.Drawing.Font("Arial", 9F);
            this.dte_BirthdayB.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dte_BirthdayB.Location = new System.Drawing.Point(121, 72);
            this.dte_BirthdayB.Name = "dte_BirthdayB";
            this.dte_BirthdayB.Size = new System.Drawing.Size(212, 21);
            this.dte_BirthdayB.TabIndex = 1;
            // 
            // txt_IssuedPlaceB
            // 
            this.txt_IssuedPlaceB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_IssuedPlaceB.Font = new System.Drawing.Font("Arial", 9F);
            this.txt_IssuedPlaceB.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_IssuedPlaceB.Location = new System.Drawing.Point(483, 45);
            this.txt_IssuedPlaceB.Name = "txt_IssuedPlaceB";
            this.txt_IssuedPlaceB.Size = new System.Drawing.Size(194, 21);
            this.txt_IssuedPlaceB.TabIndex = 4;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(57, 75);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(62, 15);
            this.label32.TabIndex = 138;
            this.label32.Text = "Ngày sinh";
            // 
            // grp_Gender
            // 
            this.grp_Gender.Controls.Add(this.rdo_MaleB);
            this.grp_Gender.Controls.Add(this.rdo_FemaleB);
            this.grp_Gender.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Italic);
            this.grp_Gender.Location = new System.Drawing.Point(121, 36);
            this.grp_Gender.Name = "grp_Gender";
            this.grp_Gender.Size = new System.Drawing.Size(138, 30);
            this.grp_Gender.TabIndex = 1;
            this.grp_Gender.TabStop = false;
            // 
            // rdo_MaleB
            // 
            this.rdo_MaleB.AutoSize = true;
            this.rdo_MaleB.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Italic);
            this.rdo_MaleB.Location = new System.Drawing.Point(6, 9);
            this.rdo_MaleB.Name = "rdo_MaleB";
            this.rdo_MaleB.Size = new System.Drawing.Size(52, 19);
            this.rdo_MaleB.TabIndex = 0;
            this.rdo_MaleB.TabStop = true;
            this.rdo_MaleB.Text = "Nam";
            this.rdo_MaleB.UseVisualStyleBackColor = true;
            // 
            // rdo_FemaleB
            // 
            this.rdo_FemaleB.AutoSize = true;
            this.rdo_FemaleB.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Italic);
            this.rdo_FemaleB.Location = new System.Drawing.Point(82, 9);
            this.rdo_FemaleB.Name = "rdo_FemaleB";
            this.rdo_FemaleB.Size = new System.Drawing.Size(42, 19);
            this.rdo_FemaleB.TabIndex = 1;
            this.rdo_FemaleB.TabStop = true;
            this.rdo_FemaleB.Text = "Nữ";
            this.rdo_FemaleB.UseVisualStyleBackColor = true;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.Location = new System.Drawing.Point(76, 22);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(43, 15);
            this.label33.TabIndex = 127;
            this.label33.Text = "Họ tên";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.BackColor = System.Drawing.Color.Transparent;
            this.label34.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(68, 48);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(53, 15);
            this.label34.TabIndex = 133;
            this.label34.Text = "Giới tính";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.BackColor = System.Drawing.Color.Transparent;
            this.label35.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Black;
            this.label35.Location = new System.Drawing.Point(39, 102);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(80, 15);
            this.label35.TabIndex = 131;
            this.label35.Text = "Nguyên quán";
            // 
            // txt_IssueIDB
            // 
            this.txt_IssueIDB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_IssueIDB.Font = new System.Drawing.Font("Arial", 9F);
            this.txt_IssueIDB.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_IssueIDB.Location = new System.Drawing.Point(483, 17);
            this.txt_IssueIDB.Name = "txt_IssueIDB";
            this.txt_IssueIDB.Size = new System.Drawing.Size(194, 21);
            this.txt_IssueIDB.TabIndex = 3;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.BackColor = System.Drawing.Color.Transparent;
            this.label36.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Black;
            this.label36.Location = new System.Drawing.Point(431, 48);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(50, 15);
            this.label36.TabIndex = 131;
            this.label36.Text = "Nơi cấp";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.BackColor = System.Drawing.Color.Transparent;
            this.label37.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Black;
            this.label37.Location = new System.Drawing.Point(438, 20);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(43, 15);
            this.label37.TabIndex = 131;
            this.label37.Text = "CMND";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.BackColor = System.Drawing.Color.Transparent;
            this.label38.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Black;
            this.label38.Location = new System.Drawing.Point(423, 76);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(58, 15);
            this.label38.TabIndex = 133;
            this.label38.Text = "Ngày cấp";
            // 
            // groupBox11
            // 
            this.groupBox11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.groupBox11.Controls.Add(this.txt_WorkingDescription);
            this.groupBox11.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.groupBox11.ForeColor = System.Drawing.Color.Black;
            this.groupBox11.Location = new System.Drawing.Point(6, 478);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(720, 101);
            this.groupBox11.TabIndex = 1;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Mô tả công việc";
            // 
            // txt_WorkingDescription
            // 
            this.txt_WorkingDescription.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_WorkingDescription.Font = new System.Drawing.Font("Arial", 9F);
            this.txt_WorkingDescription.Location = new System.Drawing.Point(6, 20);
            this.txt_WorkingDescription.Multiline = true;
            this.txt_WorkingDescription.Name = "txt_WorkingDescription";
            this.txt_WorkingDescription.Size = new System.Drawing.Size(708, 75);
            this.txt_WorkingDescription.TabIndex = 0;
            // 
            // groupBox7
            // 
            this.groupBox7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.groupBox7.Controls.Add(this.txt_EmployeeNameA);
            this.groupBox7.Controls.Add(this.label21);
            this.groupBox7.Controls.Add(this.label39);
            this.groupBox7.Controls.Add(this.txt_PositionA);
            this.groupBox7.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.groupBox7.ForeColor = System.Drawing.Color.Black;
            this.groupBox7.Location = new System.Drawing.Point(6, 313);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(720, 63);
            this.groupBox7.TabIndex = 236;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Thông tin nhà tuyển dụng";
            // 
            // txt_EmployeeNameA
            // 
            this.txt_EmployeeNameA.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_EmployeeNameA.Font = new System.Drawing.Font("Arial", 9F);
            this.txt_EmployeeNameA.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_EmployeeNameA.Location = new System.Drawing.Point(121, 20);
            this.txt_EmployeeNameA.Name = "txt_EmployeeNameA";
            this.txt_EmployeeNameA.Size = new System.Drawing.Size(212, 21);
            this.txt_EmployeeNameA.TabIndex = 126;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(48, 23);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(71, 15);
            this.label21.TabIndex = 127;
            this.label21.Text = "Đại Diện Ký";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.BackColor = System.Drawing.Color.Transparent;
            this.label39.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.Black;
            this.label39.Location = new System.Drawing.Point(428, 23);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(54, 15);
            this.label39.TabIndex = 131;
            this.label39.Text = "Chức Vụ";
            // 
            // txt_PositionA
            // 
            this.txt_PositionA.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_PositionA.Font = new System.Drawing.Font("Arial", 9F);
            this.txt_PositionA.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_PositionA.Location = new System.Drawing.Point(484, 20);
            this.txt_PositionA.Name = "txt_PositionA";
            this.txt_PositionA.Size = new System.Drawing.Size(193, 21);
            this.txt_PositionA.TabIndex = 130;
            // 
            // groupBox10
            // 
            this.groupBox10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.groupBox10.Controls.Add(this.cbo_DepartmentContract);
            this.groupBox10.Controls.Add(this.cbo_PositionContract);
            this.groupBox10.Controls.Add(this.cbo_BranchContract);
            this.groupBox10.Controls.Add(this.cbo_TeamContract);
            this.groupBox10.Controls.Add(this.label12);
            this.groupBox10.Controls.Add(this.label15);
            this.groupBox10.Controls.Add(this.label22);
            this.groupBox10.Controls.Add(this.label17);
            this.groupBox10.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.groupBox10.ForeColor = System.Drawing.Color.Black;
            this.groupBox10.Location = new System.Drawing.Point(6, 382);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(720, 92);
            this.groupBox10.TabIndex = 237;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Thông tin công việc";
            // 
            // cbo_DepartmentContract
            // 
            this.cbo_DepartmentContract.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.cbo_DepartmentContract.Font = new System.Drawing.Font("Arial", 9F);
            this.cbo_DepartmentContract.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbo_DepartmentContract.FormattingEnabled = true;
            this.cbo_DepartmentContract.Location = new System.Drawing.Point(121, 20);
            this.cbo_DepartmentContract.Name = "cbo_DepartmentContract";
            this.cbo_DepartmentContract.Size = new System.Drawing.Size(212, 23);
            this.cbo_DepartmentContract.TabIndex = 134;
            // 
            // cbo_PositionContract
            // 
            this.cbo_PositionContract.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.cbo_PositionContract.Font = new System.Drawing.Font("Arial", 9F);
            this.cbo_PositionContract.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbo_PositionContract.FormattingEnabled = true;
            this.cbo_PositionContract.Location = new System.Drawing.Point(484, 20);
            this.cbo_PositionContract.Name = "cbo_PositionContract";
            this.cbo_PositionContract.Size = new System.Drawing.Size(193, 23);
            this.cbo_PositionContract.TabIndex = 135;
            // 
            // cbo_BranchContract
            // 
            this.cbo_BranchContract.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.cbo_BranchContract.Font = new System.Drawing.Font("Arial", 9F);
            this.cbo_BranchContract.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbo_BranchContract.FormattingEnabled = true;
            this.cbo_BranchContract.Location = new System.Drawing.Point(484, 49);
            this.cbo_BranchContract.Name = "cbo_BranchContract";
            this.cbo_BranchContract.Size = new System.Drawing.Size(193, 23);
            this.cbo_BranchContract.TabIndex = 136;
            // 
            // cbo_TeamContract
            // 
            this.cbo_TeamContract.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.cbo_TeamContract.Font = new System.Drawing.Font("Arial", 9F);
            this.cbo_TeamContract.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbo_TeamContract.FormattingEnabled = true;
            this.cbo_TeamContract.Location = new System.Drawing.Point(121, 49);
            this.cbo_TeamContract.Name = "cbo_TeamContract";
            this.cbo_TeamContract.Size = new System.Drawing.Size(212, 23);
            this.cbo_TeamContract.TabIndex = 137;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(66, 23);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 15);
            this.label12.TabIndex = 138;
            this.label12.Text = "Bộ phận";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(430, 24);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(52, 15);
            this.label15.TabIndex = 139;
            this.label15.Text = "Chức vụ";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(450, 54);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(32, 15);
            this.label22.TabIndex = 140;
            this.label22.Text = "Khối";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(63, 53);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(56, 15);
            this.label17.TabIndex = 141;
            this.label17.Text = "Tổ nhóm";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.btn_Finish);
            this.panel3.Controls.Add(this.btn_Save_New);
            this.panel3.Controls.Add(this.btn_Add);
            this.panel3.Controls.Add(this.btn_New);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 709);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1050, 59);
            this.panel3.TabIndex = 2;
            // 
            // btn_Finish
            // 
            this.btn_Finish.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Finish.Location = new System.Drawing.Point(790, 7);
            this.btn_Finish.Name = "btn_Finish";
            this.btn_Finish.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Finish.Size = new System.Drawing.Size(120, 40);
            this.btn_Finish.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Finish.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Finish.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Finish.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Finish.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Finish.TabIndex = 9;
            this.btn_Finish.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Finish.Values.Image")));
            this.btn_Finish.Values.Text = "Đến hạn";
            // 
            // btn_Save_New
            // 
            this.btn_Save_New.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Save_New.Location = new System.Drawing.Point(916, 7);
            this.btn_Save_New.Name = "btn_Save_New";
            this.btn_Save_New.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Save_New.Size = new System.Drawing.Size(120, 40);
            this.btn_Save_New.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Save_New.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Save_New.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Save_New.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Save_New.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Save_New.TabIndex = 7;
            this.btn_Save_New.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Save_New.Values.Image")));
            this.btn_Save_New.Values.Text = "Thêm mới";
            // 
            // btn_Add
            // 
            this.btn_Add.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Add.Location = new System.Drawing.Point(916, 7);
            this.btn_Add.Name = "btn_Add";
            this.btn_Add.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Add.Size = new System.Drawing.Size(120, 40);
            this.btn_Add.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Add.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Add.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Add.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Add.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Add.TabIndex = 7;
            this.btn_Add.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Add.Values.Image")));
            this.btn_Add.Values.Text = "Cập nhật";
            // 
            // btn_New
            // 
            this.btn_New.Location = new System.Drawing.Point(12, 9);
            this.btn_New.Name = "btn_New";
            this.btn_New.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_New.Size = new System.Drawing.Size(120, 40);
            this.btn_New.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_New.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_New.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_New.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_New.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_New.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_New.TabIndex = 8;
            this.btn_New.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_New.Values.Image")));
            this.btn_New.Values.Text = "Làm mới";
            // 
            // Header
            // 
            this.Header.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnMini,
            this.btnMax,
            this.btnClose});
            this.Header.Dock = System.Windows.Forms.DockStyle.Top;
            this.Header.Location = new System.Drawing.Point(0, 0);
            this.Header.Name = "Header";
            this.Header.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.Header.Size = new System.Drawing.Size(1050, 42);
            this.Header.TabIndex = 5;
            this.Header.Values.Description = "";
            this.Header.Values.Heading = "Hợp đồng công nhân";
            this.Header.Values.Image = ((System.Drawing.Image)(resources.GetObject("Header.Values.Image")));
            this.Header.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Frm_Main_MouseDown);
            this.Header.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Frm_Main_MouseMove);
            this.Header.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Frm_Main_MouseUp);
            // 
            // btnMini
            // 
            this.btnMini.Image = ((System.Drawing.Image)(resources.GetObject("btnMini.Image")));
            this.btnMini.UniqueName = "F5F06E8241504E72ABB5BEA3F6A9B753";
            this.btnMini.Click += new System.EventHandler(this.btnMini_Click);
            // 
            // btnMax
            // 
            this.btnMax.Image = ((System.Drawing.Image)(resources.GetObject("btnMax.Image")));
            this.btnMax.UniqueName = "035D1A4881E44F58A084C31DE7352A94";
            this.btnMax.Click += new System.EventHandler(this.btnMax_Click);
            // 
            // btnClose
            // 
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.UniqueName = "11B07C6F4E1C4F9D8B91BD924CB0EBE6";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // Panel_Search
            // 
            this.Panel_Search.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Search.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel_Search.Controls.Add(this.btn_Search);
            this.Panel_Search.Controls.Add(this.cbo_Shearch_Branch);
            this.Panel_Search.Controls.Add(this.txt_Search);
            this.Panel_Search.Controls.Add(this.cbo_Teams);
            this.Panel_Search.Controls.Add(this.label20);
            this.Panel_Search.Controls.Add(this.label42);
            this.Panel_Search.Controls.Add(this.lbl_team);
            this.Panel_Search.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel_Search.Location = new System.Drawing.Point(0, 0);
            this.Panel_Search.Name = "Panel_Search";
            this.Panel_Search.Size = new System.Drawing.Size(300, 102);
            this.Panel_Search.TabIndex = 203;
            // 
            // btn_Search
            // 
            this.btn_Search.Location = new System.Drawing.Point(255, 47);
            this.btn_Search.Name = "btn_Search";
            this.btn_Search.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Search.Size = new System.Drawing.Size(40, 40);
            this.btn_Search.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Search.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Search.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Search.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Search.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Search.TabIndex = 203;
            this.btn_Search.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Search.Values.Image")));
            this.btn_Search.Values.Text = "";
            // 
            // Panel_Left
            // 
            this.Panel_Left.Controls.Add(this.Panel_Contract);
            this.Panel_Left.Controls.Add(this.Panel_Employee);
            this.Panel_Left.Controls.Add(this.Panel_Search);
            this.Panel_Left.Dock = System.Windows.Forms.DockStyle.Left;
            this.Panel_Left.Location = new System.Drawing.Point(0, 42);
            this.Panel_Left.Name = "Panel_Left";
            this.Panel_Left.Size = new System.Drawing.Size(300, 667);
            this.Panel_Left.TabIndex = 207;
            // 
            // Panel_Employee
            // 
            this.Panel_Employee.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Employee.Controls.Add(this.LV_Employee);
            this.Panel_Employee.Controls.Add(this.kryptonHeader5);
            this.Panel_Employee.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel_Employee.Location = new System.Drawing.Point(0, 102);
            this.Panel_Employee.Name = "Panel_Employee";
            this.Panel_Employee.Size = new System.Drawing.Size(300, 376);
            this.Panel_Employee.TabIndex = 204;
            // 
            // LV_Employee
            // 
            this.LV_Employee.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.LV_Employee.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LV_Employee.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LV_Employee.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.LV_Employee.FullRowSelect = true;
            this.LV_Employee.GridLines = true;
            this.LV_Employee.HideSelection = false;
            this.LV_Employee.Location = new System.Drawing.Point(0, 30);
            this.LV_Employee.Name = "LV_Employee";
            this.LV_Employee.Size = new System.Drawing.Size(300, 346);
            this.LV_Employee.TabIndex = 203;
            this.LV_Employee.UseCompatibleStateImageBehavior = false;
            this.LV_Employee.View = System.Windows.Forms.View.Details;
            // 
            // kryptonHeader5
            // 
            this.kryptonHeader5.AutoSize = false;
            this.kryptonHeader5.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnHide,
            this.btnShow});
            this.kryptonHeader5.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader5.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader5.Name = "kryptonHeader5";
            this.kryptonHeader5.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader5.Size = new System.Drawing.Size(300, 30);
            this.kryptonHeader5.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader5.TabIndex = 204;
            this.kryptonHeader5.Values.Description = "";
            this.kryptonHeader5.Values.Heading = "Công nhân";
            // 
            // btnHide
            // 
            this.btnHide.Image = ((System.Drawing.Image)(resources.GetObject("btnHide.Image")));
            this.btnHide.UniqueName = "ABE0F8FEF4FF4DE3448D0BAE7F1C6F22";
            this.btnHide.Click += new System.EventHandler(this.btnHide_Click);
            // 
            // btnShow
            // 
            this.btnShow.Image = ((System.Drawing.Image)(resources.GetObject("btnShow.Image")));
            this.btnShow.UniqueName = "D6E0AB28C2CE49FBDD866175ABE4E274";
            this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.DodgerBlue;
            this.label6.Dock = System.Windows.Forms.DockStyle.Left;
            this.label6.Location = new System.Drawing.Point(300, 42);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(2, 667);
            this.label6.TabIndex = 208;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.DodgerBlue;
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Location = new System.Drawing.Point(302, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(2, 667);
            this.label1.TabIndex = 209;
            // 
            // Panel_Contract
            // 
            this.Panel_Contract.Controls.Add(this.LV_List_Contract);
            this.Panel_Contract.Controls.Add(this.kryptonHeader2);
            this.Panel_Contract.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel_Contract.Location = new System.Drawing.Point(0, 478);
            this.Panel_Contract.Name = "Panel_Contract";
            this.Panel_Contract.Size = new System.Drawing.Size(300, 189);
            this.Panel_Contract.TabIndex = 205;
            // 
            // LV_List_Contract
            // 
            this.LV_List_Contract.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.LV_List_Contract.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LV_List_Contract.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LV_List_Contract.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.LV_List_Contract.FullRowSelect = true;
            this.LV_List_Contract.GridLines = true;
            this.LV_List_Contract.HideSelection = false;
            this.LV_List_Contract.Location = new System.Drawing.Point(0, 30);
            this.LV_List_Contract.Name = "LV_List_Contract";
            this.LV_List_Contract.Size = new System.Drawing.Size(300, 159);
            this.LV_List_Contract.TabIndex = 204;
            this.LV_List_Contract.UseCompatibleStateImageBehavior = false;
            this.LV_List_Contract.View = System.Windows.Forms.View.Details;
            // 
            // kryptonHeader2
            // 
            this.kryptonHeader2.AutoSize = false;
            this.kryptonHeader2.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader2.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader2.Name = "kryptonHeader2";
            this.kryptonHeader2.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader2.Size = new System.Drawing.Size(300, 30);
            this.kryptonHeader2.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader2.TabIndex = 203;
            this.kryptonHeader2.Values.Description = "";
            this.kryptonHeader2.Values.Heading = "Hợp đồng";
            // 
            // Frm_Employee_Contract
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1050, 768);
            this.Controls.Add(this.Panel_Right);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.Panel_Left);
            this.Controls.Add(this.Header);
            this.Controls.Add(this.panel3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_Employee_Contract";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.FRM_Contract_Employee_Load);
            this.Panel_Right.ResumeLayout(false);
            this.groupBox15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GV_Chil)).EndInit();
            this.groupBox12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GV_Fee_Minus)).EndInit();
            this.groupBox9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GV_Fee_Plus)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.grp_Probation.ResumeLayout(false);
            this.grp_Probation.PerformLayout();
            this.groupBox16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GV_BasicSalary)).EndInit();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.grp_Gender.ResumeLayout(false);
            this.grp_Gender.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.Panel_Search.ResumeLayout(false);
            this.Panel_Search.PerformLayout();
            this.Panel_Left.ResumeLayout(false);
            this.Panel_Employee.ResumeLayout(false);
            this.Panel_Contract.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel Panel_Right;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ComboBox cbo_Shearch_Branch;
        private System.Windows.Forms.ComboBox cbo_Teams;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label lbl_team;
        private System.Windows.Forms.TextBox txt_Search;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.DataGridView GV_Chil;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.DataGridView GV_Fee_Plus;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.DataGridView GV_BasicSalary;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.ComboBox cbo_DepartmentContract;
        private System.Windows.Forms.ComboBox cbo_PositionContract;
        private System.Windows.Forms.ComboBox cbo_BranchContract;
        private System.Windows.Forms.ComboBox cbo_TeamContract;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox txt_EmployeeNameA;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox txt_PositionA;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.TextBox txt_HomeAddressB;
        private System.Windows.Forms.DateTimePicker dte_IssueDateB;
        private System.Windows.Forms.DateTimePicker dte_BirthdayB;
        private System.Windows.Forms.TextBox txt_IssuedPlaceB;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.GroupBox grp_Gender;
        private System.Windows.Forms.RadioButton rdo_MaleB;
        private System.Windows.Forms.RadioButton rdo_FemaleB;
        private System.Windows.Forms.TextBox txt_EmployeeNameB;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox txt_IssueIDB;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox grp_Probation;
        private System.Windows.Forms.RadioButton rdo_First;
        private System.Windows.Forms.RadioButton rdo_Late;
        private System.Windows.Forms.Label lbl_Probation;
        private System.Windows.Forms.ComboBox cbo_CategoryKey;
        private System.Windows.Forms.DateTimePicker dte_FinishDay;
        private System.Windows.Forms.DateTimePicker dte_Fromdate;
        private System.Windows.Forms.DateTimePicker dte_Signday;
        private System.Windows.Forms.DateTimePicker dte_Todate;
        private System.Windows.Forms.TextBox txt_ContractID;
        private System.Windows.Forms.Label lbl_FinishDay;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label lbl_Today;
        private System.Windows.Forms.Label lbl_signday;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label lbl_Fromday;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader Header;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMini;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMax;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnClose;
        private System.Windows.Forms.Panel Panel_Search;
        private System.Windows.Forms.Panel Panel_Left;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Search;
        private System.Windows.Forms.Panel Panel_Employee;
        private System.Windows.Forms.ListView LV_Employee;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Add;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_New;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Finish;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Save_New;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label1;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader4;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.DataGridView GV_Fee_Minus;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.TextBox txt_WorkingDescription;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader5;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnHide;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnShow;
        private System.Windows.Forms.Panel Panel_Contract;
        private System.Windows.Forms.ListView LV_List_Contract;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader2;
    }
}