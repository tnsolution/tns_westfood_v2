﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using TNS.HRM;
using TNS.HRM.BasicSalary;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Employee_Contract : Form
    {
        private string _PersonalKey = "";
        private string zKey = "";
        private Employee_Object _Emp_Obj;
        private DataTable zTable;
        private string _ContractKey = "";
        private int _AutoKey = 0;
        private string zCopy = "";

        public Frm_Employee_Contract()
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            Utils.DrawGVStyle(ref GV_Chil);
            Utils.DrawGVStyle(ref GV_BasicSalary);
            Utils.DrawGVStyle(ref GV_Fee_Minus);
            Utils.DrawGVStyle(ref GV_Fee_Plus);
            Utils.DrawLVStyle(ref LV_Employee);
            Utils.DrawLVStyle(ref LV_List_Contract);

            LV_Employee.Click += LV_Employee_Click;
            LV_Employee.ItemActivate += LV_Employee_ItemActivate;

            LV_Layout(LV_Employee);
            LV_List_Layout(LV_List_Contract);
            LV_List_Contract.Click += LV_List_Contract_Click;
            //---- DataGridView_Fee_Plus
            GV_Fee_Plus_Layout();
            GV_Fee_Plus.KeyDown += GV_Fee_Plus_KeyDown;
            GV_Fee_Plus.CellClick += GV_Fee_Plus_CellClick;
            //---- DataGridView_Fee_Minus
            GV_Fee_Minus_Layout();
            GV_Fee_Minus.KeyDown += GV_Fee_Minus_KeyDown;
            GV_Fee_Minus.CellClick += GV_Fee_Minus_CellClick;
            //---- DataGridView_Fee_Chil
            GV_Chil_Layout();
            GV_Chil.CellClick += GV_Chil_CellClick;
            GV_Chil.KeyDown += GV_Chil_KeyDown;
            //---- DataGridView_Basic_Salary
            GV_Basic_Salary_Layout();
            GV_BasicSalary.KeyDown += GV_BasicSalary_KeyDown;
            GV_BasicSalary.CellClick += GV_BasicSalary_CellClick;
            //---- Button
            cbo_CategoryKey.SelectedValueChanged += cbo_CategoryKey_SelectedValueChanged;
            cbo_Shearch_Branch.SelectedIndexChanged += cbo_Shearch_Branch_SelectedIndexChanged;
            btn_Search.Click += Btn_Search_Click;
            btn_Add.Click += Btn_Add_Click;
            btn_New.Click += Btn_New_Click;
            btn_Finish.Click += Btn_Finish_Click;
            btn_Save_New.Click += Btn_Save_New_Click;

            hidden();

            btnShow.Visible = false;
            btnHide.Visible = true;
            Panel_Search.Visible = true;

            this.Bounds = Screen.PrimaryScreen.WorkingArea;
        }

       
        private void FRM_Contract_Employee_Load(object sender, EventArgs e)
        {
            LoadBox();
            LV_LoadData_Dif();
            LV_LoadData_List();
        }
        private void hidden()
        {
            txt_EmployeeNameB.Enabled = false;
            grp_Gender.Enabled = false;
            txt_IssueIDB.Enabled = false;
            txt_IssuedPlaceB.Enabled = false;
            txt_HomeAddressB.Enabled = false;
            dte_BirthdayB.Enabled = false;
            dte_IssueDateB.Enabled = false;
            cbo_BranchContract.Enabled = false;
            cbo_DepartmentContract.Enabled = false;
            cbo_PositionContract.Enabled = false;
            cbo_TeamContract.Enabled = false;
            txt_PositionA.Enabled = false;
            txt_EmployeeNameA.Enabled = false;
        }
        private void LoadBox()
        {
            LoadDataToToolbox.ComboBoxData(cbo_DepartmentContract, Department_Data.List(), false, 0, 1);
            LoadDataToToolbox.ComboBoxData(cbo_BranchContract, Branch_Data.List(), false, 0, 1);
            LoadDataToToolbox.ComboBoxData(cbo_TeamContract, Teams_Data.List(), false, 0, 1);
            LoadDataToToolbox.ComboBoxData(cbo_PositionContract, Position_Data.List(), false, 0, 1);
            LoadDataToToolbox.ComboBoxData(cbo_CategoryKey, Contract_Category_Data.List(), false, 0, 1);
            LoadDataToToolbox.ComboBoxData(cbo_Teams, "SELECT TeamKey,TeamName FROM SYS_Team WHERE TeamKey != 98  AND RecordStatus < 99", "---- Choose Teams ----");
            LoadDataToToolbox.ComboBoxData(cbo_Shearch_Branch, "SELECT BranchKey,BranchName FROM SYS_Branch WHERE BranchKey != 98  AND RecordStatus < 99", "---- Choose Branchs ----");
        }
        #region ----- Left -----
        private void LV_Layout(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Nhân Viên";
            colHead.Width = 180;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã Nhân Viên";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);
        }
        private void LV_LoadData_Dif()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LV_Employee;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            int _Team = 0;
            Name = "";
            DataTable In_Table = new DataTable();
            if (cbo_Teams.SelectedValue != null)
            {
                _Team = int.Parse(cbo_Teams.SelectedValue.ToString());
            }
            else
            {
                _Team = 0;
            }
            if (txt_Search.Text.ToString() != null)
            {
                Name = txt_Search.Text.ToString();
            }
            In_Table = Employee_Data.Load_Data(_Team, Name);
            LV.Items.Clear();
            int n = In_Table.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                DataRow nRow = In_Table.Rows[i];
                lvi.Tag = nRow["EmployeeKey"];
                lvi.ForeColor = Color.Navy;

                lvi.BackColor = Color.White;
                lvi.ImageIndex = 0;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["FullName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["CardID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["PersonalKey"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }

            this.Cursor = Cursors.Default;
        }
        private void LV_Employee_ItemActivate(object sender, EventArgs e)
        {
            zKey = LV_Employee.SelectedItems[0].Tag.ToString();
            Load_Data();
        }
        private void LV_Employee_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < LV_Employee.Items.Count; i++)
            {
                if (LV_Employee.Items[i].Selected == true)
                {
                    LV_Employee.Items[i].BackColor = Color.LightBlue;
                }
                else
                {
                    LV_Employee.Items[i].BackColor = SystemColors.Window;
                }
            }

            _PersonalKey = LV_Employee.SelectedItems[0].SubItems[3].Text;
            zKey = LV_Employee.SelectedItems[0].Tag.ToString();
            _ContractKey = "";
            LV_LoadData_List();
            Load_New_Data();
        }
        private void LV_List_Layout(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số Hợp Đồng";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tình Trạng";
            colHead.Width = 110;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);
        }
        private void LV_LoadData_List()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LV_List_Contract;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            DataTable In_Table = new DataTable();
            In_Table = Employee_Data.Load_Contract(_PersonalKey);
            LV.Items.Clear();
            int n = In_Table.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                DataRow nRow = In_Table.Rows[i];
                lvi.Tag = nRow["ContractKey"];
                lvi.ForeColor = Color.Navy;

                lvi.BackColor = Color.White;
                lvi.ImageIndex = 0;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["ContractID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                int zRecorStatus = int.Parse(nRow["RecordStatus"].ToString().Trim());
                if (zRecorStatus == 62)
                {
                    lvsi = new ListViewItem.ListViewSubItem();
                    lvsi.Text = "Hết Hạn";
                    lvi.ForeColor = Color.Red;
                    lvi.SubItems.Add(lvsi);


                }
                else
                {
                    lvsi = new ListViewItem.ListViewSubItem();
                    lvsi.Text = "Còn Hạn";
                    lvi.SubItems.Add(lvsi);

                }

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["PersonalKey"].ToString().Trim();
                lvi.SubItems.Add(lvsi);
                LV.Items.Add(lvi);
            }

            this.Cursor = Cursors.Default;
        }
        private void LV_List_Contract_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < LV_List_Contract.Items.Count; i++)
            {
                if (LV_List_Contract.Items[i].Selected == true)
                {
                    LV_List_Contract.Items[i].BackColor = Color.LightBlue;
                }
                else
                {
                    LV_List_Contract.Items[i].BackColor = SystemColors.Window;
                }
            }
            _ContractKey = LV_List_Contract.SelectedItems[0].Tag.ToString();
            //zKey = LV_Employee.SelectedItems[0].Tag.ToString();
            Load_Data();
        }

        #endregion
        #region ----- Desgin Layout DataGridView And Process Event -----
        //-----GV_Basic_Salary-----
        private void GV_Basic_Salary_Layout()
        {
            GV_BasicSalary.Columns.Add("No", "STT");
            GV_BasicSalary.Columns.Add("FromDate", "Từ Ngày");
            GV_BasicSalary.Columns.Add("Amount", "Số Tiền");

            GV_BasicSalary.Columns["No"].Width = 40;
            GV_BasicSalary.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV_BasicSalary.Columns["No"].ReadOnly = true;

            GV_BasicSalary.Columns["FromDate"].Width = 300;
            GV_BasicSalary.Columns["FromDate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV_BasicSalary.Columns["Amount"].Width = 415;
            GV_BasicSalary.Columns["Amount"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

        }
        private void GV_BasicSalary_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                DialogResult dlr = MessageBox.Show("Bạn có xóa thông tin này ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlr == DialogResult.Yes)
                {
                    if (GV_BasicSalary.CurrentRow.Tag != null)
                    {
                        BasicSalary_Info zBasic = (BasicSalary_Info)GV_Fee_Plus.CurrentRow.Tag;
                        GV_BasicSalary.CurrentRow.Visible = false;
                        zBasic.AutoKey = _AutoKey;
                        zBasic.Delete();
                    }
                }
            }
        }
        private void GV_BasicSalary_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            _AutoKey = GV_BasicSalary.Rows[e.RowIndex].Tag.ToInt();
        }
        //-----GV_Fee_Plus-----
        private void GV_Fee_Plus_Layout()
        {
            GV_Fee_Plus.Columns.Add("No", "STT");

            DataGridViewComboBoxColumn cboCategory = new DataGridViewComboBoxColumn();
            cboCategory.HeaderText = "Tên danh mục";
            cboCategory.Name = "cboCategory";
            LoadDataToToolbox.ComboBoxData(cboCategory, "SELECT CategoryKey,CategoryName  FROM FNC_Fee_Category WHERE Slug = 3 AND CategoryKey != 14 AND RecordStatus <99", true);
            cboCategory.Width = 150;
            cboCategory.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
            GV_Fee_Plus.Columns.Add(cboCategory);

            GV_Fee_Plus.Columns.Add("FromDate", "Từ Ngày");
            GV_Fee_Plus.Columns.Add("ToDate", "Đến Ngày");
            GV_Fee_Plus.Columns.Add("PercentFee", "Phần Trăm (%)");
            GV_Fee_Plus.Columns.Add("Amount", "Số Tiền");

            GV_Fee_Plus.Columns["No"].Width = 40;
            GV_Fee_Plus.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV_Fee_Plus.Columns["No"].ReadOnly = true;

            GV_Fee_Plus.Columns["FromDate"].Width = 130;
            GV_Fee_Plus.Columns["FromDate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV_Fee_Plus.Columns["ToDate"].Width = 130;
            GV_Fee_Plus.Columns["ToDate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV_Fee_Plus.Columns["PercentFee"].Width = 125;
            GV_Fee_Plus.Columns["PercentFee"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV_Fee_Plus.Columns["Amount"].Width = 125;
            GV_Fee_Plus.Columns["Amount"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

        }
        private void ShowProductInGridView_Plus(int RowIndex)
        {
            DataGridViewRow zRowView = GV_Fee_Plus.Rows[RowIndex];
            zRowView.Cells["No"].Value = (RowIndex + 1).ToString();
        }
        private void GV_Fee_Plus_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            _AutoKey = GV_Fee_Plus.Rows[e.RowIndex].Tag.ToInt();
        }
        private void GV_Fee_Plus_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                DialogResult dlr = MessageBox.Show("Bạn có xóa thông tin này ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlr == DialogResult.Yes)
                {
                    GV_Fee_Plus.CurrentRow.Visible = false;
                    Contract_Fee_Info zFeePlus = new Contract_Fee_Info();
                    zFeePlus.AutoKey = _AutoKey;
                    zFeePlus.Delete();
                }
            }
        }
        //-----GV_Fee_Minus-----
        private void GV_Fee_Minus_Layout()
        {
            GV_Fee_Minus.Columns.Add("No", "STT");

            DataGridViewComboBoxColumn cboCategory = new DataGridViewComboBoxColumn();
            cboCategory.HeaderText = "Tên danh mục";
            cboCategory.Name = "cboCategory";
            LoadDataToToolbox.ComboBoxData(cboCategory, "SELECT CategoryKey,CategoryName FROM FNC_Fee_Category WHERE Slug = 4 AND RecordStatus <99", true);
            cboCategory.Width = 150;
            cboCategory.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
            GV_Fee_Minus.Columns.Add(cboCategory);

            GV_Fee_Minus.Columns.Add("FromDate", "Từ Ngày");
            GV_Fee_Minus.Columns.Add("ToDate", "Đến Ngày");
            GV_Fee_Minus.Columns.Add("PercentFee", "Phần trăm (%)");
            GV_Fee_Minus.Columns.Add("Amount", "Số Tiền");

            GV_Fee_Minus.Columns["No"].Width = 40;
            GV_Fee_Minus.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV_Fee_Minus.Columns["No"].ReadOnly = true;

            GV_Fee_Minus.Columns["FromDate"].Width = 130;
            GV_Fee_Minus.Columns["FromDate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV_Fee_Minus.Columns["ToDate"].Width = 130;
            GV_Fee_Minus.Columns["ToDate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV_Fee_Minus.Columns["PercentFee"].Width = 125;
            GV_Fee_Minus.Columns["PercentFee"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV_Fee_Minus.Columns["Amount"].Width = 125;
            GV_Fee_Minus.Columns["Amount"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

        }
        private void ShowProductInGridView_Minus(int RowIndex)
        {
            DataGridViewRow zRowView = GV_Fee_Minus.Rows[RowIndex];
            zRowView.Cells["No"].Value = (RowIndex + 1).ToString();
        }
        private void GV_Fee_Minus_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            _AutoKey = GV_Fee_Minus.Rows[e.RowIndex].Tag.ToInt();
        }
        private void GV_Fee_Minus_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                DialogResult dlr = MessageBox.Show("Bạn có xóa thông tin này ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlr == DialogResult.Yes)
                {

                    GV_Fee_Minus.CurrentRow.Visible = false;
                    Contract_Fee_Info zFeeMinus = new Contract_Fee_Info();
                    zFeeMinus.AutoKey = _AutoKey;
                    zFeeMinus.Delete();
                }
            }
        }
        //-----GV_Chil-----
        private void GV_Chil_Layout()
        {

            GV_Chil.Columns.Add("No", "STT");

            DataGridViewComboBoxColumn cboCategory = new DataGridViewComboBoxColumn();
            cboCategory.HeaderText = "Tên danh mục";
            cboCategory.Name = "cboCategory";
            LoadDataToToolbox.ComboBoxData(cboCategory, "SELECT CategoryKey,CategoryName FROM FNC_Fee_Category WHERE Slug = 3 AND CategoryKey = 14 AND RecordStatus < 99", true);
            cboCategory.Width = 150;
            cboCategory.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
            GV_Chil.Columns.Add(cboCategory);

            GV_Chil.Columns.Add("FromDate", "Từ Ngày");
            GV_Chil.Columns.Add("ToDate", "Đến Ngày");
            GV_Chil.Columns.Add("PercentFee", "Phần Trăm (%)");
            GV_Chil.Columns.Add("Amount", "Số Tiền");

            GV_Chil.Columns["No"].Width = 40;
            GV_Chil.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV_Chil.Columns["No"].ReadOnly = true;


            GV_Chil.Columns["FromDate"].Width = 130;
            GV_Chil.Columns["FromDate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV_Chil.Columns["ToDate"].Width = 130;
            GV_Chil.Columns["ToDate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV_Chil.Columns["PercentFee"].Width = 125;
            GV_Chil.Columns["PercentFee"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV_Chil.Columns["Amount"].Width = 125;
            GV_Chil.Columns["Amount"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        }
        private void ShowProductInGridView_Chil(int RowIndex)
        {
            DataGridViewRow zRowView = GV_Chil.Rows[RowIndex];
            zRowView.Cells["No"].Value = (RowIndex + 1).ToString();
        }
        private void GV_Chil_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                DialogResult dlr = MessageBox.Show("Bạn có xóa thông tin này ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlr == DialogResult.Yes)
                {
                    GV_Chil.CurrentRow.Visible = false;
                    Contract_Fee_Info zChil = new Contract_Fee_Info();
                    zChil.AutoKey = _AutoKey;
                    zChil.Delete();
                }
            }
        }
        private void GV_Chil_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            _AutoKey = GV_Chil.Rows[e.RowIndex].Tag.ToInt();
        }
        #endregion
        #region-----Process Data-----
        private void Save_GV_Basic_Salary()
        {
            for (int i = 0; i < GV_BasicSalary.Rows.Count - 1; i++)
            {
                BasicSalary_Info zSalary = new BasicSalary_Info();
                zSalary.EmployeeKey = zKey;
                if (GV_BasicSalary.Rows[i].Tag != null)
                    zSalary.AutoKey = int.Parse(GV_BasicSalary.Rows[i].Tag.ToString());
                else
                    zSalary.AutoKey = 0;
                if (GV_BasicSalary.Rows[i].Cells["Amount"].Value != null)
                    zSalary.Amount = double.Parse(GV_BasicSalary.Rows[i].Cells["Amount"].Value.ToString());
                if (GV_BasicSalary.Rows[i].Cells["FromDate"].Value != null)
                    zSalary.FromDate = DateTime.Parse(GV_BasicSalary.Rows[i].Cells["FromDate"].Value.ToString());
                zSalary.Save();
            }
        }
        private void Load_GV_Basic_Salary()
        {
            string KeyTemp = "";
            if (_ContractKey != "")
            {
                KeyTemp = zKey;
            }
            GV_BasicSalary.Rows.Clear();
            zTable = BasicSalary_Data.Basic_Salary(KeyTemp);
            int i = 0;
            foreach (DataRow nRow in zTable.Rows)
            {
                GV_BasicSalary.Rows.Add();
                DataGridViewRow nRowView = GV_BasicSalary.Rows[i];
                nRowView.Tag = nRow["AutoKey"].ToString().Trim();
                nRowView.Cells["NO"].Value = (i + 1).ToString();
                double zAmount = double.Parse(nRow["Amount"].ToString());
                nRowView.Cells["Amount"].Value = zAmount.ToString("#,###,###");
                DateTime zFromdate = DateTime.Parse(nRow["FromDate"].ToString());
                nRowView.Cells["FromDate"].Value = zFromdate.ToString("dd/MM/yyyy");
                i++;
            }
        }

        private void Load_GV_Fee_Plus()
        {
            GV_Fee_Plus.Rows.Clear();
            zTable = Contract_Fee_Data.DataTable_Plus(_ContractKey);
            int i = 0;
            foreach (DataRow nRow in zTable.Rows)
            {
                GV_Fee_Plus.Rows.Add();
                DataGridViewRow nRowView = GV_Fee_Plus.Rows[i];
                nRowView.Tag = nRow["AutoKey"].ToString().Trim();
                nRowView.Cells["NO"].Value = (i + 1).ToString();
                nRowView.Cells["cboCategory"].Value = nRow["CategoryKey"].ToInt();
                nRowView.Cells["PercentFee"].Value = nRow["PercentFee"].ToString().Trim();
                if (nRow["FromDate"] == DBNull.Value)
                {
                    nRowView.Cells["FromDate"].Value = "";

                }
                else
                {
                    DateTime zFromdate = DateTime.Parse(nRow["FromDate"].ToString());
                    nRowView.Cells["FromDate"].Value = zFromdate.ToString("dd/MM/yyyy");
                }
                if (nRow["ToDate"]== DBNull.Value)
                {
                    nRowView.Cells["ToDate"].Value = "";

                }
                else
                {
                    DateTime zToDate = DateTime.Parse(nRow["ToDate"].ToString());
                    nRowView.Cells["ToDate"].Value = zToDate.ToString("dd/MM/yyyy");
                }
                double zAmount = double.Parse(nRow["Amount"].ToString());
                nRowView.Cells["Amount"].Value = zAmount.ToString("#,###,##0");
                i++;
            }
        }
        private void Save_GV_Fee_Plus()
        {
            for (int i = 0; i < GV_Fee_Plus.Rows.Count - 1; i++)
            {
                if (GV_Fee_Plus.Rows[i].Cells["cboCategory"].Value != null)
                {
                    int zAutoKey = 0;
                    if (GV_Fee_Plus.Rows[i].Tag == null)
                    {
                        zAutoKey = 0;
                    }
                    else
                    {
                        zAutoKey = int.Parse(GV_Fee_Plus.Rows[i].Tag.ToString());
                    }
                    Contract_Fee_Info zContractKey = new Contract_Fee_Info(zAutoKey);
                    zContractKey.ContractKey = _ContractKey;

                    zContractKey.CategoryKey = int.Parse(GV_Fee_Plus.Rows[i].Cells["cboCategory"].Value.ToString());
                    if (GV_Fee_Plus.Rows[i].Cells["Amount"].Value == null || GV_Fee_Plus.Rows[i].Cells["Amount"].Value.ToString() == "")
                        zContractKey.Amount = 0;
                    else
                        zContractKey.Amount = double.Parse(GV_Fee_Plus.Rows[i].Cells["Amount"].Value.ToString());
                    if (GV_Fee_Plus.Rows[i].Cells["PercentFee"].Value != null)
                        zContractKey.PercentFee = float.Parse(GV_Fee_Plus.Rows[i].Cells["PercentFee"].Value.ToString());

                    if (GV_Fee_Plus.Rows[i].Cells["FromDate"].Value != null || GV_Fee_Plus.Rows[i].Cells["FromDate"].Value.ToString() == "")
                    {
                        zContractKey.FromDate = DateTime.MinValue;
                    }
                    else
                    {
                        DateTime zFromDate = DateTime.MinValue;
                        if (DateTime.TryParse(GV_Fee_Plus.Rows[i].Cells["FromDate"].Value.ToString(), out zFromDate))
                        {
                            zContractKey.FromDate = DateTime.Parse(GV_Fee_Plus.Rows[i].Cells["FromDate"].Value.ToString());

                        }
                        else
                        {
                            zContractKey.ToDate = DateTime.MinValue;
                        }
                    }
                       
                    if (GV_Fee_Plus.Rows[i].Cells["ToDate"].Value == null || GV_Fee_Plus.Rows[i].Cells["ToDate"].Value.ToString() == "")
                    {
                        zContractKey.ToDate = DateTime.MinValue;
                    }
                    else
                    {
                        DateTime zToDate = DateTime.MinValue;
                        if (DateTime.TryParse(GV_Fee_Plus.Rows[i].Cells["ToDate"].Value.ToString(), out zToDate))
                        {
                            zContractKey.ToDate = DateTime.Parse(GV_Fee_Plus.Rows[i].Cells["ToDate"].Value.ToString());

                        }
                        else
                        {
                            zContractKey.ToDate = DateTime.MinValue;
                        }
                    }
                    zContractKey.Save();
                }
            }
        }


        private void Load_GV_Fee_Minus()
        {
            GV_Fee_Minus.Rows.Clear();
            zTable = Contract_Fee_Data.DataTable_Minus(_ContractKey);
            int i = 0;
            foreach (DataRow nRow in zTable.Rows)
            {
                GV_Fee_Minus.Rows.Add();
                DataGridViewRow nRowView = GV_Fee_Minus.Rows[i];
                nRowView.Tag = nRow["AutoKey"].ToString().Trim();
                nRowView.Cells["NO"].Value = (i + 1).ToString();
                nRowView.Cells["cboCategory"].Value = nRow["CategoryKey"].ToInt();
                nRowView.Cells["PercentFee"].Value = nRow["PercentFee"].ToString().Trim();
                if (nRow["FromDate"] == DBNull.Value)
                {
                    nRowView.Cells["FromDate"].Value = "";

                }
                else
                {
                    DateTime zFromdate = DateTime.Parse(nRow["FromDate"].ToString());
                    nRowView.Cells["FromDate"].Value = zFromdate.ToString("dd/MM/yyyy");
                }
                if (nRow["ToDate"] == DBNull.Value)
                {
                    nRowView.Cells["ToDate"].Value = "";

                }
                else
                {
                    DateTime zToDate = DateTime.Parse(nRow["ToDate"].ToString());
                    nRowView.Cells["ToDate"].Value = zToDate.ToString("dd/MM/yyyy");
                }
                double zAmount = double.Parse(nRow["Amount"].ToString());
                nRowView.Cells["Amount"].Value = zAmount.ToString("#,###,##0");
                i++;
            }
        }
        private void Save_GV_Fee_Minus()
        {
            for (int i = 0; i < GV_Fee_Minus.Rows.Count - 1; i++)
            {
                if (GV_Fee_Minus.Rows[i].Cells["cboCategory"].Value != null)
                {
                    Contract_Fee_Info zContractKey = new Contract_Fee_Info();
                    zContractKey.ContractKey = _ContractKey;
                    if (GV_Fee_Minus.Rows[i].Tag != null)
                        zContractKey.AutoKey = int.Parse(GV_Fee_Minus.Rows[i].Tag.ToString());
                    else
                        zContractKey.AutoKey = 0;

                    zContractKey.CategoryKey = int.Parse(GV_Fee_Minus.Rows[i].Cells["cboCategory"].Value.ToString());

                    if (GV_Fee_Minus.Rows[i].Cells["Amount"].Value == null)
                        zContractKey.Amount = 0;
                    else
                        zContractKey.Amount = double.Parse(GV_Fee_Minus.Rows[i].Cells["Amount"].Value.ToString());
                    if (GV_Fee_Minus.Rows[i].Cells["PercentFee"].Value != null)
                        zContractKey.PercentFee = float.Parse(GV_Fee_Minus.Rows[i].Cells["PercentFee"].Value.ToString());
                    if (GV_Fee_Minus.Rows[i].Cells["FromDate"].Value == null || GV_Fee_Minus.Rows[i].Cells["FromDate"].Value.ToString() == "")
                    {
                        zContractKey.FromDate = DateTime.MinValue;
                    }
                    else
                    {
                        DateTime zFromDate = DateTime.MinValue;
                        if (DateTime.TryParse(GV_Fee_Minus.Rows[i].Cells["FromDate"].Value.ToString(), out zFromDate))
                        {
                            zContractKey.FromDate = DateTime.Parse(GV_Fee_Minus.Rows[i].Cells["FromDate"].Value.ToString());

                        }
                        else
                        {
                            zContractKey.ToDate = DateTime.MinValue;
                        }
                    }

                    if (GV_Fee_Minus.Rows[i].Cells["ToDate"].Value == null || GV_Fee_Minus.Rows[i].Cells["ToDate"].Value.ToString() == "")
                    {
                        zContractKey.ToDate = DateTime.MinValue;
                    }
                    else
                    {
                        DateTime zToDate = DateTime.MinValue;
                        if (DateTime.TryParse(GV_Fee_Minus.Rows[i].Cells["ToDate"].Value.ToString(), out zToDate))
                        {
                            zContractKey.ToDate = DateTime.Parse(GV_Fee_Minus.Rows[i].Cells["ToDate"].Value.ToString());

                        }
                        else
                        {
                            zContractKey.ToDate = DateTime.MinValue;
                        }
                    }
                    zContractKey.Save();
                }
            }
        }

        private void Load_GV_Chil()
        {
            GV_Chil.Rows.Clear();
            zTable = Contract_Fee_Data.DataTable_Chil(_ContractKey);
            int i = 0;
            foreach (DataRow nRow in zTable.Rows)
            {
                GV_Chil.Rows.Add();
                DataGridViewRow nRowView = GV_Chil.Rows[i];
                nRowView.Tag = nRow["AutoKey"].ToString().Trim();
                nRowView.Cells["NO"].Value = (i + 1).ToString();
                nRowView.Cells["cboCategory"].Value = nRow["CategoryKey"].ToInt();
                nRowView.Cells["PercentFee"].Value = nRow["PercentFee"].ToString().Trim();
                if (nRow["FromDate"] == DBNull.Value)
                {
                    nRowView.Cells["FromDate"].Value = "";

                }
                else
                {
                    DateTime zFromdate = DateTime.Parse(nRow["FromDate"].ToString());
                    nRowView.Cells["FromDate"].Value = zFromdate.ToString("dd/MM/yyyy");
                }
                if (nRow["ToDate"] == DBNull.Value)
                {
                    nRowView.Cells["ToDate"].Value = "";

                }
                else
                {
                    DateTime zToDate = DateTime.Parse(nRow["ToDate"].ToString());
                    nRowView.Cells["ToDate"].Value = zToDate.ToString("dd/MM/yyyy");
                }
                double zAmount = double.Parse(nRow["Amount"].ToString());
                nRowView.Cells["Amount"].Value = zAmount.ToString("#,###,###");
                i++;
            }
        }
        private void Save_GV_Chil()
        {
            for (int i = 0; i < GV_Chil.Rows.Count - 1; i++)
            {
                if (GV_Chil.Rows[i].Cells["cboCategory"].Value != null)
                {
                    Contract_Fee_Info zContractKey = new Contract_Fee_Info();
                    zContractKey.ContractKey = _ContractKey;
                    if (GV_Chil.Rows[i].Tag != null)
                        zContractKey.AutoKey = int.Parse(GV_Chil.Rows[i].Tag.ToString());
                    else
                        zContractKey.AutoKey = 0;

                    zContractKey.CategoryKey = int.Parse(GV_Chil.Rows[i].Cells["cboCategory"].Value.ToString());

                    if (GV_Chil.Rows[i].Cells["Amount"].Value != null)
                        zContractKey.Amount = double.Parse(GV_Chil.Rows[i].Cells["Amount"].Value.ToString());
                    else
                        zContractKey.Amount = 0;
                    if (GV_Chil.Rows[i].Cells["PercentFee"].Value != null)
                        zContractKey.PercentFee = float.Parse(GV_Chil.Rows[i].Cells["PercentFee"].Value.ToString());
                    if (GV_Chil.Rows[i].Cells["FromDate"].Value == null || GV_Chil.Rows[i].Cells["FromDate"].Value.ToString() == "")
                    {
                        zContractKey.FromDate = DateTime.MinValue;
                    }
                    else
                    {
                        DateTime zFromDate = DateTime.MinValue;
                        if (DateTime.TryParse(GV_Chil.Rows[i].Cells["FromDate"].Value.ToString(), out zFromDate))
                        {
                            zContractKey.FromDate = DateTime.Parse(GV_Chil.Rows[i].Cells["FromDate"].Value.ToString());

                        }
                        else
                        {
                            zContractKey.ToDate = DateTime.MinValue;
                        }
                    }

                    if (GV_Chil.Rows[i].Cells["ToDate"].Value == null || GV_Chil.Rows[i].Cells["ToDate"].Value.ToString() == "")
                    {
                        zContractKey.ToDate = DateTime.MinValue;
                    }
                    else
                    {
                        DateTime zToDate = DateTime.MinValue;
                        if (DateTime.TryParse(GV_Chil.Rows[i].Cells["ToDate"].Value.ToString(), out zToDate))
                        {
                            zContractKey.ToDate = DateTime.Parse(GV_Chil.Rows[i].Cells["ToDate"].Value.ToString());

                        }
                        else
                        {
                            zContractKey.ToDate = DateTime.MinValue;
                        }
                    }
                    zContractKey.Save();
                }
            }
        }

        private void cbo_CategoryKey_SelectedValueChanged(object sender, EventArgs e)
        {
            if (cbo_CategoryKey.SelectedValue.ToString() != "5")
            {
                lbl_Probation.Visible = false;
                grp_Probation.Visible = false;
                lbl_signday.Visible = true;
                dte_Signday.Visible = true;
                lbl_FinishDay.Visible = true;
                dte_FinishDay.Visible = true;
                DateTime zSignday = DateTime.Now;
                dte_Signday.Value = zSignday;
            }
            else
            {
                lbl_Probation.Visible = true;
                grp_Probation.Visible = true;
                lbl_signday.Visible = false;
                dte_Signday.Visible = false;
                lbl_FinishDay.Visible = false;
                dte_FinishDay.Visible = false;
            }
            if (cbo_CategoryKey.SelectedValue.ToString() == "4")
            {
                lbl_FinishDay.Visible = false;
                dte_FinishDay.Visible = false;
                lbl_Today.Visible = false;
                dte_Todate.Visible = false;
                lbl_Fromday.Visible = false;
                dte_Fromdate.Visible = false;
            }
            else if (cbo_CategoryKey.SelectedValue.ToString() != "4")
            {
                lbl_FinishDay.Visible = true;
                dte_FinishDay.Visible = true;
                lbl_Today.Visible = true;
                dte_Todate.Visible = true;
                lbl_Fromday.Visible = true;
                dte_Fromdate.Visible = true;
                lbl_FinishDay.Visible = false;
                dte_FinishDay.Visible = false;

            }
            if (cbo_CategoryKey.SelectedValue.ToString() == "1")
            {
                lbl_signday.Visible = true;
                dte_Signday.Visible = true;
                lbl_FinishDay.Visible = true;
                dte_FinishDay.Visible = true;
                DateTime zFinish = DateTime.Now;
                DateTime zToDate = zFinish.AddMonths(6).AddDays(-1);
                dte_FinishDay.Value = zToDate;
            }
            if (cbo_CategoryKey.SelectedValue.ToString() == "2")
            {
                lbl_signday.Visible = true;
                dte_Signday.Visible = true;
                lbl_FinishDay.Visible = true;
                dte_FinishDay.Visible = true;
                DateTime zFinish = DateTime.Now;
                DateTime zToDate = zFinish.AddYears(1).AddDays(-1);
                dte_FinishDay.Value = zToDate;
            }
            if (cbo_CategoryKey.SelectedValue.ToString() == "3")
            {
                lbl_signday.Visible = true;
                dte_Signday.Visible = true;
                lbl_FinishDay.Visible = true;
                dte_FinishDay.Visible = true;
                DateTime zFinish = DateTime.Now;
                DateTime zToDate = zFinish.AddYears(3).AddDays(-1);
                dte_FinishDay.Value = zToDate;
            }
        }
        private void cbo_Shearch_Branch_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbo_Shearch_Branch.SelectedIndex != 0)
            {
                LoadDataToToolbox.ComboBoxData(cbo_Teams, "SELECT TeamKey, TeamName FROM SYS_Team WHERE BranchKey = '" + cbo_Shearch_Branch.SelectedIndex + "' AND RecordStatus< 99", "---- Choose Team ----");
            }
        }

        private void Load_Data()
        {
            #region[Employee]
            _Emp_Obj = new Employee_Object(zKey);
            txt_EmployeeNameB.Text = _Emp_Obj.PersoObject.FullName;
            txt_IssueIDB.Text = _Emp_Obj.PersoObject.IssueID;
            txt_IssuedPlaceB.Text = _Emp_Obj.PersoObject.IssuePlace;
            txt_PositionA.Text = _Emp_Obj.PersoObject.IssuePlace;
            txt_HomeAddressB.Text = _Emp_Obj.PersoObject.HomeTown;
            if (_Emp_Obj.PersoObject.Gender == 1)
            {
                rdo_MaleB.Checked = true;
            }
            else if (_Emp_Obj.PersoObject.Gender == 0)
            {
                rdo_FemaleB.Checked = true;
            }
            if (_Emp_Obj.PersoObject.IssueDate != DateTime.MinValue)
            {
                dte_IssueDateB.Value = _Emp_Obj.PersoObject.IssueDate;
            }
            if (_Emp_Obj.PersoObject.BirthDay != DateTime.MinValue)
            {
                dte_BirthdayB.Value = _Emp_Obj.PersoObject.BirthDay;
            }
            txt_PositionA.Text = "Giám Đốc";
            txt_EmployeeNameA.Text = "Nguyễn Văn A";
            if (_Emp_Obj.BranchKey != 0)
                cbo_BranchContract.SelectedValue = _Emp_Obj.BranchKey;
            if (_Emp_Obj.DepartmentKey != 0)
                cbo_DepartmentContract.SelectedValue = _Emp_Obj.DepartmentKey;
            if (_Emp_Obj.TeamKey != 0)
                cbo_TeamContract.SelectedValue = _Emp_Obj.TeamKey;
            if (_Emp_Obj.PositionKey != 0)
                cbo_PositionContract.SelectedValue = _Emp_Obj.PositionKey;
            Load_GV_Basic_Salary();
            #endregion

            #region[Contract]
            Contract_Info zContract = new Contract_Info(_PersonalKey, _ContractKey);
            if (zContract.Key != null)
            {
                _ContractKey = zContract.Key;
            }
            txt_ContractID.Text = zContract.ContractID;
            if (zContract.SignDay != DateTime.MinValue)
            {
                dte_Signday.Value = zContract.SignDay;
            }
            if (zContract.FinishDay != DateTime.MinValue)
            {
                dte_FinishDay.Value = zContract.FinishDay;
            }
            txt_WorkingDescription.Text = zContract.Description;
            if (zContract.CategoryKey != 0)
                cbo_CategoryKey.SelectedValue = zContract.CategoryKey;
            if (zContract.FromDate != DateTime.MinValue)
            {
                dte_Fromdate.Value = zContract.FromDate;
            }
            if (zContract.ToDate != DateTime.MinValue)
            {
                dte_Todate.Value = zContract.ToDate;
            }
            if (zContract.Slug == 0)
            {
                rdo_First.Checked = true;
            }
            else if (zContract.Slug == 1)
            {
                rdo_Late.Checked = true;
            }
            if (zContract.RecordStatus == 62)
            {
                btn_Save_New.Visible = true;
                btn_Add.Visible = false;
                btn_Finish.Visible = false;
                btn_New.Visible = false;
            }
            else
            {
                btn_Save_New.Visible = false;
                btn_Add.Visible = true;
                btn_Finish.Visible = true;
                btn_New.Visible = true;
            }
            Load_GV_Fee_Plus();
            Load_GV_Fee_Minus();
            Load_GV_Chil();
            #endregion
            txt_ContractID.Focus();
        }
        private void Save_Data()
        {
            _Emp_Obj = new Employee_Object();
            _Emp_Obj.Key = zKey;
            Contract_Info zContract = new Contract_Info(_PersonalKey, _ContractKey);
            _ContractKey = zContract.Key;
            zContract.PersonalKey = _PersonalKey;
            zContract.ContractID = txt_ContractID.Text.ToString();
            zContract.SignDay = dte_Signday.Value;
            zContract.FinishDay = dte_FinishDay.Value;
            zContract.FromDate = dte_Fromdate.Value;
            zContract.ToDate = dte_Todate.Value;
            if (rdo_First.Checked == true)
            {
                zContract.Slug = 0;
            }
            else if (rdo_Late.Checked == true)
            {
                zContract.Slug = 1;
            }
            if (cbo_CategoryKey.SelectedValue != null)
            {
                zContract.CategoryKey = int.Parse(cbo_CategoryKey.SelectedValue.ToString());
            }
            zContract.Description = txt_WorkingDescription.Text.ToString();
            zContract.ParentKey = "";
            zContract.Organization = "";
            zContract.ReportTo = "";
            zContract.Save();
            _ContractKey = zContract.Key;
            Save_GV_Basic_Salary();
            Save_GV_Fee_Plus();
            Save_GV_Fee_Minus();
            Save_GV_Chil();
            string zMessage = TN_Message.Show(_Emp_Obj.Message);
            if (zMessage.Length == 0)
            {
                MessageBox.Show("Câp nhật thành công", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                _ContractKey = zCopy;
            }
            else
            {
                MessageBox.Show("Vui lòng kiểm tra lại", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Load_New_Data()
        {
            #region [Employee Object] 
            _Emp_Obj = new Employee_Object(zKey);
            txt_EmployeeNameB.Text = _Emp_Obj.PersoObject.FullName;
            txt_IssueIDB.Text = _Emp_Obj.PersoObject.IssueID;
            txt_IssuedPlaceB.Text = _Emp_Obj.PersoObject.IssuePlace;
            txt_PositionA.Text = _Emp_Obj.PersoObject.IssuePlace;
            txt_HomeAddressB.Text = _Emp_Obj.PersoObject.HomeTown;
            if (_Emp_Obj.PersoObject.Gender == 0)
            {
                rdo_MaleB.Checked = true;
            }
            else if (_Emp_Obj.PersoObject.Gender == 1)
            {
                rdo_FemaleB.Checked = true;
            }
            if (_Emp_Obj.PersoObject.IssueDate != DateTime.MinValue)
            {
                dte_IssueDateB.Value = _Emp_Obj.PersoObject.IssueDate;
            }
            if (_Emp_Obj.PersoObject.BirthDay != DateTime.MinValue)
            {
                dte_BirthdayB.Value = _Emp_Obj.PersoObject.BirthDay;
            }
            txt_PositionA.Text = "Giám Đốc";
            txt_EmployeeNameA.Text = "Nguyễn Văn A";
            if (_Emp_Obj.BranchKey != 0)
                cbo_BranchContract.SelectedValue = _Emp_Obj.BranchKey;
            if (_Emp_Obj.DepartmentKey != 0)
                cbo_DepartmentContract.SelectedValue = _Emp_Obj.DepartmentKey;
            if (_Emp_Obj.TeamKey != 0)
                cbo_TeamContract.SelectedValue = _Emp_Obj.TeamKey;
            if (_Emp_Obj.PositionKey != 0)
                cbo_PositionContract.SelectedValue = _Emp_Obj.PositionKey;
            Load_GV_Basic_Salary();
            #endregion

            #region [Contract]
            Contract_Info zContract = new Contract_Info(_PersonalKey, _ContractKey);

            if (zContract.Key != null)
            {
                _ContractKey = zContract.Key;
            }
            txt_ContractID.Text = zContract.ContractID;
            if (zContract.SignDay != DateTime.MinValue)
            {
                dte_Signday.Value = zContract.SignDay;
            }
            if (zContract.FinishDay != DateTime.MinValue)
            {
                dte_FinishDay.Value = zContract.FinishDay;
            }
            txt_WorkingDescription.Text = zContract.Description;
            //txt_BasicSalary.Text = zContract.BasicSalary.ToString("###,###,###");
            if (zContract.CategoryKey != 0)
                cbo_CategoryKey.SelectedValue = zContract.CategoryKey;
            if (zContract.FromDate != DateTime.MinValue)
            {
                dte_Fromdate.Value = zContract.FromDate;
            }
            if (zContract.ToDate != DateTime.MinValue)
            {
                dte_Todate.Value = zContract.ToDate;
            }
            if (zContract.Slug == 0)
            {
                rdo_First.Checked = true;
            }
            else if (zContract.Slug == 1)
            {
                rdo_Late.Checked = true;
            }


            Load_GV_Fee_Plus();
            Load_GV_Fee_Minus();
            Load_GV_Chil();
            #endregion
        }
        private void Save_New_Data()
        {
            _Emp_Obj = new Employee_Object();
            _Emp_Obj.Key = zKey;
            Contract_Info zContract = new Contract_Info(_PersonalKey);
            zContract.Key = zCopy;
            zContract.PersonalKey = _PersonalKey;
            zContract.ContractID = txt_ContractID.Text.ToString();
            zContract.SignDay = dte_Signday.Value;
            zContract.FinishDay = dte_FinishDay.Value;
            zContract.FromDate = dte_Fromdate.Value;
            zContract.ToDate = dte_Todate.Value;
            if (rdo_First.Checked == true)
            {
                zContract.Slug = 0;
            }
            else if (rdo_Late.Checked == true)
            {
                zContract.Slug = 1;
            }
            if (cbo_CategoryKey.SelectedValue != null)
            {
                zContract.CategoryKey = int.Parse(cbo_CategoryKey.SelectedValue.ToString());
            }
            zContract.Description = txt_WorkingDescription.Text.ToString();
            zContract.RecordStatus = 0;
            zContract.ParentKey = "";
            zContract.Organization = "";
            zContract.ReportTo = "";
            zContract.Save();
            zCopy = zContract.Key;
            Save_GV_Basic_Salary();
            Save_GV_Fee_Plus();
            Save_GV_Fee_Minus();
            Save_GV_Chil();
            string zMessage = TN_Message.Show(_Emp_Obj.Message);
            if (zMessage.Length == 0)
            {
                MessageBox.Show("Câp nhật thành công", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Vui lòng kiểm tra lại", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion
        #region-----Button-----
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            LV_LoadData_Dif();
        }
        private void Btn_Add_Click(object sender, EventArgs e)
        {
            if (txt_ContractID.Text.Trim().Length != 0)
            {
                Save_Data();
            }
            else
            {
                MessageBox.Show("Vui Lòng nhập đầy đủ thông tin", "Thông Báo");
            }
            LV_LoadData_Dif();
            LV_LoadData_List();
        }
        private void Btn_New_Click(object sender, EventArgs e)
        {
            zKey = "";
            _ContractKey = "";
            Load_Data();
        }
        private void Btn_Finish_Click(object sender, EventArgs e)
        {
            DialogResult dlr = MessageBox.Show("Bạn có chắc muốn kết thúc hợp đồng ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dlr == DialogResult.Yes)
            {

                Contract_Info zContarct = new Contract_Info();
                zContarct.RecordStatus = 62;
                zContarct.Update(_ContractKey);
                Load_Data();
                MessageBox.Show("Thành Công", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                LV_LoadData_Dif();
                LV_LoadData_List();
            }
        }
        private void Btn_Save_New_Click(object sender, EventArgs e)
        {
            zCopy = "";
            Save_New_Data();
            zTable = Contract_Fee_Data.DataTable_Fee(_ContractKey);
            foreach (DataRow nRow in zTable.Rows)
            {
                Contract_Fee_Info zFee = new Contract_Fee_Info();
                zFee.AutoKey = 0;
                zFee.ContractKey = zCopy;
                zFee.CategoryKey = int.Parse(nRow["CategoryKey"].ToString().Trim());
                zFee.Amount = double.Parse(nRow["Amount"].ToString().Trim());
                zFee.PercentFee = float.Parse(nRow["PercentFee"].ToString().Trim());
                zFee.FromDate = DateTime.Parse(nRow["FromDate"].ToString().Trim());
                zFee.ToDate = DateTime.Parse(nRow["ToDate"].ToString().Trim());
                zFee.Create();
            }
            LV_LoadData_Dif();
            LV_LoadData_List();

        }
        #endregion

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        private void btnHide_Click(object sender, EventArgs e)
        {
            btnShow.Visible = true;
            btnHide.Visible = false;
            Panel_Search.Visible = false;
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            btnShow.Visible = false;
            btnHide.Visible = true;
            Panel_Search.Visible = true;
        }
    }
}
