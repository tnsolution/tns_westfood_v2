﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using TNS.HRM;
using TNS.LOC;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Employee : Form
    {
        private string _ReportTo = "";
        private string _Category = "";
        private string _PersonalKey = "";
        private string zKey = "";
        private int _Ward = 0;
        private int _Province = 0;
        private int _Country = 0;
        private int _District = 0;
        private Employee_Object _Emp_Obj;
        private List<Address_Info> zListAdress;

        public Frm_Employee()
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            Utils.DoubleBuffered(GVData, true);
            Utils.DrawGVStyle(ref GVData);
            Utils.DrawLVStyle(ref LVData);

            //----ListView
            InitLayout_LV(LVData);
            LV_Contract_Layout(LV_Contract);
            //---- DataGridView_Address
            GVData_Layout();
            GVData.EditingControlShowing += GVData_EditingControlShowing;
            GVData.CellEndEdit += GVData_CellEndEdit;
            GVData.DataError += GVData_DataError;
            //---- Button
            btn_Deny.Click += Btn_Deny_Click;
            txt_ReportyTo.Leave += Txt_ReportyTo_Leave;
            btn_Add.Click += Btn_Add_Click;
            btn_New.Click += btn_New_Click;
            //grp_Edit.Visible = false;
            GVData.KeyDown += GVData_KeyDown;
            btn_CheckCardID.Click += Btn_CheckCardID_Click;
            btn_CheckIssuedID.Click += Btn_CheckIssuedID_Click;
            txt_IssueID.KeyDown += Txt_IssueID_KeyDown;
            btn_Import.Click += Btn_Import_Click;
            btn_Search.Click += Btn_Search_Click;
            cbo_Branch_Search.SelectedIndexChanged += Cbo_Branch_Search_SelectedIndexChanged;
            cbo_Deparment_Search.SelectedIndexChanged += Cbo_Deparment_Search_SelectedIndexChanged;
            LVData.DoubleClick += LVData_DoubleClick;
            btn_IP_Leaving.Click += Btn_IP_Leaving_Click;

            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;
            Utils.SizeLastColumn_LV(LVData);
        }

      

        private void LVData_DoubleClick(object sender, EventArgs e)
        {
            Frm_HumanInfo frm = new Frm_HumanInfo();
            frm.ReportDate = DateTime.Now;
            frm.EmployeeKey= LVData.SelectedItems[0].Tag.ToString();
            frm.Show();
        }

        #region[Event Search]

        private void Cbo_Branch_Search_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDataToToolbox.ComboBoxData(cbo_Deparment_Search, "SELECT DepartmentKey, DepartmentName FROM[dbo].[SYS_Department] WHERE BranchKey = "+cbo_Branch_Search.SelectedValue.ToInt()+"  AND RecordStatus< 99", "---- Chọn tất cả----");

        }
        private void Cbo_Deparment_Search_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDataToToolbox.ComboBoxData(cbo_Team_Search, "SELECT TeamKey,TeamName FROM SYS_Team WHERE TeamKey != 98 AND DepartmentKey = "+ cbo_Deparment_Search.SelectedValue.ToInt() + "  AND RecordStatus < 99", "---- Chọn tất cả ----");
        }
        #endregion
        private void Btn_Import_Click(object sender, EventArgs e)
        {
            Frm_Import_Employee_V2 frm = new Frm_Import_Employee_V2();
            frm.Show();
        }

        private void Frm_Employees_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            Check_RoleForm();

            dte_BirthDay.Value = SessionUser.Date_Work;
            dte_IssueDate.Value = SessionUser.Date_Work;
            LoadBox();
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            
        }
        private void LoadBox()
        {
            LoadDataToToolbox.ComboBoxData(cbo_Department, "SELECT DepartmentKey, DepartmentName FROM[dbo].[SYS_Department] WHERE DepartmentKey != 98  AND RecordStatus< 99 ORDER BY RANK", "---- Chọn----");
            LoadDataToToolbox.ComboBoxData(cbo_Branch, "SELECT BranchKey,BranchName FROM SYS_Branch WHERE BranchKey != 98  AND RecordStatus < 99 ORDER BY RANK", "---- Chọn----");
            LoadDataToToolbox.ComboBoxData(cbo_Team, "SELECT TeamKey,TeamName FROM SYS_Team WHERE TeamKey != 98  AND RecordStatus < 99", "---- Chọn ----");
            LoadDataToToolbox.ComboBoxData(cbo_Position, "SELECT PositionKey,PositionName FROM SYS_Position WHERE PositionKey != 98  AND RecordStatus < 99 ORDER BY PositionName", "---- Chọn----");
            LoadDataToToolbox.ComboBoxData(cbo_WorkingStatus, "SELECT WorkingKey,WorkingName FROM HRM_WorkingStatus  WHERE  RecordStatus < 99", "---- Chọn ----");

            LoadDataToToolbox.ComboBoxData(cbo_Branch_Search, "SELECT BranchKey,BranchName FROM SYS_Branch WHERE BranchKey != 98  AND RecordStatus < 99 ORDER BY RANK", "---- Chọn tất cả ----");
            LoadDataToToolbox.ComboBoxData(cbo_Position_Search, "SELECT PositionKey,PositionName FROM SYS_Position WHERE RecordStatus < 99 ORDER BY PositionName", "---- Chọn tất cả ----");
            LoadDataToToolbox.ComboBoxData(cbo_Status_Search, "SELECT WorkingKey,WorkingName FROM HRM_WorkingStatus  WHERE  RecordStatus < 99", "---- Chọn tất cả ----");
        }

        #region ----- Left -----
        #region ----- Desgin Layout ListView -----
        private void InitLayout_LV(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Nhân Viên";
            colHead.Width = 180;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số thẻ";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "CMND";
            colHead.Width = 130;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Bộ Phận";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tổ Nhóm";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tình Trạng";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);
        }
        private void InitData_LV()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LVData;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            int _Branch = 0;
            int _Department = 0;
            int _Team = 0;
            string Name = "";
            string IssueID = "";
            string EmployeeID = "";
            DataTable In_Table = new DataTable();

            if (cbo_Branch_Search.SelectedValue != null)
            {
                _Branch = cbo_Branch_Search.SelectedValue.ToInt();
            }
            else
            {
                _Branch = 0;
            }
            if (cbo_Deparment_Search.SelectedValue != null)
            {
                _Department = cbo_Deparment_Search.SelectedValue.ToInt();
            }
            else
            {
                _Department = 0;
            }
            if (cbo_Team_Search.SelectedValue != null)
            {
                _Team = int.Parse(cbo_Team_Search.SelectedValue.ToString());
            }
            else
            {
                _Team = 0;
            }
            if (txt_EmployeeName_Search.Text.Trim() != null)
            {
                Name = txt_EmployeeName_Search.Text.Trim();
            }
            if (txt_EmployeeID_Search.Text.Trim() != string.Empty)
            {
                EmployeeID = txt_EmployeeID_Search.Text.Trim();
            }
            if (txt_IssueID_Search.Text.Trim() != string.Empty)
            {
                IssueID = txt_IssueID_Search.Text.Trim();
            }

            In_Table = Employee_Data.Search(_Branch, _Department, _Team, Name,
                EmployeeID, IssueID,cbo_Position_Search.SelectedValue.ToInt(),cbo_Status_Search.SelectedValue.ToInt());
            LV.Items.Clear();

            // Thêm 1 cột để sắp xếp
            In_Table.Columns.Add("Rank");
            for (int i = 0; i < In_Table.Rows.Count; i++)
            {
                DataRow row = In_Table.Rows[i];
                string ID = row[2].ToString();
                In_Table.Rows[i]["Rank"] = ConvertIDRank(ID);
            }
            DataView dv = In_Table.DefaultView;
            dv.Sort = " Rank ASC";
            In_Table = dv.ToTable();
            In_Table.Columns.Remove("Rank");
            //--sắp xếp xong xóa cột sắp xếp đi

            int n = In_Table.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                DataRow nRow = In_Table.Rows[i];
                lvi.Tag = nRow["EmployeeKey"];
                lvi.ForeColor = Color.Navy;

                lvi.BackColor = Color.White;
                lvi.ImageIndex = 0;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["FullName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["CardID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["IssueID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["DepartmentID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["TeamID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["WorkingName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["PersonalKey"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }

            this.Cursor = Cursors.Default;
        }
        #endregion

        #region ----- Process Event -----
        private void LV_Employee_ItemActivate(object sender, EventArgs e)
        {
            zKey = LVData.SelectedItems[0].Tag.ToString();
            Load_Data();
        }
        private void LV_Employee_Click(object sender, EventArgs e)
        {
            ////LV_Employee.Invoke(new Action(() =>
            ////{
            //for (int i = 0; i < LV_Employee.Items.Count; i++)
            //{
            //    if (LV_Employee.Items[i].Selected == true)
            //    {
            //        LV_Employee.Items[i].BackColor = Color.LightBlue;
            //    }
            //    else
            //    {
            //        LV_Employee.Items[i].BackColor = SystemColors.Window;
            //    }
            //}
            ////}));

            zKey = LVData.SelectedItems[0].Tag.ToString();
            _PersonalKey = LVData.SelectedItems[0].SubItems[7].Text;
            LV_Contract_LoadData();
            Load_Data();
            LoadDataToToolbox.AutoCompleteTextBox(txt_ReportyTo, Employee_Data.List_ReportTo("FullName", zKey));
        }
        #endregion
        #endregion

        #region ----- Right ------
        #region ----- Desgin Layout DataGridView And Process Event -----
        private void GVData_Layout()
        {
            GVData.Columns.Add("No", "STT");
            GVData.Columns.Add("Country", "Quốc Gia");
            GVData.Columns.Add("Province", "Tỉnh/Thành");
            GVData.Columns.Add("District", "Quận/Huyện");
            GVData.Columns.Add("Ward", "Phường/Xã");
            GVData.Columns.Add("Address", "Địa chỉ");
            DataGridViewComboBoxColumn combox = new DataGridViewComboBoxColumn();
            combox.HeaderText = "Loại";
            combox.Name = "Category";
            combox.Items.Add("Thường trú");
            combox.Items.Add("Tạm trú");
            GVData.Columns.Add(combox);

            GVData.Columns["No"].Width = 40;
            GVData.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GVData.Columns["No"].ReadOnly = true;

            GVData.Columns["Country"].Width = 80;
            GVData.Columns["Country"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;


            GVData.Columns["Province"].Width = 120;
            GVData.Columns["Province"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GVData.Columns["District"].Width = 120;
            GVData.Columns["District"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GVData.Columns["Ward"].Width = 120;
            GVData.Columns["Ward"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GVData.Columns["Address"].Width = 185;
            GVData.Columns["Address"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GVData.Columns["Category"].Width = 90;
            GVData.Columns["Category"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        }
        private void GVData_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow zRowEdit = GVData.Rows[e.RowIndex];
            Address_Info zAddress;
            if (GVData.Rows[e.RowIndex].Tag == null)
            {
                zAddress = new Address_Info();
                GVData.Rows[e.RowIndex].Tag = zAddress;
            }
            else
            {
                zAddress = (Address_Info)GVData.Rows[e.RowIndex].Tag;
            }
            bool zIsChangeValued = false;
            switch (e.ColumnIndex)
            {
                case 1:
                    if (GVData.CurrentCell.Value == null)
                    {
                        zAddress.CountryName = "";
                        zAddress.CountryKey = 0;
                        zIsChangeValued = true;
                    }
                    else
                    {
                        if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() != zAddress.CountryName.ToString())
                        {
                            string Name = GVData.CurrentCell.Value.ToString();
                            Country_Info zCountry = new Country_Info();
                            zCountry.Get_Country_Info(Name);
                            _Country = zCountry.Key;
                            zAddress.CountryKey = zCountry.Key;
                            zAddress.CountryName = zCountry.CountryName;
                            zIsChangeValued = true;
                        }
                    }
                    break;
                case 2:
                    if (GVData.CurrentCell.Value == null)
                    {
                        zAddress.ProvinceName = "";
                        zAddress.CountryKey = 0;
                        zIsChangeValued = true;
                    }
                    else
                    {
                        if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() != zAddress.ProvinceName.ToString())
                        {
                            string Name = GVData.CurrentCell.Value.ToString();
                            Province_Info zProvince = new Province_Info();
                            zProvince.Get_Province_Info(Name);
                            _Province = zProvince.Key;
                            zAddress.ProvinceKey = zProvince.Key;
                            zAddress.ProvinceName = zProvince.ProvinceName;
                            zIsChangeValued = true;
                        }
                    }
                    break;
                case 3:
                    if (GVData.CurrentCell.Value == null)
                    {
                        zAddress.DistrictName = "";
                        zAddress.DistrictKey = 0;
                        zIsChangeValued = true;
                    }
                    else
                    {
                        if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() != zAddress.DistrictName.ToString())
                        {
                            string Name = GVData.CurrentCell.Value.ToString();
                            District_Info zDistrict = new District_Info();
                            zDistrict.Get_District_Info(Name, zAddress.ProvinceKey);
                            _District = zDistrict.Key;
                            zAddress.DistrictName = zDistrict.DistrictName;
                            zAddress.DistrictKey = zDistrict.Key;
                            zIsChangeValued = true;
                        }
                    }
                    break;
                case 4:
                    if (GVData.CurrentCell.Value == null)
                    {
                        zAddress.WardName = "";
                        zAddress.WardKey = 0;
                        zIsChangeValued = true;
                    }
                    else
                    {
                        if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() != zAddress.WardName.ToString())
                        {
                            string Name = GVData.CurrentCell.Value.ToString();
                            Ward_Info zWard = new Ward_Info();
                            zWard.Get_Ward_Info(Name, zAddress.DistrictKey);
                            _Ward = zWard.Key;
                            zAddress.WardName = zWard.WardName;
                            zAddress.WardKey = zWard.Key;
                            zIsChangeValued = true;
                        }
                    }
                    break;
                case 5:
                    if (GVData.CurrentCell.Value == null)
                    {
                        zAddress.Address = "";
                        zIsChangeValued = true;
                    }
                    else
                    {
                        if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() != zAddress.Address.ToString())
                        {
                            zAddress.Address = GVData.CurrentCell.Value.ToString();
                            zIsChangeValued = true;
                        }
                    }
                    break;
                case 6:
                    if (GVData.CurrentCell.Value == null)
                    {
                        zAddress.CategoryKey = 0;
                        zIsChangeValued = true;
                    }
                    else
                    {

                        _Category = GVData.CurrentCell.Value.ToString();
                        zIsChangeValued = true;


                    }
                    break;
            }
            if (zIsChangeValued)
            {
                zAddress.RecordStatus = 1;
                Caculate(e.RowIndex);
                ShowProductInGridView_Address(e.RowIndex);
            }
            if (zAddress.Key != 0)
                zAddress.RecordStatus = 2;
            else
                zAddress.RecordStatus = 1;

        }
        private void GVData_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            switch (GVData.CurrentCell.ColumnIndex)
            {
                case 1: // autocomplete for product
                    DataGridViewTextBoxEditingControl te = (DataGridViewTextBoxEditingControl)e.Control;
                    te.AutoCompleteMode = AutoCompleteMode.None;
                    LoadDataToToolbox.AutoCompleteTextBox(te, "SELECT CountryName FROM LOC_Country WHERE CountryKey = 237 AND RecordStatus <99");
                    e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);
                    break;
                case 2: // autocomplete for product
                    te = (DataGridViewTextBoxEditingControl)e.Control;
                    te.AutoCompleteMode = AutoCompleteMode.None;
                    LoadDataToToolbox.AutoCompleteTextBox(te, "SELECT ProvinceName FROM LOC_Province WHERE CountryKey = '" + _Country + "' AND RecordStatus <99");
                    e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);
                    break;
                case 3:
                    te = (DataGridViewTextBoxEditingControl)e.Control;
                    te.AutoCompleteMode = AutoCompleteMode.None;
                    LoadDataToToolbox.AutoCompleteTextBox(te, "SELECT DistrictName FROM LOC_District WHERE ProvinceKey = '" + _Province + "' AND RecordStatus <99");

                    e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);
                    break;
                case 4:
                    te = (DataGridViewTextBoxEditingControl)e.Control;
                    te.AutoCompleteMode = AutoCompleteMode.None;
                    LoadDataToToolbox.AutoCompleteTextBox(te, "SELECT WardName FROM LOC_Ward WHERE DistrictKey = '" + _District + "' AND RecordStatus <99");
                    e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);
                    break;
            }
        }
        private void Control_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = false;
        }
        private void ShowProductInGridView_Address(int RowIndex)
        {
            DataGridViewRow zRowView = GVData.Rows[RowIndex];
            Address_Info zAddress = (Address_Info)zRowView.Tag;

            zRowView.Cells[1].ErrorText = "";
            zRowView.Cells[2].ErrorText = "";
            zRowView.Cells[3].ErrorText = "";
            zRowView.Cells[4].ErrorText = "";
            zRowView.Cells[5].ErrorText = "";
            _Country = zAddress.CountryKey;
            _Province = zAddress.ProvinceKey;
            _District = zAddress.DistrictKey;
            _Ward = zAddress.WardKey;

            zRowView.Cells["No"].Value = (RowIndex + 1).ToString();
            if (_Country == 0)
            {
                zRowView.Cells[1].Tag = _Country;
                zRowView.Cells[1].ErrorText = "Không tìm thấy quốc gia này!.";
            }
            else
            {
                zRowView.Cells[1].Tag = _Country;
                zRowView.Cells[1].Value = zAddress.CountryName;
            }
            if (_Province == 0)
            {
                zRowView.Cells[2].Tag = _Province;
                zRowView.Cells[2].ErrorText = "Không tìm thấy tỉnh này!.";
            }
            else
            {
                zRowView.Cells[2].Tag = _Province;
                zRowView.Cells[2].Value = zAddress.ProvinceName;
            }
            if (_District == 0)
            {
                zRowView.Cells[3].Tag = _District;
                zRowView.Cells[3].ErrorText = "Không tìm thấy quận/huyện này!.";
                zRowView.Cells[3].Value = "";
            }
            else
            {
                zRowView.Cells[3].Tag = _District;
                zRowView.Cells[3].Value = zAddress.DistrictName;
            }
            if (_Ward == 0)
            {
                zRowView.Cells[4].Tag = _Ward;
                zRowView.Cells[4].ErrorText = "Không có phường/xã này!.";
                zRowView.Cells[4].Value = "";
            }
            else
            {
                zRowView.Cells[4].Tag = _Ward;
                zRowView.Cells[4].Value = zAddress.WardName;
            }
            if (zAddress.Address == "")
            {
                zRowView.Cells[5].ErrorText = "Ðịa chỉ không được bỏ trống!.";
            }
            if (zAddress.Key == 0)
            {
                zAddress.CategoryKey = 1;
                zRowView.Cells[6].Value = "Thường trú";
            }

        }
        private void GVData_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.Cancel = true;
        }
        private void GVData_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                DialogResult dlr = MessageBox.Show("Bạn có xóa thông tin này ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlr == DialogResult.Yes)
                {
                    Address_Info zAddress = (Address_Info)GVData.CurrentRow.Tag;
                    GVData.CurrentRow.Visible = false;
                    zAddress.RecordStatus = 99;
                }
            }
        }
        private void Caculate(int RowIndex)
        {
            Address_Info zAddress = (Address_Info)GVData.Rows[RowIndex].Tag;
            District_Info zDistrict = new District_Info();
            zDistrict.Get_District_Info(zAddress.DistrictName, zAddress.ProvinceKey);
            if (zDistrict.Key == 0)
            {
                _District = zDistrict.Key;
                zAddress.DistrictKey = zDistrict.Key;
                zAddress.DistrictName = "";
            }
            Ward_Info zWard = new Ward_Info();
            zWard.Get_Ward_Info(zAddress.WardName, zAddress.DistrictKey);
            if (zWard.Key == 0)
            {
                _Ward = zWard.Key;
                zAddress.WardName = zWard.WardName;
                zAddress.WardKey = zWard.Key;
            }
            if (_Category == "Thường trú")
                zAddress.CategoryKey = 1;
            else if (_Category == "Tạm trú")
                zAddress.CategoryKey = 2;
            else
                zAddress.CategoryKey = 0;
        }
        #endregion

        #region ----- Process Data -----
        private void Load_GVData()
        {
            GVData.Rows.Clear();
            for (int i = 0; i < 15; i++)
            {
                GVData.Rows.Add();
            }
            for (int i = 0; i < _Emp_Obj.PersoObject.List_Address.Count; i++)
            {
                Address_Info zAddress = (Address_Info)_Emp_Obj.PersoObject.List_Address[i];
                GVData.Rows[i].Tag = zAddress;
                GVData.Rows[i].Cells["No"].Value = (i + 1).ToString();
                GVData.Rows[i].Cells["Country"].Value = zAddress.CountryName;
                GVData.Rows[i].Cells["Country"].Tag = zAddress.CountryKey;
                GVData.Rows[i].Cells["Province"].Value = zAddress.ProvinceName;
                GVData.Rows[i].Cells["Province"].Tag = zAddress.ProvinceKey;
                GVData.Rows[i].Cells["District"].Value = zAddress.DistrictName;
                GVData.Rows[i].Cells["District"].Tag = zAddress.DistrictKey;
                GVData.Rows[i].Cells["Ward"].Value = zAddress.WardName;
                GVData.Rows[i].Cells["Ward"].Tag = zAddress.WardKey;
                GVData.Rows[i].Cells["Address"].Value = zAddress.Address;
                if (zAddress.CategoryKey == 1)
                    GVData.Rows[i].Cells["Category"].Value = "Thường trú";
                else if (zAddress.CategoryKey == 2)
                    GVData.Rows[i].Cells["Category"].Value = "Tạm trú";
                else
                    GVData.Rows[i].Cells["Category"].Value = "";
            }
        }
       
        private void Load_Data()
        {
            txt_FullName.BackColor = Color.FromArgb(255, 255, 225);
            txt_IssuePlace.BackColor = Color.FromArgb(255, 255, 225);
            txt_HomeTown.BackColor = Color.FromArgb(255, 255, 225);
            txt_CardID.BackColor = Color.FromArgb(255, 255, 225);
            txt_IssueID.BackColor = Color.FromArgb(255, 255, 225);
            _Emp_Obj = new Employee_Object(zKey);
            _PersonalKey = _Emp_Obj.PersoObject.Key;
            txt_Description.Text = _Emp_Obj.Description;
            txt_CompanyPhone.Text = _Emp_Obj.CompanyPhone;
            txt_CompanyEmail.Text = _Emp_Obj.CompanyEmail;
            if (_Emp_Obj.BranchKey != 0)
            {
                cbo_Branch.SelectedValue = _Emp_Obj.BranchKey;
            }
            else
            {
                cbo_Branch.SelectedIndex = 0;
            }
            if (_Emp_Obj.DepartmentKey != 0)
                cbo_Department.SelectedValue = _Emp_Obj.DepartmentKey;
            else
            {
                cbo_Department.SelectedIndex = 0;
            }
            if (_Emp_Obj.TeamKey != 0)
                cbo_Team.SelectedValue = _Emp_Obj.TeamKey;
            else
                cbo_Team.SelectedIndex = 0;
            if (_Emp_Obj.PositionKey != 0)
                cbo_Position.SelectedValue = _Emp_Obj.PositionKey;
            if (_Emp_Obj.WorkingStatus != 0)
                cbo_WorkingStatus.SelectedValue = _Emp_Obj.WorkingStatus;
            else
                cbo_WorkingStatus.SelectedIndex = 1;
            if (_Emp_Obj.StartingDate != DateTime.MinValue)
            {
                dte_StartingDate.Value = _Emp_Obj.StartingDate;
            }
            else
                dte_StartingDate.Value = SessionUser.Date_Work;
            dte_LeavingDate.Value = _Emp_Obj.LeavingDate;
            if(dte_LeavingDate.Value!=DateTime.MinValue)
            {
                lbl_LeavingDate.ForeColor = Color.Red; 
            }
            else
            {
                lbl_LeavingDate.ForeColor = Color.Black;
            }
            txt_AccoutCode.Text = _Emp_Obj.AccountCode;
            txt_AccountCode2.Text = _Emp_Obj.AccountCode2;
            dte_LeavingTryDate.Value = _Emp_Obj.LeavingTryDate;

            if (_Emp_Obj.OverTime == 1)
            {
                rdo_Yes.Checked = true;
            }
            else if (_Emp_Obj.OverTime == 0)
            {
                rdo_No.Checked = true;
            }
            if (_Emp_Obj.ScoreStock == 1)
            {
                rdo_ScoreYes.Checked = true;
            }
            else if (_Emp_Obj.ScoreStock == 0)
            {
                rdo_ScoreNo.Checked = true;
            }

            txt_FullName.Text = _Emp_Obj.PersoObject.FullName;
            txt_IssueID.Text = _Emp_Obj.PersoObject.IssueID;
            txt_IssuePlace.Text = _Emp_Obj.PersoObject.IssuePlace;
            txt_HomeTown.Text = _Emp_Obj.PersoObject.HomeTown;
            txt_CardID.Text = _Emp_Obj.PersoObject.CardID;
            txt_BankCode.Text = _Emp_Obj.PersoObject.BankCode;
            txt_AddressBank.Text = _Emp_Obj.PersoObject.AddressBank;
            if (_Emp_Obj.PersoObject.Gender == 1)
            {
                rdo_Male.Checked = true;
            }
            else if (_Emp_Obj.PersoObject.Gender == 0)
            {
                rdo_Female.Checked = true;
            }
            if (_Emp_Obj.PersoObject.IssueDate != DateTime.MinValue)
            {
                dte_IssueDate.Value = _Emp_Obj.PersoObject.IssueDate;
            }
            else
            {
                dte_IssueDate.Value = DateTime.MinValue;
            }
            if (_Emp_Obj.PersoObject.BirthDay != DateTime.MinValue)
            {
                dte_BirthDay.Value = _Emp_Obj.PersoObject.BirthDay;
            }
            else
            {
                dte_BirthDay.Value = DateTime.MinValue;
            }
            Employee_Object _Report_Obj = new Employee_Object(_Emp_Obj.ReportTo);
            _ReportTo = _Emp_Obj.ReportTo;
            txt_ReportyTo.Text = _Report_Obj.PersoObject.FullName;
            Load_GVData();
            Contract_Info zContract = new Contract_Info(_PersonalKey);
            txt_FullName.Focus();

            lbl_Created.Text = "Tạo bởi:[" + _Emp_Obj.CreatedName + "][" + _Emp_Obj.CreatedOn + "]";
            lbl_Modified.Text = "Chỉnh sửa:[" + _Emp_Obj.ModifiedName + "][" + _Emp_Obj.ModifiedOn + "]";
        }
        private void SaveObject()
        {
            _Emp_Obj.Key = zKey;
            _Emp_Obj.EmployeeID = txt_CardID.Text;
            if (cbo_Branch.SelectedValue != null)
            {
                _Emp_Obj.BranchKey = int.Parse(cbo_Branch.SelectedValue.ToString());
            }
            if (cbo_Department.SelectedValue != null)
            {
                _Emp_Obj.DepartmentKey = int.Parse(cbo_Department.SelectedValue.ToString());
            }
            if (cbo_Team.SelectedIndex != 0)
            {
                _Emp_Obj.TeamKey = int.Parse(cbo_Team.SelectedValue.ToString());
            }
            else
            {
                _Emp_Obj.TeamKey = 0;
            }
            if (cbo_Position.SelectedValue != null)
            {
                _Emp_Obj.PositionKey = int.Parse(cbo_Position.SelectedValue.ToString());
            }
            _Emp_Obj.Description = txt_Description.Text.ToString();
            _Emp_Obj.CompanyPhone = txt_CompanyPhone.Text.ToString();
            _Emp_Obj.CompanyEmail = txt_CompanyEmail.Text.ToString();
            
            if(txt_AccoutCode.Text=="")
            {
                _Emp_Obj.AccountCode = "0";
            }
            else
            {
                _Emp_Obj.AccountCode = txt_AccoutCode.Text;
            }
            DateTime zStarDte = new DateTime(dte_StartingDate.Value.Year, dte_StartingDate.Value.Month, dte_StartingDate.Value.Day, 0, 0, 0);
            DateTime zLeavingDate = DateTime.MinValue;
            if (dte_LeavingDate.Value != DateTime.MinValue)
            {
                zLeavingDate = new DateTime(dte_LeavingDate.Value.Year, dte_LeavingDate.Value.Month, dte_LeavingDate.Value.Day, 23, 59, 59);
            }
            _Emp_Obj.StartingDate = zStarDte;
            _Emp_Obj.LeavingDate = zLeavingDate;
            DateTime zLeavingTryDate = DateTime.MinValue;
            if (dte_LeavingTryDate.Value != DateTime.MinValue)
            {
                zLeavingTryDate = new DateTime(dte_LeavingTryDate.Value.Year, dte_LeavingTryDate.Value.Month, dte_LeavingTryDate.Value.Day, 23, 59, 59);
            }
            _Emp_Obj.LeavingTryDate = zLeavingTryDate;

            if (zLeavingDate!=DateTime.MinValue)
            {
                _Emp_Obj.WorkingStatus = 2;
                CapNhatHetHan(_Emp_Obj.Key, zLeavingDate);
            }
            else
            {
                _Emp_Obj.WorkingStatus = cbo_WorkingStatus.SelectedValue.ToInt();
            }
            if (txt_AccountCode2.Text == "")
            {
                _Emp_Obj.AccountCode2 = "0";
            }
            else
            {
                _Emp_Obj.AccountCode2 = txt_AccountCode2.Text;
            }
            if (rdo_Yes.Checked == true)
            {
                _Emp_Obj.OverTime = 1;
            }
            else if (rdo_No.Checked == true)
            {
                _Emp_Obj.OverTime = 0;
            }

            if (rdo_ScoreYes.Checked == true)
            {
                _Emp_Obj.ScoreStock = 1;
            }
            else if (rdo_ScoreNo.Checked == true)
            {
                _Emp_Obj.ScoreStock = 0;
            }

            _Emp_Obj.Organization = Employee_Data.Check_Organization(zKey, _ReportTo);
            _Emp_Obj.ReportTo = _ReportTo;
            _Emp_Obj.PersoObject.Key = _PersonalKey;
            _Emp_Obj.PersoObject.CardID = txt_CardID.Text.ToString();
            _Emp_Obj.PersoObject.FullName = txt_FullName.Text.ToString();
            _Emp_Obj.PersoObject.IssueID = txt_IssueID.Text.ToString();
            _Emp_Obj.PersoObject.HomeTown = txt_HomeTown.Text.ToString();
            _Emp_Obj.PersoObject.IssuePlace = txt_IssuePlace.Text.ToString();
            _Emp_Obj.PersoObject.BankCode = txt_BankCode.Text.ToString();
            _Emp_Obj.PersoObject.AddressBank = txt_AddressBank.Text;
            if (rdo_Male.Checked == true)
            {
                _Emp_Obj.PersoObject.Gender = 1;
            }
            else if (rdo_Female.Checked == true)
            {
                _Emp_Obj.PersoObject.Gender = 0;
            }
            _Emp_Obj.PersoObject.BirthDay = dte_BirthDay.Value;
            _Emp_Obj.PersoObject.IssueDate = dte_IssueDate.Value;
            zListAdress = new List<Address_Info>();
            Address_Info zAddress;
            for (int i = 0; i < GVData.Rows.Count - 1; i++)
            {
                if (GVData.Rows[i].Tag != null)
                {
                    zAddress = (Address_Info)GVData.Rows[i].Tag;
                    if (_Emp_Obj.Key == "")
                    {
                        zAddress.RecordStatus = 1;
                    }
                    zListAdress.Add(zAddress);
                    _Emp_Obj.PersoObject.List_Address = zListAdress;

                }
            }
            _Emp_Obj.CreatedBy = SessionUser.UserLogin.Key;
            _Emp_Obj.CreatedName = SessionUser.UserLogin.EmployeeName;
            _Emp_Obj.ModifiedBy = SessionUser.UserLogin.Key;
            _Emp_Obj.ModifiedName = SessionUser.UserLogin.EmployeeName;
            _Emp_Obj.PersoObject.CreatedBy = SessionUser.UserLogin.Key;
            _Emp_Obj.PersoObject.CreatedName = SessionUser.UserLogin.EmployeeName;
            _Emp_Obj.PersoObject.ModifiedBy = SessionUser.UserLogin.Key;
            _Emp_Obj.PersoObject.ModifiedName = SessionUser.UserLogin.EmployeeName;

            _Emp_Obj.SaveObject();
            if(_Emp_Obj.Message=="11")
            {//Khởi tạo lưu thông tin log
                CreareWorkingHistory(_Emp_Obj.Key);
            }
            else
            {
                if (zLeavingDate != DateTime.MinValue)
                {
                    CapNhatHetHan(_Emp_Obj.Key, zLeavingDate);
                }
            }
            _PersonalKey = _Emp_Obj.PersoObject.Key;
            string zMessage = TN_Message.Show(_Emp_Obj.Message);
            if (zMessage.Length == 0)
            {
                Utils.TNMessageBox("Câp nhật thành công",3);
            }
            else
            {
                Utils.TNMessageBox(zMessage,4);
            }
        }
        string zAmountErr = "";
        private void CreareWorkingHistory(string EmployeeKey)
        {
            WorkingHistory_Detail zinfo = new WorkingHistory_Detail();
            zinfo.EmployeeKey = _Emp_Obj.Key;
            zinfo.EmployeeID = _Emp_Obj.EmployeeID.ToUpper();
            zinfo.EmployeeName = _Emp_Obj.FullName.Trim().ToString();
            zinfo.BranchKey = cbo_Branch.SelectedValue.ToInt();
            zinfo.BranchName = cbo_Branch.Text;
            zinfo.DepartmentKey = cbo_Department.SelectedValue.ToInt();
            zinfo.DepartmentName = cbo_Department.Text;
            zinfo.TeamKey = cbo_Team.SelectedValue.ToInt();
            zinfo.TeamName = cbo_Team.Text;
            zinfo.PositionKey = cbo_Position.SelectedValue.ToInt();
            zinfo.PositionName = cbo_Position.Text;
            zinfo.Description = txt_Description.Text;
            DateTime zFromDate = new DateTime(dte_StartingDate.Value.Year, dte_StartingDate.Value.Month, dte_StartingDate.Value.Day, 0, 0, 0);
            DateTime zToDate = DateTime.MinValue;
            //if (dte_LeavingDate.Value != DateTime.MinValue)
            //{
            //    zToDate = new DateTime(dte_LeavingDate.Value.Year, dte_LeavingDate.Value.Month, dte_LeavingDate.Value.Day, 23, 59, 59);
            //}
            zinfo.StartingDate = zFromDate;
            zinfo.LeavingDate = zToDate;

            zinfo.CreatedBy = SessionUser.UserLogin.Key;
            zinfo.CreatedName = SessionUser.UserLogin.EmployeeName;
            zinfo.ModifiedBy = SessionUser.UserLogin.Key;
            zinfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
            zinfo.Create();
        }

        private string CheckBeforeSave()
        {
            zAmountErr = "";

            if (txt_FullName.Text.Length == 0)
            {
                txt_FullName.BackColor = Color.FromArgb(230, 100, 100);
                zAmountErr += "-Tên nhân viên. \n";
            }
            else
            {
                txt_FullName.BackColor = Color.FromArgb(255, 255, 225);
            }
            if(dte_BirthDay.Value==DateTime.MinValue)
            {
                zAmountErr += "-Ngày sinh. \n";
            }
            if (dte_IssueDate.Value == DateTime.MinValue)
            {
                zAmountErr += "-Ngày cấp CMND. \n";
            }
            if (txt_IssuePlace.Text.Length == 0)
            {
                txt_IssuePlace.BackColor = Color.FromArgb(230, 100, 100);
                zAmountErr += "-Nơi Cấp CMND. \n";
            }
            else
            {
                txt_IssuePlace.BackColor = Color.FromArgb(255, 255, 225);
            }
            if (txt_HomeTown.Text.Length == 0)
            {
                txt_HomeTown.BackColor = Color.FromArgb(230, 100, 100);
                zAmountErr += "-Địa chỉ. \n";
            }
            else
            {
                txt_HomeTown.BackColor = Color.FromArgb(255, 255, 225);
            }
            //int zOld = Employee_Data.FindIssuedID(txt_IssueID.Text, _Emp_Obj.Key);
            //if (_Emp_Obj.Key == "")
            //{
            //    if (zOld != 0)
            //    {
            //        txt_IssueID.BackColor = Color.FromArgb(230, 100, 100);
            //        zAmountErr += "-Số CMND không hợp lệ. \n";
            //    }
            //    else
            //    {
            //        txt_IssueID.BackColor = Color.FromArgb(255, 255, 225);
            //    }
            //}
            //else
            //{
            //    if (zOld > 0)
            //    {
            //        txt_IssueID.BackColor = Color.FromArgb(230, 100, 100);
            //        zAmountErr += "-Số CMND không hợp lệ. \n";
            //    }
            //    else
            //    {
            //        txt_IssueID.BackColor = Color.FromArgb(255, 255, 225);
            //    }
            //}
            int zCard = Employee_Data.FindCard(txt_CardID.Text, _Emp_Obj.Key);
            if (_Emp_Obj.Key == "")
            {
                if (zCard != 0)
                {
                    txt_CardID.BackColor = Color.FromArgb(230, 100, 100);
                    zAmountErr += "-Mã nhân viên không hợp lệ. \n";
                }
                else
                {
                    txt_CardID.BackColor = Color.FromArgb(255, 255, 225);
                }
            }
            else
            {
                if (zCard > 0)
                {
                    txt_CardID.BackColor = Color.FromArgb(230, 100, 100);
                    zAmountErr += "-Mã nhân viên không hợp lệ. \n";
                }
                else
                {
                    txt_CardID.BackColor = Color.FromArgb(255, 255, 225);
                }
            }
            int zAddress = 0;
            for (int i = 0; i < GVData.Rows.Count; i++)
            {
                if (GVData.Rows[i].Cells["Country"].ErrorText != "" && GVData.Rows[i].Visible == true)
                {
                    zAddress++;
                }
            }
            for (int i = 0; i < GVData.Rows.Count; i++)
            {
                if (GVData.Rows[i].Cells["Province"].ErrorText != "" && GVData.Rows[i].Visible == true)
                {
                    zAddress++;
                }
            }
            for (int i = 0; i < GVData.Rows.Count; i++)
            {
                if (GVData.Rows[i].Cells["District"].ErrorText != "" && GVData.Rows[i].Visible == true)
                {
                    zAddress++;
                }
            }
            for (int i = 0; i < GVData.Rows.Count; i++)
            {
                if (GVData.Rows[i].Cells["Ward"].ErrorText != "" && GVData.Rows[i].Visible == true)
                {
                    zAddress++;
                }
            }
            for (int i = 0; i < GVData.Rows.Count; i++)
            {
                if (GVData.Rows[i].Cells["Address"].ErrorText != "" && GVData.Rows[i].Visible == true)
                {
                    zAddress++;
                }
            }
            if (zAddress != 0)
            {
                zAmountErr += " -Thông tin địa chỉ.\n";
            }
            return zAmountErr;
        }
        private int Check_IssueID()
        {
            int IssuedID = Employee_Data.FindIssuedID(txt_IssueID.Text, _Emp_Obj.Key);
            if (IssuedID > 0 && txt_IssueID.Text != "")
            {
                Utils.TNMessageBoxOK("Đã Có Số CMND",1);

            }
            else if (IssuedID == 0 && txt_IssueID.Text != "")
            {
                Utils.TNMessageBoxOK("Số CMND được sử dụng",2);
            }
            if (txt_IssueID.Text == "")
            {
                Utils.TNMessageBoxOK("Vui lòng nhập CMND",1);
            }
            return IssuedID;
        }
        private int Check_CardID()
        {
            int EmployeeID = Employee_Data.FindCard(txt_CardID.Text, _Emp_Obj.Key);
            if (EmployeeID > 0 && txt_CardID.Text != "")
            {
                Utils.TNMessageBoxOK("Đã Có mã nhân viên",1);

            }
            else if (EmployeeID == 0 && txt_CardID.Text != "")
            {
                Utils.TNMessageBoxOK("Mã nhân viên được sử dụng",1);
            }
            if (txt_CardID.Text == "")
            {
                Utils.TNMessageBoxOK("Vui lòng nhập mã nhân viên",1);
            }
            return EmployeeID;
        }
        private void Txt_IssueID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Check_IssueID();
            }
        }
        #endregion

        #region-----LV_Contract-----
        private void LV_Contract_Layout(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số Hợp Đồng";
            colHead.Width = 250;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ngày Ký Hợp Đồng";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ngày Hết Hạn Hợp Đồng";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tình Trạng";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);
        }
        private void LV_Contract_LoadData()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LV_Contract;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            DataTable In_Table = new DataTable();
            In_Table = Employee_Data.Load_Contract(_PersonalKey);
            LV.Items.Clear();
            int n = In_Table.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                DataRow nRow = In_Table.Rows[i];
                lvi.Tag = nRow["ContractKey"];
                lvi.ForeColor = Color.Navy;

                lvi.BackColor = Color.White;
                lvi.ImageIndex = 0;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["ContractID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                DateTime zDate = DateTime.Parse(nRow["SignDay"].ToString());
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zDate.ToString("dd/MM/yyyy");
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = DateTime.Parse(nRow["FinishDay"].ToString().Trim()).ToString("dd/MM/yyyy");
                lvi.SubItems.Add(lvsi);

                int zRecorStatus = int.Parse(nRow["RecordStatus"].ToString().Trim());
                if (zRecorStatus == 62)
                {
                    lvsi = new ListViewItem.ListViewSubItem();
                    lvsi.Text = "Hết Hạn";
                    lvi.ForeColor = Color.Red;
                    lvi.SubItems.Add(lvsi);
                }
                else
                {
                    lvsi = new ListViewItem.ListViewSubItem();
                    lvsi.Text = "Còn Hạn";
                    lvi.SubItems.Add(lvsi);
                }

                LV.Items.Add(lvi);
            }
            this.Cursor = Cursors.Default;
        }
        #endregion
        #endregion

        #region ----- Button -----
        private void Btn_Update_Click(object sender, EventArgs e)
        {
        }
        private void Btn_Add_Click(object sender, EventArgs e)
        {
            if (CheckBeforeSave().Trim().Length == 0)
            {
                SaveObject();
                InitData_LV();
            }
            else
            {
               Utils.TNMessageBoxOK("Vui Lòng diền đầy đủ thông tin!. \n" + zAmountErr,1);
            }
        }
        private void Btn_Deny_Click(object sender, EventArgs e)
        {
            if (Utils.TNMessageBox("Bạn có chắc muốn xóa",2) == "Y")
            {
                _Emp_Obj.CreatedBy = SessionUser.UserLogin.Key;
                _Emp_Obj.CreatedName = SessionUser.UserLogin.EmployeeName;
                _Emp_Obj.ModifiedBy = SessionUser.UserLogin.Key;
                _Emp_Obj.ModifiedName = SessionUser.UserLogin.EmployeeName;
                _Emp_Obj.PersoObject.CreatedBy = SessionUser.UserLogin.Key;
                _Emp_Obj.PersoObject.CreatedName = SessionUser.UserLogin.EmployeeName;
                _Emp_Obj.PersoObject.ModifiedBy = SessionUser.UserLogin.Key;
                _Emp_Obj.PersoObject.ModifiedName = SessionUser.UserLogin.EmployeeName;
                _Emp_Obj.DeleteObject();
                zKey = "";
                Load_Data();
            }
            InitData_LV();
        }
        private void btn_New_Click(object sender, EventArgs e)
        {
            zKey = "";
            Load_Data();
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            InitData_LV();
        }
        private void Btn_CheckIssuedID_Click(object sender, EventArgs e)
        {
            Check_IssueID();
        }
        private void Btn_CheckCardID_Click(object sender, EventArgs e)
        {
            Check_CardID();
        }
        private void Txt_ReportyTo_Leave(object sender, EventArgs e)
        {
            if (txt_ReportyTo.Text.Length > 0)
            {
                Personal_Info zPerson = new Personal_Info(txt_ReportyTo.Text, true, 1);
                _ReportTo = zPerson.ParentKey;
            }
        }
        private void Btn_IP_Leaving_Click(object sender, EventArgs e)
        {
            Frm_Import_NghiViec frm = new Frm_Import_NghiViec();
            frm.Show();
        }
        #endregion
        private  void CapNhatHetHan(string EmployeeKey, DateTime LeavingDate)
        {
            // 2.Cập nhật trích lương
            Employee_FeeDefault_Data.FeeDefault_CapNhatNgayHetHan_NghiViec(EmployeeKey, LeavingDate, SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeName);
            // 3. Mưc lương
            Employee_SalaryDefault_Data.Salary_CapNhatNgayHetHan_NghiViec(EmployeeKey, LeavingDate, SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeName);
            // 4.Lịch sử
             WorkingHistory_Data.Working_CapNhatNgayHetHan_NghiViec(EmployeeKey, LeavingDate, SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeName);
            // 5.Cập nhật hỗ trợ con nhỏ
            Employee_FeeChildren_Data.Child_CapNhatNgayHetHan_NghiViec(EmployeeKey, LeavingDate, SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeName);

        }
        //Chuyển số thành dãy chuỗi
        string ConvertIDRank(string ID)
        {
            string zResult = "";
            string temp = ID.Replace("A", "");
            string s = "";
            //Thiếu bao nhieu số kí tự thì thêm vào bấy nhiêu cho đủ chuỗi VD: 11--->0011, A11--->1000011
            for (int i = 0; i < 4 - temp.Trim().Length; i++)
            {
                s += "0";
            }
            if (ID.Substring(0, 1) == "A")
            {
                zResult = "100" + s + temp;
            }
            else
            {
                zResult = s + temp;
            }

            return zResult;
        }

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Normal)
            {
                this.WindowState = FormWindowState.Maximized;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
                this.StartPosition = FormStartPosition.CenterScreen;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
            }
            if (Role.RoleAdd == 0 && Role.RoleEdit == 0)
            {
                btn_New.Enabled = false;
                btn_Add.Enabled = false;
                btn_Import.Enabled = false;
            }
            if (Role.RoleAdd == 0 && Role.RoleEdit == 1)
            {
                btn_New.Enabled = false;
                btn_Add.Enabled = true;
            }
            if (Role.RoleDel == 0)
            {
                btn_Deny.Enabled = false;
            }
        }
        #endregion

    }
}
