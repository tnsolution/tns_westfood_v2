﻿namespace TNS.WinApp
{
    partial class Frm_Calculator_TimeKeeping
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Calculator_TimeKeeping));
            this.btnClose = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnMini = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.PicLoading = new System.Windows.Forms.PictureBox();
            this.btnHideLog = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnShowLog = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.timerEmployee = new System.Windows.Forms.Timer(this.components);
            this.kryptonHeader1 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btnMax = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.Panel_Team = new System.Windows.Forms.Panel();
            this.GVTeam = new System.Windows.Forms.DataGridView();
            this.Panel_Right = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_ViewLog = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.label1 = new System.Windows.Forms.Label();
            this.kryptonHeader2 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.Panel_Search = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rdo_All = new System.Windows.Forms.RadioButton();
            this.rdo_VanPhong = new System.Windows.Forms.RadioButton();
            this.cboTeam = new ComponentFactory.Krypton.Toolkit.KryptonComboBox();
            this.rdo_CongNhan = new System.Windows.Forms.RadioButton();
            this.rdo_HoTro = new System.Windows.Forms.RadioButton();
            this.dte_FromDate = new ComponentFactory.Krypton.Toolkit.KryptonDateTimePicker();
            this.btn_Run = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.HeaderControl = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.timerTeam = new System.Windows.Forms.Timer(this.components);
            this.txt_Log_Detail = new System.Windows.Forms.ListBox();
            this.txtLog = new System.Windows.Forms.ListBox();
            ((System.ComponentModel.ISupportInitialize)(this.PicLoading)).BeginInit();
            this.Panel_Team.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GVTeam)).BeginInit();
            this.Panel_Right.SuspendLayout();
            this.panel1.SuspendLayout();
            this.Panel_Search.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboTeam)).BeginInit();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.UniqueName = "11B07C6F4E1C4F9D8B91BD924CB0EBE6";
            // 
            // btnMini
            // 
            this.btnMini.Image = ((System.Drawing.Image)(resources.GetObject("btnMini.Image")));
            this.btnMini.UniqueName = "F5F06E8241504E72ABB5BEA3F6A9B753";
            // 
            // PicLoading
            // 
            this.PicLoading.BackColor = System.Drawing.Color.Transparent;
            this.PicLoading.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.PicLoading.Dock = System.Windows.Forms.DockStyle.Right;
            this.PicLoading.Image = ((System.Drawing.Image)(resources.GetObject("PicLoading.Image")));
            this.PicLoading.Location = new System.Drawing.Point(914, 42);
            this.PicLoading.Name = "PicLoading";
            this.PicLoading.Size = new System.Drawing.Size(68, 66);
            this.PicLoading.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.PicLoading.TabIndex = 207;
            this.PicLoading.TabStop = false;
            this.PicLoading.Visible = false;
            // 
            // btnHideLog
            // 
            this.btnHideLog.Image = ((System.Drawing.Image)(resources.GetObject("btnHideLog.Image")));
            this.btnHideLog.UniqueName = "D6E0AB28C2CE49FBDD866175ABE4E274";
            // 
            // btnShowLog
            // 
            this.btnShowLog.Image = ((System.Drawing.Image)(resources.GetObject("btnShowLog.Image")));
            this.btnShowLog.UniqueName = "ABE0F8FEF4FF4DE3448D0BAE7F1C6F22";
            // 
            // timerEmployee
            // 
            this.timerEmployee.Enabled = true;
            this.timerEmployee.Tick += new System.EventHandler(this.timerEmployee_Tick);
            // 
            // kryptonHeader1
            // 
            this.kryptonHeader1.AutoSize = false;
            this.kryptonHeader1.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnShowLog,
            this.btnHideLog});
            this.kryptonHeader1.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader1.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader1.Name = "kryptonHeader1";
            this.kryptonHeader1.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader1.Size = new System.Drawing.Size(634, 30);
            this.kryptonHeader1.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader1.TabIndex = 200;
            this.kryptonHeader1.Values.Description = "";
            this.kryptonHeader1.Values.Heading = "1 - Tổ/ nhóm";
            // 
            // btnMax
            // 
            this.btnMax.Image = ((System.Drawing.Image)(resources.GetObject("btnMax.Image")));
            this.btnMax.UniqueName = "035D1A4881E44F58A084C31DE7352A94";
            this.btnMax.Visible = false;
            // 
            // Panel_Team
            // 
            this.Panel_Team.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Team.Controls.Add(this.GVTeam);
            this.Panel_Team.Controls.Add(this.kryptonHeader1);
            this.Panel_Team.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel_Team.Location = new System.Drawing.Point(0, 110);
            this.Panel_Team.Name = "Panel_Team";
            this.Panel_Team.Size = new System.Drawing.Size(634, 451);
            this.Panel_Team.TabIndex = 215;
            // 
            // GVTeam
            // 
            this.GVTeam.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.GVTeam.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.GVTeam.ColumnHeadersHeight = 25;
            this.GVTeam.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.GVTeam.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GVTeam.Location = new System.Drawing.Point(0, 30);
            this.GVTeam.Name = "GVTeam";
            this.GVTeam.ReadOnly = true;
            this.GVTeam.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GVTeam.Size = new System.Drawing.Size(634, 421);
            this.GVTeam.TabIndex = 203;
            // 
            // Panel_Right
            // 
            this.Panel_Right.Controls.Add(this.txtLog);
            this.Panel_Right.Controls.Add(this.txt_Log_Detail);
            this.Panel_Right.Controls.Add(this.panel1);
            this.Panel_Right.Controls.Add(this.kryptonHeader2);
            this.Panel_Right.Dock = System.Windows.Forms.DockStyle.Right;
            this.Panel_Right.Location = new System.Drawing.Point(634, 110);
            this.Panel_Right.Name = "Panel_Right";
            this.Panel_Right.Size = new System.Drawing.Size(350, 451);
            this.Panel_Right.TabIndex = 214;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panel1.Controls.Add(this.btn_ViewLog);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 30);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(350, 62);
            this.panel1.TabIndex = 215;
            // 
            // btn_ViewLog
            // 
            this.btn_ViewLog.Location = new System.Drawing.Point(183, 9);
            this.btn_ViewLog.Name = "btn_ViewLog";
            this.btn_ViewLog.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_ViewLog.Size = new System.Drawing.Size(106, 40);
            this.btn_ViewLog.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ViewLog.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ViewLog.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_ViewLog.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ViewLog.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ViewLog.TabIndex = 280;
            this.btn_ViewLog.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_ViewLog.Values.Image")));
            this.btn_ViewLog.Values.Text = "Xem ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(19, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(158, 14);
            this.label1.TabIndex = 150;
            this.label1.Text = "Xem lịch sử tính chấm công";
            // 
            // kryptonHeader2
            // 
            this.kryptonHeader2.AutoSize = false;
            this.kryptonHeader2.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader2.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader2.Name = "kryptonHeader2";
            this.kryptonHeader2.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader2.Size = new System.Drawing.Size(350, 30);
            this.kryptonHeader2.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader2.TabIndex = 205;
            this.kryptonHeader2.Values.Description = "";
            this.kryptonHeader2.Values.Heading = "Theo dõi";
            // 
            // Panel_Search
            // 
            this.Panel_Search.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Search.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel_Search.Controls.Add(this.groupBox2);
            this.Panel_Search.Controls.Add(this.dte_FromDate);
            this.Panel_Search.Controls.Add(this.PicLoading);
            this.Panel_Search.Controls.Add(this.btn_Run);
            this.Panel_Search.Controls.Add(this.HeaderControl);
            this.Panel_Search.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel_Search.Location = new System.Drawing.Point(0, 0);
            this.Panel_Search.Name = "Panel_Search";
            this.Panel_Search.Size = new System.Drawing.Size(984, 110);
            this.Panel_Search.TabIndex = 213;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rdo_All);
            this.groupBox2.Controls.Add(this.rdo_VanPhong);
            this.groupBox2.Controls.Add(this.cboTeam);
            this.groupBox2.Controls.Add(this.rdo_CongNhan);
            this.groupBox2.Controls.Add(this.rdo_HoTro);
            this.groupBox2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.groupBox2.ForeColor = System.Drawing.Color.Navy;
            this.groupBox2.Location = new System.Drawing.Point(372, 42);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(507, 65);
            this.groupBox2.TabIndex = 308;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Tùy chọn";
            // 
            // rdo_All
            // 
            this.rdo_All.AutoSize = true;
            this.rdo_All.Checked = true;
            this.rdo_All.Font = new System.Drawing.Font("Tahoma", 9F);
            this.rdo_All.Location = new System.Drawing.Point(6, 17);
            this.rdo_All.Name = "rdo_All";
            this.rdo_All.Size = new System.Drawing.Size(171, 18);
            this.rdo_All.TabIndex = 209;
            this.rdo_All.TabStop = true;
            this.rdo_All.Text = "1. Tất cả hoặc chọn nhóm";
            this.rdo_All.UseVisualStyleBackColor = true;
            // 
            // rdo_VanPhong
            // 
            this.rdo_VanPhong.AutoSize = true;
            this.rdo_VanPhong.Font = new System.Drawing.Font("Tahoma", 9F);
            this.rdo_VanPhong.Location = new System.Drawing.Point(6, 38);
            this.rdo_VanPhong.Name = "rdo_VanPhong";
            this.rdo_VanPhong.Size = new System.Drawing.Size(114, 18);
            this.rdo_VanPhong.TabIndex = 210;
            this.rdo_VanPhong.Text = "2. Khối gián tiếp";
            this.rdo_VanPhong.UseVisualStyleBackColor = true;
            // 
            // cboTeam
            // 
            this.cboTeam.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTeam.DropDownWidth = 119;
            this.cboTeam.Location = new System.Drawing.Point(198, 14);
            this.cboTeam.Name = "cboTeam";
            this.cboTeam.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.cboTeam.Size = new System.Drawing.Size(271, 24);
            this.cboTeam.StateCommon.ComboBox.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.cboTeam.StateCommon.ComboBox.Border.Rounding = 4;
            this.cboTeam.StateCommon.ComboBox.Border.Width = 1;
            this.cboTeam.StateCommon.ComboBox.Content.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cboTeam.StateCommon.Item.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.cboTeam.StateCommon.Item.Border.Rounding = 4;
            this.cboTeam.StateCommon.Item.Border.Width = 1;
            this.cboTeam.StateCommon.Item.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cboTeam.TabIndex = 208;
            // 
            // rdo_CongNhan
            // 
            this.rdo_CongNhan.AutoSize = true;
            this.rdo_CongNhan.Font = new System.Drawing.Font("Tahoma", 9F);
            this.rdo_CongNhan.Location = new System.Drawing.Point(235, 42);
            this.rdo_CongNhan.Name = "rdo_CongNhan";
            this.rdo_CongNhan.Size = new System.Drawing.Size(99, 18);
            this.rdo_CongNhan.TabIndex = 210;
            this.rdo_CongNhan.Text = "4. Công nhân";
            this.rdo_CongNhan.UseVisualStyleBackColor = true;
            // 
            // rdo_HoTro
            // 
            this.rdo_HoTro.AutoSize = true;
            this.rdo_HoTro.Font = new System.Drawing.Font("Tahoma", 9F);
            this.rdo_HoTro.Location = new System.Drawing.Point(136, 41);
            this.rdo_HoTro.Name = "rdo_HoTro";
            this.rdo_HoTro.Size = new System.Drawing.Size(75, 18);
            this.rdo_HoTro.TabIndex = 210;
            this.rdo_HoTro.Text = "3. Hỗ trợ";
            this.rdo_HoTro.UseVisualStyleBackColor = true;
            // 
            // dte_FromDate
            // 
            this.dte_FromDate.CustomFormat = "MM/yyyy";
            this.dte_FromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dte_FromDate.Location = new System.Drawing.Point(3, 59);
            this.dte_FromDate.Name = "dte_FromDate";
            this.dte_FromDate.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.dte_FromDate.ShowUpDown = true;
            this.dte_FromDate.Size = new System.Drawing.Size(109, 23);
            this.dte_FromDate.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.dte_FromDate.StateCommon.Border.Rounding = 4;
            this.dte_FromDate.StateCommon.Border.Width = 1;
            this.dte_FromDate.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dte_FromDate.TabIndex = 209;
            // 
            // btn_Run
            // 
            this.btn_Run.Location = new System.Drawing.Point(116, 51);
            this.btn_Run.Name = "btn_Run";
            this.btn_Run.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Run.Size = new System.Drawing.Size(102, 40);
            this.btn_Run.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Run.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Run.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Run.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Run.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Run.TabIndex = 206;
            this.btn_Run.Tag = "0";
            this.btn_Run.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Run.Values.Image")));
            this.btn_Run.Values.Text = "Khởi động";
            // 
            // HeaderControl
            // 
            this.HeaderControl.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnMini,
            this.btnMax,
            this.btnClose});
            this.HeaderControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.HeaderControl.Location = new System.Drawing.Point(0, 0);
            this.HeaderControl.Name = "HeaderControl";
            this.HeaderControl.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.HeaderControl.Size = new System.Drawing.Size(982, 42);
            this.HeaderControl.TabIndex = 205;
            this.HeaderControl.Values.Description = "";
            this.HeaderControl.Values.Heading = "Tính toán chấm công nhân sự";
            this.HeaderControl.Values.Image = null;
            // 
            // timerTeam
            // 
            this.timerTeam.Enabled = true;
            this.timerTeam.Tick += new System.EventHandler(this.timerTeam_Tick);
            // 
            // txt_Log_Detail
            // 
            this.txt_Log_Detail.AllowDrop = true;
            this.txt_Log_Detail.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txt_Log_Detail.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.txt_Log_Detail.FormattingEnabled = true;
            this.txt_Log_Detail.HorizontalScrollbar = true;
            this.txt_Log_Detail.Location = new System.Drawing.Point(0, 343);
            this.txt_Log_Detail.Name = "txt_Log_Detail";
            this.txt_Log_Detail.ScrollAlwaysVisible = true;
            this.txt_Log_Detail.Size = new System.Drawing.Size(350, 108);
            this.txt_Log_Detail.TabIndex = 216;
            // 
            // txtLog
            // 
            this.txtLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLog.FormattingEnabled = true;
            this.txtLog.Location = new System.Drawing.Point(0, 92);
            this.txtLog.Name = "txtLog";
            this.txtLog.ScrollAlwaysVisible = true;
            this.txtLog.Size = new System.Drawing.Size(350, 251);
            this.txtLog.TabIndex = 217;
            // 
            // Frm_Calculator_TimeKeeping
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 561);
            this.Controls.Add(this.Panel_Team);
            this.Controls.Add(this.Panel_Right);
            this.Controls.Add(this.Panel_Search);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_Calculator_TimeKeeping";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Calculator_TimeKeeping";
            this.Load += new System.EventHandler(this.Calculator_TimeKeeping_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PicLoading)).EndInit();
            this.Panel_Team.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GVTeam)).EndInit();
            this.Panel_Right.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.Panel_Search.ResumeLayout(false);
            this.Panel_Search.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboTeam)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnClose;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMini;
        private System.Windows.Forms.PictureBox PicLoading;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnHideLog;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnShowLog;
        private System.Windows.Forms.Timer timerEmployee;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader1;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMax;
        private System.Windows.Forms.Panel Panel_Team;
        private System.Windows.Forms.DataGridView GVTeam;
        private System.Windows.Forms.Panel Panel_Right;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader2;
        private System.Windows.Forms.Panel Panel_Search;
        private ComponentFactory.Krypton.Toolkit.KryptonComboBox cboTeam;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Run;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader HeaderControl;
        private System.Windows.Forms.Timer timerTeam;
        private ComponentFactory.Krypton.Toolkit.KryptonDateTimePicker dte_FromDate;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rdo_All;
        private System.Windows.Forms.RadioButton rdo_VanPhong;
        private System.Windows.Forms.RadioButton rdo_CongNhan;
        private System.Windows.Forms.RadioButton rdo_HoTro;
        private System.Windows.Forms.Panel panel1;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_ViewLog;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox txt_Log_Detail;
        private System.Windows.Forms.ListBox txtLog;
    }
}