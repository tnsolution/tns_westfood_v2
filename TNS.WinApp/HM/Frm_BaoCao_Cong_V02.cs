﻿using C1.Win.C1FlexGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.CORE;
using TNS.HRM;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_BaoCao_Cong_V02 : Form
    {
        DateTime _FromDate;
        DateTime _ToDate;
        private int _BranchKey = 0;
        private int _DepartmentKey = 0;
        private int _TeamKey = 0;
        private string _Search = "";
        public Frm_BaoCao_Cong_V02()
        {
            InitializeComponent();
            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;
            btnClose.Click += btnClose_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            btn_Save.Click += Btn_Save_Click;
            btn_Export.Click += Btn_Export_Click;
            btn_Done.Click += Btn_Done_Click;
            btn_Search.Click += Btn_Search_Click;
            cbo_Branch.SelectedIndexChanged += Cbo_Branch_SelectedIndexChanged;
            cbo_Department.SelectedIndexChanged += Cbo_Department_SelectedIndexChanged;
            rdo_Clear.CheckedChanged += Rdo_Clear_CheckedChanged;

        }

        private void Frm_BaoCao_Cong_V02_Load(object sender, EventArgs e)
        {
            if (SessionUser.UserLogin.Key != "4e5a9e53-9241-4c9b-a69f-e86a7ae3507f")
            {
                Utils.TNMessageBoxOK("Trang đang nâng cấp.Vui lòng quay lại sau!", 1);
                this.Close();
            }
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            Check_RoleForm();

            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            LoadDataToToolbox.KryptonComboBox(cbo_Department, "SELECT DepartmentKey, DepartmentName FROM[dbo].[SYS_Department] WHERE DepartmentKey != 98  AND RecordStatus< 99", "---- Tất cả----");
            LoadDataToToolbox.KryptonComboBox(cbo_Team, "SELECT TeamKey,TeamName FROM SYS_Team WHERE TeamKey != 98  AND RecordStatus < 99", "---- Tất cả ----");
            LoadDataToToolbox.KryptonComboBox(cbo_Branch, "SELECT BranchKey,BranchName FROM SYS_Branch WHERE BranchKey != 98  AND RecordStatus < 99", "---- Tất cả ----");
            dte_Month.Value = SessionUser.Date_Work;
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
        }
        private void Rdo_Clear_CheckedChanged(object sender, EventArgs e)
        {
            if (rdo_Clear.Checked)
            {
                Frm_ScoreApprove frm = new Frm_ScoreApprove();
                frm.ShowDialog();
            }
        }
        private void Cbo_Branch_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDataToToolbox.KryptonComboBox(cbo_Department, "SELECT DepartmentKey, DepartmentName FROM[dbo].[SYS_Department] WHERE BranchKey = " + cbo_Branch.SelectedValue.ToInt() + "  AND RecordStatus< 99", "---- Tất cả----");

        }
        private void Cbo_Department_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDataToToolbox.KryptonComboBox(cbo_Team, "SELECT TeamKey,TeamName FROM SYS_Team WHERE TeamKey != 98 AND DepartmentKey = " + cbo_Department.SelectedValue.ToInt() + "  AND RecordStatus < 99", "---- Tất cả ----");
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            _FromDate = new DateTime(dte_Month.Value.Year, dte_Month.Value.Month, 1, 0, 0, 0);
            _ToDate = _FromDate.AddMonths(1).AddDays(-1);
            _ToDate = new DateTime(_ToDate.Year, _ToDate.Month, _ToDate.Day, 23, 59, 59);
            _BranchKey = cbo_Branch.SelectedValue.ToInt();
            _DepartmentKey = cbo_Department.SelectedValue.ToInt();
            _TeamKey = cbo_Team.SelectedValue.ToInt();
            _Search = txt_Search.Text.Trim();

            string Status = "Tìm DL chấm công chưa chốt > tháng:" + dte_Month.Value.ToString("MM/yyyy") + " > Khối:" + cbo_Branch.Text + " >Bộ phận:" + cbo_Department.Text + " > Nhóm:" + cbo_Team.Text + " > Từ khóa:" + txt_Search.Text;
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);

            try
            {
                using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }

            }
            catch (Exception ex)
            {
                Utils.TNMessageBoxOK(ex.ToString(), 4);
            }

        }
        private int _STT = 1;
        private void DisplayData()
        {

            DataTable _Intable = Report.BaoCaoCong_V02(_FromDate, _ToDate, _BranchKey, _DepartmentKey, _TeamKey, _Search);
            if (_Intable.Rows.Count > 0)
            {
                this.Invoke(new MethodInvoker(delegate ()
                {
                    InitGV_Layout(_Intable);
                    _STT = 1;
                    int NoGroup = 2;
                    int RowTam = NoGroup;
                    Row zGvRow;
                    int TeamKey = _Intable.Rows[0]["KEYPHONGBAN"].ToInt();
                    string TeamName = _Intable.Rows[0]["TENPHONGBAN"].ToString();
                    DataRow[] Array = _Intable.Select("KEYPHONGBAN=" + TeamKey);
                    if (Array.Length > 0)
                    {
                        DataTable zGroup = Array.CopyToDataTable();
                        GVData.Rows.Add();
                        zGvRow = GVData.Rows[1];
                        HeaderRow(zGvRow, zGroup, TeamKey, TeamName, 0);
                    }
                    for (int i = 0; i < _Intable.Rows.Count; i++)
                    {
                        DataRow r = _Intable.Rows[i];
                        if (TeamKey != r["KEYPHONGBAN"].ToInt())
                        {
                            #region [GROUP]
                            TeamKey = r["KEYPHONGBAN"].ToInt();
                            TeamName = r["TENPHONGBAN"].ToString();
                            Array = _Intable.Select("KEYPHONGBAN=" + TeamKey);
                            _STT = 1;
                            if (Array.Length > 0)
                            {
                                DataTable zGroup = Array.CopyToDataTable();
                                GVData.Rows.Add();
                                zGvRow = GVData.Rows[i + NoGroup];
                                HeaderRow(zGvRow, zGroup, TeamKey, TeamName, _STT);
                            }
                            #endregion
                            NoGroup++;
                        }
                        GVData.Rows.Add();
                        zGvRow = GVData.Rows[i + NoGroup];
                        DetailRow(zGvRow, r, _STT);
                        RowTam = i + NoGroup;
                        _STT++;
                    }
                    GVData.Rows.Add();
                    zGvRow = GVData.Rows[RowTam + 1];
                    TotalRow(zGvRow, _Intable, RowTam + 1);

                    string Status = "Kết quả:Tìm DL chấm công chưa chốt > tháng:" + dte_Month.Value.ToString("MM/yyyy") + " > Khối:" + cbo_Branch.Text + " >Bộ phận:" + cbo_Department.Text + " > Nhóm:" + cbo_Team.Text + " > Từ khóa:" + txt_Search.Text + ">SL trả về:" + _Intable.Rows.Count;
                    Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);

                }));
            }
            else
            {
                //string Status = "Kết quả:Tìm DL chấm công chưa chốt > tháng:" + dte_Month.Value.ToString("MM/yyyy") + " > Khối:" + cbo_Branch.Text + " >Bộ phận:" + cbo_Department.Text + " > Nhóm:" + cbo_Team.Text + " > Từ khóa:" + txt_Search.Text + ">SL trả về:0";
                //Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);

            }
        }
        private void InitGV_Layout(DataTable Table)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();

            int ToTalCol = Table.Columns.Count;
            GVData.Cols.Add(ToTalCol - 2);
            GVData.Rows.Add(1);

            GVData.Rows[0][0] = "Stt";
            GVData.Rows[0][1] = "Họ và tên";
            GVData.Rows[0][2] = "Số thẻ";
            GVData.Rows[0][3] = "Chức vụ";
            int nCol = 4;

            for (int i = 6; i < Table.Columns.Count; i++)
            {
                string[] s = Table.Columns[i].ColumnName.Split('|');
                int zCategory = s[0].ToInt();
                string zName = s[1];
                if (zCategory == 9)
                {
                    DateTime Date = new DateTime(_FromDate.Year, _ToDate.Month, zName.ToInt(), 0, 0, 0);
                    if (Date.DayOfWeek.ToString() == "Sunday")
                        GVData.Cols[nCol].StyleNew.BackColor = Color.LightPink;

                    GVData.Cols[nCol].Width = 30;
                }
                else if (zCategory == 8)// ma cong
                {
                    GVData.Cols[nCol].Width = 30;
                    GVData.Cols[nCol].StyleNew.BackColor = Color.Khaki;
                }
                else if (zCategory == 1)// ma com cong
                {
                    GVData.Cols[nCol].Width = 35;
                    GVData.Cols[nCol].StyleNew.BackColor = Color.LightGreen;
                }
                else if (zCategory == 2)// ma com tru
                {
                    GVData.Cols[nCol].Width = 35;
                    GVData.Cols[nCol].StyleNew.BackColor = Color.MediumPurple;
                }
                else
                {
                    GVData.Cols[nCol].Width = 70;
                }
                GVData.Rows[0][nCol] = zName ;
                GVData.Cols[nCol].TextAlign = TextAlignEnum.RightCenter;

                nCol++;
            }

            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.Custom;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.CenterCenter;
            GVData.Styles.Normal.WordWrap = true;

            GVData.Rows.Fixed = 1;
            GVData.Cols.Frozen = 4;
            GVData.Cols[0].StyleNew.BackColor = Color.Empty;
            GVData.Cols[1].StyleNew.BackColor = Color.Empty;
            GVData.Cols[2].StyleNew.BackColor = Color.Empty;
            GVData.Cols[3].StyleNew.BackColor = Color.Empty;

            GVData.Rows[0].Height = 80;
            GVData.Rows[0].StyleNew.WordWrap = true;
            GVData.Rows[0].TextAlign = TextAlignEnum.CenterCenter;
            //GVData.Rows[0].Caption = "Stt";

            GVData.Cols[0].Width = 40;
            //GVData.Cols[1].StyleNew.BackColor = Color.Empty;
            //GVData.Cols[1].Caption = "Họ tên";
            GVData.Cols[1].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[1].Width = 250;

            //GVData.Cols[2].StyleNew.BackColor = Color.Empty;
            //GVData.Cols[2].Caption = " Mã thẻ";
            GVData.Cols[2].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[2].Width = 100;

            //GVData.Cols[3].StyleNew.BackColor = Color.Empty;
            //GVData.Cols[3].Caption = "Chức vụ";
            GVData.Cols[3].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[3].Width = 150;
        }
        private void HeaderRow(Row RowView, DataTable Table, int TeamKey, string TeamName, int No)
        {
            RowView[0] = "";
            RowView[1] = TeamName;
            RowView[2] = "Số nhân sự " + Table.Select("KEYPHONGBAN=" + TeamKey).Length;
            RowView[3] = "";

            //int nCol = 3;
            //float zTotal = 0;
            //for (int i = 5; i < Table.Columns.Count; i++)
            //{
            //    RowView[nCol] = Table.Compute("SUM([" + Table.Columns[i].ColumnName + "])", "TeamKey=" + TeamKey).Toe0String();
            //    zTotal += RowView[nCol].ToFloat();
            //    nCol++;
            //}
            //RowView[nCol] = zTotal.Toe0String();
            RowView.StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
        }
        private void DetailRow(Row RowView, DataRow rDetail, int No)
        {
            RowView[0] = (No).ToString();
            RowView[1] = rDetail["HOTEN"].ToString().Trim();
            RowView[2] = rDetail["MANHANVIEN"].ToString().Trim();
            RowView[3] = rDetail["CHUCVU"].ToString().Trim();
            //double zTotalRow = 0;
            int nCol = 4;
            for (int i = 6; i < rDetail.ItemArray.Length; i++)
            {
                if (rDetail[i].ToString() == "00" || rDetail[i].ToString() == "0")
                    RowView[nCol] = "";
                else
                    RowView[nCol] = rDetail[i].ToString();
                //zTotalRow += rDetail[i].ToDouble();
                nCol++;
            }
            //double zTotalRow = 0;
            //for (int i = 5; i < rDetail.ItemArray.Length; i++)
            //{
            //    zTotalRow += rDetail[i].ToDouble();
            //}

            //RowView[nCol] = zTotalRow.Toe0String();
        }
        private void TotalRow(Row RowView, DataTable Table, int No)
        {
            RowView[0] = "";
            RowView[1] = "TỔNG CỘNG ";
            RowView[2] = "Số nhân sự " + Table.Rows.Count;
            RowView[3] = "";
            //float zTotal = 0;
            //int nCol = 3;
            //for (int i = 5; i < Table.Columns.Count; i++)
            //{
            //    RowView[nCol] = Table.Compute("SUM([" + Table.Columns[i].ColumnName + "])", "").Toe0String();
            //    zTotal += RowView[nCol].ToFloat();
            //    nCol++;
            //}
            //RowView[nCol] = zTotal.Toe0String(); ;
            RowView.StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);

        }
        private void Btn_Export_Click(object sender, EventArgs e)
        {

            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Báo_Cáo_Chấm_Công-Chưa-Chốt_ Tháng" + dte_Month.Value.ToString("MM_yyyy") + ".xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }

                    string Status = "Xuất Excel form " + HeaderControl.Text + " > đường dẫn và tên file xuất:" + FDialog.FileName;
                    Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);

                    GVData.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }
        }
        private void Btn_Done_Click(object sender, EventArgs e)
        {

            if (GVData.Rows.Count > 0)
            {
                if (SessionUser.Date_Lock >= dte_Month.Value)
                {
                    Utils.TNMessageBoxOK("Thời gian này đã khóa sổ.!", 2);
                    return;
                }
                string Status = "Chốt số liệu chấm công nhân sự > tháng:" + dte_Month.Value.ToString("MM/yyyy") + " > Khối:" + cbo_Branch.Text + " >Bộ phận:" + cbo_Department.Text + " > Nhóm:" + cbo_Team.Text;


                if (Utils.TNMessageBox("Bạn có chắc chốt dữ liệu này ?.", 2) == "Y")
                {

                    Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, "Khởi động " + Status, 3);

                    TimeKeeping_Month_Close_Info zInfo;
                    zInfo = new TimeKeeping_Month_Close_Info();
                    zInfo.GetParent_Info(_ToDate);
                    zInfo.Parent = 0;
                    zInfo.DateWrite = _ToDate;
                    zInfo.CreatedBy = SessionUser.UserLogin.Key;
                    zInfo.CreatedName = SessionUser.UserLogin.EmployeeName;
                    zInfo.ModifiedBy = SessionUser.UserLogin.Key;
                    zInfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
                    zInfo.Save(); //lưu cha
                    if (zInfo.Message.Substring(0, 2) == "11" || zInfo.Message.Substring(0, 2) == "20")
                    {
                        int zKey = zInfo.Key;
                        zInfo = new TimeKeeping_Month_Close_Info();
                        zInfo.CreateClose(_ToDate, _BranchKey, _DepartmentKey, _TeamKey, SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeName, zKey);
                        if (zInfo.Message != "11")
                        {
                            Utils.TNMessageBoxOK("Lỗi. Vui lòng liên hệ IT! /n Chi tiết:" + zInfo.Message, 4);
                            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status + " .Lỗi", 3);
                        }
                        else
                        {
                            Utils.TNMessageBoxOK("Lưu dữ liệu đã chốt thành công!", 3);
                            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status + " .Thành công", 3);
                        }
                    }
                }
            }
            else
            {
                Utils.TNMessageBoxOK("Không tìm thấy dữ liệu!", 1);
            }

        }
        private void Btn_Save_Click(object sender, EventArgs e)
        {
            if (SessionUser.Date_Lock >= dte_Month.Value)
            {
                Utils.TNMessageBoxOK("Thời gian này đã khóa sổ.!", 2);
                return;
            }
            if (rdo_Save.Checked)
            {
                Save();
            }
            if (rdo_Clear.Checked)
            {
                Clear();
            }
        }
        private void Save()
        {
            if (GVData.Rows.Count > 0)
            {
                string Status = "Chốt số liệu công tích lũy > tháng:" + dte_Month.Value.ToString("MM/yyyy") + " > Khối:" + cbo_Branch.Text + " >Bộ phận:" + cbo_Department.Text + " > Nhóm:" + cbo_Team.Text;
                if (Utils.TNMessageBox("Bạn có chắc chốt dữ liệu này ?.", 2) == "Y")
                {
                    Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, "Khởi động " + Status, 3);
                    Score_Stock_Info zInfo;
                    int no = 0;
                    string zMessage = "";
                    zInfo = new Score_Stock_Info();
                    zInfo.DeleteEmpty(_BranchKey, _DepartmentKey, _TeamKey, _FromDate);
                    zMessage += zInfo.Message;
                    if (zMessage == string.Empty)
                    {
                        zInfo = new Score_Stock_Info();
                        zInfo.LuuCongTichLuy(_BranchKey, _DepartmentKey, _TeamKey, _FromDate, SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeName);
                        if (zMessage == string.Empty)
                        {
                            Utils.TNMessageBoxOK("Lưu dữ liệu đã chốt thành công!", 3);
                            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status + " .Thành công", 3);
                        }
                        else
                        {
                            Utils.TNMessageBoxOK("Lỗi. Vui lòng liên hệ IT! /n Chi tiết:" + zInfo.Message, 4);
                            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status + " .Lỗi", 3);
                        }
                    }
                    else
                    {
                        Utils.TNMessageBoxOK("Lỗi. Vui lòng liên hệ IT! /n Chi tiết:" + zMessage, 4);
                    }
                }

            }
            else
            {
                MessageBox.Show("Không tìm thấy dữ liệu");
            }
        }
        private void Clear()
        {
            if (GVData.Rows.Count > 0)
            {
                string Status = "Chốt số liệu hủy công tích lũy > tháng:" + dte_Month.Value.ToString("MM/yyyy") + " > Khối:" + cbo_Branch.Text + " >Bộ phận:" + cbo_Department.Text + " > Nhóm:" + cbo_Team.Text;

                if (Utils.TNMessageBox("Bạn có chắc chốt hủy công ?.", 2) == "Y")
                {
                    Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, "Khởi động " + Status, 3);

                    Score_Stock_Info zInfo;
                    string zMessage = "";
                    zInfo = new Score_Stock_Info();
                    zInfo.DeleteEmpty(_BranchKey, _DepartmentKey, _TeamKey, _FromDate);
                    zMessage += zInfo.Message;
                    if (zMessage == string.Empty)
                    {
                        zInfo = new Score_Stock_Info();
                        zInfo.HuyCongTichLuy(_BranchKey, _DepartmentKey, _TeamKey, _FromDate, SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeName);
                        if (zMessage == string.Empty)
                        {
                            Utils.TNMessageBoxOK("Lưu dữ liệu đã chốt thành công!", 3);
                            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status + " .Thành công", 3);
                        }
                        else
                        {
                            Utils.TNMessageBoxOK("Lỗi. Vui lòng liên hệ IT! /n Chi tiết:" + zMessage, 4);
                            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status + " .Lỗi", 3);
                        }
                    }
                    else
                    {
                        Utils.TNMessageBoxOK("Lỗi. Vui lòng liên hệ IT! /n Chi tiết:" + zMessage, 4);
                    }
                }

            }
            else
            {
                Utils.TNMessageBoxOK("Không tìm thấy dữ liệu", 1);
            }
        }
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                //this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion
        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
                btn_Export.Enabled = false;
            }
            if (Role.RoleAdd == 0 && Role.RoleEdit == 0)
            {
                btn_Done.Enabled = false;
                btn_Save.Enabled = false;
            }
        }
        #endregion

    }
}
