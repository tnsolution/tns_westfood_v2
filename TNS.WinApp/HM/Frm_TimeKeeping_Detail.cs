﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.HRM;
using TNS.Misc;

namespace TNS.WinApp
{
    public partial class Frm_TimeKeeping_Detail : Form
    {
        public string _EmployeeID = "";
        public DateTime _DateWrite;
        public string _EmployeeName = "";
        public Frm_TimeKeeping_Detail()
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            Utils.DoubleBuffered(GVData, true);
            Utils.DrawGVStyle(ref GVData);
            btnMini.Click += btnMini_Click;
            //btnMax.Click += btnMax_Click;
            btnClose.Click += btnClose_Click;
        }

        private void Frm_TimeKeeping_Detail_Load(object sender, EventArgs e)
        {
            txt_EmployeeName.Text = _EmployeeName;
            txt_EmployeeID.Text = _EmployeeID;
            txt_Month.Text = _DateWrite.ToString("MM/yyyy");
            DataTable zTb = Employee_KeepingTime_Data.TimeKeepingDetail(_EmployeeID, _DateWrite);
            InitGV_Layout(GVData);
            InitGV_Data(GVData, zTb);
        }
        void InitGV_Layout(DataGridView GV)
        {
            GVData.Rows.Clear();
            GVData.Columns.Clear();

            // Setup Column 
            GV.Columns.Add("No", "STT");
            GV.Columns.Add("DateWrite", "Ngày");
            GV.Columns.Add("Day", "Thứ");
            GV.Columns.Add("BeginTime", "Giờ vào");
            GV.Columns.Add("EndTime", "Giờ Ra");
            GV.Columns.Add("OffTime", "Nghỉ trưa");
            GV.Columns.Add("TotalTime", "Tổng giờ");
            GV.Columns.Add("OverTime", "Tăng ca");
            GV.Columns.Add("TimeID", "Công");
            GV.Columns.Add("RicePlus", "Cơm");
            GV.Columns.Add("RiceMinus", "Trừ cơm");

            GV.Columns["No"].Width = 40;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV.Columns["DateWrite"].Width = 90;
            GV.Columns["DateWrite"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV.Columns["Day"].Width = 90;
            GV.Columns["Day"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["BeginTime"].Width = 80;
            GV.Columns["BeginTime"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV.Columns["EndTime"].Width = 80;
            GV.Columns["EndTime"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV.Columns["OffTime"].Width = 80;
            GV.Columns["OffTime"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV.Columns["TotalTime"].Width = 80;
            GV.Columns["TotalTime"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV.Columns["OverTime"].Width = 80;
            GV.Columns["OverTime"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV.Columns["TimeID"].Width = 80;
            GV.Columns["TimeID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV.Columns["RicePlus"].Width = 80;
            GV.Columns["RicePlus"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV.Columns["RiceMinus"].Width = 80;
            GV.Columns["RiceMinus"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

        }

        void InitGV_Data(DataGridView GV, DataTable zTable)
        {
            int no = 0;
            GVData.Rows.Clear();
            float zHTotal = 0;
            float zMTotal = 0;
            foreach (DataRow nRow in zTable.Rows)
            {
                float zH = 0;
                float zM = 0;
                GV.Rows.Add();
                DataGridViewRow nRowView = GV.Rows[no];
                nRowView.Cells["No"].Value = (no + 1).ToString();
                DateTime zDateWrite = DateTime.Parse(nRow["DateWrite"].ToString());
                nRowView.Cells["DateWrite"].Value = zDateWrite.ToString("dd/MM/yyyy");
                nRowView.Cells["Day"].Value = DayName(zDateWrite.DayOfWeek.ToString());
                if(zDateWrite.DayOfWeek.ToString()=="Sunday")
                {
                    nRowView.DefaultCellStyle.BackColor = Color.Pink;
                }
                nRowView.Cells["BeginTime"].Value = nRow["BeginTime"].ToString().Trim();
                nRowView.Cells["EndTime"].Value = nRow["EndTime"].ToString().Trim();
                nRowView.Cells["OffTime"].Value = nRow["OffTime"].ToString().Trim();
                nRowView.Cells["TotalTime"].Value = nRow["TotalTime"].ToString().Trim();


                zH = nRow["TotalTime"].ToString().Trim().Substring(0,2).ToFloat();
                zM = nRow["TotalTime"].ToString().Trim().Substring(3, 2).ToFloat();
                if(zH >=8)
                {
                    nRowView.Cells["OverTime"].Value = (zH-8)+":"+zM;
                    zHTotal += (zH - 8);
                    zMTotal += zM;
                }
                else
                {
                    nRowView.Cells["OverTime"].Value = "0:0";
                }

                nRowView.Cells["TimeID"].Value = nRow["TimeID"].ToString().Trim();
                nRowView.Cells["RicePlus"].Value = nRow["RicePlus"].ToString().Trim();
                nRowView.Cells["RiceMinus"].Value = nRow["RiceMinus"].ToString().Trim();
                no++;
            }
            txt_Count.Text = no.ToString();
            float zTam = ((zHTotal * 60) + zMTotal) / 60;
            string []zTam1 = zTam.Ton1String().Split('.');
            txt_Over.Text = zTam1[0] + ":" + (zTam1[1].ToFloat()/10 * 60).ToString();
        }
        private string DayName(string Day)
        {
            if (Day == "Monday")
                return "Thứ 2";
            if (Day == "Tuesday")
                return "Thứ 3";
            if (Day == "Wednesday")
                return "Thứ 4";
            if (Day == "Thursday")
                return "Thứ 5";
            if (Day == "Friday")
                return "Thứ 6";
            if (Day == "Saturday")
                return "Thứ 7";
            else
                return "Chủ nhật";
        }
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}
