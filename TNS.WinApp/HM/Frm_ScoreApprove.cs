﻿using C1.Win.C1FlexGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.HRM;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_ScoreApprove : Form
    {
        private int _Key = 0;
        public Frm_ScoreApprove()
        {
            InitializeComponent();
            btn_Save.Click += Btn_Save_Click;
            btn_New.Click += Btn_New_Click;
            btn_Del.Click += Btn_Del_Click;
            btn_Search.Click += Btn_Search_Click;
            GVData.Click += GVData_List_Click;
            btn_Export.Click += Btn_Export_Click;

            btnMini.Click += btnMini_Click;
            btnClose.Click += btnClose_Click;
            btnMax.Click += btnMax_Click;
            txt_EmployeeID.Enter += Txt_EmployeeID_Enter;
            txt_EmployeeID.Leave += Txt_EmployeeID_Leave;
            txt_Borrow.Leave += Txt_Borrow_Leave;

            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;
        }

       

        private void Frm_ScoreBorrow_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            //Check_RoleForm();

            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            dteDate.Value = SessionUser.Date_Work;
            DisplayData();
            SetDefault();

        }


        private void LoadData()
        {

            ScoreApprove_Info zinfo = new ScoreApprove_Info(_Key);
            txt_EmployeeID.Tag = zinfo.EmployeeKey;
            txt_EmployeeID.Text = zinfo.EmployeeID;
            txt_EmployeeName.Text = zinfo.EmployeeName;
            float zBorrow = float.Parse(zinfo.Approve.ToString());
            txt_Borrow.Text = zBorrow.Ton1String();
            txt_Description.Text = zinfo.Description;
            dte_DateWrite.Value = zinfo.DateWrite;
            txt_Description.Text = zinfo.Description;

        }
        private void Txt_Borrow_Leave(object sender, EventArgs e)
        {
            double zAmount = 0;
            if (double.TryParse(txt_Borrow.Text.Trim(), out zAmount))
            {
                txt_Borrow.Text = zAmount.Toe1String();
                txt_Borrow.Focus();
                txt_Borrow.SelectionStart = txt_Borrow.Text.Length - 2;
            }
            else
            {
                Utils.TNMessageBoxOK("Lỗi!.Định dạng số.",1);
            }
        }

        private void SetDefault()
        {
            _Key = 0;
            txt_EmployeeID.Tag = null;
            txt_EmployeeID.Text = "";
            txt_EmployeeName.Text = "";
            txt_Borrow.Text = "0";
            txt_Description.Text = "";
            dte_DateWrite.Value = SessionUser.Date_Work;
            txt_Description.Text = "";
            txt_EmployeeID.Focus();
        }
        #region[Event]
        private void Btn_Save_Click(object sender, EventArgs e)
        {
            if (SessionUser.Date_Lock >= dte_DateWrite.Value)
            {
                Utils.TNMessageBoxOK("Thời gian này đã khóa sổ.!", 2);
                return;
            }
            if (txt_EmployeeID.Tag == null)
            {
                Utils.TNMessageBoxOK("Chưa nhập mã!",1);
                return;
            }
            float zBorrow =0;
            if (!float.TryParse(txt_Borrow.Text, out zBorrow))
            {
                Utils.TNMessageBoxOK("Vui lòng nhập đúng định dạng số!",1);
                return;
            }

            else
            {
                ScoreApprove_Info zinfo = new ScoreApprove_Info(_Key);
                zinfo.EmployeeKey = txt_EmployeeID.Tag.ToString();
                zinfo.EmployeeID = txt_EmployeeID.Text.Trim().ToUpper();
                zinfo.EmployeeName = txt_EmployeeName.Text.Trim().ToString();
                zinfo.Approve = float.Parse(txt_Borrow.Text.Trim());
                zinfo.Description = txt_Description.Text;
                zinfo.DateWrite = dte_DateWrite.Value;
                zinfo.CreatedBy = SessionUser.UserLogin.Key;
                zinfo.CreatedName = SessionUser.UserLogin.EmployeeName;
                zinfo.ModifiedBy = SessionUser.UserLogin.Key;
                zinfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
                zinfo.Save();
                if (zinfo.Message == "11" || zinfo.Message == "20")
                {
                    Utils.TNMessageBoxOK("Cập nhật thành công!",3);
                    _Key = zinfo.Key;
                    DisplayData();
                    LoadData();
                }
                else
                {
                    Utils.TNMessageBoxOK(zinfo.Message,4);
                }
            }


        }
        private void Btn_Del_Click(object sender, EventArgs e)
        {
            if (_Key == 0)
            {
                Utils.TNMessageBoxOK("Chưa chọn thông tin!",1);
            }
            else
            {
                ScoreApprove_Info zinfo = new ScoreApprove_Info(_Key);
                if (SessionUser.Date_Lock >= zinfo.DateWrite)
                {
                    Utils.TNMessageBoxOK("Thời gian này đã khóa sổ.!", 2);
                    return;
                }
                if (MessageBox.Show("Bạn có chắc xóa thông tin này ?.", "Thông báo", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {

                    zinfo.ModifiedBy = SessionUser.UserLogin.Key;
                    zinfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
                    zinfo.Delete();
                    if (zinfo.Message == "30")
                    {
                        Utils.TNMessageBoxOK("Đã xóa thành công!",3);
                        DisplayData();
                        SetDefault();
                    }
                    else
                    {
                        Utils.TNMessageBoxOK(zinfo.Message,4);
                    }

                }
            }

        }
        private void Btn_New_Click(object sender, EventArgs e)
        {
            SetDefault();
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            DisplayData();
        }
        private void DisplayData()
        {
            DataTable In_Table = new DataTable();
            In_Table = ScoreApprove_Data.Search_List(dteDate.Value, txt_Search.Text);
            InitGV_Layout(In_Table);
        }
        private void GVData_List_Click(object sender, EventArgs e)
        {
            string zKey = GVData.Rows[GVData.RowSel][6].ToString();
            if (zKey != null || zKey != "")
            {
                _Key = int.Parse(zKey);
                LoadData();
            }
            else
            {
                _Key = 0;
                LoadData();
            }

        }
        private void Btn_Export_Click(object sender, EventArgs e)
        {
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Xét_Duyệt_Công.xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }

                    string Status = "Xuất Excel form " + HeaderControl.Text + " > đường dẫn và tên file xuất:" + FDialog.FileName;
                     Application_Data.Save_LogApplication(SessionUser.UserLogin.Key,SessionUser.UserLogin.EmployeeKey, Status, 3);

                    GVData.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }

        }
        #endregion

        void InitGV_Layout(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            int TotalRow = TableView.Rows.Count + 1;
            int ToTalCol = 7;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(TotalRow);

            //Row Header
            GVData.Rows[0][0] = "Stt";
            GVData.Rows[0][1] = "Ngày";
            GVData.Rows[0][2] = "Họ tên";
            GVData.Rows[0][3] = "Số thẻ";
            GVData.Rows[0][4] = "Số công";
            GVData.Rows[0][5] = "Ghi chú";

            GVData.Rows[0][6] = ""; // ẩn cột này

            //Style         
            //GVData.AllowFreezing = AllowFreezingEnum.Both;
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.Custom;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.LeftCenter;
            GVData.Styles.Normal.WordWrap = true;

            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                DataRow rData = TableView.Rows[rIndex];

                GVData.Rows[rIndex + 1][0] = rIndex + 1;
                GVData.Rows[rIndex + 1][1] = rData[1];
                GVData.Rows[rIndex + 1][2] = rData[2];
                GVData.Rows[rIndex + 1][3] = rData[3];
                GVData.Rows[rIndex + 1][4] = rData[4];
                GVData.Rows[rIndex + 1][5] = rData[5];
                GVData.Rows[rIndex + 1][6] = rData[0];
            }



            //Freeze Row and Column                              
            GVData.Rows.Fixed = 1;
            GVData.Rows[0].TextAlign = TextAlignEnum.CenterCenter;
            GVData.Rows[0].Height = 40;

            GVData.Cols[0].Width = 30;
            GVData.Cols[1].Width = 80;
            GVData.Cols[2].Width = 150;
            GVData.Cols[3].Width = 70;
            GVData.Cols[4].Width = 60;
            GVData.Cols[5].Width = 60;
            GVData.Cols[6].Visible = false;

            GVData.Cols[4].TextAlign = TextAlignEnum.RightCenter;
        }
        private void Txt_EmployeeID_Leave(object sender, EventArgs e)
        {
            if (txt_EmployeeID.Text.Trim().Length < 1)
            {
                txt_EmployeeID.Text = "Nhập mã thẻ";
                txt_EmployeeID.ForeColor = Color.White;
            }
            else
            {
                Employee_Info zinfo = new Employee_Info();
                zinfo.GetEmployee(txt_EmployeeID.Text.Trim());
                if (zinfo.Key == "")
                {
                    txt_EmployeeID.Text = "Nhập mã thẻ";
                    txt_EmployeeID.ForeColor = Color.White;
                    txt_EmployeeID.Tag = null;
                }
                else
                {
                    txt_EmployeeID.Text = zinfo.EmployeeID;
                    txt_EmployeeName.Text = zinfo.FullName;
                    txt_EmployeeID.Tag = zinfo.Key;
                    txt_EmployeeID.BackColor = Color.Black;
                }
            }
        }
        private void Txt_EmployeeID_Enter(object sender, EventArgs e)
        {
            if (txt_EmployeeID.Text.Trim() == "Nhập mã thẻ")
            {
                txt_EmployeeID.Text = "";
                txt_EmployeeID.ForeColor = Color.Brown;
            }
        }
        //private void txt_Rank_KeyPress(object sender, KeyPressEventArgs e)
        //{

        //    if ((e.KeyChar >= '0' && e.KeyChar <= '9') || (Keys)e.KeyChar == Keys.Back || e.KeyChar == 46)
        //    {

        //        e.Handled = false;

        //    }
        //    else
        //    {
        //        e.Handled = true;
        //    }

        //}
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;



        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                //this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
                btn_Export.Enabled = false;
            }
            if (Role.RoleAdd == 0 && Role.RoleEdit == 0)
            {
                btn_New.Enabled = false;
                btn_Save.Enabled = false;
            }
            if (Role.RoleAdd == 0 && Role.RoleEdit == 1)
            {
                btn_New.Enabled = false;
                btn_Save.Enabled = true;
            }
            if (Role.RoleDel == 0)
            {
                btn_Del.Enabled = false;
            }
        }
        #endregion
    }
}
