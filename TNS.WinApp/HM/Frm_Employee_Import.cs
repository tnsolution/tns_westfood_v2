﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TNS.HRM;
using TNS.LOC;
using TNS.Misc;
using TN.Library.System;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Employee_Import : Form
    {
        private bool _IsPostback = false;
        private Employee_Object _Emp_Obj;
        public Frm_Employee_Import()
        {
            InitializeComponent();
            GV_Employee_Layout();
            btn_Often.Click += btn_Often_Click;
            btn_Up.Click += btn_Up_Click;
        }

        private void Frm_Employee_Import_Load(object sender, EventArgs e)
        {
            btn_Up.Visible = false;
        }
        private void btn_Often_Click(object sender, EventArgs e)
        {
            _IsPostback = false;
            OpenFileDialog zOpf = new OpenFileDialog();

            zOpf.InitialDirectory = @"C:\";
            zOpf.Title = "Browse Excel Files";

            zOpf.CheckFileExists = true;
            zOpf.CheckPathExists = true;

            zOpf.DefaultExt = "txt";
            zOpf.Filter = "All Files|*.*";
            zOpf.FilterIndex = 2;
            zOpf.RestoreDirectory = true;

            zOpf.ReadOnlyChecked = true;
            zOpf.ShowReadOnly = true;

            if (zOpf.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    txt_Excel.Text = zOpf.FileName;
                    System.IO.FileInfo zFileImport = new System.IO.FileInfo(zOpf.FileName);
                    txt_Excel.Tag = zFileImport.Name;
                    ExcelPackage package = new OfficeOpenXml.ExcelPackage(zFileImport);

                    foreach (var sheet in package.Workbook.Worksheets)
                    {
                        cbo_Sheet.Items.Add(sheet.Name);
                    }
                    package.Dispose();

                    cbo_Sheet.SelectedIndex = 0;
                    // DataTable zTable = OfficeOpenXml.FormulaParsing.Excel.ImportToDataTable(txt_Excel.Text, cbo_Sheet.Text);
                    DataTable In_Table = Data_Access.GetTable(txt_Excel.Text, cbo_Sheet.Text);
                    GV_Employee_LoadData(In_Table);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
            _IsPostback = true;
            btn_Up.Visible = true;
        }

        #region [GV_Employee]
        private void GV_Employee_Layout()
        {
            // Setup Column 
            GV_Employee.Columns.Add("No", "STT");
            GV_Employee.Columns.Add("Message", "Message");
            GV_Employee.Columns.Add("EmployeeName", "Tên Nhân Viên");
            GV_Employee.Columns.Add("EmployeeID", "Mã NV");
            GV_Employee.Columns.Add("Gender", "Mã Giới tính");
            GV_Employee.Columns.Add("BirthDay", "Ngày sinh");
            GV_Employee.Columns.Add("IssueID", "CMND");
            GV_Employee.Columns.Add("IssueDate", "Ngày cấp");
            GV_Employee.Columns.Add("IssuePlace", "Nơi cấp");
            GV_Employee.Columns.Add("HomeTown", "Nguyên quán");
            GV_Employee.Columns.Add("Branch", "Mã khối");
            //GV_Employee.Columns.Add("BranchName", " Khối");
            GV_Employee.Columns.Add("Department", "Mã Bộ phận");
            //   GV_Employee.Columns.Add("DepartmentName", "Bộ phận");
            GV_Employee.Columns.Add("Team", "Mã nhóm");
            // GV_Employee.Columns.Add("TeamName", "Tên nhóm");
            GV_Employee.Columns.Add("Position", "Mã chức vụ");
            // GV_Employee.Columns.Add("PositionName", "Chức vụ");

            GV_Employee.Columns["No"].Width = 40;
            GV_Employee.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV_Employee.Columns["No"].ReadOnly = true;
            GV_Employee.Columns["No"].Frozen = true;

            GV_Employee.Columns["EmployeeName"].Width = 160;
            GV_Employee.Columns["EmployeeName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV_Employee.Columns["EmployeeName"].ReadOnly = true;
            GV_Employee.Columns["EmployeeName"].Frozen = true;

            GV_Employee.Columns["EmployeeID"].Width = 100;
            GV_Employee.Columns["EmployeeID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV_Employee.Columns["EmployeeID"].Frozen = true;

            GV_Employee.Columns["HomeTown"].Width = 400;
            GV_Employee.Columns["HomeTown"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;


            // setup style view

            GV_Employee.BackgroundColor = Color.White;
            GV_Employee.GridColor = Color.FromArgb(227, 239, 255);
            GV_Employee.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV_Employee.DefaultCellStyle.ForeColor = Color.FromArgb(43, 43, 43);
            GV_Employee.DefaultCellStyle.Font = new Font("Arial", 9, FontStyle.Bold);
            GV_Employee.AutoResizeColumns();

            GV_Employee.AllowUserToResizeRows = false;
            GV_Employee.AllowUserToResizeColumns = true;

            GV_Employee.RowHeadersVisible = false;
            GV_Employee.AllowUserToDeleteRows = false;
            //// setup Height Header
            //GV_Employee.ColumnHeadersHeight = GV_Employee.ColumnHeadersHeight * 3 / 2;
            GV_Employee.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            GV_Employee.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;
            GV_Employee.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 9, FontStyle.Bold);

        }
        private void GV_Employee_LoadData(DataTable In_Table)
        {
            GV_Employee.Rows.Clear();
            this.Cursor = Cursors.WaitCursor;
            for (int i = 0; i < In_Table.Rows.Count; i++)
            {
                DataRow zRow = In_Table.Rows[i];
                GV_Employee.Rows.Add();
                GV_Employee.Rows[i].Cells["No"].Value = (i + 1).ToString();
                GV_Employee.Rows[i].Cells["EmployeeName"].Value = zRow[1].ToString().Trim();
                GV_Employee.Rows[i].Cells["EmployeeID"].Value = zRow[2].ToString().Trim();
                GV_Employee.Rows[i].Cells["Gender"].Value = zRow[3].ToString().Trim();
                DateTime zBirtDay = DateTime.Parse(zRow[4].ToString().Trim());
                GV_Employee.Rows[i].Cells["BirthDay"].Value = zRow[4].ToString();
                GV_Employee.Rows[i].Cells["IssueID"].Value = zRow[5].ToString().Trim();
                GV_Employee.Rows[i].Cells["IssueDate"].Value = zRow[6].ToString().Trim();
                GV_Employee.Rows[i].Cells["IssuePlace"].Value = zRow[7].ToString().Trim();
                GV_Employee.Rows[i].Cells["HomeTown"].Value = zRow[8].ToString().Trim();
                GV_Employee.Rows[i].Cells["Branch"].Value = zRow[9].ToString().Trim();
                GV_Employee.Rows[i].Cells["BranchName"].Value = zRow[10].ToString().Trim();
                GV_Employee.Rows[i].Cells["Department"].Value = zRow[11].ToString().Trim();
                GV_Employee.Rows[i].Cells["DepartmentName"].Value = zRow[12].ToString().Trim();
                GV_Employee.Rows[i].Cells["Team"].Value = zRow[13].ToString().Trim();
                GV_Employee.Rows[i].Cells["TeamName"].Value = zRow[14].ToString().Trim();
                GV_Employee.Rows[i].Cells["Position"].Value = zRow[15].ToString().Trim();
                GV_Employee.Rows[i].Cells["PositionName"].Value = zRow[16].ToString().Trim();
            }
            txt_Amount_Excel.Text = In_Table.Rows.Count.ToString();
            this.Cursor = Cursors.Default;
        }
        private void GV_Employee_KeyDown(object sender, KeyEventArgs e)
        {

        }
        private void GV_Employee_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
           
        }
        private void Control_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = false;
        }
        private void GV_Employee_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
           
        }
        private void btn_Up_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            if (CheckBeforeSave() != 0)
            {
                MessageBox.Show("Dữ liệu không hợp lệ vui lòng kiểm tra lại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Cursor = Cursors.Default;
            }

            else
            {
                MessageBox.Show("Dữ liệu hợp lệ!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Save();
                this.Cursor = Cursors.Default;
            }
          

        }
        private void Save()
        {
            string zMessage = "";
            int zcount = 0;
            for (int i = 0; i < GV_Employee.Rows.Count - 1; i++)
            {
                GV_Employee.Rows[i].DefaultCellStyle.BackColor = Color.White;
                string zError = "";
                if (GV_Employee.Rows[i].Cells["EmployeeID"].Value != null && GV_Employee.Rows[i].Visible == true)
                {
                    string zEmployeeID = GV_Employee.Rows[i].Cells["EmployeeID"].Value.ToString().Trim();
                    Branch_Info zbranch = new Branch_Info();
                    zbranch.Get_Branch_ID((GV_Employee.Rows[i].Cells["Branch"].Value.ToString().Trim()));
                    Department_Info zDepartment = new Department_Info();
                    zDepartment.Get_Department_ID((GV_Employee.Rows[i].Cells["Department"].Value.ToString().Trim()));
                    Team_Info zTeam = new Team_Info();
                    zTeam.Get_Team_ID((GV_Employee.Rows[i].Cells["Team"].Value.ToString().Trim()));
                    Position_Info zPosition = new Position_Info();
                    zPosition.Get_Position_ID((GV_Employee.Rows[i].Cells["Position"].Value.ToString().Trim()));


                    Employee_Info zinfo = new Employee_Info();
                    zinfo.GetEmployeeID(zEmployeeID);
                    _Emp_Obj = new Employee_Object(zinfo.Key);
                    _Emp_Obj.EmployeeID = GV_Employee.Rows[i].Cells["EmployeeID"].Value.ToString().Trim();
                    _Emp_Obj.BranchKey = zbranch.Key;
                    _Emp_Obj.DepartmentKey = zDepartment.Key;
                    _Emp_Obj.TeamKey = zTeam.Key;
                    _Emp_Obj.PositionKey = zPosition.Key;
                    _Emp_Obj.CreatedBy = SessionUser.UserLogin.EmployeeKey;
                    _Emp_Obj.CreatedName = SessionUser.UserLogin.EmployeeName;

                    _Emp_Obj.ModifiedBy = SessionUser.UserLogin.EmployeeKey;
                    _Emp_Obj.ModifiedName = SessionUser.UserLogin.EmployeeName;

                    _Emp_Obj.PersoObject.FullName = GV_Employee.Rows[i].Cells["EmployeeName"].Value.ToString().Trim();
                    _Emp_Obj.PersoObject.Name = GV_Employee.Rows[i].Cells["EmployeeName"].Value.ToString().Trim();
                    _Emp_Obj.PersoObject.CardID = GV_Employee.Rows[i].Cells["EmployeeID"].Value.ToString().Trim();
                    _Emp_Obj.PersoObject.Gender = int.Parse(GV_Employee.Rows[i].Cells["Gender"].Value.ToString().Trim());
                    _Emp_Obj.PersoObject.BirthDay = DateTime.Parse(GV_Employee.Rows[i].Cells["BirthDay"].Value.ToString().Trim());
                    _Emp_Obj.PersoObject.IssueID = GV_Employee.Rows[i].Cells["IssueID"].Value.ToString().Trim();
                    _Emp_Obj.PersoObject.IssueDate = DateTime.Parse(GV_Employee.Rows[i].Cells["IssueDate"].Value.ToString().Trim());
                    _Emp_Obj.PersoObject.HomeTown = GV_Employee.Rows[i].Cells["HomeTown"].Value.ToString().Trim();
                    _Emp_Obj.PersoObject.ModifiedBy = SessionUser.UserLogin.EmployeeKey;
                    _Emp_Obj.PersoObject.ModifiedName = SessionUser.UserLogin.EmployeeName;
                    _Emp_Obj.SaveObject();
                    zError = TN_Message.Show(_Emp_Obj.Message);
                    if (zError != "")
                    {
                        GV_Employee.Rows[i].Cells["Message"].Value = zError;
                        GV_Employee.Rows[i].DefaultCellStyle.BackColor = Color.Tomato;
                    }
                    else
                    {
                        zcount++;
                    }
                      
                }
                zMessage += zError;
                txt_Amout_SQL.Text = zcount.ToString();
            }

        }
        string zMessage = "";
        int zError = 0;
        private int CheckBeforeSave()
        {
            zError = 0;

            for (int i = 0; i < GV_Employee.Rows.Count - 1; i++)
            {
                GV_Employee.Rows[i].DefaultCellStyle.BackColor = Color.White;
                string Message = "";
                if ((GV_Employee.Rows[i].Cells["EmployeeName"].Value == null || GV_Employee.Rows[i].Cells["EmployeeName"].Value == "") && GV_Employee.Rows[i].Visible == true)
                {
                    Message += "Tên nhân viên không bỏ trống /";
                }
                if ((GV_Employee.Rows[i].Cells["EmployeeID"].Value == null || GV_Employee.Rows[i].Cells["EmployeeID"].Value == "") && GV_Employee.Rows[i].Visible == true)
                {
                    Message += "Mã nhân viên không bỏ trống /";
                }
                if ((GV_Employee.Rows[i].Cells["Gender"].Value == null || GV_Employee.Rows[i].Cells["Gender"].Value == "") && GV_Employee.Rows[i].Visible == true)
                {
                    Message += "Giói tính không hợp lệ /";
                }
                else
                {
                    if (GV_Employee.Rows[i].Cells["Gender"].Value.ToInt() < 0 && GV_Employee.Rows[i].Cells["Gender"].Value.ToInt() > 1)
                    {
                        Message += "Giói tính không hợp lệ /";
                    }
                }
                if ((GV_Employee.Rows[i].Cells["BirthDay"].Value == null || GV_Employee.Rows[i].Cells["BirthDay"].Value == "") && GV_Employee.Rows[i].Visible == true)
                {
                    Message += "Ngày sinh không hợp lệ /";
                }
                else if ((GV_Employee.Rows[i].Cells["BirthDay"].Value != null || GV_Employee.Rows[i].Cells["BirthDay"].Value != "") && GV_Employee.Rows[i].Visible == true)
                {
                    int day = 0;
                    int month = 0;
                    int year = 0;
                    if ((GV_Employee.Rows[i].Cells["BirthDay"].Value.ToString().Trim().Length < 10))
                    {
                        Message += "Ngày sinh không hợp lệ /";
                    }
                    else
                    {
                        if (!int.TryParse(GV_Employee.Rows[i].Cells["BirthDay"].Value.ToString().Trim().Substring(0, 2), out day))
                        {
                            Message += "Ngày sinh không hợp lệ /";
                        }
                        else
                        {
                            if (!int.TryParse(GV_Employee.Rows[i].Cells["BirthDay"].Value.ToString().Trim().Substring(3, 2), out month))
                            {
                                Message += "Ngày sinh không hợp lệ /";
                            }
                            else
                            {
                                if (!int.TryParse(GV_Employee.Rows[i].Cells["BirthDay"].Value.ToString().Trim().Substring(6, 4), out year))
                                {
                                    Message += "Ngày sinh không hợp lệ /";
                                }
                                else
                                {

                                    if (LaNgayHopLe(day, month, year) == false)
                                    {
                                        Message += "Ngày sinh không hợp lệ /";
                                    }
                                }
                            }
                        }
                    }


                }
                if ((GV_Employee.Rows[i].Cells["IssueID"].Value == null || GV_Employee.Rows[i].Cells["IssueID"].Value == "" || GV_Employee.Rows[i].Cells["IssueID"].Value.ToString().Trim().Length < 9) && GV_Employee.Rows[i].Visible == true)
                {
                    Message += "CMND không hợp lệ /";
                }
                if ((GV_Employee.Rows[i].Cells["IssueDate"].Value == null || GV_Employee.Rows[i].Cells["IssueDate"].Value == "" || GV_Employee.Rows[i].Cells["IssueID"].Value.ToString().Trim().Length < 9) && GV_Employee.Rows[i].Visible == true)
                {
                    Message += "Ngày cấp không hợp lệ /";
                }
                else if ((GV_Employee.Rows[i].Cells["IssueDate"].Value != null || GV_Employee.Rows[i].Cells["IssueDate"].Value != "") && GV_Employee.Rows[i].Visible == true)
                {
                    int day = 0;
                    int month = 0;
                    int year = 0;
                    if ((GV_Employee.Rows[i].Cells["IssueDate"].Value.ToString().Trim().Length < 10))
                    {
                        Message += "Ngày cấp không hợp lệ /";
                    }
                    else
                    {
                        if (!int.TryParse(GV_Employee.Rows[i].Cells["IssueDate"].Value.ToString().Trim().Substring(0, 2), out day))
                        {
                            Message += "Ngày cấp không hợp lệ /";
                        }
                        else
                        {
                            if (!int.TryParse(GV_Employee.Rows[i].Cells["IssueDate"].Value.ToString().Trim().Substring(3, 2), out month))
                            {
                                Message += "Ngày cấp không hợp lệ /";
                            }
                            else
                            {
                                if (!int.TryParse(GV_Employee.Rows[i].Cells["IssueDate"].Value.ToString().Trim().Substring(6, 4), out year))
                                {
                                    Message += "Ngày cấp không hợp lệ /";
                                }
                                else
                                {

                                    if (LaNgayHopLe(day, month, year) == false)
                                    {
                                        Message += "Ngày cấp không hợp lệ /";
                                    }
                                }
                            }
                        }

                    }
                }
                if ((GV_Employee.Rows[i].Cells["IssuePlace"].Value == null && GV_Employee.Rows[i].Visible == true))
                {
                    GV_Employee.Rows[i].Cells["IssuePlace"].Value = "";
                }
                if ((GV_Employee.Rows[i].Cells["HomeTown"].Value == null && GV_Employee.Rows[i].Visible == true))
                {
                    GV_Employee.Rows[i].Cells["HomeTown"].Value = "";
                }
                if ((GV_Employee.Rows[i].Cells["Branch"].Value == null || GV_Employee.Rows[i].Cells["Branch"].Value == "") && GV_Employee.Rows[i].Visible == true)
                {
                    Message += "Khối không hợp lệ /";
                }
                else
                {
                    Branch_Info zinfo = new Branch_Info();
                    zinfo.Get_Branch_ID((GV_Employee.Rows[i].Cells["Branch"].Value.ToString().Trim()));
                    if (zinfo.Key == 0)
                    {
                        Message += "Khối không hợp lệ /";
                    }
                }
                if ((GV_Employee.Rows[i].Cells["Department"].Value == null || GV_Employee.Rows[i].Cells["Department"].Value == "") && GV_Employee.Rows[i].Visible == true)
                {
                    Message += "Phòng ban không hợp lệ /";
                }
                else
                {
                    Department_Info zinfo = new Department_Info();
                    zinfo.Get_Department_ID((GV_Employee.Rows[i].Cells["Department"].Value.ToString().Trim()));
                    if (zinfo.Key == 0)
                    {
                        Message += "Phòng ban không hợp lệ /";
                    }
                }
                if ((GV_Employee.Rows[i].Cells["Team"].Value == null || GV_Employee.Rows[i].Cells["Team"].Value == "") && GV_Employee.Rows[i].Visible == true)
                {
                    Message += "Tổ nhóm không hợp lệ /";
                }
                else
                {
                    Team_Info zinfo = new Team_Info();
                    zinfo.Get_Team_ID((GV_Employee.Rows[i].Cells["Team"].Value.ToString().Trim()));
                    if (zinfo.Key == 0)
                    {
                        Message += "Tổ nhóm không hợp lệ /";
                    }
                }
                if ((GV_Employee.Rows[i].Cells["Position"].Value == null || GV_Employee.Rows[i].Cells["Position"].Value == "") && GV_Employee.Rows[i].Visible == true)
                {
                    Message += "Chức vụ không hợp lệ /";
                }
                else
                {
                    Position_Info zinfo = new Position_Info();
                    zinfo.Get_Position_ID((GV_Employee.Rows[i].Cells["Position"].Value.ToString().Trim()));
                    if (zinfo.Key == 0)
                    {
                        Message += "Chức vụ không hợp lệ /";
                    }
                }
                if (Message != "")
                {
                    GV_Employee.Rows[i].Cells["Message"].Value = Message;
                    GV_Employee.Rows[i].DefaultCellStyle.BackColor = Color.Tomato;
                    zError++;
                }
                else
                {
                    GV_Employee.Rows[i].Cells["Message"].Value = "";
                }
            }
            txt_Error.Text = zError.ToString();
            return zError;
        }

        #endregion

        #region[Kiểm tra ngày cho trước có hợp lệ không]
        bool LaNamNhuan(int nYear)
        {
            if ((nYear % 4 == 0 && nYear % 100 != 0) || nYear % 400 == 0)
            {
                return true;
            }
            return false;
        }
        int TinhSoNgayTrongThang(int nMonth, int nYear)
        {
            int nNumOfDays = 0;

            switch (nMonth)
            {
                case 1:
                case 3:
                case 5:
                case 7:
                case 8:
                case 10:
                case 12:
                    nNumOfDays = 31;
                    break;
                case 4:
                case 6:
                case 9:
                case 11:
                    nNumOfDays = 30;
                    break;
                case 2:
                    if (LaNamNhuan(nYear))
                    {
                        nNumOfDays = 29;
                    }
                    else
                    {
                        nNumOfDays = 28;
                    }
                    break;
            }

            return nNumOfDays;
        }

        bool LaNgayHopLe(int nDay, int nMonth, int nYear)
        {
            // Kiểm tra năm < 0 , thêm dkien nhỏ hơn 1900
            if (nYear < 1 || nYear < 1900)
            {
                return false; // Ngày không còn hợp lệ nữa!
            }
            // Kiểm tra tháng
            if (nMonth < 1 || nMonth > 12)
            {
                return false; // Ngày không còn hợp lệ nữa!
            }
            // Kiểm tra ngày
            if (nDay < 1 || nDay > TinhSoNgayTrongThang(nMonth, nYear))
            {
                return false; // Ngày không còn hợp lệ nữa!
            }
            return true; // Trả về trạng thái cuối cùng...
        }
        #endregion
    }
}
