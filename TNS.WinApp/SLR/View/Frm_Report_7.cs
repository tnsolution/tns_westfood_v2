﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.CORE;
using TNS.Misc;
using TNS.SLR;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Report_7 : Form
    {
        private int _TeamKeyClose = 0;
        private DateTime _DateClose;
        public Frm_Report_7()
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            Utils.DoubleBuffered(GVData, true);
            Utils.DrawGVStyle(ref GVData);
            btn_Search.Click += Btn_Search_Click;
            btnClose.Click += btnClose_Click;
            btnMax.Click += btnMax_Click;
            btnMini.Click += btnMini_Click;
            dteDate.Value = SessionUser.Date_Work;
            btn_Done.Click += Btn_Done_Click;
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            //Lấy tất cả nhóm trực tiếp sx  và khác bộ phận hỗ trợ sx
            string zSQL = @"SELECT TeamKey,TeamID +'-'+ TeamName AS TeamName  FROM SYS_Team 
                            WHERE RecordStatus <> 99 AND BranchKey=4 AND  DepartmentKey != 26
                            ORDER BY Rank";

            LoadDataToToolbox.KryptonComboBox(cboTeam, zSQL, "---Tất cả---");

        }

        private void Frm_Report_7_Load(object sender, EventArgs e)
        {
            DataTable ztb = Report.ListCodeReportWorker();
            InitGV_Layout(GVData, ztb);

        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            if (cboTeam.SelectedIndex >= 0)
                _TeamKeyClose = cboTeam.SelectedValue.ToInt();
            _DateClose = dteDate.Value;
            GVData.Rows.Clear();
            try
            {
                using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }
            }
            catch (Exception ex)
            {
                Utils.TNMessageBox("Thông báo", ex.ToString(), 4);
            }
        }
        private void DisplayData()
        {
            DataTable _Intable = Report.No7(_DateClose, _TeamKeyClose);
            if (_Intable.Rows.Count > 0)
            {
                this.Invoke(new MethodInvoker(delegate ()
                {
                    // InitGV_Layout(GVData, _Intable);

                    int NoGroup = 1;
                    DataGridViewRow zGvRow;
                    int TeamKey = _Intable.Rows[0]["TeamKey"].ToInt();
                    string TeamName = _Intable.Rows[0]["TeamName"].ToString();
                    DataRow[] Array = _Intable.Select("TeamKey=" + TeamKey);
                    if (Array.Length > 0)
                    {
                        DataTable zGroup = Array.CopyToDataTable();
                        GVData.Rows.Add();
                        zGvRow = GVData.Rows[0];
                        HeaderRow(zGvRow, zGroup, TeamKey, TeamName, 0);
                    }
                    for (int i = 0; i < _Intable.Rows.Count; i++)
                    {
                        DataRow r = _Intable.Rows[i];
                        if (TeamKey != r["TeamKey"].ToInt())
                        {
                            #region [GROUP]
                            TeamKey = r["TeamKey"].ToInt();
                            TeamName = r["TeamName"].ToString();
                            Array = _Intable.Select("TeamKey=" + TeamKey);
                            if (Array.Length > 0)
                            {
                                DataTable zGroup = Array.CopyToDataTable();
                                GVData.Rows.Add();
                                zGvRow = GVData.Rows[i + NoGroup];
                                HeaderRow(zGvRow, zGroup, TeamKey, TeamName, i + NoGroup);
                            }
                            #endregion
                            NoGroup++;
                        }
                        GVData.Rows.Add();
                        zGvRow = GVData.Rows[i + NoGroup];
                        DetailRow(zGvRow, r, i + NoGroup);
                    }

                    zGvRow = GVData.Rows[GVData.Rows.Count - 1];
                    TotalRow(zGvRow, _Intable, GVData.Rows.Count - 1);
                }));
            }
           
        }
        void InitGV_Layout(DataGridView GV, DataTable Table)
        {
            GVData.Rows.Clear();
            GVData.Columns.Clear();

            // Setup Column 
            GV.Columns.Add("No", "STT");
            GV.Columns.Add("EmployeeName", "HỌ VÀ TÊN");
            GV.Columns.Add("TeamID", "BỘ PHẬN");
            GV.Columns.Add("EmployeeID", "SỐ THẺ");
            GV.Columns.Add("ATM", "TÀI KHOẢN");
            int ztbRow = 0;
            for (int i = 5; i <= Table.Rows.Count + 4; i++)
            {

                GV.Columns.Add(i.ToString(), Table.Rows[ztbRow][1].ToString().Trim());
                GV.Columns[i.ToString()].Width = 100;
                GV.Columns[i.ToString()].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                if (i < 6)
                {
                    GV.Columns[i.ToString()].Frozen = true;
                }

                ztbRow++;
            }
            //GV.Columns.Add("Total", "Tổng");

            #region
            GV.Columns["No"].Width = 40;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV.Columns["No"].Frozen = true;
            GV.Columns["No"].ReadOnly = true;

            GV.Columns["EmployeeName"].Width = 180;
            GV.Columns["EmployeeName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["EmployeeName"].Frozen = true;

            GV.Columns["EmployeeID"].Width = 80;
            GV.Columns["EmployeeID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["EmployeeID"].Frozen = true;

            GV.Columns["TeamID"].Width = 140;
            GV.Columns["TeamID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["TeamID"].Frozen = true;


            GV.Columns["ATM"].Width = 120;
            GV.Columns["ATM"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["ATM"].Frozen = true;

            //GV.Columns["Total"].Width = 120;
            //GV.Columns["Total"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            //GV.Columns["Total"].DefaultCellStyle.Font = new Font("Arial", 9, FontStyle.Bold);

            GV.ColumnHeadersHeight = 65;
            #endregion
        }

        private void HeaderRow(DataGridViewRow RowView, DataTable Table, int TeamKey, string TeamName, int No)
        {
            RowView.Cells["No"].Value = (No + 1).ToString();
            RowView.Cells["EmployeeName"].Tag = "";
            RowView.Cells["EmployeeName"].Value = TeamName;
            RowView.Cells["TeamID"].Value = "Số công nhân " + Table.Select("TeamKey=" + TeamKey).Length;
            RowView.Cells["ATM"].Value = "";
            int nCol = 7;
            float zTotal = 0;
            for (int i = 5; i < RowView.Cells.Count; i++)
            {
                RowView.Cells[i].Value = Table.Compute("SUM([" + Table.Columns[nCol].ColumnName + "])", "TeamKey=" + TeamKey).Ton0String();
                zTotal += RowView.Cells[i].Value.ToFloat();
                nCol++;
            }

            RowView.DefaultCellStyle.Font = new Font("Tahoma", 9, FontStyle.Bold | FontStyle.Italic);
            RowView.DefaultCellStyle.BackColor = Color.LemonChiffon;
        }
        private void DetailRow(DataGridViewRow RowView, DataRow rDetail, int No)
        {
            RowView.Cells["No"].Value = (No).ToString();
            RowView.Cells["EmployeeName"].Tag = rDetail["EmployeeKey"].ToString().Trim();
            RowView.Cells["EmployeeName"].Value = rDetail["EmployeeName"].ToString().Trim();
            RowView.Cells["TeamID"].Value = rDetail["TeamID"].ToString().Trim();
            RowView.Cells["EmployeeID"].Value = rDetail["EmployeeID"].ToString().Trim();
            RowView.Cells["ATM"].Value = rDetail["ATM"].ToString().Trim();
            int nCol = 7;
            for (int i = 5; i < RowView.Cells.Count; i++)
            {
                RowView.Cells[i].Value = rDetail[nCol].Ton0String();
                nCol++;
            }

            //double zTotalRow = 0;
            // for (int i = 6; i < RowView.Cells.Count; i++)
            //{
            // zTotalRow += rDetail[i].ToDouble();
            // }

            //RowView.Cells["Total"].Value = zTotalRow.Ton0String();
        }
        private void TotalRow(DataGridViewRow RowView, DataTable Table, int No)
        {
            RowView.Cells["No"].Value = (No).ToString();
            RowView.Cells["EmployeeName"].Tag = "";
            RowView.Cells["EmployeeName"].Value = "Tổng ";
            RowView.Cells["EmployeeID"].Value = "";
            RowView.Cells["TeamID"].Value = "Số công nhân " + Table.Rows.Count;

            //float zTotal = 0;
            int nCol = 7;
            for (int i = 5; i < RowView.Cells.Count; i++)
            {
                RowView.Cells[i.ToString()].Value = Table.Compute("SUM([" + Table.Columns[nCol].ColumnName + "])", "").Ton0String();
                // zTotal += RowView.Cells[i.ToString()].Value.ToFloat();
                nCol++;
            }
            //RowView.Cells["Total"].Value = zTotal.Ton0String(); ;
            RowView.DefaultCellStyle.Font = new Font("Tahoma", 10, FontStyle.Bold | FontStyle.Italic);
            RowView.DefaultCellStyle.BackColor = Color.LightSkyBlue;
        }
        private void Btn_Done_Click(object sender, EventArgs e)
        {

            if (GVData.Rows.Count > 0)
            {
                if (MessageBox.Show("Bạn có chắc chốt dữ liệu này ?.", "Thông báo", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    ReportWorker_Close_Info zInfo;
                    zInfo = new ReportWorker_Close_Info();
                    zInfo.GetParent_Info(_DateClose);
                    zInfo.Parent = "";
                    zInfo.DateWrite = _DateClose;
                    zInfo.Save(); //lưu cha
                    if (zInfo.Message == "11" || zInfo.Message == "20")
                    {
                        string zKey = zInfo.Key;
                        zInfo = new ReportWorker_Close_Info();
                        zInfo.CreateClose(_DateClose, _TeamKeyClose, SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeName, zKey);
                        if (zInfo.Message != "11")
                        {
                            MessageBox.Show("Lỗi. Vui lòng liên hệ IT!");
                        }
                        else
                        {
                            MessageBox.Show("Lưu dữ liệu đã chốt thành công!");
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Không tìm thấy dữ liệu");
            }

        }
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}
