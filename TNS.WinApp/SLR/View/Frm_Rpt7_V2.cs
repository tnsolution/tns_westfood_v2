﻿using C1.Win.C1FlexGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.CORE;
using TNS.LOG;
using TNS.Misc;
using TNS.SLR;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Rpt7_V2 : Form
    {
        private int _TeamKeyClose = 0;
        private DateTime _DateClose;
        public Frm_Rpt7_V2()
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            btn_Search.Click += Btn_Search_Click;
            btnClose.Click += btnClose_Click;
            btnMax.Click += btnMax_Click;
            btnMini.Click += btnMini_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            btn_Export.Click += Btn_Export_Click;
            dteDate.Value = SessionUser.Date_Work;
            btn_Done.Click += Btn_Done_Click;
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            string zSQL = @"SELECT A.TeamKey,A.TeamID +'-'+ A.TeamName AS TeamName  FROM SYS_Team A
                            LEFT JOIN [dbo].[SYS_Department] B ON B.DepartmentKey=A.DepartmentKey
                            LEFT JOIN [dbo].[SYS_Branch] C ON C.BranchKey=B.BranchKey
                            WHERE A.RecordStatus <> 99 AND A.BranchKey=4 AND  A.DepartmentKey != 26 AND A.TeamKey <> 98 
                            ORDER BY C.RANK,B.RANK,A.RANK";
            LoadDataToToolbox.KryptonComboBox(cboTeam, zSQL, "--Tất cả--");
        }

        private void Frm_Rpt7_V2_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);

            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
        }
        
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            if (cboTeam.SelectedIndex >= 0)
                _TeamKeyClose = cboTeam.SelectedValue.ToInt();
            _DateClose = dteDate.Value;

            string Status = "Tìm DL form " + HeaderControl.Text + " > tháng: " + dteDate.Value.ToString("MM/yyyy") + " > Nhóm:" + cboTeam.Text + " > Từ khóa:" + txt_Search.Text;
             Application_Data.Save_LogApplication(SessionUser.UserLogin.Key,SessionUser.UserLogin.EmployeeKey, Status, 3);

            try
            {
                using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }
            }
            catch (Exception ex)
            {
                Utils.TNMessageBox("Thông báo", ex.ToString(), 4);
            }
        }
        private void DisplayData()
        {
            DataTable _Intable = Report.No7(_DateClose, _TeamKeyClose,txt_Search.Text);

            if (_Intable.Rows.Count > 0)
            {
                this.Invoke(new MethodInvoker(delegate ()
                {

                    InitGV_Layout(_Intable);
                    int NoGroup = 3;
                    int RowTam = NoGroup;
                    Row zGvRow;
                    int TeamKey = _Intable.Rows[0]["TeamKey"].ToInt();
                    string TeamName = _Intable.Rows[0]["TeamName"].ToString();
                    DataRow[] Array = _Intable.Select("TeamKey=" + TeamKey);
                    if (Array.Length > 0)
                    {
                        DataTable zGroup = Array.CopyToDataTable();
                        GVData.Rows.Add();
                        zGvRow = GVData.Rows[2];
                        HeaderRow(zGvRow, zGroup, TeamKey, TeamName, 2);
                    }
                    for (int i = 0; i < _Intable.Rows.Count; i++)
                    {
                        DataRow r = _Intable.Rows[i];
                        if (TeamKey != r["TeamKey"].ToInt())
                        {
                            #region [GROUP]
                            TeamKey = r["TeamKey"].ToInt();
                            TeamName = r["TeamName"].ToString();
                            Array = _Intable.Select("TeamKey=" + TeamKey);
                            if (Array.Length > 0)
                            {
                                DataTable zGroup = Array.CopyToDataTable();
                                GVData.Rows.Add();
                                zGvRow = GVData.Rows[i + NoGroup];
                                HeaderRow(zGvRow, zGroup, TeamKey, TeamName, i + NoGroup);
                            }
                            #endregion
                            NoGroup++;
                        }
                        GVData.Rows.Add();
                        zGvRow = GVData.Rows[i + NoGroup];
                        DetailRow(zGvRow, r, i + NoGroup - 1);
                        RowTam = i + NoGroup;
                    }

                    zGvRow = GVData.Rows[RowTam + 1];
                    TotalRow(zGvRow, _Intable, RowTam + 1);

                }));
            }

        }
        private void HeaderRow(Row RowView, DataTable Table, int TeamKey, string TeamName, int No)
        {
            RowView[0] = (No - 1).ToString();
            RowView[1] = TeamName;
            RowView[2] = "Số nhân sự " + Table.Select("TeamKey=" + TeamKey).Length;
            RowView[3] = "";
            int nCol = 5;
            float zTotal = 0;
            for (int i = 7; i < Table.Columns.Count; i++)
            {
                RowView[nCol] = Table.Compute("SUM([" + Table.Columns[i].ColumnName + "])", "TeamKey=" + TeamKey).Ton1String();
                float zTemp = 0;
                if (float.TryParse(RowView[nCol].ToString(), out zTemp))
                {

                }

                zTotal += zTemp;
                nCol++;
            }
            RowView.StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            //RowView.StyleNew.BackColor = Color.LemonChiffon;
        }
        private void DetailRow(Row RowView, DataRow rDetail, int No)
        {
            RowView[0] = (No).ToString();
            RowView[1] = rDetail["EmployeeName"].ToString().Trim();
            RowView[2] = rDetail["TeamID"].ToString().Trim();
            RowView[3] = rDetail["EmployeeID"].ToString().Trim();
            RowView[4] = rDetail["ATM"].ToString().Trim();
            int nCol = 5;
            for (int i = 7; i < rDetail.ItemArray.Length; i++)
            {
                RowView[nCol] = rDetail[i].Ton1String();
                nCol++;
            }

            //double zTotalRow = 0;
            // for (int i = 6; i < RowView.Cells.Count; i++)
            //{
            // zTotalRow += rDetail[i].ToDouble();
            // }

            //RowView.Cells["Total"].Value = zTotalRow.Ton0String();
        }
        private void TotalRow(Row RowView, DataTable Table, int No)
        {
            RowView[0] = (No - 1).ToString();
            RowView[1] = "Tổng ";
            RowView[2] = "Số nhân sự " + Table.Rows.Count;
            RowView[3] = "";

            //float zTotal = 0;
            int nCol = 5;
            for (int i = 7; i < Table.Columns.Count; i++)
            {
                RowView[nCol] = Table.Compute("SUM([" + Table.Columns[i].ColumnName + "])", "").Ton1String();
                // zTotal += RowView.Cells[i.ToString()].Value.ToFloat();
                nCol++;
            }
            RowView.StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            //RowView.StyleNew.BackColor = Color.LightSkyBlue;
            //RowView.DefaultCellStyle.BackColor = Color.LightSkyBlue;
        }

        void InitGV_Layout(DataTable Table)
        {

            GVData.Clear();

            //int TotalRow = 1 ;
            int ToTalCol = Table.Columns.Count - 2;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(3);
            //Row Header          

            CellRange zCellRange = GVData.GetCellRange(0, 0, 1, 0);
            zCellRange.Data = "STT";

            zCellRange = GVData.GetCellRange(0, 1, 1, 1);
            zCellRange.Data = "HỌ TÊN";

            zCellRange = GVData.GetCellRange(0, 2, 1, 2);
            zCellRange.Data = "BỘ PHẬN";

            zCellRange = GVData.GetCellRange(0, 3, 1, 3);
            zCellRange.Data = "SỐ THẺ";

            zCellRange = GVData.GetCellRange(0, 4, 1, 4);
            zCellRange.Data = "TÀI KHOẢN";

            int nCol = 7;
            for (int i = 5; i < ToTalCol; i++)
            {
                DataColumn Col = Table.Columns[nCol];
                string strCaption = Col.ColumnName.ToString();
                GVData.Rows[0][i] = strCaption.ToUpper();
                GVData.Rows[1][i] = strCaption.ToUpper();
                GVData.Cols[i].Width = 110;
                nCol++;
            }
            //Một số cột cần đổi tên
            GVData.Rows[0][10] = "KHOÁN SẢN PHẨM KHÁC"; // nhập name đầu tiền 23-30 thì nhập 1 text cột 23
            GVData.Rows[1][10] = "KHOÁN SẢN PHẨM";
            GVData.Rows[1][11] = "LÀM THÊM CHỦ NHẬT";
            GVData.Rows[1][12] = "LÀM THÊM NGÀY LỄ";

            GVData.Rows[0][22] = "TRỢ CẤP LĐ NỮ CÓ CON NHỎ (TỪ 1 - 6 TUỔI)";
            GVData.Rows[1][22] = "SỐ LƯỢNG";
            GVData.Rows[1][23] = "THÀNH TIỀN";

            GVData.Rows[0][24] = "TIỀN CƠM 16.000 Đ/NGÀY";
            GVData.Rows[1][24] = "SỐ NGÀY";
            GVData.Rows[1][25] = "THÀNH TIỀN";

            GVData.Rows[0][26] = "TIỀN CƠM 20.000 Đ/NGÀY";
            GVData.Rows[1][26] = "SỐ NGÀY";
            GVData.Rows[1][27] = "THÀNH TIỀN";

            GVData.Rows[0][29] = "TRỪ THU NHẬP";

            GVData.Rows.Fixed = 2;
            GVData.Cols.Frozen = 5;
            GVData.Cols[0].StyleNew.BackColor = Color.Empty;
            GVData.Cols[1].StyleNew.BackColor = Color.Empty;
            GVData.Cols[2].StyleNew.BackColor = Color.Empty;
            GVData.Cols[3].StyleNew.BackColor = Color.Empty;
            GVData.Cols[4].StyleNew.BackColor = Color.Empty;



            //Style
            //GVData.AutoSizeCols();
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.Custom;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.LeftCenter;
            GVData.Rows[0].Height = 60;
            GVData.Rows[0].StyleNew.WordWrap = true;
            GVData.Rows[0].TextAlign = TextAlignEnum.CenterCenter;
            GVData.Rows[1].Height = 60;
            GVData.Rows[1].StyleNew.WordWrap = true;
            GVData.Rows[1].TextAlign = TextAlignEnum.CenterCenter;

            ////
            GVData.Cols[0].Width = 40;
            GVData.Cols[1].Width = 200;
            GVData.Cols[2].Width = 120;
            GVData.Cols[3].Width = 100;
            GVData.Cols[4].Width = 100;

            GVData.MergedRanges.Add(0, 0, 1, 0);
            GVData.MergedRanges.Add(0, 1, 1, 1);
            GVData.MergedRanges.Add(0, 2, 1, 2);
            GVData.MergedRanges.Add(0, 3, 1, 3);
            GVData.MergedRanges.Add(0, 4, 1, 4);
            for (int i = 5; i < ToTalCol; i++)
            {
                if ((i >= 10 && i <= 12) || (i >= 22 && i < 28) || (i > 28 && i <= ToTalCol - 2))
                {

                }
                else
                {
                    GVData.MergedRanges.Add(0, i, 1, i);
                }
                GVData.Cols[i].TextAlign = TextAlignEnum.RightCenter;
            }
            GVData.MergedRanges.Add(0, 10, 0, 12);
            GVData.MergedRanges.Add(0, 22, 0, 23);
            GVData.MergedRanges.Add(0, 24, 0, 25);
            GVData.MergedRanges.Add(0, 26, 0, 27);
            GVData.MergedRanges.Add(0, 29, 0, ToTalCol - 2);

        }
        private void Btn_Done_Click(object sender, EventArgs e)
        {

            if (GVData.Rows.Count > 0)
            {
                if (MessageBox.Show("Bạn có chắc chốt dữ liệu này ?.", "Thông báo", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    ReportWorker_Close_Info zInfo;
                    zInfo = new ReportWorker_Close_Info();
                    zInfo.GetParent_Info(_DateClose);
                    zInfo.Parent = "";
                    zInfo.DateWrite = _DateClose;
                    zInfo.Save(); //lưu cha
                    if (zInfo.Message == "11" || zInfo.Message == "20")
                    {
                        string zKey = zInfo.Key;
                        zInfo = new ReportWorker_Close_Info();
                        zInfo.CreateClose(_DateClose, _TeamKeyClose, SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeName, zKey);
                        if (zInfo.Message != "11")
                        {
                            MessageBox.Show("Lỗi. Vui lòng liên hệ IT!");
                        }
                        else
                        {
                            MessageBox.Show("Lưu dữ liệu đã chốt thành công!");
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Không tìm thấy dữ liệu");
            }

        }
        private void Btn_Export_Click(object sender, EventArgs e)
        {
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "BaoCaoLuongBoPhanSanXuat-LuongChuaChot" + dteDate.Value.ToString("MM-yyyy") + ".xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        MessageBox.Show("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }

                    string Status = "Xuất Excel form " + HeaderControl.Text + " > đường dẫn và tên file xuất:" + FDialog.FileName;
                     Application_Data.Save_LogApplication(SessionUser.UserLogin.Key,SessionUser.UserLogin.EmployeeKey, Status, 3);


                    GVData.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }
            
        }
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}
