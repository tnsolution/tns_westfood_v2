﻿using C1.Win.C1FlexGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.CORE;
using TNS.LOG;
using TNS.Misc;
using TNS.SLR;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Rpt7_V3 : Form
    {
        private int _TeamKeyClose = 0;
        private int _DepartmentKeyClose = 0;
        private DateTime _DateClose;
        private int _STT = 0;
        private string _IDSalary = "";
        public Frm_Rpt7_V3()
        {
            InitializeComponent();
            btn_Done.Click += Btn_Done_Click;
            btn_Search.Click += Btn_Search_Click;
            btnClose.Click += btnClose_Click;
            btnMax.Click += btnMax_Click;
            btnMini.Click += btnMini_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            btn_Export.Click += Btn_Export_Click;
            btn_Fillter.Click += Btn_Fillter_Click;
            chk_All.CheckedChanged += Chk_All_CheckedChanged;
            cbo_Department.SelectedIndexChanged += Cbo_Department_SelectedIndexChanged;
            dteDate.Value = SessionUser.Date_Work;
            dteDate.ValueChanged += DteDate_ValueChanged;
            InitLayout_LV(LVEmployee);

            Utils.DrawLVStyle(ref LVEmployee);
            Utils.SizeLastColumn_LV(LVEmployee);
            LoadDataToToolbox.KryptonComboBox(cbo_Department, "SELECT DepartmentKey, DepartmentName FROM[dbo].[SYS_Department] WHERE DepartmentKey != 98  AND RecordStatus< 99 AND BranchKey= 4 AND DepartmentKey != 26 ", "---- Chọn tất cả----");


        }

        
        private void Frm_Rpt7_V3_Load(object sender, EventArgs e)
        {
            //if (SessionUser.UserLogin.Key != "4e5a9e53-9241-4c9b-a69f-e86a7ae3507f")
            //{
            //    Utils.TNMessageBoxOK("Trang đang nâng cấp.Vui lòng quay lại sau!", 1);
            //    this.Close();
            //}
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            Check_RoleForm();

            InitData(true);
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            panel_left.Visible = false;
        }
        private void DteDate_ValueChanged(object sender, EventArgs e)
        {
            InitData(true);
        }

        private void Cbo_Department_SelectedIndexChanged(object sender, EventArgs e)
        {

            LoadDataToToolbox.KryptonComboBox(cboTeam, "SELECT TeamKey,TeamName FROM SYS_Team WHERE TeamKey != 98 AND DepartmentKey = " + cbo_Department.SelectedValue.ToInt() + "  AND RecordStatus < 99 ORDER BY Rank", "---- Chọn tất cả ----");
        }
        private void Chk_All_CheckedChanged(object sender, EventArgs e)
        {
            if (chk_All.Checked == true)
                InitData(true);
            else
                InitData(false);
        }
        private void Btn_Fillter_Click(object sender, EventArgs e)
        {
            if (btn_Fillter.Tag.ToString() == "0")
            {
                btn_Fillter.Text = "Ẩn lọc";
                btn_Fillter.Tag = 1;
                panel_left.Visible = true;
            }
            else
            {
                btn_Fillter.Text = "Mở lọc";
                btn_Fillter.Tag = 0;
                panel_left.Visible = false;
            }
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            if (cbo_Department.SelectedIndex > 0)
                _DepartmentKeyClose = cbo_Department.SelectedValue.ToInt();
            else _DepartmentKeyClose = 0;
            if (cboTeam.SelectedIndex > 0)
                _TeamKeyClose = cboTeam.SelectedValue.ToInt();
            else
                _TeamKeyClose = 0;
            _DateClose = dteDate.Value;
            _IDSalary = GetSalaryID();
            if (_IDSalary == "")
            {
                Utils.TNMessageBoxOK("Vui lòng chọn ít nhất 1 chỉ tiêu lương!",1);
                return;
            }
            string Status = "Tìm DL form " + HeaderControl.Text + " > tháng: " + dteDate.Value.ToString("MM/yyyy") + " > Nhóm:" + cboTeam.Text + " > Từ khóa:" + txt_Search.Text;
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);

            try
            {
                using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }
            }
            catch (Exception ex)
            {
                Utils.TNMessageBoxOK(ex.ToString(), 4);
            }
        }
        private string GetSalaryID()
        {
            string zMesage = "";
            if (LVEmployee.Items.Count > 0)
            {
                int zPlag = 0;
                zMesage += "( ";
                for (int i = 0; i < LVEmployee.Items.Count; i++)
                {
                    if (LVEmployee.Items[i].Checked)
                    {
                        zMesage += "A.CodeID ='" + LVEmployee.Items[i].Tag + "' OR ";
                        zPlag++;
                    }
                }
                if (zPlag != 0)
                {
                    zMesage = zMesage.Remove(zMesage.Length - 3, 3);
                    zMesage += " )";
                }
                else
                    zMesage = "";
            }
            return zMesage;

        }

        private void DisplayData()
        {
            DataTable zTable = new DataTable();
            zTable = Report.No7_V3(_DateClose, _DepartmentKeyClose, _TeamKeyClose, txt_Search.Text, _IDSalary);
            if (zTable.Rows.Count > 0)
            {
                PivotTable zPvt = new PivotTable(zTable);
                DataTable zTablePivot = zPvt.Generate_ASC("HeaderColumn", "LeftColumn", new string[] { "Amount" }, "CÔNG VIỆC", "TỔNG CỘNG", "TỔNG CỘNG", new string[] { "Sô tiền" }, 0);
                //Tách cột Leftcolumn
                zTablePivot.Columns.Add("EmployeeKey").SetOrdinal(1);
                zTablePivot.Columns.Add("EmpoyeeName").SetOrdinal(2);
                zTablePivot.Columns.Add("EmpoyeeID").SetOrdinal(3);
                zTablePivot.Columns.Add("PositionName").SetOrdinal(4);
                zTablePivot.Columns.Add("TeamKey", typeof(int)).SetOrdinal(5);
                zTablePivot.Columns.Add("TeamName").SetOrdinal(6);
                zTablePivot.Columns.Add("ATM").SetOrdinal(7);
                zTablePivot.Columns.Add("EmployeeRank");
                zTablePivot.Columns.Add("BranchRank");
                zTablePivot.Columns.Add("DepartmentRank");
                zTablePivot.Columns.Add("TeamRank");
                for (int i = 0; i < zTablePivot.Rows.Count - 1; i++)
                {
                    string[] temp = zTablePivot.Rows[i][0].ToString().Split('|');
                    zTablePivot.Rows[i]["EmployeeKey"] = temp[0];
                    zTablePivot.Rows[i]["EmpoyeeName"] = temp[1];
                    zTablePivot.Rows[i]["EmpoyeeID"] = temp[2];
                    zTablePivot.Rows[i]["PositionName"] = temp[3];
                    zTablePivot.Rows[i]["TeamKey"] = temp[4];
                    zTablePivot.Rows[i]["TeamName"] = temp[5];
                    zTablePivot.Rows[i]["ATM"] = temp[6];
                    zTablePivot.Rows[i]["EmployeeRank"] = ConvertIDRank(temp[2]);
                    zTablePivot.Rows[i]["BranchRank"] = temp[7];
                    zTablePivot.Rows[i]["DepartmentRank"] = temp[8];
                    zTablePivot.Rows[i]["TeamRank"] = temp[9];
                }
                //Xóa cột đầu đã cắt chuỗi và xóa dòng tổng cuối
                zTablePivot.Columns.Remove("CÔNG VIỆC");
                DataRow row = zTablePivot.Rows[zTablePivot.Rows.Count - 1];
                zTablePivot.Rows.Remove(row);
                //Sắp xếp nhân viên
                DataView dv = zTablePivot.DefaultView;
                dv.Sort = " BranchRank ASC,DepartmentRank ASC,TeamRank ASC,  EmployeeRank ASC";
                zTablePivot = dv.ToTable();
                zTablePivot.Columns.Remove("EmployeeRank");
                zTablePivot.Columns.Remove("BranchRank");
                zTablePivot.Columns.Remove("DepartmentRank");
                zTablePivot.Columns.Remove("TeamRank");




                this.Invoke(new MethodInvoker(delegate ()
                {
                    InitGVProduct(zTablePivot);
                    DataTable _Intable = zTablePivot;
                    _STT = 1;
                    int NoGroup = 3;
                    int RowTam = NoGroup;
                    Row zGvRow;
                    int TeamKey = _Intable.Rows[0]["TeamKey"].ToInt();
                    string TeamName = _Intable.Rows[0]["TeamName"].ToString();
                    DataRow[] Array = _Intable.Select("TeamKey=" + TeamKey);
                    if (Array.Length > 0)
                    {
                        DataTable zGroup = Array.CopyToDataTable();
                        GVData.Rows.Add();
                        zGvRow = GVData.Rows[2];
                        HeaderRow(zGvRow, zGroup, TeamKey, TeamName, 0);
                    }
                    for (int i = 0; i < _Intable.Rows.Count; i++)
                    {
                        DataRow r = _Intable.Rows[i];
                        if (TeamKey != r["TeamKey"].ToInt())
                        {
                            #region [GROUP]
                            TeamKey = r["TeamKey"].ToInt();
                            TeamName = r["TeamName"].ToString();
                            Array = _Intable.Select("TeamKey=" + TeamKey);
                            _STT = 1;
                            if (Array.Length > 0)
                            {
                                DataTable zGroup = Array.CopyToDataTable();
                                GVData.Rows.Add();
                                zGvRow = GVData.Rows[i + NoGroup];
                                HeaderRow(zGvRow, zGroup, TeamKey, TeamName, _STT);
                            }
                            #endregion
                            NoGroup++;
                        }
                        GVData.Rows.Add();
                        zGvRow = GVData.Rows[i + NoGroup];
                        DetailRow(zGvRow, r, _STT);
                        RowTam = i + NoGroup;
                        _STT++;
                    }
                    GVData.Rows.Add();
                    zGvRow = GVData.Rows[RowTam + 1];
                    TotalRow(zGvRow, _Intable, RowTam + 1);
                }));
            }
            else
            {
                GVData.Rows.Count = 0;
                GVData.Cols.Count = 0;
            }
        }
        private void HeaderRow(Row RowView, DataTable Table, int TeamKey, string TeamName, int No)
        {
            RowView[0] = "";
            RowView[1] = TeamName;
            RowView[2] = "Số nhân sự " + Table.Select("TeamKey='" + TeamKey + "'").Length;
            RowView[3] = "";
            int nCol = 5;
            double zTotal = 0;
            for (int i = 7; i < Table.Columns.Count - 1; i++)
            {
                double zTemp = 0;
                for (int k = 0; k < Table.Rows.Count; k++)
                {
                    double TotalRow = 0;
                    if (double.TryParse(Table.Rows[k][i].ToString(), out TotalRow))
                    {
                        zTemp += TotalRow;
                    }

                }
                RowView[nCol] = zTemp.Toe1String();
                zTotal += zTemp;
                nCol++;
            }
            RowView.StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
        }
        private void DetailRow(Row RowView, DataRow rDetail, int No)
        {
            RowView[0] = No;
            RowView[1] = rDetail[1].ToString().Trim();//họ tên
            RowView[2] = rDetail[3].ToString().Trim();//tổ nhóm
            RowView[3] = rDetail[2].ToString().Trim();//mã thẻ
            RowView[4] = rDetail[6].ToString().Trim();// atm
            int nCol = 5;
            for (int i = 7; i < rDetail.ItemArray.Length - 1; i++)
            {
                RowView[nCol] = rDetail[i].Toe1String();
                nCol++;
            }
        }
        private void TotalRow(Row RowView, DataTable Table, int No)
        {
            RowView[0] = "";
            RowView[1] = "TỔNG CỘNG ";
            RowView[2] = "Số nhân sự " + Table.Rows.Count;
            RowView[3] = "";

            //float zTotal = 0;
            int nCol = 5;
            for (int i = 7; i < Table.Columns.Count - 1; i++)
            {
                double zTemp = 0;
                for (int k = 0; k < Table.Rows.Count; k++)
                {
                    double TotalRow = 0;
                    if (double.TryParse(Table.Rows[k][i].ToString(), out TotalRow))
                    {
                        zTemp += TotalRow;
                    }

                }
                RowView[nCol] = zTemp.Toe1String();
                nCol++;
            }
            RowView.StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
        }


        void InitGVProduct(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();

            int TotalRow = 2;
            int ToTalCol = TableView.Columns.Count - 3;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(TotalRow);

            CellRange zCellRange = GVData.GetCellRange(0, 0, 1, 0);
            zCellRange.Data = "STT";

            zCellRange = GVData.GetCellRange(0, 1, 1, 1);
            zCellRange.Data = "HỌ TÊN";

            zCellRange = GVData.GetCellRange(0, 2, 1, 2);
            zCellRange.Data = "CHỨC VỤ";

            zCellRange = GVData.GetCellRange(0, 3, 1, 3);
            zCellRange.Data = "SỐ THẺ";

            zCellRange = GVData.GetCellRange(0, 4, 1, 4);
            zCellRange.Data = "TÀI KHOẢN ATM";


            //Row Header
            int ColStart = 5;
            for (int i = 7; i < TableView.Columns.Count - 1; i++)
            {
                DataColumn Col = TableView.Columns[i];
                string[] strCaption = Col.ColumnName.Split('|');
                if (strCaption.Length == 3)
                {
                    if (strCaption[0] == "")
                    {
                        zCellRange = GVData.GetCellRange(0, ColStart, 1, ColStart);
                        zCellRange.Data = strCaption[1];
                    }
                    else
                    {
                        GVData.Rows[0][ColStart] = strCaption[0];
                        GVData.Rows[1][ColStart] = strCaption[1];
                    }
                }
                GVData.Cols[ColStart].Width = 100;
                ColStart++;
            }

            ////Style
            //GVData.AllowFreezing = AllowFreezingEnum.Both;
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.FixedOnly;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.CenterCenter;
            GVData.Styles.Normal.WordWrap = true;



            //Trộn cột 
            for (int i = 0; i < GVData.Cols.Count; i++)
            {
                GVData.Cols[i].AllowMerging = true;
            }
            //Trộn dòng 0
            GVData.Rows[0].AllowMerging = true;
            GVData.Rows[0].Height = 50;
            GVData.Rows[1].Height = 60;
            //Trộn dòng 1
            //Freeze Row and Column
            GVData.Rows.Fixed = 2;
            GVData.Cols.Frozen = 5;

            GVData.Cols[0].Width = 40;
            GVData.Cols[1].Width = 200;
            GVData.Cols[2].Width = 150;
            GVData.Cols[3].Width = 100;
            GVData.Cols[4].Width = 130;
            //GVData.AutoSizeCols(1, GVData.Cols.Count - 1, 10);
            //Canh phải các cột từ số 1 trở đi
            GVData.Cols[0].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[1].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[2].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[3].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[4].TextAlign = TextAlignEnum.LeftCenter;

            GVData.Cols[0].StyleNew.BackColor = Color.Empty;
            GVData.Cols[1].StyleNew.BackColor = Color.Empty;
            GVData.Cols[2].StyleNew.BackColor = Color.Empty;
            GVData.Cols[3].StyleNew.BackColor = Color.Empty;
            GVData.Cols[4].StyleNew.BackColor = Color.Empty;

            for (int i = 5; i < GVData.Cols.Count; i++)
            {
                GVData.Cols[i].TextAlign = TextAlignEnum.RightCenter;
            }

            //In đậm các dòng, cột tổng
            //GVData.Rows[TotalRow - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            //GVData.Cols[ToTalCol - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            //GVData.Cols[ToTalCol - 2].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            ////GVData.Cols[ToTalCol - 3].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            //GVData.Cols[ToTalCol - 1].StyleNew.BackColor = Color.LightGreen;
            // tô màu cột tổng
            //GVData.Rows[TotalRow - 1].StyleNew.BackColor = Color.LightGreen;

        }

        #region[list view]
        private void InitLayout_LV(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Chỉ tiêu lương";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);
        }
        private void InitData(bool Check)
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LVEmployee;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            DataTable In_Table = new DataTable();

            In_Table = ReportWorker_Data.ListCodeWorker(dteDate.Value);
            LV.Items.Clear();
            int n = In_Table.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = "";
                DataRow nRow = In_Table.Rows[i];
                lvi.Tag = nRow["CodeID"];
                lvi.ForeColor = Color.Navy;

                lvi.BackColor = Color.White;
                lvi.ImageIndex = 0;
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = (i + 1).ToString() + "." + nRow["CodeName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = "";
                lvi.SubItems.Add(lvsi);
                lvi.Checked = Check;
                LV.Items.Add(lvi);
            }

            this.Cursor = Cursors.Default;
        }

        #endregion
        private void Btn_Done_Click(object sender, EventArgs e)
        {
            if (SessionUser.Date_Lock >= dteDate.Value)
            {
                Utils.TNMessageBoxOK("Thời gian này đã khóa sổ.!", 2);
                return;
            }
            _DateClose = dteDate.Value;

            if (Utils.TNMessageBox("Bạn có chắc chốt dữ liệu lương tất cả nhóm tháng "+dteDate.Value.ToString("MM/yyyy")+" này ?.", 2) == "Y")
                {
                    ReportWorker_Close_Info zInfo;
                    zInfo = new ReportWorker_Close_Info();
                    zInfo.GetParent_Info(_DateClose);
                    zInfo.Parent = "";
                    zInfo.DateWrite = _DateClose;
                    zInfo.Save(); //lưu cha
                    if (zInfo.Message == "11" || zInfo.Message == "20")
                    {
                        string zKey = zInfo.Key;
                        zInfo = new ReportWorker_Close_Info();
                        zInfo.CreateClose(_DateClose, SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeName, zKey);
                        if (zInfo.Message != "11")
                        {
                        Utils.TNMessageBoxOK("Lỗi. Vui lòng liên hệ IT! "+zInfo.Message,4);
                        }
                        else
                        {
                        Utils.TNMessageBoxOK("Lưu dữ liệu đã chốt thành công!",3);
                        }
                    }
                }
        }
        private void Btn_Export_Click(object sender, EventArgs e)
        {
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "BaoCaoLuongBoPhanSanXuat-LuongChuaChot" + dteDate.Value.ToString("MM-yyyy") + ".xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }

                    string Status = "Xuất Excel form " + HeaderControl.Text + " > đường dẫn và tên file xuất:" + FDialog.FileName;
                    Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);


                    GVData.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }

        }
        //Chuyển số thành dãy chuỗi
        string ConvertIDRank(string ID)
        {
            //chèn số thấp tới cao--> tới chữ
            string zResult = "";
            string s = "";
            string temp = "";
            if (ID.Substring(0, 1) == "A")
            {
                temp = ID;
                for (int i = 0; i < 8 - ID.Trim().Length; i++)
                {
                    s += "9";
                }
                zResult = s + temp;
            }
            else if (ID.Substring(0, 1) == "H" || ID.Substring(0, 1) == "L")
            {
                temp = ID;
                for (int i = 0; i < 8 - ID.Trim().Length; i++)
                {
                    s += "B";
                }
                zResult = s + temp;
            }
            else
            {
                temp = ID;
                for (int i = 0; i < 8 - ID.Trim().Length; i++)
                {
                    s += "0";
                }
                zResult = s + temp;
            }
            return zResult;
        }
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
                btn_Done.Enabled = false;
                btn_Export.Enabled = false;
            }
            if (Role.RoleAdd == 0 && Role.RoleEdit == 0)
            {
                btn_Done.Enabled = false;
            }
        }
        #endregion
    }
}
