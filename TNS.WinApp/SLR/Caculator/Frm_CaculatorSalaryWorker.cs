﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.CORE;
using TNS.LOG;
using TNS.Misc;
using TNS.SLR;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_CaculatorSalaryWorker : Form
    {
        #region [Khai Báo Biến]    
        private DataTable _TableTeam;
        private DataTable _ReportCode;
        private int _TotalTeam = 0;
        private int _IndexTeam = 0;
        private int _TeamKey = 0;
        private string _TeamID = "";
        private string _TeamName = "";
        int _Circle = 0;
        private DataTable _TableEmployee;
        private int _TotalEmployee = 0;
        private int _IndexEmployee = 0;
        private ReportWorker_Info _WorkerInfo;
        List<ReportWorker_Info> _ListEmployee;
        int _Key = 0;
        string _ID = "";
        string _Name = "";
        double _Amount = 0;
        int _Type = 0;
        string _TypeName = "";
        int _CategoryKey = 0;
        string _CategoryName = "";
        int _Rank = 0;
        int _Publish = 0;
        int _Publish_Close = 0;


        private int _TinhThapDiem = 0;
        private int _GopGioDu = 0;
        private string _TextHTTD = "Không";
        private string _TextGopGioDu = "Không";
        private string _Log_Detail = "";
        private string _txtLog = "";
        #endregion
        public Frm_CaculatorSalaryWorker()
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            Utils.DoubleBuffered(GVTeam, true);
            Utils.DrawGVStyle(ref GVTeam);
            timerEmployee.Tick += timerEmployee_Tick;
            timerTeam.Tick += timerTeam_Tick;

            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            InitGVTeam_Layout(GVTeam);

            btnClose.Click += btnClose_Click;
            btnMini.Click += btnMini_Click;
            btn_Run.Click += btn_Run_Click;
            btnShowLog.Click += btnShowLog_Click;
            btnHideLog.Click += btnHideLog_Click;
            timerTeam.Enabled = true;
            timerTeam.Stop();
            rdo_Yes.CheckedChanged += Rdo_Yes_CheckedChanged;
            btn_View.Click += Btn_View_Click;
            btn_ViewLog.Click += Btn_ViewLog_Click;

            rdo_YesCN.CheckedChanged += Rdo_YesCN_CheckedChanged;

            timerEmployee.Enabled = false;
            timerEmployee.Stop();
            dte_FromDate.Value = SessionUser.Date_Work;
        }

        private void TimerQuay_Tick(object sender, EventArgs e)
        {
            Panel_Right.Visible = true;
        }

        private void Frm_CaculatorSalaryWorker_Load(object sender, EventArgs e)
        {
            //if (SessionUser.UserLogin.Key != "4e5a9e53-9241-4c9b-a69f-e86a7ae3507f")
            //{
            //    Utils.TNMessageBoxOK("Trang đang nâng cấp.Vui lòng quay lại sau!", 1);
            //    this.Close();
            //}

            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, "Truy cập form " + HeaderControl.Text, 2);
            Check_RoleForm();

            PicLoading.Visible = false;
            Panel_Right.Visible = true;

            btnHideLog.Visible = true ;
            btnShowLog.Visible = false;
            //Lấy tất cả nhóm trực tiếp sx  và khác bộ phận hỗ trợ sx
            string zSQL = @"SELECT TeamKey,TeamID +'-'+ TeamName AS TeamName  FROM SYS_Team 
                            WHERE RecordStatus <> 99 AND BranchKey=4 AND  DepartmentKey != 26
                            ORDER BY Rank";
            LoadDataToToolbox.KryptonComboBox(cboTeam, zSQL, "---- Chọn tất cả nhóm ----");

        }
        void LoadTeam()
        {
            AddLog_Detail("/---Đọc các nhóm cần tính lương---/ ");
            if (cboTeam.SelectedValue.ToInt() > 0)
            {
                _TableTeam = ReportWorker_Data.DanhSachToNhom(cboTeam.SelectedValue.ToInt());
                _TotalTeam = _TableTeam.Rows.Count;

                if (_TotalTeam > 0)
                {
                    AddLog_Detail("--Key:" + _TableTeam.Rows[0]["TeamKey"] + ">Mã:" + _TableTeam.Rows[0]["TeamID"] + " >Tên: " + _TableTeam.Rows[0]["TeamName"]);
                    AddLog_Detail("/---Kết thúc đọc các nhóm cần tính lương---/ ");
                    _Log_Detail += Environment.NewLine;
                }
            }
            else
            {
                _TableTeam = ReportWorker_Data.DanhSachToNhom();
                _TotalTeam = _TableTeam.Rows.Count;

                if (_TotalTeam > 0)
                {
                    for (int i = 0; i < _TableTeam.Rows.Count; i++)
                    {
                        AddLog_Detail("--Key:" + _TableTeam.Rows[i]["TeamKey"] + ">Mã:" + _TableTeam.Rows[i]["TeamID"] + " >Tên: " + _TableTeam.Rows[i]["TeamName"]);
                    }
                    AddLog_Detail("/---Kết thúc đọc các nhóm cần tính lương---/ ");
                    _Log_Detail += Environment.NewLine;
                }
            }
        }
        private void btn_Run_Click(object sender, EventArgs e)
        {
            if (SessionUser.Date_Lock >= dte_FromDate.Value)
            {
                Utils.TNMessageBoxOK("Thời gian này đã khóa sổ.!", 2);
                return;
            }
            _ReportCode = ReportWorker_Data.ListCodeReportWorker(dte_FromDate.Value);
            if (_ReportCode.Rows.Count == 0)
            {
                Utils.TNMessageBoxOK("Chưa cấu hình mẫu cơm hoặc mẫu tính lương", 2);
                return;
            }
            if (btn_Run.Tag.ToInt() == 0)
            {
                #region[Log]
                _Log_Detail = "";
                _txtLog = "";
                string Status = "Khởi động tính lương bộ phận sản xuất > tháng:" + dte_FromDate.Value.ToString("MM/yyyy") + " > nhóm:" + cboTeam.Text + " > Hỗ trợ thấp điểm:" + _TextHTTD + " > Gộp GDư vào LK:" + _TextGopGioDu;
                Application_Data.Save_LogApplication(SessionUser.UserLogin.Key,SessionUser.UserLogin.EmployeeKey, Status, 3);

                AddLog_Detail("/--- " + Status + " ---/");
                AddLog_Detail("/---Lúc " + DateTime.Now.ToString("HH: mm") + "---/");
                AddLog_Detail(Environment.NewLine);

                #endregion

                if (_Circle == 0)
                {
                    _IndexTeam = 0;
                    _TotalTeam = 0;

                    _IndexEmployee = 0;
                    _TotalEmployee = 0;
                    groupBox2.Enabled = false;
                    gbx_CN.Enabled = false;

                    _Circle = 1;
                    LoadTeam();
                    timerTeam.Start();
                }
                if (_Circle == 1)
                {
                    timerTeam.Start();
                }
                if (_Circle == 2)
                {
                    timerEmployee.Start();
                }

                ShowLog("---Khởi chạy---");
                ShowLog("---Lúc " + DateTime.Now.ToString("HH:mm") + "---");
                PicLoading.Visible = true;
                btn_Run.Text = "Tạm dừng";
                btn_Run.Tag = 1;
            }
            else
            {
                if (_Circle == 1)
                {
                    timerTeam.Stop();
                }

                if (_Circle == 2)
                {
                    timerEmployee.Stop();
                }

                #region[Log]
                string Status = "Tạm dừng tính lương bộ phận sản xuất > tháng:" + dte_FromDate.Value.ToString("MM/yyyy") + " > nhóm:" + cboTeam.Text + " > Hỗ trợ thấp điểm:" + _TextHTTD + " > Gộp GDư vào LK:" + _TextGopGioDu;
                Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);

                AddLog_Detail("/--- " + Status + " ---/");
                AddLog_Detail("/---Lúc " + DateTime.Now.ToString("HH: mm") + "---/");

                #endregion

                ShowLog("---Tạm dừng---");
                ShowLog("---Lúc " + DateTime.Now.ToString("HH:mm") + "---");
                PicLoading.Visible = true;
                btn_Run.Text = "Khởi động";
                btn_Run.Tag = 0;
            }
        }
        void LoadEmployee(int TeamKey, string TeamID, string TeamName)
        {
            _TableEmployee = ReportWorker_Data.Load_DataV2(TeamKey, dte_FromDate.Value);
            _TotalEmployee = _TableEmployee.Rows.Count;

            ShowLog("\\----------------------------------------------------------------------------\\");
            ShowLog("---[   Xử lý thông tin nhân sự nhóm  " + TeamName + "  ]---");

            AddLog_Detail("/----------------------------------------------------------------------------/");
            AddLog_Detail("---[   Xử lý danh sách nhân sự nhóm  " + TeamName + "  ]---");

            if (_TableEmployee.Rows.Count > 0)
            {

              _ListEmployee = new List<ReportWorker_Info>();
                foreach (DataRow r in _TableEmployee.Rows)
                {
                    _WorkerInfo = new ReportWorker_Info();
                    _WorkerInfo.BranchKey = r["BranchKey"].ToInt();
                    _WorkerInfo.BranchID = r["BranchID"].ToString();
                    _WorkerInfo.BranchName = r["BranchName"].ToString();

                    _WorkerInfo.DepartmentKey = r["DepartmentKey"].ToInt();
                    _WorkerInfo.DepartmentID = r["DepartmentID"].ToString();
                    _WorkerInfo.DepartmentName = r["DepartmentName"].ToString();

                    _WorkerInfo.EmployeeKey = r["EmployeeKey"].ToString();
                    _WorkerInfo.EmployeeID = r["EmployeeID"].ToString();
                    _WorkerInfo.EmployeeName = r["EmployeeName"].ToString();
                    _WorkerInfo.TeamKey = TeamKey;
                    _WorkerInfo.TeamID = TeamID;
                    _WorkerInfo.TeamName = TeamName;
                    _WorkerInfo.PositionKey = r["PositionKey"].ToInt();
                    _WorkerInfo.PositionID = r["PositionID"].ToString();
                    _WorkerInfo.PositionName = r["PositionName"].ToString();

                    _WorkerInfo.ATM = r["ATM"].ToString();


                    _WorkerInfo.DateWrite = dte_FromDate.Value;
                    _WorkerInfo.CreatedBy = SessionUser.UserLogin.Key;
                    _WorkerInfo.CreatedName = SessionUser.UserLogin.EmployeeName;
                    _WorkerInfo.ModifiedBy = SessionUser.UserLogin.Key;
                    _WorkerInfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
                    _ListEmployee.Add(_WorkerInfo);
                }
            }
            else
            {
                ShowLog("Không có công nhân.");
                AddLog_Detail("---Không có công nhân.---");
            }
        }
        private void timerEmployee_Tick(object sender, EventArgs e)
        {
            timerEmployee.Stop();
            if (_IndexEmployee < _TotalEmployee)
            {
                _WorkerInfo = new ReportWorker_Info();
                _WorkerInfo = _ListEmployee[_IndexEmployee]; // truyen qua progessdata xu lý tạo mới


                Process_Data();

                _Circle = 2;
                _IndexEmployee++;
                timerEmployee.Start();
            }
            else
            {
                ShowLog("---Đã xử lý xong nhóm ---");

                AddLog_Detail("/---Đã xử lý xong nhóm ---/");
                AddLog_Detail("/-------------------------/");

                _Circle = 1;
                _IndexTeam++;
                _IndexEmployee = 0;
                timerTeam.Start();
            }
        }
        private DataTable _tbLuongKhoan;
        private void timerTeam_Tick(object sender, EventArgs e)
        {
            timerTeam.Stop();

            if (_IndexTeam < _TotalTeam)
            {
                if (_TableTeam.Rows.Count > 0)
                {


                    DataRow r = _TableTeam.Rows[_IndexTeam];
                    _TeamKey = r["TeamKey"].ToInt();
                    _TeamName = r["TeamName"].ToString();
                    _TeamID = r["TeamID"].ToString();

                    AddLog_Detail("/---Bắt đầu tính nhóm : " + _TeamName + "---/");
                    AddLog_Detail("---Xóa thông tin đã tính >nhóm : " + _TeamName + ">ngày:" + dte_FromDate.Value.ToString());
                    //Xóa để chạy lại theo nhóm
                    _WorkerInfo = new ReportWorker_Info();
                    _WorkerInfo.Delete_Team(_TeamKey, dte_FromDate.Value);
                    if(_WorkerInfo.Message=="30")
                    {
                        AddLog_Detail("---Kết quả:Xóa thông tin đã tính >nhóm : " + _TeamName + ">ngày:" + dte_FromDate.Value.ToString()+">Thành công");
                    }
                    else
                    {
                        AddLog_Detail("---Kết quả: Xóa thông tin đã tính >nhóm : " + _TeamName + ">ngày:" + dte_FromDate.Value.ToString()+">Lỗi:"+_WorkerInfo.Message);
                    }
                    //Lấy lương khoán sản xuất tổ đang tính
                    _tbLuongKhoan = ReportWorker_Data.TongHopLuongNangSuat_V2(_TeamKey, dte_FromDate.Value);

                    LoadEmployee(_TeamKey, _TeamID.Trim(), _TeamName);
                    ShowGVTeam(r, GVTeam);
                    timerEmployee.Start();
                }
                else
                {
                    _Circle = 0;
                    _IndexTeam = 0;
                    timerTeam.Stop();
                    btn_Run.Enabled = true;
                    btn_Run.Text = "Khởi động";
                    PicLoading.Visible = false;
                    btn_Run.Tag = 0;
                    ShowLog("---Đã tính xong lúc " + DateTime.Now.ToString("HH:mm") + "---");
                    groupBox2.Enabled = true;
                    gbx_CN.Enabled = true;

                    AddLog_Detail("/--Đã tính xong lúc " + DateTime.Now.ToString("HH:mm") + "---/");


                    string zStatus = "Kết thúc tính lương bộ phận sản xuất > tháng:" + dte_FromDate.Value.ToString("MM/yyyy") + " > nhóm:" + cboTeam.Text + " > Hỗ trợ thấp điểm:" + _TextHTTD + " > Gộp GDư vào LK:" + _TextGopGioDu;
                    //Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, Status, 3);
                    Save_LogCaculator_Import(zStatus);

                }
            }
            else
            {
                _Circle = 0;
                _IndexTeam = 0;
                timerTeam.Stop();
                btn_Run.Enabled = true;
                PicLoading.Visible = false;
                btn_Run.Text = "Khởi động";
                btn_Run.Tag = 0;
                ShowLog("---Đã tính xong lúc " + DateTime.Now.ToString("HH:mm") + "---");
                groupBox2.Enabled = true;
                gbx_CN.Enabled = true;

                AddLog_Detail("/--Đã tính xong lúc " + DateTime.Now.ToString("HH:mm") + "---/");
                string zStatus = "Kết thúc tính lương bộ phận sản xuất > tháng:" + dte_FromDate.Value.ToString("MM/yyyy") + " > nhóm:" + cboTeam.Text + " > Hỗ trợ thấp điểm:" + _TextHTTD + " > Gộp GDư vào LK:" + _TextGopGioDu;
                //Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, zStatus, 3);
                Save_LogCaculator_Import(zStatus);
            }
        }
        void Process_Data()
        {
            _Key = 0;
            _ID = "";
            _Name = "";
            _Amount = 0;
            _Type = 0;
            _TypeName = "";
            _CategoryKey = 0;
            _CategoryName = "";
            _Rank = 0;
            _Publish = 0;
            _Publish_Close = 0;

            string ztext = "--Bắt đầu tính lương nhân sự:[" + _WorkerInfo.EmployeeID + "-" + _WorkerInfo.EmployeeName + "]--" + Environment.NewLine;
            ztext += "-Số stt       :[" + _IndexEmployee + "] " + Environment.NewLine;
            ztext += "-Key nhân viên:"+_WorkerInfo.EmployeeKey + Environment.NewLine;
            ztext += "-Mã thẻ       :" + _WorkerInfo.EmployeeID + Environment.NewLine;
            ztext += "-Họ và tên    :" + _WorkerInfo.EmployeeName + Environment.NewLine;
            ztext += "-Key nhóm     :" + _WorkerInfo.TeamKey + Environment.NewLine;
            ztext += "-Mã nhóm      :" + _WorkerInfo.TeamID + Environment.NewLine;
            ztext += "-Tên nhóm     :" + _WorkerInfo.TeamName + Environment.NewLine;
            ztext += "-Ngày         :" + _WorkerInfo.DateWrite.ToString("dd/MM/yyyy") + Environment.NewLine;
            ztext += "-Key tạo      :" + _WorkerInfo.CreatedBy + Environment.NewLine;
            ztext += "-Tên người tạo:" + _WorkerInfo.CreatedName + Environment.NewLine;
            ztext += "-Key sửa      :" + _WorkerInfo.ModifiedBy + Environment.NewLine;
            ztext += "-Tên người sửa:" + _WorkerInfo.ModifiedName + Environment.NewLine;
            AddLog_Detail(ztext);

            float zNGGC = 0;
            float zKSPT = 0;
            float zLTCN = 0;
            float zLTNL = 0;
            float zSPTK = 0;
            float zCNTK = 0;
            float zNLTK = 0;
            float zTongLuongNangXuat = 0;

            string zGioDu = _tbLuongKhoan.Compute("SUM([" + _tbLuongKhoan.Columns[5].ColumnName + "])", "EmployeeKey='" + _WorkerInfo.EmployeeKey + "'").ToString();
            string zThuongTaiTo = _tbLuongKhoan.Compute("SUM([" + _tbLuongKhoan.Columns[6].ColumnName + "])", "EmployeeKey='" + _WorkerInfo.EmployeeKey + "'").ToString();
            string zCNTaiTo = _tbLuongKhoan.Compute("SUM([" + _tbLuongKhoan.Columns[7].ColumnName + "])", "EmployeeKey='" + _WorkerInfo.EmployeeKey + "'").ToString();
            string zLeTaiTo = _tbLuongKhoan.Compute("SUM([" + _tbLuongKhoan.Columns[8].ColumnName + "])", "EmployeeKey='" + _WorkerInfo.EmployeeKey + "'").ToString();
            string zThuongKhac = _tbLuongKhoan.Compute("SUM([" + _tbLuongKhoan.Columns[9].ColumnName + "])", "EmployeeKey='" + _WorkerInfo.EmployeeKey + "'").ToString();
            string zCNKhac = _tbLuongKhoan.Compute("SUM([" + _tbLuongKhoan.Columns[10].ColumnName + "])", "EmployeeKey='" + _WorkerInfo.EmployeeKey + "'").ToString();
            string zLeKhac = _tbLuongKhoan.Compute("SUM([" + _tbLuongKhoan.Columns[11].ColumnName + "])", "EmployeeKey='" + _WorkerInfo.EmployeeKey + "'").ToString();

            if (float.TryParse(zGioDu, out zNGGC))
            {

            }
            if (float.TryParse(zThuongTaiTo, out zKSPT))
            {

            }
            if (float.TryParse(zCNTaiTo, out zLTCN))
            {

            }
            if (float.TryParse(zLeTaiTo, out zLTNL))
            {

            }
            if (float.TryParse(zThuongKhac, out zSPTK))
            {

            }
            if (float.TryParse(zCNKhac, out zCNTK))
            {

            }
            if (float.TryParse(zLeKhac, out zNLTK))
            {

            }
            zTongLuongNangXuat = zNGGC + zKSPT + zLTCN + zLTNL + zSPTK + zCNTK + zNLTK;

            ztext = "---Tham số tính lương---"  + Environment.NewLine;
            ztext += "-LK giờ giản ca:" + zNGGC.Ton1String() + Environment.NewLine;
            ztext += "-LK tại tổ(NT) :" + zKSPT.Ton1String() + Environment.NewLine;
            ztext += "-LK tại tổ(CN) :" + zLTCN.Ton1String() + Environment.NewLine;
            ztext += "-LK tại tổ(LT) :" + zLTNL.Ton1String() + Environment.NewLine;
            ztext += "-LK tổ khác(NT):" + zSPTK.Ton1String() + Environment.NewLine;
            ztext += "-LK tổ khác(CN):" + zCNTK.Ton1String() + Environment.NewLine;
            ztext += "-LK tổ khác(LT):" + zNLTK.Ton1String() + Environment.NewLine; 

            AddLog_Detail(ztext);
            AddLog_Detail("---Bắt đầu tính bảng lương---");

            if (zTongLuongNangXuat > 0)
            {
                for (int i = 0; i < _ReportCode.Rows.Count; i++)
                {
                    _Key = _ReportCode.Rows[i]["AutoKey"].ToInt();
                    _ID = _ReportCode.Rows[i]["ID"].ToString().Trim();
                    _Name = _ReportCode.Rows[i]["Name"].ToString().Trim();
                    _Type = _ReportCode.Rows[i]["Type"].ToInt();
                    _TypeName = _ReportCode.Rows[i]["TypeName"].ToString().Trim();
                    _CategoryKey = _ReportCode.Rows[i]["CategoryKey"].ToInt();
                    _CategoryName = _ReportCode.Rows[i]["CategoryName"].ToString().Trim();
                    _Rank = _ReportCode.Rows[i]["Rank"].ToInt();
                    _Publish = _ReportCode.Rows[i]["Publish"].ToInt();
                    _Publish_Close = _ReportCode.Rows[i]["Publish_Close"].ToInt();
                       
                    switch (_ID)
                    {
                        case "MLTT"://Mưc lương tối thiểu (Chính)
                            _Amount = ReportWorker_Data.MLTT(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                            break;
                        case "NGGC"://Ngoài giờ giản ca (tăng ca)
                            _Amount = zNGGC;
                            break;
                        case "KSPT"://Khoán sản phẩm thường (tại tổ)
                            _Amount = zKSPT;
                            break;
                        case "LTCN"://Khoán sản phẩm chủ nhật(tại tổ)
                            _Amount = zLTCN;
                            break;
                        case "LTNL"://Khoán sản phẩm lễ (tại tổ)
                            _Amount = zLTNL;
                            break;
                        case "SPTK": //Khoán sản phẩm ngày thường (tại tổ khác)
                            _Amount = zSPTK;
                            break;
                        case "CNTK": //Khoán sản phẩm ngày Chủ nhật (tại tổ khác)
                            _Amount = zCNTK;
                            break;
                        case "NLTK": //Khoán sản phẩm ngày lễ (tại tổ khác)
                            _Amount = zNLTK;
                            break;
                        case "KTCC": //Khoán dùng tính chuyên cần
                            _Amount = ReportWorker_Data.KTCC(_WorkerInfo.EmployeeKey, dte_FromDate.Value, _GopGioDu);
                            break;
                        case "TCCT": //Tiền chuyên cần
                            _Amount = ReportWorker_Data.TCCT(_WorkerInfo.EmployeeKey, dte_FromDate.Value, zTongLuongNangXuat, _GopGioDu);
                            break;
                        case "STHT": //Tiền hỗ trợ
                            _Amount = ReportWorker_Data.STHT(_WorkerInfo.EmployeeKey, dte_FromDate.Value, zTongLuongNangXuat);
                            break;
                        case "NCPN": //Số ngày phép năm
                            _Amount = ReportWorker_Data.NCPN(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                            break;
                        case "HTPN": //Hỗ trợ  phép năm
                            _Amount = ReportWorker_Data.HTPN(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                            break;


                        case "NLLT": //Số ngày PHÉP,LỄ TẾT
                            _Amount = ReportWorker_Data.NLLT(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                            break;
                        case "HTLT": // HỖ TRỢ PHÉP, LỄ TẾT
                            _Amount = ReportWorker_Data.HTLT(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                            break;


                        case "NPTN": //Số ngày PHÉP THÂM NIÊN
                            _Amount = ReportWorker_Data.NPTN(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                            break;
                        case "HTTN": // HỖ TRỢ PHÉP THÂM NIÊN
                            _Amount = ReportWorker_Data.HTTN(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                            break;


                        case "TKXH": //Tiền khoán xuất hàng
                            _Amount = ReportWorker_Data.TKXH(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                            break;
                        case "DTHL": //Đào tạo huấn luyện
                            _Amount = ReportWorker_Data.DTHL(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                            break;

                        case "HTKH": //HỖ TRỢ KHÁC- DỰ PHÒNG
                            _Amount = ReportWorker_Data.HTKH(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                            break;

                        case "TCSX": // tổng cộng tiền sản xuất
                            _Amount = ReportWorker_Data.TCSX(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                            break;
                        case "HTTD": // Hỗ trợ tháng thấp điểm
                            _Amount = ReportWorker_Data.HTTD(_WorkerInfo.EmployeeKey, dte_FromDate.Value, _TinhThapDiem);
                            break;
                        case "SLCN": // Só lượng con nhỏ
                            _Amount = ReportWorker_Data.SLCN(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                            break;
                        case "STCN": // Só tiền con nhỏ
                            _Amount = ReportWorker_Data.STCN(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                            break;

                        #region[Ma com cu]
                        case "SLCC": // Số phần cơm loại C
                            _Amount = ReportWorker_Data.SLCC(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                            break;
                        case "STCC": // Số Tiền cơm loại C
                            _Amount = ReportWorker_Data.STCC(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                            break;
                        case "SLCU": // Số phần cơm loại U
                            _Amount = ReportWorker_Data.SLCU(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                            break;
                        case "STCU": // Số Tiền cơm loại U
                            _Amount = ReportWorker_Data.STCU(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                            break;
                        #endregion

                        case "SLTC": // Số lượng tổng công
                            _Amount = ReportWorker_Data.SLTC(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                            break;
                        case "HUUL": // Số tiền hoàn ứng lương
                            _Amount = ReportWorker_Data.HUUL(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                            break;
                        case "HUXH": // Số tiền hoàn ứng xuất hàng
                            _Amount = ReportWorker_Data.HUXH(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                            break;

                        //Thêm tách  riêng bhxh
                        case "XHCT": // Số tiền đóng bảo hiểm xã hội-CTY
                            _Amount = ReportWorker_Data.XHCT(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                            break;
                        case "YTCT": // Số tiền đóng bảo hiểm Y TẾ - CTY
                            _Amount = ReportWorker_Data.YTCT(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                            break;
                        case "NVCT": // Số tiền đóng bảo hiểm THAT NGHIÊP - CTY
                            _Amount = ReportWorker_Data.NVCT(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                            break;
                        case "XHCN": // Số tiền đóng bảo hiểm xã hội-CÁ NHÂN
                            _Amount = ReportWorker_Data.XHCN(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                            break;
                        case "YTCN": // Số tiền đóng bảo hiểm Y TẾ - CÁ NHÂN
                            _Amount = ReportWorker_Data.YTCN(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                            break;
                        case "NVCN": // Số tiền đóng bảo hiểm THAT NGHIÊP - CÁ NHÂN
                            _Amount = ReportWorker_Data.NVCN(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                            break;

                        //
                        case "BHXH": // Số tiền đóng bảo hiểm xã hội
                            _Amount = ReportWorker_Data.BHXH(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                            break;
                        case "STCD": // Số tiền đóng công đoàn- CÁ NHÂN
                            _Amount = ReportWorker_Data.STCD(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                            break;
                        case "CDCT": // Số tiền đóng công đoàn CTY
                            _Amount = ReportWorker_Data.CDCT(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                            break;
                        case "TNCT": //Thu nhập chịu thuế
                            _Amount = ReportWorker_Data.TNCT(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                            break;
                        case "TNCN": // Số tiền Thuế thu nhập cá nhân
                            _Amount = ReportWorker_Data.TNCN(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                            break;
                        case "TTLV": // Số tiền tấm lòng vàng
                            _Amount = ReportWorker_Data.TTLV(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                            break;

                        #region[Ma com cu]
                        case "STCT": // Số phần cơm trừ
                            _Amount = ReportWorker_Data.STCT(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                            break;
                        case "TTCT": // Số tiền cơm trừ
                            _Amount = ReportWorker_Data.TTCT(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                            break;
                        #endregion

                        case "CKTK": // CÁC KHOÁN TRỪ KHÁC
                            _Amount = ReportWorker_Data.CKTK(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                            break;

                        case "STTL": // Số tiền thực lãnh
                            _Amount = ReportWorker_Data.STTL(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                            break;
                        default:
                            if (_CategoryKey == 5 || _CategoryKey == 7) //số lượng cơm cộng hoặc trừ
                            {
                                // Tổng mã cơm thống kê
                                string RiceID = ReportWorker_Data.GetRiceID(_ID);
                                if (RiceID.Length > 0)
                                    _Amount = ReportWorker_Data.NumberRice(_WorkerInfo.EmployeeKey, RiceID, dte_FromDate.Value);
                            }
                            if (_CategoryKey == 6 || _CategoryKey == 8)// thành tiền cơm cộng hoặc trừ
                            {
                                // Tổng mã cơm thành tiền thống kê
                                string RiceID = ReportWorker_Data.GetRiceID(_ID);
                                if (RiceID.Length > 0)
                                    _Amount = ReportWorker_Data.MoneyRice(_WorkerInfo.EmployeeKey, RiceID, dte_FromDate.Value);
                            }
                            break;
                    }
                    Insert();
                }
                AddLog_Detail("----Kết thúc tính lương nhân sự:["+_WorkerInfo.EmployeeID+"-"+_WorkerInfo.EmployeeName+"]---");
                _Log_Detail+= Environment.NewLine;
            }
            else
            {
                AddLog_Detail("--Không có lương năng suất >Bỏ qua tính toán--");
                _Log_Detail+=Environment.NewLine;
            }
        }
        private void Insert()
        {
            _WorkerInfo.CodeKey = _Key;
            _WorkerInfo.CodeID = _ID;
            _WorkerInfo.CodeName = _Name;
            _WorkerInfo.Amount = _Amount;
            _WorkerInfo.Type = _Type;
            _WorkerInfo.TypeName = _TypeName;
            _WorkerInfo.CategoryKey = _CategoryKey;
            _WorkerInfo.CategoryName = _CategoryName;
            _WorkerInfo.Rank = _Rank;
            _WorkerInfo.Publish = _Publish;
            _WorkerInfo.Publish_Close = _Publish_Close;
            _WorkerInfo.Create();
            if (_WorkerInfo.Message == "")
                AddLog_Detail("-Key:" + _Key.ToString().PadLeft(2,'0') + " > Mã: " + _ID + " > SL/tiền: " + _Amount.ToString("n1").PadLeft(12,'_') + " > Tên: " + _Name.PadRight(40,' ') + " > Lưu thành công");
            else
                AddLog_Detail("-Key:" + _Key.ToString().PadLeft(2, '0') + " > Mã: " + _ID + " > SL/tiền: " + _Amount.ToString("n1").PadLeft(12,'_') + " > Tên: " + _Name.PadRight(40, ' ') +  " >> Lỗi. " + _WorkerInfo.Message);
        }
        private void Btn_View_Click(object sender, EventArgs e)
        {
            Frm_XemTruocHoTroThangThapDiem frm = new Frm_XemTruocHoTroThangThapDiem();
            frm.DateWrite = dte_FromDate.Value;
            frm.Show();
        }

        private void Rdo_Yes_CheckedChanged(object sender, EventArgs e)
        {
            if (rdo_Yes.Checked == true)
            {
                _TinhThapDiem = 1;
                _TextHTTD = "Có";
            }
            else
            {
                _TinhThapDiem = 0;
                _TextHTTD = "Không";
            }
        }
        private void Rdo_YesCN_CheckedChanged(object sender, EventArgs e)
        {
            if (rdo_YesCN.Checked == true)
            {
                _GopGioDu = 1;
                _TextGopGioDu = "Có";
            }
            else
            {
                _GopGioDu = 0;
                _TextGopGioDu = "Không";
            }
        }


        void InitGVTeam_Layout(DataGridView GV)
        {
            GV.Columns.Add("No", "#");
            GV.Columns.Add("TeamID", "Mã nhóm");
            GV.Columns.Add("TeamName", "Tên nhóm");

            GV.Columns["No"].Width = 45;
            GV.Columns["TeamID"].Width = 100;
            GV.Columns["TeamName"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.ColumnHeadersHeight = 25;
            GV.FirstDisplayedScrollingRowIndex = GV.RowCount - 1;
        }
        void ShowGVTeam(DataRow nRow, DataGridView GV)
        {
            GV.Rows.Add();
            DataGridViewRow nRowView = GV.Rows[_IndexTeam];
            nRowView.Tag = nRow["TeamKey"];
            nRowView.Cells["No"].Value = (_IndexTeam + 1).ToString();
            nRowView.Cells["TeamName"].Value = nRow["TeamName"].ToString().Trim();
            nRowView.Cells["TeamID"].Value = nRow["TeamID"].ToString().Trim();
        }

        void ShowLog(string Text)
        {
            Invoke(new MethodInvoker(delegate
            {
                txtLog.Items.Add(Text);
                txtLog.SelectedIndex = txtLog.Items.Count - 1;
            }));

            _txtLog += Text + Environment.NewLine;
        }
        void AddLog_Detail(string Text)
        {
            Invoke(new MethodInvoker(delegate
            {
                txt_Log_Detail.Items.Add(DateTime.Now.ToString("HH:mm:ss.fff") + " : " + Text);
                txt_Log_Detail.SelectedIndex = txt_Log_Detail.Items.Count - 1;
            }));

            _Log_Detail += DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss.fff") + " " + Text + Environment.NewLine;
        }

        void Save_LogCaculator_Import(string ActionName)
        {
            Application_Info zinfo = new Application_Info();
            zinfo.ActionName = ActionName;
            zinfo.DateLog = dte_FromDate.Value;
            zinfo.ActionType = 3;
            zinfo.SubDescription = txtLog.Text;
            zinfo.Description = _Log_Detail;
            zinfo.Form = 1;//Dành cho tính lương công nhân.
            zinfo.FormName = "Form " + HeaderControl.Text;
            zinfo.UserKey = SessionUser.UserLogin.Key;
            zinfo.EmployeeKey = SessionUser.UserLogin.EmployeeKey;
            zinfo.CreatedBy = SessionUser.UserLogin.Key;
            zinfo.CreatedName = SessionUser.UserLogin.EmployeeName;
            zinfo.ModifiedBy = SessionUser.UserLogin.Key;
            zinfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
            zinfo.Create();
        }
        private void Btn_ViewLog_Click(object sender, EventArgs e)
        {
            Frm_LogDetail frm = new Frm_LogDetail();
            frm.DateWrite = dte_FromDate.Value;
            frm.Form = 1;
            frm.FormName = HeaderControl.Text;
            frm.ShowDialog();
        }
        #region [Custom Control Box Form]
        private void btnHideLog_Click(object sender, EventArgs e)
        {
            Panel_Right.Visible = false;
            txtLog.Visible = false;
            btnShowLog.Visible = true;
            btnHideLog.Visible = false;
        }
        private void btnShowLog_Click(object sender, EventArgs e)
        {
            Panel_Right.Visible = true;
            txtLog.Visible = true;
            btnShowLog.Visible = false;
            btnHideLog.Visible = true;
        }
       
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion        
        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_ViewLog.Enabled = false;
            }
            if (Role.RoleAdd == 0 && Role.RoleEdit == 0)
            {
                btn_Run.Enabled = false;
                btn_ViewLog.Enabled = false;
            }

        }
        #endregion
    }
}
