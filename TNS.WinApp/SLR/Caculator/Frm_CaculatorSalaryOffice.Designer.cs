﻿namespace TNS.WinApp
{
    partial class Frm_CaculatorSalaryOffice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_CaculatorSalaryOffice));
            this.Panel_Search = new System.Windows.Forms.Panel();
            this.dte_FromDate = new ComponentFactory.Krypton.Toolkit.KryptonDateTimePicker();
            this.cboTeam = new ComponentFactory.Krypton.Toolkit.KryptonComboBox();
            this.PicLoading = new System.Windows.Forms.PictureBox();
            this.btn_Run = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.HeaderControl = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btnMini = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.buttonSpecAny2 = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnClose = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.Panel_Right = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_ViewLog = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.label1 = new System.Windows.Forms.Label();
            this.kryptonHeader2 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.Panel_Team = new System.Windows.Forms.Panel();
            this.GVTeam = new System.Windows.Forms.DataGridView();
            this.kryptonHeader3 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btnShowLog = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnHideLog = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.timerEmployee = new System.Windows.Forms.Timer(this.components);
            this.timerTeam = new System.Windows.Forms.Timer(this.components);
            this.txtLog = new System.Windows.Forms.ListBox();
            this.txt_Log_Detail = new System.Windows.Forms.ListBox();
            this.Panel_Search.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboTeam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicLoading)).BeginInit();
            this.Panel_Right.SuspendLayout();
            this.panel1.SuspendLayout();
            this.Panel_Team.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GVTeam)).BeginInit();
            this.SuspendLayout();
            // 
            // Panel_Search
            // 
            this.Panel_Search.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Search.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel_Search.Controls.Add(this.dte_FromDate);
            this.Panel_Search.Controls.Add(this.cboTeam);
            this.Panel_Search.Controls.Add(this.PicLoading);
            this.Panel_Search.Controls.Add(this.btn_Run);
            this.Panel_Search.Controls.Add(this.HeaderControl);
            this.Panel_Search.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel_Search.Location = new System.Drawing.Point(0, 0);
            this.Panel_Search.Name = "Panel_Search";
            this.Panel_Search.Size = new System.Drawing.Size(936, 106);
            this.Panel_Search.TabIndex = 216;
            // 
            // dte_FromDate
            // 
            this.dte_FromDate.CustomFormat = "MM/yyyy";
            this.dte_FromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dte_FromDate.Location = new System.Drawing.Point(3, 59);
            this.dte_FromDate.Name = "dte_FromDate";
            this.dte_FromDate.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.dte_FromDate.ShowUpDown = true;
            this.dte_FromDate.Size = new System.Drawing.Size(109, 23);
            this.dte_FromDate.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.dte_FromDate.StateCommon.Border.Rounding = 4;
            this.dte_FromDate.StateCommon.Border.Width = 1;
            this.dte_FromDate.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dte_FromDate.TabIndex = 209;
            // 
            // cboTeam
            // 
            this.cboTeam.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTeam.DropDownWidth = 119;
            this.cboTeam.Location = new System.Drawing.Point(533, 58);
            this.cboTeam.Name = "cboTeam";
            this.cboTeam.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.cboTeam.Size = new System.Drawing.Size(327, 24);
            this.cboTeam.StateCommon.ComboBox.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.cboTeam.StateCommon.ComboBox.Border.Rounding = 4;
            this.cboTeam.StateCommon.ComboBox.Border.Width = 1;
            this.cboTeam.StateCommon.ComboBox.Content.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cboTeam.StateCommon.Item.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.cboTeam.StateCommon.Item.Border.Rounding = 4;
            this.cboTeam.StateCommon.Item.Border.Width = 1;
            this.cboTeam.StateCommon.Item.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cboTeam.TabIndex = 208;
            // 
            // PicLoading
            // 
            this.PicLoading.BackColor = System.Drawing.Color.Transparent;
            this.PicLoading.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.PicLoading.Dock = System.Windows.Forms.DockStyle.Right;
            this.PicLoading.Image = ((System.Drawing.Image)(resources.GetObject("PicLoading.Image")));
            this.PicLoading.Location = new System.Drawing.Point(866, 42);
            this.PicLoading.Name = "PicLoading";
            this.PicLoading.Size = new System.Drawing.Size(68, 62);
            this.PicLoading.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.PicLoading.TabIndex = 207;
            this.PicLoading.TabStop = false;
            this.PicLoading.Visible = false;
            // 
            // btn_Run
            // 
            this.btn_Run.Location = new System.Drawing.Point(116, 51);
            this.btn_Run.Name = "btn_Run";
            this.btn_Run.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Run.Size = new System.Drawing.Size(102, 40);
            this.btn_Run.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Run.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Run.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Run.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Run.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Run.TabIndex = 206;
            this.btn_Run.Tag = "0";
            this.btn_Run.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Run.Values.Image")));
            this.btn_Run.Values.Text = "Khởi động";
            // 
            // HeaderControl
            // 
            this.HeaderControl.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnMini,
            this.buttonSpecAny2,
            this.btnClose});
            this.HeaderControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.HeaderControl.Location = new System.Drawing.Point(0, 0);
            this.HeaderControl.Name = "HeaderControl";
            this.HeaderControl.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.HeaderControl.Size = new System.Drawing.Size(934, 42);
            this.HeaderControl.TabIndex = 205;
            this.HeaderControl.Values.Description = "";
            this.HeaderControl.Values.Heading = "Tính toán lương bộ phận văn phòng";
            this.HeaderControl.Values.Image = ((System.Drawing.Image)(resources.GetObject("HeaderControl.Values.Image")));
            // 
            // btnMini
            // 
            this.btnMini.Image = ((System.Drawing.Image)(resources.GetObject("btnMini.Image")));
            this.btnMini.UniqueName = "F5F06E8241504E72ABB5BEA3F6A9B753";
            // 
            // buttonSpecAny2
            // 
            this.buttonSpecAny2.Image = ((System.Drawing.Image)(resources.GetObject("buttonSpecAny2.Image")));
            this.buttonSpecAny2.UniqueName = "035D1A4881E44F58A084C31DE7352A94";
            this.buttonSpecAny2.Visible = false;
            // 
            // btnClose
            // 
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.UniqueName = "11B07C6F4E1C4F9D8B91BD924CB0EBE6";
            // 
            // Panel_Right
            // 
            this.Panel_Right.Controls.Add(this.txtLog);
            this.Panel_Right.Controls.Add(this.txt_Log_Detail);
            this.Panel_Right.Controls.Add(this.panel1);
            this.Panel_Right.Controls.Add(this.kryptonHeader2);
            this.Panel_Right.Dock = System.Windows.Forms.DockStyle.Right;
            this.Panel_Right.Location = new System.Drawing.Point(586, 106);
            this.Panel_Right.Name = "Panel_Right";
            this.Panel_Right.Size = new System.Drawing.Size(350, 377);
            this.Panel_Right.TabIndex = 217;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panel1.Controls.Add(this.btn_ViewLog);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 30);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(350, 62);
            this.panel1.TabIndex = 215;
            // 
            // btn_ViewLog
            // 
            this.btn_ViewLog.Location = new System.Drawing.Point(165, 9);
            this.btn_ViewLog.Name = "btn_ViewLog";
            this.btn_ViewLog.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_ViewLog.Size = new System.Drawing.Size(106, 40);
            this.btn_ViewLog.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ViewLog.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ViewLog.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_ViewLog.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ViewLog.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ViewLog.TabIndex = 280;
            this.btn_ViewLog.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_ViewLog.Values.Image")));
            this.btn_ViewLog.Values.Text = "Tìm kiếm";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(19, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(129, 14);
            this.label1.TabIndex = 150;
            this.label1.Text = "Xem lịch sử tính lương";
            // 
            // kryptonHeader2
            // 
            this.kryptonHeader2.AutoSize = false;
            this.kryptonHeader2.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader2.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader2.Name = "kryptonHeader2";
            this.kryptonHeader2.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader2.Size = new System.Drawing.Size(350, 30);
            this.kryptonHeader2.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader2.TabIndex = 205;
            this.kryptonHeader2.Values.Description = "";
            this.kryptonHeader2.Values.Heading = "Theo dõi";
            // 
            // Panel_Team
            // 
            this.Panel_Team.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Team.Controls.Add(this.GVTeam);
            this.Panel_Team.Controls.Add(this.kryptonHeader3);
            this.Panel_Team.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel_Team.Location = new System.Drawing.Point(0, 106);
            this.Panel_Team.Name = "Panel_Team";
            this.Panel_Team.Size = new System.Drawing.Size(586, 377);
            this.Panel_Team.TabIndex = 218;
            // 
            // GVTeam
            // 
            this.GVTeam.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.GVTeam.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.GVTeam.ColumnHeadersHeight = 25;
            this.GVTeam.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.GVTeam.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GVTeam.Location = new System.Drawing.Point(0, 30);
            this.GVTeam.Name = "GVTeam";
            this.GVTeam.ReadOnly = true;
            this.GVTeam.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GVTeam.Size = new System.Drawing.Size(586, 347);
            this.GVTeam.TabIndex = 203;
            // 
            // kryptonHeader3
            // 
            this.kryptonHeader3.AutoSize = false;
            this.kryptonHeader3.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnShowLog,
            this.btnHideLog});
            this.kryptonHeader3.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader3.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader3.Name = "kryptonHeader3";
            this.kryptonHeader3.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader3.Size = new System.Drawing.Size(586, 30);
            this.kryptonHeader3.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader3.TabIndex = 200;
            this.kryptonHeader3.Values.Description = "";
            this.kryptonHeader3.Values.Heading = "1 - Tổ/ nhóm";
            // 
            // btnShowLog
            // 
            this.btnShowLog.Image = ((System.Drawing.Image)(resources.GetObject("btnShowLog.Image")));
            this.btnShowLog.UniqueName = "ABE0F8FEF4FF4DE3448D0BAE7F1C6F22";
            // 
            // btnHideLog
            // 
            this.btnHideLog.Image = ((System.Drawing.Image)(resources.GetObject("btnHideLog.Image")));
            this.btnHideLog.UniqueName = "D6E0AB28C2CE49FBDD866175ABE4E274";
            // 
            // timerEmployee
            // 
            this.timerEmployee.Enabled = true;
            // 
            // timerTeam
            // 
            this.timerTeam.Enabled = true;
            // 
            // txtLog
            // 
            this.txtLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLog.FormattingEnabled = true;
            this.txtLog.Location = new System.Drawing.Point(0, 92);
            this.txtLog.Name = "txtLog";
            this.txtLog.ScrollAlwaysVisible = true;
            this.txtLog.Size = new System.Drawing.Size(350, 177);
            this.txtLog.TabIndex = 222;
            // 
            // txt_Log_Detail
            // 
            this.txt_Log_Detail.AllowDrop = true;
            this.txt_Log_Detail.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txt_Log_Detail.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.txt_Log_Detail.FormattingEnabled = true;
            this.txt_Log_Detail.HorizontalScrollbar = true;
            this.txt_Log_Detail.Location = new System.Drawing.Point(0, 269);
            this.txt_Log_Detail.Name = "txt_Log_Detail";
            this.txt_Log_Detail.ScrollAlwaysVisible = true;
            this.txt_Log_Detail.Size = new System.Drawing.Size(350, 108);
            this.txt_Log_Detail.TabIndex = 221;
            // 
            // Frm_CaculatorSalaryOffice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(936, 483);
            this.Controls.Add(this.Panel_Team);
            this.Controls.Add(this.Panel_Right);
            this.Controls.Add(this.Panel_Search);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_CaculatorSalaryOffice";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tính toán lương bộ phận văn phòng";
            this.Load += new System.EventHandler(this.Frm_CacualatorSalaryOffice_Load);
            this.Panel_Search.ResumeLayout(false);
            this.Panel_Search.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboTeam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicLoading)).EndInit();
            this.Panel_Right.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.Panel_Team.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GVTeam)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel Panel_Search;
        private ComponentFactory.Krypton.Toolkit.KryptonDateTimePicker dte_FromDate;
        private ComponentFactory.Krypton.Toolkit.KryptonComboBox cboTeam;
        private System.Windows.Forms.PictureBox PicLoading;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Run;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader HeaderControl;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMini;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny buttonSpecAny2;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnClose;
        private System.Windows.Forms.Panel Panel_Right;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader2;
        private System.Windows.Forms.Panel Panel_Team;
        private System.Windows.Forms.DataGridView GVTeam;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader3;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnShowLog;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnHideLog;
        private System.Windows.Forms.Timer timerEmployee;
        private System.Windows.Forms.Timer timerTeam;
        private System.Windows.Forms.Panel panel1;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_ViewLog;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox txtLog;
        private System.Windows.Forms.ListBox txt_Log_Detail;
    }
}