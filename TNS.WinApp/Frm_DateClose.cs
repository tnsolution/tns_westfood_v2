﻿using C1.Win.C1FlexGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_DateClose : Form
    {
        Access_Role_Info _Role;
        public Frm_DateClose()
        {
            InitializeComponent();
            btnClose.Click += btnClose_Click;
            btnMax.Click += btnMax_Click;
            btnMini.Click += btnMini_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            GVData.MouseClick += GVData_DoubleClick;

        }



        private void Frm_DateClose_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            Check_RoleForm();
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            if (_Role.RoleRead==1)
            {
                DisplayData();
            }
        }
        private void GVData_DoubleClick(object sender, EventArgs e)
        {
            if (GVData.Row > 0)
            {
                int rowIndex = GVData.Row;
                int colIndex = GVData.Col;

                if (_Role.RoleEdit == 1||_Role.RoleEdit==1) //Có quyền thêm và sửa
                {
                    if (rowIndex > 0 && colIndex > 1)
                    {
                        string zYear = GVData[rowIndex, 1].ToString();
                        string zMonth = GVData.CursorCell.Data.ToString().Substring(0, 2);
                        DateTime zDate = DateTime.Parse("1/" + zMonth + "/" + zYear);

                        DateClose_Info zinfo = new DateClose_Info(zDate);
                        int zStatus = zinfo.Status;
                        string zstring = "";

                        if (zStatus == 1)
                        {
                            if (Utils.TNMessageBox("Nếu mở lại dữ liệu đã khóa sổ, thì các tháng kế tiếp đã khóa cũng sẽ mở theo." +
                                "Bạn đồng ý muốn mở lại dữ liệu tháng " + zDate.ToString("MM/yyyy") + " đã khóa ?", 4) == "Y")
                            {

                                bool zResult = false;
                                zResult = DateClose_Data.MoLaiDaKhoa(zDate);
                                if (zResult == true)
                                {
                                    Utils.TNMessageBoxOK("Cập nhật thành công!", 3);
                                    DisplayData();
                                    zstring = "Mở lại dữ liệu tháng " + zDate.ToString("MM/yyyy") + " đã khóa thành công.";
                                }
                                else
                                {
                                    Utils.TNMessageBoxOK("Lỗi.Vui lòng liên hệ IT", 4);
                                    zstring = "Mở lại dữ liệu tháng " + zDate.ToString("MM/yyyy") + " đã khóa thất bại.";
                                }

                                Save_LogCaculator_Import(zstring);
                            }
                        }
                        else
                        {
                            int zResult = 0;
                            zResult = DateClose_Data.KiemTraThangTruocDaKhoaChua(zDate);
                            if (zResult == 1)
                            {
                                if (Utils.TNMessageBox("Sau khi khóa sổ, bạn sẽ không thể sửa đổi các dữ liệu kể từ tháng khóa trở về trước." +
                                    "Bạn có chắc khóa dữ liệu tháng " + zDate.ToString("MM/yyyy") + " ?", 2) == "Y")
                                {
                                    zinfo.Status = 1;
                                    zinfo.ModifiedBy = SessionUser.UserLogin.Key;
                                    zinfo.ModifiedName = SessionUser.UserLogin.Name;
                                    zinfo.Update();
                                    if (zinfo.Message == "")
                                    {
                                        Utils.TNMessageBoxOK("Cập nhật thành công!", 3);
                                        DisplayData();
                                        zstring = "Khóa dữ liệu tháng " + zDate.ToString("MM/yyyy") + " thành công.";
                                    }
                                    else
                                    {
                                        Utils.TNMessageBoxOK(zinfo.Message, 4);
                                        zstring = "Khóa dữ liệu tháng " + zDate.ToString("MM/yyyy") + " thất bại.";
                                    }
                                }
                                Save_LogCaculator_Import(zstring);
                            }

                        }


                    }
                }

            }
        }

        private void InitGV_Layout(DataTable Table)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();

            int ToTalCol = Table.Columns.Count;
            GVData.Cols.Add(ToTalCol + 1);
            GVData.Rows.Add(1);

            GVData.Rows[0][0] = "STT";
            GVData.Rows[0][1] = "NĂM";


            CellRange zRange = GVData.GetCellRange(0, 2, 0, 13);
            zRange.Data = "THÁNG";



            CellStyle left;
            // Create a custom style for large values
            left = GVData.Styles.Add("Large");
            left.BackColor = Color.PowderBlue;
            left.ForeColor = Color.Black;

            CellStyle cs;
            // Create a custom style for large values
            cs = GVData.Styles.Add("LargeValue");
            cs.BackColor = Color.Red;
            cs.ForeColor = Color.White;


            for (int rIndex = 0; rIndex < Table.Rows.Count; rIndex++)
            {
                GVData.Rows.Add();
                DataRow rData = Table.Rows[rIndex];

                GVData.Rows[rIndex + 1][0] = rIndex + 1;
                GVData.Rows[rIndex + 1][1] = rData[0];
                GVData.SetCellStyle(rIndex + 1,0, left);
                GVData.SetCellStyle(rIndex + 1, 1, left);
                for (int j = 1; j < rData.ItemArray.Length; j++)
                {
                    string[] zTemp = rData[j].ToString().Split('|');
                    string zAutoKey = zTemp[0];
                    string zThang = zTemp[1].PadLeft(2,'0');
                    int zStatus = zTemp[2].ToInt();


                    if (zStatus == 1)
                    {
                        GVData.Rows[rIndex + 1][j + 1] = zThang + " \n Đã khóa";
                        GVData.SetCellStyle(rIndex + 1, j + 1, cs);
                    }
                    else
                    {

                        GVData.Rows[rIndex + 1][j + 1] = zThang;
                    }

                }

            }

            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.FixedOnly;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.CenterCenter;
            GVData.Styles.Normal.WordWrap = true;
            GVData.BackColor = Color.PaleGreen;

            //GVData.Styles.Highlight.BackColor = Color.Transparent;

            GVData.Rows.Fixed = 1;
            //GVData.Cols.Fixed = 2;

             GVData.Cols[0].StyleNew.BackColor = Color.Empty;
             //GVData.Cols[1].StyleNew.BackColor = Color.Empty;

            GVData.Rows[0].Height = 50;
            GVData.Rows[0].StyleNew.WordWrap = true;
            GVData.Rows[0].AllowMerging = true;
            GVData.Rows[0].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold); 
            for (int i = 0; i <= GVData.Rows.Count - 1; i++)
            {
                GVData.Rows[i].Height = 50;
            }


            GVData.Cols[0].Width = 40;
            GVData.Cols[0].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[0].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[1].Width = 160;
            GVData.Cols[1].StyleNew.WordWrap = true;
            GVData.Cols[1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
        }
        private void DisplayData()
        {
            DataTable _Intable = DateClose_Data.List();
            if (_Intable.Rows.Count > 0)
            {

                InitGV_Layout(_Intable);
            }
        }
        void Save_LogCaculator_Import(string ActionName)
        {
            Application_Info zinfo = new Application_Info();
            zinfo.ActionName = ActionName;
            zinfo.DateLog = DateTime.Now;
            zinfo.ActionType = 3;
            zinfo.SubDescription = "";
            zinfo.Description = "";
            zinfo.Form = 7;//Dành cho lưu khóa dữ liệu
            zinfo.FormName = "Form " + HeaderControl.Text;
            zinfo.UserKey = SessionUser.UserLogin.Key;
            zinfo.EmployeeKey = SessionUser.UserLogin.EmployeeKey;
            zinfo.CreatedBy = SessionUser.UserLogin.Key;
            zinfo.CreatedName = SessionUser.UserLogin.EmployeeName;
            zinfo.ModifiedBy = SessionUser.UserLogin.Key;
            zinfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
            zinfo.Create();
        }

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                //this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                //    btn_Search.Enabled = false;
                //    btn_Export.Enabled = false;
            }
            _Role = Role;
        }
        #endregion
    }
}
