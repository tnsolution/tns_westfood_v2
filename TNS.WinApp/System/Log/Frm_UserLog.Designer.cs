﻿namespace TNS.WinApp
{
    partial class Frm_UserLog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_UserLog));
            this.HeaderControl = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btnMini = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnMax = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnClose = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dte_ToDate = new TNS.SYS.TNDateTimePicker();
            this.dte_FromDate = new TNS.SYS.TNDateTimePicker();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rdo_Login = new System.Windows.Forms.RadioButton();
            this.rdo_Page = new System.Windows.Forms.RadioButton();
            this.rdo_All = new System.Windows.Forms.RadioButton();
            this.rdo_Action = new System.Windows.Forms.RadioButton();
            this.btn_IT = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Search = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Export = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.kryptonButton1 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Office = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_SanXuat = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Support = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.Panel_Left = new System.Windows.Forms.Panel();
            this.GV_User = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.kryptonHeader1 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btn_Refresh = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.Panel_Done = new System.Windows.Forms.Panel();
            this.txt_Pass = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.kryptonHeader6 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btnClose_Panel_Message = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btn_ReportDemo = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.GVData = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.lbl_TaiKhoan = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.btn_TimeKeeping = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.panel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.Panel_Left.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GV_User)).BeginInit();
            this.panel2.SuspendLayout();
            this.Panel_Done.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GVData)).BeginInit();
            this.SuspendLayout();
            // 
            // HeaderControl
            // 
            this.HeaderControl.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnMini,
            this.btnMax,
            this.btnClose});
            this.HeaderControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.HeaderControl.Location = new System.Drawing.Point(0, 0);
            this.HeaderControl.Name = "HeaderControl";
            this.HeaderControl.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.HeaderControl.Size = new System.Drawing.Size(1004, 42);
            this.HeaderControl.TabIndex = 219;
            this.HeaderControl.Values.Description = "";
            this.HeaderControl.Values.Heading = "Bảng theo dõi hoạt động trên phần mềm";
            this.HeaderControl.Values.Image = ((System.Drawing.Image)(resources.GetObject("HeaderControl.Values.Image")));
            // 
            // btnMini
            // 
            this.btnMini.Image = ((System.Drawing.Image)(resources.GetObject("btnMini.Image")));
            this.btnMini.UniqueName = "F5F06E8241504E72ABB5BEA3F6A9B753";
            // 
            // btnMax
            // 
            this.btnMax.Image = ((System.Drawing.Image)(resources.GetObject("btnMax.Image")));
            this.btnMax.UniqueName = "035D1A4881E44F58A084C31DE7352A94";
            // 
            // btnClose
            // 
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.UniqueName = "11B07C6F4E1C4F9D8B91BD924CB0EBE6";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panel1.Controls.Add(this.dte_ToDate);
            this.panel1.Controls.Add(this.dte_FromDate);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.btn_IT);
            this.panel1.Controls.Add(this.btn_Search);
            this.panel1.Controls.Add(this.btn_Export);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 42);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1004, 66);
            this.panel1.TabIndex = 220;
            // 
            // dte_ToDate
            // 
            this.dte_ToDate.CustomFormat = "dd/MM/yyyy";
            this.dte_ToDate.Location = new System.Drawing.Point(15, 34);
            this.dte_ToDate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dte_ToDate.Name = "dte_ToDate";
            this.dte_ToDate.Size = new System.Drawing.Size(100, 31);
            this.dte_ToDate.TabIndex = 309;
            this.dte_ToDate.Value = new System.DateTime(2020, 5, 5, 0, 0, 0, 0);
            // 
            // dte_FromDate
            // 
            this.dte_FromDate.CustomFormat = "dd/MM/yyyy";
            this.dte_FromDate.Location = new System.Drawing.Point(15, 10);
            this.dte_FromDate.Name = "dte_FromDate";
            this.dte_FromDate.Size = new System.Drawing.Size(100, 25);
            this.dte_FromDate.TabIndex = 310;
            this.dte_FromDate.Value = new System.DateTime(2020, 5, 1, 0, 0, 0, 0);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rdo_Login);
            this.groupBox2.Controls.Add(this.rdo_Page);
            this.groupBox2.Controls.Add(this.rdo_All);
            this.groupBox2.Controls.Add(this.rdo_Action);
            this.groupBox2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.groupBox2.ForeColor = System.Drawing.Color.Navy;
            this.groupBox2.Location = new System.Drawing.Point(121, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(324, 59);
            this.groupBox2.TabIndex = 308;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Tùy chọn";
            // 
            // rdo_Login
            // 
            this.rdo_Login.AutoSize = true;
            this.rdo_Login.Checked = true;
            this.rdo_Login.Font = new System.Drawing.Font("Tahoma", 9F);
            this.rdo_Login.Location = new System.Drawing.Point(6, 17);
            this.rdo_Login.Name = "rdo_Login";
            this.rdo_Login.Size = new System.Drawing.Size(140, 18);
            this.rdo_Login.TabIndex = 209;
            this.rdo_Login.TabStop = true;
            this.rdo_Login.Text = "1. Lịch sử đăng nhập";
            this.rdo_Login.UseVisualStyleBackColor = true;
            // 
            // rdo_Page
            // 
            this.rdo_Page.AutoSize = true;
            this.rdo_Page.Font = new System.Drawing.Font("Tahoma", 9F);
            this.rdo_Page.Location = new System.Drawing.Point(152, 36);
            this.rdo_Page.Name = "rdo_Page";
            this.rdo_Page.Size = new System.Drawing.Size(171, 18);
            this.rdo_Page.TabIndex = 210;
            this.rdo_Page.Text = "4. Số lượng truy cập trang";
            this.rdo_Page.UseVisualStyleBackColor = true;
            // 
            // rdo_All
            // 
            this.rdo_All.AutoSize = true;
            this.rdo_All.Font = new System.Drawing.Font("Tahoma", 9F);
            this.rdo_All.Location = new System.Drawing.Point(152, 17);
            this.rdo_All.Name = "rdo_All";
            this.rdo_All.Size = new System.Drawing.Size(164, 18);
            this.rdo_All.TabIndex = 210;
            this.rdo_All.Text = "3.Lịch sử tất cả tài khoản";
            this.rdo_All.UseVisualStyleBackColor = true;
            // 
            // rdo_Action
            // 
            this.rdo_Action.AutoSize = true;
            this.rdo_Action.Font = new System.Drawing.Font("Tahoma", 9F);
            this.rdo_Action.Location = new System.Drawing.Point(6, 36);
            this.rdo_Action.Name = "rdo_Action";
            this.rdo_Action.Size = new System.Drawing.Size(128, 18);
            this.rdo_Action.TabIndex = 210;
            this.rdo_Action.Text = "2. Lịch sử thao tác";
            this.rdo_Action.UseVisualStyleBackColor = true;
            // 
            // btn_IT
            // 
            this.btn_IT.Location = new System.Drawing.Point(682, 17);
            this.btn_IT.Name = "btn_IT";
            this.btn_IT.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_IT.Size = new System.Drawing.Size(106, 40);
            this.btn_IT.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_IT.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_IT.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_IT.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_IT.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_IT.TabIndex = 233;
            this.btn_IT.Values.Text = "Menu IT";
            // 
            // btn_Search
            // 
            this.btn_Search.Location = new System.Drawing.Point(451, 15);
            this.btn_Search.Name = "btn_Search";
            this.btn_Search.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Search.Size = new System.Drawing.Size(106, 40);
            this.btn_Search.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Search.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Search.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Search.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Search.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Search.TabIndex = 233;
            this.btn_Search.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Search.Values.Image")));
            this.btn_Search.Values.Text = "Tìm kiếm";
            // 
            // btn_Export
            // 
            this.btn_Export.Location = new System.Drawing.Point(563, 15);
            this.btn_Export.Name = "btn_Export";
            this.btn_Export.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Export.Size = new System.Drawing.Size(113, 40);
            this.btn_Export.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Export.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Export.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Export.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Export.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Export.TabIndex = 207;
            this.btn_Export.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Export.Values.Image")));
            this.btn_Export.Values.Text = "Xuất Excel";
            // 
            // kryptonButton1
            // 
            this.kryptonButton1.Location = new System.Drawing.Point(150, 36);
            this.kryptonButton1.Name = "kryptonButton1";
            this.kryptonButton1.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonButton1.Size = new System.Drawing.Size(106, 40);
            this.kryptonButton1.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.kryptonButton1.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.kryptonButton1.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.kryptonButton1.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kryptonButton1.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.kryptonButton1.TabIndex = 233;
            this.kryptonButton1.Values.Text = "ĐN Menu";
            this.kryptonButton1.Click += new System.EventHandler(this.KryptonButton1_Click);
            // 
            // btn_Office
            // 
            this.btn_Office.Location = new System.Drawing.Point(20, 128);
            this.btn_Office.Name = "btn_Office";
            this.btn_Office.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Office.Size = new System.Drawing.Size(106, 40);
            this.btn_Office.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Office.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Office.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Office.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Office.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Office.TabIndex = 233;
            this.btn_Office.Values.Text = "ĐN Văn phòng";
            // 
            // btn_SanXuat
            // 
            this.btn_SanXuat.Location = new System.Drawing.Point(20, 36);
            this.btn_SanXuat.Name = "btn_SanXuat";
            this.btn_SanXuat.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_SanXuat.Size = new System.Drawing.Size(106, 40);
            this.btn_SanXuat.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_SanXuat.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_SanXuat.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_SanXuat.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_SanXuat.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_SanXuat.TabIndex = 233;
            this.btn_SanXuat.Values.Text = "ĐN Sản xuất";
            // 
            // btn_Support
            // 
            this.btn_Support.Location = new System.Drawing.Point(20, 82);
            this.btn_Support.Name = "btn_Support";
            this.btn_Support.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Support.Size = new System.Drawing.Size(106, 40);
            this.btn_Support.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Support.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Support.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Support.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Support.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Support.TabIndex = 233;
            this.btn_Support.Values.Text = "ĐN Hỗ trợ";
            // 
            // Panel_Left
            // 
            this.Panel_Left.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Left.Controls.Add(this.GV_User);
            this.Panel_Left.Controls.Add(this.kryptonHeader1);
            this.Panel_Left.Dock = System.Windows.Forms.DockStyle.Left;
            this.Panel_Left.Location = new System.Drawing.Point(0, 108);
            this.Panel_Left.Name = "Panel_Left";
            this.Panel_Left.Size = new System.Drawing.Size(300, 414);
            this.Panel_Left.TabIndex = 225;
            // 
            // GV_User
            // 
            this.GV_User.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None;
            this.GV_User.AllowEditing = false;
            this.GV_User.AllowResizing = C1.Win.C1FlexGrid.AllowResizingEnum.None;
            this.GV_User.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None;
            this.GV_User.AutoResize = true;
            this.GV_User.ColumnInfo = "10,1,0,0,0,95,Columns:";
            this.GV_User.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GV_User.ExtendLastCol = true;
            this.GV_User.Location = new System.Drawing.Point(0, 30);
            this.GV_User.Name = "GV_User";
            this.GV_User.Rows.DefaultSize = 19;
            this.GV_User.Size = new System.Drawing.Size(300, 384);
            this.GV_User.TabIndex = 225;
            // 
            // kryptonHeader1
            // 
            this.kryptonHeader1.AutoSize = false;
            this.kryptonHeader1.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btn_Refresh});
            this.kryptonHeader1.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader1.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader1.Name = "kryptonHeader1";
            this.kryptonHeader1.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader1.Size = new System.Drawing.Size(300, 30);
            this.kryptonHeader1.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader1.TabIndex = 223;
            this.kryptonHeader1.Values.Description = "";
            this.kryptonHeader1.Values.Heading = "Danh sách user";
            // 
            // btn_Refresh
            // 
            this.btn_Refresh.Text = "Làm mới";
            this.btn_Refresh.UniqueName = "A1C79079EB2D475AC5B6746528C5087B";
            this.btn_Refresh.Visible = false;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Location = new System.Drawing.Point(300, 108);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 414);
            this.label1.TabIndex = 227;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panel2.Controls.Add(this.Panel_Done);
            this.panel2.Controls.Add(this.GVData);
            this.panel2.Controls.Add(this.lbl_TaiKhoan);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(325, 108);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(679, 414);
            this.panel2.TabIndex = 228;
            // 
            // Panel_Done
            // 
            this.Panel_Done.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Panel_Done.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Done.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel_Done.Controls.Add(this.txt_Pass);
            this.Panel_Done.Controls.Add(this.kryptonHeader6);
            this.Panel_Done.Controls.Add(this.btn_SanXuat);
            this.Panel_Done.Controls.Add(this.btn_Support);
            this.Panel_Done.Controls.Add(this.btn_ReportDemo);
            this.Panel_Done.Controls.Add(this.kryptonButton1);
            this.Panel_Done.Controls.Add(this.btn_TimeKeeping);
            this.Panel_Done.Controls.Add(this.btn_Office);
            this.Panel_Done.Location = new System.Drawing.Point(334, 98);
            this.Panel_Done.Name = "Panel_Done";
            this.Panel_Done.Size = new System.Drawing.Size(279, 241);
            this.Panel_Done.TabIndex = 290;
            // 
            // txt_Pass
            // 
            this.txt_Pass.Location = new System.Drawing.Point(47, 189);
            this.txt_Pass.Name = "txt_Pass";
            this.txt_Pass.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_Pass.PasswordChar = '●';
            this.txt_Pass.Size = new System.Drawing.Size(187, 26);
            this.txt_Pass.StateCommon.Border.ColorAngle = 1F;
            this.txt_Pass.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_Pass.StateCommon.Border.Rounding = 4;
            this.txt_Pass.StateCommon.Border.Width = 1;
            this.txt_Pass.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Pass.TabIndex = 322;
            this.txt_Pass.UseSystemPasswordChar = true;
            // 
            // kryptonHeader6
            // 
            this.kryptonHeader6.AutoSize = false;
            this.kryptonHeader6.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnClose_Panel_Message});
            this.kryptonHeader6.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader6.HeaderStyle = ComponentFactory.Krypton.Toolkit.HeaderStyle.Secondary;
            this.kryptonHeader6.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader6.Name = "kryptonHeader6";
            this.kryptonHeader6.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader6.Size = new System.Drawing.Size(277, 30);
            this.kryptonHeader6.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.kryptonHeader6.TabIndex = 206;
            this.kryptonHeader6.Values.Description = "";
            this.kryptonHeader6.Values.Heading = "Phần cài đặt  IT";
            this.kryptonHeader6.Values.Image = null;
            // 
            // btnClose_Panel_Message
            // 
            this.btnClose_Panel_Message.Checked = ComponentFactory.Krypton.Toolkit.ButtonCheckState.Checked;
            this.btnClose_Panel_Message.Enabled = ComponentFactory.Krypton.Toolkit.ButtonEnabled.True;
            this.btnClose_Panel_Message.Type = ComponentFactory.Krypton.Toolkit.PaletteButtonSpecStyle.Close;
            this.btnClose_Panel_Message.UniqueName = "BEA2AE54CDCE44F263AB208C74E6907A";
            // 
            // btn_ReportDemo
            // 
            this.btn_ReportDemo.Location = new System.Drawing.Point(150, 82);
            this.btn_ReportDemo.Name = "btn_ReportDemo";
            this.btn_ReportDemo.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_ReportDemo.Size = new System.Drawing.Size(106, 40);
            this.btn_ReportDemo.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ReportDemo.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ReportDemo.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_ReportDemo.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ReportDemo.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ReportDemo.TabIndex = 233;
            this.btn_ReportDemo.Values.Text = "Menu BC mẫu";
            // 
            // GVData
            // 
            this.GVData.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None;
            this.GVData.AllowEditing = false;
            this.GVData.AllowResizing = C1.Win.C1FlexGrid.AllowResizingEnum.None;
            this.GVData.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None;
            this.GVData.AutoResize = true;
            this.GVData.ColumnInfo = "10,1,0,0,0,95,Columns:";
            this.GVData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GVData.ExtendLastCol = true;
            this.GVData.Location = new System.Drawing.Point(0, 30);
            this.GVData.Name = "GVData";
            this.GVData.Rows.DefaultSize = 19;
            this.GVData.Size = new System.Drawing.Size(679, 384);
            this.GVData.TabIndex = 224;
            // 
            // lbl_TaiKhoan
            // 
            this.lbl_TaiKhoan.AutoSize = false;
            this.lbl_TaiKhoan.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_TaiKhoan.Location = new System.Drawing.Point(0, 0);
            this.lbl_TaiKhoan.Name = "lbl_TaiKhoan";
            this.lbl_TaiKhoan.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.lbl_TaiKhoan.Size = new System.Drawing.Size(679, 30);
            this.lbl_TaiKhoan.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.lbl_TaiKhoan.TabIndex = 223;
            this.lbl_TaiKhoan.Values.Description = "";
            this.lbl_TaiKhoan.Values.Heading = "Thông tin tài khoản ";
            // 
            // timer1
            // 
            this.timer1.Interval = 100000;
            // 
            // btn_TimeKeeping
            // 
            this.btn_TimeKeeping.Location = new System.Drawing.Point(150, 128);
            this.btn_TimeKeeping.Name = "btn_TimeKeeping";
            this.btn_TimeKeeping.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_TimeKeeping.Size = new System.Drawing.Size(106, 40);
            this.btn_TimeKeeping.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_TimeKeeping.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_TimeKeeping.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_TimeKeeping.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TimeKeeping.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_TimeKeeping.TabIndex = 233;
            this.btn_TimeKeeping.Values.Text = "ĐN Chấm công";
            // 
            // Frm_UserLog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1004, 522);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Panel_Left);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.HeaderControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_UserLog";
            this.Text = "Frm_Log";
            this.Load += new System.EventHandler(this.Frm_UserLog_Load);
            this.panel1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.Panel_Left.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GV_User)).EndInit();
            this.panel2.ResumeLayout(false);
            this.Panel_Done.ResumeLayout(false);
            this.Panel_Done.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GVData)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ComponentFactory.Krypton.Toolkit.KryptonHeader HeaderControl;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMini;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMax;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnClose;
        private System.Windows.Forms.Panel panel1;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Search;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Export;
        private System.Windows.Forms.Panel Panel_Left;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader1;
        private C1.Win.C1FlexGrid.C1FlexGrid GV_User;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader lbl_TaiKhoan;
        private C1.Win.C1FlexGrid.C1FlexGrid GVData;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rdo_Login;
        private System.Windows.Forms.RadioButton rdo_Action;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btn_Refresh;
        private SYS.TNDateTimePicker dte_ToDate;
        private SYS.TNDateTimePicker dte_FromDate;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.RadioButton rdo_All;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Support;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Office;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_SanXuat;
        private ComponentFactory.Krypton.Toolkit.KryptonButton kryptonButton1;
        private System.Windows.Forms.Panel Panel_Done;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader6;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnClose_Panel_Message;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_IT;
        private System.Windows.Forms.RadioButton rdo_Page;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_ReportDemo;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_Pass;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_TimeKeeping;
    }
}