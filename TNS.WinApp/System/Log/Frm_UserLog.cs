﻿using C1.Win.C1FlexGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_UserLog : Form
    {
        private int _Radio = 1;
        private string _UserKey = "";
        private string _UserName = "";
        public Frm_UserLog()
        {
            InitializeComponent();
            btnClose.Click += btnClose_Click;
            btnMax.Click += btnMax_Click;
            btnMini.Click += btnMini_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            btn_Search.Click += Btn_Search_Click;
            btn_Export.Click += Btn_Export_Click;
            GV_User.Click += GV_User_DoubleClick;
            btn_Refresh.Click += Btn_Refresh_Click;
            timer1.Tick += Timer1_Tick;
            GVData.Click += GVData_Click;
            rdo_All.CheckedChanged += Rdo_All_CheckedChanged;
            rdo_Action.CheckedChanged += Rdo_Action_CheckedChanged;
            rdo_Login.CheckedChanged += Rdo_Login_CheckedChanged;
            rdo_Page.CheckedChanged += Rdo_Page_CheckedChanged;
            btn_Support.Click += Btn_Support_Click;
            btn_Office.Click += Btn_Office_Click;
            btn_SanXuat.Click += Btn_SanXuat_Click;
            btnClose_Panel_Message.Click += BtnClose_Panel_Message_Click;
            btn_IT.Click += Btn_IT_Click;
            btn_ReportDemo.Click += Btn_ReportDemo_Click;
            btn_TimeKeeping.Click += Btn_TimeKeeping_Click;
        }

       

        private void Rdo_Login_CheckedChanged(object sender, EventArgs e)
        {
            if (rdo_Login.Checked == true)
            {
                Panel_Left.Visible = true;
                label1.Visible = true;
                GVData.Rows.Count = 0;
                GVData.Cols.Count = 0;
                GVData.Clear();
                _Radio = 1;
            }
        }

        private void Rdo_Action_CheckedChanged(object sender, EventArgs e)
        {
            if (rdo_Action.Checked == true)
            {
                Panel_Left.Visible = true;
                label1.Visible = true;
                GVData.Rows.Count = 0;
                GVData.Cols.Count = 0;
                GVData.Clear();
                _Radio = 2;
            }
        }

        private void Rdo_All_CheckedChanged(object sender, EventArgs e)
        {
            if (rdo_All.Checked == true)
            {
                Panel_Left.Visible = false;
                label1.Visible = false;
                GVData.Rows.Count = 0;
                GVData.Cols.Count = 0;
                GVData.Clear();
                _Radio = 3;
            }
        }
        private void Rdo_Page_CheckedChanged(object sender, EventArgs e)
        {
            if (rdo_Page.Checked == true)
            {
                Panel_Left.Visible = true;
                label1.Visible = true;
                GVData.Rows.Count = 0;
                GVData.Cols.Count = 0;
                GVData.Clear();
                _Radio = 4;
            }
        }
        private void Frm_UserLog_Load(object sender, EventArgs e)
        {

            dte_FromDate.Value = DateTime.Now;
            dte_ToDate.Value = DateTime.Now;
            DataTable ztb = Login_Data.User();
            if (ztb.Rows.Count > 0)
                InitUser_Layout(ztb);
            timer1.Start();
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            Panel_Done.Visible = false;
            if (SessionUser.UserLogin.Key != "4e5a9e53-9241-4c9b-a69f-e86a7ae3507f")
            {
                btn_IT.Visible = false;
                rdo_Page.Visible = false;
                Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            }
            else
            {
                //Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            }
            Check_RoleForm();
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
        }
        private void Btn_ReportDemo_Click(object sender, EventArgs e)
        {
            if(txt_Pass.Text=="")
            {
                MessageBox.Show("Nhập mật khẩu");
                return;
            }
            if(txt_Pass.Text=="triviet")
            {
                Frm_MenuReport frm = new Frm_MenuReport();
                frm.Show();
            }
            else
            {
                MessageBox.Show("Mật khẩu không đúng");
            }
        }
        private void Btn_IT_Click(object sender, EventArgs e)
        {
            Panel_Done.Visible = true;
        }
        private void BtnClose_Panel_Message_Click(object sender, EventArgs e)
        {
            Panel_Done.Visible = false;
        }
        private void Btn_SanXuat_Click(object sender, EventArgs e)
        {
            if (txt_Pass.Text == "")
            {
                MessageBox.Show("Nhập mật khẩu");
                return;
            }
            if (txt_Pass.Text == "triviet")
            {
                Frm_CodeReportWorker frm = new Frm_CodeReportWorker();
                frm.Show();
            }
            else
            {
                MessageBox.Show("Mật khẩu không đúng");
            }
           
        }

        private void Btn_Support_Click(object sender, EventArgs e)
        {
            if (txt_Pass.Text == "")
            {
                MessageBox.Show("Nhập mật khẩu");
                return;
            }
            if (txt_Pass.Text == "triviet")
            {
                Frm_CodeReportSuport frm = new Frm_CodeReportSuport();
                frm.Show();
            }
            else
            {
                MessageBox.Show("Mật khẩu không đúng");
            }

        }
        private void Btn_Office_Click(object sender, EventArgs e)
        {
            if (txt_Pass.Text == "")
            {
                MessageBox.Show("Nhập mật khẩu");
                return;
            }
            if (txt_Pass.Text == "triviet")
            {
                Frm_CodeReportOffice frm = new Frm_CodeReportOffice();
                frm.Show();
            }
            else
            {
                MessageBox.Show("Mật khẩu không đúng");
            }
            
        }
        private void Btn_TimeKeeping_Click(object sender, EventArgs e)
        {
            if (txt_Pass.Text == "")
            {
                MessageBox.Show("Nhập mật khẩu");
                return;
            }
            if (txt_Pass.Text == "triviet")
            {
                Frm_CodeReport frm = new Frm_CodeReport();
                frm.Show();
            }
            else
            {
                MessageBox.Show("Mật khẩu không đúng");
            }
        }
        private void Timer1_Tick(object sender, EventArgs e)
        {
            timer1.Stop();
            GV_User.Rows.Count = 0;
            GV_User.Cols.Count = 0;
            GV_User.Clear();

            DataTable ztb = Login_Data.User();
            if (ztb.Rows.Count > 0)
                InitUser_Layout(ztb);
            timer1.Start();
        }

        private void Btn_Refresh_Click(object sender, EventArgs e)
        {

        }

        private void Btn_Search_Click(object sender, EventArgs e)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            if (dte_ToDate.Value.Month != dte_FromDate.Value.Month || dte_ToDate.Value.Year != dte_FromDate.Value.Year)
            {
                Utils.TNMessageBoxOK("Bạn chỉ được chọn trong 1 tháng", 1);
                return;
            }

            if (rdo_Login.Checked == true)
            {
                lbl_TaiKhoan.Text = "Thông tin tài khoản << " + _UserName + " >> từ " + dte_FromDate.Value.ToString("dd/MM/yyyy") + " đến " + dte_ToDate.Value.ToString("dd/MM/yyyy");
                if (_UserKey == "")
                    MessageBox.Show("Vui lòng chọn 1 tài khoản");
                else
                {
                    using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }
                }
            }
            else if (rdo_Action.Checked == true)
            {
                lbl_TaiKhoan.Text = "Thông tin tài khoản << " + _UserName + " >> từ " + dte_FromDate.Value.ToString("dd/MM/yyyy") + " đến " + dte_ToDate.Value.ToString("dd/MM/yyyy");
                if (_UserKey == "")
                    MessageBox.Show("Vui lòng chọn 1 tài khoản");
                using (Frm_Loading frm = new Frm_Loading(DisplayLogApplication)) { frm.ShowDialog(this); }
            }
            else if (rdo_All.Checked == true)
            {
                using (Frm_Loading frm = new Frm_Loading(DisplayLogAll)) { frm.ShowDialog(this); }
            }
            else if (rdo_Page.Checked == true)
            {
                lbl_TaiKhoan.Text = "Thông tin truy cập trang << " + _UserName + " >> từ " + dte_FromDate.Value.ToString("dd/MM/yyyy") + " đến " + dte_ToDate.Value.ToString("dd/MM/yyyy");
                if (_UserKey == "")
                    MessageBox.Show("Vui lòng chọn 1 tài khoản");
                using (Frm_Loading frm = new Frm_Loading(DisplayPage)) { frm.ShowDialog(this); }
            }
        }
        void InitUser_Layout(DataTable TableView)
        {
            GV_User.Rows.Count = 0;
            GV_User.Cols.Count = 0;
            GV_User.Clear();
            int TotalRow = TableView.Rows.Count + 1;
            int ToTalCol = 4;

            GV_User.Cols.Add(ToTalCol);
            GV_User.Rows.Add(1);




            //Row Header
            GV_User.Rows[0][0] = "Stt";
            GV_User.Rows[0][1] = "Tài khoản";
            GV_User.Rows[0][2] = "Tình trạng";
            GV_User.Rows[0][3] = ""; // ẩn cột này

            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                DataRow rData = TableView.Rows[rIndex];
                GV_User.Rows.Add();
                GV_User.Rows[rIndex + 1][0] = (rIndex + 1);
                GV_User.Rows[rIndex + 1][1] = rData[1];
                GV_User.Rows[rIndex + 1][3] = rData[0];
                if (rData[2].ToInt() == 1)
                {
                    GV_User.Rows[rIndex + 1][2] = "Đang online";
                    GV_User.Rows[rIndex + 1].StyleNew.ForeColor = Color.Green;
                    GV_User.Rows[rIndex + 1].StyleNew.Font = new Font("Tahoma", 11, FontStyle.Bold);
                }
                else
                {

                    GV_User.Rows[rIndex + 1].StyleNew.Font = new Font("Tahoma", 11, FontStyle.Regular);
                }
                GV_User.Rows[rIndex + 1].Height = 25;

            }

            //Style         
            GV_User.AllowFreezing = AllowFreezingEnum.Both;
            GV_User.AllowResizing = AllowResizingEnum.Both;
            GV_User.AllowMerging = AllowMergingEnum.FixedOnly;
            GV_User.SelectionMode = SelectionModeEnum.Row;
            GV_User.VisualStyle = VisualStyle.Office2010Blue;
            //GV_User.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GV_User.Styles.Normal.TextAlign = TextAlignEnum.LeftCenter;
            GV_User.Styles.Normal.WordWrap = true;

            //Freeze Row and Column                              
            GV_User.Rows.Fixed = 1;
            GV_User.Cols.Fixed = 0;

            //GV_User.Cols[0].StyleNew.BackColor = Color.Empty;
            //GV_User.Cols[1].StyleNew.BackColor = Color.Empty;
            //GVData.Cols[2].StyleNew.BackColor = Color.Empty;

            GV_User.Rows[0].TextAlign = TextAlignEnum.CenterCenter;

            GV_User.Rows[0].Height = 40;

            GV_User.Cols[0].Width = 40;
            GV_User.Cols[1].Width = 150;
            GV_User.Cols[2].Width = 100;
            GV_User.Cols[3].Visible = false;
        }

        private void GV_User_DoubleClick(object sender, EventArgs e)
        {

            if (GV_User.Rows[GV_User.RowSel][3].ToString() != "")
            {
                _UserKey = "";
                GVData.Rows.Count = 0;
                GVData.Cols.Count = 0;
                GVData.Clear();

                _UserKey = GV_User.Rows[GV_User.RowSel][3].ToString();
                _UserName = GV_User.Rows[GV_User.RowSel][1].ToString();

                if (_Radio == 1)
                {
                    if (_UserKey == "")
                        MessageBox.Show("Vui lòng chọn 1 tài khoản");
                    else
                    {
                        lbl_TaiKhoan.Text = "Thông tin tài khoản << " + _UserName + " >> từ " + dte_FromDate.Value.ToString("dd/MM/yyyy") + " đến " + dte_ToDate.Value.ToString("dd/MM/yyyy");
                        using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }
                    }
                }
                else if (_Radio == 4)
                {
                    if (_UserKey == "")
                        MessageBox.Show("Vui lòng chọn 1 tài khoản");
                    using (Frm_Loading frm = new Frm_Loading(DisplayPage)) { frm.ShowDialog(this); }
                }
                else
                {
                    if (_UserKey == "")
                        MessageBox.Show("Vui lòng chọn 1 tài khoản");
                    else if (_UserKey == "4e5a9e53-9241-4c9b-a69f-e86a7ae3507f" && SessionUser.UserLogin.Key != "4e5a9e53-9241-4c9b-a69f-e86a7ae3507f")
                    {
                        MessageBox.Show("Vui lòng chọn tài khoản khác");
                    }
                    else
                    {
                        lbl_TaiKhoan.Text = "Thông tin truy cập trang << " + _UserName + " >> từ " + dte_FromDate.Value.ToString("dd/MM/yyyy") + " đến " + dte_ToDate.Value.ToString("dd/MM/yyyy");
                        using (Frm_Loading frm = new Frm_Loading(DisplayLogApplication)) { frm.ShowDialog(this); }
                    }
                }

            }
        }
        private void DisplayData()
        {
            DataTable _Intable = Login_Data.Login_Log(dte_FromDate.Value, dte_ToDate.Value, _UserKey);
            if (_Intable.Rows.Count > 0)
            {
                this.Invoke(new MethodInvoker(delegate ()
                {
                    InitGVLogin_Layout(_Intable);

                }));
            }
        }
        void InitGVLogin_Layout(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            int TotalRow = TableView.Rows.Count + 1;
            int ToTalCol = 6;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(1);

            //Style         
            GVData.AllowFreezing = AllowFreezingEnum.Both;
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.FixedOnly;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.LeftCenter;
            GVData.Styles.Normal.WordWrap = true;

            //Freeze Row and Column                              
            GVData.Rows.Fixed = 1;
            GVData.Cols.Frozen = 0;

            //GVData.Cols[0].StyleNew.BackColor = Color.Empty;
            //GVData.Cols[1].StyleNew.BackColor = Color.Empty;
            //GVData.Cols[2].StyleNew.BackColor = Color.Empty;

            //Row Header
            GVData.Rows[0][0] = "Stt";
            GVData.Rows[0][1] = "Thời gian";
            GVData.Rows[0][2] = "Tên máy";
            GVData.Rows[0][3] = "IP";
            GVData.Rows[0][4] = "Hệ điều hành";
            GVData.Rows[0][5] = "Nội dung"; // ẩn cột này

            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                DataRow rData = TableView.Rows[rIndex];
                GVData.Rows.Add();
                GVData.Rows[rIndex + 1][0] = (rIndex + 1);
                DateTime Date = DateTime.Parse(rData[0].ToString());
                GVData.Rows[rIndex + 1][1] = Date.ToString("dd/MM/yyyy HH:mm:ss");
                GVData.Rows[rIndex + 1][2] = rData[1];
                GVData.Rows[rIndex + 1][3] = rData[2];
                GVData.Rows[rIndex + 1][4] = rData[3];
                GVData.Rows[rIndex + 1][5] = rData[4];
                if (rData[5].ToInt() == 3)
                {
                    GVData.Rows[rIndex + 1].StyleNew.ForeColor = Color.Gray;
                }
                else if (rData[5].ToInt() == 2)
                {
                    GVData.Rows[rIndex + 1].StyleNew.ForeColor = Color.Red;
                }

            }



            GVData.Rows[0].TextAlign = TextAlignEnum.CenterCenter;

            GVData.Rows[0].Height = 40;

            GVData.Cols[0].Width = 35;
            GVData.Cols[1].Width = 150;
            GVData.Cols[2].Width = 150;
            GVData.Cols[3].Width = 200;
            GVData.Cols[4].Width = 250;
            //GVData.Cols[2].Visible = false;
        }

        private void DisplayLogApplication()
        {
            DataTable _Intable = Application_Data.List(dte_FromDate.Value, dte_ToDate.Value, _UserKey);
            if (_Intable.Rows.Count > 0)
            {
                this.Invoke(new MethodInvoker(delegate ()
                {
                    InitGVLogApplication_Layout(_Intable);

                }));
            }
        }
        void InitGVLogApplication_Layout(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            int TotalRow = TableView.Rows.Count + 1;
            int ToTalCol = 5;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(1);

            //Style         
            GVData.AllowFreezing = AllowFreezingEnum.Both;
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.FixedOnly;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 8, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.LeftCenter;
            GVData.Styles.Normal.WordWrap = true;

            //Freeze Row and Column                              
            GVData.Rows.Fixed = 1;
            GVData.Cols.Frozen = 0;

            //GVData.Cols[0].StyleNew.BackColor = Color.Empty;
            //GVData.Cols[1].StyleNew.BackColor = Color.Empty;
            //GVData.Cols[2].StyleNew.BackColor = Color.Empty;

            //Row Header
            GVData.Rows[0][0] = "Stt";
            GVData.Rows[0][1] = "Thời gian";
            GVData.Rows[0][2] = "Nội dung";
            GVData.Rows[0][3] = ""; // ẩn cột này
            GVData.Rows[0][4] = ""; // ẩn cột này

            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                DataRow rData = TableView.Rows[rIndex];
                GVData.Rows.Add();
                GVData.Rows[rIndex + 1][0] = (rIndex + 1);
                DateTime Date = DateTime.Parse(rData[0].ToString());
                GVData.Rows[rIndex + 1][1] = Date.ToString("dd/MM/yyyy HH:mm:ss");
                if (rData[2].ToInt() == 1)
                {
                    GVData.Rows[rIndex + 1][2] = "*" + rData[1];
                    GVData.Rows[rIndex + 1].StyleNew.ForeColor = Color.Blue;
                    GVData.Rows[rIndex + 1].StyleNew.Font = new Font("Tahoma", 8, FontStyle.Bold);
                }
                if (rData[2].ToInt() == 2)
                {
                    GVData.Rows[rIndex + 1][2] = "    >> " + rData[1];
                    GVData.Rows[rIndex + 1].StyleNew.ForeColor = Color.Green;
                }
                if (rData[2].ToInt() == 3)
                {
                    GVData.Rows[rIndex + 1][2] = "        >>> " + rData[1];
                    if (rData[4].ToString() != "" && rData[4].ToString() != "0")
                        GVData.Rows[rIndex + 1].StyleNew.Font = new Font("Tahoma", 8, FontStyle.Bold);
                }
                GVData.Rows[rIndex + 1][3] = rData[3];
                GVData.Rows[rIndex + 1][4] = rData[4];
                if (rData[1].ToString().Length > 150)
                    GVData.Rows[rIndex + 1].Height = 35;
            }



            GVData.Rows[0].TextAlign = TextAlignEnum.CenterCenter;

            GVData.Rows[0].Height = 40;

            GVData.Cols[0].Width = 35;
            GVData.Cols[1].Width = 130;
            GVData.Cols[2].Width = 200;

            GVData.Cols[3].Visible = false;
            GVData.Cols[4].Visible = false;
        }
        private void GVData_Click(object sender, EventArgs e)
        {
            if (_Radio != 4)
            {
                if (GVData.Rows.Count > 0 && GVData.Rows[GVData.RowSel][4].ToString() != "" && GVData.Rows[GVData.RowSel][4].ToString() != "0")
                {
                    if (rdo_Action.Checked == true)
                    {
                        Frm_LogText frm = new Frm_LogText();
                        frm.Key = GVData.Rows[GVData.RowSel][3].ToString();
                        frm.Show();
                    }
                }
            }
        }

        private void DisplayLogAll()
        {
            DataTable _Intable = Application_Data.ListLogAll(dte_FromDate.Value, dte_ToDate.Value);
            if (_Intable.Rows.Count > 0)
            {
                this.Invoke(new MethodInvoker(delegate ()
                {
                    InitGVLogAll_Layout(_Intable);

                }));
            }
        }
        void InitGVLogAll_Layout(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            int TotalRow = TableView.Rows.Count + 1;
            int ToTalCol = 6;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(1);

            //Style         
            GVData.AllowFreezing = AllowFreezingEnum.Both;
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.FixedOnly;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 8, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.LeftCenter;
            GVData.Styles.Normal.WordWrap = true;

            //Freeze Row and Column                              
            GVData.Rows.Fixed = 1;
            GVData.Cols.Frozen = 0;

            //GVData.Cols[0].StyleNew.BackColor = Color.Empty;
            //GVData.Cols[1].StyleNew.BackColor = Color.Empty;
            //GVData.Cols[2].StyleNew.BackColor = Color.Empty;

            //Row Header
            GVData.Rows[0][0] = "Stt";
            GVData.Rows[0][1] = "Thời gian";
            GVData.Rows[0][2] = "Tài khoản";
            GVData.Rows[0][3] = "Nội dung";
            GVData.Rows[0][4] = ""; // ẩn cột này
            GVData.Rows[0][5] = ""; // ẩn cột này

            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                DataRow rData = TableView.Rows[rIndex];
                GVData.Rows.Add();
                GVData.Rows[rIndex + 1][0] = (rIndex + 1);
                DateTime Date = DateTime.Parse(rData[0].ToString());
                GVData.Rows[rIndex + 1][1] = Date.ToString("dd/MM/yyyy HH:mm:ss");
                GVData.Rows[rIndex + 1][2] = rData[1];
                if (rData[3].ToInt() == 1)
                {
                    GVData.Rows[rIndex + 1][3] = "*" + rData[2];
                    GVData.Rows[rIndex + 1].StyleNew.ForeColor = Color.Blue;
                    GVData.Rows[rIndex + 1].StyleNew.Font = new Font("Tahoma", 8, FontStyle.Bold);
                }
                else if (rData[3].ToInt() == 2)
                {
                    GVData.Rows[rIndex + 1][3] = "    >> " + rData[2];
                    GVData.Rows[rIndex + 1].StyleNew.ForeColor = Color.Green;
                }
                else if (rData[3].ToInt() == 3)
                {
                    GVData.Rows[rIndex + 1][3] = "        >>> " + rData[2];
                    //if (rData[4].ToString() != "" && rData[4].ToString() != "0")
                    //GVData.Rows[rIndex + 1].StyleNew.Font = new Font("Tahoma", 8, FontStyle.Bold);
                }
                else
                {
                    GVData.Rows[rIndex + 1][3] = "*" + rData[2];
                    GVData.Rows[rIndex + 1].StyleNew.ForeColor = Color.Maroon;
                }
                GVData.Rows[rIndex + 1][4] = rData[4];
                GVData.Rows[rIndex + 1][5] = rData[5];
                if (rData[1].ToString().Length > 150)
                    GVData.Rows[rIndex + 1].Height = 35;
            }



            GVData.Rows[0].TextAlign = TextAlignEnum.CenterCenter;

            GVData.Rows[0].Height = 40;

            GVData.Cols[0].Width = 35;
            GVData.Cols[1].Width = 130;
            GVData.Cols[2].Width = 200;

            GVData.Cols[4].Visible = false;
            GVData.Cols[5].Visible = false;
        }

        private void DisplayPage()
        {
            DataTable _Intable = Login_Data.Login_Page(dte_FromDate.Value, dte_ToDate.Value, _UserKey);
            if (_Intable.Rows.Count > 0)
            {
                this.Invoke(new MethodInvoker(delegate ()
                {
                    InitGVPage_Layout(_Intable);

                }));
            }
        }
        void InitGVPage_Layout(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            int TotalRow = TableView.Rows.Count + 1;
            int ToTalCol = 3;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(2);

            //Style         
            GVData.AllowFreezing = AllowFreezingEnum.Both;
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.FixedOnly;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.LeftCenter;
            GVData.Styles.Normal.WordWrap = true;

            //Freeze Row and Column                              
            GVData.Rows.Fixed = 1;
            GVData.Cols.Frozen = 0;

            //GVData.Cols[0].StyleNew.BackColor = Color.Empty;
            //GVData.Cols[1].StyleNew.BackColor = Color.Empty;
            //GVData.Cols[2].StyleNew.BackColor = Color.Empty;

            //Row Header
            GVData.Rows[0][0] = "Stt";
            GVData.Rows[0][1] = "Số lần truy cập";
            GVData.Rows[0][2] = "Tên trang truy cập";


            int zTotal = 0;
            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                DataRow rData = TableView.Rows[rIndex];
                GVData.Rows.Add();
                GVData.Rows[rIndex + 2][0] = (rIndex + 1);
                int Number = int.Parse(rData[0].ToString());
                GVData.Rows[rIndex + 2][1] = Number.ToString("n0");
                GVData.Rows[rIndex + 2][2] = rData[1];
                zTotal += Number;
            }

            GVData.Rows[1][0] = "";
            GVData.Rows[1][1] = "Tổng cộng: " + zTotal;
            GVData.Rows[1][2] = "";
            GVData.Rows[1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);

            GVData.Rows[0].TextAlign = TextAlignEnum.CenterCenter;

            GVData.Rows[0].Height = 40;

            GVData.Cols[0].Width = 35;
            GVData.Cols[1].Width = 150;
            GVData.Cols[1].TextAlign = TextAlignEnum.CenterCenter;
            GVData.Cols[2].Width = 150;
        }

        private void Btn_Export_Click(object sender, EventArgs e)
        {
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Lịch sử truy cập" + dte_ToDate.Value.ToString("dd-MM-yyyy") + ".xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        MessageBox.Show("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }

                    string Status = "Xuất Excel form " + HeaderControl.Text + " > đường dẫn và tên file xuất:" + FDialog.FileName;
                    Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);

                    GVData.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }

        }

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Normal)
            {
                this.WindowState = FormWindowState.Maximized;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
                this.StartPosition = FormStartPosition.CenterScreen;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        private void KryptonButton1_Click(object sender, EventArgs e)
        {
            Frm_Menu frm = new Frm_Menu();
            frm.Show();
        }
        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
                btn_Export.Enabled = false;
            }

        }
        #endregion
    }
}
