﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Role : Form
    {
        private string _Message_Menu = "";
        private string _Message_Role = "";
        private DataTable _Table = new DataTable();
        private Access_Menu_Info _Access_Menu;
        private string _MenuID = "";

        public Frm_Role()
        {
            InitializeComponent();


            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            Utils.DrawGVStyle(ref GV_Role);
            Utils.DrawGVStyle(ref GV_Role_Menu);

            btn_Save.Click += Btn_Save_Click;
            GV_Role_Menu.CellClick += GV_Role_Menu_CellClick;
            GV_Role.CellPainting += GV_Role_CellPainting;
            GV_Role.CellValueChanged += GV_Role_CellValueChanged;
            GV_Role.CellMouseUp += GV_Role_CellMouseUp;
            txt_User.KeyUp += Txt_User_KeyUp;
            LV_User.ItemActivate += LV_User_ItemActivate;
            LV_User.ItemSelectionChanged += LV_User_ItemSelectionChanged;
            txt_User.Leave += Txt_User_Leave;
            txt_User.KeyDown += Txt_User_KeyDown;
            LayoutUser(LV_User);
        }


        private void Frm_Role_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);

            LV_User.Hide();
        }
        #region ---- Design Layout DataGridView----
        private void GV_Role_Menu_Layout()
        {
            DataGridViewCheckBoxColumn zColumn;

            // Setup Column 
            GV_Role_Menu.Columns.Add("No", "STT");
            GV_Role_Menu.Columns.Add("Role_Name", "Tên Chức Năng");

            zColumn = new DataGridViewCheckBoxColumn();
            zColumn.Name = "Read";
            zColumn.HeaderText = "Truy Cập";
            GV_Role_Menu.Columns.Add(zColumn);

            GV_Role_Menu.Columns["No"].Width = 40;
            GV_Role_Menu.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV_Role_Menu.Columns["Role_Name"].Width = 250;
            GV_Role_Menu.Columns["Role_Name"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV_Role_Menu.Columns["Role_Name"].ReadOnly = true;
            GV_Role_Menu.Columns["Role_Name"].Frozen = true;

            GV_Role_Menu.Columns["Read"].Width = 120;
            GV_Role_Menu.Columns["Read"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;    
        }
        private void GV_Role_Menu_LoadData()
        {
            GV_Role_Menu.Rows.Clear();
            _Table = User_Data.Get_User_Menu(txt_User.Tag.ToString());

            int i = 0;
            foreach (DataRow nRow in _Table.Rows)
            {
                GV_Role_Menu.Rows.Add();
                DataGridViewRow nRowView = GV_Role_Menu.Rows[i];
                nRowView.Cells["No"].Value = (i + 1).ToString();
                nRowView.Tag = nRow["MenuKey"].ToString().Trim();
                nRowView.Cells["Role_Name"].Tag = nRow["MenuID"].ToString().Trim();
                nRowView.Cells["Role_Name"].Value = nRow["MenuName"].ToString().Trim();
                if (nRow["IsAccess"].ToString() == "1")
                {
                    nRowView.Cells["Read"].Value = true;
                }
                i++;
            }
        }

        private void GV_Role_Layout()
        {
            DataGridViewCheckBoxColumn zColumn;

            // Setup Column 
            GV_Role.Columns.Add("No", "STT");
            GV_Role.Columns.Add("Role_Name", "Tên Chức Năng");

            zColumn = new DataGridViewCheckBoxColumn();
            zColumn.Name = "All";
            zColumn.HeaderText = "Tất cả";
            GV_Role.Columns.Add(zColumn);

            zColumn = new DataGridViewCheckBoxColumn();
            zColumn.Name = "Read";
            zColumn.HeaderText = "Xem";
            GV_Role.Columns.Add(zColumn);

            zColumn = new DataGridViewCheckBoxColumn();
            zColumn.Name = "Add";
            zColumn.HeaderText = "Thêm";
            GV_Role.Columns.Add(zColumn);

            zColumn = new DataGridViewCheckBoxColumn();
            zColumn.Name = "Edit";
            zColumn.HeaderText = "Sửa";
            GV_Role.Columns.Add(zColumn);

            zColumn = new DataGridViewCheckBoxColumn();
            zColumn.Name = "Delete";
            zColumn.HeaderText = "Xóa";
            GV_Role.Columns.Add(zColumn);

            zColumn = new DataGridViewCheckBoxColumn();
            zColumn.Name = "Approve";
            zColumn.HeaderText = "Duyệt";
            GV_Role.Columns.Add(zColumn);

            GV_Role.Columns["No"].Width = 40;
            GV_Role.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV_Role.Columns["Role_Name"].Width = 215;
            GV_Role.Columns["Role_Name"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV_Role.Columns["Role_Name"].Frozen = true;

            GV_Role.Columns["Read"].Width = 70;
            GV_Role.Columns["Read"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV_Role.Columns["Add"].Width = 70;
            GV_Role.Columns["Add"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV_Role.Columns["Edit"].Width = 70;
            GV_Role.Columns["Edit"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV_Role.Columns["Delete"].Width = 70;
            GV_Role.Columns["Delete"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV_Role.Columns["Approve"].Width = 70;
            GV_Role.Columns["Approve"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
          
        }
        private void GV_Role_LoadData()
        {
            GV_Role.Rows.Clear();
            _Table = User_Data.Get_User_Role(txt_User.Tag.ToString(), _MenuID);

            int i = 0;
            foreach (DataRow nRow in _Table.Rows)
            {
                GV_Role.Rows.Add();
                DataGridViewRow nRowView = GV_Role.Rows[i];
                nRowView.Cells["No"].Value = (i + 1).ToString();
                nRowView.Tag = nRow["RoleKey"].ToString().Trim();
                nRowView.Cells["Role_Name"].Value = nRow["RoleName"].ToString().Trim();
                nRowView.Cells["Role_Name"].Tag = nRow["RoleID"].ToString().Trim();
                if (nRow["Style"].ToString() == "2")
                {
                    nRowView.Cells["Add"].ReadOnly = true;
                    nRowView.Cells["Edit"].ReadOnly = true;
                    nRowView.Cells["Delete"].ReadOnly = true;
                }
                if (nRow["Style"].ToString() == "1")
                {
                    nRowView.Cells["Approve"].ReadOnly = true;
                }
                if (nRow["RoleRead"].ToString() == "1")
                {
                    nRowView.Cells["Read"].Value = true;
                }
                if (nRow["RoleAdd"].ToString() == "1")
                {
                    nRowView.Cells["Add"].Value = true;
                }
                if (nRow["RoleEdit"].ToString() == "1")
                {
                    nRowView.Cells["Edit"].Value = true;
                }
                if (nRow["RoleDel"].ToString() == "1")
                {
                    nRowView.Cells["Delete"].Value = true;
                }
                if (nRow["RoleApprove"].ToString() == "1")
                {
                    nRowView.Cells["Approve"].Value = true;
                }
                i++;
            }
        }
        private void GV_Role_Menu_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (GV_Role_Menu.Rows[e.RowIndex].Tag != null)
            {
                GV_Role.Visible = true;
                GV_Role.Columns.Clear();
                GV_Role_Layout();
                _MenuID = GV_Role_Menu.Rows[e.RowIndex].Cells["Role_Name"].Tag.ToString();
                GV_Role_LoadData();
            }
            else
            {
                GV_Role.Visible = false;
            }
        }
        private void GV_Role_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                if (GV_Role.Columns[e.ColumnIndex].Name == "Approve" && GV_Role.Rows[e.RowIndex].Cells["Approve"].ReadOnly == true)
                {
                    e.PaintBackground(e.ClipBounds, true);
                    e.Handled = true;
                }
                if (GV_Role.Columns[e.ColumnIndex].Name == "Add" && GV_Role.Rows[e.RowIndex].Cells["Add"].ReadOnly == true)
                {
                    e.PaintBackground(e.ClipBounds, true);
                    e.Handled = true;
                }
                if (GV_Role.Columns[e.ColumnIndex].Name == "Edit" && GV_Role.Rows[e.RowIndex].Cells["Edit"].ReadOnly == true)
                {
                    e.PaintBackground(e.ClipBounds, true);
                    e.Handled = true;
                }
                if (GV_Role.Columns[e.ColumnIndex].Name == "Delete" && GV_Role.Rows[e.RowIndex].Cells["Delete"].ReadOnly == true)
                {
                    e.PaintBackground(e.ClipBounds, true);
                    e.Handled = true;
                }
            }
        }
        private void GV_Role_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow zRowEdit = GV_Role.Rows[e.RowIndex];
            if (e.ColumnIndex == 2 && e.RowIndex != -1)
            {
                if (Convert.ToBoolean(zRowEdit.Cells[e.ColumnIndex].Value) == true)
                {
                    zRowEdit.Cells["Read"].Value = true;
                    zRowEdit.Cells["Add"].Value = true;
                    zRowEdit.Cells["Edit"].Value = true;
                    zRowEdit.Cells["Delete"].Value = true;
                    zRowEdit.Cells["Approve"].Value = true;
                }
                else
                {
                    zRowEdit.Cells["Read"].Value = false;
                    zRowEdit.Cells["Add"].Value = false;
                    zRowEdit.Cells["Edit"].Value = false;
                    zRowEdit.Cells["Delete"].Value = false;
                    zRowEdit.Cells["Approve"].Value = false;
                }
            }
        }
        private void GV_Role_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            GV_Role.EndEdit();
        }
        #endregion

        #region ---- Process Data ----
        private void Save_Menu()
        {
            string zMessage = "";
            int n = GV_Role_Menu.Rows.Count;
            for (int i = 0; i < n - 1; i++)
            {
                if (GV_Role_Menu.Rows[i].Tag != null)
                {
                    _Access_Menu = new Access_Menu_Info(int.Parse(GV_Role_Menu.Rows[i].Tag.ToString()), txt_User.Tag.ToString());
                    _Access_Menu.MenuKey = int.Parse(GV_Role_Menu.Rows[i].Tag.ToString());
                    _Access_Menu.UserKey = txt_User.Tag.ToString();
                    if (GV_Role_Menu.Rows[i].Cells["Read"].Value != null)
                    {
                        if (Convert.ToBoolean(GV_Role_Menu.Rows[i].Cells["Read"].Value) == true)
                        {
                            _Access_Menu.IsAccess = 1;
                        }
                        else
                        {
                            _Access_Menu.IsAccess = 0;
                        }
                    }

                    int zFind = Access_Menu_Data.Find_User(txt_User.Tag.ToString(), int.Parse(GV_Role_Menu.Rows[i].Tag.ToString()));
                    if (zFind == 0)
                    {
                        _Access_Menu.CreatedBy = SessionUser.UserLogin.Key;
                        _Access_Menu.CreatedName = SessionUser.UserLogin.EmployeeName;
                        _Access_Menu.Create();
                        zMessage += _Access_Menu.Message;
                    }
                    if (zFind > 0)
                    {
                        _Access_Menu.ModifiedBy = SessionUser.UserLogin.Key;
                        _Access_Menu.ModifiedName = SessionUser.UserLogin.EmployeeName;
                        _Access_Menu.Update();
                        zMessage += _Access_Menu.Message;
                    }
                }
            }
            _Message_Menu = zMessage;
        }
        private void Save_Role()
        {
            string zMessage = "";
            int n = GV_Role.Rows.Count;
            for (int i = 0; i < n - 1; i++)
            {
                if (GV_Role.Rows[i].Tag != null)
                {
                    Access_Role_Info zAccess_Role = new Access_Role_Info(txt_User.Tag.ToString(), GV_Role.Rows[i].Tag.ToString());
                    zAccess_Role.UserKey = txt_User.Tag.ToString();
                    zAccess_Role.RoleKey = GV_Role.Rows[i].Tag.ToString();
                    zAccess_Role.RoleID = GV_Role.Rows[i].Cells["Role_Name"].Tag.ToString();
                    if (GV_Role.Rows[i].Cells["Read"].Value != null)
                    {
                        if (Convert.ToBoolean(GV_Role.Rows[i].Cells["Read"].Value) == true)
                        {
                            zAccess_Role.RoleRead = 1;
                        }
                        else
                        {
                            zAccess_Role.RoleRead = 0;
                        }
                    }
                    if (GV_Role.Rows[i].Cells["Add"].Value != null)
                    {
                        if (Convert.ToBoolean(GV_Role.Rows[i].Cells["Add"].Value) == true)
                        {
                            zAccess_Role.RoleAdd = 1;
                        }
                        else
                        {
                            zAccess_Role.RoleAdd = 0;
                        }
                    }
                    if (GV_Role.Rows[i].Cells["Edit"].Value != null)
                    {
                        if (Convert.ToBoolean(GV_Role.Rows[i].Cells["Edit"].Value) == true)
                        {
                            zAccess_Role.RoleEdit = 1;
                        }
                        else
                        {
                            zAccess_Role.RoleEdit = 0;
                        }
                    }
                    if (GV_Role.Rows[i].Cells["Delete"].Value != null)
                    {
                        if (Convert.ToBoolean(GV_Role.Rows[i].Cells["Delete"].Value) == true)
                        {
                            zAccess_Role.RoleDel = 1;
                        }
                        else
                        {
                            zAccess_Role.RoleDel = 0;
                        }
                    }
                    if (GV_Role.Rows[i].Cells["Approve"].Value != null)
                    {
                        if (Convert.ToBoolean(GV_Role.Rows[i].Cells["Approve"].Value) == true)
                        {
                            zAccess_Role.RoleApprove = 1;
                        }
                        else
                        {
                            zAccess_Role.RoleApprove = 0;
                        }
                    }
                    int zFind = Acccess_Role_Data.Find_User(txt_User.Tag.ToString(), GV_Role.Rows[i].Tag.ToString());
                    if (zFind == 0)
                    {
                        zAccess_Role.CreatedBy = SessionUser.UserLogin.Key;
                        zAccess_Role.CreatedName = SessionUser.UserLogin.EmployeeName;
                        zAccess_Role.ModifiedBy = SessionUser.UserLogin.Key;
                        zAccess_Role.ModifiedName = SessionUser.UserLogin.EmployeeName;
                        zAccess_Role.Create();
                        zMessage += zAccess_Role.Message;
                    }
                    if (zFind > 0)
                    {
                        zAccess_Role.ModifiedBy = SessionUser.UserLogin.Key;
                        zAccess_Role.ModifiedName = SessionUser.UserLogin.EmployeeName;
                        zAccess_Role.Update();
                        zMessage += zAccess_Role.Message;
                    }
                }
            }
            _Message_Role = zMessage;
        }
        #endregion

        #region ---- Design Layout ListView -----
        private void LayoutUser(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 50;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên Tài Khoản";
            colHead.Width = 220;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);
        }
        private void LoadDataUser()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LV_User;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            DataTable In_Table = new DataTable();

            In_Table = User_Data.User(txt_User.Text);

            LV.Items.Clear();
            int n = In_Table.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                DataRow nRow = In_Table.Rows[i];
                lvi.Tag = nRow["UserKey"].ToString();
                lvi.BackColor = Color.White;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["UserName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);

            }

            this.Cursor = Cursors.Default;
        }
        private void Txt_User_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txt_User.Tag = LV_User.SelectedItems[0].Tag;
                LV_User.Visible = false;
            }
        }
        private void Txt_User_Leave(object sender, EventArgs e)
        {
            if (!LV_User.Focused)
                LV_User.Visible = false;

        }
        private void LV_User_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            txt_User.Tag = LV_User.Items[e.ItemIndex].Tag.ToString();
            txt_User.Text = LV_User.Items[e.ItemIndex].SubItems[1].Text.Trim();
            if (txt_User.Tag != null)
            {
                GV_Role_Menu.Columns.Clear();
                GV_Role_Menu_Layout();
                GV_Role_Menu_LoadData();
            }
        }
        private void LV_User_ItemActivate(object sender, EventArgs e)
        {
            txt_User.Tag = LV_User.SelectedItems[0].Tag;
            LV_User.Visible = false;
            txt_User.Focus();
        }
        private void Txt_User_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                LV_User.Focus();
                if (LV_User.Items.Count > 0)
                    LV_User.Items[0].Selected = true;
            }
            else
            {
                if (e.KeyCode != Keys.Enter)
                {
                    if (txt_User.Text.Length > 0)
                    {
                        LV_User.Visible = true;
                        LoadDataUser();
                    }
                    else
                        LV_User.Visible = false;
                }
            }
        }
        #endregion

        #region Process Event
        private void Btn_Save_Click(object sender, EventArgs e)
        {
            if (txt_User.Tag.ToString().Length != 0)
            {
                Save_Menu();
                if (_Message_Menu.Substring(0, 2) == "11" || _Message_Menu.Substring(0, 2) == "20")
                {
                    if (GV_Role.Rows.Count > 0)
                    {
                        Save_Role();
                        if (_Message_Menu.Substring(0, 2) == "11" || _Message_Role.Substring(0, 2) == "11" ||
                            _Message_Menu.Substring(0, 2) == "20" || _Message_Role.Substring(0, 2) == "20")
                        {
                            MessageBox.Show("Cập nhật thành công", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            MessageBox.Show("Vui lòng kiểm tra lại", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Cập nhật thành công", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    MessageBox.Show("Vui lòng kiểm tra lại", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Vui lòng kiểm tra lại", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
