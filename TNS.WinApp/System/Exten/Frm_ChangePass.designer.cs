﻿namespace TNS.WinApp
{
    partial class Frm_ChangePass
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_ChangePass));
            this.HeaderControl = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btnMini1 = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnMax1 = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnClose = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_Change = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.txt_Replay = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txt_PasswordNew = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txt_PasswordOld = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // HeaderControl
            // 
            this.HeaderControl.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnMini1,
            this.btnMax1,
            this.btnClose});
            this.HeaderControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.HeaderControl.Location = new System.Drawing.Point(0, 0);
            this.HeaderControl.Name = "HeaderControl";
            this.HeaderControl.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.HeaderControl.Size = new System.Drawing.Size(389, 42);
            this.HeaderControl.TabIndex = 220;
            this.HeaderControl.Values.Description = "";
            this.HeaderControl.Values.Heading = "Đổi mật khẩu";
            this.HeaderControl.Values.Image = null;
            // 
            // btnMini1
            // 
            this.btnMini1.Image = ((System.Drawing.Image)(resources.GetObject("btnMini1.Image")));
            this.btnMini1.UniqueName = "F5F06E8241504E72ABB5BEA3F6A9B753";
            this.btnMini1.Visible = false;
            // 
            // btnMax1
            // 
            this.btnMax1.Image = ((System.Drawing.Image)(resources.GetObject("btnMax1.Image")));
            this.btnMax1.UniqueName = "035D1A4881E44F58A084C31DE7352A94";
            this.btnMax1.Visible = false;
            // 
            // btnClose
            // 
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.UniqueName = "11B07C6F4E1C4F9D8B91BD924CB0EBE6";
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.btn_Change);
            this.panel1.Controls.Add(this.txt_Replay);
            this.panel1.Controls.Add(this.txt_PasswordNew);
            this.panel1.Controls.Add(this.txt_PasswordOld);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 42);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(389, 155);
            this.panel1.TabIndex = 306;
            // 
            // btn_Change
            // 
            this.btn_Change.Location = new System.Drawing.Point(142, 105);
            this.btn_Change.Name = "btn_Change";
            this.btn_Change.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Change.Size = new System.Drawing.Size(120, 40);
            this.btn_Change.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Change.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Change.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Change.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Change.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Change.TabIndex = 319;
            this.btn_Change.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Change.Values.Image")));
            this.btn_Change.Values.Text = "Cập nhật";
            // 
            // txt_Replay
            // 
            this.txt_Replay.Location = new System.Drawing.Point(142, 72);
            this.txt_Replay.Name = "txt_Replay";
            this.txt_Replay.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_Replay.PasswordChar = '●';
            this.txt_Replay.Size = new System.Drawing.Size(200, 26);
            this.txt_Replay.StateCommon.Border.ColorAngle = 1F;
            this.txt_Replay.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_Replay.StateCommon.Border.Rounding = 4;
            this.txt_Replay.StateCommon.Border.Width = 1;
            this.txt_Replay.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Replay.TabIndex = 316;
            this.txt_Replay.UseSystemPasswordChar = true;
            // 
            // txt_PasswordNew
            // 
            this.txt_PasswordNew.Location = new System.Drawing.Point(142, 42);
            this.txt_PasswordNew.Name = "txt_PasswordNew";
            this.txt_PasswordNew.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_PasswordNew.PasswordChar = '●';
            this.txt_PasswordNew.Size = new System.Drawing.Size(200, 26);
            this.txt_PasswordNew.StateCommon.Border.ColorAngle = 1F;
            this.txt_PasswordNew.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_PasswordNew.StateCommon.Border.Rounding = 4;
            this.txt_PasswordNew.StateCommon.Border.Width = 1;
            this.txt_PasswordNew.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PasswordNew.TabIndex = 317;
            this.txt_PasswordNew.UseSystemPasswordChar = true;
            // 
            // txt_PasswordOld
            // 
            this.txt_PasswordOld.Location = new System.Drawing.Point(142, 13);
            this.txt_PasswordOld.Name = "txt_PasswordOld";
            this.txt_PasswordOld.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_PasswordOld.PasswordChar = '●';
            this.txt_PasswordOld.Size = new System.Drawing.Size(200, 26);
            this.txt_PasswordOld.StateCommon.Border.ColorAngle = 1F;
            this.txt_PasswordOld.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_PasswordOld.StateCommon.Border.Rounding = 4;
            this.txt_PasswordOld.StateCommon.Border.Width = 1;
            this.txt_PasswordOld.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PasswordOld.TabIndex = 318;
            this.txt_PasswordOld.UseSystemPasswordChar = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(20, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 15);
            this.label2.TabIndex = 315;
            this.label2.Text = "Xác Nhận Mật Khẩu :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(53, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 15);
            this.label1.TabIndex = 314;
            this.label1.Text = "Mật Khẩu Mới :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(57, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 15);
            this.label3.TabIndex = 313;
            this.label3.Text = "Mật Khẩu Cũ :";
            // 
            // Frm_ChangePass
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(389, 197);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.HeaderControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_ChangePass";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Đổi Mật Khẩu";
            this.Load += new System.EventHandler(this.Frm_ChangePass_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private ComponentFactory.Krypton.Toolkit.KryptonHeader HeaderControl;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMini1;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMax1;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnClose;
        private System.Windows.Forms.Panel panel1;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Change;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_Replay;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_PasswordNew;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_PasswordOld;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
    }
}