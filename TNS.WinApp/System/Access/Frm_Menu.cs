﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Menu : Form
    {
        private int _Key = 0;
        public Frm_Menu()
        {
            InitializeComponent();
            btnClose.Click += btnClose_Click;
            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;
            treeView1.NodeMouseClick += TreeView1_NodeMouseClick;
            btn_Save.Click += Btn_Save_Click;
            btn_New.Click += Btn_New_Click;
            btn_Del.Click += Btn_Del_Click;
            btn_Search.Click += Btn_Search_Click;
        }

        private void TreeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Node != null)
            {
                _Key = e.Node.Tag.ToInt();
                LoadData();
            }
        }

        private void Frm_Menu_Load(object sender, EventArgs e)
        {
            LoadDataToToolbox.KryptonComboBox(cbo_menu, "select MenuKey,MenuName from [dbo].[SYS_Menu] where RecordStatus <> 99 AND MenuLevel=0 ORDER BY Rank", "--Menu chính--");
            LoadList();
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
        }
        #region[Progess]
        private void LoadList()
        {
            treeView1.Nodes.Clear();

            DataTable zMain = Menu_Data.MenuMain();
            DataTable zSub = Menu_Data.MenuSub();
            TreeNode root = new TreeNode("Danh sách menu", 0, 0);
            root.NodeFont = new Font("Tahoma", 9, FontStyle.Bold);
            root.Tag = 0;
            root.ImageIndex = 0;
            int CountMain = 1;
            int CountSub = 1;
            root.ExpandAll();
            foreach (DataRow r in zMain.Rows)
            {
                CountSub = 1;
                TreeNode child = new TreeNode(CountMain + " - " + "[ " +r["MenuID"].ToString() + "] - " + r["MenuName"].ToString(), 1, 1);
                child.Tag = r["MenuKey"].ToString();
                child.NodeFont = new Font("Tahoma", 9, FontStyle.Bold);
                root.Nodes.Add(child);
                if (zSub.Rows.Count > 0)
                {
                    DataRow[] Array1 = zSub.Select("[Parent]= '"+ r["MenuKey"].ToString() + "'");
                    if (Array1.Length > 0)
                    {
                        DataTable zGroup1 = Array1.CopyToDataTable();
                        foreach (DataRow r1 in zGroup1.Rows)
                        {
                            TreeNode child1 = new TreeNode(CountMain + "." + CountSub + " - " + "[ " +r1["MenuID"].ToString() + " ] - " + r1["MenuName"].ToString(), 2, 2);
                            child1.Tag = r1["MenuKey"].ToString();
                            child.Nodes.Add(child1);
                            CountSub++;
                        }
                    }
                }
                CountMain++;
            }
            treeView1.Nodes.Add(root);
        }
        
        private void LoadData()
        {

            Menu_Info zinfo = new Menu_Info(_Key);
            txt_ID.Text = zinfo.MenuID;
            txt_Name.Text = zinfo.MenuName;
            cbo_menu.SelectedValue = zinfo.Parent;
            txt_Rank.Text = zinfo.Rank.ToString();
            txt_Module.Text = zinfo.Module;

            if (zinfo.Type == 2)
                chk_Type.Checked = true ;
            else
                chk_Type.Checked = false;

            lbl_Created.Text = "Tạo bởi:[" + zinfo.CreatedName + "][" + zinfo.CreatedOn + "]";
            lbl_Modified.Text = "Chỉnh sửa:[" + zinfo.ModifiedName + "][" + zinfo.ModifiedOn + "]";
        }

        private void SetDefault()
        {
            _Key = 0;
            txt_ID.Text = "";
            txt_Name.Text = "";
            txt_Rank.Text = "0";
            cbo_menu.SelectedIndex = 0;
            txt_Module.Text = "";
            chk_Type.Checked = false;
            lbl_Created.Text = "Tạo bởi:";
            lbl_Modified.Text = "Chỉnh sửa:";
        }
       
        #endregion

        #region[Event]
        private void Btn_Save_Click(object sender, EventArgs e)
        {
            if (txt_ID.Text.Trim().Length == 0)
            {
                MessageBox.Show("Chưa nhập mã!");
                return;
            }
            if (txt_Name.Text.Trim().Length == 0)
            {
                MessageBox.Show("Chưa nhập tên nhóm!");
                return;
            }
            if (Menu_Data.CountID(txt_ID.Text.Trim().ToUpper()) > 0 && _Key == 0)
            {
                MessageBox.Show("Mã Menu đã tồn tại !.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            int zRank;
            if (!int.TryParse(txt_Rank.Text, out zRank))
            {
                MessageBox.Show("Vui lòng nhập đúng định dạng số!");
                return;
            }
            else
            {
                Menu_Info zinfo = new Menu_Info(_Key);
                zinfo.MenuID = txt_ID.Text.Trim().ToUpper();
                zinfo.MenuName = txt_Name.Text.Trim();
                zinfo.Rank = int.Parse(txt_Rank.Text.Trim());
                zinfo.Module = txt_Module.Text.Trim().ToUpper();
                zinfo.Parent = cbo_menu.SelectedValue.ToInt();
                if (zinfo.Parent == 0)
                    zinfo.MenuLevel = 0;
                else
                    zinfo.MenuLevel = 1;
                if (chk_Type.Checked == true)
                    zinfo.Type = 2;
                else
                    zinfo.Type = 1;
                zinfo.CreatedBy = SessionUser.UserLogin.Key;
                zinfo.CreatedName = SessionUser.UserLogin.EmployeeName;
                zinfo.ModifiedBy = SessionUser.UserLogin.Key;
                zinfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
                zinfo.Save();
                if (zinfo.Message=="")
                {
                    Menu_Data.UpdateRole(zinfo.MenuID, zinfo.MenuName, zinfo.Rank, zinfo.Type, zinfo.Parent.ToString());
                    MessageBox.Show("Cập nhật thành công!");
                    SetDefault();
                }
                else
                {
                    MessageBox.Show("Cập nhật không thành công!Vui lòng kiểm tra lại!");
                }
            }


        }

        private void Btn_Del_Click(object sender, EventArgs e)
        {
            if (_Key == 0)
            {
                MessageBox.Show("Chưa chọn thông tin!");
            }
            else
            {
                if (MessageBox.Show("Bạn có chắc xóa thông tin này ?.", "Thông báo", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    Menu_Info zinfo = new Menu_Info(_Key);
                    zinfo.ModifiedBy = SessionUser.UserLogin.Key;
                    zinfo.ModifiedBy = SessionUser.UserLogin.EmployeeName;
                    zinfo.Delete();
                    if (zinfo.Message== "")
                    {
                        MessageBox.Show("Đã xóa !");
                        SetDefault();
                    }
                    else
                    {
                        MessageBox.Show("Xóa không thành công!Vui lòng kiểm tra lại!");
                    }

                }
            }

        }


        private void Btn_New_Click(object sender, EventArgs e)
        {
            SetDefault();
        }

        private void Btn_Search_Click(object sender, EventArgs e)
        {
           
            LoadList() ;
        }
        #endregion


        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

    }
}
