﻿namespace TNS.WinApp
{
    partial class Frm_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Main));
            this.panel_Left = new System.Windows.Forms.Panel();
            this.kryptonHeader1 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btn_Warehouse_1 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Setup = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Warehouse_2 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Warehouse_3 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Production = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Human = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Salary = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Report = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Warehouse_4 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_VuonUom = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_ThachDua = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Warehouse_5 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.Panel_Right = new System.Windows.Forms.Panel();
            this.txtTitle = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.Panel_Salary = new System.Windows.Forms.Panel();
            this.btn_RiceCompany = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_TinhLuongThang13 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_TinhThuongKD = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btn_CaculatorOffice = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_CaculatorSuport = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_CaculatorWorker = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Rpt15 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Rpt14 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Rpt7 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_FeeCompany = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Payment = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Support_Leader = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Receipt = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.Panel_Human = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btn_Caculator_Keeping = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Employee = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_WorkingHistory = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_EmployeeTeamView = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_ChangeTeam = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Approve = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Diligence = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_ScoreStock = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_ReportCong = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Report_6 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_ScoreBegin = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_NCPN = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Standard = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_SalaryDefault = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_FeeChildren = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_FeeDefault = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_KhoiTao = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_TimeKeeping = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Contract = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_DayOffHoliday = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.Panel_Report = new System.Windows.Forms.Panel();
            this.btn_Business = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_TheKho = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_BangKeXuatKho = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_BangKeNhapKho = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_StockProduct = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Rpt48 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_HMList = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Rpt47 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_LuongThang13 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Rpt43 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_Rpt5 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Office = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Support = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Worker = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_PBChucVu = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_TG_LamViecTaiXuong = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Rpt_ThoiGian = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_LuongTongHopSanXuat_ChiTiet = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_LuongTongHopHoTro_ChiTiet = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_LuongTongHop_ChiTiet = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_THThuNhap = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Bao_Cao_Ngay = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Adjusted = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Rpt2 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Rpt_Mau02 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_TBLK_TaiTo = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_CC_ChiTiet = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_KhoanChuyenCan = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_SLDHChuaChiaLai = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Rpt_Mau01 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Rpt3 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_SL_TaiBP = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Report20 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Report25 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_TongLuongKhoan_SP = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Report23 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Rpt_19 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Rpt9 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Rpt45 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Rpt44 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Rpt42 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Rpt41 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_PayAtm = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Rpt17 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Rpt_16 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Rpt13 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Rpt8 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_PhieuLuong = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_UngLuong = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Rpt18 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Rpt12 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Rpt4 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_TrungBinhNgay = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Rpt1 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.Panel_Production = new System.Windows.Forms.Panel();
            this.btn_Import_Production_Time = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Import_Record = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Production_Note = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Production_Auto_Money = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Production_Auto_Calculator = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Production_ReCalculator2 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Production_ReCalculator = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Production_Time = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Compare = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_OrderView = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Production_Calculator_V2 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Production_Calculator = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Production_Payroll = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.Panel_Temp = new System.Windows.Forms.Panel();
            this.btn_Temp_Output_Approve = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Temp_Input = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Temp_Input_Approve = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Temp_Output = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.Panel_Recycle = new System.Windows.Forms.Panel();
            this.btn_Recycle_Input_Approve = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Recycle_Input_Customer = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Recycle_Ouput_Approve = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Recycle_Output = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Recycle_Input_Approve_Customer = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Recycle_Input = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.Panel_Box = new System.Windows.Forms.Panel();
            this.btn_Box_Output_Approve_Customer = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Box_Input = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Box_Output_Approve = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Box_Input_Approve = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Box_Output_Customer = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Box_Output = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.Panel_VuonUom = new System.Windows.Forms.Panel();
            this.btn_VuonUom_Approve_Customer = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_VuonUom_Input = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_VuonUom_Output_Approve = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_VuonUom_Output = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_VuonUom_Input_Approve = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_VuonUom_Customer = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.Panel_ThachDua = new System.Windows.Forms.Panel();
            this.btn_ThachDua_Approve_Customer = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_ThachDua_Input = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_ThachDua_Output_Approve = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_ThachDua_Output = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_ThachDua_Input_Approve = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_ThachDua_Customer = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.Panel_Freezing = new System.Windows.Forms.Panel();
            this.btn_Freez_Output_Approve_Customer = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Freez_Input = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Freez_Output_Approve = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Freez_Output = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Freez_Input_Approve = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Freez_Output_Customer = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.Panel_Material = new System.Windows.Forms.Panel();
            this.btn_Material_Input_Approve = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Material_Input_Customer = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Material_Output_Approve = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Material_Input_Customer_Approve = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Material_Input = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Material_Output = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.Panel_Setup = new System.Windows.Forms.Panel();
            this.btn_ProductType = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_WorkGroup = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Setup_Coefficent = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Convert_Unit = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Position = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Team = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Department = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Branch = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Time_Defines = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Holiday = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Paramaters = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Time_Work = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_ThanhPham = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Product = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Combo = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Setup_Unit = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Setup_Off = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Setup_Rice = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Stages = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Customer_Company = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Lock = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_LogUser = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Role = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Fee = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Customer_Individual = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Users = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Exit = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_ChangePass = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.Header = new ComponentFactory.Krypton.Toolkit.KryptonPanel();
            this.dte_WorkDate = new TN_Tools.TNDateTime();
            this.lbl_EmployeeName = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.btn_Info = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Download = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.HeaderControl = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btnMini = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnMax = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnClose = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.kryptonHeader2 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.txt_ChayChu = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel_Left.SuspendLayout();
            this.Panel_Right.SuspendLayout();
            this.Panel_Salary.SuspendLayout();
            this.Panel_Human.SuspendLayout();
            this.Panel_Report.SuspendLayout();
            this.Panel_Production.SuspendLayout();
            this.Panel_Temp.SuspendLayout();
            this.Panel_Recycle.SuspendLayout();
            this.Panel_Box.SuspendLayout();
            this.Panel_VuonUom.SuspendLayout();
            this.Panel_ThachDua.SuspendLayout();
            this.Panel_Freezing.SuspendLayout();
            this.Panel_Material.SuspendLayout();
            this.Panel_Setup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Header)).BeginInit();
            this.Header.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel_Left
            // 
            this.panel_Left.AutoScroll = true;
            this.panel_Left.BackColor = System.Drawing.Color.Transparent;
            this.panel_Left.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_Left.Controls.Add(this.kryptonHeader1);
            this.panel_Left.Controls.Add(this.btn_Warehouse_1);
            this.panel_Left.Controls.Add(this.btn_Setup);
            this.panel_Left.Controls.Add(this.btn_Warehouse_2);
            this.panel_Left.Controls.Add(this.btn_Warehouse_3);
            this.panel_Left.Controls.Add(this.btn_Production);
            this.panel_Left.Controls.Add(this.btn_Human);
            this.panel_Left.Controls.Add(this.btn_Salary);
            this.panel_Left.Controls.Add(this.btn_Report);
            this.panel_Left.Controls.Add(this.btn_Warehouse_4);
            this.panel_Left.Controls.Add(this.btn_VuonUom);
            this.panel_Left.Controls.Add(this.btn_ThachDua);
            this.panel_Left.Controls.Add(this.btn_Warehouse_5);
            this.panel_Left.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel_Left.Location = new System.Drawing.Point(0, 107);
            this.panel_Left.Name = "panel_Left";
            this.panel_Left.Size = new System.Drawing.Size(363, 616);
            this.panel_Left.TabIndex = 160;
            // 
            // kryptonHeader1
            // 
            this.kryptonHeader1.AutoSize = false;
            this.kryptonHeader1.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader1.HeaderStyle = ComponentFactory.Krypton.Toolkit.HeaderStyle.Secondary;
            this.kryptonHeader1.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader1.Name = "kryptonHeader1";
            this.kryptonHeader1.Size = new System.Drawing.Size(361, 30);
            this.kryptonHeader1.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kryptonHeader1.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.kryptonHeader1.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.kryptonHeader1.TabIndex = 186;
            this.kryptonHeader1.Values.Description = "";
            this.kryptonHeader1.Values.Heading = "Quản lý";
            // 
            // btn_Warehouse_1
            // 
            this.btn_Warehouse_1.Location = new System.Drawing.Point(9, 137);
            this.btn_Warehouse_1.Name = "btn_Warehouse_1";
            this.btn_Warehouse_1.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Warehouse_1.Size = new System.Drawing.Size(110, 95);
            this.btn_Warehouse_1.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Warehouse_1.StateCommon.Border.Rounding = 10;
            this.btn_Warehouse_1.StateCommon.Border.Width = 1;
            this.btn_Warehouse_1.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Warehouse_1.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Warehouse_1.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Warehouse_1.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Warehouse_1.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Warehouse_1.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Warehouse_1.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.btn_Warehouse_1.TabIndex = 185;
            this.btn_Warehouse_1.Tag = "MEN_0002";
            this.btn_Warehouse_1.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Warehouse_1.Values.Image")));
            this.btn_Warehouse_1.Values.Text = "Kho nguyên liệu";
            // 
            // btn_Setup
            // 
            this.btn_Setup.Location = new System.Drawing.Point(9, 36);
            this.btn_Setup.Name = "btn_Setup";
            this.btn_Setup.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Setup.Size = new System.Drawing.Size(110, 95);
            this.btn_Setup.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Setup.StateCommon.Border.Rounding = 10;
            this.btn_Setup.StateCommon.Border.Width = 1;
            this.btn_Setup.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Setup.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Setup.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Setup.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Setup.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.btn_Setup.TabIndex = 185;
            this.btn_Setup.Tag = "MEN_0001";
            this.btn_Setup.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Setup.Values.Image")));
            this.btn_Setup.Values.Text = "Cài đặt";
            // 
            // btn_Warehouse_2
            // 
            this.btn_Warehouse_2.Location = new System.Drawing.Point(241, 238);
            this.btn_Warehouse_2.Name = "btn_Warehouse_2";
            this.btn_Warehouse_2.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Warehouse_2.Size = new System.Drawing.Size(110, 95);
            this.btn_Warehouse_2.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Warehouse_2.StateCommon.Border.Rounding = 10;
            this.btn_Warehouse_2.StateCommon.Border.Width = 1;
            this.btn_Warehouse_2.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Warehouse_2.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Warehouse_2.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Warehouse_2.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Warehouse_2.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.btn_Warehouse_2.TabIndex = 185;
            this.btn_Warehouse_2.Tag = "MEN_0003";
            this.btn_Warehouse_2.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Warehouse_2.Values.Image")));
            this.btn_Warehouse_2.Values.Text = "Kho cấp đông";
            // 
            // btn_Warehouse_3
            // 
            this.btn_Warehouse_3.Location = new System.Drawing.Point(9, 238);
            this.btn_Warehouse_3.Name = "btn_Warehouse_3";
            this.btn_Warehouse_3.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Warehouse_3.Size = new System.Drawing.Size(110, 95);
            this.btn_Warehouse_3.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Warehouse_3.StateCommon.Border.Rounding = 10;
            this.btn_Warehouse_3.StateCommon.Border.Width = 1;
            this.btn_Warehouse_3.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Warehouse_3.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Warehouse_3.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Warehouse_3.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Warehouse_3.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.btn_Warehouse_3.TabIndex = 185;
            this.btn_Warehouse_3.Tag = "MEN_0004";
            this.btn_Warehouse_3.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Warehouse_3.Values.Image")));
            this.btn_Warehouse_3.Values.Text = "Kho đồ hộp";
            // 
            // btn_Production
            // 
            this.btn_Production.Location = new System.Drawing.Point(241, 36);
            this.btn_Production.Name = "btn_Production";
            this.btn_Production.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Production.Size = new System.Drawing.Size(110, 95);
            this.btn_Production.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Production.StateCommon.Border.Rounding = 10;
            this.btn_Production.StateCommon.Border.Width = 1;
            this.btn_Production.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Production.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Production.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Production.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Production.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Production.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Production.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.btn_Production.TabIndex = 185;
            this.btn_Production.Tag = "MEN_0007";
            this.btn_Production.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Production.Values.Image")));
            this.btn_Production.Values.Text = "Sản xuất";
            // 
            // btn_Human
            // 
            this.btn_Human.Location = new System.Drawing.Point(125, 36);
            this.btn_Human.Name = "btn_Human";
            this.btn_Human.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Human.Size = new System.Drawing.Size(110, 95);
            this.btn_Human.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Human.StateCommon.Border.Rounding = 10;
            this.btn_Human.StateCommon.Border.Width = 1;
            this.btn_Human.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Human.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Human.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Human.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Human.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Human.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Human.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.btn_Human.TabIndex = 185;
            this.btn_Human.Tag = "MEN_0008";
            this.btn_Human.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Human.Values.Image")));
            this.btn_Human.Values.Text = "Nhân sự\r\nChấm công";
            // 
            // btn_Salary
            // 
            this.btn_Salary.Location = new System.Drawing.Point(125, 137);
            this.btn_Salary.Name = "btn_Salary";
            this.btn_Salary.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Salary.Size = new System.Drawing.Size(110, 95);
            this.btn_Salary.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Salary.StateCommon.Border.Rounding = 10;
            this.btn_Salary.StateCommon.Border.Width = 1;
            this.btn_Salary.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Salary.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Salary.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Salary.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Salary.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Salary.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Salary.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.btn_Salary.TabIndex = 185;
            this.btn_Salary.Tag = "MEN_0009";
            this.btn_Salary.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Salary.Values.Image")));
            this.btn_Salary.Values.Text = "Tính lương";
            // 
            // btn_Report
            // 
            this.btn_Report.Location = new System.Drawing.Point(241, 137);
            this.btn_Report.Name = "btn_Report";
            this.btn_Report.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Report.Size = new System.Drawing.Size(110, 95);
            this.btn_Report.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Report.StateCommon.Border.Rounding = 10;
            this.btn_Report.StateCommon.Border.Width = 1;
            this.btn_Report.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Report.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Report.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Report.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Report.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Report.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Report.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.btn_Report.TabIndex = 185;
            this.btn_Report.Tag = "MEN_0010";
            this.btn_Report.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Report.Values.Image")));
            this.btn_Report.Values.Text = "Báo cáo";
            // 
            // btn_Warehouse_4
            // 
            this.btn_Warehouse_4.Location = new System.Drawing.Point(125, 238);
            this.btn_Warehouse_4.Name = "btn_Warehouse_4";
            this.btn_Warehouse_4.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Warehouse_4.Size = new System.Drawing.Size(110, 95);
            this.btn_Warehouse_4.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Warehouse_4.StateCommon.Border.Rounding = 10;
            this.btn_Warehouse_4.StateCommon.Border.Width = 1;
            this.btn_Warehouse_4.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Warehouse_4.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Warehouse_4.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Warehouse_4.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Warehouse_4.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Warehouse_4.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Warehouse_4.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.btn_Warehouse_4.TabIndex = 185;
            this.btn_Warehouse_4.Tag = "MEN_0005";
            this.btn_Warehouse_4.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Warehouse_4.Values.Image")));
            this.btn_Warehouse_4.Values.Text = "Kho tạm";
            // 
            // btn_VuonUom
            // 
            this.btn_VuonUom.Location = new System.Drawing.Point(241, 339);
            this.btn_VuonUom.Name = "btn_VuonUom";
            this.btn_VuonUom.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_VuonUom.Size = new System.Drawing.Size(110, 95);
            this.btn_VuonUom.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_VuonUom.StateCommon.Border.Rounding = 10;
            this.btn_VuonUom.StateCommon.Border.Width = 1;
            this.btn_VuonUom.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_VuonUom.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_VuonUom.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_VuonUom.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_VuonUom.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_VuonUom.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_VuonUom.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.btn_VuonUom.TabIndex = 185;
            this.btn_VuonUom.Tag = "MEN_0012";
            this.btn_VuonUom.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_VuonUom.Values.Image")));
            this.btn_VuonUom.Values.Text = "Kho vườn ươm";
            // 
            // btn_ThachDua
            // 
            this.btn_ThachDua.Location = new System.Drawing.Point(125, 339);
            this.btn_ThachDua.Name = "btn_ThachDua";
            this.btn_ThachDua.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_ThachDua.Size = new System.Drawing.Size(110, 95);
            this.btn_ThachDua.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_ThachDua.StateCommon.Border.Rounding = 10;
            this.btn_ThachDua.StateCommon.Border.Width = 1;
            this.btn_ThachDua.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ThachDua.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ThachDua.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ThachDua.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_ThachDua.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ThachDua.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ThachDua.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.btn_ThachDua.TabIndex = 185;
            this.btn_ThachDua.Tag = "MEN_0011";
            this.btn_ThachDua.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_ThachDua.Values.Image")));
            this.btn_ThachDua.Values.Text = "Kho thạch dừa";
            // 
            // btn_Warehouse_5
            // 
            this.btn_Warehouse_5.Location = new System.Drawing.Point(9, 339);
            this.btn_Warehouse_5.Name = "btn_Warehouse_5";
            this.btn_Warehouse_5.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Warehouse_5.Size = new System.Drawing.Size(110, 95);
            this.btn_Warehouse_5.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Warehouse_5.StateCommon.Border.Rounding = 10;
            this.btn_Warehouse_5.StateCommon.Border.Width = 1;
            this.btn_Warehouse_5.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Warehouse_5.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Warehouse_5.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Warehouse_5.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Warehouse_5.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Warehouse_5.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Warehouse_5.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.btn_Warehouse_5.TabIndex = 185;
            this.btn_Warehouse_5.Tag = "MEN_0006";
            this.btn_Warehouse_5.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Warehouse_5.Values.Image")));
            this.btn_Warehouse_5.Values.Text = "Kho hủy";
            // 
            // Panel_Right
            // 
            this.Panel_Right.AutoScroll = true;
            this.Panel_Right.BackColor = System.Drawing.Color.Transparent;
            this.Panel_Right.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.Panel_Right.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel_Right.Controls.Add(this.txtTitle);
            this.Panel_Right.Controls.Add(this.Panel_Salary);
            this.Panel_Right.Controls.Add(this.Panel_Human);
            this.Panel_Right.Controls.Add(this.Panel_Report);
            this.Panel_Right.Controls.Add(this.Panel_Production);
            this.Panel_Right.Controls.Add(this.Panel_Temp);
            this.Panel_Right.Controls.Add(this.Panel_Recycle);
            this.Panel_Right.Controls.Add(this.Panel_Box);
            this.Panel_Right.Controls.Add(this.Panel_VuonUom);
            this.Panel_Right.Controls.Add(this.Panel_ThachDua);
            this.Panel_Right.Controls.Add(this.Panel_Freezing);
            this.Panel_Right.Controls.Add(this.Panel_Material);
            this.Panel_Right.Controls.Add(this.Panel_Setup);
            this.Panel_Right.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel_Right.Location = new System.Drawing.Point(363, 107);
            this.Panel_Right.Name = "Panel_Right";
            this.Panel_Right.Size = new System.Drawing.Size(903, 616);
            this.Panel_Right.TabIndex = 173;
            // 
            // txtTitle
            // 
            this.txtTitle.AutoSize = false;
            this.txtTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtTitle.HeaderStyle = ComponentFactory.Krypton.Toolkit.HeaderStyle.Secondary;
            this.txtTitle.Location = new System.Drawing.Point(0, 0);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(3944, 30);
            this.txtTitle.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.txtTitle.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.txtTitle.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.txtTitle.TabIndex = 188;
            this.txtTitle.Values.Description = "";
            this.txtTitle.Values.Heading = "Chức năng";
            // 
            // Panel_Salary
            // 
            this.Panel_Salary.Controls.Add(this.btn_RiceCompany);
            this.Panel_Salary.Controls.Add(this.btn_TinhLuongThang13);
            this.Panel_Salary.Controls.Add(this.btn_TinhThuongKD);
            this.Panel_Salary.Controls.Add(this.label6);
            this.Panel_Salary.Controls.Add(this.label5);
            this.Panel_Salary.Controls.Add(this.label4);
            this.Panel_Salary.Controls.Add(this.btn_CaculatorOffice);
            this.Panel_Salary.Controls.Add(this.btn_CaculatorSuport);
            this.Panel_Salary.Controls.Add(this.btn_CaculatorWorker);
            this.Panel_Salary.Controls.Add(this.btn_Rpt15);
            this.Panel_Salary.Controls.Add(this.btn_Rpt14);
            this.Panel_Salary.Controls.Add(this.btn_Rpt7);
            this.Panel_Salary.Controls.Add(this.btn_FeeCompany);
            this.Panel_Salary.Controls.Add(this.btn_Payment);
            this.Panel_Salary.Controls.Add(this.btn_Support_Leader);
            this.Panel_Salary.Controls.Add(this.btn_Receipt);
            this.Panel_Salary.Location = new System.Drawing.Point(1417, 2380);
            this.Panel_Salary.Name = "Panel_Salary";
            this.Panel_Salary.Size = new System.Drawing.Size(474, 527);
            this.Panel_Salary.TabIndex = 199;
            // 
            // btn_RiceCompany
            // 
            this.btn_RiceCompany.Location = new System.Drawing.Point(292, 339);
            this.btn_RiceCompany.Name = "btn_RiceCompany";
            this.btn_RiceCompany.Size = new System.Drawing.Size(90, 90);
            this.btn_RiceCompany.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_RiceCompany.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_RiceCompany.StateCommon.Back.ColorAngle = 75F;
            this.btn_RiceCompany.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_RiceCompany.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_RiceCompany.StateCommon.Border.Rounding = 10;
            this.btn_RiceCompany.StateCommon.Border.Width = 2;
            this.btn_RiceCompany.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_RiceCompany.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_RiceCompany.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_RiceCompany.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_RiceCompany.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_RiceCompany.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_RiceCompany.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_RiceCompany.TabIndex = 286;
            this.btn_RiceCompany.Tag = "SUB_0163";
            this.btn_RiceCompany.Values.Text = "Tính cơm \r\ntrưa, tăng\r\nca tại\r\ncông ty";
            // 
            // btn_TinhLuongThang13
            // 
            this.btn_TinhLuongThang13.Location = new System.Drawing.Point(106, 336);
            this.btn_TinhLuongThang13.Name = "btn_TinhLuongThang13";
            this.btn_TinhLuongThang13.Size = new System.Drawing.Size(90, 90);
            this.btn_TinhLuongThang13.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_TinhLuongThang13.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_TinhLuongThang13.StateCommon.Back.ColorAngle = 75F;
            this.btn_TinhLuongThang13.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_TinhLuongThang13.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_TinhLuongThang13.StateCommon.Border.Rounding = 10;
            this.btn_TinhLuongThang13.StateCommon.Border.Width = 2;
            this.btn_TinhLuongThang13.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_TinhLuongThang13.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_TinhLuongThang13.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_TinhLuongThang13.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_TinhLuongThang13.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_TinhLuongThang13.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_TinhLuongThang13.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_TinhLuongThang13.TabIndex = 288;
            this.btn_TinhLuongThang13.Tag = "SUB_0161";
            this.btn_TinhLuongThang13.Values.Text = "Tính \r\nlương\r\ntháng 13";
            // 
            // btn_TinhThuongKD
            // 
            this.btn_TinhThuongKD.Location = new System.Drawing.Point(12, 336);
            this.btn_TinhThuongKD.Name = "btn_TinhThuongKD";
            this.btn_TinhThuongKD.Size = new System.Drawing.Size(90, 90);
            this.btn_TinhThuongKD.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_TinhThuongKD.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_TinhThuongKD.StateCommon.Back.ColorAngle = 75F;
            this.btn_TinhThuongKD.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_TinhThuongKD.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_TinhThuongKD.StateCommon.Border.Rounding = 10;
            this.btn_TinhThuongKD.StateCommon.Border.Width = 2;
            this.btn_TinhThuongKD.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_TinhThuongKD.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_TinhThuongKD.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_TinhThuongKD.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_TinhThuongKD.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_TinhThuongKD.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_TinhThuongKD.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_TinhThuongKD.TabIndex = 289;
            this.btn_TinhThuongKD.Tag = "SUB_0160";
            this.btn_TinhThuongKD.Values.Text = "Tính \r\nthưởng\r\nKinh doanh";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label6.Location = new System.Drawing.Point(9, 318);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(332, 16);
            this.label6.TabIndex = 284;
            this.label6.Text = "Lập bảng tính thưởng SXKD và lương tháng 13";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label5.Location = new System.Drawing.Point(9, 111);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(186, 16);
            this.label5.TabIndex = 284;
            this.label5.Text = "Lập bảng lương tổng hợp";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label4.Location = new System.Drawing.Point(8, 2);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(225, 16);
            this.label4.TabIndex = 284;
            this.label4.Text = "Các khoản khác tính vào lương";
            // 
            // btn_CaculatorOffice
            // 
            this.btn_CaculatorOffice.Location = new System.Drawing.Point(8, 129);
            this.btn_CaculatorOffice.Name = "btn_CaculatorOffice";
            this.btn_CaculatorOffice.Size = new System.Drawing.Size(90, 90);
            this.btn_CaculatorOffice.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_CaculatorOffice.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_CaculatorOffice.StateCommon.Back.ColorAngle = 75F;
            this.btn_CaculatorOffice.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_CaculatorOffice.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_CaculatorOffice.StateCommon.Border.Rounding = 10;
            this.btn_CaculatorOffice.StateCommon.Border.Width = 2;
            this.btn_CaculatorOffice.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_CaculatorOffice.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_CaculatorOffice.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_CaculatorOffice.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_CaculatorOffice.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_CaculatorOffice.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_CaculatorOffice.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_CaculatorOffice.TabIndex = 207;
            this.btn_CaculatorOffice.Tag = "SUB_0154";
            this.btn_CaculatorOffice.Values.Text = "Tính lương\r\nGián tiếp";
            // 
            // btn_CaculatorSuport
            // 
            this.btn_CaculatorSuport.Location = new System.Drawing.Point(102, 130);
            this.btn_CaculatorSuport.Name = "btn_CaculatorSuport";
            this.btn_CaculatorSuport.Size = new System.Drawing.Size(90, 90);
            this.btn_CaculatorSuport.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_CaculatorSuport.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_CaculatorSuport.StateCommon.Back.ColorAngle = 75F;
            this.btn_CaculatorSuport.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_CaculatorSuport.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_CaculatorSuport.StateCommon.Border.Rounding = 10;
            this.btn_CaculatorSuport.StateCommon.Border.Width = 2;
            this.btn_CaculatorSuport.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_CaculatorSuport.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_CaculatorSuport.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_CaculatorSuport.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_CaculatorSuport.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_CaculatorSuport.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_CaculatorSuport.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_CaculatorSuport.TabIndex = 207;
            this.btn_CaculatorSuport.Tag = "SUB_0156";
            this.btn_CaculatorSuport.Values.Text = "Tính lương\r\nbộ phận\r\nHTSX";
            // 
            // btn_CaculatorWorker
            // 
            this.btn_CaculatorWorker.Location = new System.Drawing.Point(196, 131);
            this.btn_CaculatorWorker.Name = "btn_CaculatorWorker";
            this.btn_CaculatorWorker.Size = new System.Drawing.Size(90, 90);
            this.btn_CaculatorWorker.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_CaculatorWorker.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_CaculatorWorker.StateCommon.Back.ColorAngle = 75F;
            this.btn_CaculatorWorker.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_CaculatorWorker.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_CaculatorWorker.StateCommon.Border.Rounding = 10;
            this.btn_CaculatorWorker.StateCommon.Border.Width = 2;
            this.btn_CaculatorWorker.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_CaculatorWorker.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_CaculatorWorker.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_CaculatorWorker.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_CaculatorWorker.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_CaculatorWorker.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_CaculatorWorker.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_CaculatorWorker.TabIndex = 207;
            this.btn_CaculatorWorker.Tag = "SUB_0158";
            this.btn_CaculatorWorker.Values.Text = "Tính lương\r\ntrực tiếp";
            // 
            // btn_Rpt15
            // 
            this.btn_Rpt15.Location = new System.Drawing.Point(8, 224);
            this.btn_Rpt15.Name = "btn_Rpt15";
            this.btn_Rpt15.Size = new System.Drawing.Size(90, 90);
            this.btn_Rpt15.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Rpt15.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Rpt15.StateCommon.Back.ColorAngle = 75F;
            this.btn_Rpt15.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Rpt15.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Rpt15.StateCommon.Border.Rounding = 10;
            this.btn_Rpt15.StateCommon.Border.Width = 2;
            this.btn_Rpt15.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Rpt15.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt15.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Rpt15.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Rpt15.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt15.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt15.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt15.TabIndex = 204;
            this.btn_Rpt15.Tag = "SUB_0155";
            this.btn_Rpt15.Values.Text = "Kiểm soát \r\nlập báo cáo \r\nlương \r\nGián tiếp";
            // 
            // btn_Rpt14
            // 
            this.btn_Rpt14.Location = new System.Drawing.Point(102, 225);
            this.btn_Rpt14.Name = "btn_Rpt14";
            this.btn_Rpt14.Size = new System.Drawing.Size(90, 90);
            this.btn_Rpt14.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Rpt14.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Rpt14.StateCommon.Back.ColorAngle = 75F;
            this.btn_Rpt14.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Rpt14.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Rpt14.StateCommon.Border.Rounding = 10;
            this.btn_Rpt14.StateCommon.Border.Width = 2;
            this.btn_Rpt14.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Rpt14.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt14.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Rpt14.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Rpt14.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt14.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt14.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt14.TabIndex = 204;
            this.btn_Rpt14.Tag = "SUB_0157";
            this.btn_Rpt14.Values.Text = "Kiểm soát\r\nlập báo cáo\r\nlương bộ \r\nphận HTSX";
            // 
            // btn_Rpt7
            // 
            this.btn_Rpt7.Location = new System.Drawing.Point(195, 226);
            this.btn_Rpt7.Name = "btn_Rpt7";
            this.btn_Rpt7.Size = new System.Drawing.Size(90, 90);
            this.btn_Rpt7.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Rpt7.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Rpt7.StateCommon.Back.ColorAngle = 75F;
            this.btn_Rpt7.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Rpt7.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Rpt7.StateCommon.Border.Rounding = 10;
            this.btn_Rpt7.StateCommon.Border.Width = 2;
            this.btn_Rpt7.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Rpt7.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt7.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Rpt7.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Rpt7.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt7.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt7.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt7.TabIndex = 204;
            this.btn_Rpt7.Tag = "SUB_0159";
            this.btn_Rpt7.Values.Text = "Kiểm soát\r\nlập báo cáo\r\nlương\r\nTrực tiếp";
            // 
            // btn_FeeCompany
            // 
            this.btn_FeeCompany.Location = new System.Drawing.Point(199, 338);
            this.btn_FeeCompany.Name = "btn_FeeCompany";
            this.btn_FeeCompany.Size = new System.Drawing.Size(90, 90);
            this.btn_FeeCompany.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_FeeCompany.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_FeeCompany.StateCommon.Back.ColorAngle = 75F;
            this.btn_FeeCompany.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_FeeCompany.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_FeeCompany.StateCommon.Border.Rounding = 10;
            this.btn_FeeCompany.StateCommon.Border.Width = 2;
            this.btn_FeeCompany.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_FeeCompany.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_FeeCompany.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_FeeCompany.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_FeeCompany.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_FeeCompany.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_FeeCompany.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_FeeCompany.TabIndex = 208;
            this.btn_FeeCompany.Tag = "SUB_0162";
            this.btn_FeeCompany.Values.Text = "Tính\r\nCông ty\r\nđóng trích \r\nlương";
            // 
            // btn_Payment
            // 
            this.btn_Payment.Location = new System.Drawing.Point(197, 21);
            this.btn_Payment.Name = "btn_Payment";
            this.btn_Payment.Size = new System.Drawing.Size(90, 90);
            this.btn_Payment.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Payment.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Payment.StateCommon.Back.ColorAngle = 75F;
            this.btn_Payment.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Payment.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Payment.StateCommon.Border.Rounding = 10;
            this.btn_Payment.StateCommon.Border.Width = 2;
            this.btn_Payment.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Payment.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Payment.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Payment.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Payment.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Payment.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Payment.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Payment.TabIndex = 208;
            this.btn_Payment.Tag = "SUB_0153";
            this.btn_Payment.Values.Text = "Phiếu chi";
            // 
            // btn_Support_Leader
            // 
            this.btn_Support_Leader.Location = new System.Drawing.Point(8, 20);
            this.btn_Support_Leader.Name = "btn_Support_Leader";
            this.btn_Support_Leader.Size = new System.Drawing.Size(90, 90);
            this.btn_Support_Leader.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Support_Leader.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Support_Leader.StateCommon.Back.ColorAngle = 75F;
            this.btn_Support_Leader.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Support_Leader.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Support_Leader.StateCommon.Border.Rounding = 10;
            this.btn_Support_Leader.StateCommon.Border.Width = 2;
            this.btn_Support_Leader.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Support_Leader.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Support_Leader.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Support_Leader.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Support_Leader.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Support_Leader.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Support_Leader.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Support_Leader.TabIndex = 206;
            this.btn_Support_Leader.Tag = "SUB_0151";
            this.btn_Support_Leader.Values.Text = "Phí \r\nhỗ trợ\r\ntheo tháng";
            // 
            // btn_Receipt
            // 
            this.btn_Receipt.Location = new System.Drawing.Point(102, 20);
            this.btn_Receipt.Name = "btn_Receipt";
            this.btn_Receipt.Size = new System.Drawing.Size(90, 90);
            this.btn_Receipt.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Receipt.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Receipt.StateCommon.Back.ColorAngle = 75F;
            this.btn_Receipt.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Receipt.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Receipt.StateCommon.Border.Rounding = 10;
            this.btn_Receipt.StateCommon.Border.Width = 2;
            this.btn_Receipt.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Receipt.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Receipt.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Receipt.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Receipt.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Receipt.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Receipt.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Receipt.TabIndex = 205;
            this.btn_Receipt.Tag = "SUB_0152";
            this.btn_Receipt.Values.Text = "Phiếu thu";
            // 
            // Panel_Human
            // 
            this.Panel_Human.Controls.Add(this.label8);
            this.Panel_Human.Controls.Add(this.label7);
            this.Panel_Human.Controls.Add(this.btn_Caculator_Keeping);
            this.Panel_Human.Controls.Add(this.btn_Employee);
            this.Panel_Human.Controls.Add(this.btn_WorkingHistory);
            this.Panel_Human.Controls.Add(this.btn_EmployeeTeamView);
            this.Panel_Human.Controls.Add(this.btn_ChangeTeam);
            this.Panel_Human.Controls.Add(this.btn_Approve);
            this.Panel_Human.Controls.Add(this.btn_Diligence);
            this.Panel_Human.Controls.Add(this.btn_ScoreStock);
            this.Panel_Human.Controls.Add(this.btn_ReportCong);
            this.Panel_Human.Controls.Add(this.btn_Report_6);
            this.Panel_Human.Controls.Add(this.btn_ScoreBegin);
            this.Panel_Human.Controls.Add(this.btn_NCPN);
            this.Panel_Human.Controls.Add(this.btn_Standard);
            this.Panel_Human.Controls.Add(this.btn_SalaryDefault);
            this.Panel_Human.Controls.Add(this.btn_FeeChildren);
            this.Panel_Human.Controls.Add(this.btn_FeeDefault);
            this.Panel_Human.Controls.Add(this.btn_KhoiTao);
            this.Panel_Human.Controls.Add(this.btn_TimeKeeping);
            this.Panel_Human.Controls.Add(this.btn_Contract);
            this.Panel_Human.Controls.Add(this.btn_DayOffHoliday);
            this.Panel_Human.Location = new System.Drawing.Point(3162, 1048);
            this.Panel_Human.Name = "Panel_Human";
            this.Panel_Human.Size = new System.Drawing.Size(782, 365);
            this.Panel_Human.TabIndex = 200;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label8.Location = new System.Drawing.Point(11, 124);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(86, 16);
            this.label8.TabIndex = 284;
            this.label8.Text = "Chấm công";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label7.Location = new System.Drawing.Point(10, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(197, 16);
            this.label7.TabIndex = 284;
            this.label7.Text = "Khai báo thông tin nhân sự";
            // 
            // btn_Caculator_Keeping
            // 
            this.btn_Caculator_Keeping.Location = new System.Drawing.Point(474, 148);
            this.btn_Caculator_Keeping.Name = "btn_Caculator_Keeping";
            this.btn_Caculator_Keeping.Size = new System.Drawing.Size(90, 90);
            this.btn_Caculator_Keeping.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Caculator_Keeping.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Caculator_Keeping.StateCommon.Back.ColorAngle = 75F;
            this.btn_Caculator_Keeping.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Caculator_Keeping.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Caculator_Keeping.StateCommon.Border.Rounding = 10;
            this.btn_Caculator_Keeping.StateCommon.Border.Width = 2;
            this.btn_Caculator_Keeping.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Caculator_Keeping.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Caculator_Keeping.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Caculator_Keeping.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Caculator_Keeping.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Caculator_Keeping.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Caculator_Keeping.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Caculator_Keeping.TabIndex = 207;
            this.btn_Caculator_Keeping.Tag = "SUB_0059";
            this.btn_Caculator_Keeping.Values.Text = "6.Tính công";
            // 
            // btn_Employee
            // 
            this.btn_Employee.Location = new System.Drawing.Point(7, 30);
            this.btn_Employee.Name = "btn_Employee";
            this.btn_Employee.Size = new System.Drawing.Size(90, 90);
            this.btn_Employee.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Employee.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Employee.StateCommon.Back.ColorAngle = 75F;
            this.btn_Employee.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Employee.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Employee.StateCommon.Border.Rounding = 10;
            this.btn_Employee.StateCommon.Border.Width = 2;
            this.btn_Employee.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Employee.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Employee.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Employee.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Employee.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Employee.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Employee.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Employee.TabIndex = 205;
            this.btn_Employee.Tag = "SUB_0051";
            this.btn_Employee.Values.Text = "Nhân sự";
            // 
            // btn_WorkingHistory
            // 
            this.btn_WorkingHistory.Location = new System.Drawing.Point(380, 31);
            this.btn_WorkingHistory.Name = "btn_WorkingHistory";
            this.btn_WorkingHistory.Size = new System.Drawing.Size(90, 90);
            this.btn_WorkingHistory.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_WorkingHistory.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_WorkingHistory.StateCommon.Back.ColorAngle = 75F;
            this.btn_WorkingHistory.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_WorkingHistory.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_WorkingHistory.StateCommon.Border.Rounding = 10;
            this.btn_WorkingHistory.StateCommon.Border.Width = 2;
            this.btn_WorkingHistory.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_WorkingHistory.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_WorkingHistory.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_WorkingHistory.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_WorkingHistory.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_WorkingHistory.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_WorkingHistory.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_WorkingHistory.TabIndex = 206;
            this.btn_WorkingHistory.Tag = "SUB_0054";
            this.btn_WorkingHistory.Values.Text = "Lịch sử và\r\nluân \r\nchuyển\r\nCông việc";
            // 
            // btn_EmployeeTeamView
            // 
            this.btn_EmployeeTeamView.Location = new System.Drawing.Point(473, 31);
            this.btn_EmployeeTeamView.Name = "btn_EmployeeTeamView";
            this.btn_EmployeeTeamView.Size = new System.Drawing.Size(90, 90);
            this.btn_EmployeeTeamView.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_EmployeeTeamView.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_EmployeeTeamView.StateCommon.Back.ColorAngle = 75F;
            this.btn_EmployeeTeamView.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_EmployeeTeamView.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_EmployeeTeamView.StateCommon.Border.Rounding = 10;
            this.btn_EmployeeTeamView.StateCommon.Border.Width = 2;
            this.btn_EmployeeTeamView.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_EmployeeTeamView.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_EmployeeTeamView.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_EmployeeTeamView.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_EmployeeTeamView.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_EmployeeTeamView.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_EmployeeTeamView.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_EmployeeTeamView.TabIndex = 206;
            this.btn_EmployeeTeamView.Tag = "SUB_0055";
            this.btn_EmployeeTeamView.Values.Text = "Nhóm chi\r\nphí khối\r\ngián tiếp";
            // 
            // btn_ChangeTeam
            // 
            this.btn_ChangeTeam.Location = new System.Drawing.Point(668, 254);
            this.btn_ChangeTeam.Name = "btn_ChangeTeam";
            this.btn_ChangeTeam.Size = new System.Drawing.Size(90, 90);
            this.btn_ChangeTeam.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_ChangeTeam.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_ChangeTeam.StateCommon.Back.ColorAngle = 75F;
            this.btn_ChangeTeam.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_ChangeTeam.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_ChangeTeam.StateCommon.Border.Rounding = 10;
            this.btn_ChangeTeam.StateCommon.Border.Width = 2;
            this.btn_ChangeTeam.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_ChangeTeam.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ChangeTeam.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_ChangeTeam.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_ChangeTeam.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ChangeTeam.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ChangeTeam.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ChangeTeam.TabIndex = 206;
            this.btn_ChangeTeam.Tag = "FTK_HRM001";
            this.btn_ChangeTeam.Values.Text = "Chuyển \r\ntổ nhóm";
            this.btn_ChangeTeam.Visible = false;
            // 
            // btn_Approve
            // 
            this.btn_Approve.Location = new System.Drawing.Point(576, 254);
            this.btn_Approve.Name = "btn_Approve";
            this.btn_Approve.Size = new System.Drawing.Size(90, 90);
            this.btn_Approve.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Approve.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Approve.StateCommon.Back.ColorAngle = 75F;
            this.btn_Approve.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Approve.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Approve.StateCommon.Border.Rounding = 10;
            this.btn_Approve.StateCommon.Border.Width = 2;
            this.btn_Approve.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Approve.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Approve.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Approve.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Approve.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Approve.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Approve.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Approve.TabIndex = 206;
            this.btn_Approve.Tag = "FTK_HRM001";
            this.btn_Approve.Values.Text = "XÉT DUYỆT\r\nCÔNG \r\nTRƯỚC\r\nKHI HỦY";
            this.btn_Approve.Visible = false;
            // 
            // btn_Diligence
            // 
            this.btn_Diligence.Location = new System.Drawing.Point(380, 147);
            this.btn_Diligence.Name = "btn_Diligence";
            this.btn_Diligence.Size = new System.Drawing.Size(90, 90);
            this.btn_Diligence.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Diligence.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Diligence.StateCommon.Back.ColorAngle = 75F;
            this.btn_Diligence.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Diligence.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Diligence.StateCommon.Border.Rounding = 10;
            this.btn_Diligence.StateCommon.Border.Width = 2;
            this.btn_Diligence.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Diligence.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Diligence.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Diligence.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Diligence.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Diligence.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Diligence.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Diligence.TabIndex = 206;
            this.btn_Diligence.Tag = "SUB_0064";
            this.btn_Diligence.Values.Text = "5.Công tính\r\nchuyên cần";
            // 
            // btn_ScoreStock
            // 
            this.btn_ScoreStock.Location = new System.Drawing.Point(287, 147);
            this.btn_ScoreStock.Name = "btn_ScoreStock";
            this.btn_ScoreStock.Size = new System.Drawing.Size(90, 90);
            this.btn_ScoreStock.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_ScoreStock.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_ScoreStock.StateCommon.Back.ColorAngle = 75F;
            this.btn_ScoreStock.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_ScoreStock.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_ScoreStock.StateCommon.Border.Rounding = 10;
            this.btn_ScoreStock.StateCommon.Border.Width = 2;
            this.btn_ScoreStock.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_ScoreStock.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ScoreStock.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_ScoreStock.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_ScoreStock.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ScoreStock.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ScoreStock.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ScoreStock.TabIndex = 206;
            this.btn_ScoreStock.Tag = "SUB_0063";
            this.btn_ScoreStock.Values.Text = "4.Ứng công";
            // 
            // btn_ReportCong
            // 
            this.btn_ReportCong.Location = new System.Drawing.Point(568, 149);
            this.btn_ReportCong.Name = "btn_ReportCong";
            this.btn_ReportCong.Size = new System.Drawing.Size(90, 90);
            this.btn_ReportCong.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_ReportCong.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_ReportCong.StateCommon.Back.ColorAngle = 75F;
            this.btn_ReportCong.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_ReportCong.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_ReportCong.StateCommon.Border.Rounding = 10;
            this.btn_ReportCong.StateCommon.Border.Width = 2;
            this.btn_ReportCong.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_ReportCong.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ReportCong.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_ReportCong.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_ReportCong.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ReportCong.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ReportCong.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ReportCong.TabIndex = 206;
            this.btn_ReportCong.Tag = "SUB_0060";
            this.btn_ReportCong.Values.Text = "7.Kiểm soát\r\nlập báo cáo \r\nchấm công\r\n";
            // 
            // btn_Report_6
            // 
            this.btn_Report_6.Location = new System.Drawing.Point(523, 257);
            this.btn_Report_6.Name = "btn_Report_6";
            this.btn_Report_6.Size = new System.Drawing.Size(90, 90);
            this.btn_Report_6.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Report_6.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Report_6.StateCommon.Back.ColorAngle = 75F;
            this.btn_Report_6.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Report_6.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Report_6.StateCommon.Border.Rounding = 10;
            this.btn_Report_6.StateCommon.Border.Width = 2;
            this.btn_Report_6.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Report_6.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Report_6.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Report_6.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Report_6.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Report_6.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Report_6.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Report_6.TabIndex = 206;
            this.btn_Report_6.Tag = "FTK_HRM001";
            this.btn_Report_6.Values.Text = "Báo cáo \r\nChấm công";
            this.btn_Report_6.Visible = false;
            // 
            // btn_ScoreBegin
            // 
            this.btn_ScoreBegin.Location = new System.Drawing.Point(662, 150);
            this.btn_ScoreBegin.Name = "btn_ScoreBegin";
            this.btn_ScoreBegin.Size = new System.Drawing.Size(90, 90);
            this.btn_ScoreBegin.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_ScoreBegin.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_ScoreBegin.StateCommon.Back.ColorAngle = 75F;
            this.btn_ScoreBegin.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_ScoreBegin.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_ScoreBegin.StateCommon.Border.Rounding = 10;
            this.btn_ScoreBegin.StateCommon.Border.Width = 2;
            this.btn_ScoreBegin.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_ScoreBegin.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ScoreBegin.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_ScoreBegin.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_ScoreBegin.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ScoreBegin.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ScoreBegin.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ScoreBegin.TabIndex = 206;
            this.btn_ScoreBegin.Tag = "SUB_0061";
            this.btn_ScoreBegin.Values.Text = "8.Kiểm soát\r\ncông tích \r\nlũy";
            // 
            // btn_NCPN
            // 
            this.btn_NCPN.Location = new System.Drawing.Point(194, 146);
            this.btn_NCPN.Name = "btn_NCPN";
            this.btn_NCPN.Size = new System.Drawing.Size(90, 90);
            this.btn_NCPN.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_NCPN.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_NCPN.StateCommon.Back.ColorAngle = 75F;
            this.btn_NCPN.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_NCPN.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_NCPN.StateCommon.Border.Rounding = 10;
            this.btn_NCPN.StateCommon.Border.Width = 2;
            this.btn_NCPN.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_NCPN.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_NCPN.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_NCPN.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_NCPN.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_NCPN.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_NCPN.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_NCPN.TabIndex = 206;
            this.btn_NCPN.Tag = "SUB_0058";
            this.btn_NCPN.Values.Text = "3. \r\nThâm niên";
            // 
            // btn_Standard
            // 
            this.btn_Standard.Location = new System.Drawing.Point(101, 147);
            this.btn_Standard.Name = "btn_Standard";
            this.btn_Standard.Size = new System.Drawing.Size(90, 90);
            this.btn_Standard.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Standard.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Standard.StateCommon.Back.ColorAngle = 75F;
            this.btn_Standard.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Standard.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Standard.StateCommon.Border.Rounding = 10;
            this.btn_Standard.StateCommon.Border.Width = 2;
            this.btn_Standard.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Standard.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Standard.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Standard.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Standard.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Standard.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Standard.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Standard.TabIndex = 206;
            this.btn_Standard.Tag = "SUB_0057";
            this.btn_Standard.Values.Text = "2. \r\nNgày công\r\nQuy định";
            // 
            // btn_SalaryDefault
            // 
            this.btn_SalaryDefault.Location = new System.Drawing.Point(194, 30);
            this.btn_SalaryDefault.Name = "btn_SalaryDefault";
            this.btn_SalaryDefault.Size = new System.Drawing.Size(90, 90);
            this.btn_SalaryDefault.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_SalaryDefault.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_SalaryDefault.StateCommon.Back.ColorAngle = 75F;
            this.btn_SalaryDefault.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_SalaryDefault.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_SalaryDefault.StateCommon.Border.Rounding = 10;
            this.btn_SalaryDefault.StateCommon.Border.Width = 2;
            this.btn_SalaryDefault.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_SalaryDefault.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_SalaryDefault.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_SalaryDefault.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_SalaryDefault.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_SalaryDefault.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_SalaryDefault.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_SalaryDefault.TabIndex = 206;
            this.btn_SalaryDefault.Tag = "SUB_0053";
            this.btn_SalaryDefault.Values.Text = "Lương \r\nmặc định";
            // 
            // btn_FeeChildren
            // 
            this.btn_FeeChildren.Location = new System.Drawing.Point(287, 30);
            this.btn_FeeChildren.Name = "btn_FeeChildren";
            this.btn_FeeChildren.Size = new System.Drawing.Size(90, 90);
            this.btn_FeeChildren.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_FeeChildren.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_FeeChildren.StateCommon.Back.ColorAngle = 75F;
            this.btn_FeeChildren.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_FeeChildren.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_FeeChildren.StateCommon.Border.Rounding = 10;
            this.btn_FeeChildren.StateCommon.Border.Width = 2;
            this.btn_FeeChildren.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_FeeChildren.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_FeeChildren.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_FeeChildren.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_FeeChildren.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_FeeChildren.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_FeeChildren.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_FeeChildren.TabIndex = 206;
            this.btn_FeeChildren.Tag = "SUB_0062";
            this.btn_FeeChildren.Values.Text = "Phí hỗ trợ \r\ncon nhỏ";
            // 
            // btn_FeeDefault
            // 
            this.btn_FeeDefault.Location = new System.Drawing.Point(101, 30);
            this.btn_FeeDefault.Name = "btn_FeeDefault";
            this.btn_FeeDefault.Size = new System.Drawing.Size(90, 90);
            this.btn_FeeDefault.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_FeeDefault.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_FeeDefault.StateCommon.Back.ColorAngle = 75F;
            this.btn_FeeDefault.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_FeeDefault.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_FeeDefault.StateCommon.Border.Rounding = 10;
            this.btn_FeeDefault.StateCommon.Border.Width = 2;
            this.btn_FeeDefault.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_FeeDefault.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_FeeDefault.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_FeeDefault.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_FeeDefault.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_FeeDefault.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_FeeDefault.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_FeeDefault.TabIndex = 206;
            this.btn_FeeDefault.Tag = "SUB_0052";
            this.btn_FeeDefault.Values.Text = "Các khoản \r\ntrích theo \r\nlương";
            // 
            // btn_KhoiTao
            // 
            this.btn_KhoiTao.Location = new System.Drawing.Point(411, 260);
            this.btn_KhoiTao.Name = "btn_KhoiTao";
            this.btn_KhoiTao.Size = new System.Drawing.Size(90, 90);
            this.btn_KhoiTao.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_KhoiTao.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_KhoiTao.StateCommon.Back.ColorAngle = 75F;
            this.btn_KhoiTao.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_KhoiTao.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_KhoiTao.StateCommon.Border.Rounding = 10;
            this.btn_KhoiTao.StateCommon.Border.Width = 2;
            this.btn_KhoiTao.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_KhoiTao.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_KhoiTao.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_KhoiTao.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_KhoiTao.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_KhoiTao.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_KhoiTao.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_KhoiTao.TabIndex = 206;
            this.btn_KhoiTao.Tag = "FTK_HRM001";
            this.btn_KhoiTao.Values.Text = "0.\r\nKhởi tạo \r\nnhóm \r\nlàm việc ";
            this.btn_KhoiTao.Visible = false;
            // 
            // btn_TimeKeeping
            // 
            this.btn_TimeKeeping.Location = new System.Drawing.Point(7, 146);
            this.btn_TimeKeeping.Name = "btn_TimeKeeping";
            this.btn_TimeKeeping.Size = new System.Drawing.Size(90, 90);
            this.btn_TimeKeeping.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_TimeKeeping.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_TimeKeeping.StateCommon.Back.ColorAngle = 75F;
            this.btn_TimeKeeping.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_TimeKeeping.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_TimeKeeping.StateCommon.Border.Rounding = 10;
            this.btn_TimeKeeping.StateCommon.Border.Width = 2;
            this.btn_TimeKeeping.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_TimeKeeping.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_TimeKeeping.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_TimeKeeping.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_TimeKeeping.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_TimeKeeping.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_TimeKeeping.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_TimeKeeping.TabIndex = 206;
            this.btn_TimeKeeping.Tag = "SUB_0056";
            this.btn_TimeKeeping.Values.Text = "1. \r\nChấm công";
            // 
            // btn_Contract
            // 
            this.btn_Contract.Location = new System.Drawing.Point(462, 257);
            this.btn_Contract.Name = "btn_Contract";
            this.btn_Contract.Size = new System.Drawing.Size(90, 90);
            this.btn_Contract.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Contract.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Contract.StateCommon.Back.ColorAngle = 75F;
            this.btn_Contract.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Contract.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Contract.StateCommon.Border.Rounding = 10;
            this.btn_Contract.StateCommon.Border.Width = 2;
            this.btn_Contract.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Contract.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Contract.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Contract.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Contract.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Contract.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Contract.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Contract.TabIndex = 208;
            this.btn_Contract.Tag = "FCT_HRM001";
            this.btn_Contract.Values.Text = "Hợp đồng ";
            this.btn_Contract.Visible = false;
            // 
            // btn_DayOffHoliday
            // 
            this.btn_DayOffHoliday.Location = new System.Drawing.Point(368, 257);
            this.btn_DayOffHoliday.Name = "btn_DayOffHoliday";
            this.btn_DayOffHoliday.Size = new System.Drawing.Size(90, 90);
            this.btn_DayOffHoliday.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_DayOffHoliday.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_DayOffHoliday.StateCommon.Back.ColorAngle = 75F;
            this.btn_DayOffHoliday.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_DayOffHoliday.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_DayOffHoliday.StateCommon.Border.Rounding = 10;
            this.btn_DayOffHoliday.StateCommon.Border.Width = 2;
            this.btn_DayOffHoliday.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_DayOffHoliday.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_DayOffHoliday.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_DayOffHoliday.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_DayOffHoliday.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_DayOffHoliday.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_DayOffHoliday.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_DayOffHoliday.TabIndex = 208;
            this.btn_DayOffHoliday.Tag = "FPM_HRM001";
            this.btn_DayOffHoliday.Values.Text = "Tính phép\r\nhiếu, hỉ";
            this.btn_DayOffHoliday.Visible = false;
            // 
            // Panel_Report
            // 
            this.Panel_Report.AutoSize = true;
            this.Panel_Report.Controls.Add(this.btn_Business);
            this.Panel_Report.Controls.Add(this.btn_TheKho);
            this.Panel_Report.Controls.Add(this.btn_BangKeXuatKho);
            this.Panel_Report.Controls.Add(this.btn_BangKeNhapKho);
            this.Panel_Report.Controls.Add(this.btn_StockProduct);
            this.Panel_Report.Controls.Add(this.btn_Rpt48);
            this.Panel_Report.Controls.Add(this.btn_HMList);
            this.Panel_Report.Controls.Add(this.btn_Rpt47);
            this.Panel_Report.Controls.Add(this.btn_LuongThang13);
            this.Panel_Report.Controls.Add(this.btn_Rpt43);
            this.Panel_Report.Controls.Add(this.label9);
            this.Panel_Report.Controls.Add(this.label10);
            this.Panel_Report.Controls.Add(this.label11);
            this.Panel_Report.Controls.Add(this.label3);
            this.Panel_Report.Controls.Add(this.label1);
            this.Panel_Report.Controls.Add(this.label2);
            this.Panel_Report.Controls.Add(this.btn_Rpt5);
            this.Panel_Report.Controls.Add(this.btn_Office);
            this.Panel_Report.Controls.Add(this.btn_Support);
            this.Panel_Report.Controls.Add(this.btn_Worker);
            this.Panel_Report.Controls.Add(this.btn_PBChucVu);
            this.Panel_Report.Controls.Add(this.btn_TG_LamViecTaiXuong);
            this.Panel_Report.Controls.Add(this.btn_Rpt_ThoiGian);
            this.Panel_Report.Controls.Add(this.btn_LuongTongHopSanXuat_ChiTiet);
            this.Panel_Report.Controls.Add(this.btn_LuongTongHopHoTro_ChiTiet);
            this.Panel_Report.Controls.Add(this.btn_LuongTongHop_ChiTiet);
            this.Panel_Report.Controls.Add(this.btn_THThuNhap);
            this.Panel_Report.Controls.Add(this.btn_Bao_Cao_Ngay);
            this.Panel_Report.Controls.Add(this.btn_Adjusted);
            this.Panel_Report.Controls.Add(this.btn_Rpt2);
            this.Panel_Report.Controls.Add(this.btn_Rpt_Mau02);
            this.Panel_Report.Controls.Add(this.btn_TBLK_TaiTo);
            this.Panel_Report.Controls.Add(this.btn_CC_ChiTiet);
            this.Panel_Report.Controls.Add(this.btn_KhoanChuyenCan);
            this.Panel_Report.Controls.Add(this.btn_SLDHChuaChiaLai);
            this.Panel_Report.Controls.Add(this.btn_Rpt_Mau01);
            this.Panel_Report.Controls.Add(this.btn_Rpt3);
            this.Panel_Report.Controls.Add(this.btn_SL_TaiBP);
            this.Panel_Report.Controls.Add(this.btn_Report20);
            this.Panel_Report.Controls.Add(this.btn_Report25);
            this.Panel_Report.Controls.Add(this.btn_TongLuongKhoan_SP);
            this.Panel_Report.Controls.Add(this.btn_Report23);
            this.Panel_Report.Controls.Add(this.btn_Rpt_19);
            this.Panel_Report.Controls.Add(this.btn_Rpt9);
            this.Panel_Report.Controls.Add(this.btn_Rpt45);
            this.Panel_Report.Controls.Add(this.btn_Rpt44);
            this.Panel_Report.Controls.Add(this.btn_Rpt42);
            this.Panel_Report.Controls.Add(this.btn_Rpt41);
            this.Panel_Report.Controls.Add(this.btn_PayAtm);
            this.Panel_Report.Controls.Add(this.btn_Rpt17);
            this.Panel_Report.Controls.Add(this.btn_Rpt_16);
            this.Panel_Report.Controls.Add(this.btn_Rpt13);
            this.Panel_Report.Controls.Add(this.btn_Rpt8);
            this.Panel_Report.Controls.Add(this.btn_PhieuLuong);
            this.Panel_Report.Controls.Add(this.btn_UngLuong);
            this.Panel_Report.Controls.Add(this.btn_Rpt18);
            this.Panel_Report.Controls.Add(this.btn_Rpt12);
            this.Panel_Report.Controls.Add(this.btn_Rpt4);
            this.Panel_Report.Controls.Add(this.btn_TrungBinhNgay);
            this.Panel_Report.Controls.Add(this.btn_Rpt1);
            this.Panel_Report.Location = new System.Drawing.Point(2993, 2175);
            this.Panel_Report.Name = "Panel_Report";
            this.Panel_Report.Size = new System.Drawing.Size(927, 968);
            this.Panel_Report.TabIndex = 199;
            // 
            // btn_Business
            // 
            this.btn_Business.Location = new System.Drawing.Point(290, 417);
            this.btn_Business.Name = "btn_Business";
            this.btn_Business.Size = new System.Drawing.Size(90, 90);
            this.btn_Business.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Business.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Business.StateCommon.Back.ColorAngle = 75F;
            this.btn_Business.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Business.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Business.StateCommon.Border.Rounding = 10;
            this.btn_Business.StateCommon.Border.Width = 2;
            this.btn_Business.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Business.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Business.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Business.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Business.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Business.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Business.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Business.TabIndex = 285;
            this.btn_Business.Tag = "SUB_0480";
            this.btn_Business.Values.Text = "Báo cáo\r\nthưởng\r\nkinh\r\ndoanh";
            // 
            // btn_TheKho
            // 
            this.btn_TheKho.Location = new System.Drawing.Point(290, 871);
            this.btn_TheKho.Name = "btn_TheKho";
            this.btn_TheKho.Size = new System.Drawing.Size(90, 90);
            this.btn_TheKho.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_TheKho.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_TheKho.StateCommon.Back.ColorAngle = 75F;
            this.btn_TheKho.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_TheKho.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_TheKho.StateCommon.Border.Rounding = 10;
            this.btn_TheKho.StateCommon.Border.Width = 2;
            this.btn_TheKho.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_TheKho.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_TheKho.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_TheKho.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_TheKho.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_TheKho.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_TheKho.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_TheKho.TabIndex = 206;
            this.btn_TheKho.Tag = "SUB_0490";
            this.btn_TheKho.Values.Text = "Thẻ kho";
            // 
            // btn_BangKeXuatKho
            // 
            this.btn_BangKeXuatKho.Location = new System.Drawing.Point(197, 870);
            this.btn_BangKeXuatKho.Name = "btn_BangKeXuatKho";
            this.btn_BangKeXuatKho.Size = new System.Drawing.Size(90, 90);
            this.btn_BangKeXuatKho.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_BangKeXuatKho.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_BangKeXuatKho.StateCommon.Back.ColorAngle = 75F;
            this.btn_BangKeXuatKho.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_BangKeXuatKho.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_BangKeXuatKho.StateCommon.Border.Rounding = 10;
            this.btn_BangKeXuatKho.StateCommon.Border.Width = 2;
            this.btn_BangKeXuatKho.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_BangKeXuatKho.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_BangKeXuatKho.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_BangKeXuatKho.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_BangKeXuatKho.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_BangKeXuatKho.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_BangKeXuatKho.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_BangKeXuatKho.TabIndex = 206;
            this.btn_BangKeXuatKho.Tag = "SUB_0489";
            this.btn_BangKeXuatKho.Values.Text = "Bảng kê \r\nxuất kho";
            // 
            // btn_BangKeNhapKho
            // 
            this.btn_BangKeNhapKho.Location = new System.Drawing.Point(103, 869);
            this.btn_BangKeNhapKho.Name = "btn_BangKeNhapKho";
            this.btn_BangKeNhapKho.Size = new System.Drawing.Size(90, 90);
            this.btn_BangKeNhapKho.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_BangKeNhapKho.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_BangKeNhapKho.StateCommon.Back.ColorAngle = 75F;
            this.btn_BangKeNhapKho.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_BangKeNhapKho.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_BangKeNhapKho.StateCommon.Border.Rounding = 10;
            this.btn_BangKeNhapKho.StateCommon.Border.Width = 2;
            this.btn_BangKeNhapKho.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_BangKeNhapKho.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_BangKeNhapKho.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_BangKeNhapKho.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_BangKeNhapKho.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_BangKeNhapKho.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_BangKeNhapKho.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_BangKeNhapKho.TabIndex = 206;
            this.btn_BangKeNhapKho.Tag = "SUB_0488";
            this.btn_BangKeNhapKho.Values.Text = "Bảng kê \r\nnhập kho";
            // 
            // btn_StockProduct
            // 
            this.btn_StockProduct.Location = new System.Drawing.Point(10, 868);
            this.btn_StockProduct.Name = "btn_StockProduct";
            this.btn_StockProduct.Size = new System.Drawing.Size(90, 90);
            this.btn_StockProduct.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_StockProduct.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_StockProduct.StateCommon.Back.ColorAngle = 75F;
            this.btn_StockProduct.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_StockProduct.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_StockProduct.StateCommon.Border.Rounding = 10;
            this.btn_StockProduct.StateCommon.Border.Width = 2;
            this.btn_StockProduct.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_StockProduct.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_StockProduct.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_StockProduct.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_StockProduct.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_StockProduct.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_StockProduct.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_StockProduct.TabIndex = 206;
            this.btn_StockProduct.Tag = "SUB_0487";
            this.btn_StockProduct.Values.Text = "Nhập xuất\r\ntồn kho";
            // 
            // btn_Rpt48
            // 
            this.btn_Rpt48.Location = new System.Drawing.Point(485, 419);
            this.btn_Rpt48.Name = "btn_Rpt48";
            this.btn_Rpt48.Size = new System.Drawing.Size(90, 90);
            this.btn_Rpt48.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Rpt48.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Rpt48.StateCommon.Back.ColorAngle = 75F;
            this.btn_Rpt48.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Rpt48.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Rpt48.StateCommon.Border.Rounding = 10;
            this.btn_Rpt48.StateCommon.Border.Width = 2;
            this.btn_Rpt48.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Rpt48.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt48.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Rpt48.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Rpt48.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt48.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt48.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt48.TabIndex = 203;
            this.btn_Rpt48.Tag = "TNST_SX001";
            this.btn_Rpt48.Values.Text = "4.8";
            this.btn_Rpt48.Visible = false;
            // 
            // btn_HMList
            // 
            this.btn_HMList.Location = new System.Drawing.Point(12, 529);
            this.btn_HMList.Name = "btn_HMList";
            this.btn_HMList.Size = new System.Drawing.Size(90, 90);
            this.btn_HMList.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_HMList.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_HMList.StateCommon.Back.ColorAngle = 75F;
            this.btn_HMList.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_HMList.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_HMList.StateCommon.Border.Rounding = 10;
            this.btn_HMList.StateCommon.Border.Width = 2;
            this.btn_HMList.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_HMList.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_HMList.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_HMList.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_HMList.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_HMList.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_HMList.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_HMList.TabIndex = 205;
            this.btn_HMList.Tag = "SUB_0468";
            this.btn_HMList.Values.Text = "Thống kê\r\ndanh sách\r\nnhân sự";
            // 
            // btn_Rpt47
            // 
            this.btn_Rpt47.Location = new System.Drawing.Point(550, 419);
            this.btn_Rpt47.Name = "btn_Rpt47";
            this.btn_Rpt47.Size = new System.Drawing.Size(90, 90);
            this.btn_Rpt47.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Rpt47.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Rpt47.StateCommon.Back.ColorAngle = 75F;
            this.btn_Rpt47.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Rpt47.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Rpt47.StateCommon.Border.Rounding = 10;
            this.btn_Rpt47.StateCommon.Border.Width = 2;
            this.btn_Rpt47.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Rpt47.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt47.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Rpt47.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Rpt47.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt47.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt47.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt47.TabIndex = 203;
            this.btn_Rpt47.Tag = "TNST_SX001";
            this.btn_Rpt47.Values.Text = "Chi phí\r\ntiền lương\r\n4.7";
            this.btn_Rpt47.Visible = false;
            // 
            // btn_LuongThang13
            // 
            this.btn_LuongThang13.Location = new System.Drawing.Point(382, 417);
            this.btn_LuongThang13.Name = "btn_LuongThang13";
            this.btn_LuongThang13.Size = new System.Drawing.Size(90, 90);
            this.btn_LuongThang13.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_LuongThang13.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_LuongThang13.StateCommon.Back.ColorAngle = 75F;
            this.btn_LuongThang13.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_LuongThang13.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_LuongThang13.StateCommon.Border.Rounding = 10;
            this.btn_LuongThang13.StateCommon.Border.Width = 2;
            this.btn_LuongThang13.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_LuongThang13.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_LuongThang13.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_LuongThang13.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_LuongThang13.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_LuongThang13.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_LuongThang13.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_LuongThang13.TabIndex = 287;
            this.btn_LuongThang13.Tag = "SUB_0481";
            this.btn_LuongThang13.Values.Text = "Báo cáo\r\nlương\r\ntháng 13";
            // 
            // btn_Rpt43
            // 
            this.btn_Rpt43.Location = new System.Drawing.Point(606, 419);
            this.btn_Rpt43.Name = "btn_Rpt43";
            this.btn_Rpt43.Size = new System.Drawing.Size(90, 90);
            this.btn_Rpt43.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Rpt43.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Rpt43.StateCommon.Back.ColorAngle = 75F;
            this.btn_Rpt43.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Rpt43.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Rpt43.StateCommon.Border.Rounding = 10;
            this.btn_Rpt43.StateCommon.Border.Width = 2;
            this.btn_Rpt43.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Rpt43.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt43.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Rpt43.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Rpt43.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt43.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt43.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt43.TabIndex = 203;
            this.btn_Rpt43.Tag = "TNST_SX001";
            this.btn_Rpt43.Values.Text = "4.3";
            this.btn_Rpt43.Visible = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label9.Location = new System.Drawing.Point(4, 511);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(358, 16);
            this.label9.TabIndex = 283;
            this.label9.Text = "Báo cáo nhân sự, chấm công và thời gian làm việc";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label10.Location = new System.Drawing.Point(3, 848);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(159, 16);
            this.label10.TabIndex = 283;
            this.label10.Text = "Báo cáo thống kê kho";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label11.Location = new System.Drawing.Point(3, 735);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(202, 16);
            this.label11.TabIndex = 283;
            this.label11.Text = "Báo cáo thống kê năng suất";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label3.Location = new System.Drawing.Point(5, 622);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(195, 16);
            this.label3.TabIndex = 283;
            this.label3.Text = "Báo cáo thống kê thu nhập";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label1.Location = new System.Drawing.Point(3, 301);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(178, 16);
            this.label1.TabIndex = 283;
            this.label1.Text = "Báo cáo tổng hợp lương";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label2.Location = new System.Drawing.Point(-1, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(220, 16);
            this.label2.TabIndex = 283;
            this.label2.Text = "Báo cáo lương khoán sản xuất";
            // 
            // btn_Rpt5
            // 
            this.btn_Rpt5.Location = new System.Drawing.Point(287, 23);
            this.btn_Rpt5.Name = "btn_Rpt5";
            this.btn_Rpt5.Size = new System.Drawing.Size(90, 90);
            this.btn_Rpt5.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Rpt5.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Rpt5.StateCommon.Back.ColorAngle = 75F;
            this.btn_Rpt5.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Rpt5.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Rpt5.StateCommon.Border.Rounding = 10;
            this.btn_Rpt5.StateCommon.Border.Width = 2;
            this.btn_Rpt5.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Rpt5.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt5.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Rpt5.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Rpt5.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt5.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt5.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt5.TabIndex = 206;
            this.btn_Rpt5.Tag = "SUB_0454";
            this.btn_Rpt5.Values.Text = "LK theo\r\n sản phẩm";
            this.btn_Rpt5.Click += new System.EventHandler(this.btn_Rpt5_Click);
            // 
            // btn_Office
            // 
            this.btn_Office.Location = new System.Drawing.Point(385, 644);
            this.btn_Office.Name = "btn_Office";
            this.btn_Office.Size = new System.Drawing.Size(90, 90);
            this.btn_Office.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Office.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Office.StateCommon.Back.ColorAngle = 75F;
            this.btn_Office.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Office.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Office.StateCommon.Border.Rounding = 10;
            this.btn_Office.StateCommon.Border.Width = 2;
            this.btn_Office.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Office.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Office.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Office.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Office.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Office.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Office.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Office.TabIndex = 205;
            this.btn_Office.Tag = "SUB_0476";
            this.btn_Office.Values.Text = "Thu nhập\r\nbộ phận\r\ngián tiếp\r\ntheo tháng";
            // 
            // btn_Support
            // 
            this.btn_Support.Location = new System.Drawing.Point(293, 643);
            this.btn_Support.Name = "btn_Support";
            this.btn_Support.Size = new System.Drawing.Size(90, 90);
            this.btn_Support.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Support.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Support.StateCommon.Back.ColorAngle = 75F;
            this.btn_Support.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Support.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Support.StateCommon.Border.Rounding = 10;
            this.btn_Support.StateCommon.Border.Width = 2;
            this.btn_Support.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Support.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Support.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Support.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Support.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Support.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Support.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Support.TabIndex = 205;
            this.btn_Support.Tag = "SUB_0475";
            this.btn_Support.Values.Text = "Thu nhập\r\nbộ phận\r\nHTSX\r\ntheo tháng";
            // 
            // btn_Worker
            // 
            this.btn_Worker.Location = new System.Drawing.Point(200, 642);
            this.btn_Worker.Name = "btn_Worker";
            this.btn_Worker.Size = new System.Drawing.Size(90, 90);
            this.btn_Worker.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Worker.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Worker.StateCommon.Back.ColorAngle = 75F;
            this.btn_Worker.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Worker.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Worker.StateCommon.Border.Rounding = 10;
            this.btn_Worker.StateCommon.Border.Width = 2;
            this.btn_Worker.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Worker.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Worker.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Worker.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Worker.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Worker.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Worker.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Worker.TabIndex = 205;
            this.btn_Worker.Tag = "SUB_0474";
            this.btn_Worker.Values.Text = "Thu nhập\r\nbộ phận\r\nsản xuât\r\ntheo tháng";
            // 
            // btn_PBChucVu
            // 
            this.btn_PBChucVu.Location = new System.Drawing.Point(474, 531);
            this.btn_PBChucVu.Name = "btn_PBChucVu";
            this.btn_PBChucVu.Size = new System.Drawing.Size(90, 90);
            this.btn_PBChucVu.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_PBChucVu.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_PBChucVu.StateCommon.Back.ColorAngle = 75F;
            this.btn_PBChucVu.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_PBChucVu.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_PBChucVu.StateCommon.Border.Rounding = 10;
            this.btn_PBChucVu.StateCommon.Border.Width = 2;
            this.btn_PBChucVu.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_PBChucVu.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_PBChucVu.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_PBChucVu.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_PBChucVu.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_PBChucVu.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_PBChucVu.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_PBChucVu.TabIndex = 205;
            this.btn_PBChucVu.Tag = "SUB_0498";
            this.btn_PBChucVu.Values.Text = "Phân bổ \r\nchức vụ";
            // 
            // btn_TG_LamViecTaiXuong
            // 
            this.btn_TG_LamViecTaiXuong.Location = new System.Drawing.Point(382, 530);
            this.btn_TG_LamViecTaiXuong.Name = "btn_TG_LamViecTaiXuong";
            this.btn_TG_LamViecTaiXuong.Size = new System.Drawing.Size(90, 90);
            this.btn_TG_LamViecTaiXuong.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_TG_LamViecTaiXuong.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_TG_LamViecTaiXuong.StateCommon.Back.ColorAngle = 75F;
            this.btn_TG_LamViecTaiXuong.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_TG_LamViecTaiXuong.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_TG_LamViecTaiXuong.StateCommon.Border.Rounding = 10;
            this.btn_TG_LamViecTaiXuong.StateCommon.Border.Width = 2;
            this.btn_TG_LamViecTaiXuong.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_TG_LamViecTaiXuong.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_TG_LamViecTaiXuong.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_TG_LamViecTaiXuong.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_TG_LamViecTaiXuong.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_TG_LamViecTaiXuong.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_TG_LamViecTaiXuong.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_TG_LamViecTaiXuong.TabIndex = 205;
            this.btn_TG_LamViecTaiXuong.Tag = "SUB_0472";
            this.btn_TG_LamViecTaiXuong.Values.Text = "Thời gian\r\nlàm việc\r\ntại xưởng\r\n các tháng";
            // 
            // btn_Rpt_ThoiGian
            // 
            this.btn_Rpt_ThoiGian.Location = new System.Drawing.Point(290, 530);
            this.btn_Rpt_ThoiGian.Name = "btn_Rpt_ThoiGian";
            this.btn_Rpt_ThoiGian.Size = new System.Drawing.Size(90, 90);
            this.btn_Rpt_ThoiGian.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Rpt_ThoiGian.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Rpt_ThoiGian.StateCommon.Back.ColorAngle = 75F;
            this.btn_Rpt_ThoiGian.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Rpt_ThoiGian.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Rpt_ThoiGian.StateCommon.Border.Rounding = 10;
            this.btn_Rpt_ThoiGian.StateCommon.Border.Width = 2;
            this.btn_Rpt_ThoiGian.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Rpt_ThoiGian.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt_ThoiGian.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Rpt_ThoiGian.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Rpt_ThoiGian.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt_ThoiGian.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt_ThoiGian.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt_ThoiGian.TabIndex = 205;
            this.btn_Rpt_ThoiGian.Tag = "SUB_0471";
            this.btn_Rpt_ThoiGian.Values.Text = "Thời gian\r\nlàm việc\r\ntại xưởng\r\n 1 tháng";
            // 
            // btn_LuongTongHopSanXuat_ChiTiet
            // 
            this.btn_LuongTongHopSanXuat_ChiTiet.Location = new System.Drawing.Point(13, 417);
            this.btn_LuongTongHopSanXuat_ChiTiet.Name = "btn_LuongTongHopSanXuat_ChiTiet";
            this.btn_LuongTongHopSanXuat_ChiTiet.Size = new System.Drawing.Size(90, 90);
            this.btn_LuongTongHopSanXuat_ChiTiet.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_LuongTongHopSanXuat_ChiTiet.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_LuongTongHopSanXuat_ChiTiet.StateCommon.Back.ColorAngle = 75F;
            this.btn_LuongTongHopSanXuat_ChiTiet.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_LuongTongHopSanXuat_ChiTiet.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_LuongTongHopSanXuat_ChiTiet.StateCommon.Border.Rounding = 10;
            this.btn_LuongTongHopSanXuat_ChiTiet.StateCommon.Border.Width = 2;
            this.btn_LuongTongHopSanXuat_ChiTiet.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_LuongTongHopSanXuat_ChiTiet.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_LuongTongHopSanXuat_ChiTiet.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_LuongTongHopSanXuat_ChiTiet.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_LuongTongHopSanXuat_ChiTiet.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_LuongTongHopSanXuat_ChiTiet.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_LuongTongHopSanXuat_ChiTiet.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_LuongTongHopSanXuat_ChiTiet.TabIndex = 205;
            this.btn_LuongTongHopSanXuat_ChiTiet.Tag = "SUB_0477";
            this.btn_LuongTongHopSanXuat_ChiTiet.Values.Text = "Lương \r\ntổng hợp  \r\nsản xuất\r\n(chi tiết)";
            // 
            // btn_LuongTongHopHoTro_ChiTiet
            // 
            this.btn_LuongTongHopHoTro_ChiTiet.Location = new System.Drawing.Point(106, 417);
            this.btn_LuongTongHopHoTro_ChiTiet.Name = "btn_LuongTongHopHoTro_ChiTiet";
            this.btn_LuongTongHopHoTro_ChiTiet.Size = new System.Drawing.Size(90, 90);
            this.btn_LuongTongHopHoTro_ChiTiet.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_LuongTongHopHoTro_ChiTiet.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_LuongTongHopHoTro_ChiTiet.StateCommon.Back.ColorAngle = 75F;
            this.btn_LuongTongHopHoTro_ChiTiet.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_LuongTongHopHoTro_ChiTiet.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_LuongTongHopHoTro_ChiTiet.StateCommon.Border.Rounding = 10;
            this.btn_LuongTongHopHoTro_ChiTiet.StateCommon.Border.Width = 2;
            this.btn_LuongTongHopHoTro_ChiTiet.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_LuongTongHopHoTro_ChiTiet.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_LuongTongHopHoTro_ChiTiet.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_LuongTongHopHoTro_ChiTiet.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_LuongTongHopHoTro_ChiTiet.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_LuongTongHopHoTro_ChiTiet.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_LuongTongHopHoTro_ChiTiet.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_LuongTongHopHoTro_ChiTiet.TabIndex = 205;
            this.btn_LuongTongHopHoTro_ChiTiet.Tag = "SUB_0478";
            this.btn_LuongTongHopHoTro_ChiTiet.Values.Text = "Lương \r\ntổng hợp  \r\nHTSX\r\n(chi tiết)";
            // 
            // btn_LuongTongHop_ChiTiet
            // 
            this.btn_LuongTongHop_ChiTiet.Location = new System.Drawing.Point(198, 417);
            this.btn_LuongTongHop_ChiTiet.Name = "btn_LuongTongHop_ChiTiet";
            this.btn_LuongTongHop_ChiTiet.Size = new System.Drawing.Size(90, 90);
            this.btn_LuongTongHop_ChiTiet.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_LuongTongHop_ChiTiet.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_LuongTongHop_ChiTiet.StateCommon.Back.ColorAngle = 75F;
            this.btn_LuongTongHop_ChiTiet.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_LuongTongHop_ChiTiet.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_LuongTongHop_ChiTiet.StateCommon.Border.Rounding = 10;
            this.btn_LuongTongHop_ChiTiet.StateCommon.Border.Width = 2;
            this.btn_LuongTongHop_ChiTiet.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_LuongTongHop_ChiTiet.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_LuongTongHop_ChiTiet.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_LuongTongHop_ChiTiet.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_LuongTongHop_ChiTiet.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_LuongTongHop_ChiTiet.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_LuongTongHop_ChiTiet.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_LuongTongHop_ChiTiet.TabIndex = 205;
            this.btn_LuongTongHop_ChiTiet.Tag = "SUB_0479";
            this.btn_LuongTongHop_ChiTiet.Values.Text = "Lương \r\ntổng hợp  \r\ngián tiếp\r\n(chi tiết)";
            // 
            // btn_THThuNhap
            // 
            this.btn_THThuNhap.Location = new System.Drawing.Point(107, 641);
            this.btn_THThuNhap.Name = "btn_THThuNhap";
            this.btn_THThuNhap.Size = new System.Drawing.Size(90, 90);
            this.btn_THThuNhap.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_THThuNhap.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_THThuNhap.StateCommon.Back.ColorAngle = 75F;
            this.btn_THThuNhap.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_THThuNhap.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_THThuNhap.StateCommon.Border.Rounding = 10;
            this.btn_THThuNhap.StateCommon.Border.Width = 2;
            this.btn_THThuNhap.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_THThuNhap.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_THThuNhap.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_THThuNhap.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_THThuNhap.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_THThuNhap.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_THThuNhap.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_THThuNhap.TabIndex = 205;
            this.btn_THThuNhap.Tag = "SUB_0473";
            this.btn_THThuNhap.Values.Text = "Thu nhập\r\ntheo khối/\r\nbộ phận/\r\ntổ nhóm";
            // 
            // btn_Bao_Cao_Ngay
            // 
            this.btn_Bao_Cao_Ngay.Location = new System.Drawing.Point(197, 530);
            this.btn_Bao_Cao_Ngay.Name = "btn_Bao_Cao_Ngay";
            this.btn_Bao_Cao_Ngay.Size = new System.Drawing.Size(90, 90);
            this.btn_Bao_Cao_Ngay.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Bao_Cao_Ngay.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Bao_Cao_Ngay.StateCommon.Back.ColorAngle = 75F;
            this.btn_Bao_Cao_Ngay.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Bao_Cao_Ngay.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Bao_Cao_Ngay.StateCommon.Border.Rounding = 10;
            this.btn_Bao_Cao_Ngay.StateCommon.Border.Width = 2;
            this.btn_Bao_Cao_Ngay.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Bao_Cao_Ngay.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Bao_Cao_Ngay.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Bao_Cao_Ngay.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Bao_Cao_Ngay.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Bao_Cao_Ngay.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Bao_Cao_Ngay.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Bao_Cao_Ngay.TabIndex = 205;
            this.btn_Bao_Cao_Ngay.Tag = "SUB_0470";
            this.btn_Bao_Cao_Ngay.Values.Text = "Báo cáo\r\nChấm công\r\nTổng giờ\r\nGiờ dư";
            // 
            // btn_Adjusted
            // 
            this.btn_Adjusted.Location = new System.Drawing.Point(563, 23);
            this.btn_Adjusted.Name = "btn_Adjusted";
            this.btn_Adjusted.Size = new System.Drawing.Size(90, 90);
            this.btn_Adjusted.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Adjusted.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Adjusted.StateCommon.Back.ColorAngle = 75F;
            this.btn_Adjusted.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Adjusted.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Adjusted.StateCommon.Border.Rounding = 10;
            this.btn_Adjusted.StateCommon.Border.Width = 2;
            this.btn_Adjusted.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Adjusted.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Adjusted.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Adjusted.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Adjusted.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Adjusted.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Adjusted.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Adjusted.TabIndex = 205;
            this.btn_Adjusted.Tag = "SUB_0456";
            this.btn_Adjusted.Values.Text = "Lương\r\nkhoán \r\nchia lại";
            // 
            // btn_Rpt2
            // 
            this.btn_Rpt2.Location = new System.Drawing.Point(195, 24);
            this.btn_Rpt2.Name = "btn_Rpt2";
            this.btn_Rpt2.Size = new System.Drawing.Size(90, 90);
            this.btn_Rpt2.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Rpt2.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Rpt2.StateCommon.Back.ColorAngle = 75F;
            this.btn_Rpt2.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Rpt2.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Rpt2.StateCommon.Border.Rounding = 10;
            this.btn_Rpt2.StateCommon.Border.Width = 2;
            this.btn_Rpt2.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Rpt2.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt2.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Rpt2.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Rpt2.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt2.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt2.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt2.TabIndex = 204;
            this.btn_Rpt2.Tag = "SUB_0453";
            this.btn_Rpt2.Values.Text = "LK công \r\nnhân theo\r\nnhóm công\r\nđoạn";
            // 
            // btn_Rpt_Mau02
            // 
            this.btn_Rpt_Mau02.Location = new System.Drawing.Point(290, 209);
            this.btn_Rpt_Mau02.Name = "btn_Rpt_Mau02";
            this.btn_Rpt_Mau02.Size = new System.Drawing.Size(90, 90);
            this.btn_Rpt_Mau02.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Rpt_Mau02.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Rpt_Mau02.StateCommon.Back.ColorAngle = 75F;
            this.btn_Rpt_Mau02.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Rpt_Mau02.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Rpt_Mau02.StateCommon.Border.Rounding = 10;
            this.btn_Rpt_Mau02.StateCommon.Border.Width = 2;
            this.btn_Rpt_Mau02.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Rpt_Mau02.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt_Mau02.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Rpt_Mau02.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Rpt_Mau02.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt_Mau02.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt_Mau02.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt_Mau02.TabIndex = 203;
            this.btn_Rpt_Mau02.Tag = "SUB_0461";
            this.btn_Rpt_Mau02.Values.Text = "Phân bổ \r\ntổng hợp";
            // 
            // btn_TBLK_TaiTo
            // 
            this.btn_TBLK_TaiTo.Location = new System.Drawing.Point(471, 23);
            this.btn_TBLK_TaiTo.Name = "btn_TBLK_TaiTo";
            this.btn_TBLK_TaiTo.Size = new System.Drawing.Size(90, 90);
            this.btn_TBLK_TaiTo.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_TBLK_TaiTo.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_TBLK_TaiTo.StateCommon.Back.ColorAngle = 75F;
            this.btn_TBLK_TaiTo.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_TBLK_TaiTo.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_TBLK_TaiTo.StateCommon.Border.Rounding = 10;
            this.btn_TBLK_TaiTo.StateCommon.Border.Width = 2;
            this.btn_TBLK_TaiTo.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_TBLK_TaiTo.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_TBLK_TaiTo.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_TBLK_TaiTo.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_TBLK_TaiTo.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_TBLK_TaiTo.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_TBLK_TaiTo.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_TBLK_TaiTo.TabIndex = 203;
            this.btn_TBLK_TaiTo.Tag = "SUB_0493";
            this.btn_TBLK_TaiTo.Values.Text = "Trung bình\r\nLK / giờ\r\n tại tổ\r\n";
            // 
            // btn_CC_ChiTiet
            // 
            this.btn_CC_ChiTiet.Location = new System.Drawing.Point(656, 117);
            this.btn_CC_ChiTiet.Name = "btn_CC_ChiTiet";
            this.btn_CC_ChiTiet.Size = new System.Drawing.Size(90, 90);
            this.btn_CC_ChiTiet.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_CC_ChiTiet.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_CC_ChiTiet.StateCommon.Back.ColorAngle = 75F;
            this.btn_CC_ChiTiet.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_CC_ChiTiet.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_CC_ChiTiet.StateCommon.Border.Rounding = 10;
            this.btn_CC_ChiTiet.StateCommon.Border.Width = 2;
            this.btn_CC_ChiTiet.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_CC_ChiTiet.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_CC_ChiTiet.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_CC_ChiTiet.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_CC_ChiTiet.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_CC_ChiTiet.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_CC_ChiTiet.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_CC_ChiTiet.TabIndex = 203;
            this.btn_CC_ChiTiet.Tag = "SUB_0497";
            this.btn_CC_ChiTiet.Values.Text = "Báo cáo\r\nkhoán tính\r\nchuyên cần\r\n- chi tiết";
            // 
            // btn_KhoanChuyenCan
            // 
            this.btn_KhoanChuyenCan.Location = new System.Drawing.Point(564, 116);
            this.btn_KhoanChuyenCan.Name = "btn_KhoanChuyenCan";
            this.btn_KhoanChuyenCan.Size = new System.Drawing.Size(90, 90);
            this.btn_KhoanChuyenCan.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_KhoanChuyenCan.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_KhoanChuyenCan.StateCommon.Back.ColorAngle = 75F;
            this.btn_KhoanChuyenCan.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_KhoanChuyenCan.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_KhoanChuyenCan.StateCommon.Border.Rounding = 10;
            this.btn_KhoanChuyenCan.StateCommon.Border.Width = 2;
            this.btn_KhoanChuyenCan.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_KhoanChuyenCan.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_KhoanChuyenCan.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_KhoanChuyenCan.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_KhoanChuyenCan.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_KhoanChuyenCan.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_KhoanChuyenCan.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_KhoanChuyenCan.TabIndex = 203;
            this.btn_KhoanChuyenCan.Tag = "SUB_0495";
            this.btn_KhoanChuyenCan.Values.Text = "Báo cáo\r\nkhoán tính\r\nchuyên cần\r\n";
            // 
            // btn_SLDHChuaChiaLai
            // 
            this.btn_SLDHChuaChiaLai.Location = new System.Drawing.Point(655, 24);
            this.btn_SLDHChuaChiaLai.Name = "btn_SLDHChuaChiaLai";
            this.btn_SLDHChuaChiaLai.Size = new System.Drawing.Size(90, 90);
            this.btn_SLDHChuaChiaLai.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_SLDHChuaChiaLai.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_SLDHChuaChiaLai.StateCommon.Back.ColorAngle = 75F;
            this.btn_SLDHChuaChiaLai.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_SLDHChuaChiaLai.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_SLDHChuaChiaLai.StateCommon.Border.Rounding = 10;
            this.btn_SLDHChuaChiaLai.StateCommon.Border.Width = 2;
            this.btn_SLDHChuaChiaLai.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_SLDHChuaChiaLai.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_SLDHChuaChiaLai.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_SLDHChuaChiaLai.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_SLDHChuaChiaLai.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_SLDHChuaChiaLai.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_SLDHChuaChiaLai.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_SLDHChuaChiaLai.TabIndex = 203;
            this.btn_SLDHChuaChiaLai.Tag = "SUB_0491";
            this.btn_SLDHChuaChiaLai.Values.Text = "Số lượng\r\nđơn hàng\r\nChưa\r\nChia lại\r\n";
            // 
            // btn_Rpt_Mau01
            // 
            this.btn_Rpt_Mau01.Location = new System.Drawing.Point(382, 209);
            this.btn_Rpt_Mau01.Name = "btn_Rpt_Mau01";
            this.btn_Rpt_Mau01.Size = new System.Drawing.Size(90, 90);
            this.btn_Rpt_Mau01.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Rpt_Mau01.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Rpt_Mau01.StateCommon.Back.ColorAngle = 75F;
            this.btn_Rpt_Mau01.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Rpt_Mau01.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Rpt_Mau01.StateCommon.Border.Rounding = 10;
            this.btn_Rpt_Mau01.StateCommon.Border.Width = 2;
            this.btn_Rpt_Mau01.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Rpt_Mau01.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt_Mau01.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Rpt_Mau01.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Rpt_Mau01.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt_Mau01.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt_Mau01.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt_Mau01.TabIndex = 203;
            this.btn_Rpt_Mau01.Tag = "SUB_0485";
            this.btn_Rpt_Mau01.Values.Text = "Phân bổ \r\nchi tiết\r\n";
            // 
            // btn_Rpt3
            // 
            this.btn_Rpt3.Location = new System.Drawing.Point(379, 23);
            this.btn_Rpt3.Name = "btn_Rpt3";
            this.btn_Rpt3.Size = new System.Drawing.Size(90, 90);
            this.btn_Rpt3.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Rpt3.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Rpt3.StateCommon.Back.ColorAngle = 75F;
            this.btn_Rpt3.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Rpt3.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Rpt3.StateCommon.Border.Rounding = 10;
            this.btn_Rpt3.StateCommon.Border.Width = 2;
            this.btn_Rpt3.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Rpt3.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt3.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Rpt3.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Rpt3.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt3.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt3.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt3.TabIndex = 203;
            this.btn_Rpt3.Tag = "SUB_0486";
            this.btn_Rpt3.Values.Text = "LK SX\r\ntại tổ \r\ntheo ngày";
            // 
            // btn_SL_TaiBP
            // 
            this.btn_SL_TaiBP.Location = new System.Drawing.Point(104, 117);
            this.btn_SL_TaiBP.Name = "btn_SL_TaiBP";
            this.btn_SL_TaiBP.Size = new System.Drawing.Size(90, 90);
            this.btn_SL_TaiBP.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_SL_TaiBP.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_SL_TaiBP.StateCommon.Back.ColorAngle = 75F;
            this.btn_SL_TaiBP.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_SL_TaiBP.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_SL_TaiBP.StateCommon.Border.Rounding = 10;
            this.btn_SL_TaiBP.StateCommon.Border.Width = 2;
            this.btn_SL_TaiBP.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_SL_TaiBP.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_SL_TaiBP.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_SL_TaiBP.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_SL_TaiBP.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_SL_TaiBP.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_SL_TaiBP.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_SL_TaiBP.TabIndex = 203;
            this.btn_SL_TaiBP.Tag = "SUB_0494";
            this.btn_SL_TaiBP.Values.Text = "Lương \r\nkhoán\r\ntại bộ \r\nphận - IN";
            // 
            // btn_Report20
            // 
            this.btn_Report20.Location = new System.Drawing.Point(12, 116);
            this.btn_Report20.Name = "btn_Report20";
            this.btn_Report20.Size = new System.Drawing.Size(90, 90);
            this.btn_Report20.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Report20.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Report20.StateCommon.Back.ColorAngle = 75F;
            this.btn_Report20.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Report20.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Report20.StateCommon.Border.Rounding = 10;
            this.btn_Report20.StateCommon.Border.Width = 2;
            this.btn_Report20.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Report20.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Report20.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Report20.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Report20.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Report20.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Report20.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Report20.TabIndex = 203;
            this.btn_Report20.Tag = "SUB_0455";
            this.btn_Report20.Values.Text = "Lương \r\nkhoán\r\ntại bộ phận";
            // 
            // btn_Report25
            // 
            this.btn_Report25.Location = new System.Drawing.Point(472, 117);
            this.btn_Report25.Name = "btn_Report25";
            this.btn_Report25.Size = new System.Drawing.Size(90, 90);
            this.btn_Report25.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Report25.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Report25.StateCommon.Back.ColorAngle = 75F;
            this.btn_Report25.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Report25.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Report25.StateCommon.Border.Rounding = 10;
            this.btn_Report25.StateCommon.Border.Width = 2;
            this.btn_Report25.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Report25.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Report25.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Report25.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Report25.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Report25.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Report25.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Report25.TabIndex = 203;
            this.btn_Report25.Tag = "SUB_0500";
            this.btn_Report25.Values.Text = "Tổng lương\r\nkhoán\r\ncông nhân\r\n- SP và CĐ";
            // 
            // btn_TongLuongKhoan_SP
            // 
            this.btn_TongLuongKhoan_SP.Location = new System.Drawing.Point(380, 117);
            this.btn_TongLuongKhoan_SP.Name = "btn_TongLuongKhoan_SP";
            this.btn_TongLuongKhoan_SP.Size = new System.Drawing.Size(90, 90);
            this.btn_TongLuongKhoan_SP.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_TongLuongKhoan_SP.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_TongLuongKhoan_SP.StateCommon.Back.ColorAngle = 75F;
            this.btn_TongLuongKhoan_SP.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_TongLuongKhoan_SP.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_TongLuongKhoan_SP.StateCommon.Border.Rounding = 10;
            this.btn_TongLuongKhoan_SP.StateCommon.Border.Width = 2;
            this.btn_TongLuongKhoan_SP.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_TongLuongKhoan_SP.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_TongLuongKhoan_SP.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_TongLuongKhoan_SP.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_TongLuongKhoan_SP.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_TongLuongKhoan_SP.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_TongLuongKhoan_SP.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_TongLuongKhoan_SP.TabIndex = 203;
            this.btn_TongLuongKhoan_SP.Tag = "SUB_0499";
            this.btn_TongLuongKhoan_SP.Values.Text = "Tổng lương\r\nkhoán\r\ncông nhân\r\n- SP";
            // 
            // btn_Report23
            // 
            this.btn_Report23.Location = new System.Drawing.Point(288, 116);
            this.btn_Report23.Name = "btn_Report23";
            this.btn_Report23.Size = new System.Drawing.Size(90, 90);
            this.btn_Report23.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Report23.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Report23.StateCommon.Back.ColorAngle = 75F;
            this.btn_Report23.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Report23.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Report23.StateCommon.Border.Rounding = 10;
            this.btn_Report23.StateCommon.Border.Width = 2;
            this.btn_Report23.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Report23.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Report23.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Report23.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Report23.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Report23.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Report23.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Report23.TabIndex = 203;
            this.btn_Report23.Tag = "SUB_0496";
            this.btn_Report23.Values.Text = "Tổng lương\r\nkhoán\r\ncông nhân\r\n- IN";
            // 
            // btn_Rpt_19
            // 
            this.btn_Rpt_19.Location = new System.Drawing.Point(196, 116);
            this.btn_Rpt_19.Name = "btn_Rpt_19";
            this.btn_Rpt_19.Size = new System.Drawing.Size(90, 90);
            this.btn_Rpt_19.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Rpt_19.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Rpt_19.StateCommon.Back.ColorAngle = 75F;
            this.btn_Rpt_19.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Rpt_19.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Rpt_19.StateCommon.Border.Rounding = 10;
            this.btn_Rpt_19.StateCommon.Border.Width = 2;
            this.btn_Rpt_19.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Rpt_19.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt_19.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Rpt_19.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Rpt_19.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt_19.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt_19.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt_19.TabIndex = 203;
            this.btn_Rpt_19.Tag = "SUB_0457";
            this.btn_Rpt_19.Values.Text = "Tổng lương\r\nkhoán\r\ncông nhân";
            // 
            // btn_Rpt9
            // 
            this.btn_Rpt9.Location = new System.Drawing.Point(198, 209);
            this.btn_Rpt9.Name = "btn_Rpt9";
            this.btn_Rpt9.Size = new System.Drawing.Size(90, 90);
            this.btn_Rpt9.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Rpt9.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Rpt9.StateCommon.Back.ColorAngle = 75F;
            this.btn_Rpt9.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Rpt9.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Rpt9.StateCommon.Border.Rounding = 10;
            this.btn_Rpt9.StateCommon.Border.Width = 2;
            this.btn_Rpt9.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Rpt9.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt9.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Rpt9.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Rpt9.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt9.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt9.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt9.TabIndex = 203;
            this.btn_Rpt9.Tag = "SUB_0460";
            this.btn_Rpt9.Values.Text = "LK\r\ntổng hợp\r\ntheo \r\nbộ phận";
            // 
            // btn_Rpt45
            // 
            this.btn_Rpt45.Location = new System.Drawing.Point(198, 756);
            this.btn_Rpt45.Name = "btn_Rpt45";
            this.btn_Rpt45.Size = new System.Drawing.Size(90, 90);
            this.btn_Rpt45.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Rpt45.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Rpt45.StateCommon.Back.ColorAngle = 75F;
            this.btn_Rpt45.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Rpt45.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Rpt45.StateCommon.Border.Rounding = 10;
            this.btn_Rpt45.StateCommon.Border.Width = 2;
            this.btn_Rpt45.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Rpt45.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt45.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Rpt45.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Rpt45.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt45.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt45.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt45.TabIndex = 203;
            this.btn_Rpt45.Tag = "SUB_0484";
            this.btn_Rpt45.Values.Text = "4.5";
            // 
            // btn_Rpt44
            // 
            this.btn_Rpt44.Location = new System.Drawing.Point(665, 419);
            this.btn_Rpt44.Name = "btn_Rpt44";
            this.btn_Rpt44.Size = new System.Drawing.Size(90, 90);
            this.btn_Rpt44.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Rpt44.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Rpt44.StateCommon.Back.ColorAngle = 75F;
            this.btn_Rpt44.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Rpt44.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Rpt44.StateCommon.Border.Rounding = 10;
            this.btn_Rpt44.StateCommon.Border.Width = 2;
            this.btn_Rpt44.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Rpt44.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt44.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Rpt44.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Rpt44.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt44.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt44.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt44.TabIndex = 203;
            this.btn_Rpt44.Tag = "TNST_SX001";
            this.btn_Rpt44.Values.Text = "4.4";
            this.btn_Rpt44.Visible = false;
            // 
            // btn_Rpt42
            // 
            this.btn_Rpt42.Location = new System.Drawing.Point(106, 755);
            this.btn_Rpt42.Name = "btn_Rpt42";
            this.btn_Rpt42.Size = new System.Drawing.Size(90, 90);
            this.btn_Rpt42.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Rpt42.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Rpt42.StateCommon.Back.ColorAngle = 75F;
            this.btn_Rpt42.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Rpt42.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Rpt42.StateCommon.Border.Rounding = 10;
            this.btn_Rpt42.StateCommon.Border.Width = 2;
            this.btn_Rpt42.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Rpt42.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt42.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Rpt42.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Rpt42.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt42.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt42.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt42.TabIndex = 203;
            this.btn_Rpt42.Tag = "SUB_0483";
            this.btn_Rpt42.Values.Text = "4.2";
            // 
            // btn_Rpt41
            // 
            this.btn_Rpt41.Location = new System.Drawing.Point(14, 754);
            this.btn_Rpt41.Name = "btn_Rpt41";
            this.btn_Rpt41.Size = new System.Drawing.Size(90, 90);
            this.btn_Rpt41.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Rpt41.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Rpt41.StateCommon.Back.ColorAngle = 75F;
            this.btn_Rpt41.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Rpt41.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Rpt41.StateCommon.Border.Rounding = 10;
            this.btn_Rpt41.StateCommon.Border.Width = 2;
            this.btn_Rpt41.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Rpt41.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt41.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Rpt41.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Rpt41.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt41.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt41.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt41.TabIndex = 203;
            this.btn_Rpt41.Tag = "SUB_0482";
            this.btn_Rpt41.Values.Text = "4.1";
            // 
            // btn_PayAtm
            // 
            this.btn_PayAtm.Location = new System.Drawing.Point(290, 324);
            this.btn_PayAtm.Name = "btn_PayAtm";
            this.btn_PayAtm.Size = new System.Drawing.Size(90, 90);
            this.btn_PayAtm.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_PayAtm.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_PayAtm.StateCommon.Back.ColorAngle = 75F;
            this.btn_PayAtm.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_PayAtm.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_PayAtm.StateCommon.Border.Rounding = 10;
            this.btn_PayAtm.StateCommon.Border.Width = 2;
            this.btn_PayAtm.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_PayAtm.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_PayAtm.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_PayAtm.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_PayAtm.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_PayAtm.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_PayAtm.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_PayAtm.TabIndex = 203;
            this.btn_PayAtm.Tag = "SUB_0465";
            this.btn_PayAtm.Values.Text = "Danh sách\r\nchi lương\r\nngân hàng";
            // 
            // btn_Rpt17
            // 
            this.btn_Rpt17.Location = new System.Drawing.Point(198, 324);
            this.btn_Rpt17.Name = "btn_Rpt17";
            this.btn_Rpt17.Size = new System.Drawing.Size(90, 90);
            this.btn_Rpt17.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Rpt17.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Rpt17.StateCommon.Back.ColorAngle = 75F;
            this.btn_Rpt17.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Rpt17.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Rpt17.StateCommon.Border.Rounding = 10;
            this.btn_Rpt17.StateCommon.Border.Width = 2;
            this.btn_Rpt17.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Rpt17.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt17.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Rpt17.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Rpt17.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt17.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt17.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt17.TabIndex = 203;
            this.btn_Rpt17.Tag = "SUB_0464";
            this.btn_Rpt17.Values.Text = "Báo cáo\r\ntổng hợp\r\nlương khối\r\nGián tiếp";
            // 
            // btn_Rpt_16
            // 
            this.btn_Rpt_16.Location = new System.Drawing.Point(105, 324);
            this.btn_Rpt_16.Name = "btn_Rpt_16";
            this.btn_Rpt_16.Size = new System.Drawing.Size(90, 90);
            this.btn_Rpt_16.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Rpt_16.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Rpt_16.StateCommon.Back.ColorAngle = 75F;
            this.btn_Rpt_16.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Rpt_16.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Rpt_16.StateCommon.Border.Rounding = 10;
            this.btn_Rpt_16.StateCommon.Border.Width = 2;
            this.btn_Rpt_16.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Rpt_16.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt_16.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Rpt_16.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Rpt_16.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt_16.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt_16.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt_16.TabIndex = 203;
            this.btn_Rpt_16.Tag = "SUB_0463";
            this.btn_Rpt_16.Values.Text = "Báo cáo\r\ntổng hợp\r\nlương bộ \r\nphận HTSX";
            // 
            // btn_Rpt13
            // 
            this.btn_Rpt13.Location = new System.Drawing.Point(13, 323);
            this.btn_Rpt13.Name = "btn_Rpt13";
            this.btn_Rpt13.Size = new System.Drawing.Size(90, 90);
            this.btn_Rpt13.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Rpt13.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Rpt13.StateCommon.Back.ColorAngle = 75F;
            this.btn_Rpt13.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Rpt13.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Rpt13.StateCommon.Border.Rounding = 10;
            this.btn_Rpt13.StateCommon.Border.Width = 2;
            this.btn_Rpt13.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Rpt13.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt13.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Rpt13.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Rpt13.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt13.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt13.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt13.TabIndex = 203;
            this.btn_Rpt13.Tag = "SUB_0462";
            this.btn_Rpt13.Values.Text = "Báo cáo\r\ntổng hợp\r\nlương khối\r\nTrực tiếp";
            // 
            // btn_Rpt8
            // 
            this.btn_Rpt8.Location = new System.Drawing.Point(105, 210);
            this.btn_Rpt8.Name = "btn_Rpt8";
            this.btn_Rpt8.Size = new System.Drawing.Size(90, 90);
            this.btn_Rpt8.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Rpt8.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Rpt8.StateCommon.Back.ColorAngle = 75F;
            this.btn_Rpt8.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Rpt8.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Rpt8.StateCommon.Border.Rounding = 10;
            this.btn_Rpt8.StateCommon.Border.Width = 2;
            this.btn_Rpt8.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Rpt8.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt8.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Rpt8.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Rpt8.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt8.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt8.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt8.TabIndex = 203;
            this.btn_Rpt8.Tag = "SUB_0459";
            this.btn_Rpt8.Values.Text = "LK\r\n tổng hợp  \r\ntheo ngày";
            // 
            // btn_PhieuLuong
            // 
            this.btn_PhieuLuong.Location = new System.Drawing.Point(474, 325);
            this.btn_PhieuLuong.Name = "btn_PhieuLuong";
            this.btn_PhieuLuong.Size = new System.Drawing.Size(90, 90);
            this.btn_PhieuLuong.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_PhieuLuong.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_PhieuLuong.StateCommon.Back.ColorAngle = 75F;
            this.btn_PhieuLuong.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_PhieuLuong.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_PhieuLuong.StateCommon.Border.Rounding = 10;
            this.btn_PhieuLuong.StateCommon.Border.Width = 2;
            this.btn_PhieuLuong.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_PhieuLuong.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_PhieuLuong.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_PhieuLuong.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_PhieuLuong.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_PhieuLuong.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_PhieuLuong.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_PhieuLuong.TabIndex = 203;
            this.btn_PhieuLuong.Tag = "SUB_0492";
            this.btn_PhieuLuong.Values.Text = "Phiếu lương";
            // 
            // btn_UngLuong
            // 
            this.btn_UngLuong.Location = new System.Drawing.Point(382, 324);
            this.btn_UngLuong.Name = "btn_UngLuong";
            this.btn_UngLuong.Size = new System.Drawing.Size(90, 90);
            this.btn_UngLuong.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_UngLuong.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_UngLuong.StateCommon.Back.ColorAngle = 75F;
            this.btn_UngLuong.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_UngLuong.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_UngLuong.StateCommon.Border.Rounding = 10;
            this.btn_UngLuong.StateCommon.Border.Width = 2;
            this.btn_UngLuong.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_UngLuong.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_UngLuong.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_UngLuong.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_UngLuong.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_UngLuong.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_UngLuong.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_UngLuong.TabIndex = 203;
            this.btn_UngLuong.Tag = "SUB_0467";
            this.btn_UngLuong.Values.Text = "Danh sách\r\nứng lương";
            // 
            // btn_Rpt18
            // 
            this.btn_Rpt18.Location = new System.Drawing.Point(15, 641);
            this.btn_Rpt18.Name = "btn_Rpt18";
            this.btn_Rpt18.Size = new System.Drawing.Size(90, 90);
            this.btn_Rpt18.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Rpt18.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Rpt18.StateCommon.Back.ColorAngle = 75F;
            this.btn_Rpt18.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Rpt18.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Rpt18.StateCommon.Border.Rounding = 10;
            this.btn_Rpt18.StateCommon.Border.Width = 2;
            this.btn_Rpt18.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Rpt18.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt18.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Rpt18.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Rpt18.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt18.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt18.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt18.TabIndex = 203;
            this.btn_Rpt18.Tag = "SUB_0466";
            this.btn_Rpt18.Values.Text = "Báo cáo\r\ntổng hợp\r\nthu nhập";
            // 
            // btn_Rpt12
            // 
            this.btn_Rpt12.Location = new System.Drawing.Point(104, 529);
            this.btn_Rpt12.Name = "btn_Rpt12";
            this.btn_Rpt12.Size = new System.Drawing.Size(90, 90);
            this.btn_Rpt12.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Rpt12.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Rpt12.StateCommon.Back.ColorAngle = 75F;
            this.btn_Rpt12.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Rpt12.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Rpt12.StateCommon.Border.Rounding = 10;
            this.btn_Rpt12.StateCommon.Border.Width = 2;
            this.btn_Rpt12.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Rpt12.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt12.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Rpt12.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Rpt12.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt12.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt12.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt12.TabIndex = 203;
            this.btn_Rpt12.Tag = "SUB_0469";
            this.btn_Rpt12.Values.Text = "Chấm công \r\ntheo tháng";
            // 
            // btn_Rpt4
            // 
            this.btn_Rpt4.Location = new System.Drawing.Point(12, 209);
            this.btn_Rpt4.Name = "btn_Rpt4";
            this.btn_Rpt4.Size = new System.Drawing.Size(90, 90);
            this.btn_Rpt4.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Rpt4.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Rpt4.StateCommon.Back.ColorAngle = 75F;
            this.btn_Rpt4.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Rpt4.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Rpt4.StateCommon.Border.Rounding = 10;
            this.btn_Rpt4.StateCommon.Border.Width = 2;
            this.btn_Rpt4.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Rpt4.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt4.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Rpt4.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Rpt4.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt4.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt4.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt4.TabIndex = 203;
            this.btn_Rpt4.Tag = "SUB_0458";
            this.btn_Rpt4.Values.Text = "Tổng hợp\r\nLương \r\nkhoán";
            // 
            // btn_TrungBinhNgay
            // 
            this.btn_TrungBinhNgay.Location = new System.Drawing.Point(103, 24);
            this.btn_TrungBinhNgay.Name = "btn_TrungBinhNgay";
            this.btn_TrungBinhNgay.Size = new System.Drawing.Size(90, 90);
            this.btn_TrungBinhNgay.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_TrungBinhNgay.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_TrungBinhNgay.StateCommon.Back.ColorAngle = 75F;
            this.btn_TrungBinhNgay.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_TrungBinhNgay.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_TrungBinhNgay.StateCommon.Border.Rounding = 10;
            this.btn_TrungBinhNgay.StateCommon.Border.Width = 2;
            this.btn_TrungBinhNgay.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_TrungBinhNgay.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_TrungBinhNgay.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_TrungBinhNgay.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_TrungBinhNgay.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_TrungBinhNgay.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_TrungBinhNgay.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_TrungBinhNgay.TabIndex = 203;
            this.btn_TrungBinhNgay.Tag = "SUB_0452";
            this.btn_TrungBinhNgay.Values.Text = "Trung bình\r\n lương \r\nkhoán / giờ";
            // 
            // btn_Rpt1
            // 
            this.btn_Rpt1.Location = new System.Drawing.Point(11, 24);
            this.btn_Rpt1.Name = "btn_Rpt1";
            this.btn_Rpt1.Size = new System.Drawing.Size(90, 90);
            this.btn_Rpt1.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Rpt1.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Rpt1.StateCommon.Back.ColorAngle = 75F;
            this.btn_Rpt1.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Rpt1.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Rpt1.StateCommon.Border.Rounding = 10;
            this.btn_Rpt1.StateCommon.Border.Width = 2;
            this.btn_Rpt1.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Rpt1.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt1.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Rpt1.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Rpt1.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt1.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt1.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Rpt1.TabIndex = 203;
            this.btn_Rpt1.Tag = "SUB_0451";
            this.btn_Rpt1.Values.Text = "Lương \r\nkhoán\r\n theo ngày";
            // 
            // Panel_Production
            // 
            this.Panel_Production.Controls.Add(this.btn_Import_Production_Time);
            this.Panel_Production.Controls.Add(this.btn_Import_Record);
            this.Panel_Production.Controls.Add(this.btn_Production_Note);
            this.Panel_Production.Controls.Add(this.btn_Production_Auto_Money);
            this.Panel_Production.Controls.Add(this.btn_Production_Auto_Calculator);
            this.Panel_Production.Controls.Add(this.btn_Production_ReCalculator2);
            this.Panel_Production.Controls.Add(this.btn_Production_ReCalculator);
            this.Panel_Production.Controls.Add(this.btn_Production_Time);
            this.Panel_Production.Controls.Add(this.btn_Compare);
            this.Panel_Production.Controls.Add(this.btn_OrderView);
            this.Panel_Production.Controls.Add(this.btn_Production_Calculator_V2);
            this.Panel_Production.Controls.Add(this.btn_Production_Calculator);
            this.Panel_Production.Controls.Add(this.btn_Production_Payroll);
            this.Panel_Production.Location = new System.Drawing.Point(2745, 1928);
            this.Panel_Production.Name = "Panel_Production";
            this.Panel_Production.Size = new System.Drawing.Size(669, 200);
            this.Panel_Production.TabIndex = 201;
            // 
            // btn_Import_Production_Time
            // 
            this.btn_Import_Production_Time.Location = new System.Drawing.Point(100, 104);
            this.btn_Import_Production_Time.Name = "btn_Import_Production_Time";
            this.btn_Import_Production_Time.Size = new System.Drawing.Size(90, 94);
            this.btn_Import_Production_Time.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Import_Production_Time.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Import_Production_Time.StateCommon.Back.ColorAngle = 75F;
            this.btn_Import_Production_Time.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Import_Production_Time.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Import_Production_Time.StateCommon.Border.Rounding = 10;
            this.btn_Import_Production_Time.StateCommon.Border.Width = 2;
            this.btn_Import_Production_Time.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Import_Production_Time.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Import_Production_Time.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Import_Production_Time.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Import_Production_Time.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Import_Production_Time.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Import_Production_Time.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.btn_Import_Production_Time.StateTracking.Content.ShortText.Color1 = System.Drawing.Color.DarkGreen;
            this.btn_Import_Production_Time.StateTracking.Content.ShortText.Color2 = System.Drawing.Color.White;
            this.btn_Import_Production_Time.StateTracking.Content.ShortText.ColorAngle = 35F;
            this.btn_Import_Production_Time.TabIndex = 204;
            this.btn_Import_Production_Time.Tag = "SUB_0108";
            this.btn_Import_Production_Time.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Import_Production_Time.Values.Image")));
            this.btn_Import_Production_Time.Values.Text = "2.Import \r\ngiờ dư";
            // 
            // btn_Import_Record
            // 
            this.btn_Import_Record.Location = new System.Drawing.Point(8, 103);
            this.btn_Import_Record.Name = "btn_Import_Record";
            this.btn_Import_Record.Size = new System.Drawing.Size(90, 94);
            this.btn_Import_Record.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Import_Record.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Import_Record.StateCommon.Back.ColorAngle = 75F;
            this.btn_Import_Record.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Import_Record.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Import_Record.StateCommon.Border.Rounding = 10;
            this.btn_Import_Record.StateCommon.Border.Width = 2;
            this.btn_Import_Record.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Import_Record.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Import_Record.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Import_Record.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Import_Record.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Import_Record.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Import_Record.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.btn_Import_Record.StateTracking.Content.ShortText.Color1 = System.Drawing.Color.DarkGreen;
            this.btn_Import_Record.StateTracking.Content.ShortText.Color2 = System.Drawing.Color.White;
            this.btn_Import_Record.StateTracking.Content.ShortText.ColorAngle = 35F;
            this.btn_Import_Record.TabIndex = 203;
            this.btn_Import_Record.Tag = "SUB_0107";
            this.btn_Import_Record.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Import_Record.Values.Image")));
            this.btn_Import_Record.Values.Text = "1.Import \r\nExcel NS";
            // 
            // btn_Production_Note
            // 
            this.btn_Production_Note.Location = new System.Drawing.Point(8, 3);
            this.btn_Production_Note.Name = "btn_Production_Note";
            this.btn_Production_Note.Size = new System.Drawing.Size(90, 94);
            this.btn_Production_Note.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Production_Note.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Production_Note.StateCommon.Back.ColorAngle = 75F;
            this.btn_Production_Note.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Production_Note.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Production_Note.StateCommon.Border.Rounding = 10;
            this.btn_Production_Note.StateCommon.Border.Width = 2;
            this.btn_Production_Note.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Production_Note.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Production_Note.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Production_Note.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Production_Note.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Production_Note.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Production_Note.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.btn_Production_Note.StateTracking.Content.ShortText.Color1 = System.Drawing.Color.DarkGreen;
            this.btn_Production_Note.StateTracking.Content.ShortText.Color2 = System.Drawing.Color.White;
            this.btn_Production_Note.StateTracking.Content.ShortText.ColorAngle = 35F;
            this.btn_Production_Note.TabIndex = 203;
            this.btn_Production_Note.Tag = "SUB_0101";
            this.btn_Production_Note.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Production_Note.Values.Image")));
            this.btn_Production_Note.Values.Text = "1.Ghi \r\nnăng suất";
            // 
            // btn_Production_Auto_Money
            // 
            this.btn_Production_Auto_Money.Location = new System.Drawing.Point(566, 100);
            this.btn_Production_Auto_Money.Name = "btn_Production_Auto_Money";
            this.btn_Production_Auto_Money.Size = new System.Drawing.Size(90, 94);
            this.btn_Production_Auto_Money.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Production_Auto_Money.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Production_Auto_Money.StateCommon.Back.ColorAngle = 75F;
            this.btn_Production_Auto_Money.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Production_Auto_Money.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Production_Auto_Money.StateCommon.Border.Rounding = 10;
            this.btn_Production_Auto_Money.StateCommon.Border.Width = 2;
            this.btn_Production_Auto_Money.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Production_Auto_Money.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Production_Auto_Money.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Production_Auto_Money.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Production_Auto_Money.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Production_Auto_Money.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Production_Auto_Money.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Production_Auto_Money.TabIndex = 203;
            this.btn_Production_Auto_Money.Tag = "TNS_CL001";
            this.btn_Production_Auto_Money.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Production_Auto_Money.Values.Image")));
            this.btn_Production_Auto_Money.Values.Text = "5.Gộp lương\r\nkhoán";
            this.btn_Production_Auto_Money.Visible = false;
            // 
            // btn_Production_Auto_Calculator
            // 
            this.btn_Production_Auto_Calculator.Location = new System.Drawing.Point(100, 4);
            this.btn_Production_Auto_Calculator.Name = "btn_Production_Auto_Calculator";
            this.btn_Production_Auto_Calculator.Size = new System.Drawing.Size(90, 94);
            this.btn_Production_Auto_Calculator.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Production_Auto_Calculator.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Production_Auto_Calculator.StateCommon.Back.ColorAngle = 75F;
            this.btn_Production_Auto_Calculator.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Production_Auto_Calculator.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Production_Auto_Calculator.StateCommon.Border.Rounding = 10;
            this.btn_Production_Auto_Calculator.StateCommon.Border.Width = 2;
            this.btn_Production_Auto_Calculator.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Production_Auto_Calculator.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Production_Auto_Calculator.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Production_Auto_Calculator.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Production_Auto_Calculator.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Production_Auto_Calculator.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Production_Auto_Calculator.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Production_Auto_Calculator.TabIndex = 203;
            this.btn_Production_Auto_Calculator.Tag = "SUB_0102";
            this.btn_Production_Auto_Calculator.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Production_Auto_Calculator.Values.Image")));
            this.btn_Production_Auto_Calculator.Values.Text = "2.Tính\r\nnăng suất";
            // 
            // btn_Production_ReCalculator2
            // 
            this.btn_Production_ReCalculator2.Location = new System.Drawing.Point(378, 4);
            this.btn_Production_ReCalculator2.Name = "btn_Production_ReCalculator2";
            this.btn_Production_ReCalculator2.Size = new System.Drawing.Size(90, 94);
            this.btn_Production_ReCalculator2.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Production_ReCalculator2.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Production_ReCalculator2.StateCommon.Back.ColorAngle = 75F;
            this.btn_Production_ReCalculator2.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Production_ReCalculator2.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Production_ReCalculator2.StateCommon.Border.Rounding = 10;
            this.btn_Production_ReCalculator2.StateCommon.Border.Width = 2;
            this.btn_Production_ReCalculator2.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Production_ReCalculator2.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Production_ReCalculator2.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Production_ReCalculator2.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Production_ReCalculator2.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Production_ReCalculator2.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Production_ReCalculator2.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.btn_Production_ReCalculator2.TabIndex = 203;
            this.btn_Production_ReCalculator2.Tag = "SUB_0105";
            this.btn_Production_ReCalculator2.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Production_ReCalculator2.Values.Image")));
            this.btn_Production_ReCalculator2.Values.Text = "5. Chia Lại\r\nLK lần 2";
            // 
            // btn_Production_ReCalculator
            // 
            this.btn_Production_ReCalculator.Location = new System.Drawing.Point(285, 3);
            this.btn_Production_ReCalculator.Name = "btn_Production_ReCalculator";
            this.btn_Production_ReCalculator.Size = new System.Drawing.Size(90, 94);
            this.btn_Production_ReCalculator.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Production_ReCalculator.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Production_ReCalculator.StateCommon.Back.ColorAngle = 75F;
            this.btn_Production_ReCalculator.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Production_ReCalculator.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Production_ReCalculator.StateCommon.Border.Rounding = 10;
            this.btn_Production_ReCalculator.StateCommon.Border.Width = 2;
            this.btn_Production_ReCalculator.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Production_ReCalculator.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Production_ReCalculator.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Production_ReCalculator.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Production_ReCalculator.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Production_ReCalculator.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Production_ReCalculator.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.btn_Production_ReCalculator.TabIndex = 203;
            this.btn_Production_ReCalculator.Tag = "SUB_0104";
            this.btn_Production_ReCalculator.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Production_ReCalculator.Values.Image")));
            this.btn_Production_ReCalculator.Values.Text = "4.Chia Lại\r\nLK  lần 1";
            // 
            // btn_Production_Time
            // 
            this.btn_Production_Time.Location = new System.Drawing.Point(565, 3);
            this.btn_Production_Time.Name = "btn_Production_Time";
            this.btn_Production_Time.Size = new System.Drawing.Size(90, 94);
            this.btn_Production_Time.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Production_Time.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Production_Time.StateCommon.Back.ColorAngle = 75F;
            this.btn_Production_Time.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Production_Time.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Production_Time.StateCommon.Border.Rounding = 10;
            this.btn_Production_Time.StateCommon.Border.Width = 2;
            this.btn_Production_Time.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Production_Time.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Production_Time.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Production_Time.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Production_Time.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Production_Time.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Production_Time.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Production_Time.TabIndex = 203;
            this.btn_Production_Time.Tag = "SUB_0106";
            this.btn_Production_Time.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Production_Time.Values.Image")));
            this.btn_Production_Time.Values.Text = "7.Tính \r\ndư giờ";
            // 
            // btn_Compare
            // 
            this.btn_Compare.Location = new System.Drawing.Point(193, 103);
            this.btn_Compare.Name = "btn_Compare";
            this.btn_Compare.Size = new System.Drawing.Size(90, 94);
            this.btn_Compare.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Compare.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Compare.StateCommon.Back.ColorAngle = 75F;
            this.btn_Compare.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Compare.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Compare.StateCommon.Border.Rounding = 10;
            this.btn_Compare.StateCommon.Border.Width = 2;
            this.btn_Compare.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Compare.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Compare.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Compare.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Compare.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Compare.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Compare.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.btn_Compare.TabIndex = 203;
            this.btn_Compare.Tag = "SUB_0109";
            this.btn_Compare.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Compare.Values.Image")));
            this.btn_Compare.Values.Text = "Kiểm soát\r\nphân bổ";
            // 
            // btn_OrderView
            // 
            this.btn_OrderView.Location = new System.Drawing.Point(471, 3);
            this.btn_OrderView.Name = "btn_OrderView";
            this.btn_OrderView.Size = new System.Drawing.Size(90, 94);
            this.btn_OrderView.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_OrderView.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_OrderView.StateCommon.Back.ColorAngle = 75F;
            this.btn_OrderView.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_OrderView.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_OrderView.StateCommon.Border.Rounding = 10;
            this.btn_OrderView.StateCommon.Border.Width = 2;
            this.btn_OrderView.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_OrderView.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_OrderView.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_OrderView.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_OrderView.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_OrderView.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_OrderView.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.btn_OrderView.TabIndex = 203;
            this.btn_OrderView.Tag = "SUB_0110";
            this.btn_OrderView.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_OrderView.Values.Image")));
            this.btn_OrderView.Values.Text = "6.Kiểm soát\r\nđơn hàng";
            // 
            // btn_Production_Calculator_V2
            // 
            this.btn_Production_Calculator_V2.Location = new System.Drawing.Point(192, 3);
            this.btn_Production_Calculator_V2.Name = "btn_Production_Calculator_V2";
            this.btn_Production_Calculator_V2.Size = new System.Drawing.Size(90, 94);
            this.btn_Production_Calculator_V2.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Production_Calculator_V2.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Production_Calculator_V2.StateCommon.Back.ColorAngle = 75F;
            this.btn_Production_Calculator_V2.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Production_Calculator_V2.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Production_Calculator_V2.StateCommon.Border.Rounding = 10;
            this.btn_Production_Calculator_V2.StateCommon.Border.Width = 2;
            this.btn_Production_Calculator_V2.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Production_Calculator_V2.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Production_Calculator_V2.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Production_Calculator_V2.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Production_Calculator_V2.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Production_Calculator_V2.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Production_Calculator_V2.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.btn_Production_Calculator_V2.TabIndex = 203;
            this.btn_Production_Calculator_V2.Tag = "SUB_0103";
            this.btn_Production_Calculator_V2.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Production_Calculator_V2.Values.Image")));
            this.btn_Production_Calculator_V2.Values.Text = "3. Kiểm \r\nsoát NS";
            // 
            // btn_Production_Calculator
            // 
            this.btn_Production_Calculator.Location = new System.Drawing.Point(472, 100);
            this.btn_Production_Calculator.Name = "btn_Production_Calculator";
            this.btn_Production_Calculator.Size = new System.Drawing.Size(90, 90);
            this.btn_Production_Calculator.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Production_Calculator.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Production_Calculator.StateCommon.Back.ColorAngle = 75F;
            this.btn_Production_Calculator.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Production_Calculator.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Production_Calculator.StateCommon.Border.Rounding = 10;
            this.btn_Production_Calculator.StateCommon.Border.Width = 2;
            this.btn_Production_Calculator.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Production_Calculator.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Production_Calculator.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Production_Calculator.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Production_Calculator.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Production_Calculator.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Production_Calculator.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.btn_Production_Calculator.TabIndex = 203;
            this.btn_Production_Calculator.Tag = "TNS_SX001";
            this.btn_Production_Calculator.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Production_Calculator.Values.Image")));
            this.btn_Production_Calculator.Values.Text = "2.Tính \r\nnăng suất";
            this.btn_Production_Calculator.Visible = false;
            // 
            // btn_Production_Payroll
            // 
            this.btn_Production_Payroll.Location = new System.Drawing.Point(378, 101);
            this.btn_Production_Payroll.Name = "btn_Production_Payroll";
            this.btn_Production_Payroll.Size = new System.Drawing.Size(90, 90);
            this.btn_Production_Payroll.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Production_Payroll.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Production_Payroll.StateCommon.Back.ColorAngle = 75F;
            this.btn_Production_Payroll.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Production_Payroll.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Production_Payroll.StateCommon.Border.Rounding = 10;
            this.btn_Production_Payroll.StateCommon.Border.Width = 2;
            this.btn_Production_Payroll.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Production_Payroll.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Production_Payroll.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Production_Payroll.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Production_Payroll.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Production_Payroll.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Production_Payroll.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.btn_Production_Payroll.TabIndex = 203;
            this.btn_Production_Payroll.Tag = "TNST_SX001";
            this.btn_Production_Payroll.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Production_Payroll.Values.Image")));
            this.btn_Production_Payroll.Values.Text = "4.Lương\r\nnăng suất";
            this.btn_Production_Payroll.Visible = false;
            // 
            // Panel_Temp
            // 
            this.Panel_Temp.Controls.Add(this.btn_Temp_Output_Approve);
            this.Panel_Temp.Controls.Add(this.btn_Temp_Input);
            this.Panel_Temp.Controls.Add(this.btn_Temp_Input_Approve);
            this.Panel_Temp.Controls.Add(this.btn_Temp_Output);
            this.Panel_Temp.Location = new System.Drawing.Point(3636, 1928);
            this.Panel_Temp.Name = "Panel_Temp";
            this.Panel_Temp.Size = new System.Drawing.Size(290, 200);
            this.Panel_Temp.TabIndex = 198;
            // 
            // btn_Temp_Output_Approve
            // 
            this.btn_Temp_Output_Approve.Location = new System.Drawing.Point(100, 100);
            this.btn_Temp_Output_Approve.Name = "btn_Temp_Output_Approve";
            this.btn_Temp_Output_Approve.Size = new System.Drawing.Size(90, 90);
            this.btn_Temp_Output_Approve.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Temp_Output_Approve.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Temp_Output_Approve.StateCommon.Back.ColorAngle = 75F;
            this.btn_Temp_Output_Approve.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Temp_Output_Approve.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Temp_Output_Approve.StateCommon.Border.Rounding = 10;
            this.btn_Temp_Output_Approve.StateCommon.Border.Width = 2;
            this.btn_Temp_Output_Approve.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Temp_Output_Approve.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Temp_Output_Approve.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Temp_Output_Approve.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Temp_Output_Approve.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Temp_Output_Approve.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Temp_Output_Approve.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Temp_Output_Approve.TabIndex = 207;
            this.btn_Temp_Output_Approve.Tag = "SUB_0354";
            this.btn_Temp_Output_Approve.Values.Text = "Duyệt Phiếu\r\n Xuất - KT";
            // 
            // btn_Temp_Input
            // 
            this.btn_Temp_Input.Location = new System.Drawing.Point(8, 9);
            this.btn_Temp_Input.Name = "btn_Temp_Input";
            this.btn_Temp_Input.Size = new System.Drawing.Size(90, 90);
            this.btn_Temp_Input.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Temp_Input.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Temp_Input.StateCommon.Back.ColorAngle = 75F;
            this.btn_Temp_Input.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Temp_Input.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Temp_Input.StateCommon.Border.Rounding = 10;
            this.btn_Temp_Input.StateCommon.Border.Width = 2;
            this.btn_Temp_Input.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Temp_Input.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Temp_Input.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Temp_Input.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Temp_Input.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Temp_Input.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Temp_Input.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Temp_Input.TabIndex = 204;
            this.btn_Temp_Input.Tag = "SUB_0351";
            this.btn_Temp_Input.Values.Text = "Nhập Kho \r\nNội Bộ - KT\r\n";
            // 
            // btn_Temp_Input_Approve
            // 
            this.btn_Temp_Input_Approve.Location = new System.Drawing.Point(8, 101);
            this.btn_Temp_Input_Approve.Name = "btn_Temp_Input_Approve";
            this.btn_Temp_Input_Approve.Size = new System.Drawing.Size(90, 90);
            this.btn_Temp_Input_Approve.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Temp_Input_Approve.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Temp_Input_Approve.StateCommon.Back.ColorAngle = 75F;
            this.btn_Temp_Input_Approve.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Temp_Input_Approve.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Temp_Input_Approve.StateCommon.Border.Rounding = 10;
            this.btn_Temp_Input_Approve.StateCommon.Border.Width = 2;
            this.btn_Temp_Input_Approve.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Temp_Input_Approve.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Temp_Input_Approve.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Temp_Input_Approve.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Temp_Input_Approve.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Temp_Input_Approve.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Temp_Input_Approve.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Temp_Input_Approve.TabIndex = 206;
            this.btn_Temp_Input_Approve.Tag = "SUB_0352";
            this.btn_Temp_Input_Approve.Values.Text = "Duyệt Phiếu \r\nNhập - KT";
            // 
            // btn_Temp_Output
            // 
            this.btn_Temp_Output.Location = new System.Drawing.Point(100, 9);
            this.btn_Temp_Output.Name = "btn_Temp_Output";
            this.btn_Temp_Output.Size = new System.Drawing.Size(90, 90);
            this.btn_Temp_Output.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Temp_Output.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Temp_Output.StateCommon.Back.ColorAngle = 75F;
            this.btn_Temp_Output.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Temp_Output.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Temp_Output.StateCommon.Border.Rounding = 10;
            this.btn_Temp_Output.StateCommon.Border.Width = 2;
            this.btn_Temp_Output.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Temp_Output.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Temp_Output.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Temp_Output.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Temp_Output.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Temp_Output.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Temp_Output.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Temp_Output.TabIndex = 205;
            this.btn_Temp_Output.Tag = "SUB_0353";
            this.btn_Temp_Output.Values.Text = "Xuất Kho \r\nNội Bộ - KT";
            // 
            // Panel_Recycle
            // 
            this.Panel_Recycle.Controls.Add(this.btn_Recycle_Input_Approve);
            this.Panel_Recycle.Controls.Add(this.btn_Recycle_Input_Customer);
            this.Panel_Recycle.Controls.Add(this.btn_Recycle_Ouput_Approve);
            this.Panel_Recycle.Controls.Add(this.btn_Recycle_Output);
            this.Panel_Recycle.Controls.Add(this.btn_Recycle_Input_Approve_Customer);
            this.Panel_Recycle.Controls.Add(this.btn_Recycle_Input);
            this.Panel_Recycle.Location = new System.Drawing.Point(3635, 1722);
            this.Panel_Recycle.Name = "Panel_Recycle";
            this.Panel_Recycle.Size = new System.Drawing.Size(290, 200);
            this.Panel_Recycle.TabIndex = 197;
            // 
            // btn_Recycle_Input_Approve
            // 
            this.btn_Recycle_Input_Approve.Location = new System.Drawing.Point(192, 101);
            this.btn_Recycle_Input_Approve.Name = "btn_Recycle_Input_Approve";
            this.btn_Recycle_Input_Approve.Size = new System.Drawing.Size(90, 90);
            this.btn_Recycle_Input_Approve.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Recycle_Input_Approve.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Recycle_Input_Approve.StateCommon.Back.ColorAngle = 75F;
            this.btn_Recycle_Input_Approve.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Recycle_Input_Approve.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Recycle_Input_Approve.StateCommon.Border.Rounding = 10;
            this.btn_Recycle_Input_Approve.StateCommon.Border.Width = 2;
            this.btn_Recycle_Input_Approve.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Recycle_Input_Approve.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Recycle_Input_Approve.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Recycle_Input_Approve.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Recycle_Input_Approve.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Recycle_Input_Approve.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Recycle_Input_Approve.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Recycle_Input_Approve.TabIndex = 209;
            this.btn_Recycle_Input_Approve.Tag = "SUB_0404";
            this.btn_Recycle_Input_Approve.Values.Text = "Duyệt Nhập\r\nNội Bộ- KH";
            // 
            // btn_Recycle_Input_Customer
            // 
            this.btn_Recycle_Input_Customer.Location = new System.Drawing.Point(8, 9);
            this.btn_Recycle_Input_Customer.Name = "btn_Recycle_Input_Customer";
            this.btn_Recycle_Input_Customer.Size = new System.Drawing.Size(90, 90);
            this.btn_Recycle_Input_Customer.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Recycle_Input_Customer.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Recycle_Input_Customer.StateCommon.Back.ColorAngle = 75F;
            this.btn_Recycle_Input_Customer.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Recycle_Input_Customer.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Recycle_Input_Customer.StateCommon.Border.Rounding = 10;
            this.btn_Recycle_Input_Customer.StateCommon.Border.Width = 2;
            this.btn_Recycle_Input_Customer.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Recycle_Input_Customer.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Recycle_Input_Customer.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Recycle_Input_Customer.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Recycle_Input_Customer.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Recycle_Input_Customer.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Recycle_Input_Customer.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Recycle_Input_Customer.TabIndex = 204;
            this.btn_Recycle_Input_Customer.Tag = "SUB_0401";
            this.btn_Recycle_Input_Customer.Values.Text = "Nhập Kho\r\nKhách Hàng\r\n- KH";
            // 
            // btn_Recycle_Ouput_Approve
            // 
            this.btn_Recycle_Ouput_Approve.Location = new System.Drawing.Point(100, 101);
            this.btn_Recycle_Ouput_Approve.Name = "btn_Recycle_Ouput_Approve";
            this.btn_Recycle_Ouput_Approve.Size = new System.Drawing.Size(90, 90);
            this.btn_Recycle_Ouput_Approve.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Recycle_Ouput_Approve.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Recycle_Ouput_Approve.StateCommon.Back.ColorAngle = 75F;
            this.btn_Recycle_Ouput_Approve.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Recycle_Ouput_Approve.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Recycle_Ouput_Approve.StateCommon.Border.Rounding = 10;
            this.btn_Recycle_Ouput_Approve.StateCommon.Border.Width = 2;
            this.btn_Recycle_Ouput_Approve.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Recycle_Ouput_Approve.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Recycle_Ouput_Approve.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Recycle_Ouput_Approve.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Recycle_Ouput_Approve.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Recycle_Ouput_Approve.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Recycle_Ouput_Approve.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Recycle_Ouput_Approve.TabIndex = 208;
            this.btn_Recycle_Ouput_Approve.Tag = "SUB_0406";
            this.btn_Recycle_Ouput_Approve.Values.Text = "Duyệt Phiếu\r\nXuất - KH";
            // 
            // btn_Recycle_Output
            // 
            this.btn_Recycle_Output.Location = new System.Drawing.Point(100, 9);
            this.btn_Recycle_Output.Name = "btn_Recycle_Output";
            this.btn_Recycle_Output.Size = new System.Drawing.Size(90, 90);
            this.btn_Recycle_Output.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Recycle_Output.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Recycle_Output.StateCommon.Back.ColorAngle = 75F;
            this.btn_Recycle_Output.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Recycle_Output.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Recycle_Output.StateCommon.Border.Rounding = 10;
            this.btn_Recycle_Output.StateCommon.Border.Width = 2;
            this.btn_Recycle_Output.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Recycle_Output.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Recycle_Output.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Recycle_Output.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Recycle_Output.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Recycle_Output.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Recycle_Output.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Recycle_Output.TabIndex = 205;
            this.btn_Recycle_Output.Tag = "SUB_0405";
            this.btn_Recycle_Output.Values.Text = "Xuất Kho \r\nNội Bộ - KH";
            // 
            // btn_Recycle_Input_Approve_Customer
            // 
            this.btn_Recycle_Input_Approve_Customer.Location = new System.Drawing.Point(8, 101);
            this.btn_Recycle_Input_Approve_Customer.Name = "btn_Recycle_Input_Approve_Customer";
            this.btn_Recycle_Input_Approve_Customer.Size = new System.Drawing.Size(90, 90);
            this.btn_Recycle_Input_Approve_Customer.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Recycle_Input_Approve_Customer.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Recycle_Input_Approve_Customer.StateCommon.Back.ColorAngle = 75F;
            this.btn_Recycle_Input_Approve_Customer.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Recycle_Input_Approve_Customer.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Recycle_Input_Approve_Customer.StateCommon.Border.Rounding = 10;
            this.btn_Recycle_Input_Approve_Customer.StateCommon.Border.Width = 2;
            this.btn_Recycle_Input_Approve_Customer.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Recycle_Input_Approve_Customer.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Recycle_Input_Approve_Customer.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Recycle_Input_Approve_Customer.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Recycle_Input_Approve_Customer.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Recycle_Input_Approve_Customer.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Recycle_Input_Approve_Customer.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Recycle_Input_Approve_Customer.TabIndex = 207;
            this.btn_Recycle_Input_Approve_Customer.Tag = "SUB_0402";
            this.btn_Recycle_Input_Approve_Customer.Values.Text = "Duyệt Phiếu\r\nNhập - KH";
            // 
            // btn_Recycle_Input
            // 
            this.btn_Recycle_Input.Location = new System.Drawing.Point(192, 9);
            this.btn_Recycle_Input.Name = "btn_Recycle_Input";
            this.btn_Recycle_Input.Size = new System.Drawing.Size(90, 90);
            this.btn_Recycle_Input.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Recycle_Input.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Recycle_Input.StateCommon.Back.ColorAngle = 75F;
            this.btn_Recycle_Input.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Recycle_Input.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Recycle_Input.StateCommon.Border.Rounding = 10;
            this.btn_Recycle_Input.StateCommon.Border.Width = 2;
            this.btn_Recycle_Input.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Recycle_Input.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Recycle_Input.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Recycle_Input.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Recycle_Input.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Recycle_Input.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Recycle_Input.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Recycle_Input.TabIndex = 206;
            this.btn_Recycle_Input.Tag = "SUB_0403";
            this.btn_Recycle_Input.Values.Text = "Nhập Kho \r\nNội Bộ - KH";
            // 
            // Panel_Box
            // 
            this.Panel_Box.Controls.Add(this.btn_Box_Output_Approve_Customer);
            this.Panel_Box.Controls.Add(this.btn_Box_Input);
            this.Panel_Box.Controls.Add(this.btn_Box_Output_Approve);
            this.Panel_Box.Controls.Add(this.btn_Box_Input_Approve);
            this.Panel_Box.Controls.Add(this.btn_Box_Output_Customer);
            this.Panel_Box.Controls.Add(this.btn_Box_Output);
            this.Panel_Box.Location = new System.Drawing.Point(3339, 1722);
            this.Panel_Box.Name = "Panel_Box";
            this.Panel_Box.Size = new System.Drawing.Size(290, 200);
            this.Panel_Box.TabIndex = 196;
            // 
            // btn_Box_Output_Approve_Customer
            // 
            this.btn_Box_Output_Approve_Customer.Location = new System.Drawing.Point(192, 101);
            this.btn_Box_Output_Approve_Customer.Name = "btn_Box_Output_Approve_Customer";
            this.btn_Box_Output_Approve_Customer.Size = new System.Drawing.Size(90, 90);
            this.btn_Box_Output_Approve_Customer.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Box_Output_Approve_Customer.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Box_Output_Approve_Customer.StateCommon.Back.ColorAngle = 75F;
            this.btn_Box_Output_Approve_Customer.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Box_Output_Approve_Customer.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Box_Output_Approve_Customer.StateCommon.Border.Rounding = 10;
            this.btn_Box_Output_Approve_Customer.StateCommon.Border.Width = 2;
            this.btn_Box_Output_Approve_Customer.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Box_Output_Approve_Customer.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Box_Output_Approve_Customer.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Box_Output_Approve_Customer.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Box_Output_Approve_Customer.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Box_Output_Approve_Customer.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Box_Output_Approve_Customer.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Box_Output_Approve_Customer.TabIndex = 209;
            this.btn_Box_Output_Approve_Customer.Tag = "SUB_0256";
            this.btn_Box_Output_Approve_Customer.Values.Text = "Duyệt Xuất\r\nKhách hàng\r\n - ĐH\r\n";
            // 
            // btn_Box_Input
            // 
            this.btn_Box_Input.Location = new System.Drawing.Point(8, 9);
            this.btn_Box_Input.Name = "btn_Box_Input";
            this.btn_Box_Input.Size = new System.Drawing.Size(90, 90);
            this.btn_Box_Input.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Box_Input.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Box_Input.StateCommon.Back.ColorAngle = 75F;
            this.btn_Box_Input.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Box_Input.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Box_Input.StateCommon.Border.Rounding = 10;
            this.btn_Box_Input.StateCommon.Border.Width = 2;
            this.btn_Box_Input.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Box_Input.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Box_Input.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Box_Input.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Box_Input.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Box_Input.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Box_Input.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Box_Input.TabIndex = 204;
            this.btn_Box_Input.Tag = "SUB_0251";
            this.btn_Box_Input.Values.Text = "Nhập Kho \r\nNội Bộ - ĐH";
            // 
            // btn_Box_Output_Approve
            // 
            this.btn_Box_Output_Approve.Location = new System.Drawing.Point(100, 101);
            this.btn_Box_Output_Approve.Name = "btn_Box_Output_Approve";
            this.btn_Box_Output_Approve.Size = new System.Drawing.Size(90, 90);
            this.btn_Box_Output_Approve.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Box_Output_Approve.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Box_Output_Approve.StateCommon.Back.ColorAngle = 75F;
            this.btn_Box_Output_Approve.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Box_Output_Approve.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Box_Output_Approve.StateCommon.Border.Rounding = 10;
            this.btn_Box_Output_Approve.StateCommon.Border.Width = 2;
            this.btn_Box_Output_Approve.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Box_Output_Approve.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Box_Output_Approve.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Box_Output_Approve.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Box_Output_Approve.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Box_Output_Approve.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Box_Output_Approve.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Box_Output_Approve.TabIndex = 208;
            this.btn_Box_Output_Approve.Tag = "SUB_0254";
            this.btn_Box_Output_Approve.Values.Text = "Duyệt Phiếu\r\n Xuất - ĐH";
            // 
            // btn_Box_Input_Approve
            // 
            this.btn_Box_Input_Approve.Location = new System.Drawing.Point(8, 101);
            this.btn_Box_Input_Approve.Name = "btn_Box_Input_Approve";
            this.btn_Box_Input_Approve.Size = new System.Drawing.Size(90, 90);
            this.btn_Box_Input_Approve.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Box_Input_Approve.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Box_Input_Approve.StateCommon.Back.ColorAngle = 75F;
            this.btn_Box_Input_Approve.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Box_Input_Approve.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Box_Input_Approve.StateCommon.Border.Rounding = 10;
            this.btn_Box_Input_Approve.StateCommon.Border.Width = 2;
            this.btn_Box_Input_Approve.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Box_Input_Approve.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Box_Input_Approve.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Box_Input_Approve.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Box_Input_Approve.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Box_Input_Approve.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Box_Input_Approve.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Box_Input_Approve.TabIndex = 205;
            this.btn_Box_Input_Approve.Tag = "SUB_0252";
            this.btn_Box_Input_Approve.Values.Text = "Duyệt Phiếu \r\nNhập - ĐH";
            // 
            // btn_Box_Output_Customer
            // 
            this.btn_Box_Output_Customer.Location = new System.Drawing.Point(192, 9);
            this.btn_Box_Output_Customer.Name = "btn_Box_Output_Customer";
            this.btn_Box_Output_Customer.Size = new System.Drawing.Size(90, 90);
            this.btn_Box_Output_Customer.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Box_Output_Customer.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Box_Output_Customer.StateCommon.Back.ColorAngle = 75F;
            this.btn_Box_Output_Customer.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Box_Output_Customer.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Box_Output_Customer.StateCommon.Border.Rounding = 10;
            this.btn_Box_Output_Customer.StateCommon.Border.Width = 2;
            this.btn_Box_Output_Customer.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Box_Output_Customer.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Box_Output_Customer.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Box_Output_Customer.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Box_Output_Customer.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Box_Output_Customer.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Box_Output_Customer.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Box_Output_Customer.TabIndex = 207;
            this.btn_Box_Output_Customer.Tag = "SUB_0255";
            this.btn_Box_Output_Customer.Values.Text = "Xuất Cho \r\nKhách hàng\r\n- ĐH";
            // 
            // btn_Box_Output
            // 
            this.btn_Box_Output.Location = new System.Drawing.Point(100, 9);
            this.btn_Box_Output.Name = "btn_Box_Output";
            this.btn_Box_Output.Size = new System.Drawing.Size(90, 90);
            this.btn_Box_Output.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Box_Output.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Box_Output.StateCommon.Back.ColorAngle = 75F;
            this.btn_Box_Output.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Box_Output.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Box_Output.StateCommon.Border.Rounding = 10;
            this.btn_Box_Output.StateCommon.Border.Width = 2;
            this.btn_Box_Output.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Box_Output.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Box_Output.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Box_Output.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Box_Output.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Box_Output.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Box_Output.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Box_Output.TabIndex = 206;
            this.btn_Box_Output.Tag = "SUB_0253";
            this.btn_Box_Output.Values.Text = "Xuất Kho \r\nNội Bộ - ĐH";
            // 
            // Panel_VuonUom
            // 
            this.Panel_VuonUom.Controls.Add(this.btn_VuonUom_Approve_Customer);
            this.Panel_VuonUom.Controls.Add(this.btn_VuonUom_Input);
            this.Panel_VuonUom.Controls.Add(this.btn_VuonUom_Output_Approve);
            this.Panel_VuonUom.Controls.Add(this.btn_VuonUom_Output);
            this.Panel_VuonUom.Controls.Add(this.btn_VuonUom_Input_Approve);
            this.Panel_VuonUom.Controls.Add(this.btn_VuonUom_Customer);
            this.Panel_VuonUom.Location = new System.Drawing.Point(3636, 1505);
            this.Panel_VuonUom.Name = "Panel_VuonUom";
            this.Panel_VuonUom.Size = new System.Drawing.Size(290, 200);
            this.Panel_VuonUom.TabIndex = 195;
            // 
            // btn_VuonUom_Approve_Customer
            // 
            this.btn_VuonUom_Approve_Customer.Location = new System.Drawing.Point(192, 101);
            this.btn_VuonUom_Approve_Customer.Name = "btn_VuonUom_Approve_Customer";
            this.btn_VuonUom_Approve_Customer.Size = new System.Drawing.Size(90, 90);
            this.btn_VuonUom_Approve_Customer.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_VuonUom_Approve_Customer.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_VuonUom_Approve_Customer.StateCommon.Back.ColorAngle = 75F;
            this.btn_VuonUom_Approve_Customer.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_VuonUom_Approve_Customer.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_VuonUom_Approve_Customer.StateCommon.Border.Rounding = 10;
            this.btn_VuonUom_Approve_Customer.StateCommon.Border.Width = 2;
            this.btn_VuonUom_Approve_Customer.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_VuonUom_Approve_Customer.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_VuonUom_Approve_Customer.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_VuonUom_Approve_Customer.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_VuonUom_Approve_Customer.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_VuonUom_Approve_Customer.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_VuonUom_Approve_Customer.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_VuonUom_Approve_Customer.TabIndex = 209;
            this.btn_VuonUom_Approve_Customer.Tag = "SUB_0656";
            this.btn_VuonUom_Approve_Customer.Values.Text = "Duyệt Xuất \r\nK.Hàng\r\n- Vườn ươm";
            // 
            // btn_VuonUom_Input
            // 
            this.btn_VuonUom_Input.Location = new System.Drawing.Point(8, 9);
            this.btn_VuonUom_Input.Name = "btn_VuonUom_Input";
            this.btn_VuonUom_Input.Size = new System.Drawing.Size(90, 90);
            this.btn_VuonUom_Input.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_VuonUom_Input.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_VuonUom_Input.StateCommon.Back.ColorAngle = 75F;
            this.btn_VuonUom_Input.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_VuonUom_Input.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_VuonUom_Input.StateCommon.Border.Rounding = 10;
            this.btn_VuonUom_Input.StateCommon.Border.Width = 2;
            this.btn_VuonUom_Input.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_VuonUom_Input.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_VuonUom_Input.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_VuonUom_Input.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_VuonUom_Input.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_VuonUom_Input.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_VuonUom_Input.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_VuonUom_Input.TabIndex = 204;
            this.btn_VuonUom_Input.Tag = "SUB_0651";
            this.btn_VuonUom_Input.Values.Text = "Nhập kho\r\nNội bộ\r\n- Vườn ươm";
            // 
            // btn_VuonUom_Output_Approve
            // 
            this.btn_VuonUom_Output_Approve.Location = new System.Drawing.Point(100, 101);
            this.btn_VuonUom_Output_Approve.Name = "btn_VuonUom_Output_Approve";
            this.btn_VuonUom_Output_Approve.Size = new System.Drawing.Size(90, 90);
            this.btn_VuonUom_Output_Approve.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_VuonUom_Output_Approve.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_VuonUom_Output_Approve.StateCommon.Back.ColorAngle = 75F;
            this.btn_VuonUom_Output_Approve.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_VuonUom_Output_Approve.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_VuonUom_Output_Approve.StateCommon.Border.Rounding = 10;
            this.btn_VuonUom_Output_Approve.StateCommon.Border.Width = 2;
            this.btn_VuonUom_Output_Approve.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_VuonUom_Output_Approve.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_VuonUom_Output_Approve.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_VuonUom_Output_Approve.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_VuonUom_Output_Approve.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_VuonUom_Output_Approve.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_VuonUom_Output_Approve.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_VuonUom_Output_Approve.TabIndex = 208;
            this.btn_VuonUom_Output_Approve.Tag = "SUB_0654";
            this.btn_VuonUom_Output_Approve.Values.Text = "Duyệt Phiếu \r\nXuất \r\n- Vườn ươm";
            // 
            // btn_VuonUom_Output
            // 
            this.btn_VuonUom_Output.Location = new System.Drawing.Point(100, 9);
            this.btn_VuonUom_Output.Name = "btn_VuonUom_Output";
            this.btn_VuonUom_Output.Size = new System.Drawing.Size(90, 90);
            this.btn_VuonUom_Output.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_VuonUom_Output.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_VuonUom_Output.StateCommon.Back.ColorAngle = 75F;
            this.btn_VuonUom_Output.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_VuonUom_Output.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_VuonUom_Output.StateCommon.Border.Rounding = 10;
            this.btn_VuonUom_Output.StateCommon.Border.Width = 2;
            this.btn_VuonUom_Output.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_VuonUom_Output.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_VuonUom_Output.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_VuonUom_Output.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_VuonUom_Output.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_VuonUom_Output.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_VuonUom_Output.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_VuonUom_Output.TabIndex = 205;
            this.btn_VuonUom_Output.Tag = "SUB_0653";
            this.btn_VuonUom_Output.Values.Text = "Xuất kho\r\nNội bộ\r\n- Vườn ươm";
            // 
            // btn_VuonUom_Input_Approve
            // 
            this.btn_VuonUom_Input_Approve.Location = new System.Drawing.Point(8, 101);
            this.btn_VuonUom_Input_Approve.Name = "btn_VuonUom_Input_Approve";
            this.btn_VuonUom_Input_Approve.Size = new System.Drawing.Size(90, 90);
            this.btn_VuonUom_Input_Approve.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_VuonUom_Input_Approve.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_VuonUom_Input_Approve.StateCommon.Back.ColorAngle = 75F;
            this.btn_VuonUom_Input_Approve.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_VuonUom_Input_Approve.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_VuonUom_Input_Approve.StateCommon.Border.Rounding = 10;
            this.btn_VuonUom_Input_Approve.StateCommon.Border.Width = 2;
            this.btn_VuonUom_Input_Approve.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_VuonUom_Input_Approve.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_VuonUom_Input_Approve.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_VuonUom_Input_Approve.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_VuonUom_Input_Approve.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_VuonUom_Input_Approve.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_VuonUom_Input_Approve.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_VuonUom_Input_Approve.TabIndex = 207;
            this.btn_VuonUom_Input_Approve.Tag = "SUB_0652";
            this.btn_VuonUom_Input_Approve.Values.Text = "Duyệt Phiếu \r\nNhập\r\n- Vườn ươm";
            // 
            // btn_VuonUom_Customer
            // 
            this.btn_VuonUom_Customer.Location = new System.Drawing.Point(192, 9);
            this.btn_VuonUom_Customer.Name = "btn_VuonUom_Customer";
            this.btn_VuonUom_Customer.Size = new System.Drawing.Size(90, 90);
            this.btn_VuonUom_Customer.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_VuonUom_Customer.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_VuonUom_Customer.StateCommon.Back.ColorAngle = 75F;
            this.btn_VuonUom_Customer.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_VuonUom_Customer.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_VuonUom_Customer.StateCommon.Border.Rounding = 10;
            this.btn_VuonUom_Customer.StateCommon.Border.Width = 2;
            this.btn_VuonUom_Customer.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_VuonUom_Customer.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_VuonUom_Customer.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_VuonUom_Customer.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_VuonUom_Customer.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_VuonUom_Customer.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_VuonUom_Customer.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_VuonUom_Customer.TabIndex = 206;
            this.btn_VuonUom_Customer.Tag = "SUB_0655";
            this.btn_VuonUom_Customer.Values.Text = "Xuất Cho \r\nKhách Hàng\r\n- Vườn ươm";
            // 
            // Panel_ThachDua
            // 
            this.Panel_ThachDua.Controls.Add(this.btn_ThachDua_Approve_Customer);
            this.Panel_ThachDua.Controls.Add(this.btn_ThachDua_Input);
            this.Panel_ThachDua.Controls.Add(this.btn_ThachDua_Output_Approve);
            this.Panel_ThachDua.Controls.Add(this.btn_ThachDua_Output);
            this.Panel_ThachDua.Controls.Add(this.btn_ThachDua_Input_Approve);
            this.Panel_ThachDua.Controls.Add(this.btn_ThachDua_Customer);
            this.Panel_ThachDua.Location = new System.Drawing.Point(3339, 1505);
            this.Panel_ThachDua.Name = "Panel_ThachDua";
            this.Panel_ThachDua.Size = new System.Drawing.Size(290, 200);
            this.Panel_ThachDua.TabIndex = 195;
            // 
            // btn_ThachDua_Approve_Customer
            // 
            this.btn_ThachDua_Approve_Customer.Location = new System.Drawing.Point(192, 101);
            this.btn_ThachDua_Approve_Customer.Name = "btn_ThachDua_Approve_Customer";
            this.btn_ThachDua_Approve_Customer.Size = new System.Drawing.Size(90, 90);
            this.btn_ThachDua_Approve_Customer.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_ThachDua_Approve_Customer.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_ThachDua_Approve_Customer.StateCommon.Back.ColorAngle = 75F;
            this.btn_ThachDua_Approve_Customer.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_ThachDua_Approve_Customer.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_ThachDua_Approve_Customer.StateCommon.Border.Rounding = 10;
            this.btn_ThachDua_Approve_Customer.StateCommon.Border.Width = 2;
            this.btn_ThachDua_Approve_Customer.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_ThachDua_Approve_Customer.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ThachDua_Approve_Customer.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_ThachDua_Approve_Customer.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_ThachDua_Approve_Customer.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ThachDua_Approve_Customer.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ThachDua_Approve_Customer.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ThachDua_Approve_Customer.TabIndex = 209;
            this.btn_ThachDua_Approve_Customer.Tag = "SUB_0606";
            this.btn_ThachDua_Approve_Customer.Values.Text = "Duyệt Xuất \r\nK.Hàng- TD";
            // 
            // btn_ThachDua_Input
            // 
            this.btn_ThachDua_Input.Location = new System.Drawing.Point(8, 9);
            this.btn_ThachDua_Input.Name = "btn_ThachDua_Input";
            this.btn_ThachDua_Input.Size = new System.Drawing.Size(90, 90);
            this.btn_ThachDua_Input.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_ThachDua_Input.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_ThachDua_Input.StateCommon.Back.ColorAngle = 75F;
            this.btn_ThachDua_Input.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_ThachDua_Input.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_ThachDua_Input.StateCommon.Border.Rounding = 10;
            this.btn_ThachDua_Input.StateCommon.Border.Width = 2;
            this.btn_ThachDua_Input.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_ThachDua_Input.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ThachDua_Input.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_ThachDua_Input.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_ThachDua_Input.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ThachDua_Input.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ThachDua_Input.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ThachDua_Input.TabIndex = 204;
            this.btn_ThachDua_Input.Tag = "SUB_0601";
            this.btn_ThachDua_Input.Values.Text = "Nhập kho\r\nNội bộ-TD";
            // 
            // btn_ThachDua_Output_Approve
            // 
            this.btn_ThachDua_Output_Approve.Location = new System.Drawing.Point(100, 101);
            this.btn_ThachDua_Output_Approve.Name = "btn_ThachDua_Output_Approve";
            this.btn_ThachDua_Output_Approve.Size = new System.Drawing.Size(90, 90);
            this.btn_ThachDua_Output_Approve.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_ThachDua_Output_Approve.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_ThachDua_Output_Approve.StateCommon.Back.ColorAngle = 75F;
            this.btn_ThachDua_Output_Approve.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_ThachDua_Output_Approve.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_ThachDua_Output_Approve.StateCommon.Border.Rounding = 10;
            this.btn_ThachDua_Output_Approve.StateCommon.Border.Width = 2;
            this.btn_ThachDua_Output_Approve.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_ThachDua_Output_Approve.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ThachDua_Output_Approve.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_ThachDua_Output_Approve.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_ThachDua_Output_Approve.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ThachDua_Output_Approve.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ThachDua_Output_Approve.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ThachDua_Output_Approve.TabIndex = 208;
            this.btn_ThachDua_Output_Approve.Tag = "SUB_0604";
            this.btn_ThachDua_Output_Approve.Values.Text = "Duyệt Phiếu \r\nXuất - TD";
            // 
            // btn_ThachDua_Output
            // 
            this.btn_ThachDua_Output.Location = new System.Drawing.Point(100, 9);
            this.btn_ThachDua_Output.Name = "btn_ThachDua_Output";
            this.btn_ThachDua_Output.Size = new System.Drawing.Size(90, 90);
            this.btn_ThachDua_Output.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_ThachDua_Output.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_ThachDua_Output.StateCommon.Back.ColorAngle = 75F;
            this.btn_ThachDua_Output.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_ThachDua_Output.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_ThachDua_Output.StateCommon.Border.Rounding = 10;
            this.btn_ThachDua_Output.StateCommon.Border.Width = 2;
            this.btn_ThachDua_Output.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_ThachDua_Output.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ThachDua_Output.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_ThachDua_Output.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_ThachDua_Output.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ThachDua_Output.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ThachDua_Output.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ThachDua_Output.TabIndex = 205;
            this.btn_ThachDua_Output.Tag = "SUB_0603";
            this.btn_ThachDua_Output.Values.Text = "Xuất kho\r\nNội bộ - TD";
            // 
            // btn_ThachDua_Input_Approve
            // 
            this.btn_ThachDua_Input_Approve.Location = new System.Drawing.Point(8, 101);
            this.btn_ThachDua_Input_Approve.Name = "btn_ThachDua_Input_Approve";
            this.btn_ThachDua_Input_Approve.Size = new System.Drawing.Size(90, 90);
            this.btn_ThachDua_Input_Approve.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_ThachDua_Input_Approve.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_ThachDua_Input_Approve.StateCommon.Back.ColorAngle = 75F;
            this.btn_ThachDua_Input_Approve.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_ThachDua_Input_Approve.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_ThachDua_Input_Approve.StateCommon.Border.Rounding = 10;
            this.btn_ThachDua_Input_Approve.StateCommon.Border.Width = 2;
            this.btn_ThachDua_Input_Approve.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_ThachDua_Input_Approve.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ThachDua_Input_Approve.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_ThachDua_Input_Approve.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_ThachDua_Input_Approve.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ThachDua_Input_Approve.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ThachDua_Input_Approve.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ThachDua_Input_Approve.TabIndex = 207;
            this.btn_ThachDua_Input_Approve.Tag = "SUB_0602";
            this.btn_ThachDua_Input_Approve.Values.Text = "Duyệt Phiếu \r\nNhập - TD";
            // 
            // btn_ThachDua_Customer
            // 
            this.btn_ThachDua_Customer.Location = new System.Drawing.Point(192, 9);
            this.btn_ThachDua_Customer.Name = "btn_ThachDua_Customer";
            this.btn_ThachDua_Customer.Size = new System.Drawing.Size(90, 90);
            this.btn_ThachDua_Customer.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_ThachDua_Customer.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_ThachDua_Customer.StateCommon.Back.ColorAngle = 75F;
            this.btn_ThachDua_Customer.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_ThachDua_Customer.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_ThachDua_Customer.StateCommon.Border.Rounding = 10;
            this.btn_ThachDua_Customer.StateCommon.Border.Width = 2;
            this.btn_ThachDua_Customer.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_ThachDua_Customer.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ThachDua_Customer.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_ThachDua_Customer.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_ThachDua_Customer.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ThachDua_Customer.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ThachDua_Customer.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ThachDua_Customer.TabIndex = 206;
            this.btn_ThachDua_Customer.Tag = "SUB_0605";
            this.btn_ThachDua_Customer.Values.Text = "Xuất Cho \r\nKhách Hàng\r\n- TD";
            // 
            // Panel_Freezing
            // 
            this.Panel_Freezing.Controls.Add(this.btn_Freez_Output_Approve_Customer);
            this.Panel_Freezing.Controls.Add(this.btn_Freez_Input);
            this.Panel_Freezing.Controls.Add(this.btn_Freez_Output_Approve);
            this.Panel_Freezing.Controls.Add(this.btn_Freez_Output);
            this.Panel_Freezing.Controls.Add(this.btn_Freez_Input_Approve);
            this.Panel_Freezing.Controls.Add(this.btn_Freez_Output_Customer);
            this.Panel_Freezing.Location = new System.Drawing.Point(3043, 1722);
            this.Panel_Freezing.Name = "Panel_Freezing";
            this.Panel_Freezing.Size = new System.Drawing.Size(290, 200);
            this.Panel_Freezing.TabIndex = 195;
            // 
            // btn_Freez_Output_Approve_Customer
            // 
            this.btn_Freez_Output_Approve_Customer.Location = new System.Drawing.Point(192, 101);
            this.btn_Freez_Output_Approve_Customer.Name = "btn_Freez_Output_Approve_Customer";
            this.btn_Freez_Output_Approve_Customer.Size = new System.Drawing.Size(90, 90);
            this.btn_Freez_Output_Approve_Customer.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Freez_Output_Approve_Customer.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Freez_Output_Approve_Customer.StateCommon.Back.ColorAngle = 75F;
            this.btn_Freez_Output_Approve_Customer.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Freez_Output_Approve_Customer.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Freez_Output_Approve_Customer.StateCommon.Border.Rounding = 10;
            this.btn_Freez_Output_Approve_Customer.StateCommon.Border.Width = 2;
            this.btn_Freez_Output_Approve_Customer.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Freez_Output_Approve_Customer.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Freez_Output_Approve_Customer.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Freez_Output_Approve_Customer.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Freez_Output_Approve_Customer.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Freez_Output_Approve_Customer.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Freez_Output_Approve_Customer.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Freez_Output_Approve_Customer.TabIndex = 209;
            this.btn_Freez_Output_Approve_Customer.Tag = "SUB_0306";
            this.btn_Freez_Output_Approve_Customer.Values.Text = "Duyệt Xuất \r\nK.Hàng- CĐ";
            // 
            // btn_Freez_Input
            // 
            this.btn_Freez_Input.Location = new System.Drawing.Point(8, 9);
            this.btn_Freez_Input.Name = "btn_Freez_Input";
            this.btn_Freez_Input.Size = new System.Drawing.Size(90, 90);
            this.btn_Freez_Input.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Freez_Input.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Freez_Input.StateCommon.Back.ColorAngle = 75F;
            this.btn_Freez_Input.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Freez_Input.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Freez_Input.StateCommon.Border.Rounding = 10;
            this.btn_Freez_Input.StateCommon.Border.Width = 2;
            this.btn_Freez_Input.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Freez_Input.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Freez_Input.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Freez_Input.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Freez_Input.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Freez_Input.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Freez_Input.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Freez_Input.TabIndex = 204;
            this.btn_Freez_Input.Tag = "SUB_0301";
            this.btn_Freez_Input.Values.Text = "Nhập kho\r\nNội bộ-CĐ";
            // 
            // btn_Freez_Output_Approve
            // 
            this.btn_Freez_Output_Approve.Location = new System.Drawing.Point(100, 101);
            this.btn_Freez_Output_Approve.Name = "btn_Freez_Output_Approve";
            this.btn_Freez_Output_Approve.Size = new System.Drawing.Size(90, 90);
            this.btn_Freez_Output_Approve.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Freez_Output_Approve.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Freez_Output_Approve.StateCommon.Back.ColorAngle = 75F;
            this.btn_Freez_Output_Approve.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Freez_Output_Approve.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Freez_Output_Approve.StateCommon.Border.Rounding = 10;
            this.btn_Freez_Output_Approve.StateCommon.Border.Width = 2;
            this.btn_Freez_Output_Approve.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Freez_Output_Approve.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Freez_Output_Approve.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Freez_Output_Approve.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Freez_Output_Approve.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Freez_Output_Approve.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Freez_Output_Approve.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Freez_Output_Approve.TabIndex = 208;
            this.btn_Freez_Output_Approve.Tag = "SUB_0304";
            this.btn_Freez_Output_Approve.Values.Text = "Duyệt Phiếu \r\nXuất - CĐ";
            // 
            // btn_Freez_Output
            // 
            this.btn_Freez_Output.Location = new System.Drawing.Point(100, 9);
            this.btn_Freez_Output.Name = "btn_Freez_Output";
            this.btn_Freez_Output.Size = new System.Drawing.Size(90, 90);
            this.btn_Freez_Output.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Freez_Output.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Freez_Output.StateCommon.Back.ColorAngle = 75F;
            this.btn_Freez_Output.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Freez_Output.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Freez_Output.StateCommon.Border.Rounding = 10;
            this.btn_Freez_Output.StateCommon.Border.Width = 2;
            this.btn_Freez_Output.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Freez_Output.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Freez_Output.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Freez_Output.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Freez_Output.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Freez_Output.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Freez_Output.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Freez_Output.TabIndex = 205;
            this.btn_Freez_Output.Tag = "SUB_0303";
            this.btn_Freez_Output.Values.Text = "Xuất kho\r\nNội bộ - CĐ";
            // 
            // btn_Freez_Input_Approve
            // 
            this.btn_Freez_Input_Approve.Location = new System.Drawing.Point(8, 101);
            this.btn_Freez_Input_Approve.Name = "btn_Freez_Input_Approve";
            this.btn_Freez_Input_Approve.Size = new System.Drawing.Size(90, 90);
            this.btn_Freez_Input_Approve.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Freez_Input_Approve.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Freez_Input_Approve.StateCommon.Back.ColorAngle = 75F;
            this.btn_Freez_Input_Approve.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Freez_Input_Approve.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Freez_Input_Approve.StateCommon.Border.Rounding = 10;
            this.btn_Freez_Input_Approve.StateCommon.Border.Width = 2;
            this.btn_Freez_Input_Approve.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Freez_Input_Approve.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Freez_Input_Approve.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Freez_Input_Approve.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Freez_Input_Approve.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Freez_Input_Approve.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Freez_Input_Approve.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Freez_Input_Approve.TabIndex = 207;
            this.btn_Freez_Input_Approve.Tag = "SUB_0302";
            this.btn_Freez_Input_Approve.Values.Text = "Duyệt Phiếu \r\nNhập - CĐ";
            // 
            // btn_Freez_Output_Customer
            // 
            this.btn_Freez_Output_Customer.Location = new System.Drawing.Point(192, 9);
            this.btn_Freez_Output_Customer.Name = "btn_Freez_Output_Customer";
            this.btn_Freez_Output_Customer.Size = new System.Drawing.Size(90, 90);
            this.btn_Freez_Output_Customer.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Freez_Output_Customer.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Freez_Output_Customer.StateCommon.Back.ColorAngle = 75F;
            this.btn_Freez_Output_Customer.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Freez_Output_Customer.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Freez_Output_Customer.StateCommon.Border.Rounding = 10;
            this.btn_Freez_Output_Customer.StateCommon.Border.Width = 2;
            this.btn_Freez_Output_Customer.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Freez_Output_Customer.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Freez_Output_Customer.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Freez_Output_Customer.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Freez_Output_Customer.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Freez_Output_Customer.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Freez_Output_Customer.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Freez_Output_Customer.TabIndex = 206;
            this.btn_Freez_Output_Customer.Tag = "SUB_0305";
            this.btn_Freez_Output_Customer.Values.Text = "Xuất Cho \r\nKhách Hàng\r\n- CĐ";
            // 
            // Panel_Material
            // 
            this.Panel_Material.Controls.Add(this.btn_Material_Input_Approve);
            this.Panel_Material.Controls.Add(this.btn_Material_Input_Customer);
            this.Panel_Material.Controls.Add(this.btn_Material_Output_Approve);
            this.Panel_Material.Controls.Add(this.btn_Material_Input_Customer_Approve);
            this.Panel_Material.Controls.Add(this.btn_Material_Input);
            this.Panel_Material.Controls.Add(this.btn_Material_Output);
            this.Panel_Material.Location = new System.Drawing.Point(2745, 1722);
            this.Panel_Material.Name = "Panel_Material";
            this.Panel_Material.Size = new System.Drawing.Size(290, 200);
            this.Panel_Material.TabIndex = 194;
            // 
            // btn_Material_Input_Approve
            // 
            this.btn_Material_Input_Approve.Location = new System.Drawing.Point(192, 101);
            this.btn_Material_Input_Approve.Name = "btn_Material_Input_Approve";
            this.btn_Material_Input_Approve.Size = new System.Drawing.Size(90, 90);
            this.btn_Material_Input_Approve.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Material_Input_Approve.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Material_Input_Approve.StateCommon.Back.ColorAngle = 75F;
            this.btn_Material_Input_Approve.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Material_Input_Approve.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Material_Input_Approve.StateCommon.Border.Rounding = 10;
            this.btn_Material_Input_Approve.StateCommon.Border.Width = 2;
            this.btn_Material_Input_Approve.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Material_Input_Approve.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Material_Input_Approve.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Material_Input_Approve.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Material_Input_Approve.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Material_Input_Approve.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Material_Input_Approve.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Material_Input_Approve.TabIndex = 209;
            this.btn_Material_Input_Approve.Tag = "SUB_0206";
            this.btn_Material_Input_Approve.Values.Text = "Duyệt Phiếu \r\nNhập - NL";
            // 
            // btn_Material_Input_Customer
            // 
            this.btn_Material_Input_Customer.Location = new System.Drawing.Point(8, 9);
            this.btn_Material_Input_Customer.Name = "btn_Material_Input_Customer";
            this.btn_Material_Input_Customer.Size = new System.Drawing.Size(90, 90);
            this.btn_Material_Input_Customer.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Material_Input_Customer.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Material_Input_Customer.StateCommon.Back.ColorAngle = 75F;
            this.btn_Material_Input_Customer.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Material_Input_Customer.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Material_Input_Customer.StateCommon.Border.Rounding = 10;
            this.btn_Material_Input_Customer.StateCommon.Border.Width = 2;
            this.btn_Material_Input_Customer.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Material_Input_Customer.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Material_Input_Customer.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Material_Input_Customer.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Material_Input_Customer.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Material_Input_Customer.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Material_Input_Customer.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Material_Input_Customer.TabIndex = 204;
            this.btn_Material_Input_Customer.Tag = "SUB_0201";
            this.btn_Material_Input_Customer.Values.Text = "Nhập kho \r\ntừ K.Hàng \r\n-NL";
            // 
            // btn_Material_Output_Approve
            // 
            this.btn_Material_Output_Approve.Location = new System.Drawing.Point(100, 101);
            this.btn_Material_Output_Approve.Name = "btn_Material_Output_Approve";
            this.btn_Material_Output_Approve.Size = new System.Drawing.Size(90, 90);
            this.btn_Material_Output_Approve.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Material_Output_Approve.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Material_Output_Approve.StateCommon.Back.ColorAngle = 75F;
            this.btn_Material_Output_Approve.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Material_Output_Approve.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Material_Output_Approve.StateCommon.Border.Rounding = 10;
            this.btn_Material_Output_Approve.StateCommon.Border.Width = 2;
            this.btn_Material_Output_Approve.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Material_Output_Approve.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Material_Output_Approve.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Material_Output_Approve.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Material_Output_Approve.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Material_Output_Approve.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Material_Output_Approve.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Material_Output_Approve.TabIndex = 208;
            this.btn_Material_Output_Approve.Tag = "SUB_0204";
            this.btn_Material_Output_Approve.Values.Text = "Duyệt Phiếu\r\n Xuất - NL";
            // 
            // btn_Material_Input_Customer_Approve
            // 
            this.btn_Material_Input_Customer_Approve.Location = new System.Drawing.Point(8, 101);
            this.btn_Material_Input_Customer_Approve.Name = "btn_Material_Input_Customer_Approve";
            this.btn_Material_Input_Customer_Approve.Size = new System.Drawing.Size(90, 90);
            this.btn_Material_Input_Customer_Approve.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Material_Input_Customer_Approve.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Material_Input_Customer_Approve.StateCommon.Back.ColorAngle = 75F;
            this.btn_Material_Input_Customer_Approve.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Material_Input_Customer_Approve.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Material_Input_Customer_Approve.StateCommon.Border.Rounding = 10;
            this.btn_Material_Input_Customer_Approve.StateCommon.Border.Width = 2;
            this.btn_Material_Input_Customer_Approve.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Material_Input_Customer_Approve.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Material_Input_Customer_Approve.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Material_Input_Customer_Approve.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Material_Input_Customer_Approve.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Material_Input_Customer_Approve.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Material_Input_Customer_Approve.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Material_Input_Customer_Approve.TabIndex = 205;
            this.btn_Material_Input_Customer_Approve.Tag = "SUB_0202";
            this.btn_Material_Input_Customer_Approve.Values.Text = "Duyệt Nhập \r\nKho K.Hàng\r\n-NL";
            // 
            // btn_Material_Input
            // 
            this.btn_Material_Input.Location = new System.Drawing.Point(192, 9);
            this.btn_Material_Input.Name = "btn_Material_Input";
            this.btn_Material_Input.Size = new System.Drawing.Size(90, 90);
            this.btn_Material_Input.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Material_Input.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Material_Input.StateCommon.Back.ColorAngle = 75F;
            this.btn_Material_Input.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Material_Input.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Material_Input.StateCommon.Border.Rounding = 10;
            this.btn_Material_Input.StateCommon.Border.Width = 2;
            this.btn_Material_Input.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Material_Input.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Material_Input.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Material_Input.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Material_Input.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Material_Input.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Material_Input.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Material_Input.TabIndex = 207;
            this.btn_Material_Input.Tag = "SUB_0205";
            this.btn_Material_Input.Values.Text = "Nhập kho\r\nNội bộ - NL";
            // 
            // btn_Material_Output
            // 
            this.btn_Material_Output.Location = new System.Drawing.Point(100, 9);
            this.btn_Material_Output.Name = "btn_Material_Output";
            this.btn_Material_Output.Size = new System.Drawing.Size(90, 90);
            this.btn_Material_Output.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Material_Output.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Material_Output.StateCommon.Back.ColorAngle = 75F;
            this.btn_Material_Output.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Material_Output.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Material_Output.StateCommon.Border.Rounding = 10;
            this.btn_Material_Output.StateCommon.Border.Width = 2;
            this.btn_Material_Output.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Material_Output.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Material_Output.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Material_Output.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Material_Output.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Material_Output.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Material_Output.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Material_Output.TabIndex = 206;
            this.btn_Material_Output.Tag = "SUB_0203";
            this.btn_Material_Output.Values.Text = "Xuất kho\r\nNội bộ - NL";
            // 
            // Panel_Setup
            // 
            this.Panel_Setup.Controls.Add(this.btn_ProductType);
            this.Panel_Setup.Controls.Add(this.btn_WorkGroup);
            this.Panel_Setup.Controls.Add(this.btn_Setup_Coefficent);
            this.Panel_Setup.Controls.Add(this.btn_Convert_Unit);
            this.Panel_Setup.Controls.Add(this.btn_Position);
            this.Panel_Setup.Controls.Add(this.btn_Team);
            this.Panel_Setup.Controls.Add(this.btn_Department);
            this.Panel_Setup.Controls.Add(this.btn_Branch);
            this.Panel_Setup.Controls.Add(this.btn_Time_Defines);
            this.Panel_Setup.Controls.Add(this.btn_Holiday);
            this.Panel_Setup.Controls.Add(this.btn_Paramaters);
            this.Panel_Setup.Controls.Add(this.btn_Time_Work);
            this.Panel_Setup.Controls.Add(this.btn_ThanhPham);
            this.Panel_Setup.Controls.Add(this.btn_Product);
            this.Panel_Setup.Controls.Add(this.btn_Combo);
            this.Panel_Setup.Controls.Add(this.btn_Setup_Unit);
            this.Panel_Setup.Controls.Add(this.btn_Setup_Off);
            this.Panel_Setup.Controls.Add(this.btn_Setup_Rice);
            this.Panel_Setup.Controls.Add(this.btn_Stages);
            this.Panel_Setup.Controls.Add(this.btn_Customer_Company);
            this.Panel_Setup.Controls.Add(this.btn_Lock);
            this.Panel_Setup.Controls.Add(this.btn_LogUser);
            this.Panel_Setup.Controls.Add(this.btn_Role);
            this.Panel_Setup.Controls.Add(this.btn_Fee);
            this.Panel_Setup.Controls.Add(this.btn_Customer_Individual);
            this.Panel_Setup.Controls.Add(this.btn_Users);
            this.Panel_Setup.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.Panel_Setup.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.Panel_Setup.Location = new System.Drawing.Point(1970, 2380);
            this.Panel_Setup.Name = "Panel_Setup";
            this.Panel_Setup.Size = new System.Drawing.Size(719, 527);
            this.Panel_Setup.TabIndex = 172;
            this.Panel_Setup.Text = "Cài Đặt ";
            // 
            // btn_ProductType
            // 
            this.btn_ProductType.Location = new System.Drawing.Point(384, 7);
            this.btn_ProductType.Name = "btn_ProductType";
            this.btn_ProductType.Size = new System.Drawing.Size(90, 90);
            this.btn_ProductType.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_ProductType.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_ProductType.StateCommon.Back.ColorAngle = 75F;
            this.btn_ProductType.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_ProductType.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_ProductType.StateCommon.Border.Rounding = 10;
            this.btn_ProductType.StateCommon.Border.Width = 2;
            this.btn_ProductType.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_ProductType.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ProductType.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_ProductType.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_ProductType.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ProductType.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ProductType.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ProductType.TabIndex = 209;
            this.btn_ProductType.Tag = "SUB_0007";
            this.btn_ProductType.Values.Text = "Loại \r\nsản phẩm";
            this.btn_ProductType.Click += new System.EventHandler(this.btn_ProductType_Click);
            // 
            // btn_WorkGroup
            // 
            this.btn_WorkGroup.Location = new System.Drawing.Point(478, 7);
            this.btn_WorkGroup.Name = "btn_WorkGroup";
            this.btn_WorkGroup.Size = new System.Drawing.Size(90, 90);
            this.btn_WorkGroup.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_WorkGroup.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_WorkGroup.StateCommon.Back.ColorAngle = 75F;
            this.btn_WorkGroup.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_WorkGroup.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_WorkGroup.StateCommon.Border.Rounding = 10;
            this.btn_WorkGroup.StateCommon.Border.Width = 2;
            this.btn_WorkGroup.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_WorkGroup.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_WorkGroup.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_WorkGroup.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_WorkGroup.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_WorkGroup.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_WorkGroup.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_WorkGroup.TabIndex = 208;
            this.btn_WorkGroup.Tag = "SUB_0008";
            this.btn_WorkGroup.Values.Text = "Nhóm \r\ncông đoạn\r\n";
            this.btn_WorkGroup.Click += new System.EventHandler(this.btn_WorkGroup_Click);
            // 
            // btn_Setup_Coefficent
            // 
            this.btn_Setup_Coefficent.Location = new System.Drawing.Point(195, 7);
            this.btn_Setup_Coefficent.Name = "btn_Setup_Coefficent";
            this.btn_Setup_Coefficent.Size = new System.Drawing.Size(90, 90);
            this.btn_Setup_Coefficent.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Setup_Coefficent.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Setup_Coefficent.StateCommon.Back.ColorAngle = 75F;
            this.btn_Setup_Coefficent.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Setup_Coefficent.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Setup_Coefficent.StateCommon.Border.Rounding = 10;
            this.btn_Setup_Coefficent.StateCommon.Border.Width = 2;
            this.btn_Setup_Coefficent.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Setup_Coefficent.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Setup_Coefficent.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Setup_Coefficent.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Setup_Coefficent.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Setup_Coefficent.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Setup_Coefficent.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Setup_Coefficent.TabIndex = 207;
            this.btn_Setup_Coefficent.Tag = "SUB_0005";
            this.btn_Setup_Coefficent.Values.Text = "Hệ số\r\nnăng suất";
            // 
            // btn_Convert_Unit
            // 
            this.btn_Convert_Unit.Location = new System.Drawing.Point(383, 103);
            this.btn_Convert_Unit.Name = "btn_Convert_Unit";
            this.btn_Convert_Unit.Size = new System.Drawing.Size(90, 90);
            this.btn_Convert_Unit.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Convert_Unit.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Convert_Unit.StateCommon.Back.ColorAngle = 75F;
            this.btn_Convert_Unit.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Convert_Unit.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Convert_Unit.StateCommon.Border.Rounding = 10;
            this.btn_Convert_Unit.StateCommon.Border.Width = 2;
            this.btn_Convert_Unit.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Convert_Unit.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Convert_Unit.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Convert_Unit.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Convert_Unit.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Convert_Unit.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Convert_Unit.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Convert_Unit.TabIndex = 207;
            this.btn_Convert_Unit.Tag = "SUB_0014";
            this.btn_Convert_Unit.Values.Text = "Đơn vị \r\nchuyển đổi";
            // 
            // btn_Position
            // 
            this.btn_Position.Location = new System.Drawing.Point(289, 103);
            this.btn_Position.Name = "btn_Position";
            this.btn_Position.Size = new System.Drawing.Size(90, 90);
            this.btn_Position.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Position.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Position.StateCommon.Back.ColorAngle = 75F;
            this.btn_Position.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Position.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Position.StateCommon.Border.Rounding = 10;
            this.btn_Position.StateCommon.Border.Width = 2;
            this.btn_Position.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Position.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Position.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Position.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Position.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Position.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Position.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Position.TabIndex = 207;
            this.btn_Position.Tag = "SUB_0013";
            this.btn_Position.Values.Text = "Vị trí\r\nchức vụ";
            // 
            // btn_Team
            // 
            this.btn_Team.Location = new System.Drawing.Point(196, 103);
            this.btn_Team.Name = "btn_Team";
            this.btn_Team.Size = new System.Drawing.Size(90, 90);
            this.btn_Team.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Team.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Team.StateCommon.Back.ColorAngle = 75F;
            this.btn_Team.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Team.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Team.StateCommon.Border.Rounding = 10;
            this.btn_Team.StateCommon.Border.Width = 2;
            this.btn_Team.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Team.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Team.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Team.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Team.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Team.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Team.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Team.TabIndex = 207;
            this.btn_Team.Tag = "SUB_0012";
            this.btn_Team.Values.Text = "Nhóm, tổ";
            // 
            // btn_Department
            // 
            this.btn_Department.Location = new System.Drawing.Point(100, 103);
            this.btn_Department.Name = "btn_Department";
            this.btn_Department.Size = new System.Drawing.Size(90, 90);
            this.btn_Department.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Department.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Department.StateCommon.Back.ColorAngle = 75F;
            this.btn_Department.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Department.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Department.StateCommon.Border.Rounding = 10;
            this.btn_Department.StateCommon.Border.Width = 2;
            this.btn_Department.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Department.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Department.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Department.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Department.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Department.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Department.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Department.StateTracking.Content.ShortText.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Department.TabIndex = 207;
            this.btn_Department.Tag = "SUB_0011";
            this.btn_Department.Values.Text = "Bộ phận";
            // 
            // btn_Branch
            // 
            this.btn_Branch.Location = new System.Drawing.Point(6, 103);
            this.btn_Branch.Name = "btn_Branch";
            this.btn_Branch.Size = new System.Drawing.Size(90, 90);
            this.btn_Branch.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Branch.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Branch.StateCommon.Back.ColorAngle = 75F;
            this.btn_Branch.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Branch.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Branch.StateCommon.Border.Rounding = 10;
            this.btn_Branch.StateCommon.Border.Width = 2;
            this.btn_Branch.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Branch.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Branch.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Branch.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Branch.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Branch.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Branch.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Branch.TabIndex = 207;
            this.btn_Branch.Tag = "SUB_0010";
            this.btn_Branch.Values.Text = "Khối\r\nkhu vực";
            // 
            // btn_Time_Defines
            // 
            this.btn_Time_Defines.Location = new System.Drawing.Point(6, 197);
            this.btn_Time_Defines.Name = "btn_Time_Defines";
            this.btn_Time_Defines.Size = new System.Drawing.Size(90, 90);
            this.btn_Time_Defines.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Time_Defines.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Time_Defines.StateCommon.Back.ColorAngle = 75F;
            this.btn_Time_Defines.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Time_Defines.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Time_Defines.StateCommon.Border.Rounding = 10;
            this.btn_Time_Defines.StateCommon.Border.Width = 2;
            this.btn_Time_Defines.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Time_Defines.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Time_Defines.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Time_Defines.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Time_Defines.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Time_Defines.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Time_Defines.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Time_Defines.TabIndex = 207;
            this.btn_Time_Defines.Tag = "SUB_0017";
            this.btn_Time_Defines.Values.Text = "Mã \r\nchấm công";
            // 
            // btn_Holiday
            // 
            this.btn_Holiday.Location = new System.Drawing.Point(195, 197);
            this.btn_Holiday.Name = "btn_Holiday";
            this.btn_Holiday.Size = new System.Drawing.Size(90, 90);
            this.btn_Holiday.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Holiday.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Holiday.StateCommon.Back.ColorAngle = 75F;
            this.btn_Holiday.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Holiday.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Holiday.StateCommon.Border.Rounding = 10;
            this.btn_Holiday.StateCommon.Border.Width = 2;
            this.btn_Holiday.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Holiday.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Holiday.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Holiday.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Holiday.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Holiday.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Holiday.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Holiday.TabIndex = 207;
            this.btn_Holiday.Tag = "SUB_0018";
            this.btn_Holiday.Values.Text = "Ngày nghĩ\r\nlễ";
            // 
            // btn_Paramaters
            // 
            this.btn_Paramaters.Location = new System.Drawing.Point(289, 197);
            this.btn_Paramaters.Name = "btn_Paramaters";
            this.btn_Paramaters.Size = new System.Drawing.Size(90, 90);
            this.btn_Paramaters.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Paramaters.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Paramaters.StateCommon.Back.ColorAngle = 75F;
            this.btn_Paramaters.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Paramaters.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Paramaters.StateCommon.Border.Rounding = 10;
            this.btn_Paramaters.StateCommon.Border.Width = 2;
            this.btn_Paramaters.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Paramaters.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Paramaters.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Paramaters.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Paramaters.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Paramaters.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Paramaters.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Paramaters.TabIndex = 207;
            this.btn_Paramaters.Tag = "SUB_0019";
            this.btn_Paramaters.Values.Text = "Tham số\r\nmặc định";
            // 
            // btn_Time_Work
            // 
            this.btn_Time_Work.Location = new System.Drawing.Point(473, 400);
            this.btn_Time_Work.Name = "btn_Time_Work";
            this.btn_Time_Work.Size = new System.Drawing.Size(90, 90);
            this.btn_Time_Work.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Time_Work.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Time_Work.StateCommon.Back.ColorAngle = 75F;
            this.btn_Time_Work.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Time_Work.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Time_Work.StateCommon.Border.Rounding = 10;
            this.btn_Time_Work.StateCommon.Border.Width = 2;
            this.btn_Time_Work.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Time_Work.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Time_Work.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Time_Work.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Time_Work.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Time_Work.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Time_Work.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Time_Work.TabIndex = 207;
            this.btn_Time_Work.Tag = "";
            this.btn_Time_Work.Values.Text = "Ca làm việc";
            this.btn_Time_Work.Visible = false;
            // 
            // btn_ThanhPham
            // 
            this.btn_ThanhPham.Location = new System.Drawing.Point(101, 7);
            this.btn_ThanhPham.Name = "btn_ThanhPham";
            this.btn_ThanhPham.Size = new System.Drawing.Size(90, 90);
            this.btn_ThanhPham.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_ThanhPham.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_ThanhPham.StateCommon.Back.ColorAngle = 75F;
            this.btn_ThanhPham.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_ThanhPham.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_ThanhPham.StateCommon.Border.Rounding = 10;
            this.btn_ThanhPham.StateCommon.Border.Width = 2;
            this.btn_ThanhPham.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_ThanhPham.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ThanhPham.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_ThanhPham.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_ThanhPham.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ThanhPham.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ThanhPham.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ThanhPham.TabIndex = 206;
            this.btn_ThanhPham.Tag = "SUB_0023";
            this.btn_ThanhPham.Values.Text = "Sản phẩm";
            // 
            // btn_Product
            // 
            this.btn_Product.Location = new System.Drawing.Point(6, 7);
            this.btn_Product.Name = "btn_Product";
            this.btn_Product.Size = new System.Drawing.Size(90, 90);
            this.btn_Product.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Product.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Product.StateCommon.Back.ColorAngle = 75F;
            this.btn_Product.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Product.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Product.StateCommon.Border.Rounding = 10;
            this.btn_Product.StateCommon.Border.Width = 2;
            this.btn_Product.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Product.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Product.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Product.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Product.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Product.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Product.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Product.TabIndex = 206;
            this.btn_Product.Tag = "SUB_0004";
            this.btn_Product.Values.Text = "Nhóm \r\nsản phẩm";
            // 
            // btn_Combo
            // 
            this.btn_Combo.Location = new System.Drawing.Point(477, 103);
            this.btn_Combo.Name = "btn_Combo";
            this.btn_Combo.Size = new System.Drawing.Size(90, 90);
            this.btn_Combo.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Combo.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Combo.StateCommon.Back.ColorAngle = 75F;
            this.btn_Combo.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Combo.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Combo.StateCommon.Border.Rounding = 10;
            this.btn_Combo.StateCommon.Border.Width = 2;
            this.btn_Combo.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Combo.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Combo.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Combo.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Combo.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Combo.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Combo.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Combo.TabIndex = 206;
            this.btn_Combo.Tag = "SUB_0015";
            this.btn_Combo.Values.Text = "Định\r\ngiá thành \r\nthành phẩm";
            // 
            // btn_Setup_Unit
            // 
            this.btn_Setup_Unit.Location = new System.Drawing.Point(572, 7);
            this.btn_Setup_Unit.Name = "btn_Setup_Unit";
            this.btn_Setup_Unit.Size = new System.Drawing.Size(90, 90);
            this.btn_Setup_Unit.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Setup_Unit.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Setup_Unit.StateCommon.Back.ColorAngle = 75F;
            this.btn_Setup_Unit.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Setup_Unit.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Setup_Unit.StateCommon.Border.Rounding = 10;
            this.btn_Setup_Unit.StateCommon.Border.Width = 2;
            this.btn_Setup_Unit.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Setup_Unit.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Setup_Unit.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Setup_Unit.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Setup_Unit.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Setup_Unit.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Setup_Unit.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Setup_Unit.TabIndex = 206;
            this.btn_Setup_Unit.Tag = "SUB_0009";
            this.btn_Setup_Unit.Values.Text = "Đơn vị tính";
            // 
            // btn_Setup_Off
            // 
            this.btn_Setup_Off.Location = new System.Drawing.Point(473, 304);
            this.btn_Setup_Off.Name = "btn_Setup_Off";
            this.btn_Setup_Off.Size = new System.Drawing.Size(90, 90);
            this.btn_Setup_Off.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Setup_Off.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Setup_Off.StateCommon.Back.ColorAngle = 75F;
            this.btn_Setup_Off.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Setup_Off.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Setup_Off.StateCommon.Border.Rounding = 10;
            this.btn_Setup_Off.StateCommon.Border.Width = 2;
            this.btn_Setup_Off.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Setup_Off.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Setup_Off.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Setup_Off.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Setup_Off.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Setup_Off.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Setup_Off.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Setup_Off.TabIndex = 206;
            this.btn_Setup_Off.Tag = "";
            this.btn_Setup_Off.Values.Text = "Mã \r\nphép trừ";
            this.btn_Setup_Off.Visible = false;
            this.btn_Setup_Off.Click += new System.EventHandler(this.btn_Setup_Off_Click);
            // 
            // btn_Setup_Rice
            // 
            this.btn_Setup_Rice.Location = new System.Drawing.Point(101, 197);
            this.btn_Setup_Rice.Name = "btn_Setup_Rice";
            this.btn_Setup_Rice.Size = new System.Drawing.Size(90, 90);
            this.btn_Setup_Rice.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Setup_Rice.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Setup_Rice.StateCommon.Back.ColorAngle = 75F;
            this.btn_Setup_Rice.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Setup_Rice.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Setup_Rice.StateCommon.Border.Rounding = 10;
            this.btn_Setup_Rice.StateCommon.Border.Width = 2;
            this.btn_Setup_Rice.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Setup_Rice.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Setup_Rice.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Setup_Rice.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Setup_Rice.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Setup_Rice.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Setup_Rice.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Setup_Rice.TabIndex = 206;
            this.btn_Setup_Rice.Tag = "SUB_0016";
            this.btn_Setup_Rice.Values.Text = "Mã cơm";
            // 
            // btn_Stages
            // 
            this.btn_Stages.Location = new System.Drawing.Point(289, 7);
            this.btn_Stages.Name = "btn_Stages";
            this.btn_Stages.Size = new System.Drawing.Size(90, 90);
            this.btn_Stages.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Stages.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Stages.StateCommon.Back.ColorAngle = 75F;
            this.btn_Stages.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Stages.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Stages.StateCommon.Border.Rounding = 10;
            this.btn_Stages.StateCommon.Border.Width = 2;
            this.btn_Stages.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Stages.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Stages.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Stages.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Stages.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Stages.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Stages.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Stages.TabIndex = 206;
            this.btn_Stages.Tag = "SUB_0006";
            this.btn_Stages.Values.Text = "Công đoạn\r\nsản xuất";
            // 
            // btn_Customer_Company
            // 
            this.btn_Customer_Company.Location = new System.Drawing.Point(102, 291);
            this.btn_Customer_Company.Name = "btn_Customer_Company";
            this.btn_Customer_Company.Size = new System.Drawing.Size(90, 90);
            this.btn_Customer_Company.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Customer_Company.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Customer_Company.StateCommon.Back.ColorAngle = 75F;
            this.btn_Customer_Company.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Customer_Company.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Customer_Company.StateCommon.Border.Rounding = 10;
            this.btn_Customer_Company.StateCommon.Border.Width = 2;
            this.btn_Customer_Company.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Customer_Company.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Customer_Company.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Customer_Company.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Customer_Company.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Customer_Company.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Customer_Company.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Customer_Company.TabIndex = 206;
            this.btn_Customer_Company.Tag = "SUB_0022";
            this.btn_Customer_Company.Values.Text = "Khách\r\ncông ty";
            // 
            // btn_Lock
            // 
            this.btn_Lock.Location = new System.Drawing.Point(196, 389);
            this.btn_Lock.Name = "btn_Lock";
            this.btn_Lock.Size = new System.Drawing.Size(90, 90);
            this.btn_Lock.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Lock.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Lock.StateCommon.Back.ColorAngle = 75F;
            this.btn_Lock.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Lock.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Lock.StateCommon.Border.Rounding = 10;
            this.btn_Lock.StateCommon.Border.Width = 2;
            this.btn_Lock.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Lock.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Lock.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Lock.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Lock.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Lock.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Lock.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Lock.TabIndex = 205;
            this.btn_Lock.Tag = "SUB_0024";
            this.btn_Lock.Values.Text = "Khóa\r\ndữ liệu";
            // 
            // btn_LogUser
            // 
            this.btn_LogUser.Location = new System.Drawing.Point(291, 391);
            this.btn_LogUser.Name = "btn_LogUser";
            this.btn_LogUser.Size = new System.Drawing.Size(90, 90);
            this.btn_LogUser.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_LogUser.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_LogUser.StateCommon.Back.ColorAngle = 75F;
            this.btn_LogUser.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_LogUser.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_LogUser.StateCommon.Border.Rounding = 10;
            this.btn_LogUser.StateCommon.Border.Width = 2;
            this.btn_LogUser.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_LogUser.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_LogUser.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_LogUser.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_LogUser.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_LogUser.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_LogUser.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_LogUser.TabIndex = 205;
            this.btn_LogUser.Tag = "SUB_0003";
            this.btn_LogUser.Values.Text = "Theo dõi\r\nHoạt động";
            // 
            // btn_Role
            // 
            this.btn_Role.Location = new System.Drawing.Point(101, 387);
            this.btn_Role.Name = "btn_Role";
            this.btn_Role.Size = new System.Drawing.Size(90, 90);
            this.btn_Role.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Role.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Role.StateCommon.Back.ColorAngle = 75F;
            this.btn_Role.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Role.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Role.StateCommon.Border.Rounding = 10;
            this.btn_Role.StateCommon.Border.Width = 2;
            this.btn_Role.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Role.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Role.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Role.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Role.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Role.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Role.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Role.TabIndex = 205;
            this.btn_Role.Tag = "SUB_0002";
            this.btn_Role.Values.Text = "Phân quyền";
            // 
            // btn_Fee
            // 
            this.btn_Fee.Location = new System.Drawing.Point(383, 197);
            this.btn_Fee.Name = "btn_Fee";
            this.btn_Fee.Size = new System.Drawing.Size(90, 90);
            this.btn_Fee.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Fee.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Fee.StateCommon.Back.ColorAngle = 75F;
            this.btn_Fee.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Fee.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Fee.StateCommon.Border.Rounding = 10;
            this.btn_Fee.StateCommon.Border.Width = 2;
            this.btn_Fee.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Fee.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Fee.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Fee.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Fee.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Fee.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Fee.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Fee.TabIndex = 206;
            this.btn_Fee.Tag = "SUB_0020";
            this.btn_Fee.Values.Text = "Danh mục\r\nphí\r\nthu-chi";
            // 
            // btn_Customer_Individual
            // 
            this.btn_Customer_Individual.Location = new System.Drawing.Point(6, 291);
            this.btn_Customer_Individual.Name = "btn_Customer_Individual";
            this.btn_Customer_Individual.Size = new System.Drawing.Size(90, 90);
            this.btn_Customer_Individual.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Customer_Individual.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Customer_Individual.StateCommon.Back.ColorAngle = 75F;
            this.btn_Customer_Individual.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Customer_Individual.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Customer_Individual.StateCommon.Border.Rounding = 10;
            this.btn_Customer_Individual.StateCommon.Border.Width = 2;
            this.btn_Customer_Individual.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Customer_Individual.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Customer_Individual.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Customer_Individual.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Customer_Individual.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Customer_Individual.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Customer_Individual.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Customer_Individual.TabIndex = 206;
            this.btn_Customer_Individual.Tag = "SUB_0021";
            this.btn_Customer_Individual.Values.Text = "Khách \r\ncá nhân";
            // 
            // btn_Users
            // 
            this.btn_Users.Location = new System.Drawing.Point(8, 387);
            this.btn_Users.Name = "btn_Users";
            this.btn_Users.Size = new System.Drawing.Size(90, 90);
            this.btn_Users.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Users.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Users.StateCommon.Back.ColorAngle = 75F;
            this.btn_Users.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Users.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Users.StateCommon.Border.Rounding = 10;
            this.btn_Users.StateCommon.Border.Width = 2;
            this.btn_Users.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Users.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Users.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Users.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Users.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Users.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Users.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Users.TabIndex = 204;
            this.btn_Users.Tag = "SUB_0001";
            this.btn_Users.Values.Text = "Người dùng";
            // 
            // btn_Exit
            // 
            this.btn_Exit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Exit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btn_Exit.Location = new System.Drawing.Point(1151, 12);
            this.btn_Exit.Name = "btn_Exit";
            this.btn_Exit.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Exit.Size = new System.Drawing.Size(120, 40);
            this.btn_Exit.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Exit.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Exit.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Exit.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Exit.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Exit.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Exit.TabIndex = 180;
            this.btn_Exit.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Exit.Values.Image")));
            this.btn_Exit.Values.Text = "Đăng xuất";
            // 
            // btn_ChangePass
            // 
            this.btn_ChangePass.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_ChangePass.Location = new System.Drawing.Point(1022, 12);
            this.btn_ChangePass.Name = "btn_ChangePass";
            this.btn_ChangePass.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_ChangePass.Size = new System.Drawing.Size(120, 40);
            this.btn_ChangePass.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_ChangePass.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_ChangePass.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ChangePass.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_ChangePass.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ChangePass.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ChangePass.TabIndex = 180;
            this.btn_ChangePass.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_ChangePass.Values.Image")));
            this.btn_ChangePass.Values.Text = "Đổi mật khẩu";
            // 
            // Header
            // 
            this.Header.AutoScroll = true;
            this.Header.Controls.Add(this.dte_WorkDate);
            this.Header.Controls.Add(this.lbl_EmployeeName);
            this.Header.Controls.Add(this.label15);
            this.Header.Controls.Add(this.btn_Exit);
            this.Header.Controls.Add(this.btn_Info);
            this.Header.Controls.Add(this.btn_Download);
            this.Header.Controls.Add(this.btn_ChangePass);
            this.Header.Dock = System.Windows.Forms.DockStyle.Top;
            this.Header.Location = new System.Drawing.Point(0, 42);
            this.Header.Name = "Header";
            this.Header.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.Header.PanelBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.HeaderPrimary;
            this.Header.Size = new System.Drawing.Size(1266, 65);
            this.Header.StateCommon.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.Header.StateCommon.Color2 = System.Drawing.Color.LightGreen;
            this.Header.StateCommon.ColorAngle = 75F;
            this.Header.StateCommon.ColorStyle = ComponentFactory.Krypton.Toolkit.PaletteColorStyle.Rounding5;
            this.Header.TabIndex = 183;
            // 
            // dte_WorkDate
            // 
            this.dte_WorkDate.CustomFormat = "dd/MM/yyyy";
            this.dte_WorkDate.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.dte_WorkDate.Location = new System.Drawing.Point(126, 32);
            this.dte_WorkDate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dte_WorkDate.Name = "dte_WorkDate";
            this.dte_WorkDate.Size = new System.Drawing.Size(121, 26);
            this.dte_WorkDate.TabIndex = 316;
            this.dte_WorkDate.Value = new System.DateTime(((long)(0)));
            // 
            // lbl_EmployeeName
            // 
            this.lbl_EmployeeName.AutoSize = true;
            this.lbl_EmployeeName.BackColor = System.Drawing.Color.Transparent;
            this.lbl_EmployeeName.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_EmployeeName.ForeColor = System.Drawing.Color.Green;
            this.lbl_EmployeeName.Location = new System.Drawing.Point(12, 12);
            this.lbl_EmployeeName.Name = "lbl_EmployeeName";
            this.lbl_EmployeeName.Size = new System.Drawing.Size(84, 16);
            this.lbl_EmployeeName.TabIndex = 182;
            this.lbl_EmployeeName.Text = "Nhân viên : ";
            this.lbl_EmployeeName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Green;
            this.label15.Location = new System.Drawing.Point(12, 36);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(112, 16);
            this.label15.TabIndex = 183;
            this.label15.Text = "Ngày Làm Việc :";
            // 
            // btn_Info
            // 
            this.btn_Info.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Info.Location = new System.Drawing.Point(770, 12);
            this.btn_Info.Name = "btn_Info";
            this.btn_Info.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Info.Size = new System.Drawing.Size(120, 40);
            this.btn_Info.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Info.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Info.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Info.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Info.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Info.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Info.TabIndex = 180;
            this.btn_Info.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Info.Values.Image")));
            this.btn_Info.Values.Text = "Hướng dẫn";
            // 
            // btn_Download
            // 
            this.btn_Download.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Download.Location = new System.Drawing.Point(896, 12);
            this.btn_Download.Name = "btn_Download";
            this.btn_Download.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Download.Size = new System.Drawing.Size(120, 40);
            this.btn_Download.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Download.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Download.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Download.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Download.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Download.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Download.TabIndex = 180;
            this.btn_Download.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Download.Values.Image")));
            this.btn_Download.Values.Text = "Excel Mẫu";
            // 
            // HeaderControl
            // 
            this.HeaderControl.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnMini,
            this.btnMax,
            this.btnClose});
            this.HeaderControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.HeaderControl.Location = new System.Drawing.Point(0, 0);
            this.HeaderControl.Name = "HeaderControl";
            this.HeaderControl.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.HeaderControl.Size = new System.Drawing.Size(1266, 42);
            this.HeaderControl.TabIndex = 184;
            this.HeaderControl.Values.Description = "";
            this.HeaderControl.Values.Heading = "Quản lý sản xuất";
            this.HeaderControl.Values.Image = ((System.Drawing.Image)(resources.GetObject("HeaderControl.Values.Image")));
            this.HeaderControl.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Frm_Main_MouseDown);
            this.HeaderControl.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Frm_Main_MouseMove);
            this.HeaderControl.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Frm_Main_MouseUp);
            // 
            // btnMini
            // 
            this.btnMini.Image = ((System.Drawing.Image)(resources.GetObject("btnMini.Image")));
            this.btnMini.UniqueName = "F5F06E8241504E72ABB5BEA3F6A9B753";
            this.btnMini.Click += new System.EventHandler(this.btnMini_Click);
            // 
            // btnMax
            // 
            this.btnMax.Image = ((System.Drawing.Image)(resources.GetObject("btnMax.Image")));
            this.btnMax.UniqueName = "035D1A4881E44F58A084C31DE7352A94";
            this.btnMax.Click += new System.EventHandler(this.btnMax_Click);
            // 
            // btnClose
            // 
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.UniqueName = "11B07C6F4E1C4F9D8B91BD924CB0EBE6";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // kryptonHeader2
            // 
            this.kryptonHeader2.AutoSize = false;
            this.kryptonHeader2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.kryptonHeader2.HeaderStyle = ComponentFactory.Krypton.Toolkit.HeaderStyle.Secondary;
            this.kryptonHeader2.Location = new System.Drawing.Point(0, 723);
            this.kryptonHeader2.Name = "kryptonHeader2";
            this.kryptonHeader2.Size = new System.Drawing.Size(1266, 45);
            this.kryptonHeader2.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kryptonHeader2.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.kryptonHeader2.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.kryptonHeader2.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.kryptonHeader2.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.kryptonHeader2.TabIndex = 185;
            this.kryptonHeader2.Values.Description = "";
            this.kryptonHeader2.Values.Heading = "Khu Công nghiệp Cái Sơn Hàng Bàng, KV4, Phường An Bình, Quận Ninh Kiều, Thành phố" +
    " Cần Thơ, Việt Nam\r\nSố Điện Thoại : 84.292.3893893-3893077-3893067\r\n";
            this.kryptonHeader2.Values.Image = null;
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(73)))), ((int)(((byte)(184)))), ((int)(((byte)(26)))));
            this.button10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button10.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button10.ForeColor = System.Drawing.Color.White;
            this.button10.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button10.Location = new System.Drawing.Point(186, 19);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(87, 86);
            this.button10.TabIndex = 11;
            this.button10.Tag = "TNS_CL001";
            this.button10.Text = "Năng Suất Chia Lại";
            this.button10.UseVisualStyleBackColor = false;
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(73)))), ((int)(((byte)(184)))), ((int)(((byte)(26)))));
            this.button11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button11.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button11.ForeColor = System.Drawing.Color.White;
            this.button11.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button11.Location = new System.Drawing.Point(6, 109);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(87, 86);
            this.button11.TabIndex = 8;
            this.button11.Tag = "TNST_SX001";
            this.button11.Text = "Tính Lương Năng Suất Ngày / Tháng";
            this.button11.UseVisualStyleBackColor = false;
            // 
            // button12
            // 
            this.button12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(73)))), ((int)(((byte)(184)))), ((int)(((byte)(26)))));
            this.button12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button12.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button12.ForeColor = System.Drawing.Color.White;
            this.button12.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button12.Location = new System.Drawing.Point(96, 109);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(87, 86);
            this.button12.TabIndex = 10;
            this.button12.Tag = "TNS_SX001";
            this.button12.Text = "Tính Tiền Giờ Dư";
            this.button12.UseVisualStyleBackColor = false;
            // 
            // button13
            // 
            this.button13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(73)))), ((int)(((byte)(184)))), ((int)(((byte)(26)))));
            this.button13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button13.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button13.ForeColor = System.Drawing.Color.White;
            this.button13.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button13.Location = new System.Drawing.Point(96, 19);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(87, 86);
            this.button13.TabIndex = 10;
            this.button13.Tag = "TNS_SX001";
            this.button13.Text = "Tính Tiền Năng Suất";
            this.button13.UseVisualStyleBackColor = false;
            // 
            // button14
            // 
            this.button14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(73)))), ((int)(((byte)(184)))), ((int)(((byte)(26)))));
            this.button14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button14.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button14.ForeColor = System.Drawing.Color.White;
            this.button14.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button14.Location = new System.Drawing.Point(6, 19);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(87, 86);
            this.button14.TabIndex = 9;
            this.button14.Tag = "GNS_XS001";
            this.button14.Text = "Ghi Năng Suất";
            this.button14.UseVisualStyleBackColor = false;
            // 
            // txt_ChayChu
            // 
            this.txt_ChayChu.AutoSize = true;
            this.txt_ChayChu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.txt_ChayChu.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_ChayChu.ForeColor = System.Drawing.Color.Red;
            this.txt_ChayChu.Location = new System.Drawing.Point(239, 13);
            this.txt_ChayChu.Name = "txt_ChayChu";
            this.txt_ChayChu.Size = new System.Drawing.Size(265, 16);
            this.txt_ChayChu.TabIndex = 186;
            this.txt_ChayChu.Text = "Vui lòng nhập liệu để hoàn thiện phần mềm!";
            this.txt_ChayChu.Visible = false;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Frm_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.CancelButton = this.btn_Exit;
            this.ClientSize = new System.Drawing.Size(1266, 768);
            this.ControlBox = false;
            this.Controls.Add(this.txt_ChayChu);
            this.Controls.Add(this.Panel_Right);
            this.Controls.Add(this.panel_Left);
            this.Controls.Add(this.Header);
            this.Controls.Add(this.HeaderControl);
            this.Controls.Add(this.kryptonHeader2);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Trang chủ";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Frm_Main_FormClosing);
            this.Load += new System.EventHandler(this.Frm_Main_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Frm_Main_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Frm_Main_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Frm_Main_MouseUp);
            this.panel_Left.ResumeLayout(false);
            this.Panel_Right.ResumeLayout(false);
            this.Panel_Right.PerformLayout();
            this.Panel_Salary.ResumeLayout(false);
            this.Panel_Salary.PerformLayout();
            this.Panel_Human.ResumeLayout(false);
            this.Panel_Human.PerformLayout();
            this.Panel_Report.ResumeLayout(false);
            this.Panel_Report.PerformLayout();
            this.Panel_Production.ResumeLayout(false);
            this.Panel_Temp.ResumeLayout(false);
            this.Panel_Recycle.ResumeLayout(false);
            this.Panel_Box.ResumeLayout(false);
            this.Panel_VuonUom.ResumeLayout(false);
            this.Panel_ThachDua.ResumeLayout(false);
            this.Panel_Freezing.ResumeLayout(false);
            this.Panel_Material.ResumeLayout(false);
            this.Panel_Setup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Header)).EndInit();
            this.Header.ResumeLayout(false);
            this.Header.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panel_Left;
        private System.Windows.Forms.Panel Panel_Right;
        private System.Windows.Forms.Panel Panel_Setup;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Exit;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_ChangePass;
        private ComponentFactory.Krypton.Toolkit.KryptonPanel Header;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader HeaderControl;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMini;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMax;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnClose;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader2;
        private System.Windows.Forms.Label lbl_EmployeeName;
        private System.Windows.Forms.Label label15;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Warehouse_1;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Setup;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Warehouse_2;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Warehouse_3;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Production;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Human;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Report;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Warehouse_4;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Warehouse_5;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Production_Note;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Production_ReCalculator;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Production_Calculator;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Production_Payroll;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Production_Time;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Rpt1;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Rpt2;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Setup_Coefficent;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Convert_Unit;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Position;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Team;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Department;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Branch;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Time_Defines;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Holiday;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Paramaters;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Time_Work;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Product;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Setup_Unit;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Setup_Rice;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Stages;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Customer_Company;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Role;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Fee;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Customer_Individual;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Users;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Caculator_Keeping;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Payment;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Contract;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_DayOffHoliday;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Receipt;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_TimeKeeping;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Employee;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader1;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader txtTitle;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Box_Output_Approve_Customer;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Box_Output_Approve;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Box_Output_Customer;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Box_Output;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Box_Input_Approve;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Box_Input;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Freez_Output_Approve_Customer;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Freez_Output_Approve;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Freez_Input_Approve;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Freez_Output_Customer;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Freez_Output;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Freez_Input;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Material_Input_Approve;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Material_Output_Approve;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Material_Input;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Material_Output;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Material_Input_Customer_Approve;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Material_Input_Customer;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Recycle_Input_Approve;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Recycle_Ouput_Approve;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Recycle_Input_Approve_Customer;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Recycle_Input;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Recycle_Output;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Recycle_Input_Customer;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Temp_Output_Approve;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Temp_Input_Approve;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Temp_Output;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Temp_Input;
        private System.Windows.Forms.Panel Panel_Material;
        private System.Windows.Forms.Panel Panel_Freezing;
        private System.Windows.Forms.Panel Panel_Box;
        private System.Windows.Forms.Panel Panel_Temp;
        private System.Windows.Forms.Panel Panel_Recycle;
        private System.Windows.Forms.Panel Panel_Human;
        private System.Windows.Forms.Panel Panel_Report;
        private System.Windows.Forms.Panel Panel_Production;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Rpt3;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Production_Auto_Calculator;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Production_Auto_Money;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Production_Calculator_V2;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Import_Record;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Rpt4;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Import_Production_Time;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Production_ReCalculator2;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_ProductType;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_WorkGroup;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Adjusted;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Setup_Off;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Rpt5;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Report_6;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_FeeDefault;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Support_Leader;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Standard;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_FeeChildren;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_SalaryDefault;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_CaculatorWorker;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Rpt9;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Rpt8;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Salary;
        private System.Windows.Forms.Panel Panel_Salary;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Rpt7;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_NCPN;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Rpt13;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Rpt12;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Rpt14;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_CaculatorSuport;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Rpt15;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_StockProduct;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_CaculatorOffice;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_ChangeTeam;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_ScoreBegin;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_ScoreStock;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Rpt47;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Rpt41;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Rpt44;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Rpt43;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Rpt42;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Rpt48;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Rpt_16;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Rpt17;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Rpt18;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Compare;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Download;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Combo;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Rpt45;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Rpt_19;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_KhoiTao;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Report20;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Rpt_Mau01;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Rpt_Mau02;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_ReportCong;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Approve;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Bao_Cao_Ngay;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_EmployeeTeamView;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_OrderView;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_WorkingHistory;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_LogUser;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Diligence;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_FeeCompany;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Business;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_RiceCompany;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_LuongThang13;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_TinhLuongThang13;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_TinhThuongKD;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_THThuNhap;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_TrungBinhNgay;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_PayAtm;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Rpt_ThoiGian;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_TG_LamViecTaiXuong;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_LuongTongHop_ChiTiet;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Worker;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Support;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Office;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_LuongTongHopHoTro_ChiTiet;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_LuongTongHopSanXuat_ChiTiet;
        private System.Windows.Forms.Label label9;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_HMList;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_UngLuong;
        private System.Windows.Forms.Label label11;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_VuonUom;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_ThachDua;
        private TN_Tools.TNDateTime dte_WorkDate;
        private System.Windows.Forms.Panel Panel_VuonUom;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_VuonUom_Approve_Customer;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_VuonUom_Input;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_VuonUom_Output_Approve;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_VuonUom_Output;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_VuonUom_Input_Approve;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_VuonUom_Customer;
        private System.Windows.Forms.Panel Panel_ThachDua;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_ThachDua_Approve_Customer;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_ThachDua_Input;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_ThachDua_Output_Approve;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_ThachDua_Output;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_ThachDua_Input_Approve;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_ThachDua_Customer;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_BangKeNhapKho;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_TheKho;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_BangKeXuatKho;
        private System.Windows.Forms.Label label10;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Info;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_ThanhPham;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_SLDHChuaChiaLai;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_PhieuLuong;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_TBLK_TaiTo;
        private System.Windows.Forms.Label txt_ChayChu;
        private System.Windows.Forms.Timer timer1;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_SL_TaiBP;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_KhoanChuyenCan;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Report23;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_CC_ChiTiet;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_PBChucVu;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_TongLuongKhoan_SP;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Report25;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Lock;
    }
}