﻿using ComponentFactory.Krypton.Toolkit;
using System;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Net;
using System.Windows.Forms;
using TNS.IVT;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Main : Form
    {
        private string _KQ = "";
        #region ----- Biến toàn cục form -----
        private int _Exit = 0;
        Frm_Stock_Output _Frm_Stock_Ouput = new Frm_Stock_Output();
        Frm_Stock_Input _Frm_Stock_Input = new Frm_Stock_Input();
        Frm_Stock_Local_Input _Frm_Stock_Local_Input = new Frm_Stock_Local_Input();
        Frm_Stock_Local_Output _Frm_Stock_Local_Output = new Frm_Stock_Local_Output();

        //Frm_Production_Record _Frm_RecordProduction = new Frm_Production_Record();

        //Frm_Employee_Contract _Frm_ContactEmployee = new Frm_Employee_Contract();
        Frm_Receipt _Frm_Receipt_Detail = new Frm_Receipt();
        Frm_Payment _Frm_Payment_Detail = new Frm_Payment();
        //Frm_Caculator_Working _Frm_Caculator_Working = new Frm_Caculator_Working();
        //Frm_DayOff_Support _Frm_DayOff_Support = new Frm_DayOff_Support();
        #endregion

        #region [Dùng kéo rê form]

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Normal)
            {
                this.WindowState = FormWindowState.Maximized;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
                this.StartPosition = FormStartPosition.CenterScreen;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
 
            if(Utils.TNMessageBox("Bạn có muốn thoát phần mềm !", 2) == "Y")
            {
                _Exit = 1;
                if (SessionUser.UserLogin != null && SessionUser.UserLogin.Key != "")
                {
                    User_Info zUser = new User_Info(SessionUser.UserLogin.Name);
                    zUser.UpdateIsLogin(0);// hạ cờ đăng nhập
                    Log_Login(3, "Đã đăng xuất phần mềm");
                }
                this.Close();
            }
            else
            {
                _Exit = 0;
            }
        }

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        private void Frm_Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_Exit == 0)
                e.Cancel = true;
            else
                e.Cancel = false;
            //if (MessageBox.Show("Bạn có muốn thoát phần mềm !.", "Thông báo", MessageBoxButtons.YesNo) == DialogResult.Yes)
            //{

            //    e.Cancel = false;
            //}
            //else
            //{
            //    e.Cancel = true;
            //}
        }
        #endregion

        public int _WareHouse = 0;
        public string _WareHouseRole = "";


        public Frm_Main()
        {
            InitializeComponent();
            this.DoubleBuffered = true;

            dte_WorkDate.Validated += Dte_WorkDate_ValueChanged;
            btn_Download.Click += Btn_Download_Click;
            btn_Info.Click += Btn_Info_Click;

            //---- Layout
            this.Panel_Material.Location = new System.Drawing.Point(17, 44);
            this.Panel_Freezing.Location = new System.Drawing.Point(17, 44);
            this.Panel_Temp.Location = new System.Drawing.Point(17, 44);
            this.Panel_Recycle.Location = new System.Drawing.Point(17, 44);
            this.Panel_Box.Location = new System.Drawing.Point(17, 44);
            this.Panel_ThachDua.Location = new System.Drawing.Point(17, 44);
            this.Panel_VuonUom.Location = new System.Drawing.Point(17, 44);
            this.Panel_Human.Location = new System.Drawing.Point(17, 44);
            this.Panel_Setup.Location = new System.Drawing.Point(17, 44);
            this.Panel_Production.Location = new System.Drawing.Point(17, 44);
            this.Panel_Report.Location = new System.Drawing.Point(17, 44);
            this.Panel_Salary.Location = new System.Drawing.Point(17, 44);
            //---- Menu          
            btn_Warehouse_1.Click += btn_Warehouse_1_Click;
            btn_Warehouse_2.Click += btn_Warehouse_2_Click;
            btn_Warehouse_3.Click += btn_Warehouse_3_Click;
            btn_Warehouse_4.Click += btn_Warehouse_4_Click;
            btn_Warehouse_5.Click += btn_Warehouse_5_Click;
            btn_VuonUom.Click += Btn_VuonUom_Click;
            btn_ThachDua.Click += Btn_ThachDua_Click;

            btn_Exit.Click += btn_Exit_Click;
            btn_Setup.Click += btn_Setup_Click;
            btn_Production.Click += btn_Production_Click;
            btn_Human.Click += btn_HumanResource_Click;
            btn_ChangePass.Click += btn_ChangePass_Click;
            btn_Report.Click += btn_Report_Click;
            btn_Salary.Click += btn_Salary_Click;

            //---- Nguyên Liệu
            btn_Material_Input_Customer.Click += btn_Material_Input_Customer_Click;                                 //Nhập Kho Từ K.Hàng
            btn_Material_Input_Customer_Approve.Click += btn_Material_Input_Customer_Approve_Click;     //Duyệt Nhập Kho K.Hàng
            btn_Material_Output.Click += btn_Material_Output_Local_Click;                                         //Xuất Sản xuất                                         
            btn_Material_Input.Click += btn_Material_Input_Local_Click;                                           // Nhập từ Sản xuất          
            btn_Material_Output_Approve.Click += btn_Material_Output_Approve_Click;                                 //Duyệt Phiếu Xuất - NL
            btn_Material_Input_Approve.Click += btn_Material_Input_Approve_Click;                                   //Duyệt Phiếu Nhập - NL
            //---- Cáp Đông
            btn_Freez_Input.Click += btn_Freez_Input_Click;                                                                                    //Nhập từ Sản xuất Cấp đông
            btn_Freez_Output.Click += btn_Freez_Output_Click;                                                                               //Xuất Sản xuất Cấp Đông
            btn_Freez_Output_Customer.Click += btn_Freez_Output_Customer_Click;                                                 //Xuất Cho Khách Hàng
            btn_Freez_Input_Approve.Click += btn_Freez_Input_Approve_Click;                                                             //Duyệt Phiếu Nhập - CĐ
            btn_Freez_Output_Approve.Click += btn_Freez_Output_Approve_Click;                                                       //Duyệt Phiếu Xuất - CĐ
            btn_Freez_Output_Approve_Customer.Click += btn_Approve_Cus_CĐ_Click;                                            //Duyệt Xuất  K.Hàng - CĐ
            //---- Đồ Hộp
            btn_Box_Input.Click += btn_Box_Input_Click;                                                                                              //Nhập Kho Nội Bộ -ĐH
            btn_Box_Output.Click += btn_Box_Output_Click;                                                                                                //Xuất Kho Nội Bộ -ĐH
            btn_Box_Output_Customer.Click += btn_Box_Output_Customer_Click;                                                             //Xuất Cho K.Hàng
            btn_Box_Input_Approve.Click += btn_Box_Input_Approve_Click;                                                                 //Duyệt Phiếu Nhập - ĐH
            btn_Box_Output_Approve.Click += btn_Box_Output_Approve_Click;                                                           //Duyệt Phiếu Xuất - ĐH
            btn_Box_Output_Approve_Customer.Click += btn_Box_Output_Approve_Customer_Click;                                                //Duyệt Xuất K.Hàng ĐH
            //---- Kho Tạm
            btn_Temp_Input.Click += btn_Temp_Input_Click;
            btn_Temp_Output.Click += btn_Temp_Output_Click;
            btn_Temp_Input_Approve.Click += btn_Temp_Input_Approve_Click;
            btn_Temp_Output_Approve.Click += btn_Temp_Output_Approve_Click;
            //---- Kho Hủy
            btn_Recycle_Input_Customer.Click += btn_Recycle_Input_Customer_Click;                                                                       //Nhập Kho Khách Hàng
            btn_Recycle_Input.Click += btn_Recycle_Input_Click;                                                                                                     //Nhập Kho Nội Bộ -KH
            btn_Recycle_Output.Click += btn_Recycle_Output_Click;                                                                                           //Xuất Kho Nội Bộ -KH
            btn_Recycle_Input_Approve_Customer.Click += btn_Recycle_Input_Approve_Customer_Click;                                               //Duyệt Phiếu Nhập Khach hàng - KH
            btn_Recycle_Ouput_Approve.Click += btn_Recycle_Ouput_Approve_Click;                                                                      //Duyệt Phiếu Xuất - KH
            btn_Recycle_Input_Approve.Click += btn_Recycle_Input_Approve_Click;
            //----Kho thạch dừa
            btn_ThachDua_Input.Click += Btn_ThachDua_Input_Click;
            btn_ThachDua_Output.Click += Btn_ThachDua_Output_Click;
            btn_ThachDua_Customer.Click += Btn_ThachDua_Customer_Click;
            btn_ThachDua_Input_Approve.Click += Btn_ThachDua_Input_Approve_Click;
            btn_ThachDua_Output_Approve.Click += Btn_ThachDua_Output_Approve_Click;
            btn_ThachDua_Approve_Customer.Click += Btn_ThachDua_Approve_Customer_Click;
            //---Kho vườn ươm
            btn_VuonUom_Input.Click += Btn_VuonUom_Input_Click;
            btn_VuonUom_Output.Click += Btn_VuonUom_Output_Click;
            btn_VuonUom_Customer.Click += Btn_VuonUom_Customer_Click;
            btn_VuonUom_Input_Approve.Click += Btn_VuonUom_Input_Approve_Click;
            btn_VuonUom_Output_Approve.Click += Btn_VuonUom_Output_Approve_Click;
            btn_VuonUom_Approve_Customer.Click += Btn_VuonUom_Approve_Customer_Click;

            //---- Ghi Năng Suất
            btn_Production_Note.Click += btn_Production_Note_Click;
            btn_Production_Calculator.Click += btn_Production_Calculator_Click;
            btn_Production_ReCalculator.Click += btn_Production_Adjusted_Click;
            btn_Production_ReCalculator2.Click += btn_Production_ReCalculator2_Click;
            btn_Production_Payroll.Click += btn_Production_Money_Click;
            btn_Production_Time.Click += btn_Production_Time_Click;
            btn_Production_Auto_Calculator.Click += btn_Production_Auto_Calculator_Click;
            btn_Production_Auto_Money.Click += btn_Production_Auto_Money_Click;
            btn_Production_Calculator_V2.Click += btn_Production_Calculator_V2_Click;
            btn_Import_Record.Click += btn_Import_Record_Click;
            btn_Import_Production_Time.Click += btn_Import_Production_Time_Click;
            btn_Compare.Click += Btn_Compare_Click;
            btn_OrderView.Click += Btn_OrderView_Click;
            //---- Nhân Sự
            btn_Employee.Click += btn_Employee_Click;
            btn_Contract.Click += btn_Contract_Click;
            btn_Payment.Click += btn_Payment_Click;
            btn_Receipt.Click += btn_Receipt_Click;
            btn_TimeKeeping.Click += btn_TimeKeeping_Click;
            btn_Caculator_Keeping.Click += btn_Caculator_Keeping_Click;
            btn_DayOffHoliday.Click += btn_DayOffHoliday_Click;
            btn_Report_6.Click += Btn_Report_6_Click;
            btn_FeeDefault.Click += Btn_FeeDefault_Click;
            btn_Support_Leader.Click += Btn_Support_Leader_Click;
            btn_Standard.Click += Btn_Standard_Click;
            btn_FeeChildren.Click += Btn_FeeChildren_Click;
            btn_SalaryDefault.Click += Btn_SalaryDefault_Click;
            btn_CaculatorWorker.Click += Btn_CaculatorWorker_Click;
            btn_NCPN.Click += Btn_NCPN_Click;
            btn_ChangeTeam.Click += Btn_ChangeTeam_Click;
            btn_ScoreBegin.Click += Btn_ScoreBorrow_Click;
            btn_KhoiTao.Click += Btn_KhoiTao_Click;
            btn_Approve.Click += Btn_Approve_Click;
            btn_EmployeeTeamView.Click += Btn_EmployeeTeamView_Click;
            btn_WorkingHistory.Click += Btn_WorkingHistory_Click;
            btn_Diligence.Click += Btn_Diligence_Click;
            btn_TinhThuongKD.Click += Btn_TinhThuongKD_Click;
            btn_TinhLuongThang13.Click += Btn_TinhLuongThang13_Click;
            btn_LuongThang13.Click += Btn_LuongThang13_Click;
            btn_Business.Click += Btn_Business_Click;
            btn_RiceCompany.Click += Btn_RiceCompany_Click;
            btn_HMList.Click += Btn_HMList_Click;
            btn_PBChucVu.Click += Btn_PBChucVu_Click;
            //---- Setup
            btn_Role.Click += btn_Role_Click;
            btn_Users.Click += BtnRoleUsers_Click;
            btn_Customer_Individual.Click += btn_Customer_Click;
            btn_Customer_Company.Click += btn_Customer_Company_Click;
            btn_Setup_Coefficent.Click += btn_Setup_Order_Rate_Click;
            btn_Branch.Click += btn_Setup_Branch_Click;
            btn_Department.Click += btn_Setup_Department_Click;
            btn_Position.Click += btn_Setup_Position_Click;
            btn_Team.Click += btn_Setup_Team_Click;
            btn_Product.Click += btn_Setup_Product_Click;
            btn_Paramaters.Click += btn_Setup_Parameter_Click;
            btn_Time_Defines.Click += btn_Setup_Working_Time_Click;
            btn_Setup_Rice.Click += btn_Time_Rice_Click;
            btn_Holiday.Click += btn_Setup_Time_Holiday_Click;
            btn_Fee.Click += btn_Category_Fee_Click;
            btn_Setup_Unit.Click += btn_Setup_Unit_Click;
            btn_Convert_Unit.Click += btn_Setup_Unit_Convert_Click;
            btn_Stages.Click += btn_Setup_Work;
            btn_Time_Work.Click += btn_Shift_Work_Click;
            btn_ScoreStock.Click += Btn_ScoreStock_Click;
            btn_Combo.Click += Btn_Combo_Click;
            btn_LogUser.Click += Btn_LogUser_Click;
            btn_ThanhPham.Click += Btn_ThanhPham_Click;
            btn_KhoanChuyenCan.Click += Btn_KhoanChuyenCan_Click;
            btn_Lock.Click += Btn_Lock_Click;
            //----Reoport
            btn_Rpt1.Click += btn_Rpt1_Click;
            btn_Rpt2.Click += btn_Rpt2_Click;
            btn_Rpt3.Click += btn_Rpt3_Click;
            btn_Rpt4.Click += btn_Rpt4_Click;

            btn_Rpt8.Click += Btn_Rpt8_Click;
            btn_Rpt9.Click += Btn_Rpt9_Click;
            btn_Rpt12.Click += Btn_Rpt12_Click;
            btn_Rpt13.Click += Btn_Rpt13_Click;
            btn_StockProduct.Click += Btn_StockProduct_Click;
            btn_Rpt41.Click += Btn_Rpt41_Click;
            btn_Rpt42.Click += Btn_Rpt42_Click;
            btn_Rpt43.Click += Btn_Rpt43_Click;
            btn_Rpt45.Click += Btn_Rpt45_Click;
            btn_Rpt44.Click += Btn_Rpt44_Click;
            btn_Rpt47.Click += Btn_Rpt47_Click;
            btn_Rpt48.Click += Btn_Rpt48_Click;
            btn_Rpt_16.Click += Btn_Rpt_16_Click;
            btn_Rpt17.Click += Btn_Rpt17_Click;
            btn_Rpt18.Click += Btn_Rpt18_Click;
            btn_Rpt_19.Click += Btn_Rpt_19_Click;
            btn_Report20.Click += Btn_Report20_Click;
            btn_Rpt_Mau01.Click += Btn_Rpt_Mau01_Click;
            btn_Rpt_Mau02.Click += Btn_Rpt_Mau02_Click;
            btn_ReportCong.Click += Btn_ReportCong_Click;
            btn_Bao_Cao_Ngay.Click += Btn_Bao_Cao_Ngay_Click;
            btn_Adjusted.Click += Btn_Adjusted_Click;
            btn_THThuNhap.Click += Btn_THThuNhap_Click;
            btn_TrungBinhNgay.Click += Btn_TrungBinhNgay_Click;
            btn_Rpt_ThoiGian.Click += Btn_Rpt_ThoiGian_Click;
            btn_TG_LamViecTaiXuong.Click += Btn_TG_LamViecTaiXuong_Click;
            btn_Worker.Click += Btn_Worker_Click;
            btn_Support.Click += Btn_Support_Click;
            btn_Office.Click += Btn_Office_Click;
            btn_UngLuong.Click += Btn_UngLuong_Click;
            btn_BangKeNhapKho.Click += Btn_BangKeNhapKho_Click;
            btn_BangKeXuatKho.Click += Btn_BangKeXuatKho_Click;
            btn_TheKho.Click += Btn_TheKho_Click;
            btn_SLDHChuaChiaLai.Click += Btn_SLDHChuaChiaLai_Click;
            btn_PhieuLuong.Click += Btn_PhieuLuong_Click;
            btn_TBLK_TaiTo.Click += Btn_TBLK_TaiTo_Click;
            btn_SL_TaiBP.Click += Btn_SL_TaiBP_Click;
            btn_Report23.Click += Btn_Report23_Click;
            btn_CC_ChiTiet.Click += Btn_CC_ChiTiet_Click;
            btn_TongLuongKhoan_SP.Click += Btn_TongLuongKhoan_SP_Click;
            btn_Report25.Click += Btn_Report25_Click;

            //----Salary
            btn_Rpt7.Click += Btn_Rpt7_Click;
            btn_Rpt14.Click += Btn_Rpt14_Click;
            btn_CaculatorSuport.Click += Btn_CaculatorSuport_Click;
            btn_Rpt15.Click += Btn_Rpt15_Click;
            btn_CaculatorOffice.Click += Btn_CaculatorOffice_Click;
            btn_FeeCompany.Click += Btn_FeeCompany_Click;
            btn_PayAtm.Click += Btn_PayAtm_Click;
            btn_LuongTongHop_ChiTiet.Click += Btn_LuongTongHop_ChiTiet_Click;
            btn_LuongTongHopHoTro_ChiTiet.Click += Btn_LuongTongHopHoTro_ChiTiet_Click;
            btn_LuongTongHopSanXuat_ChiTiet.Click += Btn_LuongTongHopSanXuat_ChiTiet_Click;
        }

       

        private void Btn_LogUser_Click(object sender, EventArgs e)
        {
            //if (SessionUser.UserLogin.Key == "4e5a9e53-9241-4c9b-a69f-e86a7ae3507f" || SessionUser.UserLogin.Key =="bb075522-d012-46d5-a2df-872411da7889")
                OpenForm("Frm_UserLog", false, btn_LogUser.Tag);
            //else
            //    MessageBox.Show("Bạn chưa được phân quyền vào form này!");
           
        }

        private void Btn_OrderView_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_OrderView", false, btn_OrderView.Tag);
        }

        private void Btn_EmployeeTeamView_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_EmployeeTeamOfficeView02", false, btn_EmployeeTeamView.Tag);
        }

        private void Btn_Download_Click(object sender, EventArgs e)
        {
            Frm_FileTemplate frm = new Frm_FileTemplate();
            frm.ShowDialog();

        }
        private void Btn_Info_Click(object sender, EventArgs e)
        {
            Frm_ReadFile frm = new Frm_ReadFile();
            frm.FilePath = Application.StartupPath + @"\FilePdf\HUONG_DAN_SU_DUNG_PHAN_MEM.pdf";
            frm.Show();
        }
        private void Btn_Compare_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Production_Compare", false, btn_Compare.Tag);
        }

        private void Btn_ScoreStock_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_ScoreBorrow", false, btn_ScoreStock.Tag);
        }
        private void Btn_Approve_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_ScoreApprove", false, btn_Approve.Tag);
        }
        private void Btn_ScoreBorrow_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_ScoreStockBegin", false, btn_ScoreBegin.Tag);
        }

        private void Btn_StockProduct_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Product_Stock", false, btn_StockProduct.Tag);
        }

        private void btn_Production_ReCalculator2_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Adjust2V2", false, btn_Production_ReCalculator2.Tag);
        }

        private void btn_Import_Production_Time_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Production_Time_V2", false, btn_Import_Production_Time.Tag);
        }

        private void btn_Rpt4_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Report_4_V2", false, btn_Rpt4.Tag);
        }

        private void btn_Import_Record_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Import_Order_V2", false, btn_Import_Record.Tag);
        }
        private void Btn_FeeCompany_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_FeeCompany", false, btn_FeeCompany.Tag);
        }
       

        private void Frm_Main_Load(object sender, EventArgs e)
        {

            dte_WorkDate.Value = DateTime.Now;
            Frm_Login frm = new Frm_Login();
            frm.ShowDialog();
            //timer1.Start();
            if (SessionUser.Personal_Info != null)
            {
                lbl_EmployeeName.Text = "Nhân viên : " + SessionUser.Personal_Info.FullName;
                CheckRole_SubMenu();
            }
            CheckRole();

            this.Bounds = Screen.PrimaryScreen.WorkingArea;
        }

        #region ----- Panel Left -----

        #region ----- btn_Warehouse_1 -----
        private void btn_Warehouse_1_Click(object sender, EventArgs e)
        {
            txtTitle.Text = btn_Warehouse_1.Text;

            #region [Groubox enable]
            Panel_Material.Visible = true;
            Panel_Setup.Visible = false;
            Panel_Freezing.Visible = false;
            Panel_Box.Visible = false;
            Panel_Temp.Visible = false;
            Panel_Production.Visible = false;
            Panel_Recycle.Visible = false;
            Panel_Human.Visible = false;
            Panel_Report.Visible = false;
            Panel_Salary.Visible = false;
            Panel_ThachDua.Visible = false;
            Panel_VuonUom.Visible = false;
            #endregion

            _WareHouseRole = btn_Warehouse_1.Tag.ToString();
            _WareHouse = 1;// new Warehouse_Info(btn_Warehouse_1.Tag.ToString()).WarehouseKey;

            //btn_Material_Input_Customer.Text = "Nhập Kho";
            //btn_Material_Output.Text = "Xuất kho\r\nnội bộ";
            //btn_Material_Input.Text = "Nhập kho\r\nnội bộ";

            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, "Truy cập menu Kho nguyên liệu", 1);
        }
        #endregion

        #region ----- btn_Warehouse_2 -----
        private void btn_Warehouse_2_Click(object sender, EventArgs e)
        {
            txtTitle.Text = btn_Warehouse_2.Text;

            #region [Groubox enable]
            Panel_Material.Visible = false;
            Panel_Setup.Visible = false;
            Panel_Freezing.Visible = true;
            Panel_Box.Visible = false;
            Panel_Recycle.Visible = false;
            Panel_Production.Visible = false;
            Panel_Temp.Visible = false;
            Panel_Human.Visible = false;
            Panel_Report.Visible = false;
            Panel_Salary.Visible = false;
            Panel_ThachDua.Visible = false;
            Panel_VuonUom.Visible = false;
            #endregion

            _WareHouseRole = btn_Warehouse_2.Tag.ToString();
            _WareHouse = 4; // new Warehouse_Info(btn_Warehouse_2.Tag.ToString()).WarehouseKey;

            //btn_Freez_Input.Text = "Nhập kho\r\nnội bộ-CĐ";
            //btn_Freez_Output.Text = "Xuất kho\r\nnội bộ-CĐ";
            //btn_Freez_Output_Customer.Text = "Xuất\r\nk.hàng";

            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, "Truy cập menu Kho cấp đông", 1);
        }
        #endregion

        #region ----- btn_WareHouse_3 -----
        private void btn_Warehouse_3_Click(object sender, EventArgs e)
        {
            txtTitle.Text = btn_Warehouse_3.Text;

            #region [Groubox enable]
            Panel_Material.Visible = false;
            Panel_Setup.Visible = false;
            Panel_Freezing.Visible = false;
            Panel_Box.Visible = true;
            Panel_Recycle.Visible = false;
            Panel_Production.Visible = false;
            Panel_Temp.Visible = false;
            Panel_Human.Visible = false;
            Panel_Report.Visible = false;
            Panel_Salary.Visible = false;
            Panel_ThachDua.Visible = false;
            Panel_VuonUom.Visible = false;
            #endregion
            _WareHouseRole = btn_Warehouse_3.Tag.ToString();
            _WareHouse = 5;// new Warehouse_Info(btn_Warehouse_3.Tag.ToString()).WarehouseKey;

            //btn_Box_Input.Text = "Nhập kho\r\nnội nộ-ĐH";
            //btn_Box_Output.Text = "Xuất kho\r\nnội bộ-ĐH";
            //btn_Box_Output_Customer.Text = "Xuất\r\nk.hàng";
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, "Truy cập menu Kho đồ hộp", 1);
        }
        #endregion

        #region ----- btn_WareHouse_4 -----
        private void btn_Warehouse_4_Click(object sender, EventArgs e)
        {
            txtTitle.Text = btn_Warehouse_4.Text;

            #region [Groubox enable]
            Panel_Material.Visible = false;
            Panel_Setup.Visible = false;
            Panel_Freezing.Visible = false;
            Panel_Box.Visible = false;
            Panel_Recycle.Visible = false;
            Panel_Production.Visible = false;
            Panel_Temp.Visible = true;
            Panel_Human.Visible = false;
            Panel_Report.Visible = false;
            Panel_Salary.Visible = false;
            Panel_ThachDua.Visible = false;
            Panel_VuonUom.Visible = false;
            #endregion
            _WareHouseRole = btn_Warehouse_4.Tag.ToString();
            _WareHouse = 6;// new Warehouse_Info(btn_Warehouse_4.Tag.ToString()).WarehouseKey;

            //btn_Temp_Input.Text = "Nhập kho\r\nnội bộ-KT";
            //btn_Temp_Output.Text = "Xuất kho\r\nnội bộ-KT";

            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, "Truy cập menu Kho tạm", 1);
        }
        #endregion

        #region ----- btn_WareHouse_5 -----
        private void btn_Warehouse_5_Click(object sender, EventArgs e)
        {
            txtTitle.Text = btn_Warehouse_5.Text;

            #region [Groubox enable]
            Panel_Material.Visible = false;
            Panel_Setup.Visible = false;
            Panel_Freezing.Visible = false;
            Panel_Box.Visible = false;
            Panel_Recycle.Visible = true;
            Panel_Production.Visible = false;
            Panel_Temp.Visible = false;
            Panel_Human.Visible = false;
            Panel_Report.Visible = false;
            Panel_Salary.Visible = false;
            Panel_ThachDua.Visible = false;
            Panel_VuonUom.Visible = false;
            #endregion
            _WareHouseRole = btn_Warehouse_5.Tag.ToString();
            _WareHouse = 9;// new Warehouse_Info(btn_Warehouse_5.Tag.ToString()).WarehouseKey;

            //btn_Recycle_Input.Text = "Nhập kho\r\nnội bộ-KH";
            //btn_Recycle_Output.Text = "Xuất kho\r\nnội bộ-KH";
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, "Truy cập menu Kho hủy", 1);

        }
        #endregion

        #region[----Kho thach dua----]
        private void Btn_ThachDua_Click(object sender, EventArgs e)
        {
            txtTitle.Text = btn_ThachDua.Text;

            #region [Groubox enable]
            Panel_Material.Visible = false;
            Panel_Setup.Visible = false;
            Panel_Freezing.Visible = false;
            Panel_Box.Visible = false;
            Panel_Recycle.Visible = false;
            Panel_Production.Visible = false;
            Panel_Temp.Visible = false;
            Panel_Human.Visible = false;
            Panel_Report.Visible = false;
            Panel_Salary.Visible = false;
            Panel_ThachDua.Visible = true;
            Panel_VuonUom.Visible = false;
            #endregion

            _WareHouseRole = btn_ThachDua.Tag.ToString();
            _WareHouse = 7; //new Warehouse_Info(btn_ThachDua.Tag.ToString()).WarehouseKey;

            //btn_ThachDua_Input.Text = "Nhập kho\r\nnội bộ-CĐ";
            //btn_ThachDua_Output.Text = "Xuất kho\r\nnội bộ-CĐ";
            //btn_ThachDua_Customer.Text = "Xuất\r\nk.hàng";

            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, "Truy cập menu Kho thạch dừa", 1);
        }
        #endregion

        #region[----Kho vườn ươm-----]
        private void Btn_VuonUom_Click(object sender, EventArgs e)
        {
            txtTitle.Text = btn_VuonUom.Text;

            #region [Groubox enable]
            Panel_Material.Visible = false;
            Panel_Setup.Visible = false;
            Panel_Freezing.Visible = false;
            Panel_Box.Visible = false;
            Panel_Recycle.Visible = false;
            Panel_Production.Visible = false;
            Panel_Temp.Visible = false;
            Panel_Human.Visible = false;
            Panel_Report.Visible = false;
            Panel_Salary.Visible = false;
            Panel_ThachDua.Visible = false;
            Panel_VuonUom.Visible = true;
            #endregion

            _WareHouseRole = btn_VuonUom.Tag.ToString();
            _WareHouse = 11;//new Warehouse_Info(btn_VuonUom.Tag.ToString()).WarehouseKey;

            //btn_VuonUom_Input.Text = "Nhập kho\r\nnội bộ-CĐ";
            //btn_VuonUom_Output.Text = "Xuất kho\r\nnội bộ-CĐ";
            //btn_VuonUom_Customer.Text = "Xuất\r\nk.hàng";

            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, "Truy cập menu Kho vườn ươm", 1);
        }
        #endregion

        #region ---- btn_Production ----
        private void btn_Production_Click(object sender, EventArgs e)
        {
            txtTitle.Text = btn_Production.Text;

            #region [Groubox enable]
            Panel_Material.Visible = false;
            Panel_Setup.Visible = false;
            Panel_Freezing.Visible = false;
            Panel_Box.Visible = false;
            Panel_Recycle.Visible = false;
            Panel_Production.Visible = true;
            Panel_Temp.Visible = false;
            Panel_Human.Visible = false;
            Panel_Report.Visible = false;
            Panel_Salary.Visible = false;
            Panel_ThachDua.Visible = false;
            Panel_VuonUom.Visible = false;
            #endregion

            _WareHouseRole = btn_Production.Tag.ToString();
            _WareHouse = new Warehouse_Info(btn_Production.Tag.ToString()).WarehouseKey;
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, "Truy cập menu Năng suất", 1);
        }

        #endregion

        #region ----- btn_HumanResource -----
        private void btn_HumanResource_Click(object sender, EventArgs e)
        {
            #region [Groubox enable]
            Panel_Material.Visible = false;
            Panel_Setup.Visible = false;
            Panel_Freezing.Visible = false;
            Panel_Box.Visible = false;
            Panel_Recycle.Visible = false;
            Panel_Production.Visible = false;
            Panel_Temp.Visible = false;
            Panel_Human.Visible = true;
            Panel_Report.Visible = false;
            Panel_Salary.Visible = false;
            Panel_ThachDua.Visible = false;
            Panel_VuonUom.Visible = false;
            #endregion
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, "Truy cập menu Nhân sự-Chấm công", 1);
        }
        #endregion

        #region ----- btn_Setup ------ 
        private void btn_Setup_Click(object sender, EventArgs e)
        {
            txtTitle.Text = btn_Setup.Text;

            #region [Groubox enable]
            Panel_Material.Visible = false;
            Panel_Setup.Visible = true;
            Panel_Freezing.Visible = false;
            Panel_Box.Visible = false;
            Panel_Recycle.Visible = false;
            Panel_Production.Visible = false;
            Panel_Temp.Visible = false;
            Panel_Human.Visible = false;
            Panel_Report.Visible = false;
            Panel_Salary.Visible = false;
            Panel_ThachDua.Visible = false;
            Panel_VuonUom.Visible = false;
            #endregion
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, "Truy cập menu Cài đặt", 1);
        }

        private void BtnRoleUsers_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_User", false, btn_Users.Tag);
        }
        private void btn_Role_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Access", false, btn_Role.Tag);
        }
        #endregion

        #region ----- btn_Report -----
        private void btn_Report_Click(object sender, EventArgs e)
        {
            txtTitle.Text = btn_Report.Text;

            #region [Groubox enable]
            Panel_Material.Visible = false;
            Panel_Setup.Visible = false;
            Panel_Freezing.Visible = false;
            Panel_Box.Visible = false;
            Panel_Recycle.Visible = false;
            Panel_Production.Visible = false;
            Panel_Temp.Visible = false;
            Panel_Human.Visible = false;
            Panel_Report.Visible = true;
            Panel_Salary.Visible = false;
            Panel_ThachDua.Visible = false;
            Panel_VuonUom.Visible = false;
            #endregion

            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, "Truy cập menu Báo cáo", 1);
        }
        #endregion

        #region----btn_Salary-----
        private void btn_Salary_Click(object sender, EventArgs e)
        {
            txtTitle.Text = btn_Salary.Text;

            #region [Groubox enable]
            Panel_Material.Visible = false;
            Panel_Setup.Visible = false;
            Panel_Freezing.Visible = false;
            Panel_Box.Visible = false;
            Panel_Recycle.Visible = false;
            Panel_Production.Visible = false;
            Panel_Temp.Visible = false;
            Panel_Human.Visible = false;
            Panel_Report.Visible = false;
            Panel_Salary.Visible = true;
            Panel_ThachDua.Visible = false;
            Panel_VuonUom.Visible = false;
            #endregion
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, "Truy cập menu Tính lương", 1);
        }

        #endregion
        #endregion

        #region ----- Panel Right -----

        #region ---- group Năng Suất -----
        private void btn_Production_Calculator_V2_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Production_Calculator_V3", false, btn_Production_Calculator_V2.Tag);
        }
        private void btn_Production_Time_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Production_Time_V2", false, btn_Production_Time.Tag);
        }
        private void btn_Production_Note_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Production_Order_V2", false, btn_Production_Note.Tag);
        }
        //tính tay
        private void btn_Production_Calculator_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Production_Calculator", false, btn_Production_Calculator.Tag);
        }
        //tính tay
        private void btn_Production_Money_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Production_Money", false, btn_Production_Payroll.Tag);
        }
        //tính auto 
        private void btn_Production_Auto_Calculator_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Calculator_Tracking", true, btn_Production_Auto_Calculator.Tag);
        }
        //tính auto
        private void btn_Production_Auto_Money_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Calculator_Money", false, btn_Production_Auto_Money.Tag);
        }
        private void btn_Production_Adjusted_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_AdjustV2", false, btn_Production_ReCalculator2.Tag);
        }
        private void Btn_KhoanChuyenCan_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_KhoanChuyenCan", false, btn_KhoanChuyenCan.Tag);
        }
        #endregion


        #region  ----- group Cáp Đông -----
        private void btn_Freez_Input_Click(object sender, EventArgs e)
        {
            string _RoleID = btn_Freez_Input.Tag.ToString();
            int i = User_Data.GetUserRoleRead(SessionUser.UserLogin.Key, btn_Freez_Input.Tag.ToString());
            if (i > 0)
            {
                if (_Frm_Stock_Local_Input.IsDisposed)
                    _Frm_Stock_Local_Input = new Frm_Stock_Local_Input();
                if (!_Frm_Stock_Local_Input.Visible)
                {
                    _Frm_Stock_Local_Input.WareHouse = _WareHouse;
                    _Frm_Stock_Local_Input.WareHouseRole = _WareHouseRole;
                    _Frm_Stock_Local_Input.StyleForm = 1;
                    _Frm_Stock_Local_Input.Status = 1;
                    _Frm_Stock_Local_Input.RoleID = _RoleID;
                    _Frm_Stock_Local_Input.Show();
                }
                else
                {
                    _Frm_Stock_Local_Input.BringToFront();
                }
            }
            else
            {
                MessageBox.Show("Bạn không được xem phần này !", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void btn_Freez_Output_Click(object sender, EventArgs e)
        {
            string _RoleID = btn_Freez_Output.Tag.ToString();
            int i = User_Data.GetUserRoleRead(SessionUser.UserLogin.Key, btn_Freez_Output.Tag.ToString());
            if (i > 0)
            {
                if (_Frm_Stock_Local_Output.IsDisposed)
                    _Frm_Stock_Local_Output = new Frm_Stock_Local_Output();
                if (!_Frm_Stock_Local_Output.Visible)
                {
                    _Frm_Stock_Local_Output.WareHouse = _WareHouse;
                    _Frm_Stock_Local_Output.WarehouseRole = _WareHouseRole;
                    _Frm_Stock_Local_Output.StyleForm = 1;
                    _Frm_Stock_Local_Output.Status = 1;
                    _Frm_Stock_Local_Output.RoleID = _RoleID;
                    _Frm_Stock_Local_Output.Show();
                }
                else
                {
                    _Frm_Stock_Local_Output.BringToFront();
                }
            }
            else
            {
                MessageBox.Show("Bạn không được xem phần này !", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void btn_Freez_Output_Customer_Click(object sender, EventArgs e)
        {
            string _RoleID = btn_Freez_Output_Customer.Tag.ToString();
            int i = User_Data.GetUserRoleRead(SessionUser.UserLogin.Key, btn_Freez_Output_Customer.Tag.ToString());
            if (i > 0)
            {
                if (_Frm_Stock_Ouput.IsDisposed)
                    _Frm_Stock_Ouput = new Frm_Stock_Output();
                if (!_Frm_Stock_Ouput.Visible)
                {
                    _Frm_Stock_Ouput.WareHouse = _WareHouse;
                    _Frm_Stock_Ouput.WareHouseRole = _WareHouseRole;
                    _Frm_Stock_Ouput.StyleForm = 1;
                    _Frm_Stock_Ouput.Status = 1;
                    _Frm_Stock_Ouput.RoleID = _RoleID;
                    _Frm_Stock_Ouput.Show();
                }
                else
                {
                    _Frm_Stock_Ouput.BringToFront();
                }
            }
            else
            {
                MessageBox.Show("Bạn không được xem phần này !", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void btn_Freez_Output_Approve_Click(object sender, EventArgs e)
        {
            string _RoleID = btn_Freez_Output_Approve.Tag.ToString();
            int i = User_Data.GetUserRoleRead(SessionUser.UserLogin.Key, btn_Freez_Output_Approve.Tag.ToString());
            if (i > 0)
            {
                if (_Frm_Stock_Local_Output.IsDisposed)
                    _Frm_Stock_Local_Output = new Frm_Stock_Local_Output();
                if (!_Frm_Stock_Local_Output.Visible)
                {
                    _Frm_Stock_Local_Output.WareHouse = _WareHouse;
                    _Frm_Stock_Local_Output.WarehouseRole = _WareHouseRole;
                    _Frm_Stock_Local_Output.StyleForm = 2;
                    _Frm_Stock_Local_Output.Status = 2;
                    _Frm_Stock_Local_Output.RoleID = _RoleID;
                    _Frm_Stock_Local_Output.Show();
                }
                else
                {
                    _Frm_Stock_Local_Output.BringToFront();
                }
            }
            else
            {
                MessageBox.Show("Bạn không được xem phần này !", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void btn_Freez_Input_Approve_Click(object sender, EventArgs e)
        {
            string _RoleID = btn_Freez_Input_Approve.Tag.ToString();
            int i = User_Data.GetUserRoleRead(SessionUser.UserLogin.Key, btn_Freez_Input_Approve.Tag.ToString());
            if (i > 0)
            {
                if (_Frm_Stock_Local_Input.IsDisposed)
                    _Frm_Stock_Local_Input = new Frm_Stock_Local_Input();
                if (!_Frm_Stock_Local_Input.Visible)
                {
                    _Frm_Stock_Local_Input.WareHouse = _WareHouse;
                    _Frm_Stock_Local_Input.WareHouseRole = _WareHouseRole;
                    _Frm_Stock_Local_Input.StyleForm = 2;
                    _Frm_Stock_Local_Input.Status = 2;
                    _Frm_Stock_Local_Input.RoleID = _RoleID;
                    _Frm_Stock_Local_Input.Show();
                }
                else
                {
                    _Frm_Stock_Local_Input.BringToFront();
                }
            }
            else
            {
                MessageBox.Show("Bạn không được xem phần này !", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void btn_Approve_Cus_CĐ_Click(object sender, EventArgs e)
        {
            string _RoleID = btn_Freez_Output_Approve_Customer.Tag.ToString();
            int i = User_Data.GetUserRoleRead(SessionUser.UserLogin.Key, btn_Freez_Output_Approve_Customer.Tag.ToString());
            if (i > 0)
            {
                if (_Frm_Stock_Ouput.IsDisposed)
                    _Frm_Stock_Ouput = new Frm_Stock_Output();
                if (!_Frm_Stock_Ouput.Visible)
                {
                    _Frm_Stock_Ouput.WareHouse = _WareHouse;
                    _Frm_Stock_Ouput.WareHouseRole = _WareHouseRole;
                    _Frm_Stock_Ouput.StyleForm = 2;
                    _Frm_Stock_Ouput.Status = 2;
                    _Frm_Stock_Ouput.RoleID = _RoleID;
                    _Frm_Stock_Ouput.Show();
                }
                else
                {
                    _Frm_Stock_Ouput.BringToFront();
                }
            }
            else
            {
                MessageBox.Show("Bạn không được xem phần này !", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        #endregion     

        #region ----- group Đồ Hộp -----
        private void btn_Box_Output_Click(object sender, EventArgs e)
        {
            string _RoleID = btn_Box_Output.Tag.ToString();
            int i = User_Data.GetUserRoleRead(SessionUser.UserLogin.Key, btn_Box_Output.Tag.ToString());
            if (i > 0)
            {
                if (_Frm_Stock_Local_Output.IsDisposed)
                    _Frm_Stock_Local_Output = new Frm_Stock_Local_Output();
                if (!_Frm_Stock_Local_Output.Visible)
                {
                    _Frm_Stock_Local_Output.WareHouse = _WareHouse;
                    _Frm_Stock_Local_Output.WarehouseRole = _WareHouseRole;
                    _Frm_Stock_Local_Output.StyleForm = 1;
                    _Frm_Stock_Local_Output.Status = 1;
                    _Frm_Stock_Local_Output.RoleID = _RoleID;
                    _Frm_Stock_Local_Output.Show();
                }
                else
                {
                    _Frm_Stock_Local_Output.BringToFront();
                }
            }
            else
            {
                MessageBox.Show("Bạn không được xem phần này !", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void btn_Box_Input_Click(object sender, EventArgs e)
        {
            string _RoleID = btn_Box_Input.Tag.ToString();
            int i = User_Data.GetUserRoleRead(SessionUser.UserLogin.Key, btn_Box_Input.Tag.ToString());
            if (i > 0)
            {
                if (_Frm_Stock_Local_Input.IsDisposed)
                    _Frm_Stock_Local_Input = new Frm_Stock_Local_Input();
                if (!_Frm_Stock_Local_Input.Visible)
                {
                    _Frm_Stock_Local_Input.WareHouse = _WareHouse;
                    _Frm_Stock_Local_Input.WareHouseRole = _WareHouseRole;
                    _Frm_Stock_Local_Input.StyleForm = 1;
                    _Frm_Stock_Local_Input.Status = 1;
                    _Frm_Stock_Local_Input.RoleID = _RoleID;
                    _Frm_Stock_Local_Input.Show();
                }
                else
                {
                    _Frm_Stock_Local_Input.BringToFront();
                }
            }
            else
            {
                MessageBox.Show("Bạn không được xem phần này !", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void btn_Box_Output_Customer_Click(object sender, EventArgs e)
        {
            string _RoleID = btn_Box_Output_Customer.Tag.ToString();
            int i = User_Data.GetUserRoleRead(SessionUser.UserLogin.Key, btn_Box_Output_Customer.Tag.ToString());
            if (i > 0)
            {
                if (_Frm_Stock_Ouput.IsDisposed)
                    _Frm_Stock_Ouput = new Frm_Stock_Output();
                if (!_Frm_Stock_Ouput.Visible)
                {
                    _Frm_Stock_Ouput.WareHouse = _WareHouse;
                    _Frm_Stock_Ouput.WareHouseRole = _WareHouseRole;
                    _Frm_Stock_Ouput.StyleForm = 1;
                    _Frm_Stock_Ouput.Status = 1;
                    _Frm_Stock_Ouput.RoleID = _RoleID;
                    _Frm_Stock_Ouput.Show();
                }
                else
                {
                    _Frm_Stock_Ouput.BringToFront();
                }
            }
            else
            {
                MessageBox.Show("Bạn không được xem phần này !", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void btn_Box_Output_Approve_Customer_Click(object sender, EventArgs e)
        {
            string _RoleID = btn_Box_Output_Approve_Customer.Tag.ToString();
            int i = User_Data.GetUserRoleRead(SessionUser.UserLogin.Key, btn_Box_Output_Approve_Customer.Tag.ToString());
            if (i > 0)
            {
                if (_Frm_Stock_Ouput.IsDisposed)
                    _Frm_Stock_Ouput = new Frm_Stock_Output();
                if (!_Frm_Stock_Ouput.Visible)
                {
                    _Frm_Stock_Ouput.WareHouse = _WareHouse;
                    _Frm_Stock_Ouput.WareHouseRole = _WareHouseRole;
                    _Frm_Stock_Ouput.StyleForm = 2;
                    _Frm_Stock_Ouput.Status = 2;
                    _Frm_Stock_Ouput.RoleID = _RoleID;
                    _Frm_Stock_Ouput.Show();
                }
                else
                {
                    _Frm_Stock_Ouput.BringToFront();
                }
            }
            else
            {
                MessageBox.Show("Bạn không được xem phần này !", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void btn_Box_Output_Approve_Click(object sender, EventArgs e)
        {
            string _RoleID = btn_Box_Output_Approve.Tag.ToString();
            int i = User_Data.GetUserRoleRead(SessionUser.UserLogin.Key, btn_Box_Output_Approve.Tag.ToString());
            if (i > 0)
            {
                if (_Frm_Stock_Local_Output.IsDisposed)
                    _Frm_Stock_Local_Output = new Frm_Stock_Local_Output();
                if (!_Frm_Stock_Local_Output.Visible)
                {
                    _Frm_Stock_Local_Output.WareHouse = _WareHouse;
                    _Frm_Stock_Local_Output.WarehouseRole = _WareHouseRole;
                    _Frm_Stock_Local_Output.StyleForm = 2;
                    _Frm_Stock_Local_Output.Status = 2;
                    _Frm_Stock_Local_Output.RoleID = _RoleID;
                    _Frm_Stock_Local_Output.Show();
                }
                else
                {
                    _Frm_Stock_Local_Output.BringToFront();
                }
            }
            else
            {
                MessageBox.Show("Bạn không được xem phần này !", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void btn_Box_Input_Approve_Click(object sender, EventArgs e)
        {
            string _RoleID = btn_Box_Input_Approve.Tag.ToString();
            int i = User_Data.GetUserRoleRead(SessionUser.UserLogin.Key, btn_Box_Input_Approve.Tag.ToString());
            if (i > 0)
            {
                if (_Frm_Stock_Local_Input.IsDisposed)
                    _Frm_Stock_Local_Input = new Frm_Stock_Local_Input();
                if (!_Frm_Stock_Local_Input.Visible)
                {
                    _Frm_Stock_Local_Input.WareHouse = _WareHouse;
                    _Frm_Stock_Local_Input.WareHouseRole = _WareHouseRole;
                    _Frm_Stock_Local_Input.StyleForm = 2;
                    _Frm_Stock_Local_Input.Status = 2;
                    _Frm_Stock_Local_Input.RoleID = _RoleID;
                    _Frm_Stock_Local_Input.Show();
                }
                else
                {
                    _Frm_Stock_Local_Input.BringToFront();
                }
            }
            else
            {
                MessageBox.Show("Bạn không được xem phần này !", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        #endregion  

        #region ----- group Kho Tạm -----
        private void btn_Temp_Output_Click(object sender, EventArgs e)
        {
            string _RoleID = btn_Temp_Output.Tag.ToString();
            int i = User_Data.GetUserRoleRead(SessionUser.UserLogin.Key, btn_Temp_Output.Tag.ToString());
            if (i > 0)
            {
                if (_Frm_Stock_Local_Output.IsDisposed)
                    _Frm_Stock_Local_Output = new Frm_Stock_Local_Output();
                if (!_Frm_Stock_Local_Output.Visible)
                {
                    _Frm_Stock_Local_Output.WareHouse = _WareHouse;
                    _Frm_Stock_Local_Output.WarehouseRole = _WareHouseRole;
                    _Frm_Stock_Local_Output.StyleForm = 1;
                    _Frm_Stock_Local_Output.Status = 1;
                    _Frm_Stock_Local_Output.RoleID = _RoleID;
                    _Frm_Stock_Local_Output.Show();
                }
                else
                {
                    _Frm_Stock_Local_Output.BringToFront();
                }
            }
            else
            {
                MessageBox.Show("Bạn không được xem phần này !", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void btn_Temp_Input_Click(object sender, EventArgs e)
        {
            string _RoleID = btn_Temp_Input.Tag.ToString();
            int i = User_Data.GetUserRoleRead(SessionUser.UserLogin.Key, btn_Temp_Input.Tag.ToString());
            if (i > 0)
            {
                if (_Frm_Stock_Local_Input.IsDisposed)
                    _Frm_Stock_Local_Input = new Frm_Stock_Local_Input();
                if (!_Frm_Stock_Local_Input.Visible)
                {
                    _Frm_Stock_Local_Input.WareHouse = _WareHouse;
                    _Frm_Stock_Local_Input.WareHouseRole = _WareHouseRole;
                    _Frm_Stock_Local_Input.StyleForm = 1;
                    _Frm_Stock_Local_Input.Status = 1;
                    _Frm_Stock_Local_Input.RoleID = _RoleID;
                    _Frm_Stock_Local_Input.Show();
                }
                else
                {
                    _Frm_Stock_Local_Input.BringToFront();
                }
            }
            else
            {
                MessageBox.Show("Bạn không được xem phần này !", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void btn_Temp_Output_Approve_Click(object sender, EventArgs e)
        {
            string _Role = btn_Temp_Output_Approve.Tag.ToString();
            int i = User_Data.GetUserRoleRead(SessionUser.UserLogin.Key, btn_Temp_Output_Approve.Tag.ToString());
            if (i > 0)
            {
                if (_Frm_Stock_Local_Output.IsDisposed)
                    _Frm_Stock_Local_Output = new Frm_Stock_Local_Output();
                if (!_Frm_Stock_Local_Output.Visible)
                {
                    _Frm_Stock_Local_Output.WareHouse = _WareHouse;
                    _Frm_Stock_Local_Output.WarehouseRole = _WareHouseRole;
                    _Frm_Stock_Local_Output.StyleForm = 2;
                    _Frm_Stock_Local_Output.Status = 2;
                    _Frm_Stock_Local_Output.RoleID = _Role;
                    _Frm_Stock_Local_Output.Show();
                }
                else
                {
                    _Frm_Stock_Local_Input.BringToFront();
                }
            }
            else
            {
                MessageBox.Show("Bạn không được xem phần này !", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void btn_Temp_Input_Approve_Click(object sender, EventArgs e)
        {
            string _RoleID = btn_Temp_Input_Approve.Tag.ToString();
            int i = User_Data.GetUserRoleRead(SessionUser.UserLogin.Key, btn_Temp_Input_Approve.Tag.ToString());
            if (i > 0)
            {
                if (_Frm_Stock_Local_Input.IsDisposed)
                    _Frm_Stock_Local_Input = new Frm_Stock_Local_Input();
                if (!_Frm_Stock_Local_Input.Visible)
                {
                    _Frm_Stock_Local_Input.WareHouse = _WareHouse;
                    _Frm_Stock_Local_Input.WareHouseRole = _WareHouseRole;
                    _Frm_Stock_Local_Input.StyleForm = 2;
                    _Frm_Stock_Local_Input.Status = 2;
                    _Frm_Stock_Local_Input.RoleID = _RoleID;
                    _Frm_Stock_Local_Input.Show();
                }
                else
                {
                    _Frm_Stock_Local_Input.BringToFront();
                }
            }
            else
            {
                MessageBox.Show("Bạn không được xem phần này !", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        #endregion

        #region ----- group Kho Nguyên Liệu -----
        private void btn_Material_Input_Customer_Click(object sender, EventArgs e)
        {
            string _RoleID = btn_Material_Input_Customer.Tag.ToString();
            int i = User_Data.GetUserRoleRead(SessionUser.UserLogin.Key, btn_Material_Input_Customer.Tag.ToString());
            if (i > 0)
            {
                if (_Frm_Stock_Input.IsDisposed)
                    _Frm_Stock_Input = new Frm_Stock_Input();
                if (!_Frm_Stock_Input.Visible)
                {
                    _Frm_Stock_Input.WareHouse = _WareHouse;
                    _Frm_Stock_Input.WareHouseRole = _WareHouseRole;
                    _Frm_Stock_Input.StyleForm = 1;
                    _Frm_Stock_Input.Status = 1;
                    _Frm_Stock_Input.RoleID = _RoleID;
                    _Frm_Stock_Input.Show();
                }
                else
                {
                    _Frm_Stock_Input.BringToFront();
                }
            }
            else
            {
                MessageBox.Show("Bạn không được xem phần này !", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void btn_Material_Input_Local_Click(object sender, EventArgs e)
        {
            string _RoleID = btn_Material_Input.Tag.ToString();
            int i = User_Data.GetUserRoleRead(SessionUser.UserLogin.Key, btn_Material_Input.Tag.ToString());
            if (i > 0)
            {
                if (_Frm_Stock_Local_Input.IsDisposed)
                    _Frm_Stock_Local_Input = new Frm_Stock_Local_Input();
                if (!_Frm_Stock_Local_Input.Visible)
                {
                    _Frm_Stock_Local_Input.WareHouse = _WareHouse;
                    _Frm_Stock_Local_Input.WareHouseRole = _WareHouseRole;
                    _Frm_Stock_Local_Input.StyleForm = 1;
                    _Frm_Stock_Local_Input.Status = 1;
                    _Frm_Stock_Local_Input.RoleID = _RoleID;
                    _Frm_Stock_Local_Input.Show();
                }
                else
                {
                    _Frm_Stock_Local_Input.BringToFront();
                }
            }
            else
            {
                MessageBox.Show("Bạn không được xem phần này !", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void btn_Material_Output_Local_Click(object sender, EventArgs e)
        {
            string _RoleID = btn_Material_Output.Tag.ToString();
            int i = User_Data.GetUserRoleRead(SessionUser.UserLogin.Key, btn_Material_Output.Tag.ToString());
            if (i > 0)
            {
                if (_Frm_Stock_Local_Output.IsDisposed)
                    _Frm_Stock_Local_Output = new Frm_Stock_Local_Output();
                if (!_Frm_Stock_Local_Output.Visible)
                {
                    _Frm_Stock_Local_Output.WareHouse = _WareHouse;
                    _Frm_Stock_Local_Output.WarehouseRole = _WareHouseRole;
                    _Frm_Stock_Local_Output.StyleForm = 1;
                    _Frm_Stock_Local_Output.Status = 1;
                    _Frm_Stock_Local_Output.RoleID = _RoleID;
                    _Frm_Stock_Local_Output.Show();
                }
                else
                {
                    _Frm_Stock_Local_Output.BringToFront();
                }
            }
            else
            {
                MessageBox.Show("Bạn không được xem phần này !", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void btn_Material_Input_Approve_Click(object sender, EventArgs e)
        {
            string _RoleID = btn_Material_Input_Approve.Tag.ToString();
            int i = User_Data.GetUserRoleRead(SessionUser.UserLogin.Key, btn_Material_Input_Approve.Tag.ToString());
            if (i > 0)
            {
                if (_Frm_Stock_Local_Input.IsDisposed)
                    _Frm_Stock_Local_Input = new Frm_Stock_Local_Input();
                if (!_Frm_Stock_Local_Input.Visible)
                {
                    _Frm_Stock_Local_Input.WareHouse = _WareHouse;
                    _Frm_Stock_Local_Input.WareHouseRole = _WareHouseRole;
                    _Frm_Stock_Local_Input.StyleForm = 2;
                    _Frm_Stock_Local_Input.Status = 2;
                    _Frm_Stock_Local_Input.RoleID = _RoleID;
                    _Frm_Stock_Local_Input.Show();
                }
                else
                {
                    _Frm_Stock_Local_Input.BringToFront();
                }
            }
            else
            {
                MessageBox.Show("Bạn không được xem phần này !", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void btn_Material_Output_Approve_Click(object sender, EventArgs e)
        {
            string _RoleID = btn_Material_Output_Approve.Tag.ToString();
            int i = User_Data.GetUserRoleRead(SessionUser.UserLogin.Key, btn_Material_Output_Approve.Tag.ToString());
            if (i > 0)
            {
                if (_Frm_Stock_Local_Output.IsDisposed)
                    _Frm_Stock_Local_Output = new Frm_Stock_Local_Output();
                if (!_Frm_Stock_Local_Output.Visible)
                {
                    _Frm_Stock_Local_Output.WareHouse = _WareHouse;
                    _Frm_Stock_Local_Output.WarehouseRole = _WareHouseRole;
                    _Frm_Stock_Local_Output.StyleForm = 2;
                    _Frm_Stock_Local_Output.Status = 2;
                    _Frm_Stock_Local_Output.RoleID = _RoleID;
                    _Frm_Stock_Local_Output.Show();
                }
                else
                {
                    _Frm_Stock_Local_Output.BringToFront();
                }
            }
            else
            {
                MessageBox.Show("Bạn không được xem phần này !", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void btn_Material_Input_Customer_Approve_Click(object sender, EventArgs e)
        {
            string _RoleID = btn_Material_Input_Customer_Approve.Tag.ToString();
            int i = User_Data.GetUserRoleRead(SessionUser.UserLogin.Key, btn_Material_Input_Customer_Approve.Tag.ToString());
            if (i > 0)
            {
                if (_Frm_Stock_Input.IsDisposed)
                    _Frm_Stock_Input = new Frm_Stock_Input();
                if (!_Frm_Stock_Input.Visible)
                {
                    _Frm_Stock_Input.WareHouse = _WareHouse;
                    _Frm_Stock_Input.WareHouseRole = _WareHouseRole;
                    _Frm_Stock_Input.StyleForm = 2;
                    _Frm_Stock_Input.Status = 2;
                    _Frm_Stock_Input.RoleID = _RoleID;
                    _Frm_Stock_Input.Show();
                }
                else
                {
                    _Frm_Stock_Input.BringToFront();
                }
            }
            else
            {
                MessageBox.Show("Bạn không được xem phần này !", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        #endregion

        #region ----- group Kho Hủy -----

        private void btn_Recycle_Input_Approve_Click(object sender, EventArgs e)
        {
            string _RoleID = btn_Recycle_Input_Approve.Tag.ToString();
            int i = User_Data.GetUserRoleRead(SessionUser.UserLogin.Key, btn_Recycle_Input_Approve.Tag.ToString());
            if (i > 0)
            {
                if (_Frm_Stock_Local_Input.IsDisposed)
                    _Frm_Stock_Local_Input = new Frm_Stock_Local_Input();
                if (!_Frm_Stock_Local_Input.Visible)
                {
                    _Frm_Stock_Local_Input.WareHouse = _WareHouse;
                    _Frm_Stock_Local_Input.WareHouseRole = _WareHouseRole;
                    _Frm_Stock_Local_Input.StyleForm = 2;
                    _Frm_Stock_Local_Input.Status = 2;
                    _Frm_Stock_Local_Input.RoleID = _RoleID;
                    _Frm_Stock_Local_Input.Show();
                }
                else
                {
                    _Frm_Stock_Local_Input.BringToFront();
                }
            }
            else
            {
                MessageBox.Show("Bạn không được xem phần này !", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void btn_Recycle_Ouput_Approve_Click(object sender, EventArgs e)
        {
            string _RoleID = btn_Recycle_Ouput_Approve.Tag.ToString();
            int i = User_Data.GetUserRoleRead(SessionUser.UserLogin.Key, btn_Recycle_Ouput_Approve.Tag.ToString());
            if (i > 0)
            {
                if (_Frm_Stock_Local_Output.IsDisposed)
                    _Frm_Stock_Local_Output = new Frm_Stock_Local_Output();
                if (!_Frm_Stock_Local_Output.Visible)
                {
                    _Frm_Stock_Local_Output.WareHouse = _WareHouse;
                    _Frm_Stock_Local_Output.WarehouseRole = _WareHouseRole;
                    _Frm_Stock_Local_Output.StyleForm = 2;
                    _Frm_Stock_Local_Output.Status = 2;
                    _Frm_Stock_Local_Output.RoleID = _RoleID;
                    _Frm_Stock_Local_Output.Show();
                }
                else
                {
                    _Frm_Stock_Local_Output.BringToFront();
                }
            }
            else
            {
                MessageBox.Show("Bạn không được xem phần này !", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void btn_Recycle_Input_Approve_Customer_Click(object sender, EventArgs e)
        {
            string _RoleID = btn_Recycle_Input_Approve_Customer.Tag.ToString();
            int i = User_Data.GetUserRoleRead(SessionUser.UserLogin.Key, btn_Recycle_Input_Approve_Customer.Tag.ToString());
            if (i > 0)
            {
                if (_Frm_Stock_Input.IsDisposed)
                    _Frm_Stock_Input = new Frm_Stock_Input();
                if (!_Frm_Stock_Input.Visible)
                {
                    _Frm_Stock_Input.WareHouse = _WareHouse;
                    _Frm_Stock_Input.WareHouseRole = _WareHouseRole;
                    _Frm_Stock_Input.StyleForm = 2;
                    _Frm_Stock_Input.Status = 2;
                    _Frm_Stock_Input.RoleID = _RoleID;
                    _Frm_Stock_Input.Show();
                }
                else
                {
                    _Frm_Stock_Input.BringToFront();
                }
            }
            else
            {
                MessageBox.Show("Bạn không được xem phần này !", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void btn_Recycle_Output_Click(object sender, EventArgs e)
        {
            string _RoleID = btn_Recycle_Output.Tag.ToString();
            int i = User_Data.GetUserRoleRead(SessionUser.UserLogin.Key, btn_Recycle_Output.Tag.ToString());
            if (i > 0)
            {
                if (_Frm_Stock_Local_Output.IsDisposed)
                    _Frm_Stock_Local_Output = new Frm_Stock_Local_Output();
                if (!_Frm_Stock_Local_Output.Visible)
                {
                    _Frm_Stock_Local_Output.WareHouse = _WareHouse;
                    _Frm_Stock_Local_Output.WarehouseRole = _WareHouseRole;
                    _Frm_Stock_Local_Output.StyleForm = 1;
                    _Frm_Stock_Local_Output.Status = 1;
                    _Frm_Stock_Local_Output.RoleID = _RoleID;
                    _Frm_Stock_Local_Output.Show();
                }
                else
                {
                    _Frm_Stock_Local_Output.BringToFront();
                }
            }
            else
            {
                MessageBox.Show("Bạn không được xem phần này !", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void btn_Recycle_Input_Customer_Click(object sender, EventArgs e)
        {
            string _RoleID = btn_Recycle_Input_Customer.Tag.ToString();
            int i = User_Data.GetUserRoleRead(SessionUser.UserLogin.Key, btn_Recycle_Input_Customer.Tag.ToString());
            if (i > 0)
            {
                if (_Frm_Stock_Input.IsDisposed)
                    _Frm_Stock_Input = new Frm_Stock_Input();
                if (!_Frm_Stock_Input.Visible)
                {
                    _Frm_Stock_Input.WareHouse = _WareHouse;
                    _Frm_Stock_Input.WareHouseRole = _WareHouseRole;
                    _Frm_Stock_Input.StyleForm = 1;
                    _Frm_Stock_Input.Status = 1;
                    _Frm_Stock_Input.RoleID = _RoleID;
                    _Frm_Stock_Input.Show();
                }
                else
                {
                    _Frm_Stock_Input.BringToFront();
                }
            }
            else
            {
                MessageBox.Show("Bạn không được xem phần này !", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void btn_Recycle_Input_Click(object sender, EventArgs e)
        {
            string _RoleID = btn_Recycle_Input.Tag.ToString();
            int i = User_Data.GetUserRoleRead(SessionUser.UserLogin.Key, btn_Recycle_Input.Tag.ToString());
            if (i > 0)
            {
                if (_Frm_Stock_Local_Input.IsDisposed)
                    _Frm_Stock_Local_Input = new Frm_Stock_Local_Input();
                if (!_Frm_Stock_Local_Input.Visible)
                {
                    _Frm_Stock_Local_Input.WareHouse = _WareHouse;
                    _Frm_Stock_Local_Input.WareHouseRole = _WareHouseRole;
                    _Frm_Stock_Local_Input.StyleForm = 1;
                    _Frm_Stock_Local_Input.Status = 1;
                    _Frm_Stock_Local_Input.RoleID = _RoleID;
                    _Frm_Stock_Local_Input.Show();
                }
                else
                {
                    _Frm_Stock_Local_Input.BringToFront();
                }
            }
            else
            {
                MessageBox.Show("Bạn không được xem phần này !", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        #endregion

        #region[---Group kho thach dừa-----]
       
        private void Btn_ThachDua_Input_Click(object sender, EventArgs e)
        {
            string _RoleID = btn_ThachDua_Input.Tag.ToString();
            int i = User_Data.GetUserRoleRead(SessionUser.UserLogin.Key, btn_ThachDua_Input.Tag.ToString());
            if (i > 0)
            {
                if (_Frm_Stock_Local_Input.IsDisposed)
                    _Frm_Stock_Local_Input = new Frm_Stock_Local_Input();
                if (!_Frm_Stock_Local_Input.Visible)
                {
                    _Frm_Stock_Local_Input.WareHouse = _WareHouse;
                    _Frm_Stock_Local_Input.WareHouseRole = _WareHouseRole;
                    _Frm_Stock_Local_Input.StyleForm = 1;
                    _Frm_Stock_Local_Input.Status = 1;
                    _Frm_Stock_Local_Input.RoleID = _RoleID;
                    _Frm_Stock_Local_Input.Show();
                }
                else
                {
                    _Frm_Stock_Local_Input.BringToFront();
                }
            }
            else
            {
                MessageBox.Show("Bạn không được xem phần này !", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void Btn_ThachDua_Output_Click(object sender, EventArgs e)
        {
            string _RoleID = btn_ThachDua_Output.Tag.ToString();
            int i = User_Data.GetUserRoleRead(SessionUser.UserLogin.Key, btn_ThachDua_Output.Tag.ToString());
            if (i > 0)
            {
                if (_Frm_Stock_Local_Output.IsDisposed)
                    _Frm_Stock_Local_Output = new Frm_Stock_Local_Output();
                if (!_Frm_Stock_Local_Output.Visible)
                {
                    _Frm_Stock_Local_Output.WareHouse = _WareHouse;
                    _Frm_Stock_Local_Output.WarehouseRole = _WareHouseRole;
                    _Frm_Stock_Local_Output.StyleForm = 1;
                    _Frm_Stock_Local_Output.Status = 1;
                    _Frm_Stock_Local_Output.RoleID = _RoleID;
                    _Frm_Stock_Local_Output.Show();
                }
                else
                {
                    _Frm_Stock_Local_Output.BringToFront();
                }
            }
            else
            {
                MessageBox.Show("Bạn không được xem phần này !", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void Btn_ThachDua_Customer_Click(object sender, EventArgs e)
        {
            string _RoleID = btn_ThachDua_Customer.Tag.ToString();
            int i = User_Data.GetUserRoleRead(SessionUser.UserLogin.Key, btn_ThachDua_Customer.Tag.ToString());
            if (i > 0)
            {
                if (_Frm_Stock_Ouput.IsDisposed)
                    _Frm_Stock_Ouput = new Frm_Stock_Output();
                if (!_Frm_Stock_Ouput.Visible)
                {
                    _Frm_Stock_Ouput.WareHouse = _WareHouse;
                    _Frm_Stock_Ouput.WareHouseRole = _WareHouseRole;
                    _Frm_Stock_Ouput.StyleForm = 1;
                    _Frm_Stock_Ouput.Status = 1;
                    _Frm_Stock_Ouput.RoleID = _RoleID;
                    _Frm_Stock_Ouput.Show();
                }
                else
                {
                    _Frm_Stock_Ouput.BringToFront();
                }
            }
            else
            {
                MessageBox.Show("Bạn không được xem phần này !", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void Btn_ThachDua_Input_Approve_Click(object sender, EventArgs e)
        {
            string _RoleID = btn_ThachDua_Input_Approve.Tag.ToString();
            int i = User_Data.GetUserRoleRead(SessionUser.UserLogin.Key, btn_ThachDua_Input_Approve.Tag.ToString());
            if (i > 0)
            {
                if (_Frm_Stock_Local_Input.IsDisposed)
                    _Frm_Stock_Local_Input = new Frm_Stock_Local_Input();
                if (!_Frm_Stock_Local_Input.Visible)
                {
                    _Frm_Stock_Local_Input.WareHouse = _WareHouse;
                    _Frm_Stock_Local_Input.WareHouseRole = _WareHouseRole;
                    _Frm_Stock_Local_Input.StyleForm = 2;
                    _Frm_Stock_Local_Input.Status = 2;
                    _Frm_Stock_Local_Input.RoleID = _RoleID;
                    _Frm_Stock_Local_Input.Show();
                }
                else
                {
                    _Frm_Stock_Local_Input.BringToFront();
                }
            }
            else
            {
                MessageBox.Show("Bạn không được xem phần này !", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void Btn_ThachDua_Output_Approve_Click(object sender, EventArgs e)
        {
            string _RoleID = btn_ThachDua_Output_Approve.Tag.ToString();
            int i = User_Data.GetUserRoleRead(SessionUser.UserLogin.Key, btn_ThachDua_Output_Approve.Tag.ToString());
            if (i > 0)
            {
                if (_Frm_Stock_Local_Output.IsDisposed)
                    _Frm_Stock_Local_Output = new Frm_Stock_Local_Output();
                if (!_Frm_Stock_Local_Output.Visible)
                {
                    _Frm_Stock_Local_Output.WareHouse = _WareHouse;
                    _Frm_Stock_Local_Output.WarehouseRole = _WareHouseRole;
                    _Frm_Stock_Local_Output.StyleForm = 2;
                    _Frm_Stock_Local_Output.Status = 2;
                    _Frm_Stock_Local_Output.RoleID = _RoleID;
                    _Frm_Stock_Local_Output.Show();
                }
                else
                {
                    _Frm_Stock_Local_Output.BringToFront();
                }
            }
            else
            {
                MessageBox.Show("Bạn không được xem phần này !", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void Btn_ThachDua_Approve_Customer_Click(object sender, EventArgs e)
        {
            string _RoleID = btn_ThachDua_Approve_Customer.Tag.ToString();
            int i = User_Data.GetUserRoleRead(SessionUser.UserLogin.Key, btn_ThachDua_Approve_Customer.Tag.ToString());
            if (i > 0)
            {
                if (_Frm_Stock_Ouput.IsDisposed)
                    _Frm_Stock_Ouput = new Frm_Stock_Output();
                if (!_Frm_Stock_Ouput.Visible)
                {
                    _Frm_Stock_Ouput.WareHouse = _WareHouse;
                    _Frm_Stock_Ouput.WareHouseRole = _WareHouseRole;
                    _Frm_Stock_Ouput.StyleForm = 2;
                    _Frm_Stock_Ouput.Status = 2;
                    _Frm_Stock_Ouput.RoleID = _RoleID;
                    _Frm_Stock_Ouput.Show();
                }
                else
                {
                    _Frm_Stock_Ouput.BringToFront();
                }
            }
            else
            {
                MessageBox.Show("Bạn không được xem phần này !", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        #endregion

        #region[---Group kho vườn ươm-----]
        
        private void Btn_VuonUom_Input_Click(object sender, EventArgs e)
        {
            string _RoleID = btn_VuonUom_Input.Tag.ToString();
            int i = User_Data.GetUserRoleRead(SessionUser.UserLogin.Key, btn_VuonUom_Input.Tag.ToString());
            if (i > 0)
            {
                if (_Frm_Stock_Local_Input.IsDisposed)
                    _Frm_Stock_Local_Input = new Frm_Stock_Local_Input();
                if (!_Frm_Stock_Local_Input.Visible)
                {
                    _Frm_Stock_Local_Input.WareHouse = _WareHouse;
                    _Frm_Stock_Local_Input.WareHouseRole = _WareHouseRole;
                    _Frm_Stock_Local_Input.StyleForm = 1;
                    _Frm_Stock_Local_Input.Status = 1;
                    _Frm_Stock_Local_Input.RoleID = _RoleID;
                    _Frm_Stock_Local_Input.Show();
                }
                else
                {
                    _Frm_Stock_Local_Input.BringToFront();
                }
            }
            else
            {
                MessageBox.Show("Bạn không được xem phần này !", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void Btn_VuonUom_Output_Click(object sender, EventArgs e)
        {
            string _RoleID = btn_VuonUom_Output.Tag.ToString();
            int i = User_Data.GetUserRoleRead(SessionUser.UserLogin.Key, btn_VuonUom_Output.Tag.ToString());
            if (i > 0)
            {
                if (_Frm_Stock_Local_Output.IsDisposed)
                    _Frm_Stock_Local_Output = new Frm_Stock_Local_Output();
                if (!_Frm_Stock_Local_Output.Visible)
                {
                    _Frm_Stock_Local_Output.WareHouse = _WareHouse;
                    _Frm_Stock_Local_Output.WarehouseRole = _WareHouseRole;
                    _Frm_Stock_Local_Output.StyleForm = 1;
                    _Frm_Stock_Local_Output.Status = 1;
                    _Frm_Stock_Local_Output.RoleID = _RoleID;
                    _Frm_Stock_Local_Output.Show();
                }
                else
                {
                    _Frm_Stock_Local_Output.BringToFront();
                }
            }
            else
            {
                MessageBox.Show("Bạn không được xem phần này !", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void Btn_VuonUom_Customer_Click(object sender, EventArgs e)
        {
            string _RoleID = btn_VuonUom_Customer.Tag.ToString();
            int i = User_Data.GetUserRoleRead(SessionUser.UserLogin.Key, btn_VuonUom_Customer.Tag.ToString());
            if (i > 0)
            {
                if (_Frm_Stock_Ouput.IsDisposed)
                    _Frm_Stock_Ouput = new Frm_Stock_Output();
                if (!_Frm_Stock_Ouput.Visible)
                {
                    _Frm_Stock_Ouput.WareHouse = _WareHouse;
                    _Frm_Stock_Ouput.WareHouseRole = _WareHouseRole;
                    _Frm_Stock_Ouput.StyleForm = 1;
                    _Frm_Stock_Ouput.Status = 1;
                    _Frm_Stock_Ouput.RoleID = _RoleID;
                    _Frm_Stock_Ouput.Show();
                }
                else
                {
                    _Frm_Stock_Ouput.BringToFront();
                }
            }
            else
            {
                MessageBox.Show("Bạn không được xem phần này !", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void Btn_VuonUom_Input_Approve_Click(object sender, EventArgs e)
        {
            string _RoleID = btn_VuonUom_Input_Approve.Tag.ToString();
            int i = User_Data.GetUserRoleRead(SessionUser.UserLogin.Key, btn_VuonUom_Input_Approve.Tag.ToString());
            if (i > 0)
            {
                if (_Frm_Stock_Local_Output.IsDisposed)
                    _Frm_Stock_Local_Output = new Frm_Stock_Local_Output();
                if (!_Frm_Stock_Local_Output.Visible)
                {
                    _Frm_Stock_Local_Output.WareHouse = _WareHouse;
                    _Frm_Stock_Local_Output.WarehouseRole = _WareHouseRole;
                    _Frm_Stock_Local_Output.StyleForm = 2;
                    _Frm_Stock_Local_Output.Status = 2;
                    _Frm_Stock_Local_Output.RoleID = _RoleID;
                    _Frm_Stock_Local_Output.Show();
                }
                else
                {
                    _Frm_Stock_Local_Output.BringToFront();
                }
            }
            else
            {
                MessageBox.Show("Bạn không được xem phần này !", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void Btn_VuonUom_Output_Approve_Click(object sender, EventArgs e)
        {
            string _RoleID = btn_VuonUom_Output_Approve.Tag.ToString();
            int i = User_Data.GetUserRoleRead(SessionUser.UserLogin.Key, btn_VuonUom_Output_Approve.Tag.ToString());
            if (i > 0)
            {
                if (_Frm_Stock_Local_Output.IsDisposed)
                    _Frm_Stock_Local_Output = new Frm_Stock_Local_Output();
                if (!_Frm_Stock_Local_Output.Visible)
                {
                    _Frm_Stock_Local_Output.WareHouse = _WareHouse;
                    _Frm_Stock_Local_Output.WarehouseRole = _WareHouseRole;
                    _Frm_Stock_Local_Output.StyleForm = 2;
                    _Frm_Stock_Local_Output.Status = 2;
                    _Frm_Stock_Local_Output.RoleID = _RoleID;
                    _Frm_Stock_Local_Output.Show();
                }
                else
                {
                    _Frm_Stock_Local_Output.BringToFront();
                }
            }
            else
            {
                MessageBox.Show("Bạn không được xem phần này !", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void Btn_VuonUom_Approve_Customer_Click(object sender, EventArgs e)
        {
            string _RoleID = btn_VuonUom_Approve_Customer.Tag.ToString();
            int i = User_Data.GetUserRoleRead(SessionUser.UserLogin.Key, btn_VuonUom_Approve_Customer.Tag.ToString());
            if (i > 0)
            {
                if (_Frm_Stock_Ouput.IsDisposed)
                    _Frm_Stock_Ouput = new Frm_Stock_Output();
                if (!_Frm_Stock_Ouput.Visible)
                {
                    _Frm_Stock_Ouput.WareHouse = _WareHouse;
                    _Frm_Stock_Ouput.WareHouseRole = _WareHouseRole;
                    _Frm_Stock_Ouput.StyleForm = 2;
                    _Frm_Stock_Ouput.Status = 2;
                    _Frm_Stock_Ouput.RoleID = _RoleID;
                    _Frm_Stock_Ouput.Show();
                }
                else
                {
                    _Frm_Stock_Ouput.BringToFront();
                }
            }
            else
            {
                MessageBox.Show("Bạn không được xem phần này !", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        #endregion

        #region ----- group Nhân Sự -----

        private void btn_Employee_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Employee", false, btn_Employee.Tag);
        }
        private void btn_Contract_Click(object sender, EventArgs e)
        {
            //if (_Frm_ContactEmployee.IsDisposed)
            //    _Frm_ContactEmployee = new Frm_Employee_Contract();
            //if (!_Frm_ContactEmployee.Visible)
            //{
            //    _Frm_ContactEmployee.Show();
            //}
            //else
            //{
            //    _Frm_ContactEmployee.BringToFront();
            //}
        }
        private void Btn_WorkingHistory_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_WorkingHistory", false, btn_WorkingHistory.Tag);
        }

        private void btn_Receipt_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Receipt", false, btn_Receipt.Tag);
        }
        private void btn_Payment_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Payment", false, btn_Payment.Tag);
        }
        private void btn_TimeKeeping_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_ImportTimeKeeping", false, btn_TimeKeeping.Tag);
        }
        private void btn_Caculator_Keeping_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Calculator_TimeKeeping", false, btn_Caculator_Keeping.Tag);
        }
        private void btn_DayOffHoliday_Click(object sender, EventArgs e)
        {
            //int i = User_Data.GetUserRoleRead(SessionUser.UserLogin.Key, btn_Caculator_Keeping.Tag.ToString());
            //if (i > 0)
            //{
            //    if (_Frm_DayOff_Support.IsDisposed)
            //        _Frm_DayOff_Support = new Frm_DayOff_Support();
            //    if (!_Frm_DayOff_Support.Visible)
            //    {
            //        _Frm_DayOff_Support.Show();
            //    }
            //    else
            //    {
            //        _Frm_DayOff_Support.BringToFront();
            //    }
            //}
            //else
            //{
            //    MessageBox.Show("Bạn không được xem phần này !", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //}
        }
        private void Btn_ReportCong_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_BaoCao_Cong_V02", false, btn_ReportCong.Tag);
        }

        private void Btn_Report_6_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Report_6", false, btn_Report_6.Tag);
        }
        private void Btn_FeeDefault_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_FeeDefault02", false, btn_FeeDefault.Tag);
        }

        private void Btn_Support_Leader_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_FeeLeader_Month02", false, btn_Support_Leader.Tag);
        }
        private void Btn_Standard_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_NumberWorkStandard02", false, btn_Standard.Tag);
        }
        private void Btn_FeeChildren_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_FeeChildren02", false, btn_FeeChildren.Tag);
        }
        private void Btn_SalaryDefault_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_SalaryDefault02", false, btn_SalaryDefault.Tag);
        }
        private void Btn_CaculatorWorker_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_CaculatorSalaryWorker", false, btn_CaculatorWorker.Tag);
        }
        private void Btn_NCPN_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Adjust_NCPN", false, btn_NCPN.Tag);
        }
        private void Btn_ChangeTeam_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_EmployeeChangeTeam", false, btn_ChangeTeam.Tag);
        }
        private void Btn_Diligence_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Number_Diligence", false, btn_Diligence.Tag);
        }
        private void Btn_TinhThuongKD_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_TinhThuongKD", false, btn_TinhThuongKD.Tag);
        }
        private void Btn_TinhLuongThang13_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_TinhLuongThang13", false, btn_TinhLuongThang13.Tag);
        }
        private void Btn_LuongThang13_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_BaoCaoLuongThang13", false, btn_LuongThang13.Tag);
        }
        private void Btn_Business_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_BaoCaoThuongKinhDoanh", false, btn_Business.Tag);
        }
        private void Btn_RiceCompany_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_RiceCompany", false, btn_RiceCompany.Tag);
        }
        private void Btn_HMList_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_BaoCaoDanhSachNhanSu", false, btn_HMList.Tag);
        }
        #endregion

        #region ----- Setup-----

        private void Btn_Lock_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_DateClose", false, btn_Lock.Tag);
        }
        private void btn_Customer_Company_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Customer_Company", false, btn_Customer_Company.Tag);
        }
        private void btn_Customer_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Customer_Person", false, btn_Customer_Individual.Tag);
        }
        private void btn_Setup_Order_Rate_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Setup_Order_Rate", false, btn_Setup_Coefficent.Tag);
        }
        private void btn_Setup_Product_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_NguyenLieu", false, btn_Product.Tag);
        }
        private void btn_Setup_Team_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Setup_Team", false, btn_Team.Tag);
        }
        private void btn_Setup_Position_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Setup_Position", false, btn_Position.Tag);
        }
        private void btn_Setup_Department_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Setup_Department", false, btn_Department.Tag);
        }
        private void btn_Setup_Branch_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Setup_Branch", false, btn_Branch.Tag);
        }
        private void btn_Setup_Parameter_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Setup_Parameter", false, btn_Paramaters.Tag);
        }

        private void btn_Setup_Working_Time_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Setup_Working_Time", false, btn_Time_Defines.Tag);
        }
        //private Frm_Setup_Rice _Frm_Time_Rice = new Frm_Setup_Rice();
        private void btn_Time_Rice_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Setup_Rice_03", false, btn_Setup_Rice.Tag);
            //if (_Frm_Time_Rice.IsDisposed)
            //    _Frm_Time_Rice = new Frm_Setup_Rice();
            //if (!_Frm_Time_Rice.Visible)
            //{
            //    _Frm_Time_Rice.Show();
            //}
            //else
            //{
            //    _Frm_Time_Rice.BringToFront();
            //}
        }
        private void Btn_Combo_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_ComboList", false, btn_Combo.Tag);
        }

        private Frm_Setup_Fee _Frm_Fee_Category = new Frm_Setup_Fee();
        private void btn_Category_Fee_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Setup_Fee", false, btn_Fee.Tag);
        }

        private void btn_Setup_Unit_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Setup_Unit", false, btn_Setup_Unit.Tag);
        }
        private void btn_Setup_Time_Holiday_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Setup_Time_Holiday", false, btn_Holiday.Tag);
        }
        private void btn_Setup_Unit_Convert_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Setup_Unit_Convert", false, btn_Convert_Unit.Tag);
        }
        private void btn_Setup_Work(object sender, EventArgs e)
        {
            OpenForm("Frm_Setup_Work", false, btn_Stages.Tag);
        }
        //private Frm_Shift_Workcs _Frm_Shift_Workcs = new Frm_Shift_Workcs();
        private void btn_Shift_Work_Click(object sender, EventArgs e)
        {
            //if (_Frm_Shift_Workcs.IsDisposed)
            //    _Frm_Shift_Workcs = new Frm_Shift_Workcs();
            //if (!_Frm_Shift_Workcs.Visible)
            //{
            //    _Frm_Shift_Workcs.Show();
            //}
            //else
            //{
            //    _Frm_Shift_Workcs.BringToFront();
            //}
        }
        private void Btn_ThanhPham_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_SanPham", false, btn_ThanhPham.Tag);
        }
        #endregion

        #endregion

        #region Event Others
        void CheckRole()
        {
            btn_ChangePass.Enabled = false;
            btn_Download.Enabled = false;

            #region[Panel_Left]          
            btn_Warehouse_1.Enabled = false;
            btn_Warehouse_2.Enabled = false;
            btn_Warehouse_3.Enabled = false;
            btn_Warehouse_4.Enabled = false;
            btn_Warehouse_5.Enabled = false;
            btn_ThachDua.Enabled = false;
            btn_VuonUom.Enabled = false;

            btn_Human.Enabled = false;
            btn_Production.Enabled = false;
            btn_Setup.Enabled = false;
            btn_Report.Enabled = false;
            btn_Salary.Enabled = false;
            #endregion

            #region[Panel Right]
            Panel_Setup.Visible = false;
            Panel_Box.Visible = false;
            Panel_Freezing.Visible = false;
            Panel_Material.Visible = false;
            Panel_Recycle.Visible = false;
            Panel_Temp.Visible = false;
            Panel_ThachDua.Visible = false;
            Panel_VuonUom.Visible = false;
            Panel_Human.Visible = false;
            Panel_Production.Visible = false;
            Panel_Report.Visible = false;
            Panel_Salary.Visible = false;
            #endregion

            if (SessionUser.UserLogin == null)
            {
                btn_Exit.Text = "Đăng nhập";
                return;
            }


            #region [CheckRole]
            DataRow zRow = User_Data.GetUserRoleIsAccess(SessionUser.UserLogin.Key, btn_Setup.Tag.ToString()).Rows[0];
            if (Convert.ToInt32(zRow[0]) > 0)
            {
                btn_Setup.Enabled = true;
                btn_Setup.BackColor = Color.FromArgb(0, 135, 156);
            }
            zRow = User_Data.GetUserRoleIsAccess(SessionUser.UserLogin.Key, btn_Warehouse_1.Tag.ToString()).Rows[0];
            if (Convert.ToInt32(zRow[0]) > 0)
            {
                btn_Warehouse_1.Enabled = true;
                btn_Warehouse_1.BackColor = Color.FromArgb(0, 135, 156);
            }
            zRow = User_Data.GetUserRoleIsAccess(SessionUser.UserLogin.Key, btn_Warehouse_2.Tag.ToString()).Rows[0];
            if (Convert.ToInt32(zRow[0]) > 0)
            {
                btn_Warehouse_2.Enabled = true;
                btn_Warehouse_2.BackColor = Color.FromArgb(47, 81, 165);
            }
            zRow = User_Data.GetUserRoleIsAccess(SessionUser.UserLogin.Key, btn_Warehouse_3.Tag.ToString()).Rows[0];
            if (Convert.ToInt32(zRow[0]) > 0)
            {
                btn_Warehouse_3.Enabled = true;
                btn_Warehouse_3.BackColor = Color.FromArgb(0, 190, 150);
            }
            zRow = User_Data.GetUserRoleIsAccess(SessionUser.UserLogin.Key, btn_Warehouse_4.Tag.ToString()).Rows[0];
            if (Convert.ToInt32(zRow[0]) > 0)
            {
                btn_Warehouse_4.Enabled = true;
                btn_Warehouse_4.BackColor = Color.FromArgb(73, 184, 36);
            }
            zRow = User_Data.GetUserRoleIsAccess(SessionUser.UserLogin.Key, btn_Warehouse_5.Tag.ToString()).Rows[0];
            if (Convert.ToInt32(zRow[0]) > 0)
            {
                btn_Warehouse_5.Enabled = true;
                btn_Warehouse_5.BackColor = Color.FromArgb(255, 141, 15);
            }
            zRow = User_Data.GetUserRoleIsAccess(SessionUser.UserLogin.Key, btn_ThachDua.Tag.ToString()).Rows[0];
            if (Convert.ToInt32(zRow[0]) > 0)
            {
                btn_ThachDua.Enabled = true;
                btn_ThachDua.BackColor = Color.FromArgb(255, 141, 15);
            }
            zRow = User_Data.GetUserRoleIsAccess(SessionUser.UserLogin.Key, btn_VuonUom.Tag.ToString()).Rows[0];
            if (Convert.ToInt32(zRow[0]) > 0)
            {
                btn_VuonUom.Enabled = true;
                btn_VuonUom.BackColor = Color.FromArgb(255, 141, 15);
            }
            zRow = User_Data.GetUserRoleIsAccess(SessionUser.UserLogin.Key, btn_Production.Tag.ToString()).Rows[0];
            if (Convert.ToInt32(zRow[0]) > 0)
            {
                btn_Production.Enabled = true;
                btn_Production.BackColor = Color.FromArgb(62, 112, 56);
            }
            zRow = User_Data.GetUserRoleIsAccess(SessionUser.UserLogin.Key, btn_Human.Tag.ToString()).Rows[0];
            if (Convert.ToInt32(zRow[0]) > 0)
            {
                btn_Human.Enabled = true;
                btn_Human.BackColor = Color.FromArgb(38, 164, 221);
            }
            zRow = User_Data.GetUserRoleIsAccess(SessionUser.UserLogin.Key, btn_Salary.Tag.ToString()).Rows[0];
            if (Convert.ToInt32(zRow[0]) > 0)
            {
                btn_Salary.Enabled = true;
                btn_Salary.BackColor = Color.FromArgb(38, 164, 221);
            }
            zRow = User_Data.GetUserRoleIsAccess(SessionUser.UserLogin.Key, btn_Report.Tag.ToString()).Rows[0];
            if (Convert.ToInt32(zRow[0]) > 0)
            {
                btn_Report.Enabled = true;
                btn_Report.BackColor = Color.FromArgb(38, 164, 221);
            }

            #endregion

            if (SessionUser.UserLogin.Key.Length > 0)
            {
                btn_ChangePass.Enabled = true;
                btn_Download.Enabled = true;
                lbl_EmployeeName.Text = "Nhân viên : " + SessionUser.UserLogin.EmployeeName;
                btn_Exit.Text = "Đăng xuất";
            }
            else
            {
                btn_Exit.Text = "Đăng nhập";
            }
        }
        private void Dte_WorkDate_ValueChanged(object sender, EventArgs e)
        {
            SessionUser.Date_Work = dte_WorkDate.Value;
        }
        #endregion

        private void btn_Rpt3_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Report_21", false, btn_Rpt3.Tag);
        }

        private void btn_Rpt1_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Report_1", false, btn_Rpt1.Tag);

        }
        private void btn_Rpt2_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Report_2_2", false, btn_Rpt2.Tag);
        }
        private void Btn_Rpt_19_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Report_19_V2", false, btn_Rpt_19.Tag);
        }
        private void Btn_Rpt7_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Rpt7_V3", false, btn_Rpt7.Tag);
        }
        private void Btn_Rpt8_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Report_8_10", false, btn_Rpt8.Tag);
        }
        private void Btn_Rpt9_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Report_11_9", false, btn_Rpt9.Tag);
        }
        private void Btn_Rpt12_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_BaoCao_Cong_Chot_V02", false, btn_Rpt12.Tag);
        }
        private void Btn_Rpt13_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Rpt13_V3", false, btn_Rpt13.Tag);
        }
        private void Btn_Rpt14_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Rpt14_V3", false, btn_Rpt14.Tag);
        }
        private void Btn_CaculatorSuport_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_CaculatorSalarySupport", false, btn_CaculatorSuport.Tag);
        }
        private void Btn_Rpt15_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Rpt15_V3", false, btn_Rpt15.Tag);
        }
        private void Btn_Rpt_16_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Rpt16_V4", false, btn_Rpt_16.Tag);
        }
        private void Btn_Rpt17_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Rpt17_V3", false, btn_Rpt17.Tag);
        }
        private void Btn_Report20_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Report_20_V2", false, btn_Report20.Tag);
        }

        private void Btn_CaculatorOffice_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_CaculatorSalaryOffice", false, btn_CaculatorOffice.Tag);
        }
        private void Btn_Rpt41_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Rpt41_V2", false, btn_Rpt41.Tag);
        }
        private void Btn_Rpt44_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Rpt42_V2", false, btn_Rpt44.Tag);
        }

        private void Btn_Rpt43_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Rpt_43", false, btn_Rpt43.Tag);
        }
        private void Btn_Rpt45_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Report_45", false, btn_Rpt45.Tag);
        }
        private void Btn_Rpt42_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Rpt42_V2", false, btn_Rpt42.Tag);
        }
        private void Btn_Rpt47_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Rpt_47", false, btn_Rpt47.Tag);
        }

        private void Btn_Rpt48_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Rpt_48", false, btn_Rpt48.Tag);
        }
        private void Btn_Rpt18_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Rpt18_V2", false, btn_Rpt18.Tag);
        }
        private void Btn_Rpt_Mau01_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_BaoCao_Mau1", false, btn_Rpt_Mau01.Tag);
        }
        private void Btn_Rpt_Mau02_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_BaoCao_Mau2", false, btn_Rpt_Mau02.Tag);
        }
        private void Btn_Bao_Cao_Ngay_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_GioChamCong", false, btn_Bao_Cao_Ngay.Tag);
        }
        private void Btn_THThuNhap_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_BaoCaoTongHopThuNhap", false, btn_THThuNhap.Tag);
        }
        private void Btn_TrungBinhNgay_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_BaoCaoTienLuongTrungBinhNgay", false, btn_TrungBinhNgay.Tag);
        }
        private void Btn_Rpt_ThoiGian_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_TG_Lam_Viec_Tại_Xuong_Trong_Thang", false, btn_Rpt_ThoiGian.Tag);
        }
        private void Btn_TG_LamViecTaiXuong_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_TG_Lam_Viec_Tại_Xuong_Tung_Thang", false, btn_TG_LamViecTaiXuong.Tag);
        }
        private void Btn_TongLuongKhoan_SP_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Report24", false, btn_TongLuongKhoan_SP.Tag);
        }
        private void Btn_PayAtm_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_PayATM", false, btn_PayAtm.Tag);
        }
        private void Btn_LuongTongHop_ChiTiet_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_BaoCaoLuongTongHopChiTiet", false, btn_LuongTongHop_ChiTiet.Tag);
        }
        private void Btn_Worker_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_SalaryMonth_Worker", false, btn_Worker.Tag);
        }
        private void Btn_Support_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_SalaryMonth_Support", false, btn_Support.Tag);
        }
        private void Btn_Office_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_SalaryMonth_Office", false, btn_Office.Tag);
        }
        private void Btn_LuongTongHopHoTro_ChiTiet_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_BaoCaoLuongTongHopChiTiet_HoTro", false, btn_LuongTongHopHoTro_ChiTiet.Tag);
        }
        private void Btn_LuongTongHopSanXuat_ChiTiet_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_BaoCaoLuongTongHopChiTiet_SanXuat", false,btn_Team.Tag);
        }
        private void Btn_UngLuong_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_BaoCaoUngLuong", false, btn_UngLuong.Tag);
        }
        private void Btn_BangKeNhapKho_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_BangKePhieuNhapKho", false, btn_BangKeNhapKho.Tag);
        }
        private void Btn_BangKeXuatKho_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_BangKePhieuXuatKho", false, btn_BangKeXuatKho.Tag);
        }

        private void Btn_TheKho_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_TheKho", false, btn_TheKho.Tag);
        }
        private void Btn_SLDHChuaChiaLai_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_SoDonHangChuaChiaLai", false, btn_SLDHChuaChiaLai.Tag); 
        }
        private void Btn_PhieuLuong_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_PhieuLuong", false, btn_PhieuLuong.Tag);
        }
        private void Btn_TBLK_TaiTo_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_BaoCaoTienLuongTrungBinh_TaiTo", false, btn_TBLK_TaiTo.Tag);
        }
        private void Btn_SL_TaiBP_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Report22", false, btn_SL_TaiBP.Tag);
        }
        private void Btn_Report23_Click(object sender, EventArgs e)
        {
             OpenForm("Frm_Report23_V2", false, btn_Report23.Tag);
        }
        private void Btn_CC_ChiTiet_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_LuongKhoanTinhChuyenCan_ChiTiet", false, btn_CC_ChiTiet.Tag);
        }
        private void Btn_PBChucVu_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_ThongKeChucVu", false, btn_PBChucVu.Tag);
        }
        private void Btn_Report25_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Report25", false, btn_Report25.Tag);
        }
        private void btn_Exit_Click(object sender, EventArgs e)
        {
            if (SessionUser.UserLogin != null && SessionUser.UserLogin.Key!="")
            {
                if (Utils.TNMessageBox("Bạn có muốn thoát phần mềm !", 2) == "Y")
                {
                    User_Info zUser = new User_Info(SessionUser.UserLogin.Name);
                    zUser.UpdateIsLogin(0);// hạ cờ đăng nhập
                    Log_Login(3, "Đã đăng xuất phần mềm");
                    new SessionUser();
                    CheckRole();
                    Frm_Login frm = new Frm_Login();
                    frm.ShowDialog();
                    lbl_EmployeeName.Text = "Nhân viên : " + SessionUser.UserLogin.EmployeeName;
                    CheckRole();
                    if (SessionUser.UserLogin != null)
                        CheckRole_SubMenu();
                }
            }
            else
            {
                new SessionUser();
                CheckRole();
                Frm_Login frm = new Frm_Login();
                frm.ShowDialog();
                lbl_EmployeeName.Text = "Nhân viên : " + SessionUser.UserLogin.EmployeeName;
                CheckRole();
                if (SessionUser.UserLogin != null)
                    CheckRole_SubMenu();
            }
        }
        private void btn_ChangePass_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_ChangePass", false, btn_ChangePass.Tag);
        }


        #region [Open Quick]
        private bool CheckOpened(string name)
        {
            FormCollection fc = Application.OpenForms;

            foreach (Form frm in fc)
            {
                if (frm.Name == name)
                {
                    frm.BringToFront();
                    return true;
                }
            }

            return false;
        }
        /// <summary>
        /// Mở nhanh form
        /// </summary>
        /// <param name="Name">truyền ID của form</param>
        /// <param name="AllowMulti">Cho phép mở nhiều form hay không</param>
        private void OpenForm(string Name, bool AllowMulti,object Tag)
        {
            string zTag = "";
            if (Tag == null)
                zTag = "";
            else
                zTag = Tag.ToString();
            var obj = Activator.CreateInstance(Type.GetType("TNS.WinApp." + Name));
            if (obj != null)
            {
                if (AllowMulti)
                {
                    var form = (Form)obj;
                    form.Tag = zTag;
                    form.Show();
                }
                else
                {
                    if (!CheckOpened(Name))
                    {
                        var form = (Form)obj;
                        form.Tag = zTag;
                        form.Show();
                    }
                }
            }
            else
            {
               Utils.TNMessageBoxOK("Không tìm thấy: " + Name, 1);
            }
        }
        #endregion

        private void btn_ProductType_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Setup_Product_Type", false, btn_ProductType.Tag);
        }

        private void btn_WorkGroup_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Setup_Work_Group", false, btn_WorkGroup.Tag);
        }

        private void Btn_Adjusted_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Report_AdjustV2", false, btn_Adjusted.Tag);
        }

        private void btn_Setup_Off_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Setup_Off", false, btn_Setup.Tag);
        }

        private void btn_Rpt5_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Report_5", false,btn_Rpt5.Tag);
        }
        private void Btn_KhoiTao_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_Team_Employee_Month", false,btn_KhoiTao.Tag);
        }
       

        ////Lưu log đăng nhập
        //private void Log_Login(bool Status, string Description)
        //{

        //    User_Info nUserLogin = new User_Info();
        //    nUserLogin.GetEmployee(SessionUser.UserLogin.Key);
        //    string zComputerName = Environment.MachineName;
        //    string zIpAddress = "";
        //    //string zIpAddress = Dns.GetHostAddresses(Environment.MachineName)[1].ToString(); //có thể bị lấy nhằm IPV6
        //    string zWinVersion = Environment.OSVersion.VersionString.ToString();
        //    string zmyHost = System.Net.Dns.GetHostName();
        //    // int zCursor = System.Net.Dns.GetHostEntry(zmyHost).AddressList.Length - 1;
        //    //zIpAddress = System.Net.Dns.GetHostEntry(zmyHost).AddressList[zCursor].ToString();
        //    //Loại IP V6
        //    for (int i = 0; i <= System.Net.Dns.GetHostEntry(zmyHost).AddressList.Length - 1; i++)
        //    {
        //        //if (System.Net.Dns.GetHostEntry(zmyHost).AddressList[i].IsIPv6LinkLocal == false)
        //        //{
        //            zIpAddress = zIpAddress+";"+ System.Net.Dns.GetHostEntry(zmyHost).AddressList[i].ToString();
        //        //}
        //    }

        //    string subKey = @"SOFTWARE\Wow6432Node\Microsoft\Windows NT\CurrentVersion";
        //    Microsoft.Win32.RegistryKey key = Microsoft.Win32.Registry.LocalMachine;
        //    Microsoft.Win32.RegistryKey skey = key.OpenSubKey(subKey);
        //    string zWinName = skey.GetValue("ProductName").ToString();

        //    Login_Info zInfo = new Login_Info();
        //    zInfo.UserKey = nUserLogin.Key;
        //    zInfo.DateLogin = DateTime.Now;
        //    zInfo.ComputerLogin = zComputerName;
        //    zInfo.IPAddress = zIpAddress;
        //    zInfo.WindownsName = zWinName;
        //    zInfo.WindownsVersion = zWinVersion;
        //    //if (Status == true)
        //    //    zInfo.FailedPassword = 0; //dăng nhập thành công
        //    //else
        //    //    zInfo.FailedPassword = 1;
        //    zInfo.Description = Description;
        //    zInfo.Create();
        //}
        public void Log_Login(int TypeLog, string Description)
        {

            if (SessionUser.UserLogin != null)
            {
                User_Info nUserLogin = new User_Info();
                nUserLogin.GetEmployee(SessionUser.UserLogin.Key);


                string zComputerName = "";
                if (Environment.MachineName == null)
                {
                    zComputerName = "Đối tượng bị null";
                }
                else
                {
                    zComputerName = Environment.MachineName;
                }

                string zIpAddress = "";
                if (Dns.GetHostAddresses(Environment.MachineName) == null)
                {
                    zIpAddress = "IP bị null";
                }
                else
                {
                    if (Dns.GetHostAddresses(Environment.MachineName).Length == 1)
                        zIpAddress = Dns.GetHostAddresses(Environment.MachineName)[0].ToString(); //có thể bị lấy nhằm IPV6
                    else
                        zIpAddress = Dns.GetHostAddresses(Environment.MachineName)[1].ToString();
                }
                string zWinVersion = "";
                //if (Environment.OSVersion.VersionString == null)
                //    zWinVersion = "Win version bị null";
                //else
                //    zWinVersion = Environment.OSVersion.VersionString.ToString();

                //string subKey = @"SOFTWARE\Wow6432Node\Microsoft\Windows NT\CurrentVersion";
                //Microsoft.Win32.RegistryKey key;
                string zWinName = "";
                //if (Microsoft.Win32.Registry.LocalMachine != null)
                //{
                //    key = Microsoft.Win32.Registry.LocalMachine;
                //    Microsoft.Win32.RegistryKey skey = key.OpenSubKey(subKey);
                //    zWinName = skey.GetValue("ProductName").ToString();
                //}
                //else
                //{
                //    zWinName = "Winname bị null";
                //}

                Login_Info zInfo = new Login_Info();
                zInfo.UserKey = nUserLogin.Key;
                zInfo.DateLogin = DateTime.Now;
                zInfo.ComputerLogin = zComputerName;
                zInfo.IPAddress = zIpAddress;
                zInfo.WindownsName = zWinName;
                zInfo.WindownsVersion = zWinVersion;
                //1: thành công
                //2:thất bại
                //3:đăng xuất
                zInfo.TypeLog = TypeLog;
                zInfo.Description = Description;
                zInfo.Create();
            }
        }

        private void CheckRole_SubMenu()
        {
            KryptonButton ctr;
            //Set up
            int count = Panel_Setup.Controls.Count;

            for(int i=0; i< count; i++)
            {
                 ctr = (KryptonButton)Panel_Setup.Controls[i];
                DataRow zRow = User_Data.GetUserRoleIsAccess(SessionUser.UserLogin.Key, ctr.Tag.ToString()).Rows[0];
                if (Convert.ToInt32(zRow[0]) > 0)
                {
                    ctr.Enabled = true;
                }
                else
                {
                    ctr.BackColor = Color.FromArgb(0, 135, 156);
                    ctr.Enabled = false;
                    ctr.StateDisabled.Back.Color1 = Color.FromArgb(252, 248, 248);
                    ctr.StateDisabled.Border.Color1 = Color.FromArgb(198,193,193);

                }

            }
            //nguyên liệu
            count = Panel_Material.Controls.Count;
            for (int i = 0; i < count; i++)
            {
                ctr = (KryptonButton)Panel_Material.Controls[i];
                DataRow zRow = User_Data.GetUserRoleIsAccess(SessionUser.UserLogin.Key, ctr.Tag.ToString()).Rows[0];
                if (Convert.ToInt32(zRow[0]) > 0)
                {
                    ctr.Enabled = true;
                }
                else
                {
                    ctr.BackColor = Color.FromArgb(0, 135, 156);
                    ctr.Enabled = false;
                    ctr.StateDisabled.Back.Color1 = Color.FromArgb(252, 248, 248);
                    ctr.StateDisabled.Border.Color1 = Color.FromArgb(198, 193, 193);

                }

            }
            // cấp đông
            count = Panel_Freezing.Controls.Count;
            for (int i = 0; i < count; i++)
            {
                ctr = (KryptonButton)Panel_Freezing.Controls[i];
                DataRow zRow = User_Data.GetUserRoleIsAccess(SessionUser.UserLogin.Key, ctr.Tag.ToString()).Rows[0];
                if (Convert.ToInt32(zRow[0]) > 0)
                {
                    ctr.Enabled = true;
                }
                else
                {
                    ctr.BackColor = Color.FromArgb(0, 135, 156);
                    ctr.Enabled = false;
                    ctr.StateDisabled.Back.Color1 = Color.FromArgb(252, 248, 248);
                    ctr.StateDisabled.Border.Color1 = Color.FromArgb(198, 193, 193);

                }

            }
            // đồ hộp
            count = Panel_Box.Controls.Count;
            for (int i = 0; i < count; i++)
            {
                ctr = (KryptonButton)Panel_Box.Controls[i];
                DataRow zRow = User_Data.GetUserRoleIsAccess(SessionUser.UserLogin.Key, ctr.Tag.ToString()).Rows[0];
                if (Convert.ToInt32(zRow[0]) > 0)
                {
                    ctr.Enabled = true;
                }
                else
                {
                    ctr.BackColor = Color.FromArgb(0, 135, 156);
                    ctr.Enabled = false;
                    ctr.StateDisabled.Back.Color1 = Color.FromArgb(252, 248, 248);
                    ctr.StateDisabled.Border.Color1 = Color.FromArgb(198, 193, 193);

                }

            }
            // kho tạm
            count = Panel_Temp.Controls.Count;
            for (int i = 0; i < count; i++)
            {
                ctr = (KryptonButton)Panel_Temp.Controls[i];
                DataRow zRow = User_Data.GetUserRoleIsAccess(SessionUser.UserLogin.Key, ctr.Tag.ToString()).Rows[0];
                if (Convert.ToInt32(zRow[0]) > 0)
                {
                    ctr.Enabled = true;
                }
                else
                {
                    ctr.BackColor = Color.FromArgb(0, 135, 156);
                    ctr.Enabled = false;
                    ctr.StateDisabled.Back.Color1 = Color.FromArgb(252, 248, 248);
                    ctr.StateDisabled.Border.Color1 = Color.FromArgb(198, 193, 193);

                }

            }
            // kho thach dua
            count = Panel_ThachDua.Controls.Count;
            for (int i = 0; i < count; i++)
            {
                ctr = (KryptonButton)Panel_ThachDua.Controls[i];
                DataRow zRow = User_Data.GetUserRoleIsAccess(SessionUser.UserLogin.Key, ctr.Tag.ToString()).Rows[0];
                if (Convert.ToInt32(zRow[0]) > 0)
                {
                    ctr.Enabled = true;
                }
                else
                {
                    ctr.BackColor = Color.FromArgb(0, 135, 156);
                    ctr.Enabled = false;
                    ctr.StateDisabled.Back.Color1 = Color.FromArgb(252, 248, 248);
                    ctr.StateDisabled.Border.Color1 = Color.FromArgb(198, 193, 193);

                }

            }
            // kho vườn ươm
            count = Panel_VuonUom.Controls.Count;
            for (int i = 0; i < count; i++)
            {
                ctr = (KryptonButton)Panel_VuonUom.Controls[i];
                DataRow zRow = User_Data.GetUserRoleIsAccess(SessionUser.UserLogin.Key, ctr.Tag.ToString()).Rows[0];
                if (Convert.ToInt32(zRow[0]) > 0)
                {
                    ctr.Enabled = true;
                }
                else
                {
                    ctr.BackColor = Color.FromArgb(0, 135, 156);
                    ctr.Enabled = false;
                    ctr.StateDisabled.Back.Color1 = Color.FromArgb(252, 248, 248);
                    ctr.StateDisabled.Border.Color1 = Color.FromArgb(198, 193, 193);

                }

            }
            // kho hủy
            count = Panel_Recycle.Controls.Count;
            for (int i = 0; i < count; i++)
            {
                ctr = (KryptonButton)Panel_Recycle.Controls[i];
                DataRow zRow = User_Data.GetUserRoleIsAccess(SessionUser.UserLogin.Key, ctr.Tag.ToString()).Rows[0];
                if (Convert.ToInt32(zRow[0]) > 0)
                {
                    ctr.Enabled = true;
                }
                else
                {
                    ctr.BackColor = Color.FromArgb(0, 135, 156);
                    ctr.Enabled = false;
                    ctr.StateDisabled.Back.Color1 = Color.FromArgb(252, 248, 248);
                    ctr.StateDisabled.Border.Color1 = Color.FromArgb(198, 193, 193);

                }

            }
            //nhân sự
            count = Panel_Human.Controls.Count;
            for (int i = 0; i < count; i++)
            {

                if (Panel_Human.Controls[i].GetType() == typeof(Label))
                {

                }
                else
                {
                    ctr = (KryptonButton)Panel_Human.Controls[i];
                    DataRow zRow = User_Data.GetUserRoleIsAccess(SessionUser.UserLogin.Key, ctr.Tag.ToString()).Rows[0];
                    if (Convert.ToInt32(zRow[0]) > 0)
                    {
                        ctr.Enabled = true;
                    }
                    else
                    {
                        ctr.BackColor = Color.FromArgb(0, 135, 156);
                        ctr.Enabled = false;
                        ctr.StateDisabled.Back.Color1 = Color.FromArgb(255, 255, 255);
                        ctr.StateDisabled.Border.Color1 = Color.FromArgb(198, 193, 193);
                    }
                }
            }
            //năng suất
            count = Panel_Production.Controls.Count;
            for (int i = 0; i < count; i++)
            {

                    ctr = (KryptonButton)Panel_Production.Controls[i];
                    DataRow zRow = User_Data.GetUserRoleIsAccess(SessionUser.UserLogin.Key, ctr.Tag.ToString()).Rows[0];
                    if (Convert.ToInt32(zRow[0]) > 0)
                    {
                        ctr.Enabled = true;
                    }
                    else
                    {
                        ctr.BackColor = Color.FromArgb(0, 135, 156);
                        ctr.Enabled = false;
                        ctr.StateDisabled.Back.Color1 = Color.FromArgb(255, 255, 255);
                        ctr.StateDisabled.Border.Color1 = Color.FromArgb(198, 193, 193);
                    }
            }

            //Tính lương
            count = Panel_Salary.Controls.Count;
            for (int i = 0; i < count; i++)
            {
                if (Panel_Salary.Controls[i].GetType() == typeof(Label))
                {

                }
                else
                {
                    ctr = (KryptonButton)Panel_Salary.Controls[i];
                    DataRow zRow = User_Data.GetUserRoleIsAccess(SessionUser.UserLogin.Key, ctr.Tag.ToString()).Rows[0];
                    if (Convert.ToInt32(zRow[0]) > 0)
                    {
                        ctr.Enabled = true;
                    }
                    else
                    {
                        ctr.BackColor = Color.FromArgb(0, 135, 156);
                        ctr.Enabled = false;
                        ctr.StateDisabled.Back.Color1 = Color.FromArgb(255, 255, 255);
                        ctr.StateDisabled.Border.Color1 = Color.FromArgb(198, 193, 193);
                    }
                }



            }
            //Bao cao
            count = Panel_Report.Controls.Count;
            for (int i = 0; i < count; i++)
            {
                if(Panel_Report.Controls[i].GetType()== typeof(Label))
                {

                }
                else
                {
                    ctr = (KryptonButton)Panel_Report.Controls[i];
                    DataRow zRow = User_Data.GetUserRoleIsAccess(SessionUser.UserLogin.Key, ctr.Tag.ToString()).Rows[0];
                    if (Convert.ToInt32(zRow[0]) > 0)
                    {
                        ctr.Enabled = true;
                    }
                    else
                    {
                        ctr.BackColor = Color.FromArgb(0, 135, 156);
                        ctr.Enabled = false;
                        ctr.StateDisabled.Back.Color1 = Color.FromArgb(255, 255, 255);
                        ctr.StateDisabled.Border.Color1 = Color.FromArgb(198, 193, 193);
                    }
                }

                

            }
        }


        int x = 3, y = 54, a = 3;
        private void timer1_Tick(object sender, EventArgs e)
        {
            x += a;
            txt_ChayChu.Location = new Point(x,y);
            if(x>=1366)
            {
                x = 3;
                txt_ChayChu.Location = new Point(x, y);
            }

        }
    }
}