﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TNS.WinApp
{
    public partial class Frm_SearchProduct : Form
    {
        public string _te = "";
        public string _ProductName = "";
        public string _ProductID = "";
        public string _UnitName = "";
        public Frm_SearchProduct()
        {
            InitializeComponent();
            txt_Product.KeyUp += Txt_Product_KeyUp;
        }
        private void Txt_Product_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                LV_Search.Focus();
                if (LV_Search.Items.Count > 0)
                    LV_Search.Items[0].Selected = true;
            }
            else
            {
                if (e.KeyCode != Keys.Enter)
                {
                    if (txt_Product.Text.Length > 0)
                    {
                        LV_Search.Visible = true;
                        LV_SearchLoadData();
                    }
                    else
                        LV_Search.Visible = false;
                }
            }
        }
        private void LV_Search_Layout(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã Sản Phẩm";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên Sản Phẩm";
            colHead.Width = 260;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Đơn vị";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);
        }
        private void Frm_SearchProduct_Load(object sender, EventArgs e)
        {
            txt_Product.Text = _te;
            LV_Search.Hide();
            LV_Search_Layout(LV_Search);
        }
        public void LV_SearchLoadData()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LV_Search;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            DataTable In_Table = new DataTable();

            In_Table = TNS.IVT.Product_Data.SearchList(txt_Product.Text);

            LV.Items.Clear();
            int n = In_Table.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.ForeColor = Color.Navy;
                DataRow nRow = In_Table.Rows[i];
                lvi.Tag = nRow["ProductKey"].ToString();
                lvi.BackColor = Color.White; ;

                lvi.ImageIndex = 0;
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["ProductID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvi.ImageIndex = 0;
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["ProductName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvi.ImageIndex = 0;
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["UnitName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);


                LV.Items.Add(lvi);

            }

            this.Cursor = Cursors.Default;

        }
        private void LV_Search_ItemActivate(object sender, EventArgs e)
        {
            _ProductID = LV_Search.SelectedItems[0].SubItems[1].Text;
            _ProductName = LV_Search.SelectedItems[0].SubItems[2].Text;
            _UnitName = LV_Search.SelectedItems[0].SubItems[3].Text;
            this.Close();

        }
    }
}
