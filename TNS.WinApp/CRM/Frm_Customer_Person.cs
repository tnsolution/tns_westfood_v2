﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.CRM;
using TNS.LOC;
using TNS.LOC;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Customer_Person : Form
    {
        private string _PersonKey = "";
        private string _CustomerKey = "";
        private int _Ward = 0;
        private int _Province = 0;
        private int _Country = 0;
        private int _District = 0;
        private string _Category = "Thường trú";
        private int _Slug = 0;
        public int Slug
        {
            get
            {
                return _Slug;
            }

            set
            {
                _Slug = value;
            }
        }
        private Customer_Person_Object _Cus_Obj;
        private List<Address_Info> _ListAdress;

        public Frm_Customer_Person()
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            Utils.DoubleBuffered(GV_Address, true);
            Utils.DrawGVStyle(ref GV_Address);
            Utils.DrawLVStyle(ref LV_Customer);
            btnClose.Click += btnClose_Click;
            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            btn_Add.Click += Btn_Add_Click;
            //btn_Update.Click += Btn_Update_Click;
            LV_Customer.Click += LV_Customer_Click;
            GV_Address_Layout();
            GV_Address.CellEndEdit += GV_Address_CellEndEdit;
            GV_Address.EditingControlShowing += GV_Address_EditingControlShowing;
            GV_Address.KeyDown += GV_Address_KeyDown;
            btn_Del.Click += Btn_Del_Click;
            btn_New.Click += Btn_New_Click;
            btn_Search.Click += Btn_Search_Click;

            Utils.SizeLastColumn_LV(LV_Customer);
        }

        private void Frm_Customer_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            Check_RoleForm();

            dte_Birthday.Value = SessionUser.Date_Work;
            dte_IssueDate.Value = SessionUser.Date_Work;
            if (txt_Slug.Text == "Cá Nhân")
            {
                Slug = 1;
            }
            LV_Layout(LV_Customer);
            LV_LoadData();
            Load_Data();
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
        }

        #region ----- Left -----

        #region ----- Design Layout----
        private void LV_Layout(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã Khách hàng";
            colHead.Width = 110;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên Khách Hàng";
            colHead.Width = 350;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số điện thoại";
            colHead.Width = 110;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

        }
        private void LV_LoadData()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LV_Customer;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            DataTable In_Table = new DataTable();

            In_Table = Customer_Data.Search(txt_Search.Text.Trim(), _Slug);

            LV.Items.Clear();
            int n = In_Table.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                DataRow nRow = In_Table.Rows[i];

                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.Tag = nRow["CustomerKey"];
                lvi.ForeColor = Color.Navy;

                lvi.BackColor = Color.White;
                lvi.ImageIndex = 0;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["CustomerID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["CustomerName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["Phone"].ToString().Trim();
                lvi.SubItems.Add(lvsi);


                //lvsi = new ListViewItem.ListViewSubItem();
                //lvsi.Text = nRow["PersonalKey"].ToString().Trim();
                //lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }

            this.Cursor = Cursors.Default;

        }
        #endregion

        #region ----- Process Event -----
        private void LV_Customer_Click(object sender, EventArgs e)
        {
            _CustomerKey = LV_Customer.SelectedItems[0].Tag.ToString();
            Load_Data();
        }
        #endregion

        #region ----- Process Data -----
        private void Load_Data()
        {
            txt_FullName.BackColor = Color.FromArgb(255, 255, 225);
            txt_IssuePlace.BackColor = Color.FromArgb(255, 255, 225);

            _Cus_Obj = new Customer_Person_Object(_CustomerKey);
            txt_Slug.Text = "Cá Nhân";
            _PersonKey = _Cus_Obj.PersoObject.Key;
            txt_IssuePlace.Text = _Cus_Obj.CustomerID;
            txt_FullName.Text = _Cus_Obj.PersoObject.FullName;
            if (_Cus_Obj.PersoObject.Gender == 1)
            {
                rdo_Male.Checked = true;
            }
            if (_Cus_Obj.PersoObject.Gender == 0)
            {
                rdo_Female.Checked = true;
            }
            if (_Cus_Obj.PersoObject.BirthDay != DateTime.MinValue)
            {
                dte_Birthday.Value = _Cus_Obj.PersoObject.BirthDay;
            }
            else
            {
                dte_Birthday.Value = DateTime.Now;
            }
            txt_HomeTown.Text = _Cus_Obj.PersoObject.HomeTown;
            txt_IssueID.Text = _Cus_Obj.PersoObject.IssueID;

            if (_Cus_Obj.PersoObject.IssueDate != DateTime.MinValue)
            {
                dte_IssueDate.Value = _Cus_Obj.PersoObject.IssueDate;
            }
            else
            {
                dte_IssueDate.Value = DateTime.Now;
            }
            txt_IssuePlace.Text = _Cus_Obj.PersoObject.IssuePlace;
            txt_TaxCode.Text = _Cus_Obj.PersoObject.TaxCode;
            txt_Email.Text = _Cus_Obj.PersoObject.Email;
            txt_Phone.Text = _Cus_Obj.PersoObject.Phone;
            txt_BankCode.Text = _Cus_Obj.PersoObject.BankCode;
            txt_AddressBank.Text = _Cus_Obj.PersoObject.AddressBank;
            txt_Description.Text = _Cus_Obj.PersoObject.Description;
            GV_Product_LoadData();
            txt_FullName.Focus();
        }
        private void GV_Product_LoadData()
        {
            GV_Address.Rows.Clear();
            for (int i = 1; i <= 15; i++)
            {
                GV_Address.Rows.Add();
            }
            for (int i = 0; i < _Cus_Obj.PersoObject.List_Address.Count; i++)
            {
                Address_Info zAddress = (Address_Info)_Cus_Obj.PersoObject.List_Address[i];
                GV_Address.Rows[i].Tag = zAddress;
                GV_Address.Rows[i].Cells["No"].Value = (i + 1).ToString();
                GV_Address.Rows[i].Cells["Country"].Value = zAddress.CountryName;
                GV_Address.Rows[i].Cells["Country"].Tag = zAddress.CountryKey;
                GV_Address.Rows[i].Cells["Province"].Value = zAddress.ProvinceName;
                GV_Address.Rows[i].Cells["Province"].Tag = zAddress.ProvinceKey;
                GV_Address.Rows[i].Cells["District"].Value = zAddress.DistrictName;
                GV_Address.Rows[i].Cells["District"].Tag = zAddress.DistrictKey;
                GV_Address.Rows[i].Cells["Ward"].Value = zAddress.WardName;
                GV_Address.Rows[i].Cells["Ward"].Tag = zAddress.WardKey;
                GV_Address.Rows[i].Cells["Address"].Value = zAddress.Address;
                if (zAddress.CategoryKey == 1)
                    GV_Address.Rows[i].Cells["Category"].Value = "Thường trú";
                else if (zAddress.CategoryKey == 2)
                    GV_Address.Rows[i].Cells["Category"].Value = "Tạm trú";
                else
                    GV_Address.Rows[i].Cells["Category"].Value = "";


            }
        }
        #endregion
        #endregion

        #region -----  Right   ------

        #region ------Desgin Layout-------

        private void GV_Address_Layout()
        {
            // Setup Column 
            GV_Address.Columns.Add("No", "STT");
            GV_Address.Columns.Add("Country", "Quốc Gia");
            GV_Address.Columns.Add("Province", "Tỉnh/Thành");
            GV_Address.Columns.Add("District", "Quận/Huyện");
            GV_Address.Columns.Add("Ward", "Phường/Xã");
            GV_Address.Columns.Add("Address", "Địa chỉ");

            DataGridViewComboBoxColumn combox = new DataGridViewComboBoxColumn();
            //combox.DataSource = ztbCategory;
            combox.HeaderText = "Loại";
            combox.Name = "Category";
            combox.Items.Add("Thường trú");
            combox.Items.Add("Tạm trú");
            GV_Address.Columns.Add(combox);

            GV_Address.Columns["No"].Width = 40;
            GV_Address.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV_Address.Columns["No"].ReadOnly = true;

            GV_Address.Columns["Country"].Width = 130;
            GV_Address.Columns["Country"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;


            GV_Address.Columns["Province"].Width = 130;
            GV_Address.Columns["Province"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV_Address.Columns["District"].Width = 130;
            GV_Address.Columns["District"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV_Address.Columns["Ward"].Width = 130;
            GV_Address.Columns["Ward"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV_Address.Columns["Address"].Width = 330;
            GV_Address.Columns["Address"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV_Address.Columns["Category"].Width = 130;
            GV_Address.Columns["Category"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV_Address.BackgroundColor = Color.White;
            GV_Address.GridColor = Color.FromArgb(227, 239, 255);
            GV_Address.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV_Address.DefaultCellStyle.ForeColor = Color.Navy;
            GV_Address.DefaultCellStyle.Font = new Font("Arial", 9);
           // GV_Address.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(0, 90, 200);
            GV_Address.ColumnHeadersDefaultCellStyle.ForeColor = Color.FromArgb(43, 43, 43);
            GV_Address.EnableHeadersVisualStyles = false;

            GV_Address.AllowUserToResizeRows = false;
            GV_Address.AllowUserToResizeColumns = true;

            GV_Address.RowHeadersVisible = false;
            GV_Address.AllowUserToAddRows = true;
            GV_Address.AllowUserToDeleteRows = false;

            // setup Height Header
            //GV_Products.ColumnHeadersHeight = GV_Products.ColumnHeadersHeight * 3 / 2;
            GV_Address.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            GV_Address.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;

        }
        private void GV_Address_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow zRowEdit = GV_Address.Rows[e.RowIndex];
            Address_Info zAddress;
            if (GV_Address.Rows[e.RowIndex].Tag == null)
            {
                zAddress = new Address_Info();
                GV_Address.Rows[e.RowIndex].Tag = zAddress;
            }
            else
            {
                zAddress = (Address_Info)GV_Address.Rows[e.RowIndex].Tag;
            }
            bool zIsChangeValued = false;
            string zName = "";
            switch (e.ColumnIndex)
            {
                case 1:
                    if (zRowEdit.Cells[1].Value == null)
                    {
                        zRowEdit.Cells[1].ErrorText = "Không tìm thấy quốc gia này!.";

                        zAddress.CountryKey = 0;
                        zAddress.CountryName = "";
                        zAddress.ProvinceKey = 0;
                        zAddress.ProvinceName = "";
                        zAddress.DistrictKey = 0;
                        zAddress.DistrictName = "";
                        zAddress.WardKey = 0;
                        zAddress.WardName = "";
                        zAddress.Address = "";

                        zRowEdit.Cells[2].Value = "";
                        zRowEdit.Cells[3].Value = "";
                        zRowEdit.Cells[4].Value = "";
                        zRowEdit.Cells[5].Value = "";

                        zIsChangeValued = true;
                    }
                    else
                    {
                        zRowEdit.Cells[1].ErrorText = "";
                        zName = GV_Address.CurrentCell.Value.ToString();
                        Country_Info zCountry = new Country_Info();
                        zCountry.Get_Country_Info(zName);
                        //Không tìm được Quốc gia
                        if (zCountry.Key == 0)
                        {
                            zRowEdit.Cells[1].ErrorText = "Không tìm thấy quốc gia này!.";
                            zAddress.CountryKey = 0;
                            zAddress.CountryName = "";
                            zAddress.ProvinceKey = 0;
                            zAddress.ProvinceName = "";
                            zAddress.DistrictKey = 0;
                            zAddress.DistrictName = "";
                            zAddress.WardKey = 0;
                            zAddress.WardName = "";
                            zAddress.Address = "";

                            zRowEdit.Cells[2].Value = "";
                            zRowEdit.Cells[3].Value = "";
                            zRowEdit.Cells[4].Value = "";
                            zRowEdit.Cells[5].Value = "";

                            zIsChangeValued = true;
                        }
                        //Tìm được quốc gia mới và Giá trị đưa vào # giá trị cũ
                        if (zCountry.Key != 0 && zCountry.Key != zAddress.CountryKey)
                        {
                            zAddress.CountryKey = zCountry.Key;
                            zAddress.CountryName = zCountry.CountryName;

                            zAddress.ProvinceKey = 0;
                            zAddress.ProvinceName = "";
                            zAddress.DistrictKey = 0;
                            zAddress.DistrictName = "";
                            zAddress.WardKey = 0;
                            zAddress.WardName = "";
                            zAddress.Address = "";

                            zRowEdit.Cells[2].Value = "";
                            zRowEdit.Cells[3].Value = "";
                            zRowEdit.Cells[4].Value = "";
                            zRowEdit.Cells[5].Value = "";
                            zIsChangeValued = true;
                        }
                        else
                        {
                            zAddress.CountryKey = zCountry.Key;
                            zAddress.CountryName = zCountry.CountryName;

                            zIsChangeValued = false;
                        }

                    }
                    break;
                case 2:
                    if (zRowEdit.Cells[2].Value == null)
                    {
                        zRowEdit.Cells[2].ErrorText = "Không tìm thấy tỉnh/thành phố này!.";

                        zAddress.ProvinceKey = 0;
                        zAddress.ProvinceName = "";
                        zAddress.DistrictKey = 0;
                        zAddress.DistrictName = "";
                        zAddress.WardKey = 0;
                        zAddress.WardName = "";
                        zAddress.Address = "";

                        zRowEdit.Cells[3].Value = "";
                        zRowEdit.Cells[4].Value = "";
                        zRowEdit.Cells[5].Value = "";

                        zIsChangeValued = true;
                    }
                    else
                    {
                        zRowEdit.Cells[2].ErrorText = "";
                        zName = GV_Address.CurrentCell.Value.ToString();
                        Province_Info zProvince = new Province_Info();
                        zProvince.Get_Province_Info(zName);
                        // Không Tìm được tỉnh thành mới
                        if (zProvince.Key == 0)
                        {
                            zRowEdit.Cells[2].ErrorText = "Không tìm thấy tỉnh/thành phố này!.";

                            zAddress.ProvinceKey = 0;
                            zAddress.ProvinceName = "";
                            zAddress.DistrictKey = 0;
                            zAddress.DistrictName = "";
                            zAddress.WardKey = 0;
                            zAddress.WardName = "";
                            zAddress.Address = "";

                            zRowEdit.Cells[3].Value = "";
                            zRowEdit.Cells[4].Value = "";
                            zRowEdit.Cells[5].Value = "";

                            zIsChangeValued = true;
                        }
                        //Tìm được tỉnh thành mới và Giá trị đưa vào # giá trị cũ
                        if (zProvince.Key != 0 && zProvince.Key != zAddress.ProvinceKey)
                        {
                            zAddress.ProvinceKey = zProvince.Key;
                            zAddress.ProvinceName = zProvince.ProvinceName;

                            zAddress.DistrictKey = 0;
                            zAddress.DistrictName = "";
                            zAddress.WardKey = 0;
                            zAddress.WardName = "";
                            zAddress.Address = "";

                            zRowEdit.Cells[3].Value = "";
                            zRowEdit.Cells[4].Value = "";
                            zRowEdit.Cells[5].Value = "";

                            zIsChangeValued = true;
                        }
                        else
                        {
                            zAddress.ProvinceKey = zProvince.Key;
                            zAddress.ProvinceName = zProvince.ProvinceName;

                            zIsChangeValued = false;
                        }
                    }
                    break;
                case 3:
                    if (zRowEdit.Cells[3].Value == null)
                    {
                        zRowEdit.Cells[3].ErrorText = "Không tìm thấy quận/huyện này!.";

                        zAddress.DistrictKey = 0;
                        zAddress.DistrictName = "";
                        zAddress.WardKey = 0;
                        zAddress.WardName = "";
                        zAddress.Address = "";

                        zRowEdit.Cells[4].Value = "";
                        zRowEdit.Cells[5].Value = "";

                        zIsChangeValued = true;
                    }
                    else
                    {
                        zRowEdit.Cells[3].ErrorText = "";
                        zName = GV_Address.CurrentCell.Value.ToString();
                        District_Info zDistrict = new District_Info();
                        zDistrict.Get_District_Info(zName, zAddress.ProvinceKey);

                        //Không Tìm được quận huyện mới
                        if (zDistrict.Key == 0)
                        {
                            zRowEdit.Cells[3].ErrorText = "Không tìm thấy quận/huyện này!";

                            zAddress.DistrictKey = 0;
                            zAddress.DistrictName = "";
                            zAddress.WardKey = 0;
                            zAddress.WardName = "";
                            zAddress.Address = "";

                            zRowEdit.Cells[4].Value = "";
                            zRowEdit.Cells[5].Value = "";

                            zIsChangeValued = true;
                        }
                        //Tìm được Quận /huyện mới và Giá trị đưa vào # giá trị cũ
                        if (zDistrict.Key != 0 && zDistrict.Key != zAddress.DistrictKey)
                        {

                            zAddress.DistrictName = zDistrict.DistrictName;
                            zAddress.DistrictKey = zDistrict.Key;

                            zAddress.WardKey = 0;
                            zAddress.WardName = "";
                            zAddress.Address = "";

                            zRowEdit.Cells[4].Value = "";
                            zRowEdit.Cells[5].Value = "";

                            zIsChangeValued = true;
                        }
                        else
                        {
                            zAddress.DistrictName = zDistrict.DistrictName;
                            zAddress.DistrictKey = zDistrict.Key;

                            zIsChangeValued = false;
                        }
                    }
                    break;
                case 4:
                    if (zRowEdit.Cells[4].Value == null)
                    {
                        zRowEdit.Cells[4].ErrorText = "Không tìm thấy phường xã này!.";

                        zAddress.WardKey = 0;
                        zAddress.WardName = "";
                        zAddress.Address = "";

                        zRowEdit.Cells[5].Value = "";

                        zIsChangeValued = true;
                    }
                    else
                    {
                        zRowEdit.Cells[4].ErrorText = "";
                        zName = GV_Address.CurrentCell.Value.ToString();
                        Ward_Info zWard = new Ward_Info();
                        zWard.Get_Ward_Info(zName, zAddress.DistrictKey);
                        //Không Tìm được phường xã mới
                        if (zWard.Key == 0)
                        {
                            zRowEdit.Cells[4].ErrorText = "Không tìm thấy phường xã này!.";

                            zAddress.WardKey = 0;
                            zAddress.WardName = "";
                            zAddress.Address = "";

                            zRowEdit.Cells[5].Value = "";

                            zIsChangeValued = true;
                        }
                        //Tìm được phường xã mới và Giá trị đưa vào # giá trị cũ
                        if (zWard.Key != 0 && zWard.Key != zAddress.WardKey)
                        {

                            zAddress.WardName = zWard.WardName;
                            zAddress.WardKey = zWard.Key;

                            zAddress.Address = "";

                            zRowEdit.Cells[5].Value = "";

                            zIsChangeValued = true;
                        }
                        else
                        {
                            zAddress.WardName = zWard.WardName;
                            zAddress.WardKey = zWard.Key;

                            zIsChangeValued = false;
                        }
                    }

                    break;
                case 5:
                    if (zRowEdit.Cells[5].Value == null)
                    {
                        zRowEdit.Cells[5].ErrorText = "Địa chỉ không được bỏ trống!.";

                        zAddress.Address = "";

                        zRowEdit.Cells[5].Value = "";

                        zIsChangeValued = true;
                    }
                    else
                    {
                        zRowEdit.Cells[5].ErrorText = "";
                        zName = GV_Address.CurrentCell.Value.ToString();
                        if (zName == "")
                        {
                            zRowEdit.Cells[5].ErrorText = "Địa chỉ không được bỏ trống!.";
                            zIsChangeValued = true;
                        }
                        else if (zName != zAddress.Address)
                        {
                            zAddress.Address = zName.Trim();
                            zIsChangeValued = true;
                        }
                        else
                        {
                            zAddress.Address = zName.Trim();
                            zIsChangeValued = false;
                        }
                    }
                    if (zAddress.Key == 0)
                    {
                        zRowEdit.Cells[6].Value = "Thường trú";
                        zAddress.CategoryKey = 1;
                    }

                    break;
                case 6:
                    zRowEdit.Cells[5].ErrorText = "";
                    if (GV_Address.CurrentCell.Value == null)
                    {
                        zAddress.CategoryKey = 0;
                        zRowEdit.Cells[6].ErrorText = "Chọn hình thức cư trú!.";
                        zIsChangeValued = true;
                    }
                    else
                    {

                        zName = GV_Address.CurrentCell.Value.ToString();
                        if (zName == "Thường trú")
                        {
                            zAddress.CategoryKey = 1;
                        }
                        else if (zName == "Tạm trú")
                        {
                            zAddress.CategoryKey = 2;
                        }
                        else
                        {
                            zRowEdit.Cells[6].ErrorText = "Chọn hình thức cư trú!.";
                            zAddress.CategoryKey = 0;
                        }
                        zIsChangeValued = true;
                    }
                    break;

            }
            if (zIsChangeValued)
            {
                zAddress.RecordStatus = 1;
                // ShowProductInGridView_Address(e.RowIndex);
                if (zAddress.Key != 0)
                    zAddress.RecordStatus = 2;
                else
                    zAddress.RecordStatus = 1;
            }

            resetRow = true;
            currentRow = e.RowIndex;
            GV_Address.SelectionChanged += GV_Address_SelectionChanged;
        }
        private int currentRow;
        private bool resetRow = false;
        private void GV_Address_SelectionChanged(object sender, EventArgs e)
        {
            if (resetRow)
            {
                resetRow = false;
                GV_Address.CurrentCell = GV_Address.Rows[currentRow].Cells[0];
            }
        }
        private void GV_Address_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            Address_Info zAddress = new Address_Info();
            if (GV_Address.CurrentRow.Tag == null)
            {
                zAddress = new Address_Info();
                GV_Address.CurrentRow.Tag = zAddress;
            }
            else
            {
                zAddress = (Address_Info)GV_Address.CurrentRow.Tag;
            }
            _Country = zAddress.CountryKey;
            _Province = zAddress.ProvinceKey;
            _District = zAddress.DistrictKey;
            _Ward = zAddress.DistrictKey;

            switch (GV_Address.CurrentCell.ColumnIndex)
            {
                case 1: // autocomplete for product
                    DataGridViewTextBoxEditingControl te = (DataGridViewTextBoxEditingControl)e.Control;
                    te.AutoCompleteMode = AutoCompleteMode.None;
                    LoadDataToToolbox.AutoCompleteTextBox(te, "SELECT CountryName FROM LOC_Country WHERE CountryKey = 237 AND RecordStatus <99");
                    e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);
                    break;
                case 2: // autocomplete for product
                    te = (DataGridViewTextBoxEditingControl)e.Control;
                    te.AutoCompleteMode = AutoCompleteMode.None;
                    LoadDataToToolbox.AutoCompleteTextBox(te, "SELECT ProvinceName FROM LOC_Province WHERE CountryKey = '" + _Country + "' AND RecordStatus <99");
                    e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);
                    break;
                case 3:
                    te = (DataGridViewTextBoxEditingControl)e.Control;
                    te.AutoCompleteMode = AutoCompleteMode.None;
                    LoadDataToToolbox.AutoCompleteTextBox(te, "SELECT DistrictName FROM LOC_District WHERE ProvinceKey = '" + _Province + "' AND RecordStatus <99");

                    e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);
                    break;
                case 4:
                    te = (DataGridViewTextBoxEditingControl)e.Control;
                    te.AutoCompleteMode = AutoCompleteMode.None;
                    LoadDataToToolbox.AutoCompleteTextBox(te, "SELECT WardName FROM LOC_Ward WHERE DistrictKey = '" + _District + "' AND RecordStatus <99");
                    e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);
                    break;
                case 5:
                    te = (DataGridViewTextBoxEditingControl)e.Control;
                    te.AutoCompleteMode = AutoCompleteMode.None;
                    break;
            }
        }
        private void Control_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = false;
        }
        private void Caculate(int RowIndex)
        {

        }
        private void ShowProductInGridView_Address(int RowIndex)
        {

        }
        private void GV_Address_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                DialogResult dlr = MessageBox.Show("Bạn có xóa thông tin này ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlr == DialogResult.Yes)
                {
                    if (GV_Address.CurrentRow.Tag != null)
                    {
                        Address_Info zAddress = (Address_Info)GV_Address.CurrentRow.Tag;
                        GV_Address.CurrentRow.Visible = false;
                        zAddress.RecordStatus = 99;
                    }
                }
            }
        }
        #endregion

        #region ------SaveObject------
        private void SaveObject()
        {
            _Cus_Obj.Slug = Slug;
            _Cus_Obj.CustomerID = txt_CustomerID.Text.Trim();

            _Cus_Obj.PersoObject.Key = _PersonKey;
            _Cus_Obj.PersoObject.FullName = txt_FullName.Text.Trim();
            if (rdo_Male.Checked == true)
            {
                _Cus_Obj.PersoObject.Gender = 1;
            }
            else if (rdo_Female.Checked == true)
            {
                _Cus_Obj.PersoObject.Gender = 0;
            }
            _Cus_Obj.PersoObject.BirthDay = dte_Birthday.Value;
            _Cus_Obj.PersoObject.HomeTown = txt_HomeTown.Text.Trim();
            _Cus_Obj.PersoObject.IssueID = txt_IssueID.Text.Trim();
            _Cus_Obj.PersoObject.IssueDate = dte_IssueDate.Value;
            _Cus_Obj.PersoObject.IssuePlace = txt_IssuePlace.Text.Trim();
            _Cus_Obj.PersoObject.TaxCode = txt_TaxCode.Text.Trim();
            _Cus_Obj.PersoObject.Email = txt_Email.Text.Trim();
            _Cus_Obj.PersoObject.Phone = txt_Phone.Text.Trim();

            _Cus_Obj.PersoObject.AddressBank = txt_AddressBank.Text.Trim();
            _Cus_Obj.PersoObject.BankCode = txt_BankCode.Text.Trim();
            _Cus_Obj.PersoObject.Description = txt_Description.Text;

            _ListAdress = new List<Address_Info>();
            Address_Info zAddress;
            for (int i = 0; i < GV_Address.Rows.Count; i++)
            {
                if (GV_Address.Rows[i].Tag != null)
                {
                    zAddress = (Address_Info)GV_Address.Rows[i].Tag;
                    if (_Cus_Obj.Key == "" && zAddress.RecordStatus != 99)
                    {
                        zAddress.RecordStatus = 1;
                    }
                    _ListAdress.Add(zAddress);
                    _Cus_Obj.PersoObject.List_Address = _ListAdress;
                }
            }

            _Cus_Obj.SaveObject();
            _CustomerKey = _Cus_Obj.Key;
            string zMessage = TN_Message.Show(_Cus_Obj.Message);
            if (zMessage.Length == 0)
            {
                MessageBox.Show("Cập nhật thành công", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                LV_LoadData();
                Load_Data();
            }
            else
            {
                MessageBox.Show("Vui lòng kiểm tra lại", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        #endregion

        #region -------Process Event------
        private void Btn_Add_Click(object sender, EventArgs e)
        {

            if (CheckBeforeSave().Trim().Length == 0)
            {
                SaveObject();
            }
            else
            {
                MessageBox.Show("Vui lòng điền đầy đủ thông tin!.\n" + zAmountErr, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void Btn_Del_Click(object sender, EventArgs e)
        {
            DialogResult dlr = MessageBox.Show("Bạn có xóa thông tin này ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dlr == DialogResult.Yes)
            {
                _Cus_Obj.DeleteObject();
                string zMessage = TN_Message.Show(_Cus_Obj.Message);
                if (zMessage.Length == 0)
                {
                    MessageBox.Show("Xóa thành công", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    _CustomerKey = "";
                    LV_LoadData();
                    Load_Data();
                }
                else
                {
                    MessageBox.Show("Vui lòng kiểm tra lại", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }


            }
        }
        private void Btn_New_Click(object sender, EventArgs e)
        {
            _CustomerKey = "";
            // _PersonKey = "";
            Load_Data();
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            LV_LoadData();
        }
        #endregion

        #region[----Progess------]

        string zAmountErr = "";
        private string CheckBeforeSave()
        {
            zAmountErr = "";
            int zAddress = 0;
            if (txt_FullName.Text.Length == 0)
            {
                txt_FullName.BackColor = Color.FromArgb(230, 100, 100);
                zAmountErr += "-Họ và tên đầy đủ.\n";
            }
            else
            {
                txt_FullName.BackColor = Color.FromArgb(255, 255, 225);
            }
            if (txt_IssuePlace.Text.Trim().Length == 0)
            {
                txt_IssuePlace.BackColor = Color.FromArgb(230, 100, 100);
                zAmountErr += "-Mã khách hàng.\n";
            }
            else
            {
                txt_IssuePlace.BackColor = Color.FromArgb(255, 255, 225);
            }
            for (int i = 0; i < GV_Address.Rows.Count - 1; i++)
            {
                if (GV_Address.Rows[i].Tag != null)
                {
                    if ((GV_Address.Rows[i].Cells["Country"].Value == null || GV_Address.Rows[i].Cells["Country"].Value == "" || GV_Address.Rows[i].Cells["Country"].ErrorText != "") && GV_Address.Rows[i].Visible == true)
                    {
                        GV_Address.Rows[i].Cells["Country"].ErrorText = "Không tìm thấy quốc gia này!.";
                        zAddress++;
                    }
                    if ((GV_Address.Rows[i].Cells["Province"].Value == null || GV_Address.Rows[i].Cells["Province"].Value == "" || GV_Address.Rows[i].Cells["Province"].ErrorText != "") && GV_Address.Rows[i].Visible == true)
                    {
                        GV_Address.Rows[i].Cells["Province"].ErrorText = "Không tìm thấy tỉnh/thành phố này!.";
                        zAddress++;
                    }
                    if ((GV_Address.Rows[i].Cells["District"].Value == null || GV_Address.Rows[i].Cells["District"].Value == "" || GV_Address.Rows[i].Cells["District"].ErrorText != "") && GV_Address.Rows[i].Visible == true)
                    {
                        GV_Address.Rows[i].Cells["District"].ErrorText = "Không tìm thấy quận/huyện này!.";
                        zAddress++;
                    }
                    if ((GV_Address.Rows[i].Cells["Ward"].Value == null || GV_Address.Rows[i].Cells["Ward"].Value == "" || GV_Address.Rows[i].Cells["Ward"].ErrorText != "") && GV_Address.Rows[i].Visible == true)
                    {
                        GV_Address.Rows[i].Cells["Ward"].ErrorText = "Không tìm thấy phường/xã này!.";
                        zAddress++;
                    }
                    if ((GV_Address.Rows[i].Cells["Address"].Value == null || GV_Address.Rows[i].Cells["Address"].Value == "" || GV_Address.Rows[i].Cells["Address"].ErrorText != "") && GV_Address.Rows[i].Visible == true)
                    {
                        GV_Address.Rows[i].Cells["Address"].ErrorText = "Địa chỉ không được bỏ trống!.";
                        zAddress++;
                    }
                }

            }
            if (zAddress > 0)
            {
                zAmountErr += "-Địa chỉ liên hệ.\n";
            }
            return zAmountErr;
        }


        #endregion

        #endregion

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Normal)
            {
                this.WindowState = FormWindowState.Maximized;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
                this.StartPosition = FormStartPosition.CenterScreen;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
            }
            if (Role.RoleAdd == 0 && Role.RoleEdit == 0)
            {
                btn_New.Enabled = false;
                btn_Add.Enabled = false;
            }
            if (Role.RoleAdd == 0 && Role.RoleEdit == 1)
            {
                btn_New.Enabled = false;
                btn_Add.Enabled = true;
            }
            if (Role.RoleDel == 0)
            {
                btn_Del.Enabled = false;
            }
        }
        #endregion
    }
}
