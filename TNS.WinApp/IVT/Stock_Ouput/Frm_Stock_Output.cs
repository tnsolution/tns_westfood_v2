﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;
using TNS.CRM;
using TNS.IVT;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Stock_Output : Form
    {
        #region ----- Filed Name -----
        private string _FormatQuantity = "#,###,##0.00";
        private IFormatProvider _FormatProviderQuantity = new CultureInfo("en-US", true);
        private string _CustomerKey = "";
        private int styleForm = 0;
        public int StyleForm
        {
            get
            {
                return styleForm;
            }

            set
            {
                styleForm = value;
            }
        }
        private int _WareHouse = 0;
        public int WareHouse
        {
            get
            {
                return _WareHouse;
            }

            set
            {
                _WareHouse = value;
            }
        }
        private string _WarehouseRole = "";
        public string WareHouseRole
        {
            get
            {
                return _WarehouseRole;
            }

            set
            {
                _WarehouseRole = value;
            }
        }
        private int _Status = 0;
        public int Status
        {
            get
            {
                return _Status;
            }

            set
            {
                _Status = value;
            }
        }

        public string RoleID
        {
            get
            {
                return _RoleID;
            }

            set
            {
                _RoleID = value;
            }
        }

        private string _OrderID = "";
        private string _RoleID = "";
        private List<Stock_Output_Products_Info> _List_Ouput;
        private Stock_Output_Object _Order_Output;
        private Customer_Info _Customer;
        private int k = 1;
        private string _ProductName = "";
        private string _ProductID = "";
        private string _UnitName = "";
        #endregion

        #region [Biến New - Copy - Update]
        private int _New = 0;
        private int _Copy = 0;
        private int _Update = 0;
        #endregion
        public Frm_Stock_Output()
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            Utils.DoubleBuffered(GVData, true);
            Utils.DoubleBuffered(GVSum, true);
            Utils.DoubleBuffered(GVNote, true);
            Utils.DrawGVStyle(ref GVData);
            Utils.DrawGVStyle(ref GVSum);
            Utils.DrawGVStyle(ref GVNote);
            Utils.DrawLVStyle(ref LVData);

            GVData_Layout();
            GVNote_Layout();
            GVSum_Layout();
            //-----List Order
            LVData.Click += LVData_Click;
            LVData.KeyDown += LVData_KeyDown;
            //-----GridView
        
            GVNote.KeyDown += GVNote_KeyDown;          
            GVData.KeyDown += GVData_KeyDown;
            GVData.CellEndEdit += GVData_CellEndEdit;
            GVData.EditingControlShowing += GVData_EditingControlShowing;
            GVData.CellClick += GVData_CellClick;
            //-----Button
            btn_Search.Click += btn_Search_Click;
            btn_Save.Click += btn_Save_Click;
            btn_Del.Click += btn_Del_Click;
            btn_Copy.Click += btn_Copy_Click;
            btn_New.Click += btn_New_Click;
            btn_Send.Click += btn_Send_Click;
            btn_Deny.Click += btn_Deny_Click;
            btn_Approve.Click += btn_Approve_Click;
            btn_Search_Customer.Click += btn_Search_Customer_Click;
            btn_Post.Click += btn_Post_Click;
            btn_Import.Click += btn_Import_Click;
            btn_Edit.Click += btn_Edit_Click;

            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            btn_Edit.Enabled = false;

            Utils.SizeLastColumn_LV(LVData);
        }

        private void btn_Import_Click(object sender, EventArgs e)
        {
            Frm_Import_Output frm = new Frm_Import_Output();
            frm.ShowDialog();
        }

        private void Frm_Stock_Output_Load(object sender, EventArgs e)
        {
            _New = 1;
            dte_FromDate.Value = SessionUser.Date_Work;
            dte_ToDate.Value = SessionUser.Date_Work;
            dte_OrderDate.Value = SessionUser.Date_Work;
            k = 1;
            LV_Stock_Output_Layout(LVData);
            LV_Stock_Output_LoadData();
            LoadDataWareHouse();
            _OrderID = "";
            _Order_Output = new Stock_Output_Object(_OrderID);
            Check_Record();
            LoadTexbox();
            if (_OrderID.Length == 0)
                btn_Send.Enabled = false;
            else
                btn_Send.Enabled = true;

            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);

            this.Bounds = Screen.PrimaryScreen.WorkingArea;
        }
        private void LoadTexbox()
        {
            if (StyleForm == 1 && Status == 1)
            {
                LoadDataToToolbox.AutoCompleteTextBox(txt_Search, "SELECT OrderID FROM IVT_Stock_Output WHERE WarehouseKey = '" + WareHouse + "' AND WarehouseLocal > 0  AND RecordStatus <> 99 ORDER BY OrderDate DESC ");
                this.Text = "PHIẾU XUẤT KHO CHO KHÁCH HÀNG: " + cbo_Warehouse.Text.ToUpper() + "";
                txtTitle.Text = "PHIẾU XUẤT KHO CHO KHÁCH HÀNG : " + cbo_Warehouse.Text.ToUpper() + ""; ;

            }
            if (StyleForm == 2 && Status == 2)
            {
                LoadDataToToolbox.AutoCompleteTextBox(txt_Search, "SELECT OrderID FROM dbo.IVT_Stock_Output WHERE WarehouseKey = '" + WareHouse + "' AND WarehouseLocal > 0 AND RecordStatus = 41 ORDER BY OrderDate DESC , OrderID DESC");
                this.Text = "DUYỆT PHIẾU XUẤT KHO CHO KHÁCH HÀNG: " + cbo_Warehouse.Text.ToUpper() + "";
                txtTitle.Text = "DUYỆT PHIẾU XUẤT KHO CHO KHÁCH HÀNG: " + cbo_Warehouse.Text.ToUpper() + "";
            }

        }

        //--- Check Buttons


        #region ----- Panel Left ------

        #region ----- List Items ------
        private void LV_Stock_Output_Layout(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "No";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ngày";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã phiếu";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tình trạng";
            colHead.Width = 70;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);


        }

        public void LV_Stock_Output_LoadData()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LVData;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            DataTable In_Table = new DataTable();

            if (StyleForm == 1 && Status == 1)
            {
                In_Table = Stock_Data.ListOuput_NotApprove_Customer_Search(WareHouse, txt_Search.Text.Trim(), dte_FromDate.Value, dte_ToDate.Value);
            }
            if (StyleForm == 2 && Status == 2)
            {
                In_Table = Stock_Data.ListOutput_Approve_Customer_Search(WareHouse, txt_Search.Text.Trim(), dte_FromDate.Value, dte_ToDate.Value);
            }

            LV.Items.Clear();
            int n = In_Table.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.ForeColor = Color.Navy;
                DataRow nRow = In_Table.Rows[i];
                int RecordStatus = (int)nRow["RecordStatus"];
                string zStatus = "";
                if (RecordStatus == 41)
                {
                    zStatus = "Chờ duyệt";
                    lvi.ForeColor = Color.Gray;
                }
                if (RecordStatus == 42)
                {
                    zStatus = "Trả lại";
                    lvi.ForeColor = Color.Maroon;
                }
                if (RecordStatus == 43)
                {
                    zStatus = "Đã duyệt";
                    lvi.ForeColor = Color.Green;
                }
                if (RecordStatus == 49)
                {
                    zStatus = "Từ chối";
                    lvi.ForeColor = Color.Maroon;
                }


                lvi.BackColor = Color.White;

                lvi.ImageIndex = 0;

                DateTime z_trdate = (DateTime)nRow["OrderDate"];
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = z_trdate.ToString("dd/MM/yyyy");
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["OrderID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);



                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zStatus;
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);

            }

            this.Cursor = Cursors.Default;

        }
        public void LV_Stock_Output_LoadData_Search()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LVData;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            DataTable In_Table = new DataTable();

            if (StyleForm == 1 && Status == 1)
            {
                In_Table = Stock_Data.ListOuput_NotApprove_Customer_Search(WareHouse, txt_Search.Text.Trim(), dte_FromDate.Value, dte_ToDate.Value);
            }
            if (StyleForm == 2 && Status == 2)
            {
                In_Table = Stock_Data.ListOutput_Approve_Customer_Search(WareHouse, txt_Search.Text.Trim(), dte_FromDate.Value, dte_ToDate.Value);
            }

            LV.Items.Clear();
            int n = In_Table.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.ForeColor = Color.Navy;
                DataRow nRow = In_Table.Rows[i];
                int RecordStatus = (int)nRow["RecordStatus"];
                string zStatus = "";
                if (RecordStatus == 41)
                {
                    zStatus = "Chờ duyệt";
                    lvi.ForeColor = Color.Gray;
                }
                if (RecordStatus == 42)
                {
                    zStatus = "Trả lại";
                    lvi.ForeColor = Color.Maroon;
                }
                if (RecordStatus == 43)
                {
                    zStatus = "Đã duyệt";
                    lvi.ForeColor = Color.Green;
                }
                if (RecordStatus == 49)
                {
                    zStatus = "Từ chối";
                    lvi.ForeColor = Color.Maroon;
                }


                lvi.BackColor = Color.White;

                lvi.ImageIndex = 0;

                DateTime z_trdate = (DateTime)nRow["OrderDate"];
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = z_trdate.ToString("dd/MM/yyyy");
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["OrderID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);



                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zStatus;
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);

            }

            this.Cursor = Cursors.Default;

        }
        #endregion

        #region ----- Process Event -----
        private void LVData_Click(object sender, EventArgs e)
        {
            _Update = 1;
            _New = 0;
            _Copy = 0;
            k = 1;
            for (int i = 0; i < LVData.Items.Count; i++)
            {
                if (LVData.Items[i].Selected == true)
                {
                    LVData.Items[i].BackColor = Color.LightBlue; // highlighted item
                }
                else
                {
                    LVData.Items[i].BackColor = SystemColors.Window; // normal item
                }
            }
            txt_Order_ID.Enabled = true;
            dte_OrderDate.Enabled = true;
            txt_Description.Enabled = true;
            cbo_Warehouse.Enabled = true;

            txt_Customer_ID.Enabled = true;
            txt_Customer_Name.Enabled = true;
            _OrderID = LVData.SelectedItems[0].SubItems[2].Text;
            txt_Order_ID.Text = _OrderID;
            _Order_Output = new Stock_Output_Object(_OrderID);
            LoadOrderInfo();
            if (_OrderID.Length == 0)
                btn_Send.Enabled = false;
            else
                btn_Send.Enabled = true;
            for (int i = 0; i < GVData.Rows.Count - 1; i++)
            {
                if (GVData.Rows[i].Tag != null)
                {
                    k++;
                }
            }
            CheckReadOnlyTrue();
        }
        private void LVData_KeyDown(object sender, KeyEventArgs e)
        {
            //string zMessage = "";
            //if (e.KeyData == Keys.Delete)
            //{
            //    DialogResult dlr = MessageBox.Show("Bạn có xóa thông tin này ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            //    if (dlr == DialogResult.Yes)
            //    {
            //        for (int i = 0; i < LVData.Items.Count; i++)
            //        {
            //            if (LVData.Items[i].Selected)
            //            {
            //                _OrderID = LVData.Items[i].SubItems[2].Text;
            //                _Order_Output = new Stock_Output_Object(_OrderID);
            //                _Order_Output.Delete_Object();
            //                zMessage += TN_Message.Show(_Order_Output.Message);
            //            }
            //        }
            //        if (zMessage.Length == 0)
            //        {
            //            MessageBox.Show("Xóa Thành Công", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //            LV_Stock_Output_LoadData();
            //        }
            //        else
            //        {
            //            MessageBox.Show("Vui lòng kiểm tra lại", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //        }
            //    }
            //}
            string zOrderID = "";
            if (CheckOrder())
            {
                for (int i = 0; i < LVData.Items.Count; i++)
                {
                    if (LVData.Items[i].Selected)
                    {
                        zOrderID += LVData.Items[i].SubItems[2].Text + ",";
                    }
                }
                if (e.KeyData == Keys.Delete)
                {
                    string zMessage = "";
                    if(Utils.TNMessageBox("Bạn có xóa thông tin phiếu " + zOrderID.Substring(0, zOrderID.Length - 1) + "  này ?", 2)=="Y")
                    {
                        for (int i = 0; i < LVData.Items.Count; i++)
                        {
                            if (LVData.Items[i].Selected)
                            {
                                zOrderID = LVData.Items[i].SubItems[2].Text;
                                _Order_Output = new Stock_Output_Object(zOrderID);
                                _Order_Output.Delete_Object();
                                zMessage += TN_Message.Show(_Order_Output.Message);
                            }
                        }
                        if (zMessage.Length == 0)
                        {
                            Utils.TNMessageBoxOK("Câp nhật thành công",3);
                            LV_Stock_Output_LoadData();
                            _Order_Output = new Stock_Output_Object();
                            LoadOrderInfo();
                        }
                        else
                        {
                            Utils.TNMessageBoxOK(zMessage,4);
                        }
                    }
                }
            }
        }

        #endregion

        #endregion

        #region ----- Panel Right -----

        #region ----- Customer -----
        private void btn_Search_Customer_Click(object sender, EventArgs e)
        {
            Frm_Search_Customer frm = new Frm_Search_Customer();
            frm.ShowDialog();
            txt_Customer_ID.Text = frm.CustomerID;
            txt_Customer_Name.Text = frm.CustomerName;
            _CustomerKey = frm.CustomerKey;
        }
        #endregion

        #region ----- Warehouse -----
        private void LoadDataWareHouse()
        {
            LoadDataToToolbox.ComboBoxData(cbo_Warehouse, Warehouse_Data.List(_WareHouse), false, 0, 1);
            cbo_Warehouse.SelectedValue = _WareHouse;
        }

        #endregion

        #region ----- GridView Product -----
        
        private void GVData_Layout()
        {
            DataGridViewButtonColumn btn;
            // Setup Column 
            GVData.Columns.Add("No", "STT");
            GVData.Columns.Add("ProductID", "Mã");
            GVData.Columns.Add("ProductName", "Tên Nguyên vật liệu");
            GVData.Columns.Add("UnitName", "Đơn vị");
            GVData.Columns.Add("QuantityDocument", "SL Chứng Từ");
            GVData.Columns.Add("Quantity", "SL Thực Tế");
            GVData.Columns.Add("Difference", "Chênh Lệch");
            GVData.Columns.Add("QuantityBasic", "SL Cơ bản");
            GVData.Columns.Add("Price", "Đơn giá");
            GVData.Columns.Add("Total", "Thành Tiền");
            GVData.Columns.Add("Follow", "Theo CT");
            btn = new DataGridViewButtonColumn();
            btn.Name = "";
            btn.Text = "...";
            btn.UseColumnTextForButtonValue = true;
            GVData.Columns.Add(btn);
            GVData.Columns.Add("ProductFollow", "Nguyên liệu CT");
            GVData.Columns.Add("NoteStatus", "Ghi chú");



            GVData.Columns["No"].Width = 40;
            GVData.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GVData.Columns["No"].ReadOnly = true;

            GVData.Columns["ProductID"].Width = 100;
            GVData.Columns["ProductID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GVData.Columns["ProductName"].Width = 230;
            GVData.Columns["ProductName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        
            GVData.Columns["UnitName"].Width = 50;
            GVData.Columns["UnitName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GVData.Columns["QuantityDocument"].Width = 100;
            GVData.Columns["QuantityDocument"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GVData.Columns["Quantity"].Width = 100;
            GVData.Columns["Quantity"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GVData.Columns["Difference"].Width = 100;
            GVData.Columns["Difference"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GVData.Columns["Difference"].ReadOnly = true;

            GVData.Columns["QuantityBasic"].Width = 0;
            GVData.Columns["QuantityBasic"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GVData.Columns["QuantityBasic"].Visible = false;

            GVData.Columns["Price"].Width = 0;
            GVData.Columns["Price"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GVData.Columns["Price"].Visible = false;

            GVData.Columns["Total"].Width = 0;
            GVData.Columns["Total"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GVData.Columns["Total"].Visible = false;

            GVData.Columns["Follow"].Width = 160;
            GVData.Columns["Follow"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GVData.Columns["ProductFollow"].Width = 100;
            GVData.Columns["ProductFollow"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GVData.Columns["NoteStatus"].Width = 200;
            GVData.Columns["NoteStatus"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GVData.Columns[""].Width = 40;
            GVData.Columns[""].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;          
        }
        private void GVData_LoadData()
        {
            GVData.Rows.Clear();
            //for (int i = 1; i <= 100; i++)
            //{
            //    GVData.Rows.Add();
            //}
            for (int i = 0; i < _Order_Output.List_Products.Count; i++)
            {
                GVData.Rows.Add();
                Stock_Output_Products_Info zProduct = _Order_Output.List_Products[i];
                GVData.Rows[i].Tag = zProduct;
                GVData.Rows[i].Cells["No"].Value = (i + 1).ToString();
                GVData.Rows[i].Cells["ProductID"].Value = zProduct.ProductID;
                GVData.Rows[i].Cells["ProductName"].Value = zProduct.ProductName;
                GVData.Rows[i].Cells["UnitName"].Value = zProduct.UnitName;
                GVData.Rows[i].Cells["QuantityDocument"].Value = zProduct.QuantityDocument.Ton2String();
                GVData.Rows[i].Cells["Quantity"].Value = zProduct.QuantityReality.Ton2String();
                if (zProduct.QuantityDocument > 0)
                {
                    GVData.Rows[i].Cells["Difference"].Value = (zProduct.QuantityDocument - zProduct.QuantityReality).Ton2String();
                }

                GVData.Rows[i].Cells["QuantityBasic"].Value = zProduct.QuantityBasic.Ton2String() + "/" + zProduct.UnitBasicName;
                GVData.Rows[i].Cells["Follow"].Value = zProduct.OrderIDFollow;
                GVData.Rows[i].Cells["ProductFollow"].Value = zProduct.ProductFollow;
                GVData.Rows[i].Cells["ProductFollow"].Tag = zProduct.ProductKeyFollow;
                GVData.Rows[i].Cells["NoteStatus"].Value = zProduct.NoteStatus;
            }
        }

        //------Event Handler
        private string CtrlValue = "";
        private void GVData_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                if(Utils.TNMessageBox("Bạn có xóa thông tin này ?",2)=="Y")
                {
                    for (int i = 0; i < GVData.Rows.Count; i++)
                    {
                        if (GVData.Rows[i].Selected)
                        {
                            if (GVData.Rows[i].Tag != null)
                            {
                                Stock_Output_Products_Info zProduct = (Stock_Output_Products_Info)GVData.Rows[i].Tag;
                                zProduct.RecordStatus = 99;
                                GVData.Rows[i].Visible = false;
                            }
                        }
                    }
                }
                GVSum_LoadData();
            }
            if (e.KeyCode == Keys.C && e.Control)
            {
                CtrlValue = GVData.CurrentCell.Value.ToString();
            }
            if (e.KeyCode == Keys.V && e.Control)
            {
                GVData.CurrentCell.Value = CtrlValue;
                GVData_CellEndEdit(sender, new DataGridViewCellEventArgs(GVData.CurrentCell.ColumnIndex, GVData.CurrentCell.RowIndex));
            }
        }
        private int currentRow;
        private bool resetRow = false;
        private void GVData_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            DataGridViewTextBoxEditingControl te = (DataGridViewTextBoxEditingControl)e.Control;
            te.AutoCompleteMode = AutoCompleteMode.None;
            switch (GVData.CurrentCell.ColumnIndex)
            {
                case 1: // autocomplete for product
                    LoadDataToToolbox.AutoCompleteTextBox(te, "SELECT ProductID FROM IVT_Product WHERE RecordStatus <99");

                    e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);
                    break;
                case 2: // autocomplete for product
                    LoadDataToToolbox.AutoCompleteTextBox(te, "SELECT ProductName FROM IVT_Product WHERE RecordStatus <99");

                    e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);
                    break;
                case 3:
                    LoadDataToToolbox.AutoCompleteTextBox(te, "SELECT UnitName FROM IVT_Product_Unit");

                    e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);
                    break;
                case 4: // Unit Price
                    e.Control.KeyPress += new KeyPressEventHandler(ControlNumber_KeyPress);
                    break;
                case 10:

                    LoadDataToToolbox.AutoCompleteTextBox(te, Stock_Data.ListOrderInput((int)cbo_Warehouse.SelectedValue));
                    e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);

                    break;

                default:
                    e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);

                    break;
            }
        }
        private void GVData_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            bool zIsChangeValued = false;
            DataGridViewRow zRowEdit = GVData.Rows[e.RowIndex];
            Stock_Output_Products_Info zProduct;
            if (GVData.Rows[e.RowIndex].Tag == null)
            {
                zProduct = new Stock_Output_Products_Info();
                GVData.Rows[e.RowIndex].Tag = zProduct;
            }
            else
            {
                zProduct = (Stock_Output_Products_Info)GVData.Rows[e.RowIndex].Tag;
            }

            switch (e.ColumnIndex)
            {
                case 1: // thong tin ve ma san pham
                    if (zRowEdit.Cells[e.ColumnIndex].Value == null)
                    {
                        zProduct.ProductKey = "";
                        zProduct.ProductID = "";
                        zProduct.ProductName = "";
                        zIsChangeValued = true;
                    }
                    else
                    {
                        if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() != zProduct.ProductID)
                        {
                            zProduct.ProductID = zRowEdit.Cells[e.ColumnIndex].Value.ToString();
                            _ProductID = zRowEdit.Cells[e.ColumnIndex].Value.ToString();//;  
                            zProduct.ProductName = "";
                            zIsChangeValued = true;
                        }
                    }
                    break;
                case 2: // Thong tin ve san pham
                    if (zRowEdit.Cells[e.ColumnIndex].Value == null)
                    {
                        zProduct.ProductKey = "";
                        zProduct.ProductID = "";
                        zProduct.ProductName = "";
                        zIsChangeValued = true;
                    }
                    else
                    {
                        if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() != zProduct.ProductName)
                        {
                            zProduct.ProductID = "";
                            zProduct.ProductName = zRowEdit.Cells[e.ColumnIndex].Value.ToString(); //; 
                            _ProductName = zRowEdit.Cells[e.ColumnIndex].Value.ToString();
                            zIsChangeValued = true;
                        }
                    }
                    break;
                case 3: // Đơn vị tính
                    if (zRowEdit.Cells[e.ColumnIndex].Value == null)
                    {
                        zProduct.UnitKey = 0;
                        zProduct.UnitName = "";
                        zIsChangeValued = true;
                    }
                    else
                    {
                        if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() != zProduct.UnitName)
                        {
                            zProduct.UnitName = zRowEdit.Cells[e.ColumnIndex].Value.ToString(); //; 
                            Product_Unit_Info zIVT_Unit = new Product_Unit_Info(zProduct.UnitName);
                            zProduct.UnitKey = zIVT_Unit.Key;
                            zIsChangeValued = true;
                        }
                    }
                    break;
                case 4: // thong tin ve so luong theo giấy tờ
                    float zQuantityDocument = 0;
                    if (zRowEdit.Cells[e.ColumnIndex].Value != null)
                    {
                        // if (float.TryParse(zRowEdit.Cells["QuantityDocument"].Value.ToString(), NumberStyles.Any, _FormatProviderQuantity, out zQuantityDocument))
                        if (float.TryParse(zRowEdit.Cells["QuantityDocument"].Value.ToString(), out zQuantityDocument))
                        {
                            zProduct.QuantityDocument = zQuantityDocument;
                            zRowEdit.Cells["QuantityDocument"].ErrorText = "";
                            zIsChangeValued = true;
                        }
                        else
                        {
                            zProduct.QuantityDocument = 0;
                            zRowEdit.Cells["QuantityDocument"].ErrorText = "Sai định dạng số lượng";

                        }
                    }

                    break;
                case 5: // thong tin ve so luong theo thực tế
                    float zQuantity = 0;
                    if (zRowEdit.Cells[e.ColumnIndex].Value != null)
                    {
                        // if (float.TryParse(zRowEdit.Cells["Quantity"].Value.ToString(), NumberStyles.Any, _FormatProviderQuantity, out zQuantity))
                        if (float.TryParse(zRowEdit.Cells["Quantity"].Value.ToString(), out zQuantity))
                        {
                            zProduct.QuantityReality = zQuantity;
                            zRowEdit.Cells["Quantity"].ErrorText = "";
                            zIsChangeValued = true;
                        }
                        else
                        {
                            zProduct.QuantityReality = 0;
                            zRowEdit.Cells["Quantity"].ErrorText = "Sai định dạng số lượng";
                        }
                    }
                    break;
                case 10: // thong tin ve so luong theo thực tế
                    if (zRowEdit.Cells[e.ColumnIndex].Value != null)
                    {
                        string zOrderID = zRowEdit.Cells[e.ColumnIndex].Value.ToString();
                        string zSQL = "SELECT OrderKey FROM [dbo].[IVT_Stock_Input] WHERE RecordStatus <99 AND OrderID ='" + zOrderID + "'";
                        string zOrderKey = Data_Access.GetValue(zSQL);

                        if (zOrderKey.Length > 0)
                        {
                            zRowEdit.Cells[e.ColumnIndex].ErrorText = "";
                            zProduct.OrderIDFollow = zOrderID;
                            zProduct.OrderKeyFollow = zOrderKey;
                        }
                        else
                        {
                            zRowEdit.Cells[e.ColumnIndex].ErrorText = "Không có số chứng từ này";
                            zProduct.OrderIDFollow = "";
                            zProduct.OrderKeyFollow = "";
                        }
                    }
                    break;
            }
            if (zIsChangeValued)
            {
                zProduct.RecordStatus = 1;
                CaculateAmount(e.RowIndex);
                ShowProductInGridView(e.RowIndex, e.ColumnIndex);
            }
            if (zProduct.AutoKey != 0)
                zProduct.RecordStatus = 2;
            else
                zProduct.RecordStatus = 1;
            resetRow = true;
            currentRow = e.RowIndex;
            GVData.SelectionChanged += GVData_SelectionChanged;
        }
        //---------------------------------------
        private void CaculateAmount(int RowIndex)
        {
            Stock_Output_Products_Info zProduct = (Stock_Output_Products_Info)GVData.Rows[RowIndex].Tag;
            Product_Info zProductSearch = new Product_Info();
            if (zProduct.ProductID.Length > 0 && zProduct.ProductName.Length == 0)
            {
                zProductSearch = new Product_Info(zProduct.ProductID);
                zProductSearch.Get_Product_Info_ID(zProduct.ProductID);
                zProduct.ProductKey = zProductSearch.ProductKey;
                zProduct.ProductID = zProductSearch.ProductID;
                zProduct.ProductName = zProductSearch.ProductName;
                zProduct.UnitName = zProductSearch.UnitName;
            }
            if (zProduct.ProductID.Length == 0 && zProduct.ProductName.Length > 0)
            {
                zProductSearch.Get_Product_Info(zProduct.ProductName);
                zProduct.ProductKey = zProductSearch.ProductKey;
                zProduct.ProductID = zProductSearch.ProductID;
                zProduct.ProductName = zProductSearch.ProductName;
                zProduct.UnitName = zProductSearch.UnitName;
            }

            zProduct.Difference = zProduct.QuantityDocument - zProduct.QuantityReality;
            zProduct.QuantityBasic = zProduct.Proportion * zProduct.QuantityReality;

        }
        private void GVData_SelectionChanged(object sender, EventArgs e)
        {
            //if (resetRow)
            //{
            //    resetRow = false;
            //    GVData.CurrentCell = GVData.Rows[currentRow].Cells[0];
            //}
        }
        private void ShowProductInGridView(int RowIndex, int Columns)
        {
            DataGridViewRow zRowView = GVData.Rows[RowIndex];
            Stock_Output_Products_Info zProduct = (Stock_Output_Products_Info)zRowView.Tag;

            zRowView.Cells["ProductID"].ErrorText = "";
            zRowView.Cells["ProductName"].ErrorText = "";
            zRowView.Cells["QuantityDocument"].ErrorText = "";
            zRowView.Cells["Quantity"].ErrorText = "";

            if (GVData.Rows[RowIndex].Cells[1].Value != null && zProduct.ProductName.Length == 0)
            {
                Frm_SearchProduct frm = new Frm_SearchProduct();
                frm._te = _ProductID;
                frm.ShowDialog();
                _ProductName = frm._ProductName;
                _ProductID = frm._ProductID;
                _UnitName = frm._UnitName;
                GVData.Rows[RowIndex].Cells["ProductID"].Value = _ProductID;
                if (_ProductID.Length > 0)
                {
                    GV(RowIndex, Columns);
                }
            }

            if (GVData.Rows[RowIndex].Cells[2].Value != null && zProduct.ProductName.Length == 0)
            {
                Frm_SearchProduct frm = new Frm_SearchProduct();
                frm._te = _ProductName;
                frm.ShowDialog();
                _ProductName = frm._ProductName;
                _ProductID = frm._ProductID;
                _UnitName = frm._UnitName;
                GVData.Rows[RowIndex].Cells["ProductName"].Value = _ProductName;
                if (_ProductName.Length > 0)
                {
                    GV(RowIndex, Columns);
                }
            }

            if (zProduct.ProductName.Length > 0 || zProduct.ProductID.Length > 0)
            {
                zRowView.Cells["No"].Value = (RowIndex + 1).ToString();
                zRowView.Cells["ProductName"].Value = zProduct.ProductName;
                zRowView.Cells["ProductID"].Value = zProduct.ProductID;
                zRowView.Cells["UnitName"].Value = zProduct.UnitName;

                if (zProduct.QuantityDocument > 0)
                {
                    zRowView.Cells["QuantityDocument"].Value = zProduct.QuantityDocument.Ton2String();
                }
                else
                {
                    zRowView.Cells["QuantityDocument"].Value = null;
                }

                if (zProduct.QuantityReality > 0)
                {
                    zRowView.Cells["Quantity"].Value = zProduct.QuantityReality.Ton2String();
                }
                else
                {
                    zRowView.Cells["Quantity"].Value = null;
                }

                if (zProduct.Difference > 0)
                {
                    zRowView.Cells["Difference"].Value = (zProduct.QuantityDocument - zProduct.QuantityReality).Ton2String();
                }
                else
                {
                    zRowView.Cells["Difference"].Value = null;
                }

                if (zProduct.QuantityBasic > 0)
                {
                    zRowView.Cells["QuantityBasic"].Value = zProduct.QuantityBasic.ToString() + "/" + zProduct.UnitBasicName;
                }
                else
                {
                    zRowView.Cells["QuantityBasic"].Value = null;
                }
                if (zProduct.QuantityDocument < zProduct.QuantityReality)
                {
                    zRowView.Cells["QuantityReality"].ErrorText = "Số lượng thực tế không được lớn hơn số lượng chứng từ";
                }
            }
        }
        private void GV(int RowIndex, int ColumnIndex)
        {
            DataGridViewRow zRowEdit = GVData.Rows[RowIndex];
            Stock_Output_Products_Info zProduct;
            if (GVData.Rows[RowIndex].Tag == null)
            {
                zProduct = new Stock_Output_Products_Info();
                GVData.Rows[RowIndex].Tag = zProduct;
            }
            else
            {
                zProduct = (Stock_Output_Products_Info)GVData.Rows[RowIndex].Tag;
            }
            bool zIsChangeValued = false;
            switch (ColumnIndex)
            {
                case 1: // thong tin ve ma san pham
                    if (zRowEdit.Cells[ColumnIndex].Value == null)
                    {
                        zProduct.ProductKey = "";
                        zProduct.ProductID = "";
                        zProduct.ProductName = "";
                        zIsChangeValued = true;
                    }
                    else
                    {
                        if (zRowEdit.Cells[ColumnIndex].Value.ToString() != zProduct.ProductID)
                        {
                            zProduct.ProductID = zRowEdit.Cells[ColumnIndex].Value.ToString();
                            _ProductID = zRowEdit.Cells[ColumnIndex].Value.ToString();  //;  
                            zProduct.ProductName = "";
                            zIsChangeValued = true;
                        }
                    }
                    break;
                case 2: // Thong tin ve san pham
                    if (zRowEdit.Cells[ColumnIndex].Value == null)
                    {
                        zProduct.ProductKey = "";
                        zProduct.ProductID = "";
                        zProduct.ProductName = "";
                        zIsChangeValued = true;
                    }
                    else
                    {
                        if (zRowEdit.Cells[ColumnIndex].Value.ToString() != zProduct.ProductName)
                        {
                            zProduct.ProductID = "";
                            zProduct.ProductName = zRowEdit.Cells[ColumnIndex].Value.ToString();
                            _ProductName = zRowEdit.Cells[ColumnIndex].Value.ToString();

                            zIsChangeValued = true;
                        }
                    }
                    break;
                case 3: // Đơn vị tính
                    if (zRowEdit.Cells[ColumnIndex].Value == null)
                    {
                        zProduct.UnitKey = 0;
                        zProduct.UnitName = "";
                        zIsChangeValued = true;
                    }
                    else
                    {
                        if (zRowEdit.Cells[ColumnIndex].Value.ToString() != zProduct.UnitName)
                        {
                            zProduct.UnitName = zRowEdit.Cells[ColumnIndex].Value.ToString(); //; 
                            Product_Unit_Info zIVT_Unit = new Product_Unit_Info(zProduct.UnitName);
                            zProduct.UnitKey = zIVT_Unit.Key;
                            zIsChangeValued = true;
                        }
                    }
                    break;
                case 4: // thong tin ve so luong theo giấy tờ
                    float zQuantityDocument = 0;
                    if (zRowEdit.Cells[ColumnIndex].Value != null)
                    {
                        // if (float.TryParse(zRowEdit.Cells["QuantityDocument"].Value.ToString(), NumberStyles.Any, _FormatProviderQuantity, out zQuantityDocument))
                        if (float.TryParse(zRowEdit.Cells["QuantityDocument"].Value.ToString(), out zQuantityDocument))
                        {
                            zProduct.QuantityDocument = zQuantityDocument;
                            zRowEdit.Cells["QuantityDocument"].ErrorText = "";
                            zIsChangeValued = true;
                        }
                        else
                        {
                            zProduct.QuantityDocument = 0;
                            zRowEdit.Cells["QuantityDocument"].ErrorText = "Sai định dạng số lượng";

                        }
                    }

                    break;
                case 5: // thong tin ve so luong theo thực tế
                    float zQuantity = 0;
                    if (zRowEdit.Cells[ColumnIndex].Value != null)
                    {
                        // if (float.TryParse(zRowEdit.Cells["QuantityReality"].Value.ToString(), NumberStyles.Any, _FormatProviderQuantity, out zQuantity))
                        if (float.TryParse(zRowEdit.Cells["QuantityReality"].Value.ToString(),  out zQuantity))
                        {
                            zProduct.QuantityReality = zQuantity;
                            zRowEdit.Cells["QuantityReality"].ErrorText = "";
                            zIsChangeValued = true;
                        }
                        else
                        {
                            zProduct.QuantityReality = 0;
                            zRowEdit.Cells["QuantityReality"].ErrorText = "Sai định dạng số lượng";
                        }
                    }
                    break;
                case 13: // thong tin ve ghi chu
                    if (zRowEdit.Cells[ColumnIndex].Value == null)
                    {
                        zProduct.NoteStatus = "";
                        zIsChangeValued = true;
                    }
                    else
                    {
                        if (zRowEdit.Cells[ColumnIndex].Value.ToString() != zProduct.NoteStatus)
                        {
                            zProduct.NoteStatus = zRowEdit.Cells[ColumnIndex].Value.ToString(); //;  
                            zIsChangeValued = true;
                        }
                    }
                    break;
            }
            if (zIsChangeValued)
            {
                zProduct.RecordStatus = 1;
                CaculateAmount(RowIndex);
                ShowProductInGridView(RowIndex, ColumnIndex);
            }
            if (zProduct.AutoKey != 0)
                zProduct.RecordStatus = 2;
            else
                zProduct.RecordStatus = 1;
        }
        private void GVData_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 11)
            {
                if (StyleForm == 1 && Status == 1)
                {
                    if (cbo_Warehouse.SelectedValue != null)
                    {
                        Frm_Voucher_Input Frm = new Frm_Voucher_Input();
                        Frm.WarehouseKey = (int)cbo_Warehouse.SelectedValue;
                        Frm.WarehouseName = cbo_Warehouse.Text;
                        Frm.ShowDialog();
                        Add_Product(GVData.CurrentRow.Index, Frm.OrderID, Frm.ProductID);
                    }

                }
                if (StyleForm == 2 && Status == 2)
                {
                    Utils.TNMessageBoxOK("Bạn không được vào phần này !",1);
                }

            }
        }
        #endregion


        #region [-----Gidview-Sum--------]

        private void GVSum_Layout()
        {
            // Setup Column 
            GVSum.Columns.Add("SUM", "TỔNG");
            GVSum.Columns.Add("Sum_Document", "SL chứng từ");
            GVSum.Columns.Add("Sum_Real", "SL thực tế");
            GVSum.Columns.Add("Sum_Difference", "SL chênh lệch");

            GVSum.Columns["SUM"].Width = 440;
            GVSum.Columns["SUM"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GVSum.Columns["SUM"].ReadOnly = true;

            GVSum.Columns["Sum_Document"].Width = 100;
            GVSum.Columns["Sum_Document"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GVSum.Columns["Sum_Document"].ReadOnly = true;

            GVSum.Columns["Sum_Real"].Width = 100;
            GVSum.Columns["Sum_Real"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GVSum.Columns["Sum_Real"].ReadOnly = true;

            GVSum.Columns["Sum_Difference"].Width = 100;
            GVSum.Columns["Sum_Difference"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GVSum.Columns["Sum_Difference"].ReadOnly = true;
        }
        private void GVSum_LoadData()
        {
            double zDocument = 0;
            double zRealy = 0;
            double zDifferent = 0;
            for (int i = 0; i < GVData.Rows.Count - 1; i++)
            {
                if (GVData.Rows[i].Tag != null && GVData.Rows[i].Visible == true)
                {
                    Stock_Output_Products_Info zProduct = (Stock_Output_Products_Info)GVData.Rows[i].Tag;
                    zDocument += zProduct.QuantityDocument;
                    zRealy += zProduct.QuantityReality;
                    zDifferent += (zProduct.QuantityDocument - zProduct.QuantityReality);
                }
            }
            GVSum.Rows.Clear();
            GVSum.Rows.Add();
            GVSum.Rows[0].Cells["Sum_Document"].Value = zDocument.Ton2String();
            GVSum.Rows[0].Cells["Sum_Real"].Value = zRealy.Ton2String();
            GVSum.Rows[0].Cells["Sum_Difference"].Value = zDifferent.Ton2String();
        }
        #endregion

        #region ----- GridView Post -----
        public void GVNote_Layout()
        {
            // Setup Column 
            GVNote.Columns.Add("No", "STT");
            GVNote.Columns.Add("Date", "Ngày");
            GVNote.Columns.Add("Content", "Nội Dung");
            GVNote.Columns.Add("Employer", "Nhân Viên");

            GVNote.Columns["No"].Width = 40;
            GVNote.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GVNote.Columns["No"].ReadOnly = true;

            GVNote.Columns["Date"].Width = 150;
            GVNote.Columns["Date"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GVNote.Columns["Date"].ReadOnly = true;

            GVNote.Columns["Content"].Width = 400;
            GVNote.Columns["Content"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GVNote.Columns["Content"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GVNote.Columns["Content"].ReadOnly = true;

            GVNote.Columns["Employer"].Width = 150;
            GVNote.Columns["Employer"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GVNote.Columns["Employer"].ReadOnly = true;
           
            GVNote.ReadOnly = true;
        }
        private void GVNote_LoadData()
        {
            GVNote.Rows.Clear();

            int i = 0;
            foreach (DataRow zRow in _Order_Output.ListNote.Rows)
            {
                GVNote.Rows.Add();

                DateTime zTime = (DateTime)zRow["CreatedOn"];
                GVNote.Rows[i].Cells["No"].Value = i + 1;
                GVNote.Rows[i].Cells["No"].Tag = zRow["AutoKey"].ToString();
                GVNote.Rows[i].Cells["Date"].Value = zTime.ToString("dd/MM/yyyy HH:mm");
                GVNote.Rows[i].Cells["Content"].Value = zRow["NoteContent"].ToString();
                GVNote.Rows[i].Cells["Employer"].Value = zRow["CreatedName"].ToString();
                i++;
            }

        }
        private void GVNote_KeyDown(object sender, KeyEventArgs e)
        {
            string zMessage = "";
            if (e.KeyCode == Keys.Delete)
            {
                if(Utils.TNMessageBox("Bạn có xóa thông tin này ?", 2)=="Y")
                {
                    for (int i = 0; i < GVNote.Rows.Count; i++)
                    {
                        if (GVNote.Rows[i].Selected)
                        {
                            if (GVNote.Rows[i].Cells["No"].Tag != null)
                            {
                                Stock_Output_Object zinfo = new Stock_Output_Object();
                                zinfo.AutoKey = int.Parse(GVNote.Rows[i].Cells["No"].Tag.ToString());
                                zinfo.DeleteNote();
                                zMessage += zinfo.Message;
                            }
                        }
                    }
                    if (zMessage.Length == 0)
                    {
                        Utils.TNMessageBoxOK("Đã xóa thành công !",3);
                    }
                    else
                    {
                        Utils.TNMessageBoxOK(zMessage, 4);
                    }
                }
            }
        }
        #endregion

        #region ----- Process Data -----
        private void CheckReadOnlyTrue()
        {
            GVData.ReadOnly = true;
            txt_Order_ID.ReadOnly = true;
            txt_Customer_ID.ReadOnly = true;
            txt_Customer_Name.ReadOnly = true;
            cbo_Warehouse.Enabled = false;
            txt_Description.ReadOnly = true;
            btn_Save.Enabled = false;
            btn_Edit.Enabled = true;
        }
        private void CheckReadOnlyFalse()
        {
            GVData.ReadOnly = false;
            txt_Order_ID.ReadOnly = false;
            txt_Customer_ID.ReadOnly = false;
            txt_Customer_Name.ReadOnly = false;
            cbo_Warehouse.Enabled = true;
            txt_Description.ReadOnly = false;
            btn_Save.Enabled = true;
            btn_Edit.Enabled = false;
        }
        private void Check_Record()
        {
            if (StyleForm == 1 && Status == 1)
            {
                if (_Order_Output.RecordStatus == 0)
                {
                    btn_Del.Visible = true;
                    btn_Send.Visible = true;
                    btn_Save.Visible = true;
                    btn_New.Visible = true;
                    btn_Copy.Visible = true;
                    btn_Import.Visible = true;
                    btn_Approve.Visible = false;
                    btn_Deny.Visible = false;
                    btn_Edit.Visible = true;
                    CheckReadOnlyFalse();
                    if (_OrderID.Length == 0)
                    {
                        DataRow zAdd = User_Data.GetUserAdd(SessionUser.UserLogin.Key, RoleID).Rows[0];
                        if (Convert.ToInt32(zAdd[0]) > 0)
                        {
                            btn_Save.Visible = true;
                        }
                        else
                        {
                            btn_Save.Visible = false;
                        }
                    }
                    if (_OrderID.Length > 0)
                    {
                        DataRow zEdit = User_Data.GetUserEdit(SessionUser.UserLogin.Key, RoleID).Rows[0];
                        if (Convert.ToInt32(zEdit[0]) > 0)
                        {
                            btn_Save.Visible = true;
                        }
                        else
                        {
                            btn_Save.Visible = false;
                        }
                    }
                    DataRow zDelete = User_Data.GetUserDel(SessionUser.UserLogin.Key, RoleID).Rows[0];
                    if (Convert.ToInt32(zDelete[0]) > 0)
                    {
                        btn_Del.Visible = true;
                    }
                    else
                    {
                        btn_Del.Visible = false;
                    }
                }
                if (_Order_Output.RecordStatus == 41)
                {
                    btn_Del.Visible = false;
                    btn_Send.Visible = false;
                    btn_New.Visible = true;
                    btn_Copy.Visible = true;
                    btn_Import.Visible = true;
                    btn_Save.Visible = false;
                    SetReadOnlyControl_Approve();
                    btn_Approve.Visible = false;
                    btn_Deny.Visible = false;
                    btn_Edit.Visible = false;
                }
                if (_Order_Output.RecordStatus == 42)
                {
                    btn_Del.Visible = true;
                    btn_Send.Visible = true;
                    btn_New.Visible = true;
                    btn_Copy.Visible = true;
                    btn_Import.Visible = true;
                    btn_Save.Visible = true;
                    SetReadOnlyControl_NotApprove();
                    btn_Approve.Visible = false;
                    btn_Deny.Visible = false;
                    btn_Edit.Visible = true;
                    CheckReadOnlyFalse();
                    if (_OrderID.Length == 0)
                    {
                        DataRow zAdd = User_Data.GetUserAdd(SessionUser.UserLogin.Key, RoleID).Rows[0];
                        if (Convert.ToInt32(zAdd[0]) > 0)
                        {
                            btn_Save.Visible = true;
                        }
                        else
                        {
                            btn_Save.Visible = false;
                        }
                    }
                    if (_OrderID.Length > 0)
                    {
                        DataRow zEdit = User_Data.GetUserEdit(SessionUser.UserLogin.Key, RoleID).Rows[0];
                        if (Convert.ToInt32(zEdit[0]) > 0)
                        {
                            btn_Save.Visible = true;
                        }
                        else
                        {
                            btn_Save.Visible = false;
                        }
                    }
                    DataRow zDelete = User_Data.GetUserDel(SessionUser.UserLogin.Key, RoleID).Rows[0];
                    if (Convert.ToInt32(zDelete[0]) > 0)
                    {
                        btn_Del.Visible = true;
                    }
                    else
                    {
                        btn_Del.Visible = false;
                    }
                }
                if (_Order_Output.RecordStatus == 43)
                {
                    btn_Del.Visible = false;
                    btn_Send.Visible = false;
                    btn_New.Visible = true;
                    btn_Copy.Visible = true;
                    btn_Import.Visible = true;
                    btn_Save.Visible = false;
                    SetReadOnlyControl_NotApprove();
                    btn_Approve.Visible = false;
                    btn_Deny.Visible = false;
                    btn_Edit.Visible = false;
                }
            }
            if (StyleForm == 2 && Status == 2)
            {
                GVData.ReadOnly = true;
                GVData.Columns[12].Visible = false;

                btn_Del.Visible = false;
                btn_Send.Visible = false;
                btn_New.Visible = false;
                btn_Copy.Visible = false;
                btn_Import.Visible = false;
                btn_Save.Visible = false;
                SetReadOnlyControl_Approve();
                btn_Approve.Visible = true;
                btn_Deny.Visible = true;
                btn_Edit.Visible = false;
                DataRow zApprove = User_Data.GetUserApprove(SessionUser.UserLogin.Key, RoleID).Rows[0];
                if (Convert.ToInt32(zApprove[0]) > 0)
                {
                    btn_Approve.Visible = true;
                    btn_Deny.Visible = true;
                }
                else
                {
                    btn_Approve.Visible = false;
                    btn_Deny.Visible = false;
                }
            }
        }
        private void LoadOrderInfo()
        {
            txt_Order_ID.Text = _Order_Output.ID;
            if (_Order_Output.OrderDate != null)
            {
                dte_OrderDate.Value = _Order_Output.OrderDate.Value;
            }
            else
            {
                dte_OrderDate.Value = SessionUser.Date_Work;
            }
            txt_Description.Text = _Order_Output.Description;

            _Customer = new Customer_Info();
            _Customer.Get_Customer_Info(_Order_Output.CustomerKey);
            _CustomerKey = _Customer.Key;
            txt_Customer_ID.Text = _Customer.CustomerID;
            if (_Customer.CategoryKey == 0)
                txt_Customer_Name.Text = _Customer.FullName;
            if (_Customer.CategoryKey == 1)
                txt_Customer_Name.Text = _Customer.CompanyName;
            if (_Customer.CategoryKey == 2)
                txt_Customer_Name.Text = _Customer.CompanyName;
            if (_Customer.CategoryKey == 3)
                txt_Customer_Name.Text = _Customer.CompanyName;

            if (_Order_Output.WarehouseKey != 0)
            {
                cbo_Warehouse.SelectedValue = _Order_Output.WarehouseKey;
            }

            GVData_LoadData();
            GVSum_LoadData();
            GVNote_LoadData();
            Check_Record();

        }
        private string CheckBeforeSave()
        {
            string zAmountErr = "";
            string zMessageNull = "";
            string zMessageQuantity = "";
            int A = 0;
            int B = 0;

            if (txt_Order_ID.Text.Trim().Length != 3 && _OrderID == "")
            {
                txt_Order_ID.BackColor = Color.FromArgb(230, 100, 100);
                zAmountErr += "- Bạn không được nhập nhỏ hoặc hơn 3 ký tự vào Số Phiếu \n";
            }
            else
            {
                txt_Order_ID.BackColor = Color.FromArgb(255, 255, 225);
            }

            if (cbo_Warehouse.SelectedValue == null)
            {
                cbo_Warehouse.BackColor = Color.FromArgb(230, 100, 100);
                zAmountErr += "- Chọn Kho \n";
            }
            else
            {
                cbo_Warehouse.BackColor = Color.FromArgb(255, 255, 225);
            }

            if (txt_Order_ID.Text.Trim().Length == 0)
            {
                txt_Order_ID.BackColor = Color.FromArgb(230, 100, 100);
                zAmountErr += "- Nhập mã phiếu \n";
            }
            else
            {
                txt_Order_ID.BackColor = Color.FromArgb(255, 255, 225);
            }

            if (txt_Customer_ID.Text.Trim().Length == 0)
            {
                txt_Customer_ID.BackColor = Color.FromArgb(230, 100, 100);
                zAmountErr += "- Chọn mã khách hàng\n";
            }
            else
            {
                txt_Customer_ID.BackColor = Color.FromArgb(255, 255, 225);
            }

            if (txt_Customer_Name.Text.Trim().Length == 0)
            {
                txt_Customer_Name.BackColor = Color.FromArgb(230, 100, 100);
                zAmountErr += "- Nhập tên khách hàng \n";
            }
            else
            {
                txt_Customer_Name.BackColor = Color.FromArgb(255, 255, 225);
            }

            for (int i = 0; i < GVData.Rows.Count; i++)
            {
                if (GVData.Rows[i].Visible == true && GVData.Rows[i].Cells["ProductID"].Value != null)
                {
                    A++;
                }
                if (GVData.Rows[i].Visible == true && GVData.Rows[i].Cells["ProductID"].Value == null)
                {
                    B++;
                }
            }
            if (A == 0 && B > 0)
            {
                zMessageNull += "- Điền thông tin Nguyên Liệu \n";
            }
            zAmountErr += zMessageNull;

            for (int i = 0; i < GVData.Rows.Count; i++)
            {
                if (GVData.Rows[i].Visible == true &&
                    GVData.Rows[i].Cells["ProductID"].Value != null &&
                    GVData.Rows[i].Cells["QuantityDocument"].Value == null
                    || GVData.Rows[i].Visible == true &&
                    GVData.Rows[i].Cells["ProductID"].Value != null &&
                    GVData.Rows[i].Cells["QuantityDocument"].Value.ToString() == "0")
                {
                    zMessageQuantity += "- Kiểm số lượng chứng từ \n";
                }
                if (GVData.Rows[i].Visible == true &&
                    GVData.Rows[i].Cells["ProductID"].Value != null &&
                    GVData.Rows[i].Cells["Quantity"].Value == null
                    || GVData.Rows[i].Visible == true &&
                    GVData.Rows[i].Cells["ProductID"].Value != null &&
                    GVData.Rows[i].Cells["Quantity"].Value.ToString() == "0")
                {
                    zMessageQuantity += "- Kiểm tra số lượng thực tế \n";
                }
                if (GVData.Rows[i].Cells["ProductID"].Value != null)
                {
                    float zQuantityDocument = 0;
                    float zQuantityReality = 0;
                    if (GVData.Rows[i].Cells["QuantityDocument"].Value != null)
                    {
                        zQuantityDocument = float.Parse(GVData.Rows[i].Cells["QuantityDocument"].Value.ToString());
                    }
                    else
                    {
                        zQuantityDocument = 0;
                    }
                    if (GVData.Rows[i].Cells["Quantity"].Value != null)
                    {
                        zQuantityReality = float.Parse(GVData.Rows[i].Cells["Quantity"].Value.ToString());
                    }
                    else
                    {
                        zQuantityReality = 0;
                    }

                    //Thông báo số lượng thực tế lớn hơn số lượng chứng từ
                    //if (zQuantityDocument < zQuantityReality)
                    //{
                    //    zMessageQuantity += "- Số lượng thực tế không được lớn hơn số lượng chứng từ \n";
                    //}
                }
            }
            zAmountErr += zMessageQuantity;

            return zAmountErr;
        }
        private void Save()
        {
            string zRoleForAction = _WarehouseRole;
            _Order_Output.ID = txt_Order_ID.Text;
            _Order_Output.OrderDate = dte_OrderDate.Value;
            _Order_Output.Description = txt_Description.Text;
            _Order_Output.WarehouseKey = (int)cbo_Warehouse.SelectedValue;
            if (txt_Customer_ID.Text.Trim().Length == 0)
                _Order_Output.CustomerKey = "";
            else
                _Order_Output.CustomerKey = _CustomerKey;
            _Order_Output.CreatedBy = SessionUser.UserLogin.Key;
            _Order_Output.CreatedName = SessionUser.UserLogin.EmployeeName; ;
            _Order_Output.ModifiedBy = SessionUser.UserLogin.Key;
            _Order_Output.ModifiedName = SessionUser.UserLogin.EmployeeName ;

            _Order_Output.RoleID = zRoleForAction;

            _List_Ouput = new List<Stock_Output_Products_Info>();
            Stock_Output_Products_Info zProduct;
            for (int i = 0; i < GVData.Rows.Count; i++)
            {
                if (_Update == 1)
                {
                    if (GVData.Rows[i].Tag != null && GVData.Rows[i].Cells["ProductID"].Value != null)
                    {
                        zProduct = (Stock_Output_Products_Info)GVData.Rows[i].Tag;
                        if (txt_Order_ID.Text.Length == 3)
                            zProduct.RecordStatus = 1;
                        if (GVData.Rows[i].Cells["NoteStatus"].Value != null)
                        {
                            zProduct.NoteStatus = GVData.Rows[i].Cells["NoteStatus"].Value.ToString();
                        }
                        else
                        {
                            zProduct.NoteStatus = "";
                        }
                        _List_Ouput.Add(zProduct);
                        _Order_Output.List_Products = _List_Ouput;
                    }
                }
                if (_Copy == 1 || _New == 1)
                    if (GVData.Rows[i].Tag != null && GVData.Rows[i].Visible == true)
                    {
                        zProduct = (Stock_Output_Products_Info)GVData.Rows[i].Tag;
                        if (txt_Order_ID.Text.Length == 3)
                            zProduct.RecordStatus = 1;
                        if (GVData.Rows[i].Cells["NoteStatus"].Value != null)
                        {
                            zProduct.NoteStatus = GVData.Rows[i].Cells["NoteStatus"].Value.ToString();
                        }
                        else
                        {
                            zProduct.NoteStatus = "";
                        }
                        _List_Ouput.Add(zProduct);
                        _Order_Output.List_Products = _List_Ouput;
                    }
            }
            _Order_Output.Save_Object();
            string zMessage = TN_Message.Show(_Order_Output.Message);
            if (zMessage.Length == 0)
            {
                Utils.TNMessageBoxOK("Câp nhật thành công",3);
            }
            else
            {
                Utils.TNMessageBoxOK(zMessage,4);
            }
        }

        #endregion

        #region ----- Process Event -----
        private void btn_Save_Click(object sender, EventArgs e)
        {
            if (SessionUser.Date_Lock >= dte_OrderDate.Value)
            {
                Utils.TNMessageBoxOK("Thời gian này đã khóa sổ.!", 2);
                return;
            }
            k = 1;
            if (CheckBeforeSave().Length == 0)
            {
                Save();
                _OrderID = _Order_Output.ID;
                _Order_Output = new Stock_Output_Object(_OrderID);
                LoadOrderInfo();
                LV_Stock_Output_LoadData();
                LoadTexbox();
                if (_OrderID.Length == 0)
                    btn_Send.Enabled = false;
                else
                    btn_Send.Enabled = true;
                for (int i = 0; i < GVData.Rows.Count - 1; i++)
                {
                    if (GVData.Rows[i].Tag != null)
                    {
                        k++;
                    }
                }
                if (_OrderID.Length == 0)
                    btn_Send.Enabled = false;
                else
                    btn_Send.Enabled = true;
                CheckReadOnlyTrue();
            }
            else
            {
                Utils.TNMessageBoxOK("Vui lòng kiểm tra lại : \n" + CheckBeforeSave(), 2);
            }
        }
        private void btn_Del_Click(object sender, EventArgs e)
        {
            if (SessionUser.Date_Lock >= dte_ToDate.Value)
            {
                Utils.TNMessageBoxOK("Thời gian này đã khóa sổ.!", 2);
                return;
            }
            string zOrderID = "";
            if (CheckOrder())
            {
                for (int i = 0; i < LVData.Items.Count; i++)
                {
                    if (LVData.Items[i].Selected)
                    {
                        zOrderID += LVData.Items[i].SubItems[2].Text + ",";
                    }
                }
                string zMessage = "";
                if(Utils.TNMessageBox("Bạn có xóa thông tin phiếu " + zOrderID.Substring(0, zOrderID.Length - 1) + "  này ?", 2)=="Y")
                {
                    for (int i = 0; i < LVData.Items.Count; i++)
                    {
                        if (LVData.Items[i].Selected)
                        {
                            zOrderID = LVData.Items[i].SubItems[2].Text;
                            _Order_Output = new Stock_Output_Object(zOrderID);
                            _Order_Output.Delete_Object();
                            zMessage += TN_Message.Show(_Order_Output.Message);
                        }
                    }
                    if (zMessage.Length == 0)
                    {
                        Utils.TNMessageBoxOK("Câp nhật thành công",3);
                        LV_Stock_Output_LoadData();
                        _Order_Output = new Stock_Output_Object();
                        LoadOrderInfo();
                    }
                    else
                    {
                        Utils.TNMessageBoxOK(zMessage, 4);
                    }
                }
            }
        }
        private void btn_Copy_Click(object sender, EventArgs e)
        {
            _Copy = 1;
            _Update = 0;
            _New = 0;
            if (_OrderID.Length > 0)
            {
                k = 1;
                if(Utils.TNMessageBox("Bạn có chắc muốn sao chép phiếu này sang phiếu mới không ?", 1)=="Y")
                {
                    txt_Order_ID.Enabled = true;
                    dte_OrderDate.Enabled = true;
                    txt_Description.Enabled = true;
                    cbo_Warehouse.Enabled = true;

                    _OrderID = "";
                    _Order_Output.Key = "";
                    _Order_Output.ID = "";
                    _Order_Output.RecordStatus = 0;
                    LoadOrderInfo();
                    if (_OrderID.Length == 0)
                        btn_Send.Enabled = false;
                    else
                        btn_Send.Enabled = true;

                    Utils.TNMessageBoxOK("Sao chép thành công !",3);
                    for (int i = 0; i < GVData.Rows.Count - 1; i++)
                    {
                        if (GVData.Rows[i].Cells[0].Value != null)
                        {
                            k++;
                        }
                    }
                }
            }
            else
            {
                Utils.TNMessageBoxOK("Vui lòng chọn đơn hàng", 1);
            }
        }
        private void btn_New_Click(object sender, EventArgs e)
        {
            _New = 1;
            _Update = 0;
            _Copy = 0;
            _OrderID = "";
            _Order_Output = new Stock_Output_Object();
            LoadOrderInfo();

            k = 1;
            if (_OrderID.Length == 0)
                btn_Send.Enabled = false;
            else
                btn_Send.Enabled = true;
            txt_Order_ID.Focus();
        }
        private void btn_Send_Click(object sender, EventArgs e)
        {
            if (CheckOrder())
            {
                _Order_Output.RoleID = _WarehouseRole;
                _Order_Output.ModifiedBy = SessionUser.UserLogin.Key;
                _Order_Output.ModifiedName = SessionUser.Personal_Info.FullName;
                _Order_Output.Send();
                LV_Stock_Output_LoadData();
                string zMessage = TN_Message.Show(_Order_Output.Message);
                if (zMessage.Length == 0)
                {
                    Utils.TNMessageBoxOK("Yêu cầu đã được gửi đi",3);
                    _Order_Output = new Stock_Output_Object(_OrderID);
                    LoadOrderInfo();
                }
                else
                {
                    Utils.TNMessageBoxOK(zMessage, 4);
                }
            }
        }
        private void btn_Approve_Click(object sender, EventArgs e)
        {
            if (CheckOrder())
            {
                _Order_Output.RoleID = _WarehouseRole;
                _Order_Output.ModifiedBy = SessionUser.UserLogin.Key;
                _Order_Output.ModifiedName = SessionUser.Personal_Info.FullName;
                _Order_Output.Approve();
                LV_Stock_Output_LoadData();
                string zMessage = TN_Message.Show(_Order_Output.Message);
                if (zMessage.Length == 0)
                {
                    Utils.TNMessageBoxOK("Đơn hàng đã được duyệt",3);
                    _Order_Output = new Stock_Output_Object();
                    LoadOrderInfo();
                }
                else
                {
                    Utils.TNMessageBoxOK(zMessage, 4);
                }
            }
        }
        private void btn_Deny_Click(object sender, EventArgs e)
        {
            if (CheckOrder())
            {
                _Order_Output.RoleID = _WarehouseRole;
                _Order_Output.ModifiedBy = SessionUser.UserLogin.Key;
                _Order_Output.ModifiedName = SessionUser.Personal_Info.FullName;
                _Order_Output.Denied();
                LV_Stock_Output_LoadData();
                string zMessage = TN_Message.Show(_Order_Output.Message);
                if (zMessage.Length == 0)
                {
                    Utils.TNMessageBoxOK("Yêu cầu đã được trả về", 2);
                    _Order_Output = new Stock_Output_Object();
                    LoadOrderInfo();
                }
                else
                {
                    Utils.TNMessageBoxOK(zMessage,4);
                }
            }
        }
        private void btn_Search_Click(object sender, EventArgs e)
        {
            LV_Stock_Output_LoadData_Search();
        }
        private void btn_Post_Click(object sender, EventArgs e)
        {
            if (CheckPost())
            {
                int zIndex = GVNote.Rows.Count - 1;
                GVNote.Rows.Insert(0, "*", DateTime.Now.ToString("dd/MM/yyyy HH:mm"), txt_Message.Text, SessionUser.Personal_Info.FullName);

                _Order_Output.CreatedBy = SessionUser.UserLogin.Key;
                _Order_Output.CreatedName = SessionUser.Personal_Info.FullName;
                _Order_Output.InsertNote(txt_Message.Text);
                string _Message = _Order_Output.Message;
                if (_Message == "11")
                {
                    Utils.TNMessageBoxOK("Gửi thông báo thành công", 3);
                    _OrderID = _Order_Output.ID;
                    _Order_Output = new Stock_Output_Object(_OrderID);
                    LoadOrderInfo();
                }
            }
        }
        private void btn_Edit_Click(object sender, EventArgs e)
        {
            GVData.ReadOnly = false;
            txt_Order_ID.ReadOnly = false;
            txt_Customer_ID.ReadOnly = false;
            txt_Customer_Name.ReadOnly = false;
            cbo_Warehouse.Enabled = true;
            txt_Description.ReadOnly = false;
            btn_Edit.Enabled = false;
            btn_Save.Enabled = true;
        }

        #endregion

        #endregion

        #region  ------ Funtion -----
        private bool CheckOrder()
        {
            Guid zGuid;
            bool zIsValid = Guid.TryParse(_Order_Output.Key, out zGuid);
            if (zIsValid)
            {
                return true;
            }
            else
            {
                Utils.TNMessageBoxOK("Vui lòng chọn phiếu trước khi thực hiện thao tác này !",1);
                return false;
            }
        }
        private bool CheckPost()
        {
            Guid zGuid;
            bool zIsValid = Guid.TryParse(_Order_Output.Key, out zGuid);
            if (zIsValid)
            {
                return true;
            }
            else
            {
                Utils.TNMessageBoxOK("Bạn phải chọn thông tin của 1 phiếu trước khi muốn gửi thông tin gì ?", 1);
                return false;
            }
        }
        private int CheckProductID(string ProductID, int nRowCurrency)
        {
            int i = 1;
            foreach (DataGridViewRow zRow in GVData.Rows)
            {
                if (zRow.Cells["ProductID"].Value != null)
                {
                    zRow.Cells["No"].Value = i;
                    if (zRow.Index != nRowCurrency && zRow.Cells["ProductID"].Value.ToString().Trim() == ProductID)
                    {
                        return zRow.Index;
                    }
                    i++;
                }
            }
            return -1;
        }
        private void ControlNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }

            if (e.KeyChar == '.')
            {
                e.Handled = false;
            }
            if (e.KeyChar == ',')
            {
                e.Handled = false;
            }
        }
        private void Control_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = false;
        }
        private bool IsRowEmpty(int RowIndex)
        {
            for (int i = 0; i < GVData.ColumnCount; i++)
            {
                if (GVData.Rows[RowIndex].Cells[i].Value != null)
                {
                    return false;
                }
            }
            return true;
        }
        private bool IsRowError(int RowIndex)
        {
            for (int i = 1; i < GVData.ColumnCount; i++)
            {
                if (GVData.Rows[RowIndex].Cells[i].ErrorText.Length > 0)
                {
                    return true;
                }
            }
            return false;
        }
        private void CheckRowError(int RowIndex)
        {
            DataGridViewRow nRow = GVData.Rows[RowIndex];
        }
        private void SetReadOnlyControl_Approve()
        {
            txt_Order_ID.Enabled = false;
            dte_OrderDate.Enabled = false;
            txt_Description.Enabled = false;
            cbo_Warehouse.Enabled = false;
        }
        private void SetReadOnlyControl_NotApprove()
        {
            txt_Order_ID.ReadOnly = true;
            dte_OrderDate.Enabled = false;
            txt_Description.ReadOnly = true;
            cbo_Warehouse.Enabled = false;
            txt_Customer_ID.ReadOnly = true;
            txt_Customer_Name.ReadOnly = true;
            GVData.ReadOnly = true;
        }
        private void Add_Product(int RowIndex, string OrderID, string ProductID)
        {

            DataTable zTable = Get_Data.Get_Products_HaveIn_Warehouse(OrderID, ProductID);
            if (zTable.Rows.Count > 0)
            {
                int n = RowIndex;
                DataRow r = zTable.Rows[0];
                GVData.Rows[n].Cells["No"].Value = (k++);
                GVData.Rows[n].Cells["ProductID"].Value = r["ProductID"].ToString();
                GVData.Rows[n].Cells["ProductName"].Value = r["ProductName"].ToString();
                GVData.Rows[n].Cells["UnitName"].Value = r["UnitName"].ToString();

                float zInputQuatity = float.Parse(r["InputQuatity"].ToString());
                float zOutputQuatity = float.Parse(r["OutputQuantity"].ToString());
                float zInventory = zInputQuatity - zOutputQuatity;
                float Difference = zInputQuatity - zInventory;
                GVData.Rows[n].Cells["QuantityDocument"].Value = zInputQuatity;
                GVData.Rows[n].Cells["Quantity"].Value = zInventory;
                GVData.Rows[n].Cells["Difference"].Value = Difference;
                GVData.Rows[n].Cells["Follow"].Value = r["OrderID"].ToString();
                GVData.Rows[n].Cells["Follow"].Value = r["OrderID"].ToString();
                GVData.Rows[n].Cells["ProductFollow"].Value = r["ProductID"].ToString();
                //gan doi tuong vao row.tag
                Stock_Output_Products_Info zProduct = new Stock_Output_Products_Info();
                if (GVData.Rows[RowIndex].Tag == null) // Nếu dòng chưa có dữ liệu
                {

                    zProduct = new Stock_Output_Products_Info();
                }
                else
                {
                    zProduct = (Stock_Output_Products_Info)GVData.Rows[RowIndex].Tag;
                }
                zProduct.ProductID = r["ProductID"].ToString();
                zProduct.ProductKey = r["ProductKey"].ToString();
                zProduct.ProductName = r["ProductName"].ToString();
                zProduct.UnitKey = int.Parse(r["UnitKey"].ToString());
                zProduct.UnitName = r["UnitName"].ToString();
                zProduct.QuantityBasic = Difference;
                zProduct.OrderKeyFollow = r["OrderKey"].ToString();
                zProduct.OrderIDFollow = r["OrderID"].ToString();
                zProduct.QuantityDocument = float.Parse(r["InputQuatity"].ToString());
                zProduct.QuantityReality = zInventory;
                zProduct.Difference = Difference;
                zProduct.ProductFollow = r["ProductID"].ToString();
                zProduct.ProductKeyFollow = r["ProductKey"].ToString();
                zProduct.RecordStatus = 1;
                GVData.Rows[n].Tag = zProduct;
            }
            k = 1;
            for (int i = 0; i < GVData.Rows.Count - 1; i++)
            {
                if (GVData.Rows[i].Cells[0].Value != null)
                {
                    k++;
                }
            }
            GVSum_LoadData();
        }



        #endregion

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                //this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}
