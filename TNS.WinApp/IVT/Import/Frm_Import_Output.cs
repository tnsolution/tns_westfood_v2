﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using TNS.CRM;
using System.IO;
using TNS.IVT;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Import_Output : Form
    {
        private Warehouse_Info zWareHouse;
        private List<Stock_Output_Products_Info> _List;
        private Stock_Output_Object _Order_Obj;
        public Frm_Import_Output()
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            Utils.DoubleBuffered(GV_Order, true);
            Utils.DoubleBuffered(GV_Product, true);
            Utils.DrawGVStyle(ref GV_Product);
            Utils.DrawGVStyle(ref GV_Order);

            GV_Order_Layout();
            GV_Product_Layout();
            btn_Import.Click += Btn_Import_Click;
            btn_Save.Click += Btn_Save_Click;
            btnClose.Click += btnClose_Click;
            btnMax.Click += btnMax_Click;
            btnMini.Click += btnMini_Click;
        }

        private void Frm_Import_Output_Local_Load(object sender, EventArgs e)
        {
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
        }
        #region [Design Layout]
        private void GV_Order_Layout()
        {
            // Setup Column 
            GV_Order.Columns.Add("No", "STT");
            GV_Order.Columns.Add("OrderID", "Mã Đơn Hàng");
            GV_Order.Columns.Add("OrderDate", "Ngày Tạo Đơn Hàng");
            GV_Order.Columns.Add("WareHouseID", "Mã Kho Mặc Định");
            GV_Order.Columns.Add("WareHouse", "Tên Kho Mặc Định");
            GV_Order.Columns.Add("WareHouseLocalID", "Mã Kho Chuyễn");
            GV_Order.Columns.Add("WareHouseLocal", "Tên Kho Chuyễn");
            GV_Order.Columns.Add("CustomerID", "Mã Khách Hàng");
            GV_Order.Columns.Add("Customer", "Tên Khách Hàng");
            GV_Order.Columns.Add("Note", "Ghi Chú");
            GV_Order.Columns.Add("Message", "Thông Báo");

            GV_Order.Columns["No"].Width = 40;
            GV_Order.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV_Order.Columns["OrderID"].Width = 120;
            GV_Order.Columns["OrderID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV_Order.Columns["OrderDate"].Width = 150;
            GV_Order.Columns["OrderDate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV_Order.Columns["WareHouseID"].Width = 120;
            GV_Order.Columns["WareHouseID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV_Order.Columns["WareHouseLocalID"].Width = 120;
            GV_Order.Columns["WareHouseLocalID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV_Order.Columns["CustomerID"].Width = 120;
            GV_Order.Columns["CustomerID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV_Order.Columns["WareHouse"].Width = 200;
            GV_Order.Columns["WareHouse"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV_Order.Columns["WareHouseLocal"].Width = 200;
            GV_Order.Columns["WareHouseLocal"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV_Order.Columns["Customer"].Width = 200;
            GV_Order.Columns["Customer"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;


            GV_Order.Columns["Note"].Width = 200;
            GV_Order.Columns["Note"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV_Order.Columns["Message"].Width = 200;
            GV_Order.Columns["Message"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV_Order.Columns["Message"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GV_Order.AllowUserToAddRows = false;
            GV_Order.AllowUserToDeleteRows = false;

        }
        private void GV_Product_Layout()
        {
            // Setup Column 
            GV_Product.Columns.Add("No", "STT");
            GV_Product.Columns.Add("ProductID", "Mã");
            GV_Product.Columns.Add("ProductName", "Tên Nguyên vật liệu");
            GV_Product.Columns.Add("UnitName", "Đơn vị");
            GV_Product.Columns.Add("QuantityDocument", "SL Chứng Từ");
            GV_Product.Columns.Add("QuantityReality", "SL Thực Tế");
            GV_Product.Columns.Add("Difference", "Chênh Lệch");
            GV_Product.Columns.Add("QuantityBasic", "SL Cơ bản");
            GV_Product.Columns.Add("Price", "Đơn giá");
            GV_Product.Columns.Add("Total", "Thành Tiền");
            GV_Product.Columns.Add("Follow", "Theo CT");
            GV_Product.Columns.Add("ProductFollow", "Nguyên liệu CT");
            GV_Product.Columns.Add("NoteStatus", "Ghi chú");
            GV_Product.Columns.Add("OrderID", "Mã Chứng Từ");
            GV_Product.Columns.Add("TypeID", "Mã loại hàng");
            GV_Product.Columns.Add("TypeName", "Tên loại hàng");
            GV_Product.Columns.Add("Message", "Thông báo");

            GV_Product.Columns["No"].Width = 40;
            GV_Product.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV_Product.Columns["No"].ReadOnly = true;

            GV_Product.Columns["ProductID"].Width = 120;
            GV_Product.Columns["ProductID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV_Product.Columns["ProductName"].Width = 250;
            GV_Product.Columns["ProductName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV_Product.Columns["UnitName"].Width = 50;
            GV_Product.Columns["UnitName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV_Product.Columns["QuantityDocument"].Width = 100;
            GV_Product.Columns["QuantityDocument"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Product.Columns["QuantityReality"].Width = 100;
            GV_Product.Columns["QuantityReality"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Product.Columns["Difference"].Width = 100;
            GV_Product.Columns["Difference"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Product.Columns["Difference"].ReadOnly = true;

            GV_Product.Columns["QuantityBasic"].Width = 0;
            GV_Product.Columns["QuantityBasic"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Product.Columns["QuantityBasic"].Visible = false;

            GV_Product.Columns["Price"].Width = 0;
            GV_Product.Columns["Price"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Product.Columns["Price"].Visible = false;

            GV_Product.Columns["Total"].Width = 0;
            GV_Product.Columns["Total"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Product.Columns["Total"].Visible = false;

            GV_Product.Columns["Follow"].Width = 160;
            GV_Product.Columns["Follow"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Product.Columns["ProductFollow"].Width = 160;
            GV_Product.Columns["ProductFollow"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Product.Columns["NoteStatus"].Width = 250;
            GV_Product.Columns["NoteStatus"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV_Product.Columns["OrderID"].Width = 100;
            GV_Product.Columns["OrderID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV_Product.Columns["TypeID"].Width = 100;
            GV_Product.Columns["TypeID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV_Product.Columns["TypeName"].Width = 160;
            GV_Product.Columns["TypeName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV_Product.Columns["Message"].Width = 200;
            GV_Product.Columns["Message"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV_Product.Columns["Message"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;


        }
        #endregion

        #region [Load GV Data]
        private void GV_Order_LoadData(DataTable In_Table)
        {
            GV_Order.Rows.Clear();
            this.Cursor = Cursors.WaitCursor;
            for (int i = 0; i < In_Table.Rows.Count; i++)
            {
                DataRow zRow = In_Table.Rows[i];
                GV_Order.Rows.Add();
                GV_Order.Rows[i].Cells["No"].Value = (i + 1).ToString();
                GV_Order.Rows[i].Cells["OrderID"].Value = zRow[1].ToString().Trim();
                DateTime zOrderDate = DateTime.Parse(zRow[2].ToString().Trim());
                GV_Order.Rows[i].Cells["OrderDate"].Value = zOrderDate.ToString("dd/MM/yyyy");
                GV_Order.Rows[i].Cells["WareHouseID"].Value = zRow[3].ToString().Trim();
                GV_Order.Rows[i].Cells["WareHouse"].Value = zRow[4].ToString().Trim();
                GV_Order.Rows[i].Cells["WareHouseLocalID"].Value = zRow[5].ToString().Trim();
                GV_Order.Rows[i].Cells["WareHouseLocal"].Value = zRow[6].ToString().Trim();
                GV_Order.Rows[i].Cells["CustomerID"].Value = zRow[7].ToString().Trim();
                GV_Order.Rows[i].Cells["Customer"].Value = zRow[8].ToString().Trim();
                GV_Order.Rows[i].Cells["Note"].Value = zRow[9].ToString().Trim();
            }
            this.Cursor = Cursors.Default;
        }
        private void GV_Product_LoadData(DataTable In_Table)
        {
            GV_Product.Rows.Clear();
            this.Cursor = Cursors.WaitCursor;
            for (int i = 0; i < In_Table.Rows.Count; i++)
            {
                DataRow zRow = In_Table.Rows[i];
                GV_Product.Rows.Add();
                GV_Product.Rows[i].Cells["No"].Value = (i + 1).ToString();
                GV_Product.Rows[i].Cells["ProductID"].Value = zRow[1].ToString().Trim();
                GV_Product.Rows[i].Cells["ProductName"].Value = zRow[2].ToString().Trim();
                GV_Product.Rows[i].Cells["UnitName"].Value = zRow[3].ToString().Trim();

                float zQuantityDocument = 0;
                if (float.TryParse(zRow[4].ToString().Trim(), out zQuantityDocument))
                {

                }
                GV_Product.Rows[i].Cells["QuantityDocument"].Value = zQuantityDocument.ToString();

                float zQuantityReality = 0;
                if (float.TryParse(zRow[5].ToString().Trim(), out zQuantityReality))
                {

                }
                GV_Product.Rows[i].Cells["QuantityReality"].Value = zQuantityReality.ToString();
                float zDifference = 0;
                if (float.TryParse(zRow[6].ToString().Trim(), out zDifference))
                {


                }
                GV_Product.Rows[i].Cells["Difference"].Value = zDifference.ToString();

                if (zRow[7].ToString() != null)
                    GV_Product.Rows[i].Cells["Follow"].Value = zRow[7].ToString().Trim();
                else
                    GV_Product.Rows[i].Cells["Follow"].Value = "";
                if (zRow[8].ToString() != null)
                    GV_Product.Rows[i].Cells["ProductFollow"].Value = zRow[8].ToString().Trim();
                else
                    GV_Product.Rows[i].Cells["ProductFollow"].Value = "";
                if (zRow[9].ToString() != null)
                    GV_Product.Rows[i].Cells["NoteStatus"].Value = zRow[9].ToString().Trim();
                else
                    GV_Product.Rows[i].Cells["NoteStatus"].Value = "";
                GV_Product.Rows[i].Cells["OrderID"].Value = zRow[10].ToString().Trim();

                GV_Product.Rows[i].Cells["TypeID"].Value = zRow[11].ToString().Trim();
                GV_Product.Rows[i].Cells["TypeName"].Value = zRow[12].ToString().Trim();
            }
            this.Cursor = Cursors.Default;
        }
        #endregion

        #region [Event]
        private void Btn_Save_Click(object sender, EventArgs e)
        {
            int zCheckOrrder = CheckBeforSaveOrder();
            int zCheckProduct = CheckBeforSaveProduct();
            if (zCheckOrrder == 0 && zCheckProduct == 0)
            {
                Save();
            }
            else
            {
                Utils.TNMessageBoxOK("Lỗi.Vui lòng kiểm tra lại trước khi import",2);
            }

        }
        private void Btn_Import_Click(object sender, EventArgs e)
        {

            OpenFileDialog zOpf = new OpenFileDialog();

            zOpf.InitialDirectory = @"C:\";
            zOpf.Title = "Browse Excel Files";

            zOpf.CheckFileExists = true;
            zOpf.CheckPathExists = true;

            zOpf.DefaultExt = "txt";
            zOpf.Filter = "All Files|*.*";
            zOpf.FilterIndex = 2;
            zOpf.RestoreDirectory = true;

            zOpf.ReadOnlyChecked = true;
            zOpf.ShowReadOnly = true;

            if (zOpf.ShowDialog() == DialogResult.OK)
            {
                FileInfo FileInfo = new FileInfo(zOpf.FileName);
                bool zcheck = true;
                zcheck = Data_Access.CheckFileStatus(FileInfo);
                if (zcheck == true)
                {
                    Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file !",2);
                }
                else
                {
                    try
                    {
                        Cursor.Current = Cursors.WaitCursor;
                        ExcelPackage package = new ExcelPackage(FileInfo);
                        DataTable In_Table = Data_Access.GetTable(FileInfo.FullName, package.Workbook.Worksheets[1].Name);
                        GV_Order_LoadData(In_Table);
                        DataTable In_Table_Employee = Data_Access.GetTable(FileInfo.FullName, package.Workbook.Worksheets[2].Name);
                        GV_Product_LoadData(In_Table_Employee);
                        package.Dispose();
                        Cursor.Current = Cursors.Default;
                        btn_Save.Enabled = true;
                    }
                    catch (Exception ex)
                    {
                        Utils.TNMessageBoxOK(ex.ToString(),4);
                        btn_Save.Enabled = false;
                    }
                }
            }



        }
        #endregion

        #region[Progess data]
        private int CheckBeforSaveOrder()
        {
            int zErr = 0;

            for (int i = 0; i < GV_Order.Rows.Count; i++)
            {
                if (GV_Order.Rows[i].Cells["Message"].Tag == null)
                {
                    string zMessage = "";
                    zMessage = CheckRowOrder(GV_Order.Rows[i]);
                    if (zMessage != "")
                    {
                        zErr++;
                    }

                }
            }
            return zErr;
        }
        string CheckRowOrder(DataGridViewRow Row)
        {
            string zMessage = "";
            int zFindOrder = Stock_Data.FindOrdderID_Output(Row.Cells["OrderID"].Value.ToString());
            if (zFindOrder > 0)
            {
                zMessage += "Đơn hàng đã tồn tại " + Environment.NewLine;

            }
            zWareHouse = new Warehouse_Info(Row.Cells["WareHouseID"].Value.ToString());
            if (zWareHouse.WarehouseKey == 0)
            {
                zMessage += "Kho đã tồn tại " + Environment.NewLine;
            }
            if (zWareHouse.Slug == 0)
            {
                zMessage += "Kho mặc định không đúng loại " + Environment.NewLine;
            }
            if (Row.Cells["OrderDate"].Value == null || Row.Cells["OrderDate"].Value.ToString() == "")
            {
                zMessage += "Ngày nhập không hợp lệ /";
            }
            else if (Row.Cells["OrderDate"].Value != null || Row.Cells["OrderDate"].Value.ToString() != "")
            {
                DateTime zDateWrite = DateTime.MinValue;
                if (!DateTime.TryParse(Row.Cells["OrderDate"].Value.ToString(), out zDateWrite))
                {
                    zMessage += "Ngày nhập không hợp lệ /";
                }
                else
                {
                    if (SessionUser.Date_Lock >= zDateWrite)
                    {
                        zMessage += "Thời gian này đã khóa sổ.!/";
                    }
                }
            }
            if (zMessage == "")
            {
                Row.DefaultCellStyle.ForeColor = Color.White;
                Row.Cells["Message"].Tag = 0;
            }
            else
            {
                Row.DefaultCellStyle.ForeColor = Color.Red;
                Row.Cells["Message"].Value = zMessage;
                Row.Cells["Message"].Tag = null;
            }
            return zMessage;
        }
        private int CheckBeforSaveProduct()
        {
            int zErr = 0;
            for (int i = 0; i < GV_Product.Rows.Count; i++)
            {
                if (GV_Product.Rows[i].Cells["Message"].Tag == null)
                {
                    string zMessage = "";
                    zMessage = CheckRowProduct(GV_Product.Rows[i]);
                    if (zMessage != "")
                    {
                        zErr++;
                    }

                }
            }
            return zErr;
        }
        string CheckRowProduct(DataGridViewRow Row)
        {
            string zMessage = "";
            int zFindOrder = Stock_Data.FindOrdderID_Output(Row.Cells["OrderID"].Value.ToString());
            if (zFindOrder > 0)
            {
                zMessage += "Đơn hàng đã tồn tại " + Environment.NewLine;

            }
            Product_Info zProduct = new Product_Info();
            zProduct.Get_Product_Info_ID(Row.Cells["ProductID"].Value.ToString());
            if (zProduct.ProductKey == "")
            {
                zMessage += "Mã sản phẩm không tồn tại " + Environment.NewLine;
            }

            Product_Unit_Info zUnit = new Product_Unit_Info(Row.Cells["UnitName"].Value.ToString());
            if (zUnit.Key == 0)
            {
                zMessage += "Đơn vị tính không tồn tại " + Environment.NewLine;
            }
            if (Row.Cells["ProductFollow"].Value.ToString() != "")
            {
                zProduct = new Product_Info();
                zProduct.Get_Product_Info_ID(Row.Cells["ProductFollow"].Value.ToString());
                if (zProduct.ProductKey == "")
                {
                    zMessage += "Mã sản phẩm chứng từ không tồn tại " + Environment.NewLine;
                }
                else
                {
                    Row.Cells["ProductFollow"].Tag = zProduct.ProductKey.ToString();
                }

            }
            if (Row.Cells["TypeID"].Value.ToString() != "")
            {
                Product_Type_Info zType = new Product_Type_Info();
                zType.GetTypeID_Info(Row.Cells["TypeID"].Value.ToString());
                if (zType.Key == 0)
                {
                    zMessage += "Loại hàng không tồn tại " + Environment.NewLine;
                }
            }

            if (zMessage == "")
            {
                Row.DefaultCellStyle.ForeColor = Color.Navy;
                Row.Cells["Message"].Tag = 0;
            }
            else
            {
                Row.DefaultCellStyle.ForeColor = Color.Red;
                Row.Cells["Message"].Value = zMessage;
                Row.Cells["Message"].Tag = null;
            }
            return zMessage;
        }
        private void Save()
        {
            int Err = 0;
            string zMessage = "";
            for (int i = 0; i < GV_Order.Rows.Count; i++)
            {
                int zFindOrder = 0;// Stock_Data.FindOrdderID_Input(GV_Order.Rows[i].Cells["OrderID"].Value.ToString());
                if (zFindOrder == 0)
                {
                    _Order_Obj = new Stock_Output_Object();
                    AddObjectOrder(GV_Order.Rows[i]);

                    _List = new List<Stock_Output_Products_Info>();
                    for (int y = 0; y < GV_Product.Rows.Count; y++)
                    {
                        if (GV_Product.Rows[y].Cells["OrderID"].Value.ToString() == _Order_Obj.ID)
                        {
                            AddObjectProduct(GV_Product.Rows[y]);
                        }
                    }
                    _Order_Obj.Save_Object();
                    zMessage = _Order_Obj.Message;
                    string zResult = TN_Message.Show(zMessage);
                    if (zResult.Length == 0)
                    {
                        GV_Order.Rows[i].Cells["Message"].Value = "Thành Công";
                        GV_Order.Rows[i].DefaultCellStyle.ForeColor = Color.Navy;
                    }
                    else
                    {
                        GV_Order.Rows[i].Cells["Message"].Value = "Lỗi!."+ zResult;
                        GV_Order.Rows[i].DefaultCellStyle.ForeColor = Color.Red;
                        Err++;
                    }
                }
                else
                {
                    GV_Order.Rows[i].Cells["Message"].Value = "Số Đơn Hàng này đã có trong dữ liệu";
                    GV_Order.Rows[i].DefaultCellStyle.ForeColor = Color.Red;
                    Err++;
                }
            }
            if(Err>0)
            {
                Utils.TNMessageBoxOK("Có phát sinh lỗi!.Vui lòng kiểm tra lại dữ liệu",2);
            }
            else
            {
                Utils.TNMessageBoxOK("Lưu thành công",3);
            }
        }
        void AddObjectOrder(DataGridViewRow Row)
        {
            _Order_Obj.ID = Row.Cells["OrderID"].Value.ToString();
            _Order_Obj.OrderDate = DateTime.Parse(Row.Cells["OrderDate"].Value.ToString());
            zWareHouse = new Warehouse_Info(Row.Cells["WareHouseID"].Value.ToString());
            _Order_Obj.WarehouseKey = zWareHouse.WarehouseKey;
            if (Row.Cells["WareHouseLocalID"].Value == null)
            {
                _Order_Obj.WarehouseLocal = 0;
            }
            else
            {
                string zWareHouseLocalID = Row.Cells["WareHouseLocalID"].Value.ToString();
                zWareHouse = new Warehouse_Info(zWareHouseLocalID);
                _Order_Obj.WarehouseLocal = zWareHouse.WarehouseKey;
            }
            Customer_Info zCustomer = new Customer_Info();
            zCustomer.Get_CustomerID(Row.Cells["CustomerID"].Value.ToString());
            _Order_Obj.CustomerKey = zCustomer.Key;
            _Order_Obj.Description = Row.Cells["Note"].Value.ToString();
            _Order_Obj.CreatedBy = SessionUser.UserLogin.Key;
            _Order_Obj.CreatedName = SessionUser.UserLogin.EmployeeName ;
            _Order_Obj.ModifiedBy = SessionUser.UserLogin.Key;
            _Order_Obj.ModifiedName = SessionUser.UserLogin.EmployeeName ;
        }
        void AddObjectProduct(DataGridViewRow Row)
        {
            Stock_Output_Products_Info zStock_Input = new Stock_Output_Products_Info();
            Product_Info zProduct = new Product_Info();
            zProduct.Get_Product_Info_ID(Row.Cells["ProductID"].Value.ToString());
            if (zProduct.ProductKey.Length > 0)
            {
                zStock_Input.ProductKey = zProduct.ProductKey;
                zStock_Input.ProductID = Row.Cells["ProductID"].Value.ToString();
                zStock_Input.ProductName = Row.Cells["ProductName"].Value.ToString();
                Product_Unit_Info zUnit = new Product_Unit_Info(Row.Cells["UnitName"].Value.ToString());
                zStock_Input.UnitName = zUnit.Unitname;
                zStock_Input.UnitKey = zUnit.Key;
                zStock_Input.QuantityDocument = float.Parse(Row.Cells["QuantityDocument"].Value.ToString());
                zStock_Input.QuantityReality = float.Parse(Row.Cells["QuantityReality"].Value.ToString());
                zStock_Input.Difference = float.Parse(Row.Cells["Difference"].Value.ToString());
                if (Row.Cells["NoteStatus"].Value != null || Row.Cells["NoteStatus"].Value.ToString() != "")
                    zStock_Input.NoteStatus = Row.Cells["NoteStatus"].Value.ToString();
                else
                    zStock_Input.NoteStatus = "";
                if (Row.Cells["ProductFollow"].Value.ToString() != "")
                {
                    zStock_Input.ProductFollow = Row.Cells["ProductFollow"].Value.ToString();
                    zStock_Input.ProductKeyFollow = Row.Cells["ProductFollow"].Tag.ToString();
                }
                Product_Type_Info zType = new Product_Type_Info();
                zType.GetTypeID_Info(Row.Cells["TypeID"].Value.ToString());
                if (zType.Key > 0)
                    zStock_Input.ProductType = zType.Key;
                else
                    zStock_Input.ProductType = 0;
                zStock_Input.RecordStatus = 1;
                _List.Add(zStock_Input);
                _Order_Obj.List_Products = _List;
            }
        }


        #endregion

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}
