﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.IVT;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Input_Local_V2 : Form
    {
        public int Status = 0;
        public int StyleForm = 0;
        public int WareHouse = 0;
        public string WareHouseRole = "";

        private List<Stock_Input_Products_Info> _List;
        private Stock_Input_Object _Order_Obj;

        public Frm_Input_Local_V2()
        {
            InitializeComponent();

            dte_FromDate.Value = SessionUser.Date_Work;
            dte_ToDate.Value = SessionUser.Date_Work;
            dte_OrderDate.Value = SessionUser.Date_Work;

            this.DoubleBuffered = true;
            Utils.DrawLVStyle(ref LVData);
        }

        private void Frm_Input_Local_V2_Load(object sender, EventArgs e)
        {

        }


    }
}
