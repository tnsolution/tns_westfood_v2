﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;
using TNS.IVT;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Stock_Local_Input : Form
    {
        private string _RoleID = "";
        private bool IsLoadData = true;
        private int _WareHouse = 0;
        private string _WareHouseRole = "";
        private int _StyleForm = 0;
        private int _Status = 0;
        private int k = 0;
        private string _OrderID = "";
        private string _ProductName = "";
        private string _ProductID = "";
        private string _UnitName = "";
        #region [New - UpDate - Copy]
        private int _New = 0;
        private int _Copy = 0;
        private int _Update = 0;
        #endregion
        #region [ Properties ]

        public int WareHouse { get => _WareHouse; set => _WareHouse = value; }
        public string WareHouseRole { get => _WareHouseRole; set => _WareHouseRole = value; }

        public int StyleForm
        {
            get
            {
                return _StyleForm;
            }

            set
            {
                _StyleForm = value;
            }
        }
        public int Status
        {
            get
            {
                return _Status;
            }

            set
            {
                _Status = value;
            }
        }

        public string RoleID
        {
            get
            {
                return _RoleID;
            }

            set
            {
                _RoleID = value;
            }
        }

        #endregion
        private string _FormatQuantity = "#,###,##0.00";
        private IFormatProvider _FormatProviderQuantity = new CultureInfo("en-US", true);
        private List<Stock_Input_Products_Info> _List;
        private Stock_Input_Object _Order_Obj;
        public Frm_Stock_Local_Input()
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            Utils.DoubleBuffered(GVData, true);
            Utils.DoubleBuffered(GVSum, true);
            Utils.DoubleBuffered(GVNote, true);
            Utils.DrawGVStyle(ref GVData);
            Utils.DrawGVStyle(ref GVSum);
            Utils.DrawGVStyle(ref GVNote);
            Utils.DrawLVStyle(ref LVData);


            InitGVData_Layout();
            InitGVNote_Layout();
            InitGVSum_Layout();

            InitLV_Layout();
            InitLV_Data();


            #region [Control Event] 
            LVData.Click += LVData_Click;
            LVData.KeyDown += LVData_KeyDown;

            GVData.CellEndEdit += GVData_CellEndEdit;
            GVData.EditingControlShowing += GVData_EditingControlShowing;
            GVData.KeyDown += GVData_KeyDown;
            GVData.CellClick += GVData_CellClick;
            GVNote.KeyDown += GV_Post_KeyDown;

            btn_Search.Click += btn_Search_Click;
            btn_Save.Click += btn_Save_Click;
            btn_Del.Click += btn_Del_Click;
            btn_Copy.Click += btn_Copy_Click;
            btn_New.Click += btn_New_Click;
            btn_Send.Click += btn_Send_Click;
            btn_Deny.Click += btn_Deny_Click;
            btn_Approve.Click += btn_Approve_Click;
            btn_Post.Click += btn_Post_Click;
            btn_Edit.Click += btn_Edit_Click;
            btn_Import.Click += btn_Import_Click;

            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;
            #endregion

            btn_Edit.Enabled = false;

            Utils.SizeLastColumn_LV(LVData);
        }

        private void FrmStock_Local_Input_Load(object sender, EventArgs e)
        {
            _New = 1;
            dte_FromDate.Value = SessionUser.Date_Work;
            dte_ToDate.Value = SessionUser.Date_Work;
            dte_OrderDate.Value = SessionUser.Date_Work;
            k = 1;

            InitCbo_Warehouse();
            _Order_Obj = new Stock_Input_Object(_OrderID);
            LoadOrderInfo();
            InitTB_Form();
            IsLoadData = false;

            if (_OrderID.Length == 0)
                btn_Send.Enabled = false;
            else
                btn_Send.Enabled = true;

            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);

            this.Bounds = Screen.PrimaryScreen.WorkingArea;
        }
        private void InitTB_Form()
        {
            if (StyleForm == 1 && Status == 1)
            {
                LoadDataToToolbox.AutoCompleteTextBox(txt_Search, "SELECT OrderID FROM dbo.IVT_Stock_Input WHERE WarehouseKey = '" + WareHouse + "' AND WarehouseLocal > 0  AND RecordStatus <> 99 ORDER BY OrderDate DESC ");
                this.Text = "PHIẾU NHẬP NỘI BỘ : " + cbo_Warehouse.Text.ToUpper() + "";
                txtTitle.Text = "PHIẾU NHẬP NỘI BỘ : " + cbo_Warehouse.Text.ToUpper() + ""; ;

            }
            if (StyleForm == 2 && Status == 2)
            {
                LoadDataToToolbox.AutoCompleteTextBox(txt_Search, "SELECT OrderID FROM dbo.IVT_Stock_Input WHERE WarehouseKey = '" + WareHouse + "' AND WarehouseLocal > 0 AND RecordStatus = 41 ORDER BY OrderDate DESC , OrderID DESC");
                this.Text = "DUYỆT PHIẾU NHẬP NỘI BỘ : " + cbo_Warehouse.Text.ToUpper() + "";
                txtTitle.Text = "DUYỆT PHIẾU NHẬP NÔI BỘ : " + cbo_Warehouse.Text.ToUpper() + "";
            }

        }

        #region ----- Left -----

        #region [ List Items ]
        private void InitLV_Layout()
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "No";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Center;
            LVData.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ngày";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LVData.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số Phiếu";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Left;
            LVData.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tình trạng";
            colHead.Width = 70;
            colHead.TextAlign = HorizontalAlignment.Left;
            LVData.Columns.Add(colHead);


        }
        private void InitLV_Data()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LVData;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            DataTable In_Table = new DataTable();
            if (StyleForm == 1 && Status == 1)
            {
                In_Table = Stock_Data.ListInput_NotApprove_Search(WareHouse, txt_Search.Text.Trim(), dte_FromDate.Value, dte_ToDate.Value);
            }
            if (StyleForm == 2 && Status == 2)
            {
                In_Table = Stock_Data.ListInput_WaitingApprove_Search(WareHouse, txt_Search.Text.Trim(), dte_FromDate.Value, dte_ToDate.Value);
            }
            LV.Items.Clear();
            int n = In_Table.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.ForeColor = Color.Navy;
                DataRow nRow = In_Table.Rows[i];
                int RecordStatus = (int)nRow["RecordStatus"];
                string zStatus = "";
                if (RecordStatus == 41)
                {
                    zStatus = "Chờ duyệt";
                    lvi.ForeColor = Color.Gray;
                }
                if (RecordStatus == 42)
                {
                    zStatus = "Trả lại";
                    lvi.ForeColor = Color.Maroon;
                }
                if (RecordStatus == 43)
                {
                    zStatus = "Đã duyệt";
                    lvi.ForeColor = Color.Green;
                }
                if (RecordStatus == 49)
                {
                    zStatus = "Từ chối";
                    lvi.ForeColor = Color.Maroon;
                }


                lvi.BackColor = Color.White;

                lvi.ImageIndex = 0;

                DateTime z_trdate = (DateTime)nRow["OrderDate"];
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = z_trdate.ToString("dd/MM/yyyy");
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["OrderID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zStatus;
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);

            }

            this.Cursor = Cursors.Default;

        }
        private void InitLV_Data_Search()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LVData;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            DataTable In_Table = new DataTable();
            if (StyleForm == 1 && Status == 1)
            {
                In_Table = Stock_Data.ListInput_NotApprove_Search(WareHouse, txt_Search.Text.Trim(), dte_FromDate.Value, dte_ToDate.Value);
            }
            if (StyleForm == 2 && Status == 2)
            {
                In_Table = Stock_Data.ListInput_WaitingApprove_Search(WareHouse, txt_Search.Text.Trim(), dte_FromDate.Value, dte_ToDate.Value);
            }
            LV.Items.Clear();
            int n = In_Table.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.ForeColor = Color.Navy;
                DataRow nRow = In_Table.Rows[i];
                int RecordStatus = (int)nRow["RecordStatus"];
                string zStatus = "";
                if (RecordStatus == 41)
                {
                    zStatus = "Chờ duyệt";
                    lvi.ForeColor = Color.Gray;
                }
                if (RecordStatus == 42)
                {
                    zStatus = "Trả lại";
                    lvi.ForeColor = Color.Maroon;
                }
                if (RecordStatus == 43)
                {
                    zStatus = "Đã duyệt";
                    lvi.ForeColor = Color.Green;
                }
                if (RecordStatus == 49)
                {
                    zStatus = "Từ chối";
                    lvi.ForeColor = Color.Maroon;
                }


                lvi.BackColor = Color.White;

                lvi.ImageIndex = 0;

                DateTime z_trdate = (DateTime)nRow["OrderDate"];
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = z_trdate.ToString("dd/MM/yyyy");
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["OrderID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);



                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zStatus;
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);

            }

            this.Cursor = Cursors.Default;

        }
        #endregion

        #region ----- Process Envent -----     
        private void LVData_Click(object sender, EventArgs e)
        {
            if (LVData.SelectedItems.Count > 0)
            {
                _Update = 1;
                _New = 0;
                _Copy = 0;
                k = 1;
                //for (int i = 0; i < LVData.Items.Count; i++)
                //{
                //    if (LVData.Items[i].Selected == true)
                //    {
                //        LVData.Items[i].BackColor = Color.LightBlue; // highlighted item
                //    }
                //    else
                //    {
                //        LVData.Items[i].BackColor = SystemColors.Window; // normal item
                //    }
                //}
                txt_Order_ID.Enabled = true;
                dte_OrderDate.Enabled = true;
                txt_Description.Enabled = true;
                cbo_Warehouse.Enabled = true;

                cbo_Warehouse_Local.Enabled = true;
                _OrderID = LVData.SelectedItems[0].SubItems[2].Text;
                txt_Order_ID.Text = _OrderID;
                _Order_Obj = new Stock_Input_Object(_OrderID);
                LoadOrderInfo();
                ReturnColor();
                if (_OrderID.Length == 0)
                    btn_Send.Enabled = false;
                else
                    btn_Send.Enabled = true;
                for (int i = 0; i < GVData.Rows.Count - 1; i++)
                {
                    if (GVData.Rows[i].Cells[0].Value != null)
                    {
                        k++;
                    }
                }
                CheckReadOnlyTrue();
            }
        }
        private void LVData_KeyDown(object sender, KeyEventArgs e)
        {
            string zMessage = "";
            if (e.KeyData == Keys.Delete)
            {
                if(Utils.TNMessageBox("Bạn có xóa thông tin này ?", 2)=="Y")
                {
                    for (int i = 0; i < LVData.Items.Count; i++)
                    {
                        if (LVData.Items[i].Selected)
                        {
                            _OrderID = LVData.Items[i].SubItems[2].Text;
                            _Order_Obj = new Stock_Input_Object(_OrderID);
                            _Order_Obj.DeleteObject();
                            zMessage += TN_Message.Show(_Order_Obj.Message);
                        }
                    }
                    if (zMessage.Length == 0)
                    {
                        Utils.TNMessageBoxOK("Xóa Thành Công", 3);
                        InitLV_Data();
                    }
                    else
                    {
                        Utils.TNMessageBoxOK(zMessage, 4);
                    }
                }
            }
        }

        #endregion

        #endregion

        #region ----- Right -----
        #region ----- Warehouse -----
        private void InitCbo_Warehouse()
        {
            LoadDataToToolbox.ComboBoxData(cbo_Warehouse, Warehouse_Data.List(_WareHouse), false, 0, 1);
            LoadDataToToolbox.ComboBoxData(cbo_Warehouse_Local, Warehouse_Data.List_Factory(_WareHouse), false, 0, 1);
        }
        #endregion

        #region ----- GridView Product ------

        private void InitGVData_Layout()
        {
            GVData.Columns.Add("No", "STT");
            GVData.Columns.Add("ProductID", "Mã");
            GVData.Columns.Add("ProductName", "Tên Nguyên vật liệu");
            GVData.Columns.Add("UnitName", "Đơn vị");
            GVData.Columns.Add("QuantityDocument", "SL Chứng Từ");
            GVData.Columns.Add("QuantityReality", "SL Thực Tế");
            GVData.Columns.Add("Difference", "Chênh Lệch");
            GVData.Columns.Add("QuantityBasic", "SL Cơ bản");
            GVData.Columns.Add("Price", "Đơn giá");
            GVData.Columns.Add("Total", "Thành Tiền");
            GVData.Columns.Add("Follow", "Theo CT");

            DataGridViewButtonColumn btn = new DataGridViewButtonColumn();
            btn.Name = "";
            btn.Text = "...";
            btn.UseColumnTextForButtonValue = true;
            GVData.Columns.Add(btn);
            GVData.Columns.Add("ProductFollow", "Nguyên liệu CT");
            DataGridViewComboBoxColumn cboCategory = new DataGridViewComboBoxColumn();
            cboCategory.HeaderText = "Loại";
            cboCategory.Name = "cboCategory";
            LoadDataToToolbox.ComboBoxData(cboCategory, " SELECT TypeKey, TypeName FROM IVT_Product_Type WHERE RecordStatus <> 99", true);
            cboCategory.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
            GVData.Columns.Add(cboCategory);

            GVData.Columns.Add("NoteStatus", "Ghi chú");


            GVData.Columns["No"].Width = 30;
            GVData.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GVData.Columns["No"].ReadOnly = true;

            GVData.Columns["ProductID"].Width = 100;
            GVData.Columns["ProductID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GVData.Columns["ProductName"].Width = 205;
            GVData.Columns["ProductName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GVData.Columns["UnitName"].Width = 50;
            GVData.Columns["UnitName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GVData.Columns["QuantityDocument"].Width = 90;
            GVData.Columns["QuantityDocument"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GVData.Columns["QuantityReality"].Width = 90;
            GVData.Columns["QuantityReality"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GVData.Columns["Difference"].Width = 90;
            GVData.Columns["Difference"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GVData.Columns["Difference"].ReadOnly = true;

            GVData.Columns["QuantityBasic"].Width = 0;
            GVData.Columns["QuantityBasic"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GVData.Columns["QuantityBasic"].Visible = false;

            GVData.Columns["Price"].Width = 0;
            GVData.Columns["Price"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GVData.Columns["Price"].Visible = false;

            GVData.Columns["Total"].Width = 0;
            GVData.Columns["Total"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GVData.Columns["Total"].Visible = false;

            GVData.Columns["Follow"].Width = 120;
            GVData.Columns["Follow"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GVData.Columns["ProductFollow"].Width = 100;
            GVData.Columns["ProductFollow"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GVData.Columns["cboCategory"].Width = 100;

            GVData.Columns["NoteStatus"].Width = 120;
            GVData.Columns["NoteStatus"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GVData.Columns[""].Width = 40;
            GVData.Columns[""].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

        }
        private void GVData_LoadData()
        {
            GVData.Rows.Clear();
            for (int i = 0; i < _Order_Obj.List_Product.Count; i++)
            {
                GVData.Rows.Add();

                Stock_Input_Products_Info zProduct = _Order_Obj.List_Product[i];
                GVData.Rows[i].Tag = zProduct;
                GVData.Rows[i].Cells["No"].Value = (i + 1).ToString();
                GVData.Rows[i].Cells["No"].Tag = zProduct.AutoKey;
                GVData.Rows[i].Cells["ProductID"].Value = zProduct.ProductID;
                GVData.Rows[i].Cells["ProductName"].Value = zProduct.ProductName;
                GVData.Rows[i].Cells["UnitName"].Value = zProduct.UnitName;
                GVData.Rows[i].Cells["QuantityReality"].Value = zProduct.QuantityReality.Ton2String();
                if (zProduct.QuantityDocument > 0)
                {
                    GVData.Rows[i].Cells["QuantityDocument"].Value = zProduct.QuantityDocument.Ton2String();
                    GVData.Rows[i].Cells["Difference"].Value = (zProduct.QuantityDocument - zProduct.QuantityReality).Ton2String();
                }
                GVData.Rows[i].Cells["QuantityBasic"].Value = zProduct.QuantityBasic.Ton2String() + "/" + zProduct.UnitBasicName;
                GVData.Rows[i].Cells["cboCategory"].Value = zProduct.ProductType;
                GVData.Rows[i].Cells["Follow"].Value = zProduct.OrderIDFollow;
                GVData.Rows[i].Cells["ProductFollow"].Value = zProduct.ProductFollow;
                GVData.Rows[i].Cells["ProductFollow"].Tag = zProduct.ProductKeyFollow;
                GVData.Rows[i].Cells["NoteStatus"].Value = zProduct.NoteStatus;
            }
        }
        private string CtrlValue = "";
        private void GVData_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                if(Utils.TNMessageBox("Bạn có xóa thông tin này ?",2)=="Y")
                {
                    for (int i = 0; i < GVData.Rows.Count; i++)
                    {
                        if (GVData.Rows[i].Selected)
                        {
                            if (GVData.Rows[i].Tag != null)
                            {
                                Stock_Input_Products_Info zProduct = (Stock_Input_Products_Info)GVData.Rows[i].Tag;
                                GVData.Rows[i].Visible = false;
                                zProduct.RecordStatus = 99;
                                k = 1;
                                for (int y = 0; y < GVData.Rows.Count - 1; y++)
                                {
                                    if (GVData.Rows[y].Cells[0].Value != null)
                                    {
                                        k++;
                                    }
                                }
                            }
                        }
                    }
                    GV_Sum_LoadData();
                }
            }
            if (e.KeyCode == Keys.C && e.Control)
            {
                CtrlValue = GVData.CurrentCell.Value.ToString();
            }
            if (e.KeyCode == Keys.V && e.Control)
            {
                GVData.CurrentCell.Value = CtrlValue;
                GVData_CellEndEdit(sender, new DataGridViewCellEventArgs(GVData.CurrentCell.ColumnIndex, GVData.CurrentCell.RowIndex));
            }

        }
        private void GVData_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            DataGridViewTextBoxEditingControl te;
            switch (GVData.CurrentCell.ColumnIndex)
            {
                case 1: // autocomplete for product
                    te = (DataGridViewTextBoxEditingControl)e.Control;
                    te.AutoCompleteMode = AutoCompleteMode.None;
                    LoadDataToToolbox.AutoCompleteTextBox(te, "SELECT ProductID FROM IVT_Product WHERE Parent != '0' AND RecordStatus < 99  ");

                    e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);
                    break;
                case 2: // autocomplete for product
                    te = (DataGridViewTextBoxEditingControl)e.Control;
                    te.AutoCompleteMode = AutoCompleteMode.None;
                    LoadDataToToolbox.AutoCompleteTextBox(te, "SELECT ProductName FROM IVT_Product WHERE Parent != '0' AND RecordStatus < 99 ");

                    e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);
                    break;
                case 3:
                    te = (DataGridViewTextBoxEditingControl)e.Control;
                    te.AutoCompleteMode = AutoCompleteMode.None;
                    LoadDataToToolbox.AutoCompleteTextBox(te, "SELECT UnitName FROM IVT_Product_Unit WHERE RecordStatus <99");

                    e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);
                    break;
                case 4: // Unit Price
                    e.Control.KeyPress += new KeyPressEventHandler(ControlNumber_KeyPress);
                    break;
                case 5:
                    e.Control.KeyPress += new KeyPressEventHandler(ControlNumber_KeyPress);
                    break;
                case 6:
                    e.Control.KeyPress += new KeyPressEventHandler(ControlNumber_KeyPress);
                    break;

                default:
                    e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);

                    break;
            }
        }
        private int currentRow;
        private bool resetRow = false;
        private void GVData_SelectionChanged(object sender, EventArgs e)
        {
            //if (resetRow)
            //{
            //    resetRow = false;
            //    GVData.CurrentCell = GVData.Rows[currentRow].Cells[0];
            //}
        }
        private void GVData_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow zRowEdit = GVData.Rows[e.RowIndex];
            Stock_Input_Products_Info zProduct;
            if (GVData.Rows[e.RowIndex].Tag == null)
            {
                zProduct = new Stock_Input_Products_Info();
                GVData.Rows[e.RowIndex].Tag = zProduct;
            }
            else
            {
                zProduct = (Stock_Input_Products_Info)GVData.Rows[e.RowIndex].Tag;
            }
            bool zIsChangeValued = false;
            switch (e.ColumnIndex)
            {
                case 1: // thong tin ve ma san pham
                    if (zRowEdit.Cells[e.ColumnIndex].Value == null)
                    {
                        zProduct.ProductKey = "";
                        zProduct.ProductID = "";
                        zProduct.ProductName = "";
                        zIsChangeValued = true;
                    }
                    else
                    {
                        if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() != zProduct.ProductID)
                        {
                            zProduct.ProductID = zRowEdit.Cells[e.ColumnIndex].Value.ToString();
                            _ProductID = zRowEdit.Cells[e.ColumnIndex].Value.ToString();
                            zProduct.ProductName = "";
                            zIsChangeValued = true;
                        }
                    }
                    break;
                case 2: // Thong tin ve san pham
                    if (zRowEdit.Cells[e.ColumnIndex].Value == null)
                    {
                        zProduct.ProductKey = "";
                        zProduct.ProductID = "";
                        zProduct.ProductName = "";
                        zIsChangeValued = true;
                    }
                    else
                    {
                        if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() != zProduct.ProductName)
                        {
                            zProduct.ProductID = "";
                            zProduct.ProductName = zRowEdit.Cells[e.ColumnIndex].Value.ToString();
                            _ProductName = zRowEdit.Cells[e.ColumnIndex].Value.ToString();

                            zIsChangeValued = true;
                        }
                    }
                    break;
                case 3: // Đơn vị tính
                    if (zRowEdit.Cells[e.ColumnIndex].Value == null)
                    {
                        zProduct.UnitKey = 0;
                        zProduct.UnitName = "";
                        zIsChangeValued = true;
                    }
                    else
                    {
                        if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() != zProduct.UnitName)
                        {
                            zProduct.UnitName = zRowEdit.Cells[e.ColumnIndex].Value.ToString(); //; 
                            Product_Unit_Info zIVT_Unit = new Product_Unit_Info(zProduct.UnitName);
                            zProduct.UnitKey = zIVT_Unit.Key;
                            zIsChangeValued = true;
                        }
                    }
                    break;
                case 4: // thong tin ve so luong theo giấy tờ
                    float zQuantityDocument = 0;
                    if (zRowEdit.Cells[e.ColumnIndex].Value != null)
                    {
                        //if (float.TryParse(zRowEdit.Cells["QuantityDocument"].Value.ToString(), NumberStyles.Any, _FormatProviderQuantity, out zQuantityDocument))
                        if (float.TryParse(zRowEdit.Cells["QuantityDocument"].Value.ToString(), out zQuantityDocument))
                        {
                            zProduct.QuantityDocument = zQuantityDocument;
                            zRowEdit.Cells["QuantityDocument"].ErrorText = "";
                            zIsChangeValued = true;
                        }
                        else
                        {
                            zProduct.QuantityDocument = 0;
                            zRowEdit.Cells["QuantityDocument"].ErrorText = "Sai định dạng số lượng";

                        }
                    }

                    break;
                case 5: // thong tin ve so luong theo thực tế
                    float zQuantity = 0;
                    if (zRowEdit.Cells[e.ColumnIndex].Value != null)
                    {
                        //if (float.TryParse(zRowEdit.Cells["QuantityReality"].Value.ToString(), NumberStyles.Any, _FormatProviderQuantity, out zQuantity))
                        if (float.TryParse(zRowEdit.Cells["QuantityReality"].Value.ToString(), out zQuantity))
                        {
                            zProduct.QuantityReality = zQuantity;
                            zRowEdit.Cells["QuantityReality"].ErrorText = "";
                            zIsChangeValued = true;
                        }
                        else
                        {
                            zProduct.QuantityReality = 0;
                            zRowEdit.Cells["QuantityReality"].ErrorText = "Sai định dạng số lượng";
                        }
                    }
                    break;
                //case 10: // thong tin ve chứng từ
                //    if (zRowEdit.Cells[e.ColumnIndex].Value != null)
                //    {
                //        string zOrderID = zRowEdit.Cells[e.ColumnIndex].Value.ToString();
                //        string zSQL = "SELECT OrderKey FROM IVT_Stock_Output WHERE RecordStatus = 43 AND OrderID = '" + zOrderID + "'";
                //        string zOrderKey = Data_Access.GetValue(zSQL);

                //        if (zOrderKey.Length > 0)
                //        {
                //            float zQuantityInput = Stock_Data.SumInput((int)cbo_Warehouse.SelectedValue, zOrderKey, zProduct.ProductKey);
                //            if (zProduct.QuantityReality > zQuantityInput)
                //            {
                //                MessageBox.Show("Vui lòng kiểm tra lại các số lượng chứng từ này !.");
                //            }

                //            zRowEdit.Cells[e.ColumnIndex].ErrorText = "";
                //            zProduct.OrderIDFollow = zOrderID;
                //            zProduct.OrderKeyFollow = zOrderKey;
                //        }
                //        else
                //        {
                //            zRowEdit.Cells[e.ColumnIndex].ErrorText = "Không có số chứng từ này";
                //            zProduct.OrderIDFollow = "";
                //            zProduct.OrderKeyFollow = "";
                //        }
                //    }
                //    else
                //    {
                //        MessageBox.Show("Vui lòng kiểm tra chứng từ !.");
                //    }
                //    break;

                case 13: // thong tin ve loại
                    if (zRowEdit.Cells[e.ColumnIndex].Value == null)
                    {
                        zProduct.ProductType = 0;
                        zIsChangeValued = true;
                    }
                    else
                    {

                        zProduct.ProductType = zRowEdit.Cells[e.ColumnIndex].Value.ToInt();
                        zIsChangeValued = true;

                    }
                    break;
                case 14:
                    if (zRowEdit.Cells[e.ColumnIndex].Value == null)
                    {
                        zProduct.NoteStatus = "";
                        zIsChangeValued = true;
                    }
                    else
                    {
                        if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() != zProduct.NoteStatus)
                        {
                            zProduct.NoteStatus = zRowEdit.Cells[e.ColumnIndex].Value.ToString(); //;  
                            zIsChangeValued = true;
                        }
                    }
                    break;
            }
            if (zIsChangeValued)
            {
                zProduct.RecordStatus = 1;
                CaculateAmount(e.RowIndex);
                ShowProductInGridView(e.RowIndex, e.ColumnIndex);
                GV_Sum_LoadData();
            }
            if (zProduct.AutoKey != 0)
                zProduct.RecordStatus = 2;
            else
                zProduct.RecordStatus = 1;

            //resetRow = true;
            currentRow = e.RowIndex;
            GVData.SelectionChanged += GVData_SelectionChanged;
        }
        private void GV(int RowIndex, int ColumnIndex)
        {
            DataGridViewRow zRowEdit = GVData.Rows[RowIndex];
            Stock_Input_Products_Info zProduct;
            if (GVData.Rows[RowIndex].Tag == null)
            {
                zProduct = new Stock_Input_Products_Info();
                GVData.Rows[RowIndex].Tag = zProduct;
            }
            else
            {
                zProduct = (Stock_Input_Products_Info)GVData.Rows[RowIndex].Tag;
            }
            bool zIsChangeValued = false;
            switch (ColumnIndex)
            {
                case 1: // thong tin ve ma san pham
                    if (zRowEdit.Cells[ColumnIndex].Value == null)
                    {
                        zProduct.ProductKey = "";
                        zProduct.ProductID = "";
                        zProduct.ProductName = "";
                        zIsChangeValued = true;
                    }
                    else
                    {
                        if (zRowEdit.Cells[ColumnIndex].Value.ToString() != zProduct.ProductID)
                        {
                            zProduct.ProductID = zRowEdit.Cells[ColumnIndex].Value.ToString(); //;  
                            zProduct.ProductName = "";
                            _ProductID = zRowEdit.Cells[ColumnIndex].Value.ToString();
                            zIsChangeValued = true;
                        }
                    }
                    break;
                case 2: // Thong tin ve san pham
                    if (zRowEdit.Cells[ColumnIndex].Value == null)
                    {
                        zProduct.ProductKey = "";
                        zProduct.ProductID = "";
                        zProduct.ProductName = "";
                        zIsChangeValued = true;
                    }
                    else
                    {
                        if (zRowEdit.Cells[ColumnIndex].Value.ToString() != zProduct.ProductName)
                        {
                            zProduct.ProductID = "";
                            zProduct.ProductName = zRowEdit.Cells[ColumnIndex].Value.ToString();
                            _ProductName = zRowEdit.Cells[ColumnIndex].Value.ToString();

                            zIsChangeValued = true;
                        }
                    }
                    break;
                case 3: // Đơn vị tính
                    if (zRowEdit.Cells[ColumnIndex].Value == null)
                    {
                        zProduct.UnitKey = 0;
                        zProduct.UnitName = "";
                        zIsChangeValued = true;
                    }
                    else
                    {
                        if (zRowEdit.Cells[ColumnIndex].Value.ToString() != zProduct.UnitName)
                        {
                            zProduct.UnitName = zRowEdit.Cells[ColumnIndex].Value.ToString(); //; 
                            Product_Unit_Info zIVT_Unit = new Product_Unit_Info(zProduct.UnitName);
                            zProduct.UnitKey = zIVT_Unit.Key;
                            zIsChangeValued = true;
                        }
                    }
                    break;
                case 4: // thong tin ve so luong theo giấy tờ
                    float zQuantityDocument = 0;
                    if (zRowEdit.Cells[ColumnIndex].Value != null)
                    {
                        //if (float.TryParse(zRowEdit.Cells["QuantityDocument"].Value.ToString(), NumberStyles.Any, _FormatProviderQuantity, out zQuantityDocument))
                        if (float.TryParse(zRowEdit.Cells["QuantityDocument"].Value.ToString(), out zQuantityDocument))
                        {
                            zProduct.QuantityDocument = zQuantityDocument;
                            zRowEdit.Cells["QuantityDocument"].ErrorText = "";
                            zIsChangeValued = true;
                        }
                        else
                        {
                            zProduct.QuantityDocument = 0;
                            zRowEdit.Cells["QuantityDocument"].ErrorText = "Sai định dạng số lượng";

                        }
                    }

                    break;
                case 5: // thong tin ve so luong theo thực tế
                    float zQuantity = 0;
                    if (zRowEdit.Cells[ColumnIndex].Value != null)
                    {//  if (float.TryParse(zRowEdit.Cells["QuantityReality"].Value.ToString(), NumberStyles.Any, _FormatProviderQuantity, out zQuantity))
                        if (float.TryParse(zRowEdit.Cells["QuantityReality"].Value.ToString(), out zQuantity))
                        {
                            zProduct.QuantityReality = zQuantity;
                            zRowEdit.Cells["QuantityReality"].ErrorText = "";
                            zIsChangeValued = true;
                        }
                        else
                        {
                            zProduct.QuantityReality = 0;
                            zRowEdit.Cells["QuantityReality"].ErrorText = "Sai định dạng số lượng";
                        }
                    }
                    break;

                case 13: // thong tin ve loại
                    if (zRowEdit.Cells[ColumnIndex].Value == null)
                    {
                        zProduct.ProductType = 0;
                        zIsChangeValued = true;
                    }
                    else
                    {

                        zProduct.ProductType = zRowEdit.Cells[ColumnIndex].Value.ToInt();
                        zIsChangeValued = true;

                    }
                    break;
                case 14: // thong tin ve ghi chu
                    if (zRowEdit.Cells[ColumnIndex].Value == null)
                    {
                        zProduct.NoteStatus = "";
                        zIsChangeValued = true;
                    }
                    else
                    {
                        if (zRowEdit.Cells[ColumnIndex].Value.ToString() != zProduct.NoteStatus)
                        {
                            zProduct.NoteStatus = zRowEdit.Cells[ColumnIndex].Value.ToString(); //;  
                            zIsChangeValued = true;
                        }
                    }
                    break;
            }
            if (zIsChangeValued)
            {
                zProduct.RecordStatus = 1;
                CaculateAmount(RowIndex);
                ShowProductInGridView(RowIndex, ColumnIndex);
            }
            if (zProduct.AutoKey != 0)
                zProduct.RecordStatus = 2;
            else
                zProduct.RecordStatus = 1;
        }
        private void CaculateAmount(int RowIndex)
        {
            Stock_Input_Products_Info zProduct = (Stock_Input_Products_Info)GVData.Rows[RowIndex].Tag;
            Product_Info zProductSearch = new Product_Info();
            if (zProduct.ProductID.Length > 0 && zProduct.ProductName.Length == 0)
            {
                zProductSearch.Get_Product_Info_ID(zProduct.ProductID);
                zProduct.ProductKey = zProductSearch.ProductKey;
                zProduct.ProductID = zProductSearch.ProductID;
                zProduct.ProductName = zProductSearch.ProductName;
                zProduct.UnitKey = zProductSearch.BasicUnit;
                zProduct.UnitName = zProductSearch.UnitName;
            }
            if (zProduct.ProductID.Length == 0 && zProduct.ProductName.Length > 0)
            {
                zProductSearch.Get_Product_Info(zProduct.ProductName);
                zProduct.ProductKey = zProductSearch.ProductKey;
                zProduct.ProductID = zProductSearch.ProductID;
                zProduct.ProductName = zProductSearch.ProductName;
                zProduct.UnitKey = zProductSearch.BasicUnit;
                zProduct.UnitName = zProductSearch.UnitName;
            }

            zProduct.Difference = zProduct.QuantityDocument - zProduct.QuantityReality;
            zProduct.QuantityBasic = zProduct.Proportion * zProduct.QuantityReality;

        }
        private void ShowProductInGridView(int RowIndex, int Columns)
        {
            DataGridViewRow zRowView = GVData.Rows[RowIndex];
            Stock_Input_Products_Info zProduct = (Stock_Input_Products_Info)zRowView.Tag;

            zRowView.Cells["ProductID"].ErrorText = "";
            zRowView.Cells["ProductName"].ErrorText = "";
            zRowView.Cells["QuantityDocument"].ErrorText = "";
            zRowView.Cells["QuantityReality"].ErrorText = "";

            if (GVData.Rows[RowIndex].Cells[1].Value != null && zProduct.ProductName.Length == 0)
            {
                Frm_SearchProduct frm = new Frm_SearchProduct();
                frm._te = _ProductID;
                frm.ShowDialog();
                _ProductName = frm._ProductName;
                _ProductID = frm._ProductID;
                _UnitName = frm._UnitName;
                GVData.Rows[RowIndex].Cells["ProductID"].Value = _ProductID;
                if (_ProductID.Length > 0)
                {
                    GV(RowIndex, Columns);
                }
            }

            if (GVData.Rows[RowIndex].Cells[2].Value != null && zProduct.ProductName.Length == 0)
            {
                Frm_SearchProduct frm = new Frm_SearchProduct();
                frm._te = _ProductName;
                frm.ShowDialog();
                _ProductName = frm._ProductName;
                _ProductID = frm._ProductID;
                _UnitName = frm._UnitName;
                GVData.Rows[RowIndex].Cells["ProductName"].Value = _ProductName;
                if (_ProductName.Length > 0)
                {
                    GV(RowIndex, Columns);
                }
            }

            if (zProduct.ProductID.Length != 0 ||
                zProduct.ProductName.Length != 0)
            {
                zRowView.Cells["No"].Value = (RowIndex + 1).ToString();
                zRowView.Cells["ProductName"].Value = zProduct.ProductName;
                zRowView.Cells["ProductID"].Value = zProduct.ProductID;
                zRowView.Cells["UnitName"].Value = zProduct.UnitName;

                if (zProduct.QuantityDocument > 0)
                {
                    zRowView.Cells["QuantityDocument"].Value = zProduct.QuantityDocument.Ton2String();
                }
                else
                {
                    zRowView.Cells["QuantityDocument"].Value = null;
                }

                if (zProduct.QuantityReality > 0)
                {
                    zRowView.Cells["QuantityReality"].Value = zProduct.QuantityReality.Ton2String();
                }
                else
                {
                    zRowView.Cells["QuantityReality"].Value = null;
                }

                if (zProduct.Difference > 0)
                {
                    zRowView.Cells["Difference"].Value = (zProduct.QuantityDocument - zProduct.QuantityReality).Ton2String();
                }
                else
                {
                    zRowView.Cells["Difference"].Value = null;
                }

                if (zProduct.QuantityBasic > 0)
                {
                    zRowView.Cells["QuantityBasic"].Value = zProduct.QuantityBasic.ToString() + "/" + zProduct.UnitBasicName;
                }
                else
                {
                    zRowView.Cells["QuantityBasic"].Value = null;
                }
                if (zProduct.QuantityDocument < zProduct.QuantityReality)
                {
                    zRowView.Cells["QuantityReality"].ErrorText = "Số lượng thực tế không được lớn hơn số lượng chứng từ";
                }
                zRowView.Cells["NoteStatus"].Value = zProduct.NoteStatus;
            }
        }
        private void GVData_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 11)
            {
                if (StyleForm == 1 && Status == 1)
                {
                    if (cbo_Warehouse.SelectedValue != null)
                    {
                        Frm_Voucher_Output Frm = new Frm_Voucher_Output();
                        Frm.WarehouseName = cbo_Warehouse.Text;
                        Frm.ShowDialog();
                        Add_Product(GVData.CurrentRow.Index, Frm.OrderID, Frm.ProductID);
                    }
                }
                if (StyleForm == 2 && Status == 2)
                {
                    Utils.TNMessageBoxOK("Bạn không được phép vào phần này !",1);
                }

            }
        }
        #endregion

        #region [-----Gidview-Sum--------]

        private void InitGVSum_Layout()
        {
            // Setup Column 
            GVSum.Columns.Add("SUM", "TỔNG");
            GVSum.Columns.Add("Sum_Document", "SL chứng từ");
            GVSum.Columns.Add("Sum_Real", "SL thực tế");
            GVSum.Columns.Add("Sum_Difference", "SL chênh lệch");

            GVSum.Columns["SUM"].Width = 440;
            GVSum.Columns["SUM"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GVSum.Columns["SUM"].ReadOnly = true;

            GVSum.Columns["Sum_Document"].Width = 100;
            GVSum.Columns["Sum_Document"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GVSum.Columns["Sum_Document"].ReadOnly = true;

            GVSum.Columns["Sum_Real"].Width = 100;
            GVSum.Columns["Sum_Real"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GVSum.Columns["Sum_Real"].ReadOnly = true;

            GVSum.Columns["Sum_Difference"].Width = 100;
            GVSum.Columns["Sum_Difference"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GVSum.Columns["Sum_Difference"].ReadOnly = true;
        }
        private void GV_Sum_LoadData()
        {
            double zDocument = 0;
            double zRealy = 0;
            double zDifferent = 0;
            for (int i = 0; i < GVData.Rows.Count - 1; i++)
            {
                if (GVData.Rows[i].Tag != null && GVData.Rows[i].Visible == true)
                {
                    Stock_Input_Products_Info zProduct = (Stock_Input_Products_Info)GVData.Rows[i].Tag;
                    zDocument += zProduct.QuantityDocument;
                    zRealy += zProduct.QuantityReality;
                    zDifferent += (zProduct.QuantityDocument - zProduct.QuantityReality);
                }
            }
            GVSum.Rows.Clear();
            GVSum.Rows.Add();
            GVSum.Rows[0].Cells["Sum_Document"].Value = zDocument.Ton2String();
            GVSum.Rows[0].Cells["Sum_Real"].Value = zRealy.Ton2String();
            GVSum.Rows[0].Cells["Sum_Difference"].Value = zDifferent.Ton2String();
        }
        #endregion

        #region ----- GridView Post -----
        public void InitGVNote_Layout()
        {
            // Setup Column 
            GVNote.Columns.Add("No", "STT");
            GVNote.Columns.Add("Date", "Ngày");
            GVNote.Columns.Add("Content", "Nội Dung");
            GVNote.Columns.Add("Employer", "Nhân Viên");

            GVNote.Columns["No"].Width = 40;
            GVNote.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GVNote.Columns["No"].ReadOnly = true;

            GVNote.Columns["Date"].Width = 150;
            GVNote.Columns["Date"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GVNote.Columns["Date"].ReadOnly = true;

            GVNote.Columns["Content"].Width = 400;
            GVNote.Columns["Content"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GVNote.Columns["Content"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GVNote.Columns["Content"].ReadOnly = true;

            GVNote.Columns["Employer"].Width = 150;
            GVNote.Columns["Employer"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GVNote.Columns["Employer"].ReadOnly = true;

            GVNote.ReadOnly = true;
        }
        private void GV_Post_LoadData()
        {
            GVNote.Rows.Clear();

            int i = 0;
            foreach (DataRow zRow in _Order_Obj.ListNote.Rows)
            {
                GVNote.Rows.Add();

                DateTime zTime = (DateTime)zRow["CreatedOn"];
                GVNote.Rows[i].Cells["No"].Value = i + 1;
                GVNote.Rows[i].Cells["No"].Tag = zRow["AutoKey"].ToString();
                GVNote.Rows[i].Cells["Date"].Value = zTime.ToString("dd/MM/yyyy HH:mm");
                GVNote.Rows[i].Cells["Content"].Value = zRow["NoteContent"].ToString();
                GVNote.Rows[i].Cells["Employer"].Value = zRow["CreatedName"].ToString();
                i++;
            }

        }
        private void GV_Post_KeyDown(object sender, KeyEventArgs e)
        {
            string zMessage = "";
            if (e.KeyCode == Keys.Delete)
            {
                if(Utils.TNMessageBox("Bạn có xóa thông tin này ?",2)=="Y")
                {
                    for (int i = 0; i < GVNote.Rows.Count; i++)
                    {
                        if (GVNote.Rows[i].Selected)
                        {
                            if (GVNote.Rows[i].Cells["No"].Tag != null)
                            {
                                Stock_Input_Object zinfo = new Stock_Input_Object();
                                zinfo.AutoKey = int.Parse(GVNote.Rows[i].Cells["No"].Tag.ToString());
                                zinfo.DeleteNote();
                                zMessage += zinfo.Message;
                            }
                        }
                    }
                    if (zMessage.Length == 0)
                    {
                        Utils.TNMessageBoxOK("Đã xóa thành công !",3);
                    }
                    else
                    {
                        Utils.TNMessageBoxOK(zMessage, 4);
                    }
                }
            }
        }
        #endregion

        #region ------ Process Data -----
        private void CheckReadOnlyTrue()
        {
            GVData.ReadOnly = true;
            txt_Order_ID.ReadOnly = true;
            cbo_Warehouse.Enabled = false;
            cbo_Warehouse_Local.Enabled = false;
            txt_Description.ReadOnly = true;
            btn_Save.Enabled = false;
            btn_Edit.Enabled = true;
        }
        private void CheckReadOnlyFalse()
        {
            GVData.ReadOnly = false;
            txt_Order_ID.ReadOnly = false;
            cbo_Warehouse.Enabled = true;
            cbo_Warehouse_Local.Enabled = true;
            txt_Description.ReadOnly = false;
            btn_Save.Enabled = true;
            btn_Edit.Enabled = false;
        }
        private void ReturnColor()
        {
            cbo_Warehouse.BackColor = Color.FromArgb(255, 255, 225);
            txt_Order_ID.BackColor = Color.FromArgb(255, 255, 225);
            cbo_Warehouse_Local.BackColor = Color.FromArgb(255, 255, 225);
        }
        private void Check_Record()
        {
            if (StyleForm == 1 && Status == 1)
            {
                if (_Order_Obj.RecordStatus == 0)
                {
                    btn_Del.Visible = true;
                    btn_Send.Visible = true;
                    btn_Save.Visible = true;
                    btn_New.Visible = true;
                    btn_Copy.Visible = true;
                    btn_Import.Visible = true;
                    CheckReadOnlyFalse();
                    btn_Approve.Visible = false;
                    GVData.ReadOnly = false;
                    btn_Deny.Visible = false;
                    btn_Edit.Visible = true;
                    if (_OrderID.Length == 0)
                    {
                        DataRow zAdd = User_Data.GetUserAdd(SessionUser.UserLogin.Key, RoleID).Rows[0];
                        if (Convert.ToInt32(zAdd[0]) > 0)
                        {
                            btn_Save.Visible = true;
                        }
                        else
                        {
                            btn_Save.Visible = false;
                        }
                    }
                    if (_OrderID.Length > 0)
                    {
                        DataRow zEdit = User_Data.GetUserEdit(SessionUser.UserLogin.Key, RoleID).Rows[0];
                        if (Convert.ToInt32(zEdit[0]) > 0)
                        {
                            btn_Save.Visible = true;
                        }
                        else
                        {
                            btn_Save.Visible = false;
                        }
                    }
                    DataRow zDelete = User_Data.GetUserDel(SessionUser.UserLogin.Key, RoleID).Rows[0];
                    if (Convert.ToInt32(zDelete[0]) > 0)
                    {
                        btn_Del.Visible = true;
                    }
                    else
                    {
                        btn_Del.Visible = false;
                    }
                }
                if (_Order_Obj.RecordStatus == 41)
                {
                    btn_Del.Visible = false;
                    btn_Send.Visible = false;
                    btn_New.Visible = true;
                    btn_Copy.Visible = true;
                    btn_Import.Visible = true;
                    btn_Save.Visible = false;
                    SetReadOnlyControl_Approve();
                    btn_Edit.Visible = false;
                    btn_Approve.Visible = false;
                    btn_Deny.Visible = false;
                }
                if (_Order_Obj.RecordStatus == 42)
                {
                    btn_Del.Visible = true;
                    btn_Send.Visible = true;
                    btn_New.Visible = true;
                    btn_Copy.Visible = true;
                    btn_Import.Visible = true;
                    btn_Save.Visible = true;
                    SetReadOnlyControl_NotApprove();
                    GVData.ReadOnly = false;
                    btn_Edit.Visible = true;
                    btn_Approve.Visible = false;
                    btn_Deny.Visible = false;
                    if (_OrderID.Length == 0)
                    {
                        DataRow zAdd = User_Data.GetUserAdd(SessionUser.UserLogin.Key, RoleID).Rows[0];
                        if (Convert.ToInt32(zAdd[0]) > 0)
                        {
                            btn_Save.Visible = true;
                        }
                        else
                        {
                            btn_Save.Visible = false;
                        }
                    }
                    if (_OrderID.Length > 0)
                    {
                        DataRow zEdit = User_Data.GetUserEdit(SessionUser.UserLogin.Key, RoleID).Rows[0];
                        if (Convert.ToInt32(zEdit[0]) > 0)
                        {
                            btn_Save.Visible = true;
                        }
                        else
                        {
                            btn_Save.Visible = false;
                        }
                    }
                    DataRow zDelete = User_Data.GetUserDel(SessionUser.UserLogin.Key, RoleID).Rows[0];
                    if (Convert.ToInt32(zDelete[0]) > 0)
                    {
                        btn_Del.Visible = true;
                    }
                    else
                    {
                        btn_Del.Visible = false;
                    }
                }
                if (_Order_Obj.RecordStatus == 43)
                {
                    btn_Del.Visible = false;
                    btn_Send.Visible = false;
                    btn_New.Visible = true;
                    btn_Copy.Visible = true;
                    btn_Import.Visible = true;
                    btn_Save.Visible = false;
                    SetReadOnlyControl_NotApprove();
                    GVData.ReadOnly = true;
                    btn_Approve.Visible = false;
                    btn_Deny.Visible = false;
                    btn_Edit.Visible = false;
                }
            }
            if (StyleForm == 2 && Status == 2)
            {
                btn_Del.Visible = false;
                btn_Send.Visible = false;
                btn_New.Visible = false;
                btn_Copy.Visible = false;
                btn_Import.Visible = false;
                btn_Save.Visible = false;
                SetReadOnlyControl_Approve();
                btn_Approve.Visible = true;
                btn_Deny.Visible = true;
                GVData.ReadOnly = true;
                GVData.Columns[12].Visible = false;
                btn_Edit.Visible = false;
                DataRow zApprove = User_Data.GetUserApprove(SessionUser.UserLogin.Key, RoleID).Rows[0];
                if (Convert.ToInt32(zApprove[0]) > 0)
                {
                    btn_Approve.Visible = true;
                    btn_Deny.Visible = true;
                }
                else
                {
                    btn_Approve.Visible = false;
                    btn_Deny.Visible = false;
                }
            }
        }
        private void LoadOrderInfo()
        {
            txt_Order_ID.Text = _Order_Obj.ID;
            if (_Order_Obj.OrderDate != DateTime.MinValue)
            {
                dte_OrderDate.Value = _Order_Obj.OrderDate;
            }
            else
            {
                dte_OrderDate.Value = SessionUser.Date_Work;
            }
            txt_Description.Text = _Order_Obj.Description;


            if (_Order_Obj.WarehouseKey != 0)
            {
                cbo_Warehouse.SelectedValue = _Order_Obj.WarehouseKey;
            }
            if (_Order_Obj.WarehouseLocal != 0)
            {
                cbo_Warehouse_Local.SelectedValue = _Order_Obj.WarehouseLocal;
            }
            GVData_LoadData();
            GV_Sum_LoadData();
            GV_Post_LoadData();
            Check_Record();
        }
        private string CheckBeforeSave()
        {
            string zAmountErr = "";
            string zMessageNull = "";
            string zMessageQuantity = "";
            int A = 0;
            int B = 0;
            if (cbo_Warehouse.SelectedValue == null)
            {
                cbo_Warehouse.BackColor = Color.FromArgb(230, 100, 100);
                zAmountErr += "- Chọn Kho \n";
            }
            else
            {
                cbo_Warehouse.BackColor = Color.FromArgb(255, 255, 225);
            }
            if (cbo_Warehouse_Local.SelectedValue == null)
            {
                cbo_Warehouse_Local.BackColor = Color.FromArgb(230, 100, 100);
                zAmountErr += "- Chọn Kho \n";
            }
            else
            {
                cbo_Warehouse_Local.BackColor = Color.FromArgb(255, 255, 225);
            }
            if (txt_Order_ID.Text.Trim().Length != 3 && _OrderID == "")
            {
                txt_Order_ID.BackColor = Color.FromArgb(230, 100, 100);
                zAmountErr += "- Bạn không được nhập nhỏ hoặc hơn 3 ký tự vào Số Phiếu \n";
            }
            else
            {
                txt_Order_ID.BackColor = Color.FromArgb(255, 255, 225);
            }

            if (txt_Order_ID.Text.Trim().Length == 0)
            {
                txt_Order_ID.BackColor = Color.FromArgb(230, 100, 100);
                zAmountErr += "- Nhập Mã Phiếu \n";
            }
            else
            {
                txt_Order_ID.BackColor = Color.FromArgb(255, 255, 225);
            }


            for (int i = 0; i < GVData.Rows.Count; i++)
            {
                if (GVData.Rows[i].Visible == true && GVData.Rows[i].Cells["ProductID"].Value != null)
                {
                    A++;
                }
                if (GVData.Rows[i].Visible == true && GVData.Rows[i].Cells["ProductID"].Value == null)
                {
                    B++;
                }
            }
            if (A == 0 && B > 0)
            {
                zMessageNull += "- Điền thông tin Nguyên Liệu \n";
            }
            zAmountErr += zMessageNull;

            for (int i = 0; i < GVData.Rows.Count; i++)
            {
                if (GVData.Rows[i].Visible == true &&
                    GVData.Rows[i].Cells["ProductID"].Value != null &&
                    GVData.Rows[i].Cells["QuantityDocument"].Value == null
                    || GVData.Rows[i].Visible == true &&
                    GVData.Rows[i].Cells["ProductID"].Value != null &&
                    GVData.Rows[i].Cells["QuantityDocument"].Value.ToString() == "0")
                {
                    zMessageQuantity += "- Kiểm số lượng chứng từ \n";
                }
                if (GVData.Rows[i].Visible == true &&
                    GVData.Rows[i].Cells["ProductID"].Value != null &&
                    GVData.Rows[i].Cells["QuantityReality"].Value == null
                    || GVData.Rows[i].Visible == true &&
                    GVData.Rows[i].Cells["ProductID"].Value != null &&
                    GVData.Rows[i].Cells["QuantityReality"].Value.ToString() == "0")
                {
                    zMessageQuantity += "- Kiểm tra số lượng thực tế \n";
                }
                if (GVData.Rows[i].Cells["ProductID"].Value != null)
                {
                    float zQuantityDocument = 0;
                    float zQuantityReality = 0;
                    if (GVData.Rows[i].Cells["QuantityDocument"].Value != null)
                    {
                        zQuantityDocument = float.Parse(GVData.Rows[i].Cells["QuantityDocument"].Value.ToString());
                    }
                    else
                    {
                        zQuantityDocument = 0;
                    }

                    if (GVData.Rows[i].Cells["QuantityReality"].Value != null)
                    {
                        zQuantityReality = float.Parse(GVData.Rows[i].Cells["QuantityReality"].Value.ToString());
                    }
                    else
                    {
                        zQuantityReality = 0;
                    }
                    // Cảnh báo Số lượng thực tế lớn hơn số lượng chứng từ
                    //if (zQuantityDocument < zQuantityReality)
                    //{
                    //    zMessageQuantity += "- Số lượng thực tế không được lớn hơn số lượng chứng từ \n";
                    //}
                }
            }
            zAmountErr += zMessageQuantity;

            return zAmountErr;
        }
        private void SaveOrder()
        {
            string zRoleForAction = _WareHouseRole;
            _Order_Obj.ID = txt_Order_ID.Text;
            _Order_Obj.OrderDate = dte_OrderDate.Value;
            _Order_Obj.Description = txt_Description.Text;
            _Order_Obj.WarehouseKey = (int)cbo_Warehouse.SelectedValue;
            _Order_Obj.WarehouseLocal = (int)cbo_Warehouse_Local.SelectedValue;
            _Order_Obj.CreatedBy = SessionUser.UserLogin.Key;
            _Order_Obj.CreatedName = SessionUser.UserLogin.EmployeeName;
            _Order_Obj.ModifiedBy = SessionUser.UserLogin.Key;
            _Order_Obj.ModifiedName = SessionUser.UserLogin.EmployeeName;
            _List = new List<Stock_Input_Products_Info>();
            Stock_Input_Products_Info zProduct;
            for (int i = 0; i < GVData.Rows.Count; i++)
            {
                if (_Update == 1)
                {
                    if (GVData.Rows[i].Tag != null && GVData.Rows[i].Cells["ProductID"].Value != null)
                    {
                        zProduct = (Stock_Input_Products_Info)GVData.Rows[i].Tag;
                        if (txt_Order_ID.Text.Length == 3)
                            zProduct.RecordStatus = 1;
                        if (GVData.Rows[i].Cells["NoteStatus"].Value != null)
                        {
                            zProduct.NoteStatus = GVData.Rows[i].Cells["NoteStatus"].Value.ToString();
                        }
                        else
                        {
                            zProduct.NoteStatus = "";
                        }
                        _List.Add(zProduct);
                        _Order_Obj.List_Product = _List;
                    }
                }
                if (_Copy == 1 || _New == 1)
                    if (GVData.Rows[i].Tag != null && GVData.Rows[i].Visible == true)
                    {
                        zProduct = (Stock_Input_Products_Info)GVData.Rows[i].Tag;
                        if (txt_Order_ID.Text.Length == 3)
                            zProduct.RecordStatus = 1;
                        if (GVData.Rows[i].Cells["NoteStatus"].Value != null)
                        {
                            zProduct.NoteStatus = GVData.Rows[i].Cells["NoteStatus"].Value.ToString();
                        }
                        else
                        {
                            zProduct.NoteStatus = "";
                        }
                        _List.Add(zProduct);
                        _Order_Obj.List_Product = _List;
                    }
            }
            _Order_Obj.RoleID = zRoleForAction;
            _Order_Obj.Save_Object();
            string zMessage = TN_Message.Show(_Order_Obj.Message);
            if (zMessage.Length == 0)
            {
                Utils.TNMessageBoxOK("Câp nhật thành công", 3);
            }
            else
            {
                Utils.TNMessageBoxOK(zMessage, 4);
            }
        }

        #endregion

        #region ----- Process Event -----
        private void btn_Save_Click(object sender, EventArgs e)
        {
            if (SessionUser.Date_Lock >= dte_OrderDate.Value)
            {
                Utils.TNMessageBoxOK("Thời gian này đã khóa sổ.!", 2);
                return;
            }
            k = 1;
            if (CheckBeforeSave().Length == 0)
            {
                SaveOrder();
                InitLV_Data();

                _OrderID = _Order_Obj.ID;
                _Order_Obj = new Stock_Input_Object(_OrderID);

                LoadOrderInfo();
                InitTB_Form();
                ReturnColor();
                if (_OrderID.Length == 0)
                    btn_Send.Enabled = false;
                else
                    btn_Send.Enabled = true;
                for (int i = 0; i < GVData.Rows.Count - 1; i++)
                {
                    if (GVData.Rows[i].Tag != null)
                    {
                        k++;
                    }
                }
                CheckReadOnlyTrue();
            }
            else
            {
                Utils.TNMessageBoxOK("Vui Lòng Kiểm Tra Lại : \n" + CheckBeforeSave(), 2);
            }
        }
        private void btn_Del_Click(object sender, EventArgs e)
        {
            if (SessionUser.Date_Lock >= dte_ToDate.Value)
            {
                Utils.TNMessageBoxOK("Thời gian này đã khóa sổ.!", 2);
                return;
            }
            string zOrderID = "";
            if (CheckOrder())
            {
                for (int i = 0; i < LVData.Items.Count; i++)
                {
                    if (LVData.Items[i].Selected)
                    {
                        zOrderID += LVData.Items[i].SubItems[2].Text + ",";
                    }
                }
                string zMessage = "";
                if(Utils.TNMessageBox("Bạn có xóa thông tin phiếu " + zOrderID.Substring(0, zOrderID.Length - 1) + "  này ?", 2)=="Y")
                {
                    for (int i = 0; i < LVData.Items.Count; i++)
                    {
                        if (LVData.Items[i].Selected)
                        {
                            zOrderID = LVData.Items[i].SubItems[2].Text;
                            _Order_Obj = new Stock_Input_Object(zOrderID);
                            _Order_Obj.DeleteObject();
                            zMessage += TN_Message.Show(_Order_Obj.Message);
                        }
                    }
                    if (zMessage.Length == 0)
                    {
                        Utils.TNMessageBoxOK("Câp nhật thành công",3);
                        InitLV_Data();
                        _Order_Obj = new Stock_Input_Object();
                        LoadOrderInfo();
                    }
                    else
                    {
                        Utils.TNMessageBoxOK(zMessage, 4);
                    }
                }
            }
        }
        private void btn_Copy_Click(object sender, EventArgs e)
        {
            _Update = 0;
            _New = 0;
            _Copy = 1;
            k = 1;
            if (_OrderID.Length > 0)
            {
                if(Utils.TNMessageBox("Bạn có chắc muốn sao chép phiếu này sang phiếu mới không ?",2)=="Y")
                {
                    txt_Order_ID.Enabled = true;
                    dte_OrderDate.Enabled = true;
                    txt_Description.Enabled = true;
                    cbo_Warehouse.Enabled = true;

                    _OrderID = "";
                    _Order_Obj.Key = "";
                    _Order_Obj.ID = "";
                    _Order_Obj.RecordStatus = 0;
                    LoadOrderInfo();

                    Utils.TNMessageBoxOK("Sao chép thành công !",3);
                    for (int i = 0; i < GVData.Rows.Count - 1; i++)
                    {
                        if (GVData.Rows[i].Cells[0].Value != null)
                        {
                            k++;
                        }
                    }
                    if (_OrderID.Length == 0)
                        btn_Send.Enabled = false;
                    else
                        btn_Send.Enabled = true;
                }
            }
            else
            {
                Utils.TNMessageBoxOK("Vui lòng chọn đơn hàng", 1);
            }
        }
        private void btn_New_Click(object sender, EventArgs e)
        {
            _Update = 0;
            _New = 0;
            _Copy = 1;
            _OrderID = "";
            _Order_Obj = new Stock_Input_Object();
            LoadOrderInfo();

            txt_Order_ID.Focus();
            k = 1;
            if (_OrderID.Length == 0)
                btn_Send.Enabled = false;
            else
                btn_Send.Enabled = true;
        }
        private void btn_Send_Click(object sender, EventArgs e)
        {
            if (CheckOrder())
            {
                _Order_Obj.RoleID = _WareHouseRole;
                _Order_Obj.ModifiedBy = SessionUser.UserLogin.Key;
                _Order_Obj.ModifiedName = SessionUser.Personal_Info.FullName;
                _Order_Obj.Send();
                InitLV_Data();
                string zMessage = TN_Message.Show(_Order_Obj.Message);
                if (zMessage.Length == 0)
                {
                    Utils.TNMessageBoxOK("Yêu cầu đã được gửi đi", 1);
                    _Order_Obj = new Stock_Input_Object(_OrderID);
                    LoadOrderInfo();
                }
                else
                {
                    Utils.TNMessageBoxOK(zMessage, 4);
                }
            }
        }
        private void btn_Approve_Click(object sender, EventArgs e)
        {
            if (CheckOrder())
            {
                _Order_Obj.RoleID = _WareHouseRole;
                _Order_Obj.ModifiedBy = SessionUser.UserLogin.Key;
                _Order_Obj.ModifiedName = SessionUser.Personal_Info.FullName;
                _Order_Obj.Approve();
                InitLV_Data();
                string zMessage = TN_Message.Show(_Order_Obj.Message);
                if (zMessage.Length == 0)
                {
                    Utils.TNMessageBoxOK("Đơn hàng đã được duyệt", 3);
                    _Order_Obj = new Stock_Input_Object();
                    LoadOrderInfo();
                }
                else
                {
                    Utils.TNMessageBoxOK(zMessage, 4);
                }
            }

        }
        private void btn_Deny_Click(object sender, EventArgs e)
        {
            if (CheckOrder())
            {
                _Order_Obj.RoleID = _WareHouseRole;
                _Order_Obj.ModifiedBy = SessionUser.UserLogin.Key;
                _Order_Obj.ModifiedName = SessionUser.Personal_Info.FullName;
                _Order_Obj.Denied();
                InitLV_Data();
                string zMessage = TN_Message.Show(_Order_Obj.Message);
                if (zMessage.Length == 0)
                {
                    Utils.TNMessageBoxOK("Yêu cầu đã được trả về", 2);
                    _Order_Obj = new Stock_Input_Object();
                    LoadOrderInfo();
                }
                else
                {
                    Utils.TNMessageBoxOK(zMessage, 4);
                }
            }

        }
        private void btn_Search_Click(object sender, EventArgs e)
        {
            InitLV_Data_Search();
        }
        private void btn_Post_Click(object sender, EventArgs e)
        {
            if (CheckPost())
            {
                int zIndex = GVNote.Rows.Count - 1;
                GVNote.Rows.Insert(0, "*", DateTime.Now.ToString("dd/MM/yyyy HH:mm"), txt_Message.Text, SessionUser.Personal_Info.FullName);

                _Order_Obj.CreatedBy = SessionUser.UserLogin.Key;
                _Order_Obj.CreatedName = SessionUser.Personal_Info.FullName;
                _Order_Obj.InsertNote(txt_Message.Text);
                string _Message = _Order_Obj.Message;
                if (_Message == "11")
                {
                    Utils.TNMessageBoxOK("Gửi thông báo thành công",3);
                    _OrderID = txt_Order_ID.Text;
                    _Order_Obj = new Stock_Input_Object(_OrderID);
                    LoadOrderInfo();
                }
            }
        }
        private void btn_Edit_Click(object sender, EventArgs e)
        {
            GVData.ReadOnly = false;
            txt_Order_ID.ReadOnly = false;
            cbo_Warehouse.Enabled = true;
            cbo_Warehouse_Local.Enabled = true;
            txt_Description.ReadOnly = false;
            btn_Edit.Enabled = false;
            btn_Save.Enabled = true;
        }
        #endregion

        #endregion

        #region  ------ Funtion -----
        private bool CheckOrder()
        {
            Guid zGuid;
            bool zIsValid = Guid.TryParse(_Order_Obj.Key, out zGuid);
            if (zIsValid)
            {
                return true;
            }
            else
            {
                Utils.TNMessageBoxOK("Bạn phải chọn thông tin của 1 phiếu trước khi thao tác ?",1);
                return false;
            }
        }
        private bool CheckPost()
        {
            Guid zGuid;
            bool zIsValid = Guid.TryParse(_Order_Obj.Key, out zGuid);
            if (zIsValid)
            {
                return true;
            }
            else
            {
               Utils.TNMessageBoxOK("Bạn phải chọn thông tin của 1 phiếu trước khi muốn gửi thông tin gì ?",1);
                return false;
            }
        }
        private int CheckProductID(string ProductID, int nRowCurrency)
        {
            int i = 1;
            foreach (DataGridViewRow zRow in GVData.Rows)
            {
                if (zRow.Cells["ProductID"].Value != null)
                {
                    zRow.Cells["No"].Value = i;
                    if (zRow.Index != nRowCurrency && zRow.Cells["ProductID"].Value.ToString().Trim() == ProductID)
                    {
                        return zRow.Index;
                    }
                    i++;
                }
            }
            return -1;
        }
        private void ControlNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }

            if (e.KeyChar == '.')
            {
                e.Handled = false;
            }
            if (e.KeyChar == ',')
            {
                e.Handled = false;
            }
        }
        private void Control_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = false;
        }
        private bool IsRowEmpty(int RowIndex)
        {
            for (int i = 0; i < GVData.ColumnCount; i++)
            {
                if (GVData.Rows[RowIndex].Cells[i].Value != null)
                {
                    return false;
                }
            }
            return true;
        }
        private bool IsRowError(int RowIndex)
        {
            for (int i = 1; i < GVData.ColumnCount; i++)
            {
                if (GVData.Rows[RowIndex].Cells[i].ErrorText.Length > 0)
                {
                    return true;
                }
            }
            return false;
        }
        private void CheckRowError(int RowIndex)
        {
            DataGridViewRow nRow = GVData.Rows[RowIndex];
        }
        private void SetReadOnlyControl_Approve()
        {
            txt_Order_ID.Enabled = false;
            dte_OrderDate.Enabled = false;
            txt_Description.Enabled = false;
            cbo_Warehouse.Enabled = false;
            cbo_Warehouse_Local.Enabled = false;
            GVData.ReadOnly = true;
        }
        private void SetReadOnlyControl_NotApprove()
        {
            txt_Order_ID.Enabled = true;
            dte_OrderDate.Enabled = true;
            txt_Description.Enabled = true;
            cbo_Warehouse.Enabled = true;
            cbo_Warehouse_Local.Enabled = true;
        }
        private void Add_Product(int RowIndex, string OrderID, string ProductID)
        {
            DataTable zTable = Get_Voucher_Data.Get_Products_Output_Warehouse(OrderID, ProductID);
            if (zTable.Rows.Count > 0)
            {

                int n = RowIndex;
                DataRow r = zTable.Rows[0];
                GVData.Rows[n].Cells["No"].Value = (k++);
                GVData.Rows[n].Cells["ProductID"].Value = r["ProductID"].ToString();
                GVData.Rows[n].Cells["ProductName"].Value = r["ProductName"].ToString();
                GVData.Rows[n].Cells["UnitName"].Value = r["UnitName"].ToString();

                float zOutputQuatity = float.Parse(r["QuantityReality"].ToString());
                GVData.Rows[n].Cells["QuantityDocument"].Value = zOutputQuatity;
                GVData.Rows[n].Cells["QuantityReality"].Value = 0;
                GVData.Rows[n].Cells["Difference"].Value = zOutputQuatity;
                GVData.Rows[n].Cells["Follow"].Value = r["OrderID"].ToString();

                GVData.Rows[n].Cells["ProductFollow"].Value = r["ProductID"].ToString();
                //gan doi tuong vao row.tag
                Stock_Input_Products_Info zProduct = new Stock_Input_Products_Info();
                if (GVData.Rows[RowIndex].Tag == null) // Nếu dòng chưa có dữ liệu
                {

                    zProduct = new Stock_Input_Products_Info();
                }
                else
                {
                    zProduct = (Stock_Input_Products_Info)GVData.Rows[RowIndex].Tag;
                }
                zProduct.ProductID = r["ProductID"].ToString();
                zProduct.ProductKey = r["ProductKey"].ToString();
                zProduct.ProductName = r["ProductName"].ToString();
                zProduct.UnitKey = int.Parse(r["UnitKey"].ToString());
                zProduct.UnitName = r["UnitName"].ToString();
                zProduct.QuantityBasic = zOutputQuatity;
                zProduct.OrderKeyFollow = r["OrderKey"].ToString();
                zProduct.OrderIDFollow = r["OrderID"].ToString();
                zProduct.QuantityDocument = zOutputQuatity;
                zProduct.QuantityReality = 0;
                zProduct.Difference = zOutputQuatity;
                zProduct.ProductFollow = r["ProductID"].ToString();
                zProduct.ProductKeyFollow = r["ProductKey"].ToString();
                zProduct.RecordStatus = 1;
                GVData.Rows[n].Tag = zProduct;
            }
            k = 1;
            for (int i = 0; i < GVData.Rows.Count - 1; i++)
            {
                if (GVData.Rows[i].Cells[0].Value != null)
                {
                    k++;
                }
            }
            GV_Sum_LoadData();
        }
        #endregion

        private void btn_Import_Click(object sender, EventArgs e)
        {
            Frm_Import_Input frm = new Frm_Import_Input();
            frm.ShowDialog();
        }

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                //this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}