﻿using C1.Win.C1FlexGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.IVT;
using TNS.Misc;

namespace TNS.WinApp
{
    public partial class Frm_ComboList : Form
    {
        public Frm_ComboList()
        {
            InitializeComponent();
            btnClose.Click += btnClose_Click;
            btnMax.Click += btnMax_Click;
            btnMini.Click += btnMini_Click;
            this.FormClosing += Frm_Main_FormClosing;
            btn_New.Click += Btn_New_Click;
            GVData.DoubleClick += GVData_DoubleClick;
            btn_Search.Click += Btn_Search_Click;
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
        }

       

        private void Frm_ComboList_Load(object sender, EventArgs e)
        {
            DataTable _Intable = Combo_Data.List();
            InitGV_Layout(_Intable);
            if (_Intable.Rows.Count > 0)
                InitGV_Data(_Intable);
        }
        private void Btn_New_Click(object sender, EventArgs e)
        {
            Frm_ComboItem frm = new Frm_ComboItem();
            frm.Show();
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {

            DataTable _Intable = Combo_Data.List();
            InitGV_Layout(_Intable);
            if(_Intable.Rows.Count>0)
            InitGV_Data(_Intable);
        }
        void InitGV_Layout(DataTable Table)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();

            //int TotalRow = 1 ;
            //int ToTalCol = Table.Columns.Count - 2;

            GVData.Cols.Add(11);
            GVData.Rows.Add(1);
            //Row Header      
            GVData.Rows[0][0] = "STT";
            GVData.Rows[0][1] = "MÃ BẢNG GIÁ"; // nhập name đầu tiền 23-30 thì nhập 1 text cột 23
            GVData.Rows[0][2] = "TIÊU ĐỀ BẢNG GIÁ";
            GVData.Rows[0][3] = "MÃ THÀNH PHẢM";
            GVData.Rows[0][4] = "TÊN THÀNH PHẨM";
            GVData.Rows[0][5] = "ĐƠN VỊ TÍNH";
            GVData.Rows[0][6] = "TỔNG ĐƠN GIÁ";
            GVData.Rows[0][7] = "TỪ NGÀY";
            GVData.Rows[0][8] = "ĐẾN NGÀY";
            GVData.Rows[0][9] = "GHI CHÚ";
            GVData.Rows[0][10] = "";

            //Style
            //GVData.AutoSizeCols();
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.Custom;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.LeftCenter;
            GVData.Rows[0].Height = 60;
            GVData.Rows[0].StyleNew.WordWrap = true;
            GVData.Rows[0].TextAlign = TextAlignEnum.CenterCenter;
            //GVData.Rows[1].Height = 60;
            //GVData.Rows[1].StyleNew.WordWrap = true;
            //GVData.Rows[1].TextAlign = TextAlignEnum.CenterCenter;



            GVData.Rows.Fixed = 1;
            GVData.Cols.Fixed = 3;
            ////
            GVData.Cols[0].Width = 40;
            GVData.Cols[1].Width = 100;
            GVData.Cols[2].Width = 300;
            GVData.Cols[3].Width = 100;
            GVData.Cols[4].Width = 300;
            GVData.Cols[5].Width = 100;
            GVData.Cols[6].Width = 100;
            GVData.Cols[6].TextAlign = TextAlignEnum.RightCenter;
            GVData.Cols[7].Width = 100;
            GVData.Cols[8].Width = 100;
            GVData.Cols[9].Width = 100;
            GVData.Cols[10].Width = 300;
            GVData.Cols[10].Visible = false;


        }
        void InitGV_Data(DataTable Table)
        {

            int No = 1;
            for (int i = 0; i < Table.Rows.Count; i++)
            {
                GVData.Rows.Add();
                DataRow zRow = Table.Rows[i];
                GVData.Rows[No][0] = (i).ToString();
                GVData.Rows[No][1] = zRow["ComboID"];
                GVData.Rows[No][2] = zRow["ComboName"];
                GVData.Rows[No][3] = zRow["ProductID"];
                GVData.Rows[No][4] = zRow["ProductName"];
                GVData.Rows[No][5] = zRow["UnitName"];
                float zCost = 0;
                if (zRow["ComboCost"] != null)
                {
                    if(float.TryParse(zRow["ComboCost"].ToString(), out zCost))
                    {

                    }
                }
                GVData.Rows[No][6] = zCost.ToString("n0");
                if (zRow["FromDate"] == null)
                {
                    GVData.Rows[No][7] = "";
                }
                else
                {
                    DateTime zFromDate = DateTime.Parse(zRow["FromDate"].ToString());
                    GVData.Rows[No][7] = zFromDate.ToString("dd/MM/yyyy");
                }
                if (zRow["ToDate"] == null || zRow["ToDate"].ToString() == "")
                {
                    GVData.Rows[No][8] = "";
                }
                else
                {

                    DateTime zToDate = DateTime.Parse(zRow["ToDate"].ToString());
                    GVData.Rows[No][8] = zToDate.ToString("dd/MM/yyyy");
                }
                GVData.Rows[No][9] = zRow["Description"];
                GVData.Rows[No][10] = zRow["ComboKey"];
                No++;
            }
            //RowView.StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            //RowView.StyleNew.BackColor = Color.LightSkyBlue;
            //RowView.DefaultCellStyle.BackColor = Color.LightSkyBlue;
        }
        private void GVData_DoubleClick(object sender, EventArgs e)
        {
            Frm_ComboItem frm = new Frm_ComboItem();
            frm.Key= GVData.Rows[GVData.RowSel][10].ToString();
            frm.Show();
        }

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;
        private void Frm_Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Bạn có muốn đóng trang này !.", "Thông báo", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                e.Cancel = false;
            }
            else
            {
                e.Cancel = true;
            }
        }
        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion
    }
}
