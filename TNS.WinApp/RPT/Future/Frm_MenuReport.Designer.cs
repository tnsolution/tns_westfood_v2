﻿namespace TNS.WinApp
{
    partial class Frm_MenuReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Demo1 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_ReportDemo_02 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_rpt3 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.SuspendLayout();
            // 
            // btn_Demo1
            // 
            this.btn_Demo1.Location = new System.Drawing.Point(12, 12);
            this.btn_Demo1.Name = "btn_Demo1";
            this.btn_Demo1.Size = new System.Drawing.Size(90, 90);
            this.btn_Demo1.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Demo1.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Demo1.StateCommon.Back.ColorAngle = 75F;
            this.btn_Demo1.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Demo1.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Demo1.StateCommon.Border.Rounding = 10;
            this.btn_Demo1.StateCommon.Border.Width = 2;
            this.btn_Demo1.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Demo1.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Demo1.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Demo1.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Demo1.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Demo1.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Demo1.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Demo1.TabIndex = 205;
            this.btn_Demo1.Tag = "";
            this.btn_Demo1.Values.Text = "Trung bình\r\nlương\r\ntổ nhóm";
            // 
            // btn_ReportDemo_02
            // 
            this.btn_ReportDemo_02.Location = new System.Drawing.Point(108, 12);
            this.btn_ReportDemo_02.Name = "btn_ReportDemo_02";
            this.btn_ReportDemo_02.Size = new System.Drawing.Size(90, 90);
            this.btn_ReportDemo_02.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_ReportDemo_02.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_ReportDemo_02.StateCommon.Back.ColorAngle = 75F;
            this.btn_ReportDemo_02.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_ReportDemo_02.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_ReportDemo_02.StateCommon.Border.Rounding = 10;
            this.btn_ReportDemo_02.StateCommon.Border.Width = 2;
            this.btn_ReportDemo_02.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_ReportDemo_02.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ReportDemo_02.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_ReportDemo_02.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_ReportDemo_02.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ReportDemo_02.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ReportDemo_02.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ReportDemo_02.TabIndex = 205;
            this.btn_ReportDemo_02.Tag = "";
            this.btn_ReportDemo_02.Values.Text = "Tổng lương \r\nkhoán năng \r\nsuất theo \r\ntháng";
            // 
            // btn_rpt3
            // 
            this.btn_rpt3.Location = new System.Drawing.Point(204, 12);
            this.btn_rpt3.Name = "btn_rpt3";
            this.btn_rpt3.Size = new System.Drawing.Size(90, 90);
            this.btn_rpt3.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_rpt3.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_rpt3.StateCommon.Back.ColorAngle = 75F;
            this.btn_rpt3.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_rpt3.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_rpt3.StateCommon.Border.Rounding = 10;
            this.btn_rpt3.StateCommon.Border.Width = 2;
            this.btn_rpt3.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_rpt3.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_rpt3.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_rpt3.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_rpt3.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_rpt3.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_rpt3.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_rpt3.TabIndex = 205;
            this.btn_rpt3.Tag = "";
            this.btn_rpt3.Values.Text = "Lương\r\nbộ phận";
            // 
            // Frm_MenuReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btn_rpt3);
            this.Controls.Add(this.btn_ReportDemo_02);
            this.Controls.Add(this.btn_Demo1);
            this.Name = "Frm_MenuReport";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Frm_MenuReport";
            this.Load += new System.EventHandler(this.Frm_MenuReport_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Demo1;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_ReportDemo_02;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_rpt3;
    }
}