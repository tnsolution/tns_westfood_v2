﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TNS.WinApp
{
    public partial class Frm_MenuReport : Form
    {
        public Frm_MenuReport()
        {
            InitializeComponent();
            btn_Demo1.Click += Btn_Demo1_Click;
            btn_ReportDemo_02.Click += Btn_ReportDemo_02_Click;
            btn_rpt3.Click += Btn_rpt3_Click;
        }

        private void Btn_rpt3_Click(object sender, EventArgs e)
        {
            Frm_ReportDemo_03 frm = new Frm_ReportDemo_03();
            frm.Show();
        }

        private void Frm_MenuReport_Load(object sender, EventArgs e)
        {

        }
        private void Btn_Demo1_Click(object sender, EventArgs e)
        {
            Frm_ReportDemo_01 frm = new Frm_ReportDemo_01();
            frm.Show();
        }
        private void Btn_ReportDemo_02_Click(object sender, EventArgs e)
        {
            Frm_ReportDemo_02 frm = new Frm_ReportDemo_02();
            frm.Show();
        }
    }
}
