﻿using C1.Win.C1FlexGrid;
using System;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Windows.Forms;
using TNS.CORE;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_BaoCao_Mau1 : Form
    {
        private int _DepartmentKey = 0;
        private int _TeamKey = 0;
        private string _Parent = "";
        public Frm_BaoCao_Mau1()
        {
            InitializeComponent();
            Load += Frm_BaoCao_Mau1_Load;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;
            cbo_Department.SelectedIndexChanged += Cbo_Department_SelectedIndexChanged;
        }

        private void Frm_BaoCao_Mau1_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            Check_RoleForm();

            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;
            btnClose.Click += btnClose_Click;

            btn_Search.Click += Btn_Search_Click;
            btn_Export.Click += Btn_Export_Click;

            dte_FromDate.Value = SessionUser.Date_Work;
            dte_ToDate.Value = SessionUser.Date_Work;
            LoadDataToToolbox.KryptonComboBox(cbo_Department, "SELECT DepartmentKey, DepartmentName FROM[dbo].[SYS_Department] WHERE BranchKey = 4 AND DepartmentKey!=26  AND RecordStatus< 99", "---- Chọn tất cả----");
            LoadDataToToolbox.KryptonComboBox(cbo_Parent, "SELECT ProductKey, ProductName FROM [dbo].[IVT_Product] WHERE RecordStatus <> 99 AND Parent ='0' ORDER BY Rank ", "--Chọn tất cả--");
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
        }
        private void Cbo_Department_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDataToToolbox.KryptonComboBox(cboTeam, "SELECT TeamKey,TeamName FROM SYS_Team WHERE TeamKey != 98 AND DepartmentKey = " + cbo_Department.SelectedValue.ToInt() + "  AND RecordStatus < 99", "---- Chọn tất cả ----");
        }
        private void Btn_Export_Click(object sender, EventArgs e)
        {

            string zTeamName = "";
            if (cboTeam.SelectedValue.ToInt() != 0)
            {
                string[] s = cboTeam.Text.Trim().Split('-');
                zTeamName = "_Nhóm_" + s[0];
            }
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Phân_bổ_chi_tiết" + zTeamName + "_Từ_" + dte_FromDate.Value.ToString("dd_MM_yyyy") + "_Đến_" + dte_ToDate.Value.ToString("dd_MM_yyyy") + ".xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }

                    string Status = "Xuất Excel form " + HeaderControl.Text + " > đường dẫn và tên file xuất:" + FDialog.FileName;
                     Application_Data.Save_LogApplication(SessionUser.UserLogin.Key,SessionUser.UserLogin.EmployeeKey, Status, 3);

                    GVData.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }
        }

        private void Btn_Search_Click(object sender, EventArgs e)
        {
            txt_TitleGrid.Text = "Thông tin chi tiết sản phẩm theo công đoạn, nhóm (LK, TG,TP).";
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            if (dte_ToDate.Value.Month != dte_FromDate.Value.Month || dte_ToDate.Value.Year != dte_FromDate.Value.Year)
            {
                Utils.TNMessageBoxOK("Bạn chỉ được chọn trong 1 tháng", 1);
                return;
            }
            _DepartmentKey = cbo_Department.SelectedValue.ToInt();
            _TeamKey = cboTeam.SelectedValue.ToInt();
            _Parent = cbo_Parent.SelectedValue.ToString();

            string Status = "Tìm DL form " + HeaderControl.Text + " > từ: " + dte_FromDate.Value.ToString("dd/MM/yyyy") + " > đến:" + dte_ToDate.Value.ToString("dd/MM/yyyy") +" > BP:"+cbo_Department.Text+ " > Nhóm:" + cboTeam.Text +" > Nhóm SP:"+cbo_Parent.Text+ " > Từ khóa:" + txt_Search.Text;
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);

            using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }
        }

        private void DisplayData()
        {
            DateTime FromDate = new DateTime(dte_FromDate.Value.Year, dte_FromDate.Value.Month, dte_FromDate.Value.Day, 0, 0, 0);
            DateTime ToDate = new DateTime(dte_ToDate.Value.Year, dte_ToDate.Value.Month, dte_ToDate.Value.Day, 23, 59, 59);
           

            DataTable zTable = Report.ReportMau1(FromDate, ToDate,_DepartmentKey, _TeamKey, _Parent,txt_Search.Text.Trim());

            if (zTable.Rows.Count > 0)
            {
                PivotTable zPvt = new PivotTable(zTable);
                DataTable zTablePivot = zPvt.Generate_ASC("HeaderColumn", "LeftColumn", new string[] { "LK", "TG", "TP" }, "CÔNG VIỆC", "TỔNG CỘNG", "TỔNG CỘNG", new string[] { "LK", "TG", "TP" }, 0);

                string[] temp = zTablePivot.Columns[1].ColumnName.Split('|');
                string TeamKeyFlag = temp[0];
                string TeamNameFlag = temp[1];

                for (int i = 1; i < zTablePivot.Columns.Count; i++)
                {
                    temp = zTablePivot.Columns[i].ColumnName.Split('|');
                    if (temp.Length >= 4)
                    {
                        string TeamKey = temp[0];
                        string TeamName = temp[1];

                        if (TeamKeyFlag != "0" && TeamKeyFlag != TeamKey)
                        {
                            //add 4 cột tổng
                            zTablePivot.Columns.Add(TeamNameFlag + "|Tổng|LK", typeof(float)).SetOrdinal(i);
                            zTablePivot.Columns.Add(TeamNameFlag + "|Tổng|TG", typeof(float)).SetOrdinal(i + 1);
                            zTablePivot.Columns.Add(TeamNameFlag + "|Tổng|TP", typeof(float)).SetOrdinal(i + 2);

                            TeamKeyFlag = TeamKey;
                            TeamNameFlag = TeamName;
                        }
                    }
                }

                int c = zTablePivot.Columns.Count;

                zTablePivot.Columns.Add(TeamNameFlag + "|Tổng|LK", typeof(float)).SetOrdinal(c - 3);
                zTablePivot.Columns.Add(TeamNameFlag + "|Tổng|TG", typeof(float)).SetOrdinal(c - 2);
                zTablePivot.Columns.Add(TeamNameFlag + "|Tổng|TP", typeof(float)).SetOrdinal(c - 1);

                zTablePivot.Columns.Add("LoaiSP").SetOrdinal(0);

                for (int i = 0; i < zTablePivot.Rows.Count; i++)
                {
                    string loai = zTablePivot.Rows[i][1].ToString().Split('|')[0];
                    zTablePivot.Rows[i][0] = loai;
                }

                string Loai = zTablePivot.Rows[0][0].ToString();
                DataRow[] Array = zTablePivot.Select("LoaiSP='" + Loai + "'");
                if (Array.Length > 0)
                {
                    AddGroup(zTablePivot, Loai, Array, 0);
                }

                for (int i = 0; i < zTablePivot.Rows.Count; i++)
                {
                    string temploai = zTablePivot.Rows[i][0].ToString();

                    if (temploai != Loai && temploai.ToUpper() != "TỔNG CỘNG")
                    {
                        Loai = temploai;
                        Array = zTablePivot.Select("LoaiSP='" + Loai + "'");
                        if (Array.Length > 0)
                        {
                            AddGroup(zTablePivot, Loai, Array, i);
                        }
                    }
                }

                this.Invoke(new MethodInvoker(delegate ()
                {
                    InitGVProduct(zTablePivot);
                }));
            }
            else
            {
                GVData.Rows.Count = 0;
                GVData.Cols.Count = 0;
            }
        }

        private void AddGroup(DataTable zTablePivot, string Loai, DataRow[] Array, int Index)
        {
            DataTable zGroup = Array.CopyToDataTable();
            DataRow dr = zTablePivot.NewRow();
            dr[0] = Loai;
            dr[1] = Loai;

            for (int c = 2; c < zGroup.Columns.Count; c++)
            {
                float Tong = 0;
                foreach (DataRow r in zGroup.Rows)
                {
                    float ztemp = 0;
                    if(float.TryParse(r[c].ToString(),out ztemp))
                    {

                    }
                    Tong += ztemp;
                }
                dr[c] = Tong;
            }

            zTablePivot.Rows.InsertAt(dr, Index);
        }

        void InitGVProduct(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();

            int TotalRow = TableView.Rows.Count + 4;
            int ToTalCol = TableView.Columns.Count + 4;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(TotalRow);

            CellRange zCellRange = GVData.GetCellRange(0, 0, 3, 0);
            zCellRange.Data = "Stt";

            zCellRange = GVData.GetCellRange(0, 1, 3, 1);
            zCellRange.Data = "Sản phẩm";

            zCellRange = GVData.GetCellRange(0, 2, 3, 2);
            zCellRange.Data = "Mã";

            zCellRange = GVData.GetCellRange(0, 3, 3, 3);
            zCellRange.Data = "Đơn vị";

            zCellRange = GVData.GetCellRange(0, 4, 3, 4);
            zCellRange.Data = "Nhóm sản phẩm";

            zCellRange = GVData.GetCellRange(0, 5, 3, 5);
            zCellRange.Data = "Loại sản phẩm";

            //Row Header
            int ColStart = 6;
            for (int i = 2; i < TableView.Columns.Count; i++)
            {
                DataColumn Col = TableView.Columns[i];
                string[] strCaption = Col.ColumnName.Split('|');
                if (strCaption.Length > 3)
                {
                    GVData.Rows[0][ColStart] = strCaption[1];
                    GVData.Rows[1][ColStart] = strCaption[2];
                    string temp = "";
                    if(strCaption[3]!="")
                    {
                        float zPrice = float.Parse(strCaption[3].ToString());
                        temp = zPrice.ToString("n0");
                    }
                    GVData.Rows[2][ColStart] = temp;
                    GVData.Rows[3][ColStart] = strCaption[4];
                }
                else
                {
                    if (strCaption.Length > 2)
                    {
                        GVData.Rows[0][ColStart] = strCaption[0];
                        GVData.Rows[1][ColStart] = strCaption[1];

                        CellRange CellRange = GVData.GetCellRange(3, ColStart, 3, ColStart);
                        CellRange.Data = strCaption[2];
                    }
                    else
                    {
                        GVData.Rows[1][ColStart] = strCaption[0];

                        CellRange CellRange = GVData.GetCellRange(2, ColStart, 3, ColStart);
                        CellRange.Data = strCaption[1];
                    }
                }
                ColStart++;
            }


            //--------------------
            double TotalLK = 0;
            double TotalTG = 0;
            double TotalTP = 0;

            //Fill Data
            ColStart = 6;// add data vào cột thứ 5 trở đi
            int RowStart = 4;//add dòng từ số thứ tự 4 trở đi
            int zNo = 1;
            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                DataRow rData = TableView.Rows[rIndex];

                if (rData[1].ToString().Contains("|"))
                {
                    string[] strTemp = rData[1].ToString().Split('|');
                    GVData.Rows[rIndex + RowStart][0] = zNo;
                    GVData.Rows[rIndex + RowStart][1] = strTemp[1]; // ten sản phẩm
                    GVData.Rows[rIndex + RowStart][2] = strTemp[2]; // ma sản phẩm
                    GVData.Rows[rIndex + RowStart][3] = strTemp[3];//Đơn vị
                    GVData.Rows[rIndex + RowStart][4] = strTemp[4];//Nhóm sản phẩm
                    GVData.Rows[rIndex + RowStart][5] = strTemp[5];//Loại sản phẩm
                    zNo++;
                }
                else
                {
                    zNo = 1;
                    GVData.Rows[rIndex + RowStart][1] = rData[1].ToString();
                    GVData.Rows[rIndex + RowStart].StyleNew.Font = new Font("Tahoma", 9F, FontStyle.Bold | FontStyle.Italic);
                    GVData.Rows[rIndex + RowStart].StyleFixedNew.Font = new Font("Tahoma", 9F, FontStyle.Bold | FontStyle.Italic);
                }

                string[] temp = TableView.Columns[3].ColumnName.Split('|');
                string TeamKeyFlag = temp[0];
                string TeamNameFlag = temp[1];

                //chạy từ cột 2 của bảng pivot
                for (int cIndex = 2; cIndex < TableView.Columns.Count; cIndex = cIndex + 3)
                {
                    //fill cột thứ 3 trở đi trong grid

                    //RowIndex là GridView
                    int RowIndex = RowStart + rIndex;
                    //ColIndex là GridView
                    int ColIndex = cIndex + 4;

                    temp = TableView.Columns[cIndex].ColumnName.Split('|');
                    string TeamKey = temp[0];
                    string TeamName = temp[1];

                    if (temp.Length >= 4)
                    {
                        double LK = 0;
                        double TG = 0;
                        double TP = 0;
                        if(double.TryParse(rData[cIndex].ToString(),out LK))
                        {

                        }
                        if (double.TryParse(rData[cIndex + 1].ToString(), out TG))
                        {

                        }
                        if (double.TryParse(rData[cIndex + 2].ToString(), out TP))
                        {

                        }
                        TotalLK +=LK;
                        TotalTG += TG;
                        TotalTP += TP;

                        GVData.Rows[RowIndex][ColIndex] = LK.Toe0String();
                        GVData.Rows[RowIndex][ColIndex + 1] = TG.Toe2String();
                        GVData.Rows[RowIndex][ColIndex + 2] = TP.Toe2String();
                    }
                    else
                    {
                        //4 cột tổng cuối cùng
                        if (temp.Length == 2)
                        {
                            GVData.Rows[RowIndex][ColIndex] = rData[cIndex].Toe0String();
                            GVData.Rows[RowIndex][ColIndex + 1] = rData[cIndex + 1].Toe2String();
                            GVData.Rows[RowIndex][ColIndex + 2] = rData[cIndex + 2].Toe2String();

                        }
                        else
                        {
                            if (TeamKeyFlag != "0" && TeamKeyFlag != TeamKey)
                            {
                                GVData.Rows[RowIndex][ColIndex] = TotalLK.Toe0String();
                                GVData.Rows[RowIndex][ColIndex + 1] = TotalTG.Toe2String();
                                GVData.Rows[RowIndex][ColIndex + 2] = TotalTP.Toe2String();

                                GVData.Cols[ColIndex].StyleNew.ForeColor = Color.Blue;
                                GVData.Cols[ColIndex + 1].StyleNew.ForeColor = Color.Blue;
                                GVData.Cols[ColIndex + 2].StyleNew.ForeColor = Color.Blue;

                                TeamKeyFlag = TeamKey;
                                TeamNameFlag = TeamName;

                                TotalLK = 0;
                                TotalTG = 0;
                                TotalTP = 0;
                            }
                        }
                    }
                }
            }

            ////Style

            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.FixedOnly;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.CenterCenter;
            GVData.Styles.Normal.WordWrap = true;

            //Trộn dòng 0
            GVData.Rows[0].AllowMerging = true;
            GVData.Rows[0].Height = 30;
            //Trộn dòng 1
            GVData.Rows[1].AllowMerging = true;
            GVData.Rows[1].Height = 45;
            //Trộn dòng 2
            GVData.Rows[2].AllowMerging = true;
            //Trộn dòng cuối
            GVData.Rows[GVData.Rows.Count - 1].AllowMerging = true;

            //Trộn cột đầu
            GVData.Cols[0].AllowMerging = true;
            GVData.Cols[0].Width = 35;
            GVData.Cols[1].AllowMerging = true;
            GVData.Cols[1].Width = 450;
            GVData.Cols[2].AllowMerging = true;
            GVData.Cols[2].Width = 115;
            GVData.Cols[3].AllowMerging = true;
            GVData.Cols[3].Width = 60;
            GVData.Cols[4].AllowMerging = true;
            GVData.Cols[4].Width = 120;
            GVData.Cols[5].AllowMerging = true;
            GVData.Cols[5].Width = 120;
            //Trộn 2 cột cuối
            GVData.Cols[ToTalCol - 1].AllowMerging = true;
            GVData.Cols[ToTalCol - 2].AllowMerging = true;
            GVData.Cols[ToTalCol - 3].AllowMerging = true;
            //Freeze Row and Column
            GVData.Rows.Fixed = 4;
            GVData.Cols.Frozen = 5;

            //GVData.AutoSizeCols(1, GVData.Cols.Count - 1, 10);
            //Canh phải các cột từ số 1 trở đi
            GVData.Cols[0].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[1].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[2].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[3].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[4].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[5].TextAlign = TextAlignEnum.LeftCenter;

            GVData.Cols[0].StyleNew.BackColor = Color.Empty;
            GVData.Cols[1].StyleNew.BackColor = Color.Empty;
            GVData.Cols[2].StyleNew.BackColor = Color.Empty;
            GVData.Cols[3].StyleNew.BackColor = Color.Empty;
            GVData.Cols[4].StyleNew.BackColor = Color.Empty;
            GVData.Cols[5].StyleNew.BackColor = Color.Empty;

            for (int i = 6; i < GVData.Cols.Count; i++)
            {
                GVData.Cols[i].TextAlign = TextAlignEnum.RightCenter;
            }

            //In đậm các dòng, cột tổng
            GVData.Rows[TotalRow - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[ToTalCol - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[ToTalCol - 2].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[ToTalCol - 3].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            // tô màu cột tổng
            //GVData.Rows[TotalRow - 1].StyleNew.BackColor = Color.LightGreen;
            GVData.Cols[ToTalCol - 1].StyleNew.ForeColor = Color.Red;
            GVData.Cols[ToTalCol - 2].StyleNew.ForeColor = Color.Red;
            GVData.Cols[ToTalCol - 3].StyleNew.ForeColor = Color.Red;
            // fix độ rộng 3 cột tổng
            GVData.Cols[ToTalCol - 1].Width = 130;
            GVData.Cols[ToTalCol - 2].Width = 130;
            GVData.Cols[ToTalCol - 3].Width = 130;
        }

        string FormatMoney(object input)
        {
            try
            {
                if (input.ToString() != "0")
                {
                    double zResult = double.Parse(input.ToString());
                    return zResult.ToString("n", CultureInfo.GetCultureInfo("vi-VN"));
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
                btn_Export.Enabled = false;
            }
        }
        #endregion
    }
}
