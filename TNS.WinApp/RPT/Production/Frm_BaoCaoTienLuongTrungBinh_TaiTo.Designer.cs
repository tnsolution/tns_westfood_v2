﻿namespace TNS.WinApp
{
    partial class Frm_BaoCaoTienLuongTrungBinh_TaiTo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_BaoCaoTienLuongTrungBinh_TaiTo));
            this.HeaderControl = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btnMini = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnMax = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnClose = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txt_Search = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cboTeam = new ComponentFactory.Krypton.Toolkit.KryptonComboBox();
            this.btn_Search = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Export = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.dte_ToDate = new TNS.SYS.TNDateTimePicker();
            this.dte_FromDate = new TNS.SYS.TNDateTimePicker();
            this.txt_TitleGrid = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.GVData = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboTeam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVData)).BeginInit();
            this.SuspendLayout();
            // 
            // HeaderControl
            // 
            this.HeaderControl.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnMini,
            this.btnMax,
            this.btnClose});
            this.HeaderControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.HeaderControl.Location = new System.Drawing.Point(0, 0);
            this.HeaderControl.Name = "HeaderControl";
            this.HeaderControl.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.HeaderControl.Size = new System.Drawing.Size(1350, 42);
            this.HeaderControl.TabIndex = 220;
            this.HeaderControl.Values.Description = "";
            this.HeaderControl.Values.Heading = "BÁO CÁO TIỀN LƯƠNG-GIỜ LÀM TẠI TỔ-TRUNG BÌNH NGÀY -TẠI TỔ (Dùng tính khoán tổ trư" +
    "ởng)";
            this.HeaderControl.Values.Image = ((System.Drawing.Image)(resources.GetObject("HeaderControl.Values.Image")));
            // 
            // btnMini
            // 
            this.btnMini.Image = ((System.Drawing.Image)(resources.GetObject("btnMini.Image")));
            this.btnMini.UniqueName = "F5F06E8241504E72ABB5BEA3F6A9B753";
            // 
            // btnMax
            // 
            this.btnMax.Image = ((System.Drawing.Image)(resources.GetObject("btnMax.Image")));
            this.btnMax.UniqueName = "035D1A4881E44F58A084C31DE7352A94";
            // 
            // btnClose
            // 
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.UniqueName = "11B07C6F4E1C4F9D8B91BD924CB0EBE6";
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panel1.Controls.Add(this.txt_Search);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.cboTeam);
            this.panel1.Controls.Add(this.btn_Search);
            this.panel1.Controls.Add(this.btn_Export);
            this.panel1.Controls.Add(this.dte_ToDate);
            this.panel1.Controls.Add(this.dte_FromDate);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 42);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1350, 80);
            this.panel1.TabIndex = 221;
            // 
            // txt_Search
            // 
            this.txt_Search.Location = new System.Drawing.Point(241, 30);
            this.txt_Search.Name = "txt_Search";
            this.txt_Search.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_Search.Size = new System.Drawing.Size(255, 26);
            this.txt_Search.StateCommon.Border.ColorAngle = 1F;
            this.txt_Search.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_Search.StateCommon.Border.Rounding = 4;
            this.txt_Search.StateCommon.Border.Width = 1;
            this.txt_Search.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Search.TabIndex = 287;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F);
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(4, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 14);
            this.label3.TabIndex = 286;
            this.label3.Text = "Đến ngày";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F);
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(9, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 14);
            this.label2.TabIndex = 286;
            this.label2.Text = "Từ ngày";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(182, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 14);
            this.label1.TabIndex = 286;
            this.label1.Text = "Tổ nhóm";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F);
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(191, 36);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 14);
            this.label4.TabIndex = 286;
            this.label4.Text = "Mã thẻ";
            // 
            // cboTeam
            // 
            this.cboTeam.DropDownWidth = 119;
            this.cboTeam.Location = new System.Drawing.Point(241, 5);
            this.cboTeam.Name = "cboTeam";
            this.cboTeam.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.cboTeam.Size = new System.Drawing.Size(255, 22);
            this.cboTeam.StateCommon.ComboBox.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.cboTeam.StateCommon.ComboBox.Border.Rounding = 4;
            this.cboTeam.StateCommon.ComboBox.Border.Width = 1;
            this.cboTeam.StateCommon.ComboBox.Content.Font = new System.Drawing.Font("Tahoma", 9F);
            this.cboTeam.StateCommon.Item.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.cboTeam.StateCommon.Item.Border.Rounding = 4;
            this.cboTeam.StateCommon.Item.Border.Width = 1;
            this.cboTeam.StateCommon.Item.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cboTeam.TabIndex = 234;
            // 
            // btn_Search
            // 
            this.btn_Search.Location = new System.Drawing.Point(502, 9);
            this.btn_Search.Name = "btn_Search";
            this.btn_Search.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Search.Size = new System.Drawing.Size(106, 40);
            this.btn_Search.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Search.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Search.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Search.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Search.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Search.TabIndex = 233;
            this.btn_Search.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Search.Values.Image")));
            this.btn_Search.Values.Text = "Tìm kiếm";
            // 
            // btn_Export
            // 
            this.btn_Export.Location = new System.Drawing.Point(614, 9);
            this.btn_Export.Name = "btn_Export";
            this.btn_Export.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Export.Size = new System.Drawing.Size(113, 40);
            this.btn_Export.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Export.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Export.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Export.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Export.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Export.TabIndex = 207;
            this.btn_Export.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Export.Values.Image")));
            this.btn_Export.Values.Text = "Xuất Excel";
            // 
            // dte_ToDate
            // 
            this.dte_ToDate.CustomFormat = "dd/MM/yyyy";
            this.dte_ToDate.Location = new System.Drawing.Point(66, 32);
            this.dte_ToDate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dte_ToDate.Name = "dte_ToDate";
            this.dte_ToDate.Size = new System.Drawing.Size(100, 31);
            this.dte_ToDate.TabIndex = 157;
            this.dte_ToDate.Value = new System.DateTime(2020, 5, 5, 0, 0, 0, 0);
            // 
            // dte_FromDate
            // 
            this.dte_FromDate.CustomFormat = "dd/MM/yyyy";
            this.dte_FromDate.Location = new System.Drawing.Point(66, 8);
            this.dte_FromDate.Name = "dte_FromDate";
            this.dte_FromDate.Size = new System.Drawing.Size(100, 25);
            this.dte_FromDate.TabIndex = 157;
            this.dte_FromDate.Value = new System.DateTime(2020, 5, 1, 0, 0, 0, 0);
            // 
            // txt_TitleGrid
            // 
            this.txt_TitleGrid.AutoSize = false;
            this.txt_TitleGrid.Dock = System.Windows.Forms.DockStyle.Top;
            this.txt_TitleGrid.Location = new System.Drawing.Point(0, 122);
            this.txt_TitleGrid.Name = "txt_TitleGrid";
            this.txt_TitleGrid.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_TitleGrid.Size = new System.Drawing.Size(1350, 30);
            this.txt_TitleGrid.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.txt_TitleGrid.TabIndex = 222;
            this.txt_TitleGrid.Values.Description = "";
            this.txt_TitleGrid.Values.Heading = "Công nhân\r\n";
            // 
            // GVData
            // 
            this.GVData.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None;
            this.GVData.AllowEditing = false;
            this.GVData.AllowResizing = C1.Win.C1FlexGrid.AllowResizingEnum.None;
            this.GVData.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None;
            this.GVData.AutoResize = true;
            this.GVData.ColumnInfo = "10,1,0,0,0,95,Columns:";
            this.GVData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GVData.ExtendLastCol = true;
            this.GVData.Location = new System.Drawing.Point(0, 152);
            this.GVData.Name = "GVData";
            this.GVData.Rows.DefaultSize = 19;
            this.GVData.Size = new System.Drawing.Size(1350, 409);
            this.GVData.TabIndex = 223;
            // 
            // Frm_BaoCaoTienLuongTrungBinh_TaiTo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1350, 561);
            this.Controls.Add(this.GVData);
            this.Controls.Add(this.txt_TitleGrid);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.HeaderControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_BaoCaoTienLuongTrungBinh_TaiTo";
            this.Text = "BÁO CÁO TIỀN LƯƠNG-GIỜ LÀM TẠI TỔ-TRUNG BÌNH NGÀY -TẠI TỔ";
            this.Load += new System.EventHandler(this.Frm_BaoCaoTienLuongTrungBinh_TaiTo_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboTeam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVData)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ComponentFactory.Krypton.Toolkit.KryptonHeader HeaderControl;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMini;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMax;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnClose;
        private System.Windows.Forms.Panel panel1;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_Search;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private ComponentFactory.Krypton.Toolkit.KryptonComboBox cboTeam;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Search;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Export;
        private SYS.TNDateTimePicker dte_ToDate;
        private SYS.TNDateTimePicker dte_FromDate;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader txt_TitleGrid;
        private C1.Win.C1FlexGrid.C1FlexGrid GVData;
    }
}