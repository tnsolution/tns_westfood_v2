﻿using C1.Win.C1FlexGrid;
using System;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Windows.Forms;
using TNS.CORE;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_BaoCaoTienLuongTrungBinhNgay : Form
    {
        int ViewTeam = 0;
        public Frm_BaoCaoTienLuongTrungBinhNgay()
        {
            InitializeComponent();
            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;
            btnClose.Click += btnClose_Click;
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            btn_Search.Click += Btn_Search_Click;
            btn_Export.Click += Btn_Export_Click;
            DateTime ViewDate = SessionUser.Date_Work;
            DateTime FromDate = new DateTime(ViewDate.Year, ViewDate.Month, 1, 0, 0, 0);
            DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            dte_FromDate.Value = FromDate;
            dte_ToDate.Value = ToDate;
            //Lấy tất cả nhóm trực tiếp sx  và khác bộ phận hỗ trợ sx
            string zSQL = @"
SELECT A.TeamKey, A.TeamID +'-'+ A.TeamName AS TeamName  
FROM SYS_Team A
LEFT JOIN [dbo].[SYS_Department] B ON B.DepartmentKey=A.DepartmentKey
LEFT JOIN [dbo].[SYS_Branch] C ON C.BranchKey=B.BranchKey
WHERE A.RecordStatus <> 99 
AND   A.BranchKey = 4 
AND   A.DepartmentKey != 26
AND   A.TeamKey !=98 
ORDER BY C.RANK,B.RANK,A.RANK";
            LoadDataToToolbox.KryptonComboBox(cboTeam, zSQL, "--- Tất cả---");
        }

        private void Frm_BaoCaoTienLuongTrungBinhNgay_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            Check_RoleForm();

            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
        }
        private void Btn_Export_Click(object sender, EventArgs e)
        {
            string zTeamName = "";
            if (cboTeam.SelectedValue.ToInt() != 0)
            {
                string[] s = cboTeam.Text.Trim().Split('-');
                zTeamName = "_Nhóm_" + s[0];
            }
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Báo cáo lương ngày-Số giờ làm việc-trung bình lương ngày" + zTeamName + "_Từ_" + dte_FromDate.Value.ToString("dd_MM_yyyy") + "_Đến_" + dte_ToDate.Value.ToString("dd_MM_yyyy") + ".xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }
                    string Status = "Xuất Excel form " + HeaderControl.Text + " > đường dẫn và tên file xuất:" + FDialog.FileName;
                    Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);

                    GVData.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }
        }

        private void Btn_Search_Click(object sender, EventArgs e)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();

            ViewTeam = cboTeam.SelectedValue.ToInt();
            if (dte_ToDate.Value.Month != dte_FromDate.Value.Month || dte_ToDate.Value.Year != dte_FromDate.Value.Year)
            {
                Utils.TNMessageBoxOK("Bạn chỉ được chọn trong 1 tháng", 1);
                return;
            }
            string Status = "Tìm DL form " + HeaderControl.Text + " > từ: " + dte_FromDate.Value.ToString("dd/MM/yyyy") + " > đến:" + dte_ToDate.Value.ToString("dd/MM/yyyy") + " > Nhóm:" + cboTeam.Text + " > Từ khóa:" + txt_Search.Text;
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);

            try
            {
                using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }

            }
            catch (Exception ex)
            {
                Utils.TNMessageBox(ex.ToString(), 4);
            }
        }
        private void DisplayData()
        {
            DateTime FromDate = new DateTime(dte_FromDate.Value.Year, dte_FromDate.Value.Month, dte_FromDate.Value.Day, 0, 0, 0);
            DateTime ToDate = new DateTime(dte_ToDate.Value.Year, dte_ToDate.Value.Month, dte_ToDate.Value.Day, 23, 59, 59);

            System.Data.DataTable zTable = Report.TRUNGBINHNGAY(FromDate, ToDate, ViewTeam, txt_Search.Text.Trim());

            if (zTable.Rows.Count > 0)
            {
                PivotTable zPvt = new PivotTable(zTable);
                System.Data.DataTable zTablePivot = zPvt.Generate_ASC("HeaderColumn", "LeftColumn", new string[] { "MoneyDate", "HourDate", "MediumDate" }, "LeftColumn", "TỔNG CỘNG", "TỔNG CỘNG", new string[] { "Tiền lương", "Số giờ \n (Đã nhân hệ số)", "Trung bình lương" }, 0);



                zTablePivot.Columns.Add("LoaiSP").SetOrdinal(0);
                zTablePivot.Columns.Add("DepartmentRank");
                zTablePivot.Columns.Add("TeamRank");
                zTablePivot.Columns.Add("EmployeeID");

                for (int i = 0; i < zTablePivot.Rows.Count - 1; i++)
                {
                    string loai = zTablePivot.Rows[i][1].ToString().Split('|')[0];
                    zTablePivot.Rows[i][0] = loai;
                    zTablePivot.Rows[i]["EmployeeID"] = ConvertIDRank(zTablePivot.Rows[i][1].ToString().Split('|')[2]);
                    zTablePivot.Rows[i]["DepartmentRank"] = zTablePivot.Rows[i][1].ToString().Split('|')[3];
                    zTablePivot.Rows[i]["TeamRank"] = zTablePivot.Rows[i][1].ToString().Split('|')[4];
                }

                zTablePivot.Rows[zTablePivot.Rows.Count - 1]["DepartmentRank"] = "9999";

                DataView dv = zTablePivot.DefaultView;
                dv.Sort = "DepartmentRank ASC, TeamRank ASC , EmployeeID ASC";
                zTablePivot = dv.ToTable();
                zTablePivot.Columns.Remove("DepartmentRank");
                zTablePivot.Columns.Remove("TeamRank");
                zTablePivot.Columns.Remove("EmployeeID");
                //--sắp xếp xong xóa cột sắp xếp đi

                string Loai = zTablePivot.Rows[0][0].ToString();
                DataRow[] Array = zTablePivot.Select("LoaiSP='" + Loai + "'");
                if (Array.Length > 0)
                {
                    AddGroup(zTablePivot, Loai, Array, 0);
                }

                for (int i = 0; i < zTablePivot.Rows.Count; i++)
                {
                    string temploai = zTablePivot.Rows[i][0].ToString();

                    if (temploai != Loai && temploai.ToUpper() != "TỔNG CỘNG")
                    {
                        Loai = temploai;
                        Array = zTablePivot.Select("LoaiSP='" + Loai + "'");
                        if (Array.Length > 0)
                        {
                            AddGroup(zTablePivot, Loai, Array, i);
                        }
                    }
                }
                //Chia số trung bình cột tỏng
                for (int i = 0; i < zTablePivot.Rows.Count; i++)
                {
                    double zTongTien = 0;
                    if (double.TryParse(zTablePivot.Rows[i][zTablePivot.Columns.Count - 3].ToString(), out zTongTien))
                    {

                    }
                    double zTongGio = 0;
                    if (double.TryParse(zTablePivot.Rows[i][zTablePivot.Columns.Count - 2].ToString(), out zTongGio))
                    {

                    }
                    double zTrungBinh = 0;
                    if (zTongGio != 0)
                    {
                        zTrungBinh = Math.Round(zTongTien / zTongGio, 0);
                    }
                    zTablePivot.Rows[i][zTablePivot.Columns.Count - 1] = zTrungBinh.Toe0String();
                }
                //chia trung bình dòng tổng cuối
                int zTam = 4;
                for (int c = 2; c < zTablePivot.Columns.Count; c++)
                {
                    if (zTam == c)
                    {
                        //cột trung bình phải chia
                        double zTongTien = 0;
                        double zTongGio = 0;
                        if (double.TryParse(zTablePivot.Rows[zTablePivot.Rows.Count - 1][c - 2].ToString(), out zTongTien))
                        {

                        }
                        if (double.TryParse(zTablePivot.Rows[zTablePivot.Rows.Count - 1][c - 1].ToString(), out zTongGio))
                        {

                        }

                        if (zTongGio > 0)
                            zTablePivot.Rows[zTablePivot.Rows.Count - 1][c] = Math.Round(zTongTien / zTongGio);
                        else
                            zTablePivot.Rows[zTablePivot.Rows.Count - 1][c] = 0;
                        zTam += 3;
                    }
                }

                this.Invoke(new MethodInvoker(delegate ()
                {
                    InitGVProduct(zTablePivot);
                }));
            }
        }

        private void AddGroup(System.Data.DataTable zTablePivot, string Loai, DataRow[] Array, int Index)
        {
            DataTable zGroup = Array.CopyToDataTable();
            DataRow dr = zTablePivot.NewRow();
            dr[0] = Loai;
            dr[1] = Loai;
            int zTam = 4;

            for (int c = 2; c < zGroup.Columns.Count; c++)
            {
                double Tong = 0;
                foreach (DataRow r in zGroup.Rows)
                {
                    double ztemp = 0;
                    if (double.TryParse(r[c].ToString(), out ztemp))
                    {

                    }
                    Tong += ztemp;
                }
                if (zTam == c)
                {
                    //cột trung bình phải chia
                    double zTongTien = 0;
                    double zTongGio = 0;
                    if (double.TryParse(dr[c - 2].ToString(), out zTongTien))
                    {

                    }
                    if (double.TryParse(dr[c - 1].ToString(), out zTongGio))
                    {

                    }
                    if (zTongGio > 0)
                        dr[c] = Math.Round(zTongTien / zTongGio);
                    else
                        dr[c] = 0;
                    zTam += 3;
                }
                else
                    dr[c] = Tong;
            }

            zTablePivot.Rows.InsertAt(dr, Index);
        }

        void InitGVProduct(System.Data.DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();

            int TotalRow = TableView.Rows.Count + 2;
            int ToTalCol = TableView.Columns.Count + 1;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(TotalRow);

            CellRange zCellRange = GVData.GetCellRange(0, 0, 1, 0);
            zCellRange.Data = "Stt";

            zCellRange = GVData.GetCellRange(0, 1, 1, 1);
            zCellRange.Data = "Họ và tên";

            zCellRange = GVData.GetCellRange(0, 2, 1, 2);
            zCellRange.Data = "Mã thẻ";


            //Row Header
            int ColStart = 3;
            for (int i = 2; i < TableView.Columns.Count; i++)
            {
                DataColumn Col = TableView.Columns[i];
                string[] strCaption = Col.ColumnName.Split('|');
                if (strCaption.Length > 2)
                {
                    GVData.Rows[0][ColStart] = strCaption[1];
                    GVData.Rows[1][ColStart] = strCaption[2];
                }
                else
                {
                    GVData.Rows[0][ColStart] = strCaption[0];
                    GVData.Rows[1][ColStart] = strCaption[1];
                }
                GVData.Cols[i].Width = 120;
                ColStart++;
            }
            for (int i = 2; i < GVData.Cols.Count; i++)
            {

                GVData.Cols[i].Width = 120;

            }


            ColStart = 1;
            int RowStart = 2;
            int zNo = 1;
            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {

                DataRow rData = TableView.Rows[rIndex];

                string[] strTemp = rData[1].ToString().Split('|');

                if (strTemp.Length > 1)
                {
                    GVData.Rows[rIndex + RowStart][0] = zNo;
                    GVData.Rows[rIndex + RowStart][1] = strTemp[1];
                    GVData.Rows[rIndex + RowStart][2] = strTemp[2];
                    zNo++;
                }
                else
                {
                    zNo = 1;
                    GVData.Rows[rIndex + RowStart][1] = strTemp[0];
                    GVData.Rows[rIndex + RowStart].StyleNew.Font = new Font("Tahoma", 9F, FontStyle.Bold | FontStyle.Italic);
                }

                int zColumn_HS = 4;
                for (int cIndex = 2; cIndex < TableView.Columns.Count; cIndex++)
                {
                    //Nếu là hệ số giờ thì lấy 1 số thập phân
                    if((ColStart + cIndex )== zColumn_HS)
                    {
                        GVData.Rows[RowStart + rIndex][ColStart + cIndex] = rData[cIndex].Toe1String();
                        zColumn_HS += 3;
                    }
                    else
                    GVData.Rows[RowStart + rIndex][ColStart + cIndex] = rData[cIndex].Toe0String();
                }
            }


            ////Style
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.FixedOnly;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.CenterCenter;
            GVData.Styles.Normal.WordWrap = true;

            //Trộn dòng 0
            GVData.Rows[0].AllowMerging = true;
            GVData.Rows[0].Height = 30;
            //Trộn dòng 1
            GVData.Rows[1].AllowMerging = true;
            GVData.Rows[1].Height = 45;

            //Trộn cột đầu
            GVData.Cols[0].AllowMerging = true;
            GVData.Cols[0].Width = 35;
            GVData.Cols[1].AllowMerging = true;
            GVData.Cols[1].Width = 200;
            GVData.Cols[2].AllowMerging = true;
            GVData.Cols[2].Width = 100;
            //Trộn 2 cột cuối
            GVData.Cols[ToTalCol - 1].AllowMerging = true;
            GVData.Cols[ToTalCol - 2].AllowMerging = true;
            GVData.Cols[ToTalCol - 3].AllowMerging = true;
            GVData.Cols[ToTalCol - 4].AllowMerging = true;
            //Freeze Row and Column
            GVData.Rows.Fixed = 2;
            GVData.Cols.Frozen = 3;

            //GVData.AutoSizeCols(1, GVData.Cols.Count - 1, 10);
            //Canh phải các cột từ số 1 trở đi
            GVData.Cols[0].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[1].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[2].TextAlign = TextAlignEnum.LeftCenter;

            GVData.Cols[0].StyleNew.BackColor = Color.Empty;
            GVData.Cols[1].StyleNew.BackColor = Color.Empty;
            GVData.Cols[2].StyleNew.BackColor = Color.Empty;

            for (int i = 3; i < GVData.Cols.Count; i++)
            {
                GVData.Cols[i].TextAlign = TextAlignEnum.RightCenter;
            }

            //In đậm các dòng, cột tổng
            GVData.Rows[TotalRow - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[ToTalCol - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[ToTalCol - 2].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[ToTalCol - 3].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            //GVData.Cols[ToTalCol - 4].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
        }

        //Chuyển số thành dãy chuỗi
        string ConvertIDRank(string ID)
        {
            //chèn số thấp tới cao--> tới chữ
            string zResult = "";
            string s = "";
            string temp = "";
            if (ID.Substring(0, 1) == "A")
            {
                temp = ID;
                for (int i = 0; i < 8 - ID.Trim().Length; i++)
                {
                    s += "9";
                }
                zResult = s + temp;
            }
            else if (ID.Substring(0, 1) == "H" || ID.Substring(0, 1) == "L")
            {
                temp = ID;
                for (int i = 0; i < 8 - ID.Trim().Length; i++)
                {
                    s += "B";
                }
                zResult = s + temp;
            }
            else
            {
                temp = ID;
                for (int i = 0; i < 8 - ID.Trim().Length; i++)
                {
                    s += "0";
                }
                zResult = s + temp;
            }
            return zResult;
        }

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                //this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
                btn_Export.Enabled = false;
            }
        }
        #endregion
    }
}
