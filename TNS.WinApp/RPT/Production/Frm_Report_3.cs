﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using TNS.CORE;
using TNS.HRM;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Report_3 : Form
    {
        public Frm_Report_3()
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            Utils.DoubleBuffered(GVData1, true);
            Utils.DrawGVStyle(ref GVData1);
        }

        private void Frm_Report_3_Load(object sender, EventArgs e)
        {
            DateTime ViewDate = SessionUser.Date_Work;
            DateTime FromDate = new DateTime(ViewDate.Year, ViewDate.Month, 1, 0, 0, 0);
            DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            dte_FromDate.Value = FromDate;
            dte_ToDate.Value = ToDate;

            this.Bounds = Screen.PrimaryScreen.WorkingArea;
        }
        private void btn_Search_Click(object sender, EventArgs e)
        {
            if (dte_ToDate.Value.Month != dte_FromDate.Value.Month || dte_ToDate.Value.Year != dte_FromDate.Value.Year)
            {
                Utils.TNMessageBox("Thông báo", "Bạn chỉ được chọn trong 1 tháng", 2);
                return;
            }
            GVData1.Rows.Clear();
            try
            {
                using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }
            }
            catch (Exception ex)
            {
                Utils.TNMessageBox("Thông báo", ex.ToString(), 4);
            }
        }
        private void btn_Export_Click(object sender, EventArgs e)
        {
            string Path = "";
            FolderBrowserDialog FDialog = new FolderBrowserDialog();
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                Path = FDialog.SelectedPath + @"\PhanBo" + dte_ToDate.Value.ToString("MM-yyyy") + ".xlsx";

                DataTable dt = new DataTable();
                foreach (DataGridViewColumn col in GVData1.Columns)
                {
                    dt.Columns.Add(col.HeaderText);
                }

                foreach (DataGridViewRow row in GVData1.Rows)
                {
                    DataRow dRow = dt.NewRow();
                    foreach (DataGridViewCell cell in row.Cells)
                    {
                        dRow[cell.ColumnIndex] = cell.Value;
                    }
                    dt.Rows.Add(dRow);
                }
                string Message = ExportTableToExcel(dt, Path);
                if (Message != "OK")
                    MessageBox.Show(Message);
                else
                {
                    Message = "Đã tạo tập tin thành công !." + Environment.NewLine + "Bạn có muốn mở thư mục chứa ?.";
                    if (MessageBox.Show(Message, "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        Process.Start(Path);
                    }
                }
            }
        }

        #region  //Process Data
        protected virtual bool IsFileLocked(FileInfo file)
        {
            try
            {
                using (FileStream stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    stream.Close();
                }
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }

            //file is not locked
            return false;
        }
        private string ExportTableToExcel(DataTable zTable, string Folder)
        {
            try
            {
                var newFile = new FileInfo(Folder);
                if (newFile.Exists)
                {
                    if (!IsFileLocked(newFile))
                    {
                        newFile.Delete();
                    }
                    else
                    {
                        return "Tập tin đang sử dụng !.";
                    }
                }

                using (var package = new ExcelPackage(newFile))
                {
                    //Tạo Sheet 1 mới với tên Sheet là DonHang
                    ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("PhanBo");
                    worksheet.Cells["A1"].LoadFromDataTable(zTable, true);
                    worksheet.Cells.Style.Font.Name = "Times New Roman";
                    worksheet.Cells.Style.Font.Size = 12;
                    worksheet.Cells.AutoFilter = true;
                    worksheet.Cells.AutoFitColumns();
                    worksheet.View.FreezePanes(2, 4);

                    //Rowheader và row tổng
                    worksheet.Row(1).Height = 60;
                    worksheet.Row(1).Style.WrapText = true;
                    worksheet.Row(1).Style.Font.Bold = true;
                    worksheet.Row(1).Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                    worksheet.Row(1).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Row(zTable.Rows.Count + 1).Style.Font.Bold = true;//dòng tổng
                    //Chỉnh fix độ rộng cột
                    for (int j = 4; j <= zTable.Columns.Count; j++)
                    {
                        worksheet.Column(j).Width = 15;
                    }
                    worksheet.Column(1).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Column(1).Width = 5;
                    worksheet.Column(2).Width = 20;
                    worksheet.Column(3).Width = 40;
                    worksheet.Column(zTable.Columns.Count).Style.Font.Bold = true;//côt tổng
                    //Canh phải số
                    for (int i = 2; i <= zTable.Rows.Count + 1; i++)
                    {
                        for (int j = 4; j <= zTable.Columns.Count; j++)
                        {
                            worksheet.Cells[i, j].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                        }
                    }
                    //Lưu file Excel
                    package.SaveAs(newFile);
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
            return "OK";
        }
        private void DisplayData()
        {
            DataTable zTableColumnName = Teams_Data.List();
            DataTable zTable = Report.No3(dte_FromDate.Value, dte_ToDate.Value, zTableColumnName);
            this.Invoke(new MethodInvoker(delegate ()
            {
                InitGV_Layout(zTable);
                InitGV_Data(zTable);
            }));
        }
        private void InitGV_Layout(DataTable zTable)
        {

            GVData1.Columns.Clear();
            GVData1.Rows.Clear();
            GVData1.ColumnHeadersHeight = 65;
            if (zTable.Rows.Count > 0)
            {
                GVData1.Columns.Add("No", "STT");
                GVData1.Columns.Add("ProductID", "Mã hàng ");
                GVData1.Columns.Add("ProductName", "Tên hàng");
                for (int j = 2; j < zTable.Columns.Count; j++)
                {
                    string Name = zTable.Columns[j].ColumnName;
                    GVData1.Columns.Add(Name, Name);
                    GVData1.Columns[Name].Width = 100;
                    GVData1.Columns[Name].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    GVData1.Columns[Name].ReadOnly = true;
                }
                GVData1.Columns.Add("ToTal", "TỔNG CỘNG");

                GVData1.Columns["No"].Width = 40;
                GVData1.Columns["No"].Frozen = true;
                GVData1.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                GVData1.Columns["No"].ReadOnly = true;
                GVData1.Columns["No"].Frozen = true;

                GVData1.Columns["ProductID"].Width = 100;
                GVData1.Columns["ProductID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                GVData1.Columns["ProductID"].ReadOnly = true;

                GVData1.Columns["ProductName"].Width = 200;
                GVData1.Columns["ProductName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                GVData1.Columns["ProductName"].ReadOnly = true;
                GVData1.Columns["ProductName"].Frozen = true;

                GVData1.Columns["ToTal"].Width = 120;
                GVData1.Columns["ToTal"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                GVData1.Columns["ToTal"].DefaultCellStyle.ForeColor = Color.Navy;
                GVData1.Columns["ToTal"].DefaultCellStyle.Font = new Font("Arial", 9, FontStyle.Bold);
                GVData1.Columns["ToTal"].ReadOnly = true;
                GVData1.Visible = true;
            }
            else
            {
                GVData1.Visible = false;
            }
        }
        private void InitGV_Data(DataTable zTable)
        {
            GVData1.Rows.Clear();
            DataGridViewRow nRowView;
            int rows = 0;

            if (zTable.Rows.Count > 0)
            {
                double[] Total = new double[zTable.Columns.Count];
                double _TOTAL = 0;
                foreach (DataRow nRow in zTable.Rows)
                {

                    double ToTal_Row = 0;
                    GVData1.Rows.Add();
                    nRowView = GVData1.Rows[rows];
                    nRowView.Cells["No"].Value = (rows + 1).ToString();
                    nRowView.Cells["ProductID"].Value = nRow["ProductID"].ToString().Trim();
                    nRowView.Cells["ProductName"].Value = nRow["ProductName"].ToString().Trim();
                    for (int j = 2; j < zTable.Columns.Count; j++)
                    {
                        string Name = zTable.Columns[j].ColumnName;
                        double zteam = 0;
                        if (nRow[Name].ToString().Trim().Length == 0)
                        {
                            nRowView.Cells[Name].Value = "0";
                        }
                        else
                        {
                            zteam = double.Parse((nRow[Name].ToString().Trim()));

                            if (zteam == 0)
                            {
                                nRowView.Cells[Name].Value = "0";
                            }
                            else
                            {
                                nRowView.Cells[Name].Value = zteam.ToString("n0");
                                ToTal_Row += zteam;
                            }

                        }

                        Total[j] += zteam;
                    }
                    nRowView.Cells["ToTal"].Value = ToTal_Row.ToString("n0");
                    _TOTAL += ToTal_Row;
                    rows++;
                }
                nRowView = GVData1.Rows[rows];
                nRowView.Cells["ProductName"].Value = "TỔNG LƯƠNG KHOÁN";
                nRowView.DefaultCellStyle.ForeColor = Color.Navy;
                nRowView.DefaultCellStyle.Font = new Font("Arial", 9, FontStyle.Bold);

                int col = 3;
                for (int j = 2; j < Total.Length; j++)
                {
                    nRowView.Cells[col].Value = Total[j].ToString("n0");
                    nRowView.Cells["ToTal"].Value = _TOTAL.ToString("n0");
                    col++;
                }
            }
        }
        #endregion

        #region [Dùng kéo rê form]
        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Normal)
            {
                this.WindowState = FormWindowState.Maximized;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
                this.StartPosition = FormStartPosition.CenterScreen;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion                
    }
}
