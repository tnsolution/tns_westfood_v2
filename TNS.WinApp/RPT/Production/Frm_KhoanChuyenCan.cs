﻿using C1.Win.C1FlexGrid;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.CORE;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_KhoanChuyenCan : Form
    {
        public Frm_KhoanChuyenCan()
        {
            InitializeComponent();
            btn_Search.Click += Btn_Search_Click;

            btn_Export.Click += Btn_Export_Click;

            btnClose.Click += btnClose_Click;
            btnMax.Click += btnMax_Click;
            btnMini.Click += btnMini_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;
            //Lấy tất cả nhóm trực tiếp sx  và khác bộ phận hỗ trợ sx
            string zSQL = @"
SELECT A.TeamKey, A.TeamID +'-'+ A.TeamName AS TeamName  
FROM SYS_Team A
LEFT JOIN [dbo].[SYS_Department] B ON B.DepartmentKey=A.DepartmentKey
LEFT JOIN [dbo].[SYS_Branch] C ON C.BranchKey=B.BranchKey
WHERE A.RecordStatus <> 99 
AND   A.BranchKey = 4 
AND   A.DepartmentKey != 26
AND   A.TeamKey !=98 
ORDER BY C.RANK,B.RANK,A.RANK";
            LoadDataToToolbox.KryptonComboBox(cbo_Team, zSQL, "--- Tất cả---");
        }

        private void Frm_KhoanChuyenCan_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            Check_RoleForm();
            this.Bounds = Screen.PrimaryScreen.WorkingArea;

            DateTime ViewDate = SessionUser.Date_Work;
            DateTime FromDate = new DateTime(ViewDate.Year, ViewDate.Month, 1, 0, 0, 0);
            DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            dte_FromDate.Value = FromDate;
            dte_ToDate.Value = ToDate;


            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            DisplayData();
        }
        private void DisplayData()
        {
            int GopTienGioDu = 1;
            if (rdo_GopGioDu.Checked == true)
            {
                GopTienGioDu = 1;
            }
            else
            {
                GopTienGioDu = 0;
            }

            DataTable _Intable = HoTroSanXuat.KhoanChuyenCan( cbo_Team.SelectedValue.ToInt(), txt_Search.Text.Trim(), dte_FromDate.Value,dte_ToDate.Value,GopTienGioDu);
            InitGV_Layout(_Intable);
        }
        void InitGV_Layout(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            //int TotalRow = TableView.Rows.Count + 1;
            int ToTalCol = 6;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(1);

            //Row Header
            GVData.Rows[0][0] = "STT";
            GVData.Rows[0][1] = "HỌ VÀ TÊN";
            GVData.Rows[0][2] = "SỐ THẺ";
            GVData.Rows[0][3] = "TỔ NHÓM";
            GVData.Rows[0][4] = "SỐ TIỀN";
            GVData.Rows[0][5] = "";
            double zTotal = 0;
            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                GVData.Rows.Add();
                DataRow rData = TableView.Rows[rIndex];

                GVData.Rows[rIndex + 1][0] = rIndex + 1;
                GVData.Rows[rIndex + 1][1] = rData[0];
                GVData.Rows[rIndex + 1][2] = rData[1];
                GVData.Rows[rIndex + 1][3] = rData[2];
                GVData.Rows[rIndex + 1][4] = rData[3].Toe0String();
                GVData.Rows[rIndex + 1][5] = "";
                if(rData[3].ToString()!="")
                zTotal += double.Parse(rData[3].ToString());
            }
            GVData.Rows.Add(1);
            GVData.Rows[GVData.Rows.Count- 1][1] = "TỔNG CỘNG";
            GVData.Rows[GVData.Rows.Count - 1][4] = zTotal.ToString("n0");
            GVData.Rows[GVData.Rows.Count - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            //Style         
            GVData.AllowFreezing = AllowFreezingEnum.Both;
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.Default;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.LeftCenter;
            GVData.Styles.Normal.WordWrap = true;

            //Freeze Row and Column                              
            GVData.Rows.Fixed = 1;

            GVData.Rows[0].TextAlign = TextAlignEnum.CenterCenter;

            GVData.Rows[0].Height = 40;

            GVData.Cols[0].Width = 40;
            GVData.Cols[1].Width = 200;
            GVData.Cols[2].Width = 100;
            GVData.Cols[3].Width = 100;
            GVData.Cols[4].Width = 100;
            GVData.Cols[5].Width = 100;
            GVData.Cols[4].TextAlign = TextAlignEnum.RightCenter;
        }



        private void Btn_Export_Click(object sender, EventArgs e)
        {

            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Bao cao khoan chuyen can.xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }

                    string Status = "Xuất Excel form " + HeaderControl.Text + " > đường dẫn và tên file xuất:" + FDialog.FileName;
                    Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);

                    GVData.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }

        }
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion
        #region [ Get Auth-Action-Chg_Lang ]
        Access_Role_Info Role;
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
                btn_Export.Enabled = false;
            }

        }
        #endregion
    }
}
