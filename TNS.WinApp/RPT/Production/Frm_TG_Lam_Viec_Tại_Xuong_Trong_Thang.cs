﻿using C1.Win.C1FlexGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.RPT;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace TNS.WinApp
{
    public partial class Frm_TG_Lam_Viec_Tại_Xuong_Trong_Thang : Form
    {
        private int _DepartmentKey = 0;
        private int _TeamKey = 0;
        private DateTime _FromDate;
        private DateTime _ToDate;
        private string _Search = "";
        private int _STT = 0;
        public Frm_TG_Lam_Viec_Tại_Xuong_Trong_Thang()
        {
            InitializeComponent();

            btnClose.Click += btnClose_Click;
            btnMax.Click += btnMax_Click;
            btnMini.Click += btnMini_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            btn_Export.Click += btn_Export_Click;
            btn_Search.Click += Btn_Search_Click;
            cbo_Department.SelectedIndexChanged += Cbo_Department_SelectedIndexChanged;

            btn_Print.Click += Btn_Print_Click;
            btnClose_Panel_Message.Click += BtnClose_Panel_Message_Click;
            btn_Done.Click += Btn_Done_Click;

            rdo_Department.CheckedChanged += Rdo_Department_CheckedChanged; ;
            rdo_Team.CheckedChanged += Rdo_Team_CheckedChanged;
            rdo_Worker.CheckedChanged += Rdo_Worker_CheckedChanged;

            DateTime ViewDate = SessionUser.Date_Work;
            DateTime FromDate = new DateTime(ViewDate.Year, ViewDate.Month, 1, 0, 0, 0);
            DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            dte_FromDate.Value = FromDate;
            dte_ToDate.Value = ToDate;
        }

        

        private void Frm_Thoi_Gian_Lam_Viec_Tại_Xuong_Trong_Thang_Load(object sender, EventArgs e)
        {
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            Check_RoleForm();


            LoadDataToToolbox.KryptonComboBox(cbo_Department, "SELECT DepartmentKey, DepartmentName FROM[dbo].[SYS_Department] WHERE BranchKey = 4  AND RecordStatus< 99", "---- Chọn tất cả----");
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;

            // Khởi tạo thông tin báo cáo
            LoadInfo_Report();
            Panel_Done.Visible = false;
        }
        private void Rdo_Department_CheckedChanged(object sender, EventArgs e)
        {
            if (rdo_Department.Checked == true)
            {
                cbo_Team.Enabled = false;
                txt_Search.Enabled = false;
            }
        }
        private void Rdo_Team_CheckedChanged(object sender, EventArgs e)
        {
            if (rdo_Team.Checked == true)
            {
                cbo_Department.Enabled = true;
                cbo_Team.Enabled = true;
                txt_Search.Enabled = false;
            }
        }
        private void Rdo_Worker_CheckedChanged(object sender, EventArgs e)
        {
            cbo_Department.Enabled = true;
            cbo_Team.Enabled = true;
            txt_Search.Enabled = true;
        }

        private void Cbo_Department_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDataToToolbox.KryptonComboBox(cbo_Team, "SELECT TeamKey,TeamName FROM SYS_Team WHERE TeamKey != 98 AND DepartmentKey = " + cbo_Department.SelectedValue.ToInt() + "  AND RecordStatus < 99", "---- Chọn tất cả ----");
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            _DepartmentKey = cbo_Department.SelectedValue.ToInt();
            _TeamKey = cbo_Team.SelectedValue.ToInt();
            _FromDate = dte_FromDate.Value;
            _ToDate = dte_ToDate.Value;
            _Search = txt_Search.Text.Trim();
            if (dte_ToDate.Value.Month != dte_FromDate.Value.Month && dte_ToDate.Value.Year != dte_FromDate.Value.Year)
            {
                Utils.TNMessageBoxOK("Vui lòng chỉ chọn trong 1 tháng !",1);
                return;
            }

            if (rdo_Department.Checked)
                using (Frm_Loading frm = new Frm_Loading(DisplayData_BoPhan)) { frm.ShowDialog(this); }
            if (rdo_Team.Checked)
            using (Frm_Loading frm = new Frm_Loading(DisplayData_Nhom)) { frm.ShowDialog(this); }
            if (rdo_Worker.Checked)
                using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }
        }

        #region[Xem theo nhan sự]
        private void DisplayData()
        {
            DataTable zTable = new DataTable();
            if (rdo_NoRate.Checked == true)
            {
                zTable = Report_TimeKeeping.TG_LamTaiXuong_TheoCongNhan(_FromDate, _ToDate, _DepartmentKey, _TeamKey, _Search);
            }
            if(rdo_Rate.Checked==true)
            {   // có nhân hệ số
                zTable = Report_TimeKeeping.TG_LamTaiXuong_TheoCongNhan_DaNhanHeSo(_FromDate, _ToDate, _DepartmentKey, _TeamKey, _Search);
            }
            if (zTable.Rows.Count > 0)
            {
                PivotTable zPvt = new PivotTable(zTable);
                DataTable zTablePivot = zPvt.Generate_ASC("HeaderColumn", "LeftColumn", new string[] { "TotalTime", "OverTime", "DifferentTime" }, "HeaderColumn", "TỔNG CỘNG", "TỔNG CỘNG", new string[] { "Tổng giờ", "Giờ dư", "Giờ công việc khác" }, 0);
                //Tách cột Leftcolumn
                zTablePivot.Columns.Add("EmployeeKey").SetOrdinal(1);
                zTablePivot.Columns.Add("EmpoyeeName").SetOrdinal(2);
                zTablePivot.Columns.Add("EmpoyeeID").SetOrdinal(3);
                zTablePivot.Columns.Add("PositionName").SetOrdinal(4);
                zTablePivot.Columns.Add("TeamKey", typeof(int)).SetOrdinal(5);
                zTablePivot.Columns.Add("TeamName").SetOrdinal(6);
                zTablePivot.Columns.Add("EmployeeRank");
                zTablePivot.Columns.Add("DepartmentRank");
                zTablePivot.Columns.Add("TeamRank");
                for (int i = 0; i < zTablePivot.Rows.Count - 1; i++)
                {
                    string[] temp = zTablePivot.Rows[i][0].ToString().Split('|');
                    zTablePivot.Rows[i]["EmployeeKey"] = temp[0];
                    zTablePivot.Rows[i]["EmpoyeeName"] = temp[1];
                    zTablePivot.Rows[i]["EmpoyeeID"] = temp[2];
                    zTablePivot.Rows[i]["PositionName"] = temp[3];
                    zTablePivot.Rows[i]["TeamKey"] = temp[4];
                    zTablePivot.Rows[i]["TeamName"] = temp[5];
                    zTablePivot.Rows[i]["EmployeeRank"] = ConvertIDRank(temp[2]);
                    zTablePivot.Rows[i]["DepartmentRank"] = temp[6];
                    zTablePivot.Rows[i]["TeamRank"] = temp[7];
                }
                //Xóa cột đầu đã cắt chuỗi và xóa dòng tổng cuối
                zTablePivot.Columns.Remove("HeaderColumn");
                DataRow row = zTablePivot.Rows[zTablePivot.Rows.Count - 1];
                zTablePivot.Rows.Remove(row);
                //Sắp xếp nhân viên
                DataView dv = zTablePivot.DefaultView;
                dv.Sort = " DepartmentRank ASC,TeamRank ASC,  EmployeeRank ASC";
                zTablePivot = dv.ToTable();
                zTablePivot.Columns.Remove("EmployeeRank");
                zTablePivot.Columns.Remove("DepartmentRank");
                zTablePivot.Columns.Remove("TeamRank");




                this.Invoke(new MethodInvoker(delegate ()
                {
                    InitGVProduct(zTablePivot);
                    DataTable _Intable = zTablePivot;
                    _STT = 1;
                    int NoGroup = 3;
                    int RowTam = NoGroup;
                    Row zGvRow;
                    int TeamKey = _Intable.Rows[0]["TeamKey"].ToInt();
                    string TeamName = _Intable.Rows[0]["TeamName"].ToString();
                    DataRow[] Array = _Intable.Select("TeamKey=" + TeamKey);
                    if (Array.Length > 0)
                    {
                        DataTable zGroup = Array.CopyToDataTable();
                        GVData.Rows.Add();
                        zGvRow = GVData.Rows[2];
                        HeaderRow(zGvRow, zGroup, TeamKey, TeamName, 0);
                    }
                    for (int i = 0; i < _Intable.Rows.Count; i++)
                    {
                        DataRow r = _Intable.Rows[i];
                        if (TeamKey != r["TeamKey"].ToInt())
                        {
                            #region [GROUP]
                            TeamKey = r["TeamKey"].ToInt();
                            TeamName = r["TeamName"].ToString();
                            Array = _Intable.Select("TeamKey=" + TeamKey);
                            _STT = 1;
                            if (Array.Length > 0)
                            {
                                DataTable zGroup = Array.CopyToDataTable();
                                GVData.Rows.Add();
                                zGvRow = GVData.Rows[i + NoGroup];
                                HeaderRow(zGvRow, zGroup, TeamKey, TeamName, _STT);
                            }
                            #endregion
                            NoGroup++;
                        }
                        GVData.Rows.Add();
                        zGvRow = GVData.Rows[i + NoGroup];
                        DetailRow(zGvRow, r, _STT);
                        RowTam = i + NoGroup;
                        _STT++;
                    }
                    GVData.Rows.Add();
                    zGvRow = GVData.Rows[RowTam + 1];
                    TotalRow(zGvRow, _Intable, RowTam + 1);
                }));
            }
            else
            {
                GVData.Rows.Count = 0;
                GVData.Cols.Count = 0;
            }
        }
        private void HeaderRow(Row RowView, DataTable Table, int TeamKey, string TeamName, int No)
        {
            RowView[0] = "";
            RowView[1] = TeamName;
            RowView[2] = "Số nhân sự " + Table.Select("TeamKey='" + TeamKey + "'").Length;
            RowView[3] = "";
            int nCol = 4;
            double zTotal = 0;
            for (int i = 6; i < Table.Columns.Count - 1; i++)
            {
                double zTemp = 0;
                for (int k = 0; k < Table.Rows.Count; k++)
                {
                    double TotalRow = 0;
                    if (double.TryParse(Table.Rows[k][i].ToString(), out TotalRow))
                    {
                        zTemp += TotalRow;
                    }

                }
                RowView[nCol] =SoGio(zTemp.ToString());
                zTotal += zTemp;
                nCol++;
            }

            RowView.StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
        }
        private void DetailRow(Row RowView, DataRow rDetail, int No)
        {
            RowView[0] = No;
            RowView[1] = rDetail[1].ToString().Trim();//họ tên
            RowView[2] = rDetail[3].ToString().Trim();//chức vụ
            RowView[3] = rDetail[2].ToString().Trim();//mã thẻ
            int nCol = 4;
            for (int i = 6; i < rDetail.ItemArray.Length - 1; i++)
            {
                RowView[nCol] = SoGio(rDetail[i].ToString());
                nCol++;
            }
        }
        private void TotalRow(Row RowView, DataTable Table, int No)
        {
            RowView[0] = "";
            RowView[1] = "TỔNG CỘNG ";
            RowView[2] = "Số nhân sự " + Table.Rows.Count;
            RowView[3] = "";

            //float zTotal = 0;
            int nCol = 4;
            for (int i = 6; i < Table.Columns.Count - 1; i++)
            {
                double zTemp = 0;
                for (int k = 0; k < Table.Rows.Count; k++)
                {
                    double TotalRow = 0;
                    if (double.TryParse(Table.Rows[k][i].ToString(), out TotalRow))
                    {
                        zTemp += TotalRow;
                    }

                }
                RowView[nCol] = SoGio(zTemp.ToString());
                nCol++;
            }
            RowView.StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
        }
        void InitGVProduct(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();

            int TotalRow = 2;
            int ToTalCol = TableView.Columns.Count - 2;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(TotalRow);

            CellRange zCellRange = GVData.GetCellRange(0, 0, 1, 0);
            zCellRange.Data = "STT";

            zCellRange = GVData.GetCellRange(0, 1, 1, 1);
            zCellRange.Data = "HỌ TÊN";

            zCellRange = GVData.GetCellRange(0, 2, 1, 2);
            zCellRange.Data = "CHỨC VỤ";

            zCellRange = GVData.GetCellRange(0, 3, 1, 3);
            zCellRange.Data = "SỐ THẺ";

            int ColStart = 4;
            // header
            for (int i = 6; i < TableView.Columns.Count; i++)
            {
                DataColumn Col = TableView.Columns[i];
                string[] strCaption = Col.ColumnName.Split('|');
                if (strCaption.Length > 1)
                {
                    GVData.Rows[0][ColStart] = strCaption[0];
                    GVData.Rows[1][ColStart] = strCaption[1];
                }
                else
                {
                    GVData.Rows[0][ColStart] = strCaption[0];
                    GVData.Rows[1][ColStart] = strCaption[0];
                }
                ColStart++;
            }

            

            ////Style
            //GVData.AllowFreezing = AllowFreezingEnum.Both;
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.FixedOnly;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.CenterCenter;
            GVData.Styles.Normal.WordWrap = true;



            //Trộn cột 
            for (int i = 0; i < GVData.Cols.Count; i++)
            {
                GVData.Cols[i].AllowMerging = true;
            }
            //Trộn dòng 0
            GVData.Rows[0].AllowMerging = true;
            GVData.Rows[0].Height = 50;
            GVData.Rows[1].Height = 60;
            //Trộn dòng 1
            //Freeze Row and Column
            GVData.Rows.Fixed = 2;
            GVData.Cols.Frozen = 4;

            GVData.Cols[0].Width = 40;
            GVData.Cols[1].Width = 200;
            GVData.Cols[2].Width = 150;
            GVData.Cols[3].Width = 100;
            //GVData.AutoSizeCols(1, GVData.Cols.Count - 1, 10);
            //Canh phải các cột từ số 1 trở đi
            GVData.Cols[0].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[1].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[2].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[3].TextAlign = TextAlignEnum.LeftCenter;

            GVData.Cols[0].StyleNew.BackColor = Color.Empty;
            GVData.Cols[1].StyleNew.BackColor = Color.Empty;
            GVData.Cols[2].StyleNew.BackColor = Color.Empty;
            GVData.Cols[3].StyleNew.BackColor = Color.Empty;

            for (int i = 4; i < GVData.Cols.Count; i++)
            {
                GVData.Cols[i].Width = 70;
                GVData.Cols[i].TextAlign = TextAlignEnum.RightCenter;
            }

            //In đậm các dòng, cột tổng
            //GVData.Rows[TotalRow - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[ToTalCol - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[ToTalCol - 2].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[ToTalCol - 3].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);

            GVData.Cols[ToTalCol - 1].Width = 100;
            GVData.Cols[ToTalCol - 2].Width = 100;
            GVData.Cols[ToTalCol - 3].Width = 100;

            //GVData.Cols[ToTalCol - 1].StyleNew.BackColor = Color.LightGreen;
            // tô màu cột tổng
            //GVData.Rows[TotalRow - 1].StyleNew.BackColor = Color.LightGreen;

        }
        #endregion

        #region[Xem theo nhóm]
        private void DisplayData_Nhom()
        {
            DataTable zTable = new DataTable();
            if(rdo_NoRate.Checked==true)
            zTable = Report_TimeKeeping.TG_LamTaiXuong_TheoNhom(_FromDate, _ToDate, _DepartmentKey, _TeamKey);
            if(rdo_Rate.Checked==true)
                zTable = Report_TimeKeeping.TG_LamTaiXuong_TheoNhom_DaNhanHeSo(_FromDate, _ToDate, _DepartmentKey, _TeamKey);
            if (zTable.Rows.Count > 0)
            {
                PivotTable zPvt = new PivotTable(zTable);
                DataTable zTablePivot = zPvt.Generate_ASC("HeaderColumn", "LeftColumn", new string[] { "TotalTime", "OverTime", "DifferentTime" }, "HeaderColumn", "TỔNG CỘNG", "TỔNG CỘNG", new string[] { "Tổng giờ", "Giờ dư", "Giờ công việc khác" }, 0);
                //Tách cột Leftcolumn
                zTablePivot.Columns.Add("TeamKey").SetOrdinal(1);
                zTablePivot.Columns.Add("TeamName").SetOrdinal(2);
                zTablePivot.Columns.Add("TeamID").SetOrdinal(3);
                zTablePivot.Columns.Add("DepartmentKey", typeof(int)).SetOrdinal(4);
                zTablePivot.Columns.Add("DepartmentName").SetOrdinal(5);
                zTablePivot.Columns.Add("DepartmentRank");
                zTablePivot.Columns.Add("TeamRank");
                for (int i = 0; i < zTablePivot.Rows.Count - 1; i++)
                {
                    string[] temp = zTablePivot.Rows[i][0].ToString().Split('|');
                    zTablePivot.Rows[i]["TeamKey"] = temp[0];
                    zTablePivot.Rows[i]["TeamName"] = temp[1];
                    zTablePivot.Rows[i]["TeamID"] = temp[2];
                    zTablePivot.Rows[i]["DepartmentKey"] = temp[3];
                    zTablePivot.Rows[i]["DepartmentName"] = temp[4];
                    zTablePivot.Rows[i]["DepartmentRank"] = temp[5];
                    zTablePivot.Rows[i]["TeamRank"] = temp[6];
                }
                //Xóa cột đầu đã cắt chuỗi và xóa dòng tổng cuối
                zTablePivot.Columns.Remove("HeaderColumn");
                DataRow row = zTablePivot.Rows[zTablePivot.Rows.Count - 1];
                zTablePivot.Rows.Remove(row);
                //Sắp xếp nhân viên
                DataView dv = zTablePivot.DefaultView;
                dv.Sort = " DepartmentRank ASC,TeamRank ASC";
                zTablePivot = dv.ToTable();
                zTablePivot.Columns.Remove("DepartmentRank");
                zTablePivot.Columns.Remove("TeamRank");




                this.Invoke(new MethodInvoker(delegate ()
                {
                    InitGVProduct_Nhom(zTablePivot);
                    DataTable _Intable = zTablePivot;
                    _STT = 1;
                    int NoGroup = 3;
                    int RowTam = NoGroup;
                    Row zGvRow;
                    int DepartmentKey = _Intable.Rows[0]["DepartmentKey"].ToInt();
                    string DepartmentName = _Intable.Rows[0]["DepartmentName"].ToString();
                    DataRow[] Array = _Intable.Select("DepartmentKey=" + DepartmentKey);
                    if (Array.Length > 0)
                    {
                        DataTable zGroup = Array.CopyToDataTable();
                        GVData.Rows.Add();
                        zGvRow = GVData.Rows[2];
                        HeaderRow_Nhom(zGvRow, zGroup, DepartmentKey, DepartmentName, 0);
                    }
                    for (int i = 0; i < _Intable.Rows.Count; i++)
                    {
                        DataRow r = _Intable.Rows[i];
                        if (DepartmentKey != r["DepartmentKey"].ToInt())
                        {
                            #region [GROUP]
                            DepartmentKey = r["DepartmentKey"].ToInt();
                            DepartmentName = r["DepartmentName"].ToString();
                            Array = _Intable.Select("DepartmentKey=" + DepartmentKey);
                            _STT = 1;
                            if (Array.Length > 0)
                            {
                                DataTable zGroup = Array.CopyToDataTable();
                                GVData.Rows.Add();
                                zGvRow = GVData.Rows[i + NoGroup];
                                HeaderRow_Nhom(zGvRow, zGroup, DepartmentKey, DepartmentName, _STT);
                            }
                            #endregion
                            NoGroup++;
                        }
                        GVData.Rows.Add();
                        zGvRow = GVData.Rows[i + NoGroup];
                        DetailRow_Nhom(zGvRow, r, _STT);
                        RowTam = i + NoGroup;
                        _STT++;
                    }
                    GVData.Rows.Add();
                    zGvRow = GVData.Rows[RowTam + 1];
                    TotalRow_Nhom(zGvRow, _Intable, RowTam + 1);
                }));
            }
            else
            {
                GVData.Rows.Count = 0;
                GVData.Cols.Count = 0;
            }
        }
        private void HeaderRow_Nhom(Row RowView, DataTable Table, int DepartmentKey, string DepartmentName, int No)
        {
            RowView[0] = "";
            RowView[1] = DepartmentName;
            RowView[2] = "Số tổ nhóm " + Table.Select("DepartmentKey='" + DepartmentKey + "'").Length;
            RowView[3] = "";
            int nCol = 3;
            double zTotal = 0;
            for (int i = 5; i < Table.Columns.Count - 1; i++)
            {
                double zTemp = 0;
                for (int k = 0; k < Table.Rows.Count; k++)
                {
                    double TotalRow = 0;
                    if (double.TryParse(Table.Rows[k][i].ToString(), out TotalRow))
                    {
                        zTemp += TotalRow;
                    }

                }
                RowView[nCol] = SoGio(zTemp.ToString());
                zTotal += zTemp;
                nCol++;
            }

            RowView.StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
        }
        private void DetailRow_Nhom(Row RowView, DataRow rDetail, int No)
        {
            RowView[0] = No;
            RowView[1] = rDetail[1].ToString().Trim();// tên
            RowView[2] = rDetail[2].ToString().Trim();//mã
            int nCol = 3;
            for (int i = 5; i < rDetail.ItemArray.Length - 1; i++)
            {
                RowView[nCol] = SoGio(rDetail[i].ToString());
                nCol++;
            }
        }
        private void TotalRow_Nhom(Row RowView, DataTable Table, int No)
        {
            RowView[0] = "";
            RowView[1] = "TỔNG CỘNG ";
            RowView[2] = "Số tổ nhóm " + Table.Rows.Count;

            //float zTotal = 0;
            int nCol = 3;
            for (int i = 5; i < Table.Columns.Count - 1; i++)
            {
                double zTemp = 0;
                for (int k = 0; k < Table.Rows.Count; k++)
                {
                    double TotalRow = 0;
                    if (double.TryParse(Table.Rows[k][i].ToString(), out TotalRow))
                    {
                        zTemp += TotalRow;
                    }

                }
                RowView[nCol] = SoGio(zTemp.ToString());
                nCol++;
            }
            RowView.StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
        }
        void InitGVProduct_Nhom(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();

            int TotalRow = 2;
            int ToTalCol = TableView.Columns.Count - 2;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(TotalRow);

            CellRange zCellRange = GVData.GetCellRange(0, 0, 1, 0);
            zCellRange.Data = "STT";

            zCellRange = GVData.GetCellRange(0, 1, 1, 1);
            zCellRange.Data = "TÊN TỔ NHÓM";

            zCellRange = GVData.GetCellRange(0, 2, 1, 2);
            zCellRange.Data = "MÃ TỔ NHÓM";

            int ColStart = 3;
            // header
            for (int i = 5; i < TableView.Columns.Count; i++)
            {
                DataColumn Col = TableView.Columns[i];
                string[] strCaption = Col.ColumnName.Split('|');
                if (strCaption.Length > 1)
                {
                    GVData.Rows[0][ColStart] = strCaption[0];
                    GVData.Rows[1][ColStart] = strCaption[1];
                }
                else
                {
                    GVData.Rows[0][ColStart] = strCaption[0];
                    GVData.Rows[1][ColStart] = strCaption[0];
                }
                ColStart++;
            }



            ////Style
            //GVData.AllowFreezing = AllowFreezingEnum.Both;
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.FixedOnly;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.CenterCenter;
            GVData.Styles.Normal.WordWrap = true;



            //Trộn cột 
            for (int i = 0; i < GVData.Cols.Count; i++)
            {
                GVData.Cols[i].AllowMerging = true;
            }
            //Trộn dòng 0
            GVData.Rows[0].AllowMerging = true;
            GVData.Rows[0].Height = 50;
            GVData.Rows[1].Height = 60;
            //Trộn dòng 1
            //Freeze Row and Column
            GVData.Rows.Fixed = 2;
            GVData.Cols.Frozen = 3;

            GVData.Cols[0].Width = 40;
            GVData.Cols[1].Width = 200;
            GVData.Cols[2].Width = 150;
            //GVData.AutoSizeCols(1, GVData.Cols.Count - 1, 10);
            //Canh phải các cột từ số 1 trở đi
            GVData.Cols[0].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[1].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[2].TextAlign = TextAlignEnum.LeftCenter;

            GVData.Cols[0].StyleNew.BackColor = Color.Empty;
            GVData.Cols[1].StyleNew.BackColor = Color.Empty;
            GVData.Cols[2].StyleNew.BackColor = Color.Empty;

            for (int i = 3; i < GVData.Cols.Count; i++)
            {
                GVData.Cols[i].Width = 75;
                GVData.Cols[i].TextAlign = TextAlignEnum.RightCenter;
            }

            //In đậm các dòng, cột tổng
            //GVData.Rows[TotalRow - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[ToTalCol - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[ToTalCol - 2].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[ToTalCol - 3].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);

            GVData.Cols[ToTalCol - 1].Width = 100;
            GVData.Cols[ToTalCol - 2].Width = 100;
            GVData.Cols[ToTalCol - 3].Width = 100;

            //GVData.Cols[ToTalCol - 1].StyleNew.BackColor = Color.LightGreen;
            // tô màu cột tổng
            //GVData.Rows[TotalRow - 1].StyleNew.BackColor = Color.LightGreen;

        }
        #endregion

        #region[Xem theo bộ phận]
        private void DisplayData_BoPhan()
        {
            DataTable zTable = new DataTable();
            if(rdo_NoRate.Checked==true)
            zTable = Report_TimeKeeping.TG_LamTaiXuong_TheoBoPhan(_FromDate, _ToDate, _DepartmentKey);
            if(rdo_Rate.Checked==true)
                zTable = Report_TimeKeeping.TG_LamTaiXuong_TheoBoPhan_DaNhanHeSo(_FromDate, _ToDate, _DepartmentKey);
            if (zTable.Rows.Count > 0)
            {
                PivotTable zPvt = new PivotTable(zTable);
                DataTable zTablePivot = zPvt.Generate_ASC("HeaderColumn", "LeftColumn", new string[] { "TotalTime", "OverTime", "DifferentTime" }, "HeaderColumn", "TỔNG CỘNG", "TỔNG CỘNG", new string[] { "Tổng giờ", "Giờ dư", "Giờ công việc khác" }, 0);
                //Tách cột Leftcolumn
                zTablePivot.Columns.Add("DepartmentKey").SetOrdinal(1);
                zTablePivot.Columns.Add("DepartmentName").SetOrdinal(2);
                zTablePivot.Columns.Add("DepartmentID").SetOrdinal(3);
                zTablePivot.Columns.Add("BranchKey", typeof(int)).SetOrdinal(4);
                zTablePivot.Columns.Add("BranchName").SetOrdinal(5);
                zTablePivot.Columns.Add("BranchRank");
                zTablePivot.Columns.Add("DepartmentRank");
                for (int i = 0; i < zTablePivot.Rows.Count - 1; i++)
                {
                    string[] temp = zTablePivot.Rows[i][0].ToString().Split('|');
                    zTablePivot.Rows[i]["DepartmentKey"] = temp[0];
                    zTablePivot.Rows[i]["DepartmentName"] = temp[1];
                    zTablePivot.Rows[i]["DepartmentID"] = temp[2];
                    zTablePivot.Rows[i]["BranchKey"] = temp[3];
                    zTablePivot.Rows[i]["BranchName"] = temp[4];
                    zTablePivot.Rows[i]["BranchRank"] = temp[5];
                    zTablePivot.Rows[i]["DepartmentRank"] = temp[6];
                }
                //Xóa cột đầu đã cắt chuỗi và xóa dòng tổng cuối
                zTablePivot.Columns.Remove("HeaderColumn");
                DataRow row = zTablePivot.Rows[zTablePivot.Rows.Count - 1];
                zTablePivot.Rows.Remove(row);
                //Sắp xếp nhân viên
                DataView dv = zTablePivot.DefaultView;
                dv.Sort = " BranchRank ASC, DepartmentRank ASC";
                zTablePivot = dv.ToTable();
                zTablePivot.Columns.Remove("BranchRank");
                zTablePivot.Columns.Remove("DepartmentRank");




                this.Invoke(new MethodInvoker(delegate ()
                {
                    InitGVProduct_BoPhan(zTablePivot);
                    DataTable _Intable = zTablePivot;
                    _STT = 1;
                    int NoGroup = 3;
                    int RowTam = NoGroup;
                    Row zGvRow;
                    int BranchKey = _Intable.Rows[0]["BranchKey"].ToInt();
                    string BranchName = _Intable.Rows[0]["BranchName"].ToString();
                    DataRow[] Array = _Intable.Select("BranchKey=" + BranchKey);
                    if (Array.Length > 0)
                    {
                        DataTable zGroup = Array.CopyToDataTable();
                        GVData.Rows.Add();
                        zGvRow = GVData.Rows[2];
                        HeaderRow_BoPhan(zGvRow, zGroup, BranchKey, BranchName, 0);
                    }
                    for (int i = 0; i < _Intable.Rows.Count; i++)
                    {
                        DataRow r = _Intable.Rows[i];
                        if (BranchKey != r["BranchKey"].ToInt())
                        {
                            #region [GROUP]
                            BranchKey = r["BranchKey"].ToInt();
                            BranchName = r["BranchName"].ToString();
                            Array = _Intable.Select("BranchKey=" + BranchKey);
                            _STT = 1;
                            if (Array.Length > 0)
                            {
                                DataTable zGroup = Array.CopyToDataTable();
                                GVData.Rows.Add();
                                zGvRow = GVData.Rows[i + NoGroup];
                                HeaderRow_BoPhan(zGvRow, zGroup, BranchKey, BranchName, _STT);
                            }
                            #endregion
                            NoGroup++;
                        }
                        GVData.Rows.Add();
                        zGvRow = GVData.Rows[i + NoGroup];
                        DetailRow_BoPhan(zGvRow, r, _STT);
                        RowTam = i + NoGroup;
                        _STT++;
                    }
                    GVData.Rows.Add();
                    zGvRow = GVData.Rows[RowTam + 1];
                    TotalRow_BoPhan(zGvRow, _Intable, RowTam + 1);
                }));
            }
            else
            {
                GVData.Rows.Count = 0;
                GVData.Cols.Count = 0;
            }
        }
        private void HeaderRow_BoPhan(Row RowView, DataTable Table, int BranchKey, string BranchName, int No)
        {
            RowView[0] = "";
            RowView[1] = BranchName;
            RowView[2] = "Số bộ phận" + Table.Select("BranchKey='" + BranchKey + "'").Length;
            RowView[3] = "";
            int nCol = 3;
            double zTotal = 0;
            for (int i = 5; i < Table.Columns.Count - 1; i++)
            {
                double zTemp = 0;
                for (int k = 0; k < Table.Rows.Count; k++)
                {
                    double TotalRow = 0;
                    if (double.TryParse(Table.Rows[k][i].ToString(), out TotalRow))
                    {
                        zTemp += TotalRow;
                    }

                }
                RowView[nCol] = SoGio(zTemp.ToString());
                zTotal += zTemp;
                nCol++;
            }

            RowView.StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
        }
        private void DetailRow_BoPhan(Row RowView, DataRow rDetail, int No)
        {
            RowView[0] = No;
            RowView[1] = rDetail[1].ToString().Trim();// tên
            RowView[2] = rDetail[2].ToString().Trim();//mã
            int nCol = 3;
            for (int i = 5; i < rDetail.ItemArray.Length - 1; i++)
            {
                RowView[nCol] = SoGio(rDetail[i].ToString());
                nCol++;
            }
        }
        private void TotalRow_BoPhan(Row RowView, DataTable Table, int No)
        {
            RowView[0] = "";
            RowView[1] = "TỔNG CỘNG ";
            RowView[2] = "Số bộ phận " + Table.Rows.Count;

            //float zTotal = 0;
            int nCol = 3;
            for (int i = 5; i < Table.Columns.Count - 1; i++)
            {
                double zTemp = 0;
                for (int k = 0; k < Table.Rows.Count; k++)
                {
                    double TotalRow = 0;
                    if (double.TryParse(Table.Rows[k][i].ToString(), out TotalRow))
                    {
                        zTemp += TotalRow;
                    }

                }
                RowView[nCol] = SoGio(zTemp.ToString());
                nCol++;
            }
            RowView.StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
        }
        void InitGVProduct_BoPhan(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();

            int TotalRow = 2;
            int ToTalCol = TableView.Columns.Count - 2;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(TotalRow);

            CellRange zCellRange = GVData.GetCellRange(0, 0, 1, 0);
            zCellRange.Data = "STT";

            zCellRange = GVData.GetCellRange(0, 1, 1, 1);
            zCellRange.Data = "TÊN BỘ PHẬN";

            zCellRange = GVData.GetCellRange(0, 2, 1, 2);
            zCellRange.Data = "MÃ BỘ PHẬN";

            int ColStart = 3;
            // header
            for (int i = 5; i < TableView.Columns.Count; i++)
            {
                DataColumn Col = TableView.Columns[i];
                string[] strCaption = Col.ColumnName.Split('|');
                if (strCaption.Length > 1)
                {
                    GVData.Rows[0][ColStart] = strCaption[0];
                    GVData.Rows[1][ColStart] = strCaption[1];
                }
                else
                {
                    GVData.Rows[0][ColStart] = strCaption[0];
                    GVData.Rows[1][ColStart] = strCaption[0];
                }
                ColStart++;
            }



            ////Style
            //GVData.AllowFreezing = AllowFreezingEnum.Both;
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.FixedOnly;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.CenterCenter;
            GVData.Styles.Normal.WordWrap = true;



            //Trộn cột 
            for (int i = 0; i < GVData.Cols.Count; i++)
            {
                GVData.Cols[i].AllowMerging = true;
            }
            //Trộn dòng 0
            GVData.Rows[0].AllowMerging = true;
            GVData.Rows[0].Height = 50;
            GVData.Rows[1].Height = 60;
            //Trộn dòng 1
            //Freeze Row and Column
            GVData.Rows.Fixed = 2;
            GVData.Cols.Frozen = 3;

            GVData.Cols[0].Width = 40;
            GVData.Cols[1].Width = 200;
            GVData.Cols[2].Width = 150;
            //GVData.AutoSizeCols(1, GVData.Cols.Count - 1, 10);
            //Canh phải các cột từ số 1 trở đi
            GVData.Cols[0].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[1].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[2].TextAlign = TextAlignEnum.LeftCenter;

            GVData.Cols[0].StyleNew.BackColor = Color.Empty;
            GVData.Cols[1].StyleNew.BackColor = Color.Empty;
            GVData.Cols[2].StyleNew.BackColor = Color.Empty;

            for (int i = 3; i < GVData.Cols.Count; i++)
            {
                GVData.Cols[i].Width = 75;
                GVData.Cols[i].TextAlign = TextAlignEnum.RightCenter;
            }

            //In đậm các dòng, cột tổng
            //GVData.Rows[TotalRow - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[ToTalCol - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[ToTalCol - 2].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[ToTalCol - 3].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);

            GVData.Cols[ToTalCol - 1].Width = 100;
            GVData.Cols[ToTalCol - 2].Width = 100;
            GVData.Cols[ToTalCol - 3].Width = 100;

            //GVData.Cols[ToTalCol - 1].StyleNew.BackColor = Color.LightGreen;
            // tô màu cột tổng
            //GVData.Rows[TotalRow - 1].StyleNew.BackColor = Color.LightGreen;

        }
        #endregion
        //Chuyển số thành dãy chuỗi
        string ConvertIDRank(string ID)
        {
            //chèn số thấp tới cao--> tới chữ
            string zResult = "";
            string s = "";
            string temp = "";
            if (ID.Substring(0, 1) == "A")
            {
                temp = ID;
                for (int i = 0; i < 8 - ID.Trim().Length; i++)
                {
                    s += "9";
                }
                zResult = s + temp;
            }
            else if (ID.Substring(0, 1) == "H" || ID.Substring(0, 1) == "L")
            {
                temp = ID;
                for (int i = 0; i < 8 - ID.Trim().Length; i++)
                {
                    s += "B";
                }
                zResult = s + temp;
            }
            else
            {
                temp = ID;
                for (int i = 0; i < 8 - ID.Trim().Length; i++)
                {
                    s += "0";
                }
                zResult = s + temp;
            }
            return zResult;
        }
        private void btn_Export_Click(object sender, EventArgs e)
        {
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Báo_Cáo_Thời_Gian_Làm_Việc_Tại_Xưởng Từ_" + dte_FromDate.Value.ToString("dd_MM_yyyy") + "_Đến_" + dte_ToDate.Value.ToString("dd_MM_yyyy") + ".xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }

                    string Status = "Xuất Excel form " + HeaderControl.Text + " > đường dẫn và tên file xuất:" + FDialog.FileName;
                    Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);

                    GVData.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }

        }
        string SoGio(string TongSoPhut)
        {
            double ztemp = 0;
            if(double.TryParse(TongSoPhut.ToString(),out ztemp))
            {

            }
            //Biên hệ số giờ thành tổng số phút
            if (ztemp >0)
            {
                int zHour = Convert.ToInt32(ztemp) / 60;
                int zMinus = Convert.ToInt32(ztemp) % 60;
                return zHour.ToString().PadLeft(2, '0') + ":" + zMinus.ToString().PadLeft(2, '0');
            }
            return string.Empty;
        }

        #region[In]
        private void LoadInfo_Report()
        {
            //Ngày lập
            dte_RpDate.Value = SessionUser.Date_Work;
            //Tên báo cáo
            txt_RpTitle.Text = "THỐNG KÊ THỜI GIAN LÀM VIỆC TẠI XƯỞNG TRONG THÁNG";
            //Người lập
            txt_RpNguoiLap.Text = SessionUser.UserLogin.EmployeeName;
            //Kế toán trưởng
            txt_RpKeToan.Text = "Trần Tấn Long Thạch";
            //HCNS
            txt_RpHCNS.Text = "Lê Văn Hòa";
            //Tổng giám đốc
            txt_RpGiamDoc.Text = "Nguyễn Vũ Lộc";
        }
        private void Btn_Print_Click(object sender, EventArgs e)
        {
            if (GVData.Rows.Count > 0)
            {
                Panel_Done.Visible = true;
            }
            else
            {
                Utils.TNMessageBoxOK("Không tìm thấy dữ liệu", 1);
            }
        }
        private void Btn_Done_Click(object sender, EventArgs e)
        {
            Panel_Done.Visible = false;

            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "THỐNG KÊ THỜI GIAN LÀM VIỆC TẠI XƯỞNG TRONG THÁNG.xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }
                    XuatExcelMau(newFile);
                    Process.Start(Path);
                }

            }
        }
        private void BtnClose_Panel_Message_Click(object sender, EventArgs e)
        {
            Panel_Done.Visible = false;
        }

        private void XuatExcelMau(FileInfo newFile)
        {
            string TieuDe = txt_RpTitle.Text;
            string Font = "Time New Roman";
            string LapBang = txt_RpNguoiLap.Text;
            string KeToan = txt_RpKeToan.Text;
            string NhanSu = txt_RpHCNS.Text;
            string GiamDoc = txt_RpGiamDoc.Text;

            var fileMau = new FileInfo(Application.StartupPath + @"\FileTemplate\MauBaoCao_So1.xlsx");
            using (ExcelPackage xlPackage = new ExcelPackage(fileMau))
            {
                ExcelWorksheet workSheet = xlPackage.Workbook.Worksheets.FirstOrDefault();

                //tham số chỉ định tùy chọn
                int ColumnGV = GVData.Cols.Count;  //tổng cột cần hiển thị của Gidview
                int RowGV = GVData.Rows.Count; //số dòng dữ liệu Gidview
                int RowExcel = 6;  //dòng bắt đầu cần ghi dữ liệu trong excel         

                #region 2. DATA DỮ LIỆU                
                for (int i = 1; i <= RowGV; i++)
                {
                    int rowGV = i - 1;
                    for (int c = 1; c <= ColumnGV; c++)
                    {
                        int colGV = c - 1;

                        object val = GVData.Rows[rowGV][colGV];
                        if (val != null)
                        {
                            workSheet.Cells[RowExcel + i, c].Value = val.ToString();

                        }
                        else
                        {
                            workSheet.Cells[RowExcel + i, c].Value = string.Empty;
                        }
                        if (rowGV > 1 && GVData.Rows[rowGV][0] == null)
                        {
                            workSheet.Cells[RowExcel + i, c].Style.Font.Color.SetColor(Color.Navy);
                            workSheet.Cells[RowExcel + i, c].Style.Font.Bold = true;
                        }
                        if (c == 2 || c == 3 || c == 4 || c == 5 || c == 6)
                        { // cot ten nhan vien
                            workSheet.Cells[RowExcel + i, c].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        }
                        else
                        {
                            workSheet.Cells[RowExcel + i, c].Style.Numberformat.Format = "0.00";
                            workSheet.Cells[RowExcel + i, c].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        }
                    }
                }
                #endregion
                #region 3. ĐỊNH DẠNG  DATA EXCEL
                //DÒNG THỨ 7 TRONG EXCEL ĐẾN DÒNG 11 TRONG EXCEL LÀ TIÊU ĐỀ BẢNG
                for (int r = 7; r <= 8; r++)
                {
                    for (int c = 1; c <= ColumnGV; c++)
                    {
                        int colGV = c - 1;
                        workSheet.Cells[r, c].Style.Font.Name = Font;
                        workSheet.Cells[r, c].Style.Font.Size = 12;
                        workSheet.Cells[r, c].Style.Font.Bold = true;
                        workSheet.Cells[r, c].Style.Font.Color.SetColor(Color.Navy);
                        workSheet.Cells[r, c].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        workSheet.Cells[r, c].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        workSheet.Cells[r, c].Style.WrapText = true;
                        //workSheet.Column(c).Width = 15;
                        workSheet.Column(c).AutoFit();

                    }
                }

                //DÒNG TỔNG FOR TỪNG CỘT
                for (int c = 1; c <= ColumnGV; c++)
                {
                    int colGV = c - 1;

                    workSheet.Cells[RowExcel + RowGV, c].Style.Font.Name = Font;
                    workSheet.Cells[RowExcel + RowGV, c].Style.Font.Size = 12;
                    workSheet.Cells[RowExcel + RowGV, c].Style.Font.Bold = true;
                    workSheet.Cells[RowExcel + RowGV, c].Style.Font.Color.SetColor(Color.Navy);
                    workSheet.Cells[RowExcel + RowGV, c].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    workSheet.Cells[RowExcel + RowGV, c].Style.WrapText = true;

                    if (c == 2 || c == 3 || c == 4 || c == 5)
                    {
                        workSheet.Cells[RowExcel + RowGV, c].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    }
                }

                //CỘT TỔNG
                workSheet.Column(ColumnGV - 3).Style.Font.Bold = true;
                workSheet.Column(ColumnGV - 2).Style.Font.Bold = true;
                workSheet.Column(ColumnGV - 1).Style.Font.Bold = true;
                workSheet.Column(ColumnGV).Style.Font.Bold = true;

                #endregion

                #region[4.Trộn ngang > dọc tự động tiêu đề column báo cáo]       
                MergeAutoExcel(workSheet, 7, 8, 1, ColumnGV);
                workSheet.Row(7).Height = 65; //Độ cao tiêu đề
                workSheet.Row(8).Height = 100; // Độ cao tiêu đề

                //Trộn tuy chình
                //workSheet.Cells[8, ColumnGV, 10, ColumnGV].Merge = true;
                //workSheet.Cells[8, ColumnGV - 1, 10, ColumnGV - 1].Merge = true;
                //workSheet.Cells[8, ColumnGV - 2, 10, ColumnGV - 2].Merge = true;
                #endregion

                #region 1. TÊN BÁO CÁO EXCEL
                //Thêm các dường line excel
                workSheet.Cells.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                workSheet.Cells.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                workSheet.Cells.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                workSheet.Cells.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                // Dòng Tên báo cáo
                workSheet.Cells[4, 7].Value = TieuDe.ToUpper();
                workSheet.Cells[4, 7].Style.Font.Size = 16;
                workSheet.Cells[4, 7].Style.Font.Name = Font;
                workSheet.Cells[4, 7].Style.Font.Bold = true;
                workSheet.Cells[4, 7].Style.Font.Color.SetColor(Color.Navy);
                workSheet.Cells[4, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                // Xóa các đường line Từ dòng 1 đến dòng tiêu đề
                workSheet.Cells["1:6"].Style.Border.Left.Style = ExcelBorderStyle.None;
                workSheet.Cells["1:6"].Style.Border.Right.Style = ExcelBorderStyle.None;
                workSheet.Cells["1:6"].Style.Border.Top.Style = ExcelBorderStyle.None;
                workSheet.Cells["1:5"].Style.Border.Bottom.Style = ExcelBorderStyle.None;
                #endregion

                #region[6.Định dạng màu,độ rộng các cột fit]



                workSheet.Row(7).Height = 45;
                workSheet.Row(8).Height = 65;

                workSheet.Column(1).Width = 7;
                workSheet.Column(1).Style.Font.Color.SetColor(Color.Navy);
                workSheet.Column(2).Style.Font.Color.SetColor(Color.Navy);
                workSheet.Column(3).Style.Font.Color.SetColor(Color.Navy);
                workSheet.Column(4).Style.Font.Color.SetColor(Color.Navy);
                workSheet.Column(5).Style.Font.Color.SetColor(Color.Navy);

                workSheet.View.FreezePanes(10, 4);
                #endregion

                #region[7.Khung chữ ký]
                // Ngày ký
                workSheet.Cells[RowExcel + RowGV + 2, 13].Value = "Ngày " + dte_RpDate.Value.Day + " tháng " + dte_RpDate.Value.Month + " năm " + dte_RpDate.Value.Year;
                workSheet.Cells[RowExcel + RowGV + 2, 13].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 2, 13].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 2, 13].Style.Font.Color.SetColor(Color.Navy);

                //Tiêu đề Người lập bảng
                workSheet.Cells[RowExcel + RowGV + 3, 2].Value = "Lập bảng";
                workSheet.Cells[RowExcel + RowGV + 3, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 3, 2].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 3, 2].Style.Font.Color.SetColor(Color.Black);
                // Tên Người lập bảng
                workSheet.Cells[RowExcel + RowGV + 9, 2].Value = LapBang;
                workSheet.Cells[RowExcel + RowGV + 9, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 9, 2].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 9, 2].Style.Font.Color.SetColor(Color.Black);

                //Tiêu đề Kế toán trưởng
                workSheet.Cells[RowExcel + RowGV + 3, 5].Value = "Kế toán trưởng";
                workSheet.Cells[RowExcel + RowGV + 3, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 3, 5].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 3, 5].Style.Font.Color.SetColor(Color.Black);

                //Tên kế toán trưởng
                workSheet.Cells[RowExcel + RowGV + 9, 5].Value = KeToan;
                workSheet.Cells[RowExcel + RowGV + 9, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 9, 5].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 9, 5].Style.Font.Color.SetColor(Color.Black);

                //Tiêu đề BP HCNS
                workSheet.Cells[RowExcel + RowGV + 3, 9].Value = "BP HCNS";
                workSheet.Cells[RowExcel + RowGV + 3, 9].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 3, 9].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 3, 9].Style.Font.Color.SetColor(Color.Black);
                //Tên Trưởng phòng HCNS
                workSheet.Cells[RowExcel + RowGV + 9, 9].Value = NhanSu;
                workSheet.Cells[RowExcel + RowGV + 9, 9].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 9, 9].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 9, 9].Style.Font.Color.SetColor(Color.Black);

                //Tiêu đề Tổng giám đốc
                workSheet.Cells[RowExcel + RowGV + 3, 13].Value = "Tổng giám đốc";
                workSheet.Cells[RowExcel + RowGV + 3, 13].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 3, 13].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 3, 13].Style.Font.Color.SetColor(Color.Black);
                //Tên tổng giám đốc
                workSheet.Cells[RowExcel + RowGV + 9, 13].Value = GiamDoc;
                workSheet.Cells[RowExcel + RowGV + 9, 13].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 9, 13].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 9, 13].Style.Font.Color.SetColor(Color.Black);

                //Xóa đường line các dòng ký tên
                workSheet.Cells[(RowExcel + RowGV + 1) + ":" + (RowExcel + RowGV + 10)].Style.Border.Left.Style = ExcelBorderStyle.None;
                workSheet.Cells[(RowExcel + RowGV + 1) + ":" + (RowExcel + RowGV + 10)].Style.Border.Right.Style = ExcelBorderStyle.None;
                workSheet.Cells[(RowExcel + RowGV + 1) + ":" + (RowExcel + RowGV + 10)].Style.Border.Top.Style = ExcelBorderStyle.None;
                workSheet.Cells[(RowExcel + RowGV + 1) + ":" + (RowExcel + RowGV + 10)].Style.Border.Bottom.Style = ExcelBorderStyle.None;
                #endregion

                #region[8.Tiện ích Cài đặt in]
                workSheet.PrinterSettings.PaperSize = ePaperSize.A3;//Khổ giấy
                workSheet.PrinterSettings.PrintArea = workSheet.Cells[1, 1, RowExcel + RowGV + 9, ColumnGV];//Khu vực in
                workSheet.PrinterSettings.PageOrder = ePageOrder.OverThenDown;//Kiểu in chữ Z
                workSheet.PrinterSettings.RepeatRows = workSheet.Cells["7:10"];// Tiêu đề lặp lại khi qua trang khác
                //workSheet.PrinterSettings.RepeatColumns = workSheet.Cells["A:B"];// Column lặp lại khi qua trang khác
                workSheet.PrinterSettings.Orientation = eOrientation.Landscape;//Trang giấy nằm ngang
                workSheet.PrinterSettings.FitToPage = false;//In nhiu trang giấy
                workSheet.View.PageBreakView = true;
                workSheet.PrinterSettings.FitToHeight = 0;//Tự động cắt trang
                #endregion

                xlPackage.SaveAs(newFile);
            }
        }
        //trộn dòng cố định
        private void MergeDongExcel(ExcelWorksheet workSheet, int currentRow, int fromCol, int ToCol)
        {
            try
            {
                workSheet.Cells[currentRow, fromCol, currentRow, ToCol].Merge = true;
            }
            catch (Exception)
            {
                workSheet.Cells[currentRow, fromCol + 1, currentRow, ToCol].Merge = true;
            }
        }
        //trộn cột cố định
        private void MergeCotExcel(ExcelWorksheet workSheet, int currentColumn, int fromRow, int toRow)
        {
            try
            {
                workSheet.Cells[fromRow, currentColumn, toRow, currentColumn].Merge = true;
            }
            catch (Exception)
            {
                workSheet.Cells[fromRow + 1, currentColumn, toRow, currentColumn].Merge = true;
            }
        }
        //cấp 3 trộn kết hợp
        private void MergeAutoExcel(ExcelWorksheet workSheet, int fromRow, int toRow, int fromCol, int toCol)
        {
            string Val = workSheet.Cells[fromRow, fromCol].Value.ToString();
            int TronCot = 0;

            #region [Trộn cột :(dòng trên dòng dưới) ]

            for (int r = fromRow; r < toRow; r++)
            {
                for (int c = fromCol + 1; c < toCol; c++)
                {
                    string Dta = workSheet.Cells[r, c].Value.ToString();

                    if (Val == Dta)
                    {
                        TronCot++;
                    }
                    else
                    {
                        if (TronCot >= 1)
                        {
                            try
                            {
                                workSheet.Cells[r, c - TronCot - 1, r, c - 1].Merge = true;
                                TronCot = 0;
                            }
                            catch (Exception)
                            {

                            }
                        }
                        Val = Dta;
                    }
                }

                //trộn cuối cùng 
                if (Val == workSheet.Cells[r, toCol, r, toCol].Value.ToString())
                {
                    try
                    {
                        workSheet.Cells[r, toCol - TronCot - 1, r, toCol].Merge = true;
                        TronCot = 0;
                    }
                    catch (Exception)
                    {

                    }
                }
                else if (TronCot >= 1)
                {
                    try
                    {
                        workSheet.Cells[r, toCol - TronCot - 1, r, toCol - 1].Merge = true;
                        TronCot = 0;
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {

                }
            }
            #endregion

            #region [Trộn dòng :(cột trái cột phải)]

            for (int c = fromCol; c <= toCol; c++)
            {
                int TronDong = 0;
                Val = workSheet.Cells[fromRow, c].Value.ToString();
                for (int r = fromRow + 1; r <= toRow; r++)
                {
                    string Dta = workSheet.Cells[r, c].Value.ToString();
                    if (Val == Dta)
                    {
                        TronDong++;
                    }
                    else
                    {
                        Val = Dta;
                    }
                }

                if (TronDong > 0)
                {
                    try
                    {
                        workSheet.Cells[fromRow, c, fromRow + TronDong, c].Merge = true;
                        TronDong = 0;
                    }
                    catch (Exception)
                    {

                    }
                }
                if (c == toCol && Val == workSheet.Cells[fromRow, c].Value.ToString())
                {
                    try
                    {
                        workSheet.Cells[toRow - TronDong, toCol, toRow, toCol].Merge = true;
                        TronDong = 0;
                    }
                    catch (Exception)
                    {

                    }
                }

            }
            #endregion
        }
        #endregion

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                //this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
                btn_Export.Enabled = false;
            }
        }
        #endregion

    }
}
