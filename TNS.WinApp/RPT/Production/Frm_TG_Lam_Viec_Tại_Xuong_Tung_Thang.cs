﻿using C1.Win.C1FlexGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.LOG;
using TNS.Misc;
using TNS.RPT;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_TG_Lam_Viec_Tại_Xuong_Tung_Thang : Form
    {
        private int _DepartmentKey = 0;
        private int _TeamKey = 0;
        private DateTime _FromDate;
        private DateTime _ToDate;
        private string _Search = "";
        private int _STT = 0;
        public Frm_TG_Lam_Viec_Tại_Xuong_Tung_Thang()
        {
            InitializeComponent();
            btnClose.Click += btnClose_Click;
            btnMax.Click += btnMax_Click;
            btnMini.Click += btnMini_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            btn_Export.Click += btn_Export_Click;
            btn_Search.Click += Btn_Search_Click;
            cbo_Department.SelectedIndexChanged += Cbo_Department_SelectedIndexChanged;

            rdo_Department.CheckedChanged += Rdo_Department_CheckedChanged; ;
            rdo_Team.CheckedChanged += Rdo_Team_CheckedChanged;
            rdo_Worker.CheckedChanged += Rdo_Worker_CheckedChanged;

            DateTime ViewDate = SessionUser.Date_Work;
            DateTime FromDate = new DateTime(ViewDate.Year, 1, 1, 0, 0, 0);
            DateTime ToDate = FromDate.AddYears(1).AddDays(-1);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            dte_FromDate.Value = FromDate;
            dte_ToDate.Value = ToDate;
        }

        private void Frm_TG_Lam_Viec_Tại_Xuong_Tung_Thang_Load(object sender, EventArgs e)
        {
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            Check_RoleForm();

            LoadDataToToolbox.KryptonComboBox(cbo_Department, "SELECT DepartmentKey, DepartmentName FROM[dbo].[SYS_Department] WHERE BranchKey = 4  AND RecordStatus< 99", "---- Chọn tất cả----");
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
        }
        private void Rdo_Department_CheckedChanged(object sender, EventArgs e)
        {
            if (rdo_Department.Checked == true)
            {
                cbo_Team.Enabled = false;
                txt_Search.Enabled = false;
            }
        }
        private void Rdo_Team_CheckedChanged(object sender, EventArgs e)
        {
            if (rdo_Team.Checked == true)
            {
                cbo_Department.Enabled = true;
                cbo_Team.Enabled = true;
                txt_Search.Enabled = false;
            }
        }
        private void Rdo_Worker_CheckedChanged(object sender, EventArgs e)
        {
            cbo_Department.Enabled = true;
            cbo_Team.Enabled = true;
            txt_Search.Enabled = true;
        }

        private void Cbo_Department_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDataToToolbox.KryptonComboBox(cbo_Team, "SELECT TeamKey,TeamName FROM SYS_Team WHERE TeamKey != 98 AND DepartmentKey = " + cbo_Department.SelectedValue.ToInt() + "  AND RecordStatus < 99", "---- Chọn tất cả ----");
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            _DepartmentKey = cbo_Department.SelectedValue.ToInt();
            _TeamKey = cbo_Team.SelectedValue.ToInt();
            _FromDate = dte_FromDate.Value;
            _ToDate = dte_ToDate.Value;
            _Search = txt_Search.Text.Trim();
            if (dte_ToDate.Value.Year != dte_FromDate.Value.Year)
            {
                Utils.TNMessageBoxOK("Vui lòng chỉ chọn trong 1 năm !",1);
                return;
            }

            if (rdo_Department.Checked)
                using (Frm_Loading frm = new Frm_Loading(DisplayData_BoPhan)) { frm.ShowDialog(this); }
            if (rdo_Team.Checked)
                using (Frm_Loading frm = new Frm_Loading(DisplayData_Nhom)) { frm.ShowDialog(this); }
            if (rdo_Worker.Checked)
                using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }
        }

        #region[Xem theo nhan sự]
        private void DisplayData()
        {
            DataTable zTable = new DataTable();
            if(rdo_NoRate.Checked==true)
            zTable = Report_TimeKeeping.TG_LamTaiXuong_TheoThang_TheoCongNhan(_FromDate, _ToDate, _DepartmentKey, _TeamKey, _Search);
            if(rdo_Rate.Checked==true)
                zTable = Report_TimeKeeping.TG_LamTaiXuong_TheoThang_TheoCongNhan_DaNhanHeSo(_FromDate, _ToDate, _DepartmentKey, _TeamKey, _Search);
            if (zTable.Rows.Count > 0)
            {
                PivotTable zPvt = new PivotTable(zTable);
                DataTable zTablePivot = zPvt.Generate_ASC("HeaderColumn", "LeftColumn", new string[] { "TotalTime", "OverTime", "DifferentTime" }, "HeaderColumn", "TỔNG CỘNG", "TỔNG CỘNG", new string[] { "Tổng giờ", "Giờ dư", "Giờ công việc khác" }, 0);
                //Tách cột Leftcolumn
                zTablePivot.Columns.Add("EmployeeKey").SetOrdinal(1);
                zTablePivot.Columns.Add("EmpoyeeName").SetOrdinal(2);
                zTablePivot.Columns.Add("EmpoyeeID").SetOrdinal(3);
                zTablePivot.Columns.Add("PositionName").SetOrdinal(4);
                zTablePivot.Columns.Add("TeamKey", typeof(int)).SetOrdinal(5);
                zTablePivot.Columns.Add("TeamName").SetOrdinal(6);
                zTablePivot.Columns.Add("EmployeeRank");
                zTablePivot.Columns.Add("DepartmentRank");
                zTablePivot.Columns.Add("TeamRank");
                for (int i = 0; i < zTablePivot.Rows.Count - 1; i++)
                {
                    string[] temp = zTablePivot.Rows[i][0].ToString().Split('|');
                    zTablePivot.Rows[i]["EmployeeKey"] = temp[0];
                    zTablePivot.Rows[i]["EmpoyeeName"] = temp[1];
                    zTablePivot.Rows[i]["EmpoyeeID"] = temp[2];
                    zTablePivot.Rows[i]["PositionName"] = temp[3];
                    zTablePivot.Rows[i]["TeamKey"] = temp[4];
                    zTablePivot.Rows[i]["TeamName"] = temp[5];
                    zTablePivot.Rows[i]["EmployeeRank"] = ConvertIDRank(temp[2]);
                    zTablePivot.Rows[i]["DepartmentRank"] = temp[6];
                    zTablePivot.Rows[i]["TeamRank"] = temp[7];
                }
                //Xóa cột đầu đã cắt chuỗi và xóa dòng tổng cuối
                zTablePivot.Columns.Remove("HeaderColumn");
                DataRow row = zTablePivot.Rows[zTablePivot.Rows.Count - 1];
                zTablePivot.Rows.Remove(row);
                //Sắp xếp nhân viên
                DataView dv = zTablePivot.DefaultView;
                dv.Sort = " DepartmentRank ASC,TeamRank ASC,  EmployeeRank ASC";
                zTablePivot = dv.ToTable();
                zTablePivot.Columns.Remove("EmployeeRank");
                zTablePivot.Columns.Remove("DepartmentRank");
                zTablePivot.Columns.Remove("TeamRank");




                this.Invoke(new MethodInvoker(delegate ()
                {
                    InitGVProduct(zTablePivot);
                    DataTable _Intable = zTablePivot;
                    _STT = 1;
                    int NoGroup = 3;
                    int RowTam = NoGroup;
                    Row zGvRow;
                    int TeamKey = _Intable.Rows[0]["TeamKey"].ToInt();
                    string TeamName = _Intable.Rows[0]["TeamName"].ToString();
                    DataRow[] Array = _Intable.Select("TeamKey=" + TeamKey);
                    if (Array.Length > 0)
                    {
                        DataTable zGroup = Array.CopyToDataTable();
                        GVData.Rows.Add();
                        zGvRow = GVData.Rows[2];
                        HeaderRow(zGvRow, zGroup, TeamKey, TeamName, 0);
                    }
                    for (int i = 0; i < _Intable.Rows.Count; i++)
                    {
                        DataRow r = _Intable.Rows[i];
                        if (TeamKey != r["TeamKey"].ToInt())
                        {
                            #region [GROUP]
                            TeamKey = r["TeamKey"].ToInt();
                            TeamName = r["TeamName"].ToString();
                            Array = _Intable.Select("TeamKey=" + TeamKey);
                            _STT = 1;
                            if (Array.Length > 0)
                            {
                                DataTable zGroup = Array.CopyToDataTable();
                                GVData.Rows.Add();
                                zGvRow = GVData.Rows[i + NoGroup];
                                HeaderRow(zGvRow, zGroup, TeamKey, TeamName, _STT);
                            }
                            #endregion
                            NoGroup++;
                        }
                        GVData.Rows.Add();
                        zGvRow = GVData.Rows[i + NoGroup];
                        DetailRow(zGvRow, r, _STT);
                        RowTam = i + NoGroup;
                        _STT++;
                    }
                    GVData.Rows.Add();
                    zGvRow = GVData.Rows[RowTam + 1];
                    TotalRow(zGvRow, _Intable, RowTam + 1);
                }));
            }
            else
            {
                GVData.Rows.Count = 0;
                GVData.Cols.Count = 0;
            }
        }
        private void HeaderRow(Row RowView, DataTable Table, int TeamKey, string TeamName, int No)
        {
            RowView[0] = "";
            RowView[1] = TeamName;
            RowView[2] = "Số nhân sự " + Table.Select("TeamKey='" + TeamKey + "'").Length;
            RowView[3] = "";
            int nCol = 4;
            double zTotal = 0;
            for (int i = 6; i < Table.Columns.Count - 1; i++)
            {
                double zTemp = 0;
                for (int k = 0; k < Table.Rows.Count; k++)
                {
                    double TotalRow = 0;
                    if (double.TryParse(Table.Rows[k][i].ToString(), out TotalRow))
                    {
                        zTemp += TotalRow;
                    }

                }
                RowView[nCol] = SoGio(zTemp.ToString());
                zTotal += zTemp;
                nCol++;
            }

            RowView.StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
        }
        private void DetailRow(Row RowView, DataRow rDetail, int No)
        {
            RowView[0] = No;
            RowView[1] = rDetail[1].ToString().Trim();//họ tên
            RowView[2] = rDetail[3].ToString().Trim();//chức vụ
            RowView[3] = rDetail[2].ToString().Trim();//mã thẻ
            int nCol = 4;
            for (int i = 6; i < rDetail.ItemArray.Length - 1; i++)
            {
                RowView[nCol] = SoGio(rDetail[i].ToString());
                nCol++;
            }
        }
        private void TotalRow(Row RowView, DataTable Table, int No)
        {
            RowView[0] = "";
            RowView[1] = "TỔNG CỘNG ";
            RowView[2] = "Số nhân sự " + Table.Rows.Count;
            RowView[3] = "";

            //float zTotal = 0;
            int nCol = 4;
            for (int i = 6; i < Table.Columns.Count - 1; i++)
            {
                double zTemp = 0;
                for (int k = 0; k < Table.Rows.Count; k++)
                {
                    double TotalRow = 0;
                    if (double.TryParse(Table.Rows[k][i].ToString(), out TotalRow))
                    {
                        zTemp += TotalRow;
                    }

                }
                RowView[nCol] = SoGio(zTemp.ToString());
                nCol++;
            }
            RowView.StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
        }
        void InitGVProduct(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();

            int TotalRow = 2;
            int ToTalCol = TableView.Columns.Count - 2;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(TotalRow);

            CellRange zCellRange = GVData.GetCellRange(0, 0, 1, 0);
            zCellRange.Data = "STT";

            zCellRange = GVData.GetCellRange(0, 1, 1, 1);
            zCellRange.Data = "HỌ TÊN";

            zCellRange = GVData.GetCellRange(0, 2, 1, 2);
            zCellRange.Data = "CHỨC VỤ";

            zCellRange = GVData.GetCellRange(0, 3, 1, 3);
            zCellRange.Data = "SỐ THẺ";

            int ColStart = 4;
            // header
            for (int i = 6; i < TableView.Columns.Count; i++)
            {
                DataColumn Col = TableView.Columns[i];
                string[] strCaption = Col.ColumnName.Split('|');
                if (i < TableView.Columns.Count-3)
                {
                    GVData.Rows[0][ColStart] = "THÁNG "+strCaption[0];
                    GVData.Rows[1][ColStart] = strCaption[1];
                }
                else
                {
                    GVData.Rows[0][ColStart] =strCaption[0];
                    GVData.Rows[1][ColStart] = strCaption[1];
                }
                ColStart++;
            }



            ////Style
            //GVData.AllowFreezing = AllowFreezingEnum.Both;
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.FixedOnly;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.CenterCenter;
            GVData.Styles.Normal.WordWrap = true;



            //Trộn cột 
            for (int i = 0; i < GVData.Cols.Count; i++)
            {
                GVData.Cols[i].AllowMerging = true;
            }
            //Trộn dòng 0
            GVData.Rows[0].AllowMerging = true;
            GVData.Rows[0].Height = 50;
            GVData.Rows[1].Height = 60;
            //Trộn dòng 1
            //Freeze Row and Column
            GVData.Rows.Fixed = 2;
            GVData.Cols.Frozen = 4;

            GVData.Cols[0].Width = 40;
            GVData.Cols[1].Width = 200;
            GVData.Cols[2].Width = 150;
            GVData.Cols[3].Width = 100;
            //GVData.AutoSizeCols(1, GVData.Cols.Count - 1, 10);
            //Canh phải các cột từ số 1 trở đi
            GVData.Cols[0].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[1].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[2].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[3].TextAlign = TextAlignEnum.LeftCenter;

            GVData.Cols[0].StyleNew.BackColor = Color.Empty;
            GVData.Cols[1].StyleNew.BackColor = Color.Empty;
            GVData.Cols[2].StyleNew.BackColor = Color.Empty;
            GVData.Cols[3].StyleNew.BackColor = Color.Empty;

            for (int i = 4; i < GVData.Cols.Count; i++)
            {
                GVData.Cols[i].Width = 75;
                GVData.Cols[i].TextAlign = TextAlignEnum.RightCenter;
            }

            //In đậm các dòng, cột tổng
            //GVData.Rows[TotalRow - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[ToTalCol - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[ToTalCol - 2].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[ToTalCol - 3].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);

            GVData.Cols[ToTalCol - 1].Width = 100;
            GVData.Cols[ToTalCol - 2].Width = 100;
            GVData.Cols[ToTalCol - 3].Width = 100;

            //GVData.Cols[ToTalCol - 1].StyleNew.BackColor = Color.LightGreen;
            // tô màu cột tổng
            //GVData.Rows[TotalRow - 1].StyleNew.BackColor = Color.LightGreen;

        }
        #endregion

        #region[Xem theo nhóm]
        private void DisplayData_Nhom()
        {
            DataTable zTable = new DataTable();
            if(rdo_NoRate.Checked==true)
            zTable = Report_TimeKeeping.TG_LamTaiXuong_TheoThang_TheoNhom(_FromDate, _ToDate, _DepartmentKey, _TeamKey);
            if(rdo_Rate.Checked==true)
                zTable = Report_TimeKeeping.TG_LamTaiXuong_TheoThang_TheoNhom_DaNhanHeSo(_FromDate, _ToDate, _DepartmentKey, _TeamKey);
            if (zTable.Rows.Count > 0)
            {
                PivotTable zPvt = new PivotTable(zTable);
                DataTable zTablePivot = zPvt.Generate_ASC("HeaderColumn", "LeftColumn", new string[] { "TotalTime", "OverTime", "DifferentTime" }, "HeaderColumn", "TỔNG CỘNG", "TỔNG CỘNG", new string[] { "Tổng giờ", "Giờ dư", "Giờ công việc khác" }, 0);
                //Tách cột Leftcolumn
                zTablePivot.Columns.Add("TeamKey").SetOrdinal(1);
                zTablePivot.Columns.Add("TeamName").SetOrdinal(2);
                zTablePivot.Columns.Add("TeamID").SetOrdinal(3);
                zTablePivot.Columns.Add("DepartmentKey", typeof(int)).SetOrdinal(4);
                zTablePivot.Columns.Add("DepartmentName").SetOrdinal(5);
                zTablePivot.Columns.Add("DepartmentRank");
                zTablePivot.Columns.Add("TeamRank");
                for (int i = 0; i < zTablePivot.Rows.Count - 1; i++)
                {
                    string[] temp = zTablePivot.Rows[i][0].ToString().Split('|');
                    zTablePivot.Rows[i]["TeamKey"] = temp[0];
                    zTablePivot.Rows[i]["TeamName"] = temp[1];
                    zTablePivot.Rows[i]["TeamID"] = temp[2];
                    zTablePivot.Rows[i]["DepartmentKey"] = temp[3];
                    zTablePivot.Rows[i]["DepartmentName"] = temp[4];
                    zTablePivot.Rows[i]["DepartmentRank"] = temp[5];
                    zTablePivot.Rows[i]["TeamRank"] = temp[6];
                }
                //Xóa cột đầu đã cắt chuỗi và xóa dòng tổng cuối
                zTablePivot.Columns.Remove("HeaderColumn");
                DataRow row = zTablePivot.Rows[zTablePivot.Rows.Count - 1];
                zTablePivot.Rows.Remove(row);
                //Sắp xếp nhân viên
                DataView dv = zTablePivot.DefaultView;
                dv.Sort = " DepartmentRank ASC,TeamRank ASC";
                zTablePivot = dv.ToTable();
                zTablePivot.Columns.Remove("DepartmentRank");
                zTablePivot.Columns.Remove("TeamRank");




                this.Invoke(new MethodInvoker(delegate ()
                {
                    InitGVProduct_Nhom(zTablePivot);
                    DataTable _Intable = zTablePivot;
                    _STT = 1;
                    int NoGroup = 3;
                    int RowTam = NoGroup;
                    Row zGvRow;
                    int DepartmentKey = _Intable.Rows[0]["DepartmentKey"].ToInt();
                    string DepartmentName = _Intable.Rows[0]["DepartmentName"].ToString();
                    DataRow[] Array = _Intable.Select("DepartmentKey=" + DepartmentKey);
                    if (Array.Length > 0)
                    {
                        DataTable zGroup = Array.CopyToDataTable();
                        GVData.Rows.Add();
                        zGvRow = GVData.Rows[2];
                        HeaderRow_Nhom(zGvRow, zGroup, DepartmentKey, DepartmentName, 0);
                    }
                    for (int i = 0; i < _Intable.Rows.Count; i++)
                    {
                        DataRow r = _Intable.Rows[i];
                        if (DepartmentKey != r["DepartmentKey"].ToInt())
                        {
                            #region [GROUP]
                            DepartmentKey = r["DepartmentKey"].ToInt();
                            DepartmentName = r["DepartmentName"].ToString();
                            Array = _Intable.Select("DepartmentKey=" + DepartmentKey);
                            _STT = 1;
                            if (Array.Length > 0)
                            {
                                DataTable zGroup = Array.CopyToDataTable();
                                GVData.Rows.Add();
                                zGvRow = GVData.Rows[i + NoGroup];
                                HeaderRow_Nhom(zGvRow, zGroup, DepartmentKey, DepartmentName, _STT);
                            }
                            #endregion
                            NoGroup++;
                        }
                        GVData.Rows.Add();
                        zGvRow = GVData.Rows[i + NoGroup];
                        DetailRow_Nhom(zGvRow, r, _STT);
                        RowTam = i + NoGroup;
                        _STT++;
                    }
                    GVData.Rows.Add();
                    zGvRow = GVData.Rows[RowTam + 1];
                    TotalRow_Nhom(zGvRow, _Intable, RowTam + 1);
                }));
            }
            else
            {
                GVData.Rows.Count = 0;
                GVData.Cols.Count = 0;
            }
        }
        private void HeaderRow_Nhom(Row RowView, DataTable Table, int DepartmentKey, string DepartmentName, int No)
        {
            RowView[0] = "";
            RowView[1] = DepartmentName;
            RowView[2] = "Số tổ nhóm " + Table.Select("DepartmentKey='" + DepartmentKey + "'").Length;
            RowView[3] = "";
            int nCol = 3;
            double zTotal = 0;
            for (int i = 5; i < Table.Columns.Count - 1; i++)
            {
                double zTemp = 0;
                for (int k = 0; k < Table.Rows.Count; k++)
                {
                    double TotalRow = 0;
                    if (double.TryParse(Table.Rows[k][i].ToString(), out TotalRow))
                    {
                        zTemp += TotalRow;
                    }

                }
                RowView[nCol] = SoGio(zTemp.ToString());
                zTotal += zTemp;
                nCol++;
            }

            RowView.StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
        }
        private void DetailRow_Nhom(Row RowView, DataRow rDetail, int No)
        {
            RowView[0] = No;
            RowView[1] = rDetail[1].ToString().Trim();// tên
            RowView[2] = rDetail[2].ToString().Trim();//mã
            int nCol = 3;
            for (int i = 5; i < rDetail.ItemArray.Length - 1; i++)
            {
                RowView[nCol] = SoGio(rDetail[i].ToString());
                nCol++;
            }
        }
        private void TotalRow_Nhom(Row RowView, DataTable Table, int No)
        {
            RowView[0] = "";
            RowView[1] = "TỔNG CỘNG ";
            RowView[2] = "Số tổ nhóm " + Table.Rows.Count;

            //float zTotal = 0;
            int nCol = 3;
            for (int i = 5; i < Table.Columns.Count - 1; i++)
            {
                double zTemp = 0;
                for (int k = 0; k < Table.Rows.Count; k++)
                {
                    double TotalRow = 0;
                    if (double.TryParse(Table.Rows[k][i].ToString(), out TotalRow))
                    {
                        zTemp += TotalRow;
                    }

                }
                RowView[nCol] = SoGio(zTemp.ToString());
                nCol++;
            }
            RowView.StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
        }
        void InitGVProduct_Nhom(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();

            int TotalRow = 2;
            int ToTalCol = TableView.Columns.Count - 2;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(TotalRow);

            CellRange zCellRange = GVData.GetCellRange(0, 0, 1, 0);
            zCellRange.Data = "STT";

            zCellRange = GVData.GetCellRange(0, 1, 1, 1);
            zCellRange.Data = "TÊN TỔ NHÓM";

            zCellRange = GVData.GetCellRange(0, 2, 1, 2);
            zCellRange.Data = "MÃ TỔ NHÓM";

            int ColStart = 3;
            // header
            for (int i = 5; i < TableView.Columns.Count; i++)
            {
                DataColumn Col = TableView.Columns[i];
                string[] strCaption = Col.ColumnName.Split('|');
                if (i < TableView.Columns.Count - 3)
                {
                    GVData.Rows[0][ColStart] = "THÁNG " + strCaption[0];
                    GVData.Rows[1][ColStart] = strCaption[1];
                }
                else
                {
                    GVData.Rows[0][ColStart] = strCaption[0];
                    GVData.Rows[1][ColStart] = strCaption[1];
                }
                ColStart++;
            }



            ////Style
            //GVData.AllowFreezing = AllowFreezingEnum.Both;
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.FixedOnly;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.CenterCenter;
            GVData.Styles.Normal.WordWrap = true;



            //Trộn cột 
            for (int i = 0; i < GVData.Cols.Count; i++)
            {
                GVData.Cols[i].AllowMerging = true;
            }
            //Trộn dòng 0
            GVData.Rows[0].AllowMerging = true;
            GVData.Rows[0].Height = 50;
            GVData.Rows[1].Height = 60;
            //Trộn dòng 1
            //Freeze Row and Column
            GVData.Rows.Fixed = 2;
            GVData.Cols.Frozen = 3;

            GVData.Cols[0].Width = 40;
            GVData.Cols[1].Width = 200;
            GVData.Cols[2].Width = 150;
            //GVData.AutoSizeCols(1, GVData.Cols.Count - 1, 10);
            //Canh phải các cột từ số 1 trở đi
            GVData.Cols[0].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[1].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[2].TextAlign = TextAlignEnum.LeftCenter;

            GVData.Cols[0].StyleNew.BackColor = Color.Empty;
            GVData.Cols[1].StyleNew.BackColor = Color.Empty;
            GVData.Cols[2].StyleNew.BackColor = Color.Empty;

            for (int i = 3; i < GVData.Cols.Count; i++)
            {
                GVData.Cols[i].Width = 95;
                GVData.Cols[i].TextAlign = TextAlignEnum.RightCenter;
            }

            //In đậm các dòng, cột tổng
            //GVData.Rows[TotalRow - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[ToTalCol - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[ToTalCol - 2].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[ToTalCol - 3].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);

            GVData.Cols[ToTalCol - 1].Width = 100;
            GVData.Cols[ToTalCol - 2].Width = 100;
            GVData.Cols[ToTalCol - 3].Width = 100;

            //GVData.Cols[ToTalCol - 1].StyleNew.BackColor = Color.LightGreen;
            // tô màu cột tổng
            //GVData.Rows[TotalRow - 1].StyleNew.BackColor = Color.LightGreen;

        }
        #endregion

        #region[Xem theo BỘ PHẬN]
        private void DisplayData_BoPhan()
        {
            DataTable zTable = new DataTable();
            if(rdo_NoRate.Checked==true)
            zTable = Report_TimeKeeping.TG_LamTaiXuong_TheoThang_TheoBoPhan(_FromDate, _ToDate, _DepartmentKey);
            if(rdo_Rate.Checked==true)
                zTable = Report_TimeKeeping.TG_LamTaiXuong_TheoThang_TheoBoPhan_DaNhanHeSo(_FromDate, _ToDate, _DepartmentKey);
            if (zTable.Rows.Count > 0)
            {
                PivotTable zPvt = new PivotTable(zTable);
                DataTable zTablePivot = zPvt.Generate_ASC("HeaderColumn", "LeftColumn", new string[] { "TotalTime", "OverTime", "DifferentTime" }, "HeaderColumn", "TỔNG CỘNG", "TỔNG CỘNG", new string[] { "Tổng giờ", "Giờ dư", "Giờ công việc khác" }, 0);
                //Tách cột Leftcolumn
                zTablePivot.Columns.Add("DepartmentKey").SetOrdinal(1);
                zTablePivot.Columns.Add("DepartmentName").SetOrdinal(2);
                zTablePivot.Columns.Add("DepartmentID").SetOrdinal(3);
                zTablePivot.Columns.Add("BranchKey", typeof(int)).SetOrdinal(4);
                zTablePivot.Columns.Add("BranchName").SetOrdinal(5);
                zTablePivot.Columns.Add("BranchRank");
                zTablePivot.Columns.Add("DepartmentRank");
                for (int i = 0; i < zTablePivot.Rows.Count - 1; i++)
                {
                    string[] temp = zTablePivot.Rows[i][0].ToString().Split('|');
                    zTablePivot.Rows[i]["DepartmentKey"] = temp[0];
                    zTablePivot.Rows[i]["DepartmentName"] = temp[1];
                    zTablePivot.Rows[i]["DepartmentID"] = temp[2];
                    zTablePivot.Rows[i]["BranchKey"] = temp[3];
                    zTablePivot.Rows[i]["BranchName"] = temp[4];
                    zTablePivot.Rows[i]["BranchRank"] = temp[5];
                    zTablePivot.Rows[i]["DepartmentRank"] = temp[6];
                }
                //Xóa cột đầu đã cắt chuỗi và xóa dòng tổng cuối
                zTablePivot.Columns.Remove("HeaderColumn");
                DataRow row = zTablePivot.Rows[zTablePivot.Rows.Count - 1];
                zTablePivot.Rows.Remove(row);
                //Sắp xếp nhân viên
                DataView dv = zTablePivot.DefaultView;
                dv.Sort = " BranchRank ASC, DepartmentRank ASC";
                zTablePivot = dv.ToTable();
                zTablePivot.Columns.Remove("BranchRank");
                zTablePivot.Columns.Remove("DepartmentRank");




                this.Invoke(new MethodInvoker(delegate ()
                {
                    InitGVProduct_BoPhan(zTablePivot);
                    DataTable _Intable = zTablePivot;
                    _STT = 1;
                    int NoGroup = 3;
                    int RowTam = NoGroup;
                    Row zGvRow;
                    int BranchKey = _Intable.Rows[0]["BranchKey"].ToInt();
                    string BranchName = _Intable.Rows[0]["BranchName"].ToString();
                    DataRow[] Array = _Intable.Select("BranchKey=" + BranchKey);
                    if (Array.Length > 0)
                    {
                        DataTable zGroup = Array.CopyToDataTable();
                        GVData.Rows.Add();
                        zGvRow = GVData.Rows[2];
                        HeaderRow_BoPhan(zGvRow, zGroup, BranchKey, BranchName, 0);
                    }
                    for (int i = 0; i < _Intable.Rows.Count; i++)
                    {
                        DataRow r = _Intable.Rows[i];
                        if (BranchKey != r["BranchKey"].ToInt())
                        {
                            #region [GROUP]
                            BranchKey = r["BranchKey"].ToInt();
                            BranchName = r["BranchName"].ToString();
                            Array = _Intable.Select("BranchKey=" + BranchKey);
                            _STT = 1;
                            if (Array.Length > 0)
                            {
                                DataTable zGroup = Array.CopyToDataTable();
                                GVData.Rows.Add();
                                zGvRow = GVData.Rows[i + NoGroup];
                                HeaderRow_BoPhan(zGvRow, zGroup, BranchKey, BranchName, _STT);
                            }
                            #endregion
                            NoGroup++;
                        }
                        GVData.Rows.Add();
                        zGvRow = GVData.Rows[i + NoGroup];
                        DetailRow_BoPhan(zGvRow, r, _STT);
                        RowTam = i + NoGroup;
                        _STT++;
                    }
                    GVData.Rows.Add();
                    zGvRow = GVData.Rows[RowTam + 1];
                    TotalRow_BoPhan(zGvRow, _Intable, RowTam + 1);
                }));
            }
            else
            {
                GVData.Rows.Count = 0;
                GVData.Cols.Count = 0;
            }
        }
        private void HeaderRow_BoPhan(Row RowView, DataTable Table, int BranchKey, string BranchName, int No)
        {
            RowView[0] = "";
            RowView[1] = BranchName;
            RowView[2] = "Số bộ phận " + Table.Select("BranchKey='" + BranchKey + "'").Length;
            RowView[3] = "";
            int nCol = 3;
            double zTotal = 0;
            for (int i = 5; i < Table.Columns.Count - 1; i++)
            {
                double zTemp = 0;
                for (int k = 0; k < Table.Rows.Count; k++)
                {
                    double TotalRow = 0;
                    if (double.TryParse(Table.Rows[k][i].ToString(), out TotalRow))
                    {
                        zTemp += TotalRow;
                    }

                }
                RowView[nCol] = SoGio(zTemp.ToString());
                zTotal += zTemp;
                nCol++;
            }

            RowView.StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
        }
        private void DetailRow_BoPhan(Row RowView, DataRow rDetail, int No)
        {
            RowView[0] = No;
            RowView[1] = rDetail[1].ToString().Trim();// tên
            RowView[2] = rDetail[2].ToString().Trim();//mã
            int nCol = 3;
            for (int i = 5; i < rDetail.ItemArray.Length - 1; i++)
            {
                RowView[nCol] = SoGio(rDetail[i].ToString());
                nCol++;
            }
        }
        private void TotalRow_BoPhan(Row RowView, DataTable Table, int No)
        {
            RowView[0] = "";
            RowView[1] = "TỔNG CỘNG ";
            RowView[2] = "Số bộ phận " + Table.Rows.Count;

            //float zTotal = 0;
            int nCol = 3;
            for (int i = 5; i < Table.Columns.Count - 1; i++)
            {
                double zTemp = 0;
                for (int k = 0; k < Table.Rows.Count; k++)
                {
                    double TotalRow = 0;
                    if (double.TryParse(Table.Rows[k][i].ToString(), out TotalRow))
                    {
                        zTemp += TotalRow;
                    }

                }
                RowView[nCol] = SoGio(zTemp.ToString());
                nCol++;
            }
            RowView.StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
        }
        void InitGVProduct_BoPhan(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();

            int TotalRow = 2;
            int ToTalCol = TableView.Columns.Count - 2;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(TotalRow);

            CellRange zCellRange = GVData.GetCellRange(0, 0, 1, 0);
            zCellRange.Data = "STT";

            zCellRange = GVData.GetCellRange(0, 1, 1, 1);
            zCellRange.Data = "TÊN BỘ PHẬN";

            zCellRange = GVData.GetCellRange(0, 2, 1, 2);
            zCellRange.Data = "MÃ BỘ PHẬN";

            int ColStart = 3;
            // header
            for (int i = 5; i < TableView.Columns.Count; i++)
            {
                DataColumn Col = TableView.Columns[i];
                string[] strCaption = Col.ColumnName.Split('|');
                if (i< TableView.Columns.Count-3)
                {
                    GVData.Rows[0][ColStart] ="THÁNG "+ strCaption[0];
                    GVData.Rows[1][ColStart] = strCaption[1];
                }
                else
                {
                    GVData.Rows[0][ColStart] = strCaption[0];
                    GVData.Rows[1][ColStart] = strCaption[1];
                }
                ColStart++;
            }



            ////Style
            //GVData.AllowFreezing = AllowFreezingEnum.Both;
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.FixedOnly;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.CenterCenter;
            GVData.Styles.Normal.WordWrap = true;



            //Trộn cột 
            for (int i = 0; i < GVData.Cols.Count; i++)
            {
                GVData.Cols[i].AllowMerging = true;
            }
            //Trộn dòng 0
            GVData.Rows[0].AllowMerging = true;
            GVData.Rows[0].Height = 50;
            GVData.Rows[1].Height = 60;
            //Trộn dòng 1
            //Freeze Row and Column
            GVData.Rows.Fixed = 2;
            GVData.Cols.Frozen = 3;

            GVData.Cols[0].Width = 40;
            GVData.Cols[1].Width = 200;
            GVData.Cols[2].Width = 150;
            //GVData.AutoSizeCols(1, GVData.Cols.Count - 1, 10);
            //Canh phải các cột từ số 1 trở đi
            GVData.Cols[0].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[1].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[2].TextAlign = TextAlignEnum.LeftCenter;

            GVData.Cols[0].StyleNew.BackColor = Color.Empty;
            GVData.Cols[1].StyleNew.BackColor = Color.Empty;
            GVData.Cols[2].StyleNew.BackColor = Color.Empty;

            for (int i = 3; i < GVData.Cols.Count; i++)
            {
                GVData.Cols[i].Width = 100;
                GVData.Cols[i].TextAlign = TextAlignEnum.RightCenter;
            }

            //In đậm các dòng, cột tổng
            //GVData.Rows[TotalRow - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[ToTalCol - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[ToTalCol - 2].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[ToTalCol - 3].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);

            GVData.Cols[ToTalCol - 1].Width = 100;
            GVData.Cols[ToTalCol - 2].Width = 100;
            GVData.Cols[ToTalCol - 3].Width = 100;

            //GVData.Cols[ToTalCol - 1].StyleNew.BackColor = Color.LightGreen;
            // tô màu cột tổng
            //GVData.Rows[TotalRow - 1].StyleNew.BackColor = Color.LightGreen;

        }
        #endregion
        //Chuyển số thành dãy chuỗi
        string ConvertIDRank(string ID)
        {
            //chèn số thấp tới cao--> tới chữ
            string zResult = "";
            string s = "";
            string temp = "";
            if (ID.Substring(0, 1) == "A")
            {
                temp = ID;
                for (int i = 0; i < 8 - ID.Trim().Length; i++)
                {
                    s += "9";
                }
                zResult = s + temp;
            }
            else if (ID.Substring(0, 1) == "H" || ID.Substring(0, 1) == "L")
            {
                temp = ID;
                for (int i = 0; i < 8 - ID.Trim().Length; i++)
                {
                    s += "B";
                }
                zResult = s + temp;
            }
            else
            {
                temp = ID;
                for (int i = 0; i < 8 - ID.Trim().Length; i++)
                {
                    s += "0";
                }
                zResult = s + temp;
            }
            return zResult;
        }
        private void btn_Export_Click(object sender, EventArgs e)
        {
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Báo_Cáo_Thời_Gian_Làm_Việc_Tại_Xưởng Từ_" + dte_FromDate.Value.ToString("MM_yyyy") + "_Đến_" + dte_ToDate.Value.ToString("MM_yyyy") + ".xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }

                    string Status = "Xuất Excel form " + HeaderControl.Text + " > đường dẫn và tên file xuất:" + FDialog.FileName;
                    Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);

                    GVData.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }

        }
        string SoGio(string TongSoPhut)
        {
            double ztemp = 0;
            if (double.TryParse(TongSoPhut.ToString(), out ztemp))
            {

            }
            //Biên hệ số giờ thành tổng số phút
            if (ztemp > 0)
            {
                int zHour = Convert.ToInt32(ztemp) / 60;
                int zMinus = Convert.ToInt32(ztemp) % 60;
                return zHour.ToString().PadLeft(2, '0') + ":" + zMinus.ToString().PadLeft(2, '0');
            }
            return string.Empty;
        }

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                //this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
                btn_Export.Enabled = false;
            }
        }
        #endregion
    }
}
