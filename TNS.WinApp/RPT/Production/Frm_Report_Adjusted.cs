﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.CORE;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Report_Adjusted : Form
    {
        private int _TeamView=0;
        public Frm_Report_Adjusted()
        {
            InitializeComponent();

            btn_Search.Click += Btn_Search_Click;
            btn_Export.Click += btn_Export_Click;

            btnClose.Click += btnClose_Click;
            btnMax.Click += btnMax_Click;
            btnMini.Click += btnMini_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            this.DoubleBuffered = true;
            Utils.DoubleBuffered(GVData, true);
            Utils.DrawGVStyle(ref GVData);
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            //Lấy tất cả nhóm trực tiếp sx  và khác bộ phận hỗ trợ sx
            string zSQL = @"SELECT TeamKey,TeamID +'-'+ TeamName AS TeamName  FROM SYS_Team 
                            WHERE RecordStatus <> 99 AND BranchKey=4 AND  DepartmentKey != 26
                            ORDER BY Rank";
            LoadDataToToolbox.KryptonComboBox(cboTeam, zSQL, "---Tất cả---");
        }

        private void Report_Adjusted_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            Check_RoleForm();

            DateTime ViewDate = SessionUser.Date_Work;
            DateTime FromDate = new DateTime(ViewDate.Year, ViewDate.Month, 1, 0, 0, 0);
            DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            dte_FromDate.Value = FromDate;
            dte_ToDate.Value = ToDate;
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            _TeamView = cboTeam.SelectedValue.ToInt();
            GVData.Rows.Clear();
            if (dte_ToDate.Value.Month != dte_FromDate.Value.Month && dte_ToDate.Value.Year !=dte_FromDate.Value.Year)
            {
                Utils.TNMessageBoxOK("Vui lòng chỉ chọn trong 1 tháng !",1);
                return;
            }
            else
            {
                try
                {
                    using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }
                }
                catch (Exception ex)
                {
                    Utils.TNMessageBox( ex.ToString(), 4);
                }
            }
        }

        private void DisplayData()
        {
            DataTable _Intable = Report.Adjusted(dte_FromDate.Value, dte_ToDate.Value, _TeamView);
            if (_Intable.Rows.Count > 0)
            {
                this.Invoke(new MethodInvoker(delegate ()
                {
                    InitGV_Layout(GVData, _Intable);

                    int NoGroup = 1;
                    DataGridViewRow zGvRow;
                    int TeamKey = _Intable.Rows[0]["TeamKey"].ToInt();
                    string TeamName = _Intable.Rows[0]["TeamName"].ToString();
                    DataRow[] Array = _Intable.Select("TeamKey=" + TeamKey);
                    if (Array.Length > 0)
                    {
                        DataTable zGroup = Array.CopyToDataTable();
                        GVData.Rows.Add();
                        zGvRow = GVData.Rows[0];
                        HeaderRow(zGvRow, zGroup, TeamKey, TeamName, 0);
                    }
                    for (int i = 0; i < _Intable.Rows.Count; i++)
                    {
                        DataRow r = _Intable.Rows[i];
                        if (TeamKey != r["TeamKey"].ToInt())
                        {
                            #region [GROUP]
                            TeamKey = r["TeamKey"].ToInt();
                            TeamName = r["TeamName"].ToString();
                            Array = _Intable.Select("TeamKey=" + TeamKey);
                            if (Array.Length > 0)
                            {
                                DataTable zGroup = Array.CopyToDataTable();
                                GVData.Rows.Add();
                                zGvRow = GVData.Rows[i + NoGroup];
                                HeaderRow(zGvRow, zGroup, TeamKey, TeamName, i + NoGroup);
                            }
                            #endregion
                            NoGroup++;
                        }
                        GVData.Rows.Add();
                        zGvRow = GVData.Rows[i + NoGroup];
                        DetailRow(zGvRow, r, i + NoGroup);
                    }

                    zGvRow = GVData.Rows[GVData.Rows.Count - 1];
                    TotalRow(zGvRow, _Intable, GVData.Rows.Count - 1);
                }));
            }
            //else
            //{
            //    GVData.Rows.Clear();
            //}
        }
        private void InitGV_Layout(DataGridView GV, DataTable Table)
        {
            GVData.Rows.Clear();
            GVData.Columns.Clear();

            // Setup Column 
            GV.Columns.Add("No", "STT");
            GV.Columns.Add("EmployeeName", "HỌ VÀ TÊN");
            GV.Columns.Add("EmployeeID", "SỐ THẺ");

            for (int i = 5; i < Table.Columns.Count; i++)
            {
                GV.Columns.Add(i.ToString(), "Ngày " + Table.Columns[i].ColumnName);
                GV.Columns[i.ToString()].Width = 100;
                GV.Columns[i.ToString()].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            }
            GV.Columns.Add("Total", "Tổng");

            #region
            GV.Columns["No"].Width = 40;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV.Columns["No"].Frozen = true;
            GV.Columns["No"].ReadOnly = true;

            GV.Columns["EmployeeName"].Width = 200;
            GV.Columns["EmployeeName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["EmployeeName"].Frozen = true;

            GV.Columns["EmployeeID"].Width = 200;
            GV.Columns["EmployeeID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["EmployeeID"].Frozen = true;

            GV.Columns["Total"].Width = 120;
            GV.Columns["Total"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV.Columns["Total"].DefaultCellStyle.Font = new Font("Arial", 9, FontStyle.Bold);
            #endregion
        }
        private void HeaderRow(DataGridViewRow RowView, DataTable Table, int TeamKey, string TeamName, int No)
        {
            RowView.Cells["No"].Value = No.ToString();
            RowView.Cells["EmployeeName"].Tag = "";
            RowView.Cells["EmployeeName"].Value = TeamName;
            RowView.Cells["EmployeeID"].Value = "Số công nhân " + Table.Select("TeamKey=" + TeamKey).Length;
            int nCol = 3;
            float zTotal = 0;
            for (int i = 5; i < Table.Columns.Count ; i++)
            {
                RowView.Cells[nCol].Value = Table.Compute("SUM([" + Table.Columns[i].ColumnName + "])", "TeamKey=" + TeamKey).Ton0String();
                float ztemp = 0;
                if (float.TryParse(RowView.Cells[nCol].Value.ToString(), out ztemp))
                {

                }
                zTotal += ztemp;
                nCol++;
            }
            RowView.Cells["Total"].Value = zTotal.Ton0String(); 
            RowView.DefaultCellStyle.Font = new Font("Tahoma", 9, FontStyle.Bold | FontStyle.Italic);
            RowView.DefaultCellStyle.BackColor = Color.LemonChiffon;
        }
        private void DetailRow(DataGridViewRow RowView, DataRow rDetail, int No)
        {
            RowView.Cells["No"].Value = (No).ToString();
            RowView.Cells["EmployeeName"].Tag = rDetail["EmployeeKey"].ToString().Trim();
            RowView.Cells["EmployeeName"].Value = rDetail["EmployeeName"].ToString().Trim();
            RowView.Cells["EmployeeID"].Value = rDetail["EmployeeID"].ToString().Trim();
            int nCol = 5;
            for (int i = 3; i < RowView.Cells.Count-1; i++)
            {
                RowView.Cells[i].Value = rDetail[nCol].Ton0String();
                nCol++;
            }
            double zTotalRow = 0;
            for (int i = 5; i < rDetail.ItemArray.Length; i++)
            {
                float ztemp = 0;
                if (float.TryParse(rDetail[i].ToString(), out ztemp))
                {

                }
                zTotalRow += ztemp;
            }

            RowView.Cells["Total"].Value = zTotalRow.Ton0String();
        }
        private void TotalRow(DataGridViewRow RowView, DataTable Table, int No)
        {
            RowView.Cells["No"].Value = (No).ToString();
            RowView.Cells["EmployeeName"].Tag = "";
            RowView.Cells["EmployeeName"].Value = "Tổng ";
            RowView.Cells["EmployeeID"].Value = "Số công nhân " + Table.Rows.Count;
            float zTotal = 0;
            for (int i = 5; i < Table.Columns.Count; i++)
            {
                RowView.Cells[i.ToString()].Value = Table.Compute("SUM([" + Table.Columns[i].ColumnName + "])", "").Ton0String();
                float ztemp = 0;
                if(float.TryParse(RowView.Cells[i.ToString()].Value.ToString(),out ztemp))
                {

                }
                zTotal += ztemp;
            }
            RowView.Cells["Total"].Value = zTotal.Ton0String(); ;
            RowView.DefaultCellStyle.Font = new Font("Tahoma", 10, FontStyle.Bold | FontStyle.Italic);
            RowView.DefaultCellStyle.BackColor = Color.LightSkyBlue;
        }
        private void btn_Export_Click(object sender, EventArgs e)
        {
            string zTeamName = "";
            //if (cbo_Team.SelectedValue.ToInt() != 0)
            //{
                //string[] s = cbo_Team.Text.Trim().Split('-');
                //zTeamName = "_Nhóm_" + s[0];
           // }
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Bảng_Thông_Tin_Sản_Phẩm_Theo_Công_Đoạn_Số_Lượng_Và_Lương_Khoán" + zTeamName + "_Từ_" + dte_FromDate.Value.ToString("dd_MM_yyyy") + "_Đến_" + dte_ToDate.Value.ToString("dd_MM_yyyy") + ".xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }

                    if (GVData.Rows.Count > 0)
                    {
                        DataTable dt = new DataTable();
                        foreach (DataGridViewColumn col in GVData.Columns)
                        {
                            dt.Columns.Add(col.HeaderText);

                        }
                        DataRow dRow = dt.NewRow();
                        dt.Rows.Add(dRow);
                        foreach (DataGridViewRow row in GVData.Rows)
                        {
                            dRow = dt.NewRow();
                            foreach (DataGridViewCell cell in row.Cells)
                            {
                                dRow[cell.ColumnIndex] = cell.Value;
                            }
                            dt.Rows.Add(dRow);
                        }
                        ExportTableToExcel(dt, Path);
                        Process.Start(Path);
                    }
                    else
                    {
                        Utils.TNMessageBox("Không tìm thấy dữ liệu!", 4);
                    }
                }

            }
        }
        private string ExportTableToExcel(DataTable zTable, string Folder)
        {
            try
            {
                var newFile = new FileInfo(Folder);
                if (newFile.Exists)
                {
                    newFile.Delete();
                }
                using (var package = new ExcelPackage(newFile))
                {
                    //Tạo Sheet 1 mới với tên Sheet là DonHang
                    ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("PhanBo");
                    worksheet.Cells["A1"].LoadFromDataTable(zTable, true);
                    worksheet.Cells.Style.Font.Name = "Times New Roman";
                    worksheet.Cells.Style.Font.Size = 12;
                    worksheet.Cells.AutoFilter = true;
                    worksheet.Cells.AutoFitColumns();
                    worksheet.View.FreezePanes(3, 4);

                    //Rowheader và row tổng
                    worksheet.Row(1).Height = 60;
                    worksheet.Row(1).Style.WrapText = true;
                    worksheet.Row(1).Style.Font.Bold = true;
                    worksheet.Row(1).Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                    worksheet.Row(1).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Row(zTable.Rows.Count + 1).Style.Font.Bold = true;//dòng tổng
                    //Chỉnh fix độ rộng cột
                    for (int j = 4; j <= zTable.Columns.Count; j++)
                    {
                        worksheet.Column(j).Width = 15;
                    }
                    worksheet.Column(1).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Column(1).Width = 5;
                    worksheet.Column(2).Width = 30;
                    worksheet.Column(3).Width = 15;
                    worksheet.Column(3).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Column(zTable.Columns.Count).Style.Font.Bold = true;//côt tổng
                    //Canh phải số
                    for (int i = 2; i <= zTable.Rows.Count + 1; i++)
                    {
                        for (int j = 4; j <= zTable.Columns.Count; j++)
                        {
                            worksheet.Cells[i, j].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                        }
                    }
                    //Merge header
                    //DataTable zTablStage = Salary_Report_Productivity.StageName(dte_FromDate.Value, dte_ToDate.Value, _TeamKey); //Danh sách tên công đoạn
                    worksheet.Cells[1, 1].Value = "STT";
                    worksheet.Cells[1, 2].Value = "Tên thành phẩm";
                    worksheet.Cells[1, 3].Value = " Mã ";
                    worksheet.Cells[1, 1, 2, 1].Merge = true;
                    worksheet.Cells[1, 2, 2, 2].Merge = true;
                    worksheet.Cells[1, 3, 2, 3].Merge = true;
                    int k = 0;
                    //for (int i = 4; i < TitleColumn.Length; i = i + 2)
                    //{
                    //    worksheet.Cells[1, i, 1, i + 1].Merge = true;
                    //    worksheet.Cells[1, i, 1, i + 1].Value = TitleMerge[k]; //zTablStage.Rows[k]["StageName"].ToString();
                    //    worksheet.Cells[2, i].Value = "Số lượng";
                    //    worksheet.Cells[2, i].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    //    worksheet.Cells[2, i + 1].Value = "Lương khoán";
                    //    worksheet.Cells[2, i + 1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    //    k++;
                    //}

                    //for (int i = 4; i < zTable.Columns.Count; i = i + 2)
                    //{
                    //    worksheet.Cells[1, i, 1, i + 1].Merge = true;
                    //    worksheet.Cells[1, i, 1, i + 1].Value = zTablStage.Rows[k]["StageName"].ToString();
                    //    worksheet.Cells[2, i].Value = "Số lượng";
                    //    worksheet.Cells[2, i].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    //    worksheet.Cells[2, i + 1].Value = "Lương khoán";
                    //    worksheet.Cells[2, i + 1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    //    k++;
                    //}
                    worksheet.Cells[1, zTable.Columns.Count].Value = " Tổng cộng ";
                    worksheet.Cells[1, zTable.Columns.Count, 2, zTable.Columns.Count].Merge = true;                   //Lưu file Excel
                    package.SaveAs(newFile);
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
            return "OK";
        }
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                //this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
                //btn_Export.Enabled = false;
            }
        }
        #endregion
    }
}
