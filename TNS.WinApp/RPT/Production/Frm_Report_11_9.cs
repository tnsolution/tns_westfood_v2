﻿using C1.Win.C1FlexGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.CORE;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Report_11_9 : Form
    {
        public Frm_Report_11_9()
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;
            btnClose.Click += btnClose_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            btn_Search.Click += Btn_Search_Click;
            btn_Export.Click += Btn_Export_Click;
            //
            btn_Export2.Click += Btn_Export2_Click;
        }

        private void Frm_Report_11_9_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            Check_RoleForm();

            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;

            DateTime ViewDate = SessionUser.Date_Work;
            DateTime FromDate = new DateTime(ViewDate.Year, ViewDate.Month, 1, 0, 0, 0);
            DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            dte_FromDate.Value = FromDate;
            dte_ToDate.Value = ToDate;
            //tab 2
            GVData2.Rows.Count = 0;
            GVData2.Cols.Count = 0;
        }
        private void DisplayData()
        {
            LoadData();
            LoadData2();
        }
        #region[tab1]
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            if (dte_ToDate.Value.Month != dte_FromDate.Value.Month || dte_ToDate.Value.Year != dte_FromDate.Value.Year)
            {
                Utils.TNMessageBoxOK( "Bạn chỉ được chọn trong 1 tháng", 1);
                return;
            }
            try
            {
                //Clear
                GVData.Rows.Count = 0;
                GVData.Cols.Count = 0;
                GVData2.Rows.Count = 0;
                GVData2.Cols.Count = 0;
                using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }

            }
            catch (Exception ex)
            {
                Utils.TNMessageBoxOK( ex.ToString(), 4);
            }
        }
        private void Btn_Export_Click(object sender, EventArgs e)
        {
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Báo_Cáo_Lương_Khoán_Tổng_Hợp_Bộ_Phận_Theo_Thành_Phẩm_Từ_" + dte_FromDate.Value.ToString("dd_MM_yyyy") + "_Đến_" + dte_ToDate.Value.ToString("dd_MM_yyyy") + ".xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }
                    string Status = "Xuất Excel form " + HeaderControl.Text + " > đường dẫn và tên file xuất:" + FDialog.FileName;
                     Application_Data.Save_LogApplication(SessionUser.UserLogin.Key,SessionUser.UserLogin.EmployeeKey, Status, 3);

                    GVData.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }
        }
        void LoadData()
        {
            DataTable zTable = Report.No11(dte_FromDate.Value, dte_ToDate.Value);
            PivotTable zPvt = null;
            if (zTable.Rows.Count > 0)
            {
                zPvt = new PivotTable(zTable);
                DataTable TableView = new DataTable(); ;
                if (rdoProduct.Checked)
                {
                    TableView = zPvt.Generate("TenNhom", "CongViec", new string[] { "SL", "LK" }, "CÔNG VIỆC", "TỔNG", "TỔNG", new string[] { "SL", "LK" });
                }
                if (rdoTeam.Checked)
                {
                    TableView = zPvt.Generate("CongViec", "TenNhom", new string[] { "SL", "LK" }, "TÊN NHÓM", "TỔNG", "TỔNG", new string[] { "SL", "LK" });
                }
                this.Invoke(new MethodInvoker(delegate ()
                {
                   
                    if (rdoProduct.Checked)
                    {
                        InitGVProduct(TableView);
                    }

                    if (rdoTeam.Checked)
                    {
                        InitGVTeam(TableView);
                    }
                }));
            }
        }
        void InitGVProduct(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();

            int TotalRow = TableView.Rows.Count + 2;
            int ToTalCol = TableView.Columns.Count + 2;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(TotalRow);

            //Row Header           

            GVData.Rows[0][0] = "SẢN PHẨM";
            GVData.Rows[0][1] = "CÔNG VIỆC";
            GVData.Rows[0][2] = "ĐƠN GIÁ";
            GVData.Rows[1][0] = "SẢN PHẨM";
            GVData.Rows[1][1] = "CÔNG VIỆC";
            GVData.Rows[1][2] = "ĐƠN GIÁ";

            int ColStart = 3;
            for (int i = 1; i < TableView.Columns.Count; i++)
            {
                DataColumn Col = TableView.Columns[i];
                string[] strCaption = Col.ColumnName.Split('|');

                GVData.Rows[0][ColStart] = strCaption[0];           //Ten Nhom
                GVData.Rows[1][ColStart] = strCaption[1];           //SL - LK                  

                ColStart++;
            }

            ColStart = 2;
            int RowStart = 2;//add dòng từ số thứ tự 2 trở đi
            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                DataRow rData = TableView.Rows[rIndex];
                string[] strTemp = rData[0].ToString().Split('|');

                if (strTemp.Length > 2)
                {
                    GVData.Rows[rIndex + RowStart][0] = strTemp[0];
                    GVData.Rows[rIndex + RowStart][1] = strTemp[1];
                    GVData.Rows[rIndex + RowStart][2] = strTemp[2].Ton0String();
                }
                else
                {
                    GVData.Rows[rIndex + RowStart][0] = strTemp[0];
                    GVData.Rows[rIndex + RowStart][1] = strTemp[0];
                    GVData.Rows[rIndex + RowStart][2] = strTemp[0];
                }
                int zColumn_LK = 4;
                for (int cIndex = 1; cIndex < TableView.Columns.Count; cIndex++)
                {
                    if(zColumn_LK==(ColStart + cIndex))
                    {
                        GVData.Rows[RowStart + rIndex][ColStart + cIndex] = rData[cIndex].Toe0String();
                        zColumn_LK += 2;
                    }
                    else
                    GVData.Rows[RowStart + rIndex][ColStart + cIndex] = rData[cIndex].Toe2String();
                }
            }

            //Style
            GVData.AutoSizeCols();
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.FixedOnly;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.CenterCenter;
            //
            GVData.Rows[0].AllowMerging = true;
            GVData.Rows[0].Height = 40;
            GVData.Rows[1].AllowMerging = true;
            GVData.Rows[1].Height = 40;

            GVData.Rows[TotalRow - 1].AllowMerging = true;

            GVData.Cols[0].AllowMerging = true;
            GVData.Cols[0].Width = 180;

            GVData.Cols[1].AllowMerging = true;
            GVData.Cols[1].Width = 450;

            GVData.Cols[2].AllowMerging = true;
            GVData.Cols[2].Width = 80;
            GVData.Cols[2].TextAlign = TextAlignEnum.RightCenter;

            //Freeze Row and Column
            GVData.Rows.Fixed = 2;
            GVData.Cols.Fixed = 3;

            GVData.AutoSizeCols(1, GVData.Cols.Count - 1, 10);
            GVData.Cols[1].TextAlign = TextAlignEnum.LeftCenter;
            //Canh phải các cột từ số 3 trở đi
            for (int i = 3; i < GVData.Cols.Count; i++)
            {
                GVData.Cols[i].TextAlign = TextAlignEnum.RightCenter;
            }
            //In đậm dòng tổng
            GVData.Rows[TotalRow - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[ToTalCol - 2].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[ToTalCol - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[ToTalCol - 2].StyleNew.ForeColor = Color.Blue;
            GVData.Cols[ToTalCol - 1].StyleNew.ForeColor = Color.Blue;
        }
        void InitGVTeam(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();

            int TotalRow = TableView.Rows.Count + 4;
            int ToTalCol = TableView.Columns.Count;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(TotalRow);

            CellRange zCellRange = GVData.GetCellRange(0, 0, 3, 0);
            zCellRange.Data = "TÊN NHÓM";

            //Row Header
            int ColStart = 1;
            for (int i = 1; i < TableView.Columns.Count; i++)
            {
                DataColumn Col = TableView.Columns[i];
                string[] strCaption = Col.ColumnName.Split('|');
                if (strCaption.Length > 2)
                {
                    GVData.Rows[0][ColStart] = strCaption[0];           //Ten Nhom SP
                    GVData.Rows[1][ColStart] = strCaption[1];           //Ten Cong Viec
                    GVData.Rows[2][ColStart] = strCaption[2].Toe0String();           //Don Gia
                    GVData.Rows[3][ColStart] = strCaption[3];
                }
                else
                {
                    CellRange CellRange = GVData.GetCellRange(1, ColStart, 3, ColStart);
                    CellRange.Data = strCaption[1];
                }
                ColStart++;
            }

            //Row Data
            int RowStart = 4;//add dòng từ số thứ tự 4 trở đi
            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                DataRow rData = TableView.Rows[rIndex];
                int zColumn_LK = 2;
                for (int cIndex = 0; cIndex < TableView.Columns.Count; cIndex++)
                {
                    if (cIndex >= 1)
                    {
                        if(zColumn_LK==cIndex)
                        {
                            GVData.Rows[RowStart + rIndex][cIndex] = rData[cIndex].Toe0String();
                            zColumn_LK += 2;
                        }
                        else
                        GVData.Rows[RowStart + rIndex][cIndex] = rData[cIndex].Toe2String();
                    }
                    else
                    {
                        GVData.Rows[RowStart + rIndex][cIndex] = rData[cIndex];
                    }
                }
            }

            zCellRange = GVData.GetCellRange(0, ToTalCol - 2, 0, ToTalCol - 1);
            zCellRange.Data = "TỔNG";

            //Style

            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.Free;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.CenterCenter;
            GVData.Styles.Normal.WordWrap = true;

            //Trộn dòng 0
            GVData.Rows[0].AllowMerging = true;
            GVData.Rows[0].Height = 40;
            //Trộn dòng 1
            GVData.Rows[1].AllowMerging = true;
            GVData.Rows[1].Height = 60;
            //Trộn dòng 2
            GVData.Rows[2].AllowMerging = true;

            //Trộn cột đầu
            GVData.Cols[0].AllowMerging = true;
            GVData.Cols[0].Width = 200;
            //Trộn 2 cột cuối
            GVData.Cols[ToTalCol - 1].AllowMerging = true;
            GVData.Cols[ToTalCol - 2].AllowMerging = true;

            //Freeze Row and Column
            GVData.Rows.Fixed = 4;
            GVData.Cols.Fixed = 1;

            //GVData.AutoSizeCols(1, GVData.Cols.Count - 1, 10);
            //Canh phải các cột từ số 1 trở đi
            GVData.Cols[0].TextAlign = TextAlignEnum.LeftCenter;
            for (int i = 1; i < GVData.Cols.Count; i++)
            {
                GVData.Cols[i].TextAlign = TextAlignEnum.RightCenter;
            }
            //In đậm dòng tổng
            GVData.Rows[TotalRow - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[ToTalCol - 2].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[ToTalCol - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[ToTalCol - 2].StyleNew.ForeColor = Color.Blue;
            GVData.Cols[ToTalCol - 1].StyleNew.ForeColor = Color.Blue;
            //for (int i = 2; i < GVData.Rows.Count; i++)
            //{
            //    GVData.Rows[i].Height = 27;                
            //}
        }
        #endregion

        #region[tab2]
        private void Btn_Export2_Click(object sender, EventArgs e)
        {
            
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Báo_Cáo_Lương_Khoán_Tổng_Hợp_Bộ_Phận_CVTG_Theo_Ngày_Theo_Thời_Gian_Từ_" + dte_FromDate.Value.ToString("dd_MM_yyyy") + "_Đến_" + dte_ToDate.Value.ToString("dd_MM_yyyy") + ".xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }

                    string Status = "Xuất Excel form " + HeaderControl.Text + " > đường dẫn và tên file xuất:" + FDialog.FileName;
                     Application_Data.Save_LogApplication(SessionUser.UserLogin.Key,SessionUser.UserLogin.EmployeeKey, Status, 3);

                    GVData2.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }
        }
        void LoadData2()
        {

            DataTable zTable = Report.No9(dte_FromDate.Value, dte_ToDate.Value);
            PivotTable zPvt = null;
            if (zTable.Rows.Count > 0)
            {
                zPvt = new PivotTable(zTable);
                DataTable TableView = new DataTable();
                if (rdoProduct.Checked)
                {
                    TableView = zPvt.Generate("TenNhom", "CongViec", new string[] { "SL", "LK" }, "CÔNG VIỆC", "TỔNG", "TỔNG", new string[] { "SL", "LK" });
                }

                if (rdoTeam.Checked)
                {
                    TableView = zPvt.Generate("CongViec", "TenNhom", new string[] { "SL", "LK" }, "TÊN NHÓM", "TỔNG", "TỔNG", new string[] { "SL", "LK" });
                }
                this.Invoke(new MethodInvoker(delegate ()
                {
                   
                    if (rdoProduct.Checked)
                    {
                        InitGVProduct2(TableView);
                    }

                    if (rdoTeam.Checked)
                    {
                        InitGVTeam2(TableView);
                    }
                }));
            }


        }
        void InitGVProduct2(DataTable TableView)
        {
            GVData2.Rows.Count = 0;
            GVData2.Cols.Count = 0;
            GVData2.Clear();

            int TotalRow = TableView.Rows.Count + 2;
            int ToTalCol = TableView.Columns.Count + 2;

            GVData2.Cols.Add(ToTalCol);
            GVData2.Rows.Add(TotalRow);

            //Row Header           

            GVData2.Rows[0][0] = "SẢN PHẨM";
            GVData2.Rows[0][1] = "CÔNG VIỆC";
            GVData2.Rows[0][2] = "ĐƠN GIÁ";
            GVData2.Rows[1][0] = "SẢN PHẨM";
            GVData2.Rows[1][1] = "CÔNG VIỆC";
            GVData2.Rows[1][2] = "ĐƠN GIÁ";

            int ColStart = 3;
            for (int i = 1; i < TableView.Columns.Count; i++)
            {
                DataColumn Col = TableView.Columns[i];
                string[] strCaption = Col.ColumnName.Split('|');

                GVData2.Rows[0][ColStart] = strCaption[0];           //Ten Nhom
                GVData2.Rows[1][ColStart] = strCaption[1];           //SL - LK                  

                ColStart++;
            }

            ColStart = 2;
            int RowStart = 2;//add dòng từ số thứ tự 2 trở đi
            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                DataRow rData = TableView.Rows[rIndex];
                string[] strTemp = rData[0].ToString().Split('|');

                if (strTemp.Length > 2)
                {
                    GVData2.Rows[rIndex + RowStart][0] = strTemp[0];
                    GVData2.Rows[rIndex + RowStart][1] = strTemp[1];
                    GVData2.Rows[rIndex + RowStart][2] = strTemp[2].Toe0String();
                }
                else
                {
                    GVData2.Rows[rIndex + RowStart][0] = strTemp[0];
                    GVData2.Rows[rIndex + RowStart][1] = strTemp[0];
                    GVData2.Rows[rIndex + RowStart][2] = strTemp[0];
                }

                for (int cIndex = 1; cIndex < TableView.Columns.Count; cIndex++)
                {
                    GVData2.Rows[RowStart + rIndex][ColStart + cIndex] = rData[cIndex].Toe2String();
                }
            }

            //Style
            GVData2.AutoSizeCols();
            GVData2.AllowResizing = AllowResizingEnum.Both;
            GVData2.AllowMerging = AllowMergingEnum.FixedOnly;
            GVData2.SelectionMode = SelectionModeEnum.Row;
            GVData2.VisualStyle = VisualStyle.Office2010Blue;
            GVData2.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData2.Styles.Normal.TextAlign = TextAlignEnum.CenterCenter;
            //
            GVData2.Rows[0].AllowMerging = true;
            GVData2.Rows[0].Height = 40;
            GVData2.Rows[1].AllowMerging = true;
            GVData2.Rows[1].Height = 40;

            GVData2.Rows[TotalRow - 1].AllowMerging = true;

            GVData2.Cols[0].AllowMerging = true;
            GVData2.Cols[0].Width = 180;

            GVData2.Cols[1].AllowMerging = true;
            GVData2.Cols[1].Width = 450;

            GVData2.Cols[2].AllowMerging = true;
            GVData2.Cols[2].Width = 80;
            GVData2.Cols[2].TextAlign = TextAlignEnum.RightCenter;

            //Freeze Row and Column
            GVData2.Rows.Fixed = 2;
            GVData2.Cols.Fixed = 3;

            GVData2.AutoSizeCols(1, GVData2.Cols.Count - 1, 10);
            GVData2.Cols[1].TextAlign = TextAlignEnum.LeftCenter;
            //Canh phải các cột từ số 3 trở đi
            for (int i = 3; i < GVData2.Cols.Count; i++)
            {
                GVData2.Cols[i].TextAlign = TextAlignEnum.RightCenter;
            }
            //In đậm dòng tổng
            GVData2.Rows[TotalRow - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData2.Cols[ToTalCol - 2].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData2.Cols[ToTalCol - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData2.Cols[ToTalCol - 2].StyleNew.ForeColor = Color.Blue;
            GVData2.Cols[ToTalCol - 1].StyleNew.ForeColor = Color.Blue;
        }
        void InitGVTeam2(DataTable TableView)
        {
            GVData2.Rows.Count = 0;
            GVData2.Cols.Count = 0;
            GVData2.Clear();

            int TotalRow = TableView.Rows.Count + 4;
            int ToTalCol = TableView.Columns.Count;

            GVData2.Cols.Add(ToTalCol);
            GVData2.Rows.Add(TotalRow);

            CellRange zCellRange = GVData2.GetCellRange(0, 0, 3, 0);
            zCellRange.Data = "TÊN NHÓM";

            //Row Header
            int ColStart = 1;
            for (int i = 1; i < TableView.Columns.Count; i++)
            {
                DataColumn Col = TableView.Columns[i];
                string[] strCaption = Col.ColumnName.Split('|');
                if (strCaption.Length > 2)
                {
                    GVData2.Rows[0][ColStart] = strCaption[0];           //Ten Nhom SP
                    GVData2.Rows[1][ColStart] = strCaption[1];           //Ten Cong Viec
                    GVData2.Rows[2][ColStart] = strCaption[2].Toe0String();           //Don Gia
                    GVData2.Rows[3][ColStart] = strCaption[3];
                }
                else
                {
                    CellRange CellRange = GVData2.GetCellRange(1, ColStart, 3, ColStart);
                    CellRange.Data = strCaption[1];
                }
                ColStart++;
            }

            //Row Data
            int RowStart = 4;//add dòng từ số thứ tự 4 trở đi
            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                DataRow rData = TableView.Rows[rIndex];
                for (int cIndex = 0; cIndex < TableView.Columns.Count; cIndex++)
                {
                    if (cIndex >= 1)
                    {
                        GVData2.Rows[RowStart + rIndex][cIndex] = rData[cIndex].Toe2String();
                    }
                    else
                    {
                        GVData2.Rows[RowStart + rIndex][cIndex] = rData[cIndex];
                    }
                }
            }

            zCellRange = GVData2.GetCellRange(0, ToTalCol - 2, 0, ToTalCol - 1);
            zCellRange.Data = "TỔNG";

            //Style

            GVData2.AllowResizing = AllowResizingEnum.Both;
            GVData2.AllowMerging = AllowMergingEnum.Free;
            GVData2.SelectionMode = SelectionModeEnum.Row;
            GVData2.VisualStyle = VisualStyle.Office2010Blue;
            GVData2.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData2.Styles.Normal.TextAlign = TextAlignEnum.CenterCenter;
            GVData2.Styles.Normal.WordWrap = true;

            //Trộn dòng 0
            GVData2.Rows[0].AllowMerging = true;
            GVData2.Rows[0].Height = 40;
            //Trộn dòng 1
            GVData2.Rows[1].AllowMerging = true;
            GVData2.Rows[1].Height = 60;
            //Trộn dòng 2
            GVData2.Rows[2].AllowMerging = true;

            //Trộn cột đầu
            GVData2.Cols[0].AllowMerging = true;
            GVData2.Cols[0].Width = 200;
            //Trộn 2 cột cuối
            GVData2.Cols[ToTalCol - 1].AllowMerging = true;
            GVData2.Cols[ToTalCol - 2].AllowMerging = true;

            //Freeze Row and Column
            GVData2.Rows.Fixed = 4;
            GVData2.Cols.Fixed = 1;

            //GVData2.AutoSizeCols(1, GVData2.Cols.Count - 1, 10);
            //Canh phải các cột từ số 1 trở đi
            GVData2.Cols[0].TextAlign = TextAlignEnum.LeftCenter;
            for (int i = 1; i < GVData2.Cols.Count; i++)
            {
                GVData2.Cols[i].TextAlign = TextAlignEnum.RightCenter;
            }
            //In đậm dòng tổng
            GVData2.Rows[TotalRow - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData2.Cols[ToTalCol - 2].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData2.Cols[ToTalCol - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData2.Cols[ToTalCol - 2].StyleNew.ForeColor = Color.Blue;
            GVData2.Cols[ToTalCol - 1].StyleNew.ForeColor = Color.Blue;
            //for (int i = 2; i < GVData2.Rows.Count; i++)
            //{
            //    GVData2.Rows[i].Height = 27;                
            //}
        }
        #endregion
        string FormatMoney(object input)
        {
            try
            {
                if (input.ToString() != "0")
                {
                    double zResult = double.Parse(input.ToString());
                    return zResult.ToString("n", CultureInfo.GetCultureInfo("vi-VN"));
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
               // this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
                btn_Export.Enabled = false;
                btn_Export2.Enabled = false;
            }
        }
        #endregion
    }
}
