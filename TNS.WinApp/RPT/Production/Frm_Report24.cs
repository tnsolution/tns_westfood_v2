﻿using C1.Win.C1FlexGrid;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TN.Connect;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Report24 : Form
    {
        int _TeamKey = 0;
        string _TeamName = "";
        string _InEmployeeID = "";
        string _TeamKeySalary = "";
        public Frm_Report24()
        {
            InitializeComponent();
            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;
            btnClose.Click += btnClose_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            btn_Search.Click += Btn_Search_Click;
            btn_Export.Click += Btn_Export_Click;

            btn_Print.Click += Btn_Print_Click;
            btn_Done.Click += Btn_Done_Click;
            btnClose_Panel_Message.Click += BtnClose_Panel_Message_Click;
        }

        private void Frm_Report24_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            Check_RoleForm();

            DateTime ViewDate = SessionUser.Date_Work;
            DateTime FromDate = new DateTime(ViewDate.Year, ViewDate.Month, 1, 0, 0, 0);
            DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            dte_FromDate.Value = FromDate;
            dte_ToDate.Value = ToDate;

            string zSQL = @"SELECT TeamKey,TeamID +'-'+ TeamName AS TeamName  FROM SYS_Team 
                            WHERE RecordStatus <> 99 AND BranchKey=4 AND  DepartmentKey != 26
                            ORDER BY RANK";

            LoadDataToToolbox.KryptonComboBox(cbo_TeamMain, zSQL, "");// nhóm chính

            GVEmployee.Rows.Count = 0;
            GVEmployee.Cols.Count = 0;
            GVEmployee.Clear();
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            LoadInfo_Report();
            Panel_Done.Visible = false;
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            GVEmployee.Rows.Count = 0;
            GVEmployee.Cols.Count = 0;
            GVEmployee.Clear();
            string Status = "Tìm DL form " + HeaderControl.Text + " > từ: " + dte_FromDate.Value.ToString("dd/MM/yyyy") + " > đến:" + dte_ToDate.Value.ToString("dd/MM/yyyy") + " > Nhóm:" + cbo_TeamMain.Text + " > Từ khóa:" + txt_Search.Text;
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);


            if (txt_Search.Text.Trim().Length > 0)
            {
                _InEmployeeID = "'" + txt_Search.Text.Trim() + "'";
                using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }
               
            }
            else
            {
                if (cbo_TeamMain.SelectedValue.ToString() != "0")
                {

                    _TeamKey = cbo_TeamMain.SelectedValue.ToInt();
                    // Lấy danh sách nhân viên của nhóm chính
                    DataTable zEmployee = TNS.WinApp.Frm_Report24_DLL.Report_Data.ListEmployeeOfTeam(_TeamKey, dte_FromDate.Value, dte_ToDate.Value);
                    _InEmployeeID = "";
                    if (zEmployee.Rows.Count > 0)
                    {
                        for (int i = 0; i < zEmployee.Rows.Count; i++)
                        {

                            _InEmployeeID += "'" + zEmployee.Rows[i]["EmployeeID"] + "',";

                        }
                        _InEmployeeID = _InEmployeeID.Remove(_InEmployeeID.Length - 1, 1);

                        using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }
                        
                    }
                    else
                    {
                        Utils.TNMessageBoxOK("Không tìm thấy nhân viên!", 1);
                    }
                }
                else
                {
                    Utils.TNMessageBoxOK("Vui lòng chọn 1 nhóm!", 1);
                }
            }
        }
        private void Btn_Export_Click(object sender, EventArgs e)
        {

            string zTeamName = "";
            if (cbo_TeamMain.SelectedValue.ToInt() != 0)
            {
                string[] s = cbo_TeamMain.Text.Trim().Split('-');
                zTeamName = "_Nhóm_" + s[0];
            }
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Báo_Cáo_Lương_Khoán_Sản_Xuất_Công_Nhân_Sản_Phẩm" + zTeamName + "_Từ_" + dte_FromDate.Value.ToString("dd_MM_yyyy") + "_Đến_" + dte_ToDate.Value.ToString("dd_MM_yyyy") + ".xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }

                    string Status = "Xuất Excel form " + HeaderControl.Text + " > đường dẫn và tên file xuất:" + FDialog.FileName;
                    Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);

                    GVEmployee.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }
        }
        private void DisplayData()
        {
            DataTable zTable = TNS.WinApp.Frm_Report24_DLL.Report_Data.Report_24(dte_FromDate.Value, dte_ToDate.Value, _InEmployeeID, _TeamKeySalary);
            if (zTable.Rows.Count > 0)
            {
                for (int i = 0; i < zTable.Rows.Count; i++)
                {

                    //Nếu nhân sự không có làm việc hôm đó thì xóa luôn , nếu không sẽ phát sinh colum rỗng
                    float zgiodu = 0;
                    if (float.TryParse(zTable.Rows[i][2].ToString(), out zgiodu))
                    {

                    }
                    float zluonkhoan = 0;
                    if (float.TryParse(zTable.Rows[i][3].ToString(), out zluonkhoan))
                    {

                    }
                    if (zluonkhoan == 0 && zgiodu == 0)
                    {
                        zTable.Rows.Remove(zTable.Rows[i]);
                        i--;
                    }
                }


                DataTable BangGioDu = new DataTable();

                DataRow[] rGioDu = zTable.Select("[TIENDU] > 0");
                if (rGioDu.Length > 0)
                {
                    BangGioDu = rGioDu.CopyToDataTable();

                    for (int i = 0; i < rGioDu.Length; i++)
                    {
                        rGioDu[i].Delete();
                    }
                }
                zTable.AcceptChanges();

                PivotTable zPvt = new PivotTable(zTable);
                DataTable zTablePivot = zPvt.Generate("WORK", "ORDER",
                     new string[] { "LK" }, "CÔNG NHÂN", "TỔNG CỘNG", "TỔNG CỘNG",
                     new string[] { "LƯƠNG KHOÁN" }, 0, true);


                this.Invoke(new MethodInvoker(delegate ()
                {
                    InitLayoutGV(zTablePivot);
                    FillDataGV(zTablePivot);
                    FillDataGV_GioDu(BangGioDu);
                }));
            }
        }
        void InitLayoutGV(DataTable zTable)
        {
            GVEmployee.Rows.Count = 0;
            GVEmployee.Cols.Count = 0;
            GVEmployee.Clear();

            GVEmployee.Cols.Add(zTable.Columns.Count + 3);
            GVEmployee.Rows.Add(4);

            CellRange zRange = GVEmployee.GetCellRange(0, 0, 3, 0);
            zRange.Data = "STT";

            zRange = GVEmployee.GetCellRange(0, 1, 3, 1);
            zRange.Data = "HỌ VÀ TÊN";

            zRange = GVEmployee.GetCellRange(0, 2, 3, 2);
            zRange.Data = "MÃ THẺ";

            zRange = GVEmployee.GetCellRange(0, 3, 3, 3);
            zRange.Data = "GIÃN CA";

            //
            for (int i = 1; i < zTable.Columns.Count; i++)
            {
                DataColumn Col = zTable.Columns[i];

                string[] strCaption = Col.ColumnName.Split('|');
                if (strCaption.Length >= 4)
                {
                    GVEmployee.Rows[0][i + 3] = strCaption[0];  
                    GVEmployee.Rows[1][i + 3] = strCaption[1];  
                    GVEmployee.Rows[2][i + 3] = strCaption[2];
                    GVEmployee.Rows[3][i + 3] = strCaption[3];
                }
                else
                {
                    if (strCaption.Length == 3)
                    {
                        GVEmployee.Rows[0][i + 3] = strCaption[0];  //NHÓM CÔNG NHÂN
                        GVEmployee.Rows[2][i + 3] = strCaption[1];  //Tổng
                        GVEmployee.Rows[3][i + 3] = strCaption[2];   //"LK", "TG", "SLTP", "NS"

                        //zRange = GVEmployee.GetCellRange(0, i + 3, 3, i + 3);
                        //zRange.Data = strCaption[1];
                        //GVEmployee.MergedRanges.Add(zRange);
                    }
                    else
                    {
                        GVEmployee.Rows[2][i + 3] = strCaption[0];
                        GVEmployee.Rows[3][i + 3] = strCaption[1];
                        //zRange = GVEmployee.GetCellRange(0, i + 3, 3, i + 3);
                        //zRange.Data = strCaption[0];
                    }
                }
                GVEmployee.Cols[i].Width = 110;
            }

            GVEmployee.AllowResizing = AllowResizingEnum.Both;
            GVEmployee.AllowMerging = AllowMergingEnum.FixedOnly;
            GVEmployee.SelectionMode = SelectionModeEnum.Row;
            GVEmployee.VisualStyle = VisualStyle.Office2010Blue;
            GVEmployee.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVEmployee.Styles.Normal.TextAlign = TextAlignEnum.LeftCenter;

            GVEmployee.Rows[0].AllowMerging = true;
            GVEmployee.Rows[1].AllowMerging = true;
            GVEmployee.Rows[2].AllowMerging = true;
            //GVEmployee.Rows[3].AllowMerging = true;
            //GVEmployee.Rows[4].AllowMerging = true;

            GVEmployee.Cols[0].Width = 40;
            GVEmployee.Cols[0].AllowMerging = true;

            GVEmployee.Cols[1].Width = 180;
            GVEmployee.Cols[1].AllowMerging = true;

            GVEmployee.Cols[2].AllowMerging = true;
            GVEmployee.Cols[2].Width = 100;

            GVEmployee.Cols[3].AllowMerging = true;
            GVEmployee.Cols[3].Width = 100;


            //4 cột tổng cuối 
            //TỔNG CỘT
            GVEmployee.Cols[GVEmployee.Cols.Count - 1].AllowMerging = true;
            CellRange zCellRange;
            zCellRange = GVEmployee.GetCellRange(0, GVEmployee.Cols.Count - 1, 3, GVEmployee.Cols.Count - 1);
            zCellRange.Data = "TỔNG CỘNG";
            GVEmployee.Cols[GVEmployee.Cols.Count - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);

            //----------------------

            GVEmployee.Rows.Fixed = 4;
            GVEmployee.Cols.Frozen = 3;

            GVEmployee.Cols[0].StyleNew.BackColor = Color.Empty;
            GVEmployee.Cols[1].StyleNew.BackColor = Color.Empty;
            GVEmployee.Cols[2].StyleNew.BackColor = Color.Empty;

            GVEmployee.Rows[0].StyleNew.WordWrap = true;
            GVEmployee.Rows[0].TextAlign = TextAlignEnum.CenterCenter;
            GVEmployee.Rows[0].Height = 50;

            GVEmployee.Rows[1].StyleNew.WordWrap = true;
            GVEmployee.Rows[1].TextAlign = TextAlignEnum.CenterCenter;
            GVEmployee.Rows[1].Height = 50;

            GVEmployee.Rows[2].StyleNew.WordWrap = true;
            GVEmployee.Rows[2].TextAlign = TextAlignEnum.CenterCenter;
            GVEmployee.Rows[2].Height = 90;

            //GVEmployee.Rows[3].StyleNew.WordWrap = true;
            //GVEmployee.Rows[3].TextAlign = TextAlignEnum.CenterCenter;

            //GVEmployee.Rows[4].StyleNew.WordWrap = true;
            //GVEmployee.Rows[4].TextAlign = TextAlignEnum.CenterCenter;

            for (int i = 3; i < GVEmployee.Cols.Count; i++)
            {
                GVEmployee.Cols[i].TextAlign = TextAlignEnum.RightCenter;
            }
        }
        void FillDataGV(DataTable zTable)
        {
            

            int TeamKeyFlag = _TeamKey;

            double TotalLK = 0;

            // Thêm 1 cột để sắp xếp
            zTable.Columns.Add("Rank");
            for (int i = 0; i < zTable.Rows.Count; i++)
            {
                DataRow rPivot = zTable.Rows[i];
                string rColumn0 = rPivot[0].ToString();
                string[] temp = rColumn0.Split('|');
                if (temp[0].ToString() == "TỔNG CỘNG")
                {
                    zTable.Rows[i]["Rank"] = "ZZZZZZZZZ";
                }
                else
                {
                    string EmployeeID = temp[1];
                    zTable.Rows[i]["Rank"] = ConvertIDRank(EmployeeID);
                }
            }
            DataView dv = zTable.DefaultView;
            dv.Sort = " Rank ASC";
            zTable = dv.ToTable();
            zTable.Columns.Remove("Rank");
            //--sắp xếp xong xóa cột sắp xếp đi

            int Row = 4;
            for (int i = 0; i < zTable.Rows.Count; i++)
            {
                GVEmployee.Rows.Add();
                GVEmployee.Rows[Row][0] = i + 1;

                #region [--Fill data bình thường--]
                for (int j = 0; j < zTable.Columns.Count; j++)
                {
                    DataRow rPivot = zTable.Rows[i];

                    string rColumn0 = rPivot[j].ToString();

                    if (rColumn0.Contains("|"))
                    {
                        string[] temp = rColumn0.Split('|');
                        string EmployeeName = temp[0];
                        string EmployeeID = temp[1];

                        GVEmployee.Rows[Row][j + 1] = temp[0];
                        GVEmployee.Rows[Row][j + 2] = temp[1];

                    }
                    else
                    {
                        GVEmployee.Rows[Row][j + 3] = zTable.Rows[i][j].Toe0String(); //--FormatMoney(zTable.Rows[i][j].ToString());
                    }
                }
                #endregion
                Row++;
            }
            GVEmployee[GVEmployee.Rows.Count - 1, 0] = "";
            GVEmployee[GVEmployee.Rows.Count - 1, 1] = "TỔNG CỘNG";
            GVEmployee.Rows[GVEmployee.Rows.Count - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
        }
        void FillDataGV_GioDu(DataTable BangGioDu)
        {
            if (GVEmployee.Rows.Count > 4)
            {
                double zTotal = 0;
                for (int i = 4; i < GVEmployee.Rows.Count-1; i++)
                {
                    string ID = GVEmployee.Rows[i][2].ToString();

                    foreach (DataRow rgiodu in BangGioDu.Rows)
                    {
                        string[] temp = rgiodu[0].ToString().Split('|');

                        if (ID == temp[1]) //temp[1]:EmployeeID
                        {
                            GVEmployee.Rows[i][3] = rgiodu["TIENDU"].Toe0String();

                            double zTemp = 0;
                            if(double.TryParse(rgiodu["TIENDU"].Toe0String(),out zTemp))
                            {
                                zTotal += zTemp;
                            }
                            BangGioDu.Rows.Remove(rgiodu);
                            break;
                        }
                    }

                }
                GVEmployee.Rows[GVEmployee.Rows.Count - 1][3] = zTotal.Toe0String();
            }
            else
            {
                foreach (DataRow rgiodu in BangGioDu.Rows)
                {
                    string[] temp = rgiodu[0].ToString().Split('|');
                    string EmployeeName = temp[0];
                    string EmployeeID = temp[1];
                    string OverMoney = rgiodu["TIENDU"].Toe0String();

                    GVEmployee.Rows.Add();
                    int r = GVEmployee.Rows.Count;

                    GVEmployee.Rows[r - 1][1] = EmployeeName;
                    GVEmployee.Rows[r - 1][2] = EmployeeID;
                    GVEmployee.Rows[r - 1][3] = OverMoney.Toe0String();
                }
            }
            if (GVEmployee.Rows.Count > 4)
            {
                for (int i = 4; i < GVEmployee.Rows.Count; i++)
                {
                    double m1 = 0;
                    double m2 = 0;
                    if (GVEmployee.Rows[i][3] != null && double.TryParse(GVEmployee.Rows[i][3].ToString(), out m1))
                    {

                    }
                    if (GVEmployee.Rows[i][GVEmployee.Cols.Count - 1] != null && double.TryParse(GVEmployee.Rows[i][GVEmployee.Cols.Count - 1].ToString(), out m2))
                    {

                    }
                    double m3 = m1 + m2; // tổng giãn ca + tổng lương khoán
                    GVEmployee.Rows[i][GVEmployee.Cols.Count - 1] = m3.Toe0String();

                    //double m1 = UnFormatMoney(GVEmployee.Rows[i][3]).ToDouble();

                    //double m2 = UnFormatMoney(GVEmployee.Rows[i][GVEmployee.Cols.Count - 4]).ToDouble();
                    //double m3 = m1 + m2; // tổng giãn ca + tổng lương khoán
                    //GVEmployee.Rows[i][GVEmployee.Cols.Count - 4] = FormatMoney(m3);
                }
            }

        }


        //Chuyển số thành dãy chuỗi
        string ConvertIDRank(string ID)
        {
            string zResult = "";
            string s = "";
            string temp = "";
            if (ID.Substring(0, 1) == "A")
            {
                temp = ID;
                for (int i = 0; i < 8 - ID.Trim().Length; i++)
                {
                    s += "9";
                }
                zResult = s + temp;
            }
            else if (ID.Substring(0, 1) == "H" || ID.Substring(0, 1) == "L")
            {
                temp = ID;
                for (int i = 0; i < 8 - ID.Trim().Length; i++)
                {
                    s += "B";
                }
                zResult = s + temp;
            }
            else
            {
                temp = ID;
                for (int i = 0; i < 8 - ID.Trim().Length; i++)
                {
                    s += "0";
                }
                zResult = s + temp;
            }
            return zResult;
        }
        #region[In]
        private void LoadInfo_Report()
        {
            //Ngày lập
            dte_RpDate.Value = SessionUser.Date_Work;
            //Tên báo cáo
            txt_RpTitle.Text = "BẢNG LƯƠNG KHOÁN CÔNG NHÂN THEO SẢN PHẨM ";
            //Người lập
            txt_RpNguoiLap.Text = SessionUser.UserLogin.EmployeeName;
            //Kế toán trưởng
            txt_RpKeToan.Text = "Trần Tấn Long Thạch";
            //HCNS
            txt_RpHCNS.Text = "Lê Văn Hòa";
            //Tổng giám đốc
            txt_RpGiamDoc.Text = "Nguyễn Vũ Lộc";
        }
        private void Btn_Print_Click(object sender, EventArgs e)
        {
            if (GVEmployee.Rows.Count > 0)
            {
                string[] zTeam = cbo_TeamMain.Text.Split('-');
                txt_RpTitle.Text = "BẢNG LƯƠNG KHOÁN CÔNG NHÂN THEO SẢN PHẨM NHÓM " + zTeam[1] + " THÁNG " + dte_RpDate.Value.Month + " NĂM " + dte_RpDate.Value.Year;
                Panel_Done.Visible = true;
            }
            else
            {
                Utils.TNMessageBoxOK("Không tìm thấy dữ liệu", 1);
            }
        }
        private void Btn_Done_Click(object sender, EventArgs e)
        {
            Panel_Done.Visible = false;

            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = txt_RpTitle.Text + ".xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }
                    XuatExcelMau(newFile);
                    Process.Start(Path);
                }

            }
        }
        private void BtnClose_Panel_Message_Click(object sender, EventArgs e)
        {
            Panel_Done.Visible = false;
        }

        private void XuatExcelMau(FileInfo newFile)
        {
            string TieuDe = txt_RpTitle.Text;
            string Font = "Time New Roman";
            string LapBang = txt_RpNguoiLap.Text;
            string KeToan = txt_RpKeToan.Text;
            string NhanSu = txt_RpHCNS.Text;
            string GiamDoc = txt_RpGiamDoc.Text;

            var fileMau = new FileInfo(Application.StartupPath + @"\FileTemplate\MauBaoCao_So1.xlsx");
            using (ExcelPackage xlPackage = new ExcelPackage(fileMau))
            {
                ExcelWorksheet workSheet = xlPackage.Workbook.Worksheets.FirstOrDefault();

                //tham số chỉ định tùy chọn
                int ColumnGV = GVEmployee.Cols.Count;  //tổng cột cần hiển thị của Gidview
                int RowGV = GVEmployee.Rows.Count; //số dòng dữ liệu Gidview
                int RowExcel = 6;  //dòng bắt đầu cần ghi dữ liệu trong excel         

                #region 2. DATA DỮ LIỆU                
                for (int i = 1; i <= RowGV; i++)
                {
                    int rowGV = i - 1;
                    for (int c = 1; c <= ColumnGV; c++)
                    {
                        int colGV = c - 1;

                        object val = GVEmployee.Rows[rowGV][colGV];
                        if (val != null)
                        {
                            workSheet.Cells[RowExcel + i, c].Value = val.ToString();
                        }
                        else
                        {
                            workSheet.Cells[RowExcel + i, c].Value = string.Empty;
                        }
                        if (c == 2)
                        { // cot ten nhan vien
                            workSheet.Cells[RowExcel + i, c].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        }
                        else
                        {
                            workSheet.Cells[RowExcel + i, c].Style.Numberformat.Format = "0.00";
                            workSheet.Cells[RowExcel + i, c].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        }
                    }
                }
                #endregion
                #region 3. ĐỊNH DẠNG  DATA EXCEL
                //DÒNG THỨ 7 TRONG EXCEL ĐẾN DÒNG 11 TRONG EXCEL LÀ TIÊU ĐỀ BẢNG
                for (int r = 7; r < 11; r++)
                {
                    for (int c = 1; c <= ColumnGV; c++)
                    {
                        int colGV = c - 1;
                        workSheet.Cells[r, c].Style.Font.Name = Font;
                        workSheet.Cells[r, c].Style.Font.Size = 12;
                        workSheet.Cells[r, c].Style.Font.Bold = true;
                        workSheet.Cells[r, c].Style.Font.Color.SetColor(Color.Navy);
                        workSheet.Cells[r, c].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        workSheet.Cells[r, c].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        workSheet.Cells[r, c].Style.WrapText = true;
                        //workSheet.Column(c).Width = 15;
                        workSheet.Column(c).AutoFit();

                    }
                }

                //DÒNG TỔNG FOR TỪNG CỘT
                for (int c = 1; c <= ColumnGV; c++)
                {
                    int colGV = c - 1;

                    workSheet.Cells[RowExcel + RowGV, c].Style.Font.Name = Font;
                    workSheet.Cells[RowExcel + RowGV, c].Style.Font.Size = 12;
                    workSheet.Cells[RowExcel + RowGV, c].Style.Font.Bold = true;
                    workSheet.Cells[RowExcel + RowGV, c].Style.Font.Color.SetColor(Color.Navy);
                    workSheet.Cells[RowExcel + RowGV, c].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    workSheet.Cells[RowExcel + RowGV, c].Style.WrapText = true;

                    if (c == 2 || c == 3)
                    {
                        workSheet.Cells[RowExcel + RowGV, c].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    }
                }

                //CỘT TỔNG
                workSheet.Column(ColumnGV - 2).Style.Font.Bold = true;
                workSheet.Column(ColumnGV - 1).Style.Font.Bold = true;
                workSheet.Column(ColumnGV).Style.Font.Bold = true;

                #endregion

                #region[4.Trộn ngang > dọc tự động tiêu đề column báo cáo]       
                MergeAutoExcel(workSheet, 7, 10, 1, ColumnGV);
                workSheet.Row(7).Height = 50; //Độ cao tiêu đề
                workSheet.Row(8).Height = 50; // Độ cao tiêu đề
                workSheet.Row(9).Height = 150; // Độ cao tiêu đề
                workSheet.Row(10).Height = 50; // Độ cao tiêu đề
                workSheet.Row(10).Style.WrapText = true;
                workSheet.Row(10).Style.VerticalAlignment = ExcelVerticalAlignment.Bottom;
                //workSheet.Row(11).Height = 40; // Độ cao tiêu đề

                //Trộn tuy chình
                //workSheet.Cells[8, ColumnGV, 10, ColumnGV].Merge = true;
                //workSheet.Cells[8, ColumnGV - 1, 10, ColumnGV - 1].Merge = true;
                //workSheet.Cells[8, ColumnGV - 2, 10, ColumnGV - 2].Merge = true;
                #endregion

                #region 1. TÊN BÁO CÁO EXCEL
                //Thêm các dường line excel
                workSheet.Cells.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                workSheet.Cells.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                workSheet.Cells.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                workSheet.Cells.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                // Dòng Tên báo cáo
                workSheet.Cells[4, 7].Value = TieuDe.ToUpper();
                workSheet.Cells[4, 7].Style.Font.Size = 16;
                workSheet.Cells[4, 7].Style.Font.Name = Font;
                workSheet.Cells[4, 7].Style.Font.Bold = true;
                workSheet.Cells[4, 7].Style.Font.Color.SetColor(Color.Navy);
                workSheet.Cells[4, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                // Xóa các đường line Từ dòng 1 đến dòng tiêu đề
                workSheet.Cells["1:6"].Style.Border.Left.Style = ExcelBorderStyle.None;
                workSheet.Cells["1:6"].Style.Border.Right.Style = ExcelBorderStyle.None;
                workSheet.Cells["1:6"].Style.Border.Top.Style = ExcelBorderStyle.None;
                workSheet.Cells["1:5"].Style.Border.Bottom.Style = ExcelBorderStyle.None;
                #endregion

                #region[6.Định dạng màu,độ rộng các cột fit]



                //workSheet.Row(7).Height = 45;
                //workSheet.Row(8).Height = 65;

                workSheet.Column(1).Width = 7;
                workSheet.Column(1).Style.Font.Color.SetColor(Color.Navy);
                workSheet.Column(2).Style.Font.Color.SetColor(Color.Navy);
                workSheet.Column(3).Style.Font.Color.SetColor(Color.Navy);

                workSheet.View.FreezePanes(11, 4);
                #endregion

                #region[7.Khung chữ ký]
                // Ngày ký
                workSheet.Cells[RowExcel + RowGV + 2, 13].Value = "Ngày " + dte_RpDate.Value.Day + " tháng " + dte_RpDate.Value.Month + " năm " + dte_RpDate.Value.Year;
                workSheet.Cells[RowExcel + RowGV + 2, 13].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 2, 13].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 2, 13].Style.Font.Color.SetColor(Color.Navy);

                //Tiêu đề Người lập bảng
                workSheet.Cells[RowExcel + RowGV + 3, 2].Value = "Lập bảng";
                workSheet.Cells[RowExcel + RowGV + 3, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 3, 2].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 3, 2].Style.Font.Color.SetColor(Color.Black);
                // Tên Người lập bảng
                workSheet.Cells[RowExcel + RowGV + 9, 2].Value = LapBang;
                workSheet.Cells[RowExcel + RowGV + 9, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 9, 2].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 9, 2].Style.Font.Color.SetColor(Color.Black);

                //Tiêu đề Kế toán trưởng
                workSheet.Cells[RowExcel + RowGV + 3, 5].Value = "Kế toán trưởng";
                workSheet.Cells[RowExcel + RowGV + 3, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 3, 5].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 3, 5].Style.Font.Color.SetColor(Color.Black);

                //Tên kế toán trưởng
                workSheet.Cells[RowExcel + RowGV + 9, 5].Value = KeToan;
                workSheet.Cells[RowExcel + RowGV + 9, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 9, 5].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 9, 5].Style.Font.Color.SetColor(Color.Black);

                //Tiêu đề BP HCNS
                workSheet.Cells[RowExcel + RowGV + 3, 9].Value = "BP HCNS";
                workSheet.Cells[RowExcel + RowGV + 3, 9].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 3, 9].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 3, 9].Style.Font.Color.SetColor(Color.Black);
                //Tên Trưởng phòng HCNS
                workSheet.Cells[RowExcel + RowGV + 9, 9].Value = NhanSu;
                workSheet.Cells[RowExcel + RowGV + 9, 9].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 9, 9].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 9, 9].Style.Font.Color.SetColor(Color.Black);

                //Tiêu đề Tổng giám đốc
                workSheet.Cells[RowExcel + RowGV + 3, 13].Value = "Tổng giám đốc";
                workSheet.Cells[RowExcel + RowGV + 3, 13].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 3, 13].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 3, 13].Style.Font.Color.SetColor(Color.Black);
                //Tên tổng giám đốc
                workSheet.Cells[RowExcel + RowGV + 9, 13].Value = GiamDoc;
                workSheet.Cells[RowExcel + RowGV + 9, 13].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 9, 13].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 9, 13].Style.Font.Color.SetColor(Color.Black);

                //Xóa đường line các dòng ký tên
                workSheet.Cells[(RowExcel + RowGV + 1) + ":" + (RowExcel + RowGV + 10)].Style.Border.Left.Style = ExcelBorderStyle.None;
                workSheet.Cells[(RowExcel + RowGV + 1) + ":" + (RowExcel + RowGV + 10)].Style.Border.Right.Style = ExcelBorderStyle.None;
                workSheet.Cells[(RowExcel + RowGV + 1) + ":" + (RowExcel + RowGV + 10)].Style.Border.Top.Style = ExcelBorderStyle.None;
                workSheet.Cells[(RowExcel + RowGV + 1) + ":" + (RowExcel + RowGV + 10)].Style.Border.Bottom.Style = ExcelBorderStyle.None;
                #endregion

                #region[8.Tiện ích Cài đặt in]
                workSheet.PrinterSettings.PaperSize = ePaperSize.A3;//Khổ giấy
                workSheet.PrinterSettings.PrintArea = workSheet.Cells[1, 1, RowExcel + RowGV + 9, ColumnGV];//Khu vực in
                workSheet.PrinterSettings.PageOrder = ePageOrder.OverThenDown;//Kiểu in chữ Z
                workSheet.PrinterSettings.RepeatRows = workSheet.Cells["7:10"];// Tiêu đề lặp lại khi qua trang khác
                //workSheet.PrinterSettings.RepeatColumns = workSheet.Cells["A:B"];// Column lặp lại khi qua trang khác
                workSheet.PrinterSettings.Orientation = eOrientation.Landscape;//Trang giấy nằm ngang
                workSheet.PrinterSettings.FitToPage = false;//In nhiu trang giấy
                workSheet.View.PageBreakView = true;
                workSheet.PrinterSettings.FitToHeight = 0;//Tự động cắt trang
                #endregion

                xlPackage.SaveAs(newFile);
            }
        }
        //trộn dòng cố định
        private void MergeDongExcel(ExcelWorksheet workSheet, int currentRow, int fromCol, int ToCol)
        {
            try
            {
                workSheet.Cells[currentRow, fromCol, currentRow, ToCol].Merge = true;
            }
            catch (Exception)
            {
                workSheet.Cells[currentRow, fromCol + 1, currentRow, ToCol].Merge = true;
            }
        }
        //trộn cột cố định
        private void MergeCotExcel(ExcelWorksheet workSheet, int currentColumn, int fromRow, int toRow)
        {
            try
            {
                workSheet.Cells[fromRow, currentColumn, toRow, currentColumn].Merge = true;
            }
            catch (Exception)
            {
                workSheet.Cells[fromRow + 1, currentColumn, toRow, currentColumn].Merge = true;
            }
        }
        //cấp 3 trộn kết hợp
        private void MergeAutoExcel(ExcelWorksheet workSheet, int fromRow, int toRow, int fromCol, int toCol)
        {
            string Val = workSheet.Cells[fromRow, fromCol].Value.ToString();
            int TronCot = 0;

            #region [Trộn cột :(dòng trên dòng dưới) ]

            for (int r = fromRow; r < toRow; r++)
            {
                for (int c = fromCol + 1; c < toCol; c++)
                {
                    string Dta = workSheet.Cells[r, c].Value.ToString();

                    if (Val == Dta)
                    {
                        TronCot++;
                    }
                    else
                    {
                        if (TronCot >= 1)
                        {
                            try
                            {
                                workSheet.Cells[r, c - TronCot - 1, r, c - 1].Merge = true;
                                TronCot = 0;
                            }
                            catch (Exception)
                            {

                            }
                        }
                        Val = Dta;
                    }
                }

                //trộn cuối cùng 
                if (Val == workSheet.Cells[r, toCol, r, toCol].Value.ToString())
                {
                    try
                    {
                        workSheet.Cells[r, toCol - TronCot - 1, r, toCol].Merge = true;
                        TronCot = 0;
                    }
                    catch (Exception)
                    {

                    }
                }
                else if (TronCot >= 1)
                {
                    try
                    {
                        workSheet.Cells[r, toCol - TronCot - 1, r, toCol - 1].Merge = true;
                        TronCot = 0;
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {

                }
            }
            #endregion

            #region [Trộn dòng :(cột trái cột phải)]

            for (int c = fromCol; c <= toCol; c++)
            {
                int TronDong = 0;
                Val = workSheet.Cells[fromRow, c].Value.ToString();
                for (int r = fromRow + 1; r <= toRow; r++)
                {
                    string Dta = workSheet.Cells[r, c].Value.ToString();
                    if (Val == Dta)
                    {
                        TronDong++;
                    }
                    else
                    {
                        Val = Dta;
                    }
                }

                if (TronDong > 0)
                {
                    try
                    {
                        workSheet.Cells[fromRow, c, fromRow + TronDong, c].Merge = true;
                        TronDong = 0;
                    }
                    catch (Exception)
                    {

                    }
                }
                if (c == toCol && Val == workSheet.Cells[fromRow, c].Value.ToString())
                {
                    try
                    {
                        workSheet.Cells[toRow - TronDong, toCol, toRow, toCol].Merge = true;
                        TronDong = 0;
                    }
                    catch (Exception)
                    {

                    }
                }

            }
            #endregion
        }
        #endregion
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                // this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
                btn_Export.Enabled = false;
            }
        }
        #endregion
    }
}
namespace TNS.WinApp.Frm_Report24_DLL
{
    public class Report_Data
    {
        public static DataTable ListEmployeeOfTeam(int TeamKey, DateTime FromDate, DateTime ToDate)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT EmployeeID FROM [dbo].[HRM_Employee] 
WHERE [dbo].[Fn_TeamKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate)= @TeamKey 
AND RecordStatus <> 99 
--AND StartingDate <= @FromDate   
AND (LeavingDate IS NULL OR LeavingDate >= @ToDate)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable Report_24(DateTime FromDate, DateTime ToDate, string MaCongNhan, string TeamKey) // nhóm thể hiện đã làm ỏ tổ gốc hoặc đã làm ở tổ khác
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            #region [SQL]
            string zSQL = @"
--declare @FromDate Datetime ='2021-04-01 00:00:00'
--declare @ToDate Datetime ='2021-04-10 23:59:59'

CREATE TABLE REPORT24 (
	[OrderDate] [datetime] NULL,
	[EmployeeName] [nvarchar](250) NULL,
	[EmployeeID] [nvarchar](250) NULL,
	[EmployeeKey] [nvarchar](250) NULL,
	[ParentProductKey] [nvarchar](250) NULL,
	[ProductKey] [nvarchar](250) NULL,
	[ProductID] [nvarchar](250) NULL,
	[ProductName] [nvarchar](250) NULL,
	[TIENDU] [float] NULL,
	[LK] [float] NULL
)


-- 1 GHI NĂNG XUẤT
INSERT INTO  REPORT24
SELECT 
	A.OrderDate,																		-- NGÀY ĐƠN HÀNG
	A.EmployeeName,																-- TÊN CÔNG NHÂN
	A.EmployeeID,																	-- MÃ CÔNG NHÂN
	A.EmployeeKey,																	-- KEY CÔNG NHÂN
	[dbo].[LayKeyNhomSanPham](C.ProductKey) AS ParentProductKey,
	C.ProductKey,	-- KEY SẢN PHẨM
	C.ProductID,
	C.ProductName,																	-- TÊN SẢN PHẨM																-- KEY CÔNG VIỆC															-- TÊN CÔNG VIỆC																			-- GIÁ CÔNG VIỆC
	0 AS TIENDU,																							-- TIEN GIO DU
	A.[Money] + A.MoneyPrivate + A.Money_Borrow AS LK								-- LƯƠNG KHOÁN
	FROM FTR_Order_Money A 
	LEFT JOIN FTR_Order C ON A.OrderKey = C.OrderKey
	LEFT JOIN IVT_Product_Stages D ON D.StageKey = A.StageKey
	WHERE 
	A.RecordStatus <> 99
	AND C.RecordStatus <> 99
	AND A.OrderDate BETWEEN @FromDate AND @ToDate 
	@CustomParamater
-- 2 CHI LẠI
INSERT INTO  REPORT24
SELECT
	A.OrderDate,																	-- NGÀY ĐƠN HÀNG
	A.EmployeeName,															-- TÊN CÔNG NHÂN
	A.EmployeeID,																-- MÃ CÔNG NHÂN
	A.EmployeeKey,																-- KEY CÔNG NHÂN
	[dbo].[LayKeyNhomSanPham](C.ProductKey)AS ParentProductKey,
	C.ProductKey,																	-- KEY SẢN PHẨM
	C.ProductID,
	C.ProductName,																-- TÊN SẢN PHẨM
	0 AS TIENDU,																							-- TIEN GIO DU
	(A.MoneyPersonal) AS LK																		-- LƯƠNG KHOÁN																		-- THÀNH PHẨM
	FROM FTR_Order_Adjusted A 
	LEFT JOIN FTR_Order C ON A.OrderKey = C.OrderKey
	LEFT JOIN IVT_Product_Stages D ON D.StageKey = A.StageKey
	WHERE
	A.RecordStatus <> 99
	AND C.RecordStatus <> 99
	AND A.OrderDate BETWEEN @FromDate AND @ToDate AND A.Share !=1
	@CustomParamater
-- 3 GIỜ DƯ
INSERT INTO  REPORT24
SELECT
	NULL AS OrderDate,												-- NGÀY NHẬP
	A.EmployeeName,															-- TÊN CÔNG NHÂN
	A.EmployeeID,																-- MÃ CÔNG NHÂN
	A.EmployeeKey,																-- KEY CÔNG NHÂN
	'' AS  ParentProductKey,
	'' AS ProductKey,															-- KEY SẢN PHẨM
	'' AS ProductID,
	'' AS ProductName,															-- TÊN SẢN PHẨM
	SUM(A.[Money]) AS TIENDU,																	-- TIEN GIO DU
	0 AS LK																								-- LƯƠNG KHOÁN																							    -- THÀNH PHẨM
	FROM [dbo].[Temp_Import_Detail] A
	WHERE
	A.RecordStatus <> 99
	@CustomParamater
	AND A.DateImport BETWEEN @FromDate AND @ToDate 
	GROUP BY A.EmployeeName, A.EmployeeID, A.EmployeeKey, A.TeamKey

SELECT
EmployeeName + '|' + EmployeeID AS [ORDER],
[dbo].[Get_ProductName](ParentProductKey)+'|'+ProductID+'|'+ ProductName  AS WORK,
SUM(TIENDU) TIENDU,
SUM(LK) LK
FROM REPORT24
GROUP BY EmployeeName, EmployeeID, EmployeeKey, ParentProductKey,[dbo].[Get_ProductName](ParentProductKey), ProductKey,ProductID, ProductName
ORDER BY LEN([dbo].[LaySapXep_NhomSanPham](ParentProductKey)), [dbo].[LaySapXep_NhomSanPham](ParentProductKey),ProductID 
DROP TABLE REPORT24

";
            #endregion

            zSQL = zSQL.Replace("@CustomParamater", " AND A.EmployeeID IN (" + MaCongNhan + ")");
           
            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.CommandTimeout = 1000;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

    }
}

#region [Thông tin hướng dẫn]
/*
 * KÍ HIỆU: REPORT 24
 * TÊN FORM: TỔNG LƯƠNG KHOÁN SẢN XUẤT CÔNG NHÂN THEO SẢN PHẨM
 * NGÀY TẠO: 05/06/2021
 * NGƯỜI TẠO: NGUYỄN AN ĐÔNG
 * NGÀY SỬA:
 * NGƯỜI SỬA:
 * KHÁCH YÊU CẦU: NGUYỄN VĂN NHỰT
 * ---------------------------
 * NỘI DUNG:
 * Báo cáo dùng để in (được rút gọn từ báo cáo Tổng lương khoán sản xuất công nhân , kí hiệu : Report 19_v2)
 * Làm báo cáo REPORT 24 từ báo cáo 19_V2 : coppy báo cáo 19 giữ lại tiền giờ dư, tiền các công đoạn
 * ---
 * Bỏ nhóm sản xuất
 * Thêm nhóm(parent) sản phẩm, 
 * Thêm mã sản phẩm
 * Thêm đơn giá giờ dư
 * Thêm số giờ dư
 * Giữ số lượng công đoạn
 * Tổng tiền  = tổng tiền các công đoạn + tiền giờ dư ( tất cả đã nhân hệ số)
 * ---
 * Quy trình:
 * Đổ dữ liệu danh sách công nhân: số giờ dư
 * Đỗ dữ liệu số lượng các công đoạn
 * Tổng tiền  = Tổng tiền các công đoạn + tiền giờ dư
 */
#endregion

