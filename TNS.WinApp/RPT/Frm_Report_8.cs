﻿using C1.Win.C1FlexGrid;
using System;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Windows.Forms;
using TNS.CORE;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Report_8 : Form
    {
        private int _TeamView = 0;
        public Frm_Report_8()
        {
            InitializeComponent();

            this.DoubleBuffered = true;
            this.Bounds = Screen.PrimaryScreen.WorkingArea;

            btn_Search.Click += btn_Search_Click;
            btn_Export.Click += btn_Export_Click;
            LoadDataToToolbox.KryptonComboBox(cbo_TeamID_Search, "SELECT TeamKey, TeamID + ' - ' + TeamName FROM SYS_Team WHERE RecordStatus <> 99 ORDER BY TeamID", "---Tất cả---");
        }

        private void Frm_Report_8_Load(object sender, EventArgs e)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;

            DateTime ViewDate = SessionUser.Date_Work;
            DateTime FromDate = new DateTime(ViewDate.Year, ViewDate.Month, 1, 0, 0, 0);
            DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            dte_FromDate.Value = FromDate;
            dte_ToDate.Value = ToDate;

        }


        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        private void btn_Search_Click(object sender, EventArgs e)
        {
            _TeamView = cbo_TeamID_Search.SelectedValue.ToInt();
            if (dte_ToDate.Value.Month != dte_FromDate.Value.Month || dte_ToDate.Value.Year != dte_FromDate.Value.Year)
            {
                Utils.TNMessageBox("Thông báo", "Bạn chỉ được chọn trong 1 tháng", 2);
                return;
            }
            try
            {
                using (Frm_Loading frm = new Frm_Loading(LoadData)) { frm.ShowDialog(this); }

            }
            catch (Exception ex)
            {
                Utils.TNMessageBox("Thông báo", ex.ToString(), 4);
            }

        }
        private void btn_Export_Click(object sender, EventArgs e)
        {
            string Path = "";
            FolderBrowserDialog FDialog = new FolderBrowserDialog();
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                Path = FDialog.SelectedPath + @"\ThongKeLuongKhoan" + dte_ToDate.Value.ToString("MM-yyyy") + ".xlsx";
                var newFile = new FileInfo(Path);

                if (newFile.Exists)
                {
                    newFile.Delete();
                }

                GVData.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                Process.Start(Path);
            }
        }

        void LoadData()
        {

            DataTable zTable = Report.No8(dte_FromDate.Value, dte_ToDate.Value,_TeamView);
            PivotTable zPvt = null;
            if (zTable.Rows.Count > 0)
            {
                this.Invoke(new MethodInvoker(delegate ()
                {
                    zPvt = new PivotTable(zTable);
                    if (rdoProduct.Checked)
                    {
                        DataTable TableView = zPvt.Generate("NgaySanXuat", "CongViec", new string[] { "SL", "LK" }, "CÔNG VIỆC", "TỔNG", "TỔNG", new string[] { "SL", "LK" }, 0);
                        InitGVProduct(TableView);
                    }

                    if (rdoTeam.Checked)
                    {
                        DataTable TableView = zPvt.Generate("CongViec", "NgaySanXuat", new string[] { "SL", "LK" }, "THỜI GIAN", "TỔNG", "TỔNG", new string[] { "SL", "LK" });
                        InitGVTeam(TableView);
                    }
                }));
            }
        }
        void InitGVProduct(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();

            int TotalRow = TableView.Rows.Count + 2;
            int ToTalCol = TableView.Columns.Count + 2;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(TotalRow);

            //Row Header           

            GVData.Rows[0][0] = "SẢN PHẨM";
            GVData.Rows[0][1] = "CÔNG VIỆC";
            GVData.Rows[0][2] = "ĐƠN GIÁ";
            GVData.Rows[1][0] = "SẢN PHẨM";
            GVData.Rows[1][1] = "CÔNG VIỆC";
            GVData.Rows[1][2] = "ĐƠN GIÁ";

            int ColStart = 3;
            for (int i = 1; i < TableView.Columns.Count; i++)
            {
                DataColumn Col = TableView.Columns[i];
                string[] strCaption = Col.ColumnName.Split('|');

                GVData.Rows[0][ColStart] = strCaption[0];           //Ten Nhom
                GVData.Rows[1][ColStart] = strCaption[1];           //SL - LK                  

                ColStart++;
            }

            ColStart = 2;
            int RowStart = 2;//add dòng từ số thứ tự 2 trở đi
            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                DataRow rData = TableView.Rows[rIndex];
                string[] strTemp = rData[0].ToString().Split('|');

                if (strTemp.Length > 2)
                {
                    GVData.Rows[rIndex + RowStart][0] = strTemp[0];
                    GVData.Rows[rIndex + RowStart][1] = strTemp[1];
                    GVData.Rows[rIndex + RowStart][2] = strTemp[2];
                }
                else
                {
                    GVData.Rows[rIndex + RowStart][0] = strTemp[0];
                    GVData.Rows[rIndex + RowStart][1] = strTemp[0];
                    GVData.Rows[rIndex + RowStart][2] = strTemp[0];
                }

                for (int cIndex = 1; cIndex < TableView.Columns.Count; cIndex++)
                {
                    GVData.Rows[RowStart + rIndex][ColStart + cIndex] = FormatMoney(rData[cIndex]);
                }
            }

            //Style
            GVData.AutoSizeCols();
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.Free;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.CenterCenter;
            //
            GVData.Rows[0].AllowMerging = true;
            GVData.Rows[0].Height = 40;
            GVData.Rows[1].AllowMerging = true;
            GVData.Rows[1].Height = 40;

            GVData.Rows[TotalRow - 1].AllowMerging = true;

            GVData.Cols[0].AllowMerging = true;
            GVData.Cols[0].Width = 100;

            GVData.Cols[1].AllowMerging = true;
            GVData.Cols[1].Width = 330;

            GVData.Cols[2].AllowMerging = true;
            GVData.Cols[2].Width = 80;

            //Freeze Row and Column
            GVData.Rows.Fixed = 2;
            GVData.Cols.Fixed = 3;

            GVData.AutoSizeCols(1, GVData.Cols.Count - 1, 10);
            GVData.Cols[1].TextAlign = TextAlignEnum.LeftCenter;
            //Canh phải các cột từ số 3 trở đi
            for (int i = 3; i < GVData.Cols.Count; i++)
            {
                GVData.Cols[i].TextAlign = TextAlignEnum.RightCenter;
            }
        }

        void InitGVTeam(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();

            int TotalRow = TableView.Rows.Count + 4;
            int ToTalCol = TableView.Columns.Count;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(TotalRow);

            CellRange zCellRange = GVData.GetCellRange(0, 0, 3, 0);
            zCellRange.Data = "NGÀY SẢN XUẤT";

            //Row Header
            int ColStart = 1;
            for (int i = 1; i < TableView.Columns.Count; i++)
            {
                DataColumn Col = TableView.Columns[i];
                string[] strCaption = Col.ColumnName.Split('|');
                if (strCaption.Length > 2)
                {
                    GVData.Rows[0][ColStart] = strCaption[0];           //Ten Nhom SP
                    GVData.Rows[1][ColStart] = strCaption[1];           //Ten Cong Viec
                    GVData.Rows[2][ColStart] = strCaption[2];           //Don Gia
                    GVData.Rows[3][ColStart] = strCaption[3];
                }
                else
                {
                    CellRange CellRange = GVData.GetCellRange(1, ColStart, 3, ColStart);
                    CellRange.Data = strCaption[1];
                }
                ColStart++;
            }

            //Row Data
            int RowStart = 4;//add dòng từ số thứ tự 4 trở đi
            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                DataRow rData = TableView.Rows[rIndex];
                for (int cIndex = 0; cIndex < TableView.Columns.Count; cIndex++)
                {
                    if (cIndex >= 1)
                    {
                        GVData.Rows[RowStart + rIndex][cIndex] = FormatMoney(rData[cIndex]);
                    }
                    else
                    {
                        GVData.Rows[RowStart + rIndex][cIndex] = rData[cIndex];
                    }
                }
            }

            zCellRange = GVData.GetCellRange(0, ToTalCol - 2, 0, ToTalCol - 1);
            zCellRange.Data = "TỔNG";

            //Style

            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.Free;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.CenterCenter;
            GVData.Styles.Normal.WordWrap = true;

            //Trộn dòng 0
            GVData.Rows[0].AllowMerging = true;
            GVData.Rows[0].Height = 40;
            //Trộn dòng 1
            GVData.Rows[1].AllowMerging = true;
            GVData.Rows[1].Height = 60;
            //Trộn dòng 2
            GVData.Rows[2].AllowMerging = true;

            //Trộn cột đầu
            GVData.Cols[0].AllowMerging = true;
            GVData.Cols[0].Width = 200;
            //Trộn 2 cột cuối
            GVData.Cols[ToTalCol - 1].AllowMerging = true;
            GVData.Cols[ToTalCol - 2].AllowMerging = true;

            //Freeze Row and Column
            GVData.Rows.Fixed = 4;
            GVData.Cols.Fixed = 1;

            //GVData.AutoSizeCols(1, GVData.Cols.Count - 1, 10);
            //Canh phải các cột từ số 1 trở đi
            GVData.Cols[0].TextAlign = TextAlignEnum.LeftCenter;
            for (int i = 1; i < GVData.Cols.Count; i++)
            {
                GVData.Cols[i].TextAlign = TextAlignEnum.RightCenter;
            }

            //for (int i = 2; i < GVData.Rows.Count; i++)
            //{
            //    GVData.Rows[i].Height = 27;                
            //}
        }

        string FormatMoney(object input)
        {
            try
            {
                if (input.ToString() != "0")
                {
                    double zResult = double.Parse(input.ToString());
                    return zResult.ToString("n", CultureInfo.GetCultureInfo("vi-VN"));
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
    }
}
