﻿using C1.Win.C1FlexGrid;
using System;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;
using TNS.CORE;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Rpt_43 : Form
    {
        string _TeamID = "";
        public Frm_Rpt_43()
        {
            InitializeComponent();
            this.Load += Frm_Rpt_43_Load;
            this.DoubleBuffered = true;
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;
            btnClose.Click += btnClose_Click;
        }

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        private void Frm_Rpt_43_Load(object sender, EventArgs e)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;

            DateTime ViewDate = SessionUser.Date_Work;
            DateTime FromDate = new DateTime(ViewDate.Year, ViewDate.Month, 1, 0, 0, 0);
            DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            dte_FromDate.Value = FromDate;
            dte_ToDate.Value = ToDate;

            LoadDataToToolbox.KryptonComboBox(cbo_TeamID, @"
SELECT TeamKey, TeamID + '-' + TeamName AS TeamName  
FROM SYS_Team
WHERE RecordStatus<> 99 AND BranchKey = 4 AND DepartmentKey != 26
ORDER BY Rank", "--Tất cả--");

        }

        private void btn_Search_Click(object sender, EventArgs e)
        {
            _TeamID = cbo_TeamID.SelectedValue.ToString();
            using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }
        }

        private void btn_Export_Click(object sender, EventArgs e)
        {

        }

        private void DisplayData()
        {

            DataTable zTable = Report.Report_43(dte_FromDate.Value, dte_ToDate.Value, _TeamID);
            PivotTable zPvt = null;

            if (zTable.Rows.Count > 0)
            {
                zPvt = new PivotTable(zTable);
                DataTable TableView = zPvt.Generate("CongViec", "SanPham", "LK", "SẢN PHẨM", "TỔNG", "TỔNG", 0);

                this.Invoke(new MethodInvoker(delegate ()
                {
                    InitGVProduct(TableView);
                }));
            }
        }

        void InitGVProduct(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();

            int TotalRow = TableView.Rows.Count + 4;
            int ToTalCol = TableView.Columns.Count + 1;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(TotalRow);

            CellRange zCellRange = GVData.GetCellRange(0, 0, 3, 0);
            zCellRange.Data = "STT";

            zCellRange = GVData.GetCellRange(0, 1, 3, 1);
            zCellRange.Data = "SẢN PHẨM";

            //Row Header
            int ColStart = 2;
            for (int i = 1; i < TableView.Columns.Count; i++)
            {
                DataColumn Col = TableView.Columns[i];
                string[] strCaption = Col.ColumnName.Split('|');
                if (strCaption.Length > 2)
                {
                    GVData.Rows[0][ColStart] = strCaption[0];           //Ten Nhom công nhân
                    GVData.Rows[1][ColStart] = strCaption[1];           //Ten Cong Viec
                    GVData.Rows[2][ColStart] = strCaption[2];           //Don Gia
                    GVData.Rows[3][ColStart] = "Lương Khoán";
                }
                else
                {
                    CellRange CellRange = GVData.GetCellRange(1, ColStart, 3, ColStart);
                    CellRange.Data = strCaption[0];
                }
                ColStart++;
            }

            ColStart = 1;       // add data vào cột thứ 2 trở đi
            int RowStart = 4;//add dòng từ số thứ tự 4 trở đi
            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                DataRow rData = TableView.Rows[rIndex];

                GVData.Rows[rIndex + RowStart][0] = rIndex + 1;
                GVData.Rows[rIndex + RowStart][1] = rData[0];

                for (int cIndex = 1; cIndex < TableView.Columns.Count; cIndex++)
                {
                    GVData.Rows[RowStart + rIndex][ColStart + cIndex] = FormatMoney(rData[cIndex]);
                }
            }

            //TỔNG DÒNG
            zCellRange = GVData.GetCellRange(TotalRow - 1, 0, TotalRow - 1, 1);
            zCellRange.Data = "TỔNG";

            //Style
            GVData.ExtendLastCol = false;
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.FixedOnly;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.CenterCenter;
            GVData.Styles.Normal.WordWrap = true;

            //Trộn dòng 0
            GVData.Rows[0].AllowMerging = true;
            GVData.Rows[0].Height = 40;
            //Trộn dòng 1
            GVData.Rows[1].AllowMerging = true;
            GVData.Rows[1].Height = 60;
            //Trộn dòng 2
            GVData.Rows[2].AllowMerging = true;
            //Trộn dòng cuối
            GVData.Rows[TotalRow - 1].AllowMerging = true;

            //Trộn cột đầu
            GVData.Cols[0].AllowMerging = true;
            GVData.Cols[0].Width = 35;
            GVData.Cols[1].AllowMerging = true;
            GVData.Cols[1].Width = 100;
            //Trộn 1 cột cuối
            GVData.Cols[ToTalCol - 1].AllowMerging = true;

            //Freeze Row and Column
            GVData.Rows.Fixed = 4;
            GVData.Cols.Fixed = 2;

            //GVData.AutoSizeCols(1, GVData.Cols.Count - 1, 10);
            //Canh phải các cột từ số 1 trở đi
            GVData.Cols[0].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[1].TextAlign = TextAlignEnum.LeftCenter;          
            for (int i = 2; i < GVData.Cols.Count; i++)
            {
                GVData.Cols[i].TextAlign = TextAlignEnum.RightCenter;
            }
        }

        string FormatMoney(object input)
        {
            try
            {
                if (input.ToString() != "0")
                {
                    double zResult = double.Parse(input.ToString());
                    return zResult.ToString("n", CultureInfo.GetCultureInfo("vi-VN"));
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
    }
}
