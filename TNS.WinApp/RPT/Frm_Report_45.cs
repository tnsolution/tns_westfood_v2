﻿using C1.Win.C1FlexGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.CORE;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Report_45 : Form
    {
        int _ProductGroup = 0;
        int _StageKey = 0;
        float _SotienChuan = 0;
        float _GioHanhChanh = 0;
        public Frm_Report_45()
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;
            btnClose.Click += btnClose_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            btn_Search.Click += btn_Search_Click;
            btn_Export.Click += Btn_Export_Click;
            btn_Hide.Click += Btn_Hide_Click;
            btn_Show.Click += Btn_Show_Click;
            txt_Money.KeyUp += Txt_Money_KeyUp;
            txt_Hour.Leave += Txt_Hour_Leave;

            splitContainer1.SplitterDistance = 540;
        }

        private void Txt_Hour_Leave(object sender, EventArgs e)
        {
            double zAmount = 0;
            if (double.TryParse(txt_Hour.Text.Trim(), out zAmount))
            {
                txt_Hour.Text = zAmount.Toe1String();
                txt_Hour.Focus();
                txt_Hour.SelectionStart = txt_Hour.Text.Length;
            }
            else
            {
                MessageBox.Show("Lỗi!.Định dạng số.");
            }
        }

        private void Txt_Money_KeyUp(object sender, KeyEventArgs e)
        {
            double zAmount = 0;
            double.TryParse(txt_Money.Text.Trim(), out zAmount);
            txt_Money.Text = zAmount.Ton0String();
            txt_Money.Focus();
            txt_Money.SelectionStart = txt_Money.Text.Length;
        }

        private void Frm_Report_45_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            Check_RoleForm();

            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.DoubleClick += GVData_SelChange;

            GVDetail.Rows.Count = 0;
            GVDetail.Cols.Count = 0;

            DateTime ViewDate = SessionUser.Date_Work;
            DateTime FromDate = new DateTime(ViewDate.Year, ViewDate.Month, 1, 0, 0, 0);
            DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            dte_FromDate.Value = FromDate;
            dte_ToDate.Value = ToDate;

            LoadDataToToolbox.KryptonComboBox(cbo_ProductID, @"
SELECT GroupKey, GroupName 
FROM IVT_Product_Stages_Group
WHERE RecordStatus <> 99
ORDER BY GroupID", "--Tất cả--");
            cbo_ProductID.SelectedIndex = 0;
            cbo_ProductID.SelectedValueChanged += Cbo_ProductID_SelectedValueChanged;
        }
        private void Btn_Show_Click(object sender, EventArgs e)
        {
            splitContainer1.SplitterDistance = 540;
            btn_Hide.Visible = true;
            btn_Show.Visible = false;
        }
        private void Btn_Hide_Click(object sender, EventArgs e)
        {
            splitContainer1.SplitterDistance = 120;
            btn_Hide.Visible = false;
            btn_Show.Visible = true;
        }
        private void Cbo_ProductID_SelectedValueChanged(object sender, EventArgs e)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            _ProductGroup = cbo_ProductID.SelectedValue.ToInt();
            using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }
            _StageKey = 0;
        }
        private void GVData_SelChange(object sender, EventArgs e)
        {
            if (GVData.RowSel > 0)
            {
                GVDetail.Rows.Count = 0;
                GVDetail.Cols.Count = 0;
                float zHour = 0;
                float zMoney = 0;
                if (float.TryParse(txt_Hour.Text.Trim(), out zHour))
                {

                }
                if (float.TryParse(txt_Money.Text.Trim(), out zMoney))
                {

                }
                _StageKey = GVData.Rows[GVData.RowSel][5].ToInt();
                _GioHanhChanh = zHour;
                _SotienChuan = zMoney;
                using (Frm_Loading frm = new Frm_Loading(DisplayDetail)) { frm.ShowDialog(this); }
            }
        }
        private void Btn_Export_Click(object sender, EventArgs e)
        {
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Thong_Ke_Nang_Suat_Trung_Binh_Tung_Cong_Doan" + dte_FromDate.Value.ToString("MM-yyyy") + ".xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        MessageBox.Show("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }
                    GVDetail.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }

        }
        private void btn_Search_Click(object sender, EventArgs e)
        {
            GVDetail.Rows.Count = 0;
            GVDetail.Cols.Count = 0;
            if (_StageKey > 0)
            {
                float zHour = 0;
                float zMoney = 0;
                if (float.TryParse(txt_Hour.Text.Trim(), out zHour))
                {

                }
                if (float.TryParse(txt_Money.Text.Trim(), out zMoney))
                {

                }

                _GioHanhChanh = zHour;
                _SotienChuan = zMoney;
                using (Frm_Loading frm = new Frm_Loading(DisplayDetail)) { frm.ShowDialog(this); }
            }
            else
            {

                Utils.TNMessageBoxOK("Vui lòng chọn công đoạn!",1);
            }


        }
        private void DisplayData()
        {
            DataTable zTable = Report.ListStageOfGroup(_ProductGroup);

            if (zTable.Rows.Count > 0)
            {
                this.Invoke(new MethodInvoker(delegate ()
                {
                    InitDataMaster(zTable);
                }));
            }
        }
        private void DisplayDetail()
        {
            DataTable zTable = Report.No45(dte_FromDate.Value, dte_ToDate.Value, _StageKey, _SotienChuan, _GioHanhChanh);

            if (zTable.Rows.Count > 0)
            {
                this.Invoke(new MethodInvoker(delegate ()
                {
                    InitDataDetail(zTable);
                }));
            }
        }
        void InitDataMaster(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();

            int TotalRow = TableView.Rows.Count + 1;
            int ToTalCol = TableView.Columns.Count + 1;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(TotalRow);

            //Row Header
            GVData.Rows[0][0] = "STT";
            GVData.Rows[0][1] = "SẢN PHẨM";
            GVData.Rows[0][2] = "CÔNG ĐOẠN";
            GVData.Rows[0][3] = "MÃ CÔNG ĐOẠN";
            GVData.Rows[0][4] = "ĐƠN GIÁ";
            GVData.Rows[0][5] = ""; // ẩn cột này

            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                DataRow rData = TableView.Rows[rIndex];

                GVData.Rows[rIndex + 1][0] = rIndex + 1;
                GVData.Rows[rIndex + 1][1] = rData[1];
                GVData.Rows[rIndex + 1][2] = rData[2];
                GVData.Rows[rIndex + 1][3] = rData[3];

                GVData.Rows[rIndex + 1][4] = rData[4].Toe1String();
                GVData.Rows[rIndex + 1][5] = rData[0];
            }

            //Style         
            GVData.AllowFreezing = AllowFreezingEnum.Both;
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.FixedOnly;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.CenterCenter;
            GVData.Styles.Normal.WordWrap = true;

            GVData.Rows[0].Height = 40;

            GVData.Cols[0].Width = 40;
            GVData.Cols[1].Width = 80;
            GVData.Cols[1].AllowMerging = true;
            GVData.Cols[2].Width = 270;
            GVData.Cols[3].Width = 70;
            GVData.Cols[4].Width = 60;
            GVData.Cols[5].Visible = false;

            //Freeze Row and Column                              
            GVData.Rows.Fixed = 1;
            GVData.Cols.Fixed = 5;

            GVData.Cols[0].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[1].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[2].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[3].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[4].TextAlign = TextAlignEnum.RightCenter;

        }
        void InitDataDetail(DataTable TableView)
        {
            GVDetail.Rows.Count = 0;
            GVDetail.Cols.Count = 0;
            GVDetail.Clear();

            int TotalRow = TableView.Rows.Count + 3;
            int ToTalCol = TableView.Columns.Count + 1;

            GVDetail.Cols.Add(ToTalCol);
            GVDetail.Rows.Add(TotalRow);

            //Row Header
            GVDetail.Rows[0][0] = "STT";
            GVDetail.Rows[0][1] = "Tên công đoạn";
            GVDetail.Rows[0][2] = "Mã";
            GVDetail.Rows[0][3] = "Ngày";
            GVDetail.Rows[0][4] = "Số Người";
            GVDetail.Rows[0][5] = "Tổng Thời Gian";
            GVDetail.Rows[0][6] = "Tổng Số Lượng Thành Phẩm";
            GVDetail.Rows[0][7] = "Công Suất (Thành phẩm /Số giờ)";
            GVDetail.Rows[0][8] = "Đơn giá";

            double TotalCol3 = 0;
            double TotalCol4 = 0;
            double TotalCol5 = 0;
            double TotalCol6 = 0;
            double TotalCol7 = 0;

            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                DataRow rData = TableView.Rows[rIndex];

                GVDetail.Rows[rIndex + 1][0] = rIndex + 1;
                GVDetail.Rows[rIndex + 1][1] = rData[0];
                GVDetail.Rows[rIndex + 1][2] = rData[1];
                DateTime zDatetime = DateTime.Parse(rData[2].ToString());

                GVDetail.Rows[rIndex + 1][3] = zDatetime.ToString("dd/MM/yyyy");
                GVDetail.Rows[rIndex + 1][4] = rData[3];
                GVDetail.Rows[rIndex + 1][5] = rData[4].Toe2String();
                GVDetail.Rows[rIndex + 1][6] = rData[5].Toe2String();
                GVDetail.Rows[rIndex + 1][7] = rData[6].Toe2String();
                GVDetail.Rows[rIndex + 1][8] = rData[7].Toe2String();

                double Col3 = 0;
                double Col4 = 0;
                double Col5 = 0;
                double Col6 = 0;
                double Col7 = 0;
                if (double.TryParse(rData[3].ToString(), out Col3))
                {

                }
                if (double.TryParse(rData[4].ToString(), out Col4))
                {

                }
                if (double.TryParse(rData[5].ToString(), out Col5))
                {

                }
                if (double.TryParse(rData[6].ToString(), out Col6))
                {

                }
                if (double.TryParse(rData[7].ToString(), out Col7))
                {

                }

                TotalCol3 += Col3;
                TotalCol4 += Col4;
                TotalCol5 += Col5;
                TotalCol6 += Col6;
                TotalCol7 += Col7;
                //if (rData[7].ToInt() == 1)
                //{
                //    GVEmployee.Rows[rIndex + 1].StyleNew.BackColor = Color.LemonChiffon;
                //}
            }
            //Row trung bình
            GVDetail.Rows[TotalRow - 2][1] = "TRUNG BÌNH";
            GVDetail.Rows[TotalRow - 2][4] = (TotalCol3 / TableView.Rows.Count).ToString("n0");
            GVDetail.Rows[TotalRow - 2][5] = (TotalCol6 / TableView.Rows.Count).Toe2String();
            GVDetail.Rows[TotalRow - 2][6] = (TotalCol5 / TableView.Rows.Count).Toe2String();
            GVDetail.Rows[TotalRow - 2][7] = (TotalCol6 / TableView.Rows.Count).Toe2String();
            GVDetail.Rows[TotalRow - 2][8] = (TotalCol7 / TableView.Rows.Count).Toe2String();
            GVDetail.Rows[TotalRow - 2].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            //Row Tổng
            GVDetail.Rows[TotalRow - 1][1] = "TỔNG";
            GVDetail.Rows[TotalRow - 1][4] = TotalCol3;
            GVDetail.Rows[TotalRow - 1][5] = TotalCol4.Toe2String();
            GVDetail.Rows[TotalRow - 1][6] = TotalCol5.Toe2String();
            GVDetail.Rows[TotalRow - 1][7] = TotalCol6.Toe2String();
            GVDetail.Rows[TotalRow - 1][8] = TotalCol7.Toe2String();
            GVDetail.Rows[TotalRow - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            //Style         
            GVDetail.AllowFreezing = AllowFreezingEnum.Both;
            GVDetail.AllowResizing = AllowResizingEnum.Both;
            GVDetail.AllowMerging = AllowMergingEnum.FixedOnly;
            GVDetail.SelectionMode = SelectionModeEnum.Row;
            GVDetail.VisualStyle = VisualStyle.Office2010Blue;
            GVDetail.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVDetail.Styles.Normal.TextAlign = TextAlignEnum.CenterCenter;
            GVDetail.Styles.Normal.WordWrap = true;

            GVDetail.Rows[0].Height = 65;

            GVDetail.Cols[0].Width = 40;
            GVDetail.Cols[1].Width = 300;
            GVDetail.Cols[1].AllowMerging = true;
            GVDetail.Cols[2].AllowMerging = true;
            GVDetail.Cols[2].Width = 80;

            //Freeze Row and Column                              
            GVDetail.Rows.Fixed = 1;
            GVDetail.Cols.Fixed = 4;

            GVDetail.Cols[0].TextAlign = TextAlignEnum.LeftCenter;
            for (int i = 3; i < GVDetail.Cols.Count; i++)
            {
                GVDetail.Cols[i].TextAlign = TextAlignEnum.RightCenter;
            }
        }
        string FormatMoney(object input)
        {
            try
            {
                if (input.ToString() != "0")
                {
                    double zResult = double.Parse(input.ToString());
                    return zResult.ToString("n", CultureInfo.GetCultureInfo("vi-VN"));
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                //this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
                btn_Export.Enabled = false;
            }
        }
        #endregion
    }
}
