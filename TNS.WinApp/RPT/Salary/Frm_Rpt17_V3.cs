﻿using C1.Win.C1FlexGrid;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.CORE;
using TNS.LOG;
using TNS.Misc;
using TNS.SLR;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Rpt17_V3 : Form
    {
        private int _TeamKeyClose = 0;
        private int _DepartmentKeyClose = 0;
        private DateTime _DateClose;
        private int _STT = 0;
        private string _IDSalary = "";
        private int _Pay = 0;
        public Frm_Rpt17_V3()
        {
            InitializeComponent();
            btn_Search.Click += Btn_Search_Click;
            btnClose.Click += btnClose_Click;
            btnMax.Click += btnMax_Click;
            btnMini.Click += btnMini_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            btn_Print.Click += Btn_Print_Click;
            btnClose_Panel_Message.Click += BtnClose_Panel_Message_Click;
            btn_Done.Click += Btn_Done_Click;
            btn_Export.Click += Btn_Export_Click;
            btn_Fillter.Click += Btn_Fillter_Click;
            chk_All.CheckedChanged += Chk_All_CheckedChanged;
            cbo_Department.SelectedIndexChanged += Cbo_Department_SelectedIndexChanged;
            rdo_Team.CheckedChanged += Rdo_Team_CheckedChanged;
            dteDate.Value = SessionUser.Date_Work;
            dteDate.ValueChanged += DteDate_ValueChanged;
            InitLayout_LV(LVEmployee);

            Utils.DrawLVStyle(ref LVEmployee);
            Utils.SizeLastColumn_LV(LVEmployee);
            LoadDataToToolbox.KryptonComboBox(cbo_Department, "SELECT DepartmentKey, DepartmentName FROM[dbo].[SYS_Department] WHERE DepartmentKey != 98  AND RecordStatus< 99 AND BranchKey= 2", "---- Chọn tất cả----");


        }

        private void Frm_Rpt17_V3_Load(object sender, EventArgs e)
        {
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            Check_RoleForm();

            InitData(true);
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            panel_left.Visible = false;
            // Khởi tạo thông tin báo cáo
            LoadInfo_Report();
            Panel_Done.Visible = false;
        }
        private void DteDate_ValueChanged(object sender, EventArgs e)
        {
            InitData(true);
        }
        private void Cbo_Department_SelectedIndexChanged(object sender, EventArgs e)
        {

            LoadDataToToolbox.KryptonComboBox(cboTeam, "SELECT TeamKey,TeamName FROM SYS_Team WHERE TeamKey != 98 AND DepartmentKey = " + cbo_Department.SelectedValue.ToInt() + "  AND RecordStatus < 99 ORDER BY Rank", "---- Chọn tất cả ----");
        }
        private void Rdo_Team_CheckedChanged(object sender, EventArgs e)
        {
            if (rdo_Team.Checked == true)
            {
                txt_Search.Enabled = false;
            }
            else
            {
                txt_Search.Enabled = true;
            }
        }
        private void Chk_All_CheckedChanged(object sender, EventArgs e)
        {
            if (chk_All.Checked == true)
                InitData(true);
            else
                InitData(false);
        }
        private void Btn_Fillter_Click(object sender, EventArgs e)
        {
            if (btn_Fillter.Tag.ToString() == "0")
            {
                btn_Fillter.Text = "Ẩn lọc";
                btn_Fillter.Tag = 1;
                panel_left.Visible = true;
            }
            else
            {
                btn_Fillter.Text = "Mở lọc";
                btn_Fillter.Tag = 0;
                panel_left.Visible = false;
            }
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            if (cbo_Department.SelectedIndex > 0)
                _DepartmentKeyClose = cbo_Department.SelectedValue.ToInt();
            else
                _DepartmentKeyClose=0;
            if (cboTeam.SelectedIndex > 0)
                _TeamKeyClose = cboTeam.SelectedValue.ToInt();
            else
                _TeamKeyClose = 0;
            if (rdo_PayAll.Checked == true)
                _Pay = 0;
            if (rdo_ATM.Checked == true)
                _Pay = 1;
            if (rdo_Cash.Checked == true)
                _Pay = 2;

            _DateClose = dteDate.Value;
            _IDSalary = GetSalaryID();
            if (_IDSalary == "")
            {
               Utils.TNMessageBoxOK("Vui lòng chọn ít nhất 1 chỉ tiêu lương!",1);
                return;
            }
            string Status = "Tìm DL form " + HeaderControl.Text + " > tháng: " + dteDate.Value.ToString("MM/yyyy") + " > Nhóm:" + cboTeam.Text + " > Từ khóa:" + txt_Search.Text;
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);

            try
            {
                if (rdo_Employee.Checked == true)
                    using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }
                else
                    using (Frm_Loading frm = new Frm_Loading(DisplayData_ToNhom)) { frm.ShowDialog(this); }
            }
            catch (Exception ex)
            {
                Utils.TNMessageBoxOK( ex.ToString(), 4);
            }
        }
        private string GetSalaryID()
        {
            string zMesage = "";
            if (LVEmployee.Items.Count > 0)
            {
                int zPlag = 0;
                zMesage += "( ";
                for (int i = 0; i < LVEmployee.Items.Count; i++)
                {
                    if (LVEmployee.Items[i].Checked)
                    {
                        zMesage += "A.CodeID ='" + LVEmployee.Items[i].Tag + "' OR ";
                        zPlag++;
                    }
                }
                if (zPlag != 0)
                {
                    zMesage = zMesage.Remove(zMesage.Length - 3, 3);
                    zMesage += " )";
                }
                else
                    zMesage = "";
            }
            return zMesage;

        }

        #region[Nhân sự]
        private void DisplayData()
        {
            DataTable zTable = new DataTable();
            if (rdo_Default.Checked)
            {
                zTable = Report.No17_NHANSUMACDINH(_DateClose, _DepartmentKeyClose, _TeamKeyClose, txt_Search.Text, _IDSalary,_Pay);
            }
            else
            {
                zTable = Report.No17_NHANSUQUANLY(_DateClose, _DepartmentKeyClose, _TeamKeyClose, txt_Search.Text, _IDSalary,_Pay);
            }
            if (zTable.Rows.Count > 0)
            {
                PivotTable zPvt = new PivotTable(zTable);
                DataTable zTablePivot = zPvt.Generate_ASC("HeaderColumn", "LeftColumn", new string[] { "Amount" }, "CÔNG VIỆC", "TỔNG CỘNG", "TỔNG CỘNG", new string[] { "Sô tiền" }, 0);
                //Tách cột Leftcolumn
                zTablePivot.Columns.Add("EmployeeKey").SetOrdinal(1);
                zTablePivot.Columns.Add("EmpoyeeName").SetOrdinal(2);
                zTablePivot.Columns.Add("EmpoyeeID").SetOrdinal(3);
                zTablePivot.Columns.Add("PositionName").SetOrdinal(4);
                zTablePivot.Columns.Add("TeamKey", typeof(int)).SetOrdinal(5);
                zTablePivot.Columns.Add("TeamName").SetOrdinal(6);
                zTablePivot.Columns.Add("ATM").SetOrdinal(7);
                zTablePivot.Columns.Add("EmployeeRank");
                zTablePivot.Columns.Add("BranchRank");
                zTablePivot.Columns.Add("DepartmentRank");
                zTablePivot.Columns.Add("TeamRank");
                for (int i = 0; i < zTablePivot.Rows.Count - 1; i++)
                {
                    string[] temp = zTablePivot.Rows[i][0].ToString().Split('|');
                    zTablePivot.Rows[i]["EmployeeKey"] = temp[0];
                    zTablePivot.Rows[i]["EmpoyeeName"] = temp[1];
                    zTablePivot.Rows[i]["EmpoyeeID"] = temp[2];
                    zTablePivot.Rows[i]["PositionName"] = temp[3];
                    zTablePivot.Rows[i]["TeamKey"] = temp[4];
                    zTablePivot.Rows[i]["TeamName"] = temp[5];
                    zTablePivot.Rows[i]["ATM"] = temp[6];
                    zTablePivot.Rows[i]["EmployeeRank"] = ConvertIDRank(temp[2]);
                    zTablePivot.Rows[i]["BranchRank"] = temp[7];
                    zTablePivot.Rows[i]["DepartmentRank"] = temp[8];
                    zTablePivot.Rows[i]["TeamRank"] = temp[9];
                }
                //Xóa cột đầu đã cắt chuỗi và xóa dòng tổng cuối
                zTablePivot.Columns.Remove("CÔNG VIỆC");
                DataRow row = zTablePivot.Rows[zTablePivot.Rows.Count - 1];
                zTablePivot.Rows.Remove(row);
                //Sắp xếp nhân viên
                DataView dv = zTablePivot.DefaultView;
                dv.Sort = " BranchRank ASC,DepartmentRank ASC,TeamRank ASC,  EmployeeRank ASC";
                zTablePivot = dv.ToTable();
                zTablePivot.Columns.Remove("EmployeeRank");
                zTablePivot.Columns.Remove("BranchRank");
                zTablePivot.Columns.Remove("DepartmentRank");
                zTablePivot.Columns.Remove("TeamRank");




                this.Invoke(new MethodInvoker(delegate ()
                {
                    InitGVProduct(zTablePivot);
                    DataTable _Intable = zTablePivot;
                    _STT = 1;
                    int NoGroup = 3;
                    int RowTam = NoGroup;
                    Row zGvRow;
                    int TeamKey = _Intable.Rows[0]["TeamKey"].ToInt();
                    string TeamName = _Intable.Rows[0]["TeamName"].ToString();
                    DataRow[] Array = _Intable.Select("TeamKey=" + TeamKey);
                    if (Array.Length > 0)
                    {
                        DataTable zGroup = Array.CopyToDataTable();
                        GVData.Rows.Add();
                        zGvRow = GVData.Rows[2];
                        HeaderRow(zGvRow, zGroup, TeamKey, TeamName, 0);
                    }
                    for (int i = 0; i < _Intable.Rows.Count; i++)
                    {
                        DataRow r = _Intable.Rows[i];
                        if (TeamKey != r["TeamKey"].ToInt())
                        {
                            #region [GROUP]
                            TeamKey = r["TeamKey"].ToInt();
                            TeamName = r["TeamName"].ToString();
                            Array = _Intable.Select("TeamKey=" + TeamKey);
                            _STT = 1;
                            if (Array.Length > 0)
                            {
                                DataTable zGroup = Array.CopyToDataTable();
                                GVData.Rows.Add();
                                zGvRow = GVData.Rows[i + NoGroup];
                                HeaderRow(zGvRow, zGroup, TeamKey, TeamName, _STT);
                            }
                            #endregion
                            NoGroup++;
                        }
                        GVData.Rows.Add();
                        zGvRow = GVData.Rows[i + NoGroup];
                        DetailRow(zGvRow, r, _STT);
                        RowTam = i + NoGroup;
                        _STT++;
                    }
                    GVData.Rows.Add();
                    zGvRow = GVData.Rows[RowTam + 1];
                    TotalRow(zGvRow, _Intable, RowTam + 1);
                }));
            }
            else
            {
                GVData.Rows.Count = 0;
                GVData.Cols.Count = 0;
            }
        }
        private void HeaderRow(Row RowView, DataTable Table, int TeamKey, string TeamName, int No)
        {
            RowView[0] = "";
            RowView[1] = TeamName;
            RowView[2] = "Số nhân sự " + Table.Select("TeamKey='" + TeamKey + "'").Length;
            RowView[3] = "";
            int nCol = 5;
            double zTotal = 0;
            for (int i = 7; i < Table.Columns.Count - 1; i++)
            {
                double zTemp = 0;
                for (int k = 0; k < Table.Rows.Count; k++)
                {
                    double TotalRow = 0;
                    if (double.TryParse(Table.Rows[k][i].ToString(), out TotalRow))
                    {
                        zTemp += TotalRow;
                    }

                }
                RowView[nCol] = zTemp.Toe1String();
                zTotal += zTemp;
                nCol++;
            }
            RowView.StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
        }
        private void DetailRow(Row RowView, DataRow rDetail, int No)
        {
            RowView[0] = No;
            RowView[1] = rDetail[1].ToString().Trim();//họ tên
            RowView[2] = rDetail[3].ToString().Trim();//tổ nhóm
            RowView[3] = rDetail[2].ToString().Trim();//mã thẻ
            RowView[4] = rDetail[6].ToString().Trim();// atm
            int nCol = 5;
            for (int i = 7; i < rDetail.ItemArray.Length - 1; i++)
            {
                RowView[nCol] = rDetail[i].Toe1String();
                nCol++;
            }
        }
        private void TotalRow(Row RowView, DataTable Table, int No)
        {
            RowView[0] = "";
            RowView[1] = "TỔNG CỘNG ";
            RowView[2] = "Số nhân sự " + Table.Rows.Count;
            RowView[3] = "";

            //float zTotal = 0;
            int nCol = 5;
            for (int i = 7; i < Table.Columns.Count - 1; i++)
            {
                double zTemp = 0;
                for (int k = 0; k < Table.Rows.Count; k++)
                {
                    double TotalRow = 0;
                    if (double.TryParse(Table.Rows[k][i].ToString(), out TotalRow))
                    {
                        zTemp += TotalRow;
                    }

                }
                RowView[nCol] = zTemp.Toe1String();
                nCol++;
            }
            RowView.StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
        }
        void InitGVProduct(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();

            int TotalRow = 2;
            int ToTalCol = TableView.Columns.Count - 3;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(TotalRow);

            CellRange zCellRange = GVData.GetCellRange(0, 0, 1, 0);
            zCellRange.Data = "STT";

            zCellRange = GVData.GetCellRange(0, 1, 1, 1);
            zCellRange.Data = "HỌ TÊN";

            zCellRange = GVData.GetCellRange(0, 2, 1, 2);
            zCellRange.Data = "CHỨC VỤ";

            zCellRange = GVData.GetCellRange(0, 3, 1, 3);
            zCellRange.Data = "SỐ THẺ";

            zCellRange = GVData.GetCellRange(0, 4, 1, 4);
            zCellRange.Data = "TÀI KHOẢN ATM";


            //Row Header
            int ColStart = 5;
            for (int i = 7; i < TableView.Columns.Count - 1; i++)
            {
                DataColumn Col = TableView.Columns[i];
                string[] strCaption = Col.ColumnName.Split('|');
                if (strCaption.Length == 3)
                {
                    if (strCaption[0] == "")
                    {
                        zCellRange = GVData.GetCellRange(0, ColStart, 1, ColStart);
                        zCellRange.Data = strCaption[1];
                    }
                    else
                    {
                        GVData.Rows[0][ColStart] = strCaption[0];
                        GVData.Rows[1][ColStart] = strCaption[1];
                    }
                }
                GVData.Cols[ColStart].Width = 100;
                ColStart++;
            }

            ////Style
            //GVData.AllowFreezing = AllowFreezingEnum.Both;
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.FixedOnly;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.CenterCenter;
            GVData.Styles.Normal.WordWrap = true;



            //Trộn cột 
            for (int i = 0; i < GVData.Cols.Count; i++)
            {
                GVData.Cols[i].AllowMerging = true;
            }
            //Trộn dòng 0
            GVData.Rows[0].AllowMerging = true;
            GVData.Rows[0].Height = 50;
            GVData.Rows[1].Height = 60;
            //Trộn dòng 1
            //Freeze Row and Column
            GVData.Rows.Fixed = 2;
            GVData.Cols.Frozen = 5;

            GVData.Cols[0].Width = 40;
            GVData.Cols[1].Width = 200;
            GVData.Cols[2].Width = 150;
            GVData.Cols[3].Width = 100;
            GVData.Cols[4].Width = 130;
            //GVData.AutoSizeCols(1, GVData.Cols.Count - 1, 10);
            //Canh phải các cột từ số 1 trở đi
            GVData.Cols[0].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[1].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[2].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[3].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[4].TextAlign = TextAlignEnum.LeftCenter;

            GVData.Cols[0].StyleNew.BackColor = Color.Empty;
            GVData.Cols[1].StyleNew.BackColor = Color.Empty;
            GVData.Cols[2].StyleNew.BackColor = Color.Empty;
            GVData.Cols[3].StyleNew.BackColor = Color.Empty;
            GVData.Cols[4].StyleNew.BackColor = Color.Empty;

            for (int i = 5; i < GVData.Cols.Count; i++)
            {
                GVData.Cols[i].TextAlign = TextAlignEnum.RightCenter;
            }

            //In đậm các dòng, cột tổng
            //GVData.Rows[TotalRow - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            //GVData.Cols[ToTalCol - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            //GVData.Cols[ToTalCol - 2].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            ////GVData.Cols[ToTalCol - 3].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            //GVData.Cols[ToTalCol - 1].StyleNew.BackColor = Color.LightGreen;
            // tô màu cột tổng
            //GVData.Rows[TotalRow - 1].StyleNew.BackColor = Color.LightGreen;

        }
        #endregion

        #region[Xem theo Tổ nhóm]
        private void DisplayData_ToNhom()
        {
            DataTable zTable = new DataTable();
            if (rdo_Default.Checked)
            {
                zTable = Report.No17_NHOMMACDINH(_DateClose, _DepartmentKeyClose, _TeamKeyClose, _IDSalary,_Pay);
            }
            else
            {
                zTable = Report.No17_NHOMQUANLY(_DateClose, _DepartmentKeyClose, _TeamKeyClose, _IDSalary,_Pay);
            }
            if (zTable.Rows.Count > 0)
            {
                PivotTable zPvt = new PivotTable(zTable);
                DataTable zTablePivot = zPvt.Generate_ASC("HeaderColumn", "LeftColumn", new string[] { "Amount" }, "CÔNG VIỆC", "TỔNG CỘNG", "TỔNG CỘNG", new string[] { "Sô tiền" }, 0);
                //Tách cột Leftcolumn
                zTablePivot.Columns.Add("TeamKey").SetOrdinal(1);
                zTablePivot.Columns.Add("TeamName").SetOrdinal(2);
                zTablePivot.Columns.Add("TeamID").SetOrdinal(3);
                zTablePivot.Columns.Add("DepartmentKey", typeof(int)).SetOrdinal(4);
                zTablePivot.Columns.Add("DepartmentName").SetOrdinal(5);
                zTablePivot.Columns.Add("BranchRank");
                zTablePivot.Columns.Add("DepartmentRank");
                for (int i = 0; i < zTablePivot.Rows.Count - 1; i++)
                {
                    string[] temp = zTablePivot.Rows[i][0].ToString().Split('|');
                    zTablePivot.Rows[i]["TeamKey"] = temp[0];
                    zTablePivot.Rows[i]["TeamName"] = temp[1];
                    zTablePivot.Rows[i]["TeamID"] = temp[2];
                    zTablePivot.Rows[i]["DepartmentKey"] = temp[3];
                    zTablePivot.Rows[i]["DepartmentName"] = temp[4];
                    zTablePivot.Rows[i]["BranchRank"] = temp[5];
                    zTablePivot.Rows[i]["DepartmentRank"] = temp[6];
                }
                //Xóa cột đầu đã cắt chuỗi và xóa dòng tổng cuối
                zTablePivot.Columns.Remove("CÔNG VIỆC");
                DataRow row = zTablePivot.Rows[zTablePivot.Rows.Count - 1];
                zTablePivot.Rows.Remove(row);
                //Sắp xếp nhân viên
                DataView dv = zTablePivot.DefaultView;
                dv.Sort = " BranchRank ASC,DepartmentRank ASC";
                zTablePivot = dv.ToTable();
                zTablePivot.Columns.Remove("BranchRank");
                zTablePivot.Columns.Remove("DepartmentRank");




                this.Invoke(new MethodInvoker(delegate ()
                {
                    InitGV_ToNhom(zTablePivot);
                    DataTable _Intable = zTablePivot;
                    _STT = 1;
                    int NoGroup = 3;
                    int RowTam = NoGroup;
                    Row zGvRow;
                    int DepartmentKey = _Intable.Rows[0]["DepartmentKey"].ToInt();
                    string DepartmentName = _Intable.Rows[0]["DepartmentName"].ToString();
                    DataRow[] Array = _Intable.Select("DepartmentKey=" + DepartmentKey);
                    if (Array.Length > 0)
                    {
                        DataTable zGroup = Array.CopyToDataTable();
                        GVData.Rows.Add();
                        zGvRow = GVData.Rows[2];
                        HeaderRow_ToNhom(zGvRow, zGroup, DepartmentKey, DepartmentName, 0);
                    }
                    for (int i = 0; i < _Intable.Rows.Count; i++)
                    {
                        DataRow r = _Intable.Rows[i];
                        if (DepartmentKey != r["DepartmentKey"].ToInt())
                        {
                            #region [GROUP]
                            DepartmentKey = r["DepartmentKey"].ToInt();
                            DepartmentName = r["DepartmentName"].ToString();
                            Array = _Intable.Select("DepartmentKey=" + DepartmentKey);
                            _STT = 1;
                            if (Array.Length > 0)
                            {
                                DataTable zGroup = Array.CopyToDataTable();
                                GVData.Rows.Add();
                                zGvRow = GVData.Rows[i + NoGroup];
                                HeaderRow_ToNhom(zGvRow, zGroup, DepartmentKey, DepartmentName, _STT);
                            }
                            #endregion
                            NoGroup++;
                        }
                        GVData.Rows.Add();
                        zGvRow = GVData.Rows[i + NoGroup];
                        DetailRow_ToNhom(zGvRow, r, _STT);
                        RowTam = i + NoGroup;
                        _STT++;
                    }
                    GVData.Rows.Add();
                    zGvRow = GVData.Rows[RowTam + 1];
                    TotalRow_ToNhom(zGvRow, _Intable, RowTam + 1);
                }));
            }
            else
            {
                GVData.Rows.Count = 0;
                GVData.Cols.Count = 0;
            }
        }
        private void HeaderRow_ToNhom(Row RowView, DataTable Table, int DepartmentKey, string TeamName, int No)
        {
            RowView[0] = "";
            RowView[1] = TeamName;
            RowView[2] = "Số tổ nhóm " + Table.Select("DepartmentKey='" + DepartmentKey + "'").Length;
            int nCol = 3;
            double zTotal = 0;
            for (int i = 5; i < Table.Columns.Count - 1; i++)
            {
                double zTemp = 0;
                for (int k = 0; k < Table.Rows.Count; k++)
                {
                    double TotalRow = 0;
                    if (double.TryParse(Table.Rows[k][i].ToString(), out TotalRow))
                    {
                        zTemp += TotalRow;
                    }

                }
                RowView[nCol] = zTemp.Toe1String();
                zTotal += zTemp;
                nCol++;
            }
            RowView.StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
        }
        private void DetailRow_ToNhom(Row RowView, DataRow rDetail, int No)
        {
            RowView[0] = No;
            RowView[1] = rDetail[1].ToString().Trim();// tên
            RowView[2] = rDetail[2].ToString().Trim();// mã tổ nhóm
            int nCol = 3;
            for (int i = 5; i < rDetail.ItemArray.Length - 1; i++)
            {
                RowView[nCol] = rDetail[i].Toe1String();
                nCol++;
            }
        }
        private void TotalRow_ToNhom(Row RowView, DataTable Table, int No)
        {
            RowView[0] = "";
            RowView[1] = "TỔNG CỘNG ";
            RowView[2] = "Số tổ nhóm " + Table.Rows.Count;

            //float zTotal = 0;
            int nCol = 3;
            for (int i = 5; i < Table.Columns.Count - 1; i++)
            {
                double zTemp = 0;
                for (int k = 0; k < Table.Rows.Count; k++)
                {
                    double TotalRow = 0;
                    if (double.TryParse(Table.Rows[k][i].ToString(), out TotalRow))
                    {
                        zTemp += TotalRow;
                    }

                }
                RowView[nCol] = zTemp.Toe1String();
                nCol++;
            }
            RowView.StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
        }
        void InitGV_ToNhom(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();

            int TotalRow = 2;
            int ToTalCol = TableView.Columns.Count - 3;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(TotalRow);

            CellRange zCellRange = GVData.GetCellRange(0, 0, 1, 0);
            zCellRange.Data = "STT";

            zCellRange = GVData.GetCellRange(0, 1, 1, 1);
            zCellRange.Data = "TÊN TỔ NHÓM";

            zCellRange = GVData.GetCellRange(0, 2, 1, 2);
            zCellRange.Data = "MÃ TỔ NHÓM";
            //Row Header
            int ColStart = 3;
            for (int i = 5; i < TableView.Columns.Count - 1; i++)
            {
                DataColumn Col = TableView.Columns[i];
                string[] strCaption = Col.ColumnName.Split('|');
                if (strCaption.Length == 3)
                {
                    if (strCaption[0] == "")
                    {
                        zCellRange = GVData.GetCellRange(0, ColStart, 1, ColStart);
                        zCellRange.Data = strCaption[1];
                    }
                    else
                    {
                        GVData.Rows[0][ColStart] = strCaption[0];
                        GVData.Rows[1][ColStart] = strCaption[1];
                    }
                }
                GVData.Cols[ColStart].Width = 100;
                ColStart++;
            }

            ////Style
            //GVData.AllowFreezing = AllowFreezingEnum.Both;
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.FixedOnly;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.CenterCenter;
            GVData.Styles.Normal.WordWrap = true;



            //Trộn cột 
            for (int i = 0; i < GVData.Cols.Count; i++)
            {
                GVData.Cols[i].AllowMerging = true;
            }
            //Trộn dòng 0
            GVData.Rows[0].AllowMerging = true;
            GVData.Rows[0].Height = 50;
            GVData.Rows[1].Height = 60;
            //Trộn dòng 1
            //Freeze Row and Column
            GVData.Rows.Fixed = 2;
            GVData.Cols.Frozen = 3;

            GVData.Cols[0].Width = 40;
            GVData.Cols[1].Width = 200;
            GVData.Cols[2].Width = 150;
            //GVData.AutoSizeCols(1, GVData.Cols.Count - 1, 10);
            //Canh phải các cột từ số 1 trở đi
            GVData.Cols[0].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[1].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[2].TextAlign = TextAlignEnum.LeftCenter;

            GVData.Cols[0].StyleNew.BackColor = Color.Empty;
            GVData.Cols[1].StyleNew.BackColor = Color.Empty;
            GVData.Cols[2].StyleNew.BackColor = Color.Empty;

            for (int i = 3; i < GVData.Cols.Count; i++)
            {
                GVData.Cols[i].Width = 130;
                GVData.Cols[i].TextAlign = TextAlignEnum.RightCenter;
            }

            //In đậm các dòng, cột tổng
            //GVData.Rows[TotalRow - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            //GVData.Cols[ToTalCol - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            //GVData.Cols[ToTalCol - 2].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            ////GVData.Cols[ToTalCol - 3].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            //GVData.Cols[ToTalCol - 1].StyleNew.BackColor = Color.LightGreen;
            // tô màu cột tổng
            //GVData.Rows[TotalRow - 1].StyleNew.BackColor = Color.LightGreen;

        }
        #endregion


        #region[list view]
        private void InitLayout_LV(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Chỉ tiêu lương";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);
        }
        private void InitData(bool Check)
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LVEmployee;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            DataTable In_Table = new DataTable();

            In_Table = Report.ListCodeOffice(dteDate.Value);
            LV.Items.Clear();
            int n = In_Table.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = "";
                DataRow nRow = In_Table.Rows[i];
                lvi.Tag = nRow["CodeID"];
                lvi.ForeColor = Color.Navy;

                lvi.BackColor = Color.White;
                lvi.ImageIndex = 0;
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = (i + 1).ToString() + "." + nRow["CodeName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = "";
                lvi.SubItems.Add(lvsi);
                lvi.Checked = Check;
                LV.Items.Add(lvi);
            }

            this.Cursor = Cursors.Default;
        }

        #endregion

        #region[In]
        private void LoadInfo_Report()
        {
            //Ngày lập
            dte_RpDate.Value = SessionUser.Date_Work;
            //Tên báo cáo
            txt_RpTitle.Text = "Báo cáo tổng hợp các bộ phận gián tiếp";
            //Người lập
            txt_RpNguoiLap.Text = SessionUser.UserLogin.EmployeeName;
            //Kế toán trưởng
            txt_RpKeToan.Text = "Trần Tấn Long Thạch";
            //HCNS
            txt_RpHCNS.Text = "Lê Văn Hòa";
            //Tổng giám đốc
            txt_RpGiamDoc.Text = "Nguyễn Vũ Lộc";
        }
        private void Btn_Print_Click(object sender, EventArgs e)
        {
            if (GVData.Rows.Count > 0)
            {
                Panel_Done.Visible = true;
            }
            else
            {
                Utils.TNMessageBoxOK("Không tìm thấy dữ liệu", 1);
            }
        }
        private void Btn_Done_Click(object sender, EventArgs e)
        {
            Panel_Done.Visible = false;

            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Báo_Cáo_Lương_Bộ_Phận_Gián_Tiếp.xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }
                    XuatExcelMau(newFile);
                    Process.Start(Path);
                }

            }
        }
        private void BtnClose_Panel_Message_Click(object sender, EventArgs e)
        {
            Panel_Done.Visible = false;
        }
        private void XuatExcelMau(FileInfo newFile)
        {
            string TieuDe = txt_RpTitle.Text;
            string Font = "Time New Roman";
            string LapBang = txt_RpNguoiLap.Text;
            string KeToan = txt_RpKeToan.Text;
            string NhanSu = txt_RpHCNS.Text;
            string GiamDoc = txt_RpGiamDoc.Text;

            var fileMau = new FileInfo(Application.StartupPath + @"\FileTemplate\MauBaoCao_So1.xlsx");
            using (ExcelPackage xlPackage = new ExcelPackage(fileMau))
            {
                ExcelWorksheet workSheet = xlPackage.Workbook.Worksheets.FirstOrDefault();

                //tham số chỉ định tùy chọn
                int ColumnGV = GVData.Cols.Count;  //tổng cột cần hiển thị của Gidview
                int RowGV = GVData.Rows.Count; //số dòng dữ liệu Gidview
                int RowExcel = 6;  //dòng bắt đầu cần ghi dữ liệu trong excel               

                #region 2. DATA DỮ LIỆU                
                for (int i = 1; i <= RowGV; i++)
                {
                    int rowGV = i - 1;
                    for (int c = 1; c <= ColumnGV; c++)
                    {
                        int colGV = c - 1;

                        object val = GVData.Rows[rowGV][colGV];
                        if (val != null)
                        {
                            workSheet.Cells[RowExcel + i, c].Value = val.ToString();
                            //Dòng tổng thì in đậm
                            if (GVData.Rows[rowGV][0].ToString() == "")
                            {
                                workSheet.Cells[RowExcel + i, c].Style.Font.Color.SetColor(Color.Navy);
                                workSheet.Cells[RowExcel + i, c].Style.Font.Bold = true;
                            }
                        }
                        else
                        {
                            workSheet.Cells[RowExcel + i, c].Value = string.Empty;
                        }
                        if (c == 2 || c == 3)
                        { // cot ten nhan vien
                            workSheet.Cells[RowExcel + i, c].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        }
                        else
                        {
                            workSheet.Cells[RowExcel + i, c].Style.Numberformat.Format = "0.00";
                            workSheet.Cells[RowExcel + i, c].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        }

                    }
                }

                #endregion

                #region 3. ĐỊNH DẠNG  DATA EXCEL
                //DÒNG THỨ 7 TRONG EXCEL ĐẾN DÒNG 8 TRONG EXCEL LÀ TIÊU ĐỀ BẢNG
                for (int r = 7; r < 9; r++)
                {
                    for (int c = 1; c <= ColumnGV; c++)
                    {
                        int colGV = c - 1;
                        workSheet.Cells[r, c].Style.Font.Name = Font;
                        workSheet.Cells[r, c].Style.Font.Size = 12;
                        workSheet.Cells[r, c].Style.Font.Bold = true;
                        workSheet.Cells[r, c].Style.Font.Color.SetColor(Color.Navy);
                        workSheet.Cells[r, c].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        workSheet.Cells[r, c].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        workSheet.Cells[r, c].Style.WrapText = true;
                        //workSheet.Column(c).Width = 15;
                        workSheet.Column(c).AutoFit();

                    }
                }
                workSheet.Column(ColumnGV).Style.Font.Bold = true;

                #endregion

                #region[4.Trộn ngang > dọc tự động tiêu đề column báo cáo]       
                MergeAutoExcel(workSheet, 7, 8, 1, ColumnGV);
                workSheet.Row(7).Height = 65; //Độ cao tiêu đề
                workSheet.Row(8).Height = 100; // Độ cao tiêu đề
                #endregion

                #region 1. TÊN BÁO CÁO EXCEL
                //Thêm các dường line excel
                workSheet.Cells.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                workSheet.Cells.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                workSheet.Cells.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                workSheet.Cells.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                // Dòng Tên báo cáo
                workSheet.Cells[4, ColumnGV / 2].Value = TieuDe.ToUpper();
                workSheet.Cells[4, ColumnGV / 2].Style.Font.Size = 16;
                workSheet.Cells[4, ColumnGV / 2].Style.Font.Name = Font;
                workSheet.Cells[4, ColumnGV / 2].Style.Font.Bold = true;
                workSheet.Cells[4, ColumnGV / 2].Style.Font.Color.SetColor(Color.Navy);
                workSheet.Cells[4, ColumnGV / 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                // Xóa các đường line Từ dòng 1 đến dòng tiêu đề
                workSheet.Cells["1:6"].Style.Border.Left.Style = ExcelBorderStyle.None;
                workSheet.Cells["1:6"].Style.Border.Right.Style = ExcelBorderStyle.None;
                workSheet.Cells["1:6"].Style.Border.Top.Style = ExcelBorderStyle.None;
                workSheet.Cells["1:5"].Style.Border.Bottom.Style = ExcelBorderStyle.None;
                #endregion

                #region[5.Định dạng màu,độ rộng các cột fit]
                if (rdo_Team.Checked)
                {
                    workSheet.Column(1).Width = 7;
                    workSheet.Column(1).Style.Font.Color.SetColor(Color.Navy);
                    workSheet.Column(2).Style.Font.Color.SetColor(Color.Navy);
                    workSheet.View.FreezePanes(9, 4);
                }
                else
                {
                    workSheet.Column(1).Width = 7;
                    workSheet.Column(1).Style.Font.Color.SetColor(Color.Navy);
                    workSheet.Column(2).Style.Font.Color.SetColor(Color.Navy);
                    workSheet.Column(3).Style.Font.Color.SetColor(Color.Navy);
                    workSheet.Column(4).Style.Font.Color.SetColor(Color.Navy);
                    workSheet.Column(5).Style.Font.Color.SetColor(Color.Navy);
                    workSheet.View.FreezePanes(9, 6);
                }

                #endregion

                #region[6.Khung chữ ký]
                // Ngày ký
                workSheet.Cells[RowExcel + RowGV + 2, ColumnGV - 2].Value = "Ngày " + dte_RpDate.Value.Day + " tháng " + dte_RpDate.Value.Month + " năm " + dte_RpDate.Value.Year;
                workSheet.Cells[RowExcel + RowGV + 2, ColumnGV - 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 2, ColumnGV - 2].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 2, ColumnGV - 2].Style.Font.Color.SetColor(Color.Navy);

                //Tiêu đề Người lập bảng
                workSheet.Cells[RowExcel + RowGV + 3, 2].Value = "Lập bảng";
                workSheet.Cells[RowExcel + RowGV + 3, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 3, 2].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 3, 2].Style.Font.Color.SetColor(Color.Black);
                // Tên Người lập bảng
                workSheet.Cells[RowExcel + RowGV + 9, 2].Value = LapBang;
                workSheet.Cells[RowExcel + RowGV + 9, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 9, 2].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 9, 2].Style.Font.Color.SetColor(Color.Black);

                //Tiêu đề Kế toán trưởng
                workSheet.Cells[RowExcel + RowGV + 3, (ColumnGV / 4) + 2].Value = "Kế toán trưởng";
                workSheet.Cells[RowExcel + RowGV + 3, (ColumnGV / 4) + 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 3, (ColumnGV / 4) + 2].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 3, (ColumnGV / 4) + 2].Style.Font.Color.SetColor(Color.Black);
                //Tên kế toán trưởng
                workSheet.Cells[RowExcel + RowGV + 9, (ColumnGV / 4) + 2].Value = KeToan;
                workSheet.Cells[RowExcel + RowGV + 9, (ColumnGV / 4) + 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 9, (ColumnGV / 4) + 2].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 9, (ColumnGV / 4) + 2].Style.Font.Color.SetColor(Color.Black);

                //Tiêu đề BP HCNS
                workSheet.Cells[RowExcel + RowGV + 3, (ColumnGV / 2) + (ColumnGV / 4) - 2].Value = "BP HCNS";
                workSheet.Cells[RowExcel + RowGV + 3, (ColumnGV / 2) + (ColumnGV / 4) - 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 3, (ColumnGV / 2) + (ColumnGV / 4) - 2].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 3, (ColumnGV / 2) + (ColumnGV / 4) - 2].Style.Font.Color.SetColor(Color.Black);
                //Tên Trưởng phòng HCNS
                workSheet.Cells[RowExcel + RowGV + 9, (ColumnGV / 2) + (ColumnGV / 4) - 2].Value = NhanSu;
                workSheet.Cells[RowExcel + RowGV + 9, (ColumnGV / 2) + (ColumnGV / 4) - 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 9, (ColumnGV / 2) + (ColumnGV / 4) - 2].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 9, (ColumnGV / 2) + (ColumnGV / 4) - 2].Style.Font.Color.SetColor(Color.Black);

                //Tiêu đề Tổng giám đốc
                workSheet.Cells[RowExcel + RowGV + 3, ColumnGV - 2].Value = "Tổng giám đốc";
                workSheet.Cells[RowExcel + RowGV + 3, ColumnGV - 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 3, ColumnGV - 2].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 3, ColumnGV - 2].Style.Font.Color.SetColor(Color.Black);
                //Tên tổng giám đốc
                workSheet.Cells[RowExcel + RowGV + 9, ColumnGV - 2].Value = GiamDoc;
                workSheet.Cells[RowExcel + RowGV + 9, ColumnGV - 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 9, ColumnGV - 2].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 9, ColumnGV - 2].Style.Font.Color.SetColor(Color.Black);

                //Xóa đường line các dòng ký tên
                workSheet.Cells[(RowExcel + RowGV + 1) + ":" + (RowExcel + RowGV + 9)].Style.Border.Left.Style = ExcelBorderStyle.None;
                workSheet.Cells[(RowExcel + RowGV + 1) + ":" + (RowExcel + RowGV + 9)].Style.Border.Right.Style = ExcelBorderStyle.None;
                workSheet.Cells[(RowExcel + RowGV + 1) + ":" + (RowExcel + RowGV + 9)].Style.Border.Top.Style = ExcelBorderStyle.None;
                workSheet.Cells[(RowExcel + RowGV + 1) + ":" + (RowExcel + RowGV + 9)].Style.Border.Bottom.Style = ExcelBorderStyle.None;

                #endregion

                #region[7.Tiện ích Cài đặt in]
                workSheet.PrinterSettings.PaperSize = ePaperSize.A3; //Khổ giấy
                workSheet.PrinterSettings.PrintArea = workSheet.Cells[1, 1, RowExcel + RowGV + 9, ColumnGV];//Khu vực in
                workSheet.PrinterSettings.PageOrder = ePageOrder.OverThenDown;//Kiểu in chữ Z
                workSheet.PrinterSettings.RepeatRows = workSheet.Cells["7:8"];// Tiêu đề lặp lại khi qua trang khác
                //workSheet.PrinterSettings.RepeatColumns = workSheet.Cells["A:A"];// Colunm lặp lại khi qua trang khác
                workSheet.PrinterSettings.Orientation = eOrientation.Landscape;//Trang giấy nằm ngang
                workSheet.View.PageBreakView = true;//Đường phân cách trang in
                workSheet.PrinterSettings.FitToPage = true;//In độ cao rộng vào 1 trang duy nhất
                workSheet.PrinterSettings.FitToWidth = 1;//Chiều rộng cột đúng 1 trang
                workSheet.PrinterSettings.FitToHeight = 0;//Tự động cắt trang
                #endregion

                xlPackage.SaveAs(newFile);
            }
        }
        //cấp 1 trộn dòng cố định
        private void MergeDongExcel(ExcelWorksheet workSheet, int currentRow, int fromCol, int ToCol)
        {
            try
            {
                workSheet.Cells[currentRow, fromCol, currentRow, ToCol].Merge = true;
            }
            catch (Exception)
            {
                workSheet.Cells[currentRow, fromCol + 1, currentRow, ToCol].Merge = true;
            }
        }
        //cấp 2 trộn cột cố định
        private void MergeCotExcel(ExcelWorksheet workSheet, int currentColumn, int fromRow, int toRow)
        {
            try
            {
                workSheet.Cells[fromRow, currentColumn, toRow, currentColumn].Merge = true;
            }
            catch (Exception)
            {
                workSheet.Cells[fromRow + 1, currentColumn, toRow, currentColumn].Merge = true;
            }
        }
        //cấp 3 trộn kết hợp
        private void MergeAutoExcel(ExcelWorksheet workSheet, int fromRow, int toRow, int fromCol, int toCol)
        {
            string Val = workSheet.Cells[fromRow, fromCol].Value.ToString();
            int TronCot = 0;

            #region [Trộn cột :(dòng trên dòng dưới) ]

            for (int r = fromRow; r < toRow; r++)
            {
                for (int c = fromCol + 1; c < toCol; c++)
                {
                    string Dta = workSheet.Cells[r, c].Value.ToString();

                    if (Val == Dta)
                    {
                        TronCot++;
                    }
                    else
                    {
                        if (TronCot >= 1)
                        {
                            try
                            {
                                workSheet.Cells[r, c - TronCot - 1, r, c - 1].Merge = true;
                                TronCot = 0;
                            }
                            catch (Exception)
                            {

                            }
                        }
                        Val = Dta;
                    }
                }

                //trộn cuối cùng 
                if (Val == workSheet.Cells[r, toCol, r, toCol].Value.ToString())
                {
                    try
                    {
                        workSheet.Cells[r, toCol - TronCot - 1, r, toCol].Merge = true;
                        TronCot = 0;
                    }
                    catch (Exception)
                    {

                    }
                }
                else if (TronCot >= 1)
                {
                    try
                    {
                        workSheet.Cells[r, toCol - TronCot - 1, r, toCol - 1].Merge = true;
                        TronCot = 0;
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {

                }
            }
            #endregion

            #region [Trộn dòng :(cột trái cột phải)]

            for (int c = fromCol; c <= toCol; c++)
            {
                int TronDong = 0;
                Val = workSheet.Cells[fromRow, c].Value.ToString();
                for (int r = fromRow + 1; r <= toRow; r++)
                {
                    string Dta = workSheet.Cells[r, c].Value.ToString();
                    if (Val == Dta)
                    {
                        TronDong++;
                    }
                    else
                    {
                        Val = Dta;
                    }
                }

                if (TronDong > 0)
                {
                    try
                    {
                        workSheet.Cells[fromRow, c, fromRow + TronDong, c].Merge = true;
                        TronDong = 0;
                    }
                    catch (Exception)
                    {

                    }
                }
                if (c == toCol && Val == workSheet.Cells[fromRow, c].Value.ToString())
                {
                    try
                    {
                        workSheet.Cells[toRow - TronDong, toCol, toRow, toCol].Merge = true;
                        TronDong = 0;
                    }
                    catch (Exception)
                    {

                    }
                }

            }
            #endregion
        }
        #endregion

        private void Btn_Export_Click(object sender, EventArgs e)
        {
            string zTeamName = "";
            if (cboTeam.SelectedValue.ToInt() != 0)
            {
                string[] s = cboTeam.Text.Trim().Split('-');
                zTeamName = "_Nhóm_" + s[0];
            }
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Báo_Cáo_Lương_Bộ_Phận_Văn_Phòng_Lương_Đúng_Tháng_" + dteDate.Value.ToString("MM-yyyy") + zTeamName + ".xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }

                    string Status = "Xuất Excel form " + HeaderControl.Text + " > đường dẫn và tên file xuất:" + FDialog.FileName;
                    Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);

                    GVData.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }
        }

        //Chuyển số thành dãy chuỗi
        string ConvertIDRank(string ID)
        {
            //chèn số thấp tới cao--> tới chữ
            string zResult = "";
            string s = "";
            string temp = "";
            if (ID.Substring(0, 1) == "A")
            {
                temp = ID;
                for (int i = 0; i < 8 - ID.Trim().Length; i++)
                {
                    s += "9";
                }
                zResult = s + temp;
            }
            else if (ID.Substring(0, 1) == "H" || ID.Substring(0, 1) == "L")
            {
                temp = ID;
                for (int i = 0; i < 8 - ID.Trim().Length; i++)
                {
                    s += "B";
                }
                zResult = s + temp;
            }
            else
            {
                temp = ID;
                for (int i = 0; i < 8 - ID.Trim().Length; i++)
                {
                    s += "0";
                }
                zResult = s + temp;
            }
            return zResult;
        }
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                //this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
                btn_Export.Enabled = false;
            }
        }
        #endregion
    }
}
