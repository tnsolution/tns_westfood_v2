﻿using C1.Win.C1FlexGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.CORE;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Rpt18_V2 : Form
    {
        private int _BranchKey = 0;
        private int _DepartmentKey = 0;
        private int _TeamKey = 0;
        private string _MathMoney = "";
        private string _MathTime = "";
        private double _MoneySearch = 0;
        private double _TimeSearch = 0;
        private string _Search = "";
        public Frm_Rpt18_V2()
        {
            InitializeComponent();
            btn_Search.Click += Btn_Search_Click;
            btnClose.Click += btnClose_Click;
            btnMax.Click += btnMax_Click;
            btnMini.Click += btnMini_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            btn_Export.Click += Btn_Export_Click;
            cbo_Branch.SelectedIndexChanged += Cbo_Branch_SelectedIndexChanged;
            cbo_Department.SelectedIndexChanged += Cbo_Department_SelectedIndexChanged;
            txt_Money.Leave += Txt_Money_Leave;
            txt_ThoiGian.Leave += Txt_ThoiGian_Leave;
        }

        private void Txt_ThoiGian_Leave(object sender, EventArgs e)
        {
            double zTemp = 0;
            if (double.TryParse(txt_ThoiGian.Text.Trim(), out zTemp))
            {

            }
            txt_ThoiGian.Text = zTemp.ToString("n1");
        }

        private void Txt_Money_Leave(object sender, EventArgs e)
        {
            double zTemp = 0;
            if (double.TryParse(txt_Money.Text.Trim(), out zTemp))
            {

            }
            txt_Money.Text = zTemp.ToString("n0");
        }

        private void Frm_Rpt18_V2_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            Check_RoleForm();

            DateTime ViewDate = SessionUser.Date_Work;
            DateTime FromDate = new DateTime(ViewDate.Year, 1, 1, 0, 0, 0);
            DateTime ToDate = FromDate.AddYears(1).AddDays(-1);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            dte_FromDate.Value = FromDate;
            dte_ToDate.Value = ToDate;
            if (Role.AccessBranch == "2,4")
                LoadDataToToolbox.KryptonComboBox(cbo_Branch, "SELECT BranchKey,BranchName FROM SYS_Branch WHERE BranchKey IN (" + Role.AccessBranch + ")  AND RecordStatus < 99", "--Chọn tất cả--");
            else
            {
                LoadDataToToolbox.KryptonComboBox(cbo_Branch, "SELECT BranchKey,BranchName FROM SYS_Branch WHERE BranchKey IN (" + Role.AccessBranch + ")  AND RecordStatus < 99", "");
                Cbo_Branch_SelectedIndexChanged(null, null);
            }

            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            cbo_MThuNhap.SelectedIndex = 0;
            cbo_MGio.SelectedIndex = 0;
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
        }
        private void Cbo_Branch_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDataToToolbox.KryptonComboBox(cbo_Department, "SELECT DepartmentKey, DepartmentName FROM[dbo].[SYS_Department] WHERE BranchKey = " + cbo_Branch.SelectedValue.ToInt() + "  AND RecordStatus< 99", "---- Chọn tất cả----");

        }
        private void Cbo_Department_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDataToToolbox.KryptonComboBox(cbo_Team, "SELECT TeamKey,TeamName FROM SYS_Team WHERE TeamKey != 98 AND DepartmentKey = " + cbo_Department.SelectedValue.ToInt() + "  AND RecordStatus < 99", "---- Chọn tất cả ----");
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            _BranchKey = cbo_Branch.SelectedValue.ToInt();
            _DepartmentKey = cbo_Department.SelectedValue.ToInt();
            _TeamKey = cbo_Team.SelectedValue.ToInt();
            _MathMoney = "";
            _MathTime = "";
            _MoneySearch = 0;
            _TimeSearch = 0;
            _Search = txt_SearchID.Text.Trim();

            if (txt_Money.Text.Trim()!="0")
            {
                double zTemp = 0;
                if (double.TryParse(txt_Money.Text.Trim(), out zTemp))
                {
                    _MoneySearch = zTemp;
                }
                _MathMoney = cbo_MThuNhap.Text;
            }
            if (txt_ThoiGian.Text.Trim() != "0")
            {
                double zTemp = 0;
                if (double.TryParse(txt_ThoiGian.Text.Trim(), out zTemp))
                {
                    _TimeSearch = zTemp;
                }
                _MathTime = cbo_MGio.Text;
            }


            string Status = "Tìm DL form " + HeaderControl.Text + " > từ: " + dte_FromDate.Value.ToString("dd/MM/yyyy") + " > đến:" + dte_ToDate.Value.ToString("dd/MM/yyyy") + " > Nhóm:" + cbo_Team.Text;
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);

            if (dte_ToDate.Value.Month < dte_FromDate.Value.Month || dte_ToDate.Value.Year != dte_FromDate.Value.Year)
            {
                Utils.TNMessageBoxOK( "Vui lòng chỉ chọn trong 1 năm !", 1);
                return;
            }
            else
            {
                try
                {
                    using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }
                }
                catch (Exception ex)
                {
                    Utils.TNMessageBoxOK( ex.ToString(), 4);
                }
            }
        }
        private void DisplayData()
        {
            DataTable zTable;
            if (rdo_NoRate.Checked==true)
            zTable = Report.No18_V2_ChuaNhanHeSoGio(dte_FromDate.Value, dte_ToDate.Value, _BranchKey, _DepartmentKey, _TeamKey,_MathMoney,_MoneySearch,_MathTime,_TimeSearch,_Search);
            else
                zTable = Report.No18_V2_DaNhanHeSoGio(dte_FromDate.Value, dte_ToDate.Value, _BranchKey, _DepartmentKey, _TeamKey, _MathMoney, _MoneySearch, _MathTime, _TimeSearch,_Search);

            if (zTable.Rows.Count > 0)
            {
                PivotTable zPvt = new PivotTable(zTable);
                DataTable zTablePivot = zPvt.Generate_ASC("HeaderColumn", "LeftColumn", new string[] { "MONEY", "TIMEKEEPING", "TIME", "MEDIUM" }, "HeaderColumn", "TỔNG CỘNG", "TỔNG CỘNG", new string[] { "TỔNG THU NHẬP", "SỐ NGÀY CÔNG", "SỐ GIỜ LÀM VIỆC","TRUNG BÌNH GIỜ / NGÀY" }, 0);


                zTablePivot.Columns.Add("EmployeeRank");
                zTablePivot.Columns.Add("BranchRank", typeof(int));
                zTablePivot.Columns.Add("DepartmentRank", typeof(int));
                zTablePivot.Columns.Add("TeamRank", typeof(int));
                zTablePivot.Columns.Add("TeamName").SetOrdinal(0);

                for (int i = 0; i < zTablePivot.Rows.Count; i++)
                {
                    if (i < zTablePivot.Rows.Count - 1)
                    {
                        string[] temp = zTablePivot.Rows[i][1].ToString().Split('|');
                        zTablePivot.Rows[i]["EmployeeRank"] = ConvertIDRank(temp[2]);
                        zTablePivot.Rows[i]["BranchRank"] = temp[3];
                        zTablePivot.Rows[i]["DepartmentRank"] = temp[4];
                        zTablePivot.Rows[i]["TeamRank"] = temp[5];
                    }
                    else
                    {
                        zTablePivot.Rows[i]["EmployeeRank"] = "99999999";
                        zTablePivot.Rows[i]["BranchRank"] = "99999999";
                        zTablePivot.Rows[i]["DepartmentRank"] = "99999999";
                        zTablePivot.Rows[i]["TeamRank"] = "99999999";
                    }

                }
                for (int i = 0; i < zTablePivot.Rows.Count; i++)
                {
                    string loai = zTablePivot.Rows[i][1].ToString().Split('|')[0];
                    zTablePivot.Rows[i][0] = loai;
                }
                //Sắp xếp nhân viên
                DataView dv = zTablePivot.DefaultView;
                dv.Sort = " BranchRank ASC,DepartmentRank ASC,TeamRank ASC,  EmployeeRank ASC";
                zTablePivot = dv.ToTable();
                zTablePivot.Columns.Remove("EmployeeRank");
                zTablePivot.Columns.Remove("BranchRank");
                zTablePivot.Columns.Remove("DepartmentRank");
                zTablePivot.Columns.Remove("TeamRank");

                string Loai = zTablePivot.Rows[0][0].ToString();
                DataRow[] Array = zTablePivot.Select("TeamName='" + Loai + "'");
                if (Array.Length > 0)
                {
                    AddGroup(zTablePivot, Loai, Array, 0);
                }

                for (int i = 0; i < zTablePivot.Rows.Count; i++)
                {
                    string temploai = zTablePivot.Rows[i][0].ToString();

                    if (temploai != Loai && temploai.ToUpper() != "TỔNG CỘNG")
                    {
                        Loai = temploai;
                        Array = zTablePivot.Select("TeamName='" + Loai + "'");
                        if (Array.Length > 0)
                        {
                            AddGroup(zTablePivot, Loai, Array, i);
                        }
                    }
                }
                
                //chia trung bình dòng tổng cuối
                for (int c = 5; c < zTablePivot.Columns.Count; c+=4)
                {
                    //cột trung bình phải chia
                    double zTongCong = 0;
                    double zTongGio = 0;
                    if (double.TryParse(zTablePivot.Rows[zTablePivot.Rows.Count - 1][c-2].ToString(), out zTongCong))
                    {

                    }
                    if (double.TryParse(zTablePivot.Rows[zTablePivot.Rows.Count - 1][c-1].ToString(), out zTongGio))
                    {

                    }

                    if (zTongCong > 0)
                        zTablePivot.Rows[zTablePivot.Rows.Count - 1][c] = Math.Round( zTongGio/60/ zTongCong);
                    else
                        zTablePivot.Rows[zTablePivot.Rows.Count - 1][c] = 0;
                }
                this.Invoke(new MethodInvoker(delegate ()
                {
                    InitGVProduct(zTablePivot);
                }));
            }
        }

        void InitGVProduct(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();

            int TotalRow = TableView.Rows.Count + 2;
            int ToTalCol = TableView.Columns.Count - 3;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(TotalRow);

            CellRange zCellRange = GVData.GetCellRange(0, 0, 1, 0);
            zCellRange.Data = "STT";

            zCellRange = GVData.GetCellRange(0, 1, 1, 1);
            zCellRange.Data = "HỌ VÀ TÊN";

            zCellRange = GVData.GetCellRange(0, 2, 1, 2);
            zCellRange.Data = "MÃ THẺ";

            //Row Header
            int ColStart = 3;
            for (int i = 2; i < TableView.Columns.Count - 4; i++)
            {
                DataColumn Col = TableView.Columns[i];
                string[] strCaption = Col.ColumnName.Split('|');
                //if (strCaption.Length > 2)
                //{
                    if (strCaption[0].ToInt()==98)
                    {
                        GVData.Rows[0][ColStart] = "TỔNG CỘNG";
                        GVData.Rows[1][ColStart] = strCaption[1];
                    }
                    else if (strCaption[0].ToInt()==99)
                    {
                        GVData.Rows[0][ColStart] = "TRUNG BÌNH";
                        GVData.Rows[1][ColStart] = strCaption[1];

                    }
                    else
                    {
                        GVData.Rows[0][ColStart] = "THÁNG "+strCaption[0];
                        GVData.Rows[1][ColStart] = strCaption[1];
                    }
                //}
                //else
                //{
                    //GVData.Rows[0][ColStart] = strCaption[0];
                    //GVData.Rows[1][ColStart] = strCaption[1];
                //}
                ColStart++;
            }

            ColStart = 1;
            int RowStart = 2;
            int zNo = 1;
            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {

                DataRow rData = TableView.Rows[rIndex];

                string[] strTemp = rData[1].ToString().Split('|');

                if (strTemp.Length > 1)
                {
                    GVData.Rows[rIndex + RowStart][0] = zNo;
                    GVData.Rows[rIndex + RowStart][1] = strTemp[1];
                    GVData.Rows[rIndex + RowStart][2] = strTemp[2];
                    zNo++;
                }
                else
                {
                    zNo = 1;
                    GVData.Rows[rIndex + RowStart][1] = strTemp[0];
                    GVData.Rows[rIndex + RowStart].StyleNew.Font = new Font("Tahoma", 9F, FontStyle.Bold | FontStyle.Italic);
                }
                int k = 4;
                int n = 5;
                for (int cIndex = 2; cIndex < TableView.Columns.Count - 4; cIndex++)
                {
                    if (cIndex == k)
                    {
                        GVData.Rows[RowStart + rIndex][ColStart + cIndex] = SoGio(rData[cIndex].ToString()); //Đổi phút thành giờ
                        k += 4;
                    }
                    else if(cIndex==n)
                    {
                        GVData.Rows[RowStart + rIndex][ColStart + cIndex] = rData[cIndex].Toe1String();
                        n += 4;
                    }
                    else
                        GVData.Rows[RowStart + rIndex][ColStart + cIndex] = rData[cIndex].Toe0String();
                }
            }


            ////Style
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.FixedOnly;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.CenterCenter;
            GVData.Styles.Normal.WordWrap = true;

            //Trộn dòng 0
            GVData.Rows[0].AllowMerging = true;
            GVData.Rows[0].Height = 30;
            //Trộn dòng 1
            GVData.Rows[1].AllowMerging = true;
            GVData.Rows[1].Height = 45;

            //Trộn cột đầu
            GVData.Cols[0].AllowMerging = true;
            GVData.Cols[0].Width = 35;
            GVData.Cols[1].AllowMerging = true;
            GVData.Cols[1].Width = 250;
            GVData.Cols[2].AllowMerging = true;
            GVData.Cols[2].Width = 100;
            //Trộn 2 cột cuối
            GVData.Cols[ToTalCol - 1].AllowMerging = true;
            GVData.Cols[ToTalCol - 2].AllowMerging = true;
            GVData.Cols[ToTalCol - 3].AllowMerging = true;
            GVData.Cols[ToTalCol - 4].AllowMerging = true;
            //Freeze Row and Column
            GVData.Rows.Fixed = 2;
            GVData.Cols.Frozen = 3;

            //GVData.AutoSizeCols(1, GVData.Cols.Count - 1, 10);
            //Canh phải các cột từ số 1 trở đi
            GVData.Cols[0].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[1].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[2].TextAlign = TextAlignEnum.LeftCenter;

            GVData.Cols[0].StyleNew.BackColor = Color.Empty;
            GVData.Cols[1].StyleNew.BackColor = Color.Empty;
            GVData.Cols[2].StyleNew.BackColor = Color.Empty;

            for (int i = 3; i < GVData.Cols.Count; i++)
            {
                GVData.Cols[i].TextAlign = TextAlignEnum.RightCenter;
            }

            //In đậm các dòng, cột tổng
            GVData.Rows[TotalRow - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            //GVData.Cols[ToTalCol - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            //GVData.Cols[ToTalCol - 2].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            //GVData.Cols[ToTalCol - 3].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            //GVData.Cols[ToTalCol - 4].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);

            GVData.Cols[ToTalCol - 1].StyleNew.ForeColor = Color.Red;
            GVData.Cols[ToTalCol - 2].StyleNew.ForeColor = Color.Red;
            GVData.Cols[ToTalCol - 3].StyleNew.ForeColor = Color.Red;
            GVData.Cols[ToTalCol - 4].StyleNew.ForeColor = Color.Red;

            GVData.Cols[ToTalCol - 5].StyleNew.ForeColor = Color.Blue;
            GVData.Cols[ToTalCol - 6].StyleNew.ForeColor = Color.Blue;
            GVData.Cols[ToTalCol - 7].StyleNew.ForeColor = Color.Blue;
            GVData.Cols[ToTalCol - 8].StyleNew.ForeColor = Color.Blue;
        }
        private void AddGroup(DataTable zTablePivot, string Loai, DataRow[] Array, int Index)
        {
            DataTable zGroup = Array.CopyToDataTable();
            DataRow dr = zTablePivot.NewRow();
            dr[0] = Loai;
            dr[1] = Loai.ToUpper();

            for (int c = 2; c < zGroup.Columns.Count; c++)
            {
                float Tong = 0;
                foreach (DataRow r in zGroup.Rows)
                {
                    float ztemp = 0;
                    if (float.TryParse(r[c].ToString(), out ztemp))
                    {

                    }
                    Tong += ztemp;
                }
                dr[c] = Tong;
            }
            for (int c = 5; c < zGroup.Columns.Count; c+=4)
            {
                double zCong = 0;
                double zGio = 0;
                if (double.TryParse(dr[c-2].ToString(), out zCong))
                {

                }
                if (double.TryParse(dr[c - 1].ToString(), out zGio))
                {

                }
                if(zCong !=0)
                {
                    dr[c] = Math.Round(zGio/60/zCong,0); // số phút đổi ra giờ rồi mới chia
                }
                
            }


            zTablePivot.Rows.InsertAt(dr, Index);
        }
        private void Btn_Export_Click(object sender, EventArgs e)
        {
            string zTeamName = "";
            if (cbo_Team.SelectedValue.ToInt() != 0)
            {
                string[] s = cbo_Team.Text.Trim().Split('-');
                zTeamName = "_Nhóm_" + s[0];
            }
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Thống kê lương tổng hợp hàng năm" + zTeamName + "_Từ_" + dte_FromDate.Value.ToString("dd_MM_yyyy") + "_Đến_" + dte_ToDate.Value.ToString("dd_MM_yyyy") + ".xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }

                    string Status = "Xuất Excel form " + HeaderControl.Text + " > đường dẫn và tên file xuất:" + FDialog.FileName;
                    Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);

                    GVData.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }
        }
        //Chuyển số thành dãy chuỗi
        string ConvertIDRank(string ID)
        {
            //chèn số thấp tới cao--> tới chữ
            string zResult = "";
            string s = "";
            string temp = "";
            if (ID.Substring(0, 1) == "A")
            {
                temp = ID;
                for (int i = 0; i < 8 - ID.Trim().Length; i++)
                {
                    s += "9";
                }
                zResult = s + temp;
            }
            else if (ID.Substring(0, 1) == "H" || ID.Substring(0, 1) == "L")
            {
                temp = ID;
                for (int i = 0; i < 8 - ID.Trim().Length; i++)
                {
                    s += "B";
                }
                zResult = s + temp;
            }
            else
            {
                temp = ID;
                for (int i = 0; i < 8 - ID.Trim().Length; i++)
                {
                    s += "0";
                }
                zResult = s + temp;
            }
            return zResult;
        }
        string SoGio(string TongSoPhut)
        {
            double ztemp = 0;
            if (double.TryParse(TongSoPhut.ToString(), out ztemp))
            {

            }
            //Biên hệ số giờ thành tổng số phút
            if (ztemp > 0)
            {
                long zHour = Convert.ToInt64(ztemp) / 60;
                long zMinus = Convert.ToInt64(ztemp) % 60;
                return zHour.ToString().PadLeft(2, '0') + ":" + zMinus.ToString().PadLeft(2, '0');
            }
            return string.Empty;
        }
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                //this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
        #region [ Get Auth-Action-Chg_Lang ]
        Access_Role_Info Role;
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
                btn_Export.Enabled = false;
            }
        }
        #endregion
    }
}
