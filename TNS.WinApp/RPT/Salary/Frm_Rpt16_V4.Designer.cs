﻿namespace TNS.WinApp
{
    partial class Frm_Rpt16_V4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Rpt16_V4));
            this.HeaderControl = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btnMini = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnMax = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnClose = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_Print = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbo_Department = new ComponentFactory.Krypton.Toolkit.KryptonComboBox();
            this.cboTeam = new ComponentFactory.Krypton.Toolkit.KryptonComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rdo_PayAll = new System.Windows.Forms.RadioButton();
            this.rdo_Cash = new System.Windows.Forms.RadioButton();
            this.rdo_ATM = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rdo_Team = new System.Windows.Forms.RadioButton();
            this.rdo_Employee = new System.Windows.Forms.RadioButton();
            this.dteDate = new ComponentFactory.Krypton.Toolkit.KryptonDateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txt_Search = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.btn_Export = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Fillter = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Search = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.panel_left = new System.Windows.Forms.Panel();
            this.LVEmployee = new System.Windows.Forms.ListView();
            this.chk_All = new System.Windows.Forms.CheckBox();
            this.kryptonHeader1 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.panel_right = new System.Windows.Forms.Panel();
            this.Panel_Done = new System.Windows.Forms.Panel();
            this.dte_RpDate = new TN_Tools.TNDateTime();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txt_RpGiamDoc = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txt_RpHCNS = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txt_RpKeToan = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txt_RpNguoiLap = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txt_RpTitle = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.kryptonHeader6 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btnClose_Panel_Message = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btn_Done = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.GVData = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.kryptonHeader2 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbo_Department)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboTeam)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel_left.SuspendLayout();
            this.panel_right.SuspendLayout();
            this.Panel_Done.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GVData)).BeginInit();
            this.SuspendLayout();
            // 
            // HeaderControl
            // 
            this.HeaderControl.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnMini,
            this.btnMax,
            this.btnClose});
            this.HeaderControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.HeaderControl.Location = new System.Drawing.Point(0, 0);
            this.HeaderControl.Name = "HeaderControl";
            this.HeaderControl.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.HeaderControl.Size = new System.Drawing.Size(1366, 42);
            this.HeaderControl.TabIndex = 211;
            this.HeaderControl.Values.Description = "";
            this.HeaderControl.Values.Heading = "Bảng lương tổng hợp các bộ phận hỗ trợ sản xuất - Lương đúng";
            this.HeaderControl.Values.Image = ((System.Drawing.Image)(resources.GetObject("HeaderControl.Values.Image")));
            // 
            // btnMini
            // 
            this.btnMini.Image = ((System.Drawing.Image)(resources.GetObject("btnMini.Image")));
            this.btnMini.UniqueName = "F5F06E8241504E72ABB5BEA3F6A9B753";
            // 
            // btnMax
            // 
            this.btnMax.Image = ((System.Drawing.Image)(resources.GetObject("btnMax.Image")));
            this.btnMax.UniqueName = "035D1A4881E44F58A084C31DE7352A94";
            // 
            // btnClose
            // 
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.UniqueName = "11B07C6F4E1C4F9D8B91BD924CB0EBE6";
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panel1.Controls.Add(this.btn_Print);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.cbo_Department);
            this.panel1.Controls.Add(this.cboTeam);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.dteDate);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.txt_Search);
            this.panel1.Controls.Add(this.btn_Export);
            this.panel1.Controls.Add(this.btn_Fillter);
            this.panel1.Controls.Add(this.btn_Search);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 42);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1366, 80);
            this.panel1.TabIndex = 213;
            // 
            // btn_Print
            // 
            this.btn_Print.Location = new System.Drawing.Point(1188, 20);
            this.btn_Print.Name = "btn_Print";
            this.btn_Print.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Print.Size = new System.Drawing.Size(111, 40);
            this.btn_Print.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Print.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Print.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Print.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Print.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Print.TabIndex = 320;
            this.btn_Print.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Print.Values.Image")));
            this.btn_Print.Values.Text = "Xuất In";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(428, 45);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 16);
            this.label3.TabIndex = 318;
            this.label3.Text = "Tổ nhóm";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(426, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 16);
            this.label2.TabIndex = 319;
            this.label2.Text = "Bộ phận";
            // 
            // cbo_Department
            // 
            this.cbo_Department.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_Department.DropDownWidth = 119;
            this.cbo_Department.Location = new System.Drawing.Point(492, 9);
            this.cbo_Department.Name = "cbo_Department";
            this.cbo_Department.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.cbo_Department.Size = new System.Drawing.Size(210, 24);
            this.cbo_Department.StateCommon.ComboBox.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.cbo_Department.StateCommon.ComboBox.Border.Rounding = 4;
            this.cbo_Department.StateCommon.ComboBox.Border.Width = 1;
            this.cbo_Department.StateCommon.ComboBox.Content.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cbo_Department.StateCommon.Item.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.cbo_Department.StateCommon.Item.Border.Rounding = 4;
            this.cbo_Department.StateCommon.Item.Border.Width = 1;
            this.cbo_Department.StateCommon.Item.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cbo_Department.TabIndex = 316;
            // 
            // cboTeam
            // 
            this.cboTeam.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTeam.DropDownWidth = 119;
            this.cboTeam.Location = new System.Drawing.Point(492, 39);
            this.cboTeam.Name = "cboTeam";
            this.cboTeam.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.cboTeam.Size = new System.Drawing.Size(210, 24);
            this.cboTeam.StateCommon.ComboBox.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.cboTeam.StateCommon.ComboBox.Border.Rounding = 4;
            this.cboTeam.StateCommon.ComboBox.Border.Width = 1;
            this.cboTeam.StateCommon.ComboBox.Content.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cboTeam.StateCommon.Item.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.cboTeam.StateCommon.Item.Border.Rounding = 4;
            this.cboTeam.StateCommon.Item.Border.Width = 1;
            this.cboTeam.StateCommon.Item.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cboTeam.TabIndex = 317;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rdo_PayAll);
            this.groupBox1.Controls.Add(this.rdo_Cash);
            this.groupBox1.Controls.Add(this.rdo_ATM);
            this.groupBox1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.groupBox1.ForeColor = System.Drawing.Color.Navy;
            this.groupBox1.Location = new System.Drawing.Point(709, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(107, 64);
            this.groupBox1.TabIndex = 314;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Hình thức trả";
            // 
            // rdo_PayAll
            // 
            this.rdo_PayAll.AutoSize = true;
            this.rdo_PayAll.Checked = true;
            this.rdo_PayAll.Font = new System.Drawing.Font("Tahoma", 9F);
            this.rdo_PayAll.Location = new System.Drawing.Point(6, 12);
            this.rdo_PayAll.Name = "rdo_PayAll";
            this.rdo_PayAll.Size = new System.Drawing.Size(75, 18);
            this.rdo_PayAll.TabIndex = 209;
            this.rdo_PayAll.TabStop = true;
            this.rdo_PayAll.Text = "1. Tất cả";
            this.rdo_PayAll.UseVisualStyleBackColor = true;
            // 
            // rdo_Cash
            // 
            this.rdo_Cash.AutoSize = true;
            this.rdo_Cash.Font = new System.Drawing.Font("Tahoma", 9F);
            this.rdo_Cash.Location = new System.Drawing.Point(6, 44);
            this.rdo_Cash.Name = "rdo_Cash";
            this.rdo_Cash.Size = new System.Drawing.Size(89, 18);
            this.rdo_Cash.TabIndex = 210;
            this.rdo_Cash.Text = "3. Tiền mặt";
            this.rdo_Cash.UseVisualStyleBackColor = true;
            // 
            // rdo_ATM
            // 
            this.rdo_ATM.AutoSize = true;
            this.rdo_ATM.Font = new System.Drawing.Font("Tahoma", 9F);
            this.rdo_ATM.Location = new System.Drawing.Point(6, 28);
            this.rdo_ATM.Name = "rdo_ATM";
            this.rdo_ATM.Size = new System.Drawing.Size(65, 18);
            this.rdo_ATM.TabIndex = 210;
            this.rdo_ATM.Text = "2. ATM";
            this.rdo_ATM.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rdo_Team);
            this.groupBox2.Controls.Add(this.rdo_Employee);
            this.groupBox2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.groupBox2.ForeColor = System.Drawing.Color.Navy;
            this.groupBox2.Location = new System.Drawing.Point(822, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(121, 63);
            this.groupBox2.TabIndex = 315;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Kiểu hiển thị";
            // 
            // rdo_Team
            // 
            this.rdo_Team.AutoSize = true;
            this.rdo_Team.Font = new System.Drawing.Font("Tahoma", 9F);
            this.rdo_Team.Location = new System.Drawing.Point(6, 17);
            this.rdo_Team.Name = "rdo_Team";
            this.rdo_Team.Size = new System.Drawing.Size(105, 18);
            this.rdo_Team.TabIndex = 209;
            this.rdo_Team.Text = "1.DS Tổ nhóm";
            this.rdo_Team.UseVisualStyleBackColor = true;
            // 
            // rdo_Employee
            // 
            this.rdo_Employee.AutoSize = true;
            this.rdo_Employee.Checked = true;
            this.rdo_Employee.Font = new System.Drawing.Font("Tahoma", 9F);
            this.rdo_Employee.Location = new System.Drawing.Point(6, 36);
            this.rdo_Employee.Name = "rdo_Employee";
            this.rdo_Employee.Size = new System.Drawing.Size(109, 18);
            this.rdo_Employee.TabIndex = 210;
            this.rdo_Employee.TabStop = true;
            this.rdo_Employee.Text = "2.DS Nhân viên";
            this.rdo_Employee.UseVisualStyleBackColor = true;
            // 
            // dteDate
            // 
            this.dteDate.CustomFormat = "MM/yyyy";
            this.dteDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dteDate.Location = new System.Drawing.Point(242, 13);
            this.dteDate.Name = "dteDate";
            this.dteDate.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.dteDate.ShowUpDown = true;
            this.dteDate.Size = new System.Drawing.Size(99, 23);
            this.dteDate.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.dteDate.StateCommon.Border.Rounding = 4;
            this.dteDate.StateCommon.Border.Width = 1;
            this.dteDate.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dteDate.TabIndex = 308;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(192, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 15);
            this.label1.TabIndex = 307;
            this.label1.Text = "Tháng";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(137, 45);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(97, 16);
            this.label12.TabIndex = 214;
            this.label12.Text = "Mã thẻ/ họ tên";
            // 
            // txt_Search
            // 
            this.txt_Search.Location = new System.Drawing.Point(242, 39);
            this.txt_Search.Name = "txt_Search";
            this.txt_Search.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_Search.Size = new System.Drawing.Size(180, 26);
            this.txt_Search.StateCommon.Border.ColorAngle = 1F;
            this.txt_Search.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_Search.StateCommon.Border.Rounding = 4;
            this.txt_Search.StateCommon.Border.Width = 1;
            this.txt_Search.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Search.TabIndex = 213;
            // 
            // btn_Export
            // 
            this.btn_Export.Location = new System.Drawing.Point(1072, 19);
            this.btn_Export.Name = "btn_Export";
            this.btn_Export.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Export.Size = new System.Drawing.Size(113, 40);
            this.btn_Export.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Export.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Export.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Export.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Export.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Export.TabIndex = 205;
            this.btn_Export.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Export.Values.Image")));
            this.btn_Export.Values.Text = "Xuất Excel";
            // 
            // btn_Fillter
            // 
            this.btn_Fillter.Location = new System.Drawing.Point(12, 13);
            this.btn_Fillter.Name = "btn_Fillter";
            this.btn_Fillter.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Fillter.Size = new System.Drawing.Size(113, 40);
            this.btn_Fillter.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Fillter.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Fillter.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Fillter.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Fillter.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Fillter.TabIndex = 201;
            this.btn_Fillter.Tag = "0";
            this.btn_Fillter.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Fillter.Values.Image")));
            this.btn_Fillter.Values.Text = "Mở lọc";
            // 
            // btn_Search
            // 
            this.btn_Search.Location = new System.Drawing.Point(954, 20);
            this.btn_Search.Name = "btn_Search";
            this.btn_Search.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Search.Size = new System.Drawing.Size(113, 40);
            this.btn_Search.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Search.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Search.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Search.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Search.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Search.TabIndex = 201;
            this.btn_Search.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Search.Values.Image")));
            this.btn_Search.Values.Text = "Tìm kiếm";
            // 
            // panel_left
            // 
            this.panel_left.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panel_left.Controls.Add(this.LVEmployee);
            this.panel_left.Controls.Add(this.chk_All);
            this.panel_left.Controls.Add(this.kryptonHeader1);
            this.panel_left.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel_left.Location = new System.Drawing.Point(0, 122);
            this.panel_left.Name = "panel_left";
            this.panel_left.Size = new System.Drawing.Size(422, 478);
            this.panel_left.TabIndex = 214;
            // 
            // LVEmployee
            // 
            this.LVEmployee.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.LVEmployee.CheckBoxes = true;
            this.LVEmployee.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LVEmployee.Font = new System.Drawing.Font("Tahoma", 9F);
            this.LVEmployee.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.LVEmployee.FullRowSelect = true;
            this.LVEmployee.GridLines = true;
            this.LVEmployee.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.LVEmployee.HideSelection = false;
            this.LVEmployee.Location = new System.Drawing.Point(0, 30);
            this.LVEmployee.Name = "LVEmployee";
            this.LVEmployee.Size = new System.Drawing.Size(422, 448);
            this.LVEmployee.TabIndex = 219;
            this.LVEmployee.UseCompatibleStateImageBehavior = false;
            this.LVEmployee.View = System.Windows.Forms.View.Details;
            // 
            // chk_All
            // 
            this.chk_All.AutoSize = true;
            this.chk_All.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chk_All.Checked = true;
            this.chk_All.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chk_All.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chk_All.ForeColor = System.Drawing.Color.Navy;
            this.chk_All.Location = new System.Drawing.Point(312, 5);
            this.chk_All.Name = "chk_All";
            this.chk_All.Size = new System.Drawing.Size(93, 20);
            this.chk_All.TabIndex = 215;
            this.chk_All.Text = "Chọn tất cả";
            this.chk_All.UseVisualStyleBackColor = true;
            // 
            // kryptonHeader1
            // 
            this.kryptonHeader1.AutoSize = false;
            this.kryptonHeader1.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader1.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader1.Name = "kryptonHeader1";
            this.kryptonHeader1.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader1.Size = new System.Drawing.Size(422, 30);
            this.kryptonHeader1.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader1.TabIndex = 220;
            this.kryptonHeader1.Values.Description = "";
            this.kryptonHeader1.Values.Heading = "Danh sách cột lương";
            // 
            // panel_right
            // 
            this.panel_right.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panel_right.Controls.Add(this.Panel_Done);
            this.panel_right.Controls.Add(this.GVData);
            this.panel_right.Controls.Add(this.kryptonHeader2);
            this.panel_right.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_right.Location = new System.Drawing.Point(422, 122);
            this.panel_right.Name = "panel_right";
            this.panel_right.Size = new System.Drawing.Size(944, 478);
            this.panel_right.TabIndex = 215;
            // 
            // Panel_Done
            // 
            this.Panel_Done.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.Panel_Done.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Done.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel_Done.Controls.Add(this.dte_RpDate);
            this.Panel_Done.Controls.Add(this.label10);
            this.Panel_Done.Controls.Add(this.label9);
            this.Panel_Done.Controls.Add(this.label7);
            this.Panel_Done.Controls.Add(this.label6);
            this.Panel_Done.Controls.Add(this.txt_RpGiamDoc);
            this.Panel_Done.Controls.Add(this.txt_RpHCNS);
            this.Panel_Done.Controls.Add(this.txt_RpKeToan);
            this.Panel_Done.Controls.Add(this.txt_RpNguoiLap);
            this.Panel_Done.Controls.Add(this.label8);
            this.Panel_Done.Controls.Add(this.label16);
            this.Panel_Done.Controls.Add(this.txt_RpTitle);
            this.Panel_Done.Controls.Add(this.kryptonHeader6);
            this.Panel_Done.Controls.Add(this.btn_Done);
            this.Panel_Done.Location = new System.Drawing.Point(203, 111);
            this.Panel_Done.Name = "Panel_Done";
            this.Panel_Done.Size = new System.Drawing.Size(492, 325);
            this.Panel_Done.TabIndex = 291;
            // 
            // dte_RpDate
            // 
            this.dte_RpDate.CustomFormat = "dd/MM/yyyy";
            this.dte_RpDate.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.dte_RpDate.Location = new System.Drawing.Point(183, 35);
            this.dte_RpDate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dte_RpDate.Name = "dte_RpDate";
            this.dte_RpDate.Size = new System.Drawing.Size(121, 26);
            this.dte_RpDate.TabIndex = 316;
            this.dte_RpDate.Value = new System.DateTime(((long)(0)));
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label10.Location = new System.Drawing.Point(51, 250);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(90, 15);
            this.label10.TabIndex = 269;
            this.label10.Text = "Tổng giám đốc";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label9.Location = new System.Drawing.Point(78, 219);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(61, 15);
            this.label9.TabIndex = 269;
            this.label9.Text = "BP HCNS";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label7.Location = new System.Drawing.Point(47, 181);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(94, 15);
            this.label7.TabIndex = 269;
            this.label7.Text = "Kế toán trưởng";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label6.Location = new System.Drawing.Point(78, 149);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 15);
            this.label6.TabIndex = 269;
            this.label6.Text = "Người lập";
            // 
            // txt_RpGiamDoc
            // 
            this.txt_RpGiamDoc.Location = new System.Drawing.Point(183, 241);
            this.txt_RpGiamDoc.Name = "txt_RpGiamDoc";
            this.txt_RpGiamDoc.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_RpGiamDoc.Size = new System.Drawing.Size(282, 26);
            this.txt_RpGiamDoc.StateCommon.Border.ColorAngle = 1F;
            this.txt_RpGiamDoc.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_RpGiamDoc.StateCommon.Border.Rounding = 4;
            this.txt_RpGiamDoc.StateCommon.Border.Width = 1;
            this.txt_RpGiamDoc.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_RpGiamDoc.TabIndex = 289;
            // 
            // txt_RpHCNS
            // 
            this.txt_RpHCNS.Location = new System.Drawing.Point(183, 209);
            this.txt_RpHCNS.Name = "txt_RpHCNS";
            this.txt_RpHCNS.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_RpHCNS.Size = new System.Drawing.Size(282, 26);
            this.txt_RpHCNS.StateCommon.Border.ColorAngle = 1F;
            this.txt_RpHCNS.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_RpHCNS.StateCommon.Border.Rounding = 4;
            this.txt_RpHCNS.StateCommon.Border.Width = 1;
            this.txt_RpHCNS.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_RpHCNS.TabIndex = 289;
            // 
            // txt_RpKeToan
            // 
            this.txt_RpKeToan.Location = new System.Drawing.Point(183, 178);
            this.txt_RpKeToan.Name = "txt_RpKeToan";
            this.txt_RpKeToan.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_RpKeToan.Size = new System.Drawing.Size(282, 26);
            this.txt_RpKeToan.StateCommon.Border.ColorAngle = 1F;
            this.txt_RpKeToan.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_RpKeToan.StateCommon.Border.Rounding = 4;
            this.txt_RpKeToan.StateCommon.Border.Width = 1;
            this.txt_RpKeToan.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_RpKeToan.TabIndex = 289;
            // 
            // txt_RpNguoiLap
            // 
            this.txt_RpNguoiLap.Location = new System.Drawing.Point(183, 147);
            this.txt_RpNguoiLap.Name = "txt_RpNguoiLap";
            this.txt_RpNguoiLap.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_RpNguoiLap.Size = new System.Drawing.Size(282, 26);
            this.txt_RpNguoiLap.StateCommon.Border.ColorAngle = 1F;
            this.txt_RpNguoiLap.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_RpNguoiLap.StateCommon.Border.Rounding = 4;
            this.txt_RpNguoiLap.StateCommon.Border.Width = 1;
            this.txt_RpNguoiLap.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_RpNguoiLap.TabIndex = 289;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label8.Location = new System.Drawing.Point(94, 39);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 15);
            this.label8.TabIndex = 269;
            this.label8.Text = "Ngày in";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label16.Location = new System.Drawing.Point(97, 67);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(48, 15);
            this.label16.TabIndex = 269;
            this.label16.Text = "Tiêu đề";
            // 
            // txt_RpTitle
            // 
            this.txt_RpTitle.Location = new System.Drawing.Point(183, 67);
            this.txt_RpTitle.Multiline = true;
            this.txt_RpTitle.Name = "txt_RpTitle";
            this.txt_RpTitle.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_RpTitle.Size = new System.Drawing.Size(282, 76);
            this.txt_RpTitle.StateCommon.Border.ColorAngle = 1F;
            this.txt_RpTitle.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_RpTitle.StateCommon.Border.Rounding = 4;
            this.txt_RpTitle.StateCommon.Border.Width = 1;
            this.txt_RpTitle.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_RpTitle.TabIndex = 268;
            // 
            // kryptonHeader6
            // 
            this.kryptonHeader6.AutoSize = false;
            this.kryptonHeader6.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnClose_Panel_Message});
            this.kryptonHeader6.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader6.HeaderStyle = ComponentFactory.Krypton.Toolkit.HeaderStyle.Secondary;
            this.kryptonHeader6.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader6.Name = "kryptonHeader6";
            this.kryptonHeader6.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader6.Size = new System.Drawing.Size(490, 30);
            this.kryptonHeader6.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.kryptonHeader6.TabIndex = 206;
            this.kryptonHeader6.Values.Description = "";
            this.kryptonHeader6.Values.Heading = "!.Thông tin xuất báo cáo";
            this.kryptonHeader6.Values.Image = null;
            // 
            // btnClose_Panel_Message
            // 
            this.btnClose_Panel_Message.Checked = ComponentFactory.Krypton.Toolkit.ButtonCheckState.Checked;
            this.btnClose_Panel_Message.Enabled = ComponentFactory.Krypton.Toolkit.ButtonEnabled.True;
            this.btnClose_Panel_Message.Type = ComponentFactory.Krypton.Toolkit.PaletteButtonSpecStyle.Close;
            this.btnClose_Panel_Message.UniqueName = "BEA2AE54CDCE44F263AB208C74E6907A";
            // 
            // btn_Done
            // 
            this.btn_Done.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Done.Location = new System.Drawing.Point(203, 274);
            this.btn_Done.Name = "btn_Done";
            this.btn_Done.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Done.Size = new System.Drawing.Size(119, 40);
            this.btn_Done.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Done.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Done.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Done.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Done.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Done.TabIndex = 12;
            this.btn_Done.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Done.Values.Image")));
            this.btn_Done.Values.Text = "OK";
            // 
            // GVData
            // 
            this.GVData.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None;
            this.GVData.AllowEditing = false;
            this.GVData.AllowResizing = C1.Win.C1FlexGrid.AllowResizingEnum.None;
            this.GVData.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None;
            this.GVData.AutoResize = true;
            this.GVData.ColumnInfo = "10,1,0,0,0,95,Columns:";
            this.GVData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GVData.ExtendLastCol = true;
            this.GVData.Location = new System.Drawing.Point(0, 30);
            this.GVData.Name = "GVData";
            this.GVData.Rows.DefaultSize = 19;
            this.GVData.Size = new System.Drawing.Size(944, 448);
            this.GVData.TabIndex = 235;
            // 
            // kryptonHeader2
            // 
            this.kryptonHeader2.AutoSize = false;
            this.kryptonHeader2.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader2.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader2.Name = "kryptonHeader2";
            this.kryptonHeader2.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader2.Size = new System.Drawing.Size(944, 30);
            this.kryptonHeader2.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader2.TabIndex = 213;
            this.kryptonHeader2.Values.Description = "";
            this.kryptonHeader2.Values.Heading = "Danh sách nhân sự";
            // 
            // Frm_Rpt16_V4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1366, 600);
            this.Controls.Add(this.panel_right);
            this.Controls.Add(this.panel_left);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.HeaderControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_Rpt16_V4";
            this.Text = "Bảng lương tổng hợp các bộ phận hỗ trợ sản xuất - Lương đúng";
            this.Load += new System.EventHandler(this.Frm_Rpt16_V4_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbo_Department)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboTeam)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel_left.ResumeLayout(false);
            this.panel_left.PerformLayout();
            this.panel_right.ResumeLayout(false);
            this.Panel_Done.ResumeLayout(false);
            this.Panel_Done.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GVData)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ComponentFactory.Krypton.Toolkit.KryptonHeader HeaderControl;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMini;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMax;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnClose;
        private System.Windows.Forms.Panel panel1;
        private ComponentFactory.Krypton.Toolkit.KryptonDateTimePicker dteDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label12;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_Search;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Export;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Fillter;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Search;
        private System.Windows.Forms.Panel panel_left;
        private System.Windows.Forms.ListView LVEmployee;
        private System.Windows.Forms.CheckBox chk_All;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader1;
        private System.Windows.Forms.Panel panel_right;
        private C1.Win.C1FlexGrid.C1FlexGrid GVData;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rdo_PayAll;
        private System.Windows.Forms.RadioButton rdo_Cash;
        private System.Windows.Forms.RadioButton rdo_ATM;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rdo_Team;
        private System.Windows.Forms.RadioButton rdo_Employee;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private ComponentFactory.Krypton.Toolkit.KryptonComboBox cbo_Department;
        private ComponentFactory.Krypton.Toolkit.KryptonComboBox cboTeam;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Print;
        private System.Windows.Forms.Panel Panel_Done;
        private TN_Tools.TNDateTime dte_RpDate;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_RpGiamDoc;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_RpHCNS;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_RpKeToan;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_RpNguoiLap;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label16;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_RpTitle;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader6;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnClose_Panel_Message;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Done;
    }
}