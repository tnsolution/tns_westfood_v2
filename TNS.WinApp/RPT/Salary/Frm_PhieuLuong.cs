﻿using C1.Win.C1FlexGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TN.SLR;
using TNS.HRM;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;
using TNS.WinApp.FileRpt;

namespace TNS.WinApp
{
    public partial class Frm_PhieuLuong : Form
    {
        private int _EmployeeKey = 0;
        public Frm_PhieuLuong()
        {
            InitializeComponent();
            btnClose.Click += btnClose_Click;
            btnMax.Click += btnMax_Click;
            btnMini.Click += btnMini_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            btn_Search.Click += Btn_Search_Click;
        }

        private void Frm_PhieuLuong_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            Check_RoleForm();
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            dte_Date.Value = SessionUser.Date_Work;
            Refresh_ThongTinPhieu();
            DataTable ztb = new DataTable();
            DataTable ztb_Info = new DataTable();
            LoadReport(ztb, ztb_Info);
        }

        private void Btn_Search_Click(object sender, EventArgs e)
        {

            string Status = "Tìm DL form " + HeaderControl.Text + " > Từ khóa:" + txt_Search.Text;
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);

            DataTable ztb = new DataTable();
            DataTable ztb_Info = new DataTable();
            if (txt_Search.Text.Trim().Length > 0)
            {
                Employee_Info zInfo = new Employee_Info();
                zInfo.GetEmployeeID(txt_Search.Text.Trim());
                if (zInfo.Key == "")
                {
                    Utils.TNMessageBoxOK("Mã nhân viên không đúng !", 1);
                    LoadReport(ztb, ztb_Info);
                    Refresh_ThongTinPhieu();
                }
                else
                {
                    ztb_Info = Salary_Data.ThongTinPhieuLuong(dte_Date.Value, zInfo.Key);
                    if (ztb_Info.Rows.Count == 0)
                    {
                        Utils.TNMessageBoxOK("Không tìm thấy thông tin lương !", 1);
                        Refresh_ThongTinPhieu();
                        LoadReport(ztb, ztb_Info);
                    }
                    else
                    {
                        DataRow r = ztb_Info.Rows[0];
                        txt_EmployeeID.Text = r["EmployeeID"].ToString().Trim();
                        txt_EmployeeName.Text = r["EmployeeName"].ToString().Trim();
                        txt_TeamName.Text = r["TeamName"].ToString().Trim();
                        txt_PositionName.Text = r["PositionName"].ToString().Trim();
                        txt_ATM.Text = r["ATM"].ToString().Trim();
                        txt_Month.Text = r["DateWrite"].ToString().Trim();

                        ztb = Salary_Data.ChitietPhieuLuong(dte_Date.Value, zInfo.Key);
                        LoadReport(ztb, ztb_Info);
                    }

                }
            }
            else
            {
                Utils.TNMessageBoxOK("Vui lòng nhập mã nhân viên !", 1);
                Refresh_ThongTinPhieu();
                LoadReport(ztb, ztb_Info);
            }
        }
        void Refresh_ThongTinPhieu()
        {
            txt_EmployeeID.Text = "";
            txt_EmployeeName.Text = "";
            txt_TeamName.Text = "";
            txt_PositionName.Text = "";
            txt_ATM.Text = "";
            txt_Month.Text = "";
        }
        void LoadReport(DataTable ztb, DataTable ztb_Info)
        {
            DataSet Ds = new DataSet();
            DataTable zTable = Ds.Tables["Employee"];
            DataTable zTableDetail = Ds.Tables["Salary"];

            zTableDetail = ztb;
            zTable = ztb_Info;

            rpt_PhieuLuong rpt = new rpt_PhieuLuong();
            rpt.Database.Tables["Employee"].SetDataSource(zTable);
            rpt.Database.Tables["Salary"].SetDataSource(zTableDetail);
            rptViewer.ReportSource = rpt;
        }

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                //this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
                //btn_Export.Enabled = false;
            }

        }
        #endregion
    }
}
