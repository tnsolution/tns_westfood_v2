﻿using C1.Win.C1FlexGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.LOG;
using TNS.Misc;
using TNS.RPT;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_SalaryMonth_Worker : Form
    {
        private int _TeamKeyClose = 0;
        private int _DepartmentKeyClose = 0;
        private DateTime _FromDate;
        private DateTime _ToDate;
        private string _IDSalary = "";
        public Frm_SalaryMonth_Worker()
        {
            InitializeComponent();
            btn_Search.Click += Btn_Search_Click;
            btnClose.Click += btnClose_Click;
            btnMax.Click += btnMax_Click;
            btnMini.Click += btnMini_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            btn_Export.Click += Btn_Export_Click;
            btn_Fillter.Click += Btn_Fillter_Click;
            chk_All.CheckedChanged += Chk_All_CheckedChanged;
            cbo_Department.SelectedIndexChanged += Cbo_Department_SelectedIndexChanged;
            rdo_Department.CheckedChanged += Rdo_Department_CheckedChanged;
            rdo_Team.CheckedChanged += Rdo_Team_CheckedChanged;
            rdo_Employee.CheckedChanged += Rdo_Employee_CheckedChanged;
            dte_FromDate.Validating += DteDate_ValueChanged;
            dte_ToDate.Validating += DteDate_ValueChanged;

            InitLayout_LV(LVEmployee);

            Utils.DrawLVStyle(ref LVEmployee);
            Utils.SizeLastColumn_LV(LVEmployee);
            LoadDataToToolbox.KryptonComboBox(cbo_Department, "SELECT DepartmentKey, DepartmentName FROM[dbo].[SYS_Department] WHERE DepartmentKey != 98  AND RecordStatus< 99 AND BranchKey= 4 AND DepartmentKey != 26 ", "---- Chọn tất cả----");

        }

        private void Frm_SalaryMonth_Worker_Load(object sender, EventArgs e)
        {
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            Check_RoleForm();

            DateTime DateView = SessionUser.Date_Work;
            dte_FromDate.Value = new DateTime(DateView.Year, 1, 1);
            dte_ToDate.Value = dte_FromDate.Value.AddYears(1).AddDays(-1);
            dte_ToDate.Value = new DateTime(dte_ToDate.Value.Year, dte_ToDate.Value.Month, 1);
            InitData(true);
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            cbo_Team.Enabled = false;
            txt_Search.Enabled = false;
            panel_left.Visible = false;
        }
        private void DteDate_ValueChanged(object sender, EventArgs e)
        {
            InitData(true);
        }
        private void Cbo_Department_SelectedIndexChanged(object sender, EventArgs e)
        {

            LoadDataToToolbox.KryptonComboBox(cbo_Team, "SELECT TeamKey,TeamName FROM SYS_Team WHERE TeamKey != 98 AND DepartmentKey = " + cbo_Department.SelectedValue.ToInt() + "  AND RecordStatus < 99 ORDER BY Rank", "---- Chọn tất cả ----");
        }
        private void Rdo_Department_CheckedChanged(object sender, EventArgs e)
        {
            if (rdo_Department.Checked == true)
            {
                cbo_Department.Enabled = true;
                cbo_Team.Enabled = false;
                txt_Search.Enabled = false;
            }
        }
        private void Rdo_Employee_CheckedChanged(object sender, EventArgs e)
        {
            cbo_Department.Enabled = false;
            cbo_Team.Enabled = false;
            txt_Search.Enabled = true;

        }
        private void Rdo_Team_CheckedChanged(object sender, EventArgs e)
        {
            cbo_Department.Enabled = true;
            cbo_Team.Enabled = true;
            txt_Search.Enabled = false;
        }
        private void Chk_All_CheckedChanged(object sender, EventArgs e)
        {
            if (chk_All.Checked == true)
                InitData(true);
            else
                InitData(false);
        }
        private void Btn_Fillter_Click(object sender, EventArgs e)
        {
            if (btn_Fillter.Tag.ToString() == "0")
            {
                btn_Fillter.Text = "Ẩn lọc";
                btn_Fillter.Tag = 1;
                panel_left.Visible = true;
            }
            else
            {
                btn_Fillter.Text = "Mở lọc";
                btn_Fillter.Tag = 0;
                panel_left.Visible = false;
            }
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            if (cbo_Department.SelectedIndex > 0)
                _DepartmentKeyClose = cbo_Department.SelectedValue.ToInt();
            else
                _DepartmentKeyClose = 0;
            if (cbo_Team.SelectedIndex > 0)
                _TeamKeyClose = cbo_Team.SelectedValue.ToInt();
            else
                _TeamKeyClose = 0;
            _FromDate = dte_FromDate.Value;
            _ToDate = dte_ToDate.Value;
            _IDSalary = GetSalaryID();

            if (_IDSalary == "")
            {
                Utils.TNMessageBoxOK("Vui lòng chọn ít nhất 1 chỉ tiêu lương!", 1);
                return;
            }
            //if (rdo_Employee.Checked == true)
            //{
            //    if (_DepartmentKeyClose == 0)
            //    {
            //        Utils.TNMessageBoxOK("Vui lòng chọn 1 nhóm hoặc 1 nhân viên", 1);
            //        return;
            //    }
            //    if (_TeamKeyClose == 0)
            //    {
            //        Utils.TNMessageBoxOK("Vui lòng chọn 1 nhóm hoặc 1 nhân viên", 1);
            //        return;
            //    }

            //}
            if (rdo_Employee.Checked == true && txt_Search.Text.Length ==0)
            {
                Utils.TNMessageBoxOK("Vui lòng nhập 1 mã thẻ", 1);
                       return;
            }
            string Status = "Tìm DL form " + HeaderControl.Text + " > tháng: " + dte_FromDate.Value.ToString("MM/yyyy") + "-" + dte_ToDate.Value.ToString("MM/yyyy");
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);

            try
            {
                using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }
            }
            catch (Exception ex)
            {
                Utils.TNMessageBoxOK(ex.ToString(), 4);
            }
        }
        private string GetSalaryID()
        {
            string zMesage = "";
            if (LVEmployee.Items.Count > 0)
            {
                int zPlag = 0;
                zMesage += "( ";
                for (int i = 0; i < LVEmployee.Items.Count; i++)
                {
                    if (LVEmployee.Items[i].Checked)
                    {
                        zMesage += "A.CodeID ='" + LVEmployee.Items[i].Tag + "' OR ";
                        zPlag++;
                    }
                }
                if (zPlag != 0)
                {
                    zMesage = zMesage.Remove(zMesage.Length - 3, 3);
                    zMesage += " )";
                }
                else
                    zMesage = "";
            }
            return zMesage;

        }

        #region[Xem DislayData]
        private void DisplayData()
        {

            DateTime FromDate = new DateTime(dte_FromDate.Value.Year, dte_FromDate.Value.Month, 1, 0, 0, 0);

            DateTime DateView = dte_ToDate.Value;
            DateTime zFromDate = new DateTime(DateView.Year, DateView.Month, 1, 0, 0, 0);
            DateTime ToDate = zFromDate.AddMonths(1).AddDays(-1);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            DataTable zTable = new DataTable();
            if (rdo_Employee.Checked == true)
                zTable = Report_Salary.THUNHAPTHEOTHANG_BOPHANSANXUAT_NHANSU(_FromDate, _ToDate, 0, 0, txt_Search.Text, _IDSalary);
            if (rdo_Team.Checked == true)
                zTable = Report_Salary.THUNHAPTHEOTHANG_BOPHANSANXUAT_NHOM(_FromDate, _ToDate, _DepartmentKeyClose, _TeamKeyClose, _IDSalary);
            if (rdo_Department.Checked == true)
                zTable = Report_Salary.THUNHAPTHEOTHANG_BOPHANSANXUAT_BOPHAN(_FromDate, _ToDate, _DepartmentKeyClose, _IDSalary);

            if (zTable.Rows.Count > 0)
            {
                PivotTable zPvt = new PivotTable(zTable);
                DataTable zTablePivot = zPvt.Generate_ASC("HeaderColumn", "LeftColumn", new string[] { "Amount" }, "CÔNG VIỆC", "TỔNG CỘNG", "TỔNG CỘNG", new string[] { "Sô tiền" }, 0);

                zTablePivot.Columns.Add("CodeRank", typeof(int));
                for (int i = 0; i < zTablePivot.Rows.Count - 1; i++)
                {
                    string[] s = zTablePivot.Rows[i][0].ToString().Split('|');
                    zTablePivot.Rows[i]["CodeRank"] = s[1];

                }
                //Xóa cột đầu đã cắt chuỗi và xóa dòng tổng cuối
                DataRow row = zTablePivot.Rows[zTablePivot.Rows.Count - 1];
                zTablePivot.Rows.Remove(row);
                //Sắp xếp nhân viên
                DataView dv = zTablePivot.DefaultView;
                dv.Sort = " CodeRank ASC";
                zTablePivot = dv.ToTable();
                zTablePivot.Columns.Remove("CodeRank");


                string[] temp = zTablePivot.Columns[1].ColumnName.Split('|');
                string DateFlag = temp[0];
                string KeyFlag = temp[1];
                int m = 0;
                for (int i = 1; i < zTablePivot.Columns.Count; i++)
                {
                    temp = zTablePivot.Columns[i].ColumnName.Split('|');
                    if (temp.Length == 2)// nếu là TONGCONG thì add vào cột tổng nhỏ
                    {
                        zTablePivot.Columns.Add(DateFlag + "|Tổng", typeof(double)).SetOrdinal(i);
                        break;
                    }
                    string Date = temp[0];
                    string TeamKey = temp[1];
                    string TeamName = temp[2];


                    if (DateFlag != "0" && DateFlag != Date)
                    {
                        //add cột tổng
                        zTablePivot.Columns.Add(DateFlag + "|Tổng", typeof(double)).SetOrdinal(i);
                        DateFlag = Date;
                    }

                    if (KeyFlag != TeamKey)
                    {
                        KeyFlag = TeamKey;
                    }

                }

                //tính toán cột tổng mỗi tháng
                foreach (DataRow r in zTablePivot.Rows)
                {
                    temp = zTablePivot.Columns[1].ColumnName.Split('|');
                    double TongCotThang = 0;
                    DateFlag = temp[0];

                    for (int k = 1; k < zTablePivot.Columns.Count; k++)
                    {
                        temp = zTablePivot.Columns[k].ColumnName.Split('|');
                        string Date = temp[0];

                        if (DateFlag != Date)
                        {
                            r[k - 1] = TongCotThang.ToString();

                            TongCotThang = 0;
                            DateFlag = temp[0];
                        }
                        double zSoTien = 0;
                        if (double.TryParse(r[k].ToString(), out zSoTien))
                        {

                        }
                        TongCotThang += zSoTien;
                    }
                }

                this.Invoke(new MethodInvoker(delegate ()
                {
                    InitGVProduct(zTablePivot);
                }));
            }
            else
            {
                GVData.Rows.Count = 0;
                GVData.Cols.Count = 0;
                GVData.Clear();
            }
        }

        void InitGVProduct(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();

            int TotalRow = TableView.Rows.Count + 2;
            int ToTalCol = TableView.Columns.Count;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(TotalRow);

            CellRange zCellRange = GVData.GetCellRange(0, 0, 1, 0);
            zCellRange.Data = "STT";

            zCellRange = GVData.GetCellRange(0, 1, 1, 1);
            zCellRange.Data = "NỘI DUNG";

            //Row Header
            int ColStart = 2;
            for (int i = 1; i < TableView.Columns.Count - 1; i++)
            {
                DataColumn Col = TableView.Columns[i];
                string[] strCaption = Col.ColumnName.Split('|');
                if (strCaption.Length > 2)
                {
                    if (strCaption[0] == "99/9999")
                    {
                        GVData.Rows[0][ColStart] = "TỔNG CỘNG";
                        GVData.Rows[1][ColStart] = strCaption[2];
                        GVData.Cols[ColStart].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
                        GVData.Cols[ColStart].StyleNew.ForeColor = Color.Purple;
                        GVData.Cols[ColStart].Width = 120;
                    }
                    else
                    {
                        GVData.Rows[0][ColStart] = "THÁNG " + strCaption[0];
                        GVData.Rows[1][ColStart] = strCaption[2];
                        GVData.Cols[ColStart].Width = 120;
                    }

                }
                else
                {

                    if (strCaption[0] == "99/9999")
                    {
                        GVData.Rows[0][ColStart] = "TỔNG CỘNG";
                        GVData.Rows[1][ColStart] = strCaption[1];
                        GVData.Cols[ColStart].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
                        GVData.Cols[ColStart].StyleNew.ForeColor = Color.Red;
                        GVData.Cols[ColStart].Width = 120;
                    }
                    else
                    {
                        if (strCaption[1] == "Tổng")
                        {
                            GVData.Rows[0][ColStart] = "THÁNG " + strCaption[0];
                            GVData.Rows[1][ColStart] = strCaption[1];
                            GVData.Cols[ColStart].Width = 120;
                            GVData.Cols[ColStart].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
                            //GVData.Cols[ColStart].StyleNew.BackColor = Color.Pink;
                            GVData.Cols[ColStart].StyleNew.ForeColor = Color.Blue;
                        }
                        else
                        {
                            GVData.Rows[0][ColStart] = "THÁNG " + strCaption[0];
                            GVData.Rows[1][ColStart] = strCaption[1];
                            GVData.Cols[ColStart].Width = 120;
                        }
                    }
                }

                ColStart++;
            }


            //--------------------
            //double TotalLK = 0;
            //double TotalTG = 0;
            //double TotalTP = 0;

            //Fill Data
            ColStart = 1;// add data vào cột thứ 1 trở đi
            int RowStart = 2;//add dòng từ số thứ tự 4 trở đi
            int zNo = 1;
            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                DataRow rData = TableView.Rows[rIndex];
                GVData.Rows[RowStart][0] = zNo;
                string[] s = rData[0].ToString().Trim().Split('|');
                GVData.Rows[RowStart][1] = s[0];
                ColStart = 2;
                for (int k = 1; k < rData.ItemArray.Length - 1; k++)
                {
                    GVData.Rows[RowStart][ColStart] = rData[k].Toe0String();// tên nội dung
                    ColStart++;
                }
                zNo++;
                RowStart++;
            }

            ////Style

            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.FixedOnly;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.CenterCenter;
            GVData.Styles.Normal.WordWrap = true;

            //Trộn dòng 0
            GVData.Rows[0].AllowMerging = true;
            GVData.Rows[0].Height = 45;
            //Trộn dòng 1
            //GVData.Rows[1].AllowMerging = true;
            GVData.Rows[1].Height = 45;
            //Trộn dòng 2
            GVData.Rows[2].AllowMerging = true;
            //Trộn dòng cuối
            GVData.Rows[GVData.Rows.Count - 1].AllowMerging = true;

            //Trộn cột đầu
            GVData.Cols[0].AllowMerging = true;
            GVData.Cols[0].Width = 35;
            GVData.Cols[1].AllowMerging = true;
            GVData.Cols[1].Width = 400;

            //Freeze Row and Column
            GVData.Rows.Fixed = 2;
            GVData.Cols.Frozen = 2;

            //GVData.AutoSizeCols(1, GVData.Cols.Count - 1, 10);
            //Canh phải các cột từ số 1 trở đi
            GVData.Cols[0].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[1].TextAlign = TextAlignEnum.LeftCenter;

            GVData.Cols[0].StyleNew.BackColor = Color.Empty;
            GVData.Cols[1].StyleNew.BackColor = Color.Empty;

            for (int i = 2; i < GVData.Cols.Count; i++)
            {
                GVData.Cols[i].TextAlign = TextAlignEnum.RightCenter;
            }

        }
        #endregion

        #region[list view]
        private void InitLayout_LV(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Chỉ tiêu lương";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);
        }
        private void InitData(bool Check)
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LVEmployee;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            DataTable In_Table = new DataTable();
            In_Table = Report_Salary.ListCodeWorker(dte_FromDate.Value,dte_ToDate.Value);
            LV.Items.Clear();
            int n = In_Table.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = "";
                DataRow nRow = In_Table.Rows[i];
                lvi.Tag = nRow["CodeID"];
                lvi.ForeColor = Color.Navy;

                lvi.BackColor = Color.White;
                lvi.ImageIndex = 0;
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = (i + 1).ToString() + "." + nRow["CodeName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = "";
                lvi.SubItems.Add(lvsi);
                lvi.Checked = Check;
                LV.Items.Add(lvi);
            }

            this.Cursor = Cursors.Default;
        }

        #endregion
        private void Btn_Export_Click(object sender, EventArgs e)
        {
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Báo_Cáo_Lương_Tổng _Hợp_Khối_Sản_Xuất_" + dte_FromDate.Value.ToString("MM-yyyy") + "_" + dte_ToDate.Value.ToString("MM-yyyy") + ".xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }

                    string Status = "Xuất Excel form " + HeaderControl.Text + " > đường dẫn và tên file xuất:" + FDialog.FileName;
                    Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);

                    GVData.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }

        }

        void GVData_Message()
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();

            GVData.Cols.Add(1);
            GVData.Rows.Add(1);

            GVData.Rows[0][0] = "Bạn chưa cập nhật, hoặc không tìm thấy dữ liệu phù hợp !.";

            ////Style
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.FixedOnly;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 11, FontStyle.Italic);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.CenterCenter;
            GVData.Rows[0].Height = 50;

            //Freeze Row and Column
            GVData.Rows.Fixed = 1;

            GVData.Cols[0].StyleFixedNew.ForeColor = Color.DarkRed;
            //GVData.Cols[0].StyleFixedNew.BackColor = ColorTranslator.FromHtml("#E3EFFF");
        }

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                //this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
                btn_Export.Enabled = false;
            }
        }
        #endregion
    }
}
