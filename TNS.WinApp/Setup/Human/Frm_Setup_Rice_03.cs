﻿using C1.Win.C1FlexGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TN.Library.System;
using TNS.HRM;
using TNS.LOG;
using TNS.Misc;
using TNS.SLR;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Setup_Rice_03 : Form
    {
        private int _Key = 0;
        private int _KeyTemp = 0;
        private DateTime _FromDate;
        private DateTime _ToDate;
        public Frm_Setup_Rice_03()
        {
            InitializeComponent();
            btnClose.Click += btnClose_Click;
            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;

            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            btn_Save.Click += Btn_Save_Click;
            btn_New.Click += Btn_New_Click;
            btn_Del.Click += Btn_Del_Click;
            btn_Search.Click += Btn_Search_Click;
            GVData.Click += GVData_List_Click;
            btn_Export.Click += Btn_Export_Click;

            chk_Minus.Click += Chk_Minus_Click;
            chk_Plus.Click += Chk_Plus_Click;
            txt_Money.KeyPress += txt_Money_KeyPress;
            txt_Money.KeyUp += Txt_Money_KeyUp;
            GV_Temp.Click += GV_Temp_List_Click;
            btn_Done.Click += Btn_Done_Click;
            btn_SearchTemp.Click += Btn_SearchTemp_Click;
            btn_Copy.Click += Btn_Copy_Click;
            btn_Ad.Click += Btn_Ad_Click;
            btn_Refresh.Click += Btn_Refresh_Click;
            btn_OK.Click += Btn_OK_Click;
            btn_SaveTemp.Click += Btn_SaveTemp_Click;
            btnClose_Panel_Message.Click += BtnClose_Panel_Message_Click;
        }



        private void Frm_Setup_Rice_03_Load(object sender, EventArgs e)
        {
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            //Check_RoleForm();
            LoadComboBoxRice();
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            LoadData();
            GV_Temp.Rows.Count = 0;
            GV_Temp.Cols.Count = 0;
            GV_Temp.Clear();
            btn_Copy.Enabled = false;
            btn_Done.Enabled = false;
            DisplayData_Temp();
        }

        private void LoadComboBoxRice()
        {
            var zResult = dte_Month.Value.FirstEndMonth();
            LoadDataToToolbox.KryptonComboBox(cbo_Rice, @"SELECT A.RiceKey,(A.RiceID+'-'+A.RiceName)AS Name FROM [dbo].[HRM_Time_Rice] A WHERE RecordStatus <> 99 AND Slug = 0", "");
            Panel_Done.Visible = false;

        }

        #region[Tab-1]
        #region[Progess]
        private void Chk_Plus_Click(object sender, EventArgs e)
        {

            if (chk_Plus.Checked == true)
            {
                chk_Plus.Checked = true;
                chk_Minus.Checked = false;
            }
            else
            {
                chk_Plus.Checked = false;
                chk_Minus.Checked = true;
            }
        }
        private void Chk_Minus_Click(object sender, EventArgs e)
        {
            if (chk_Minus.Checked == true)
            {
                chk_Plus.Checked = false;
                chk_Minus.Checked = true;
            }
            else
            {
                chk_Plus.Checked = true;
                chk_Minus.Checked = false;
            }
        }
        private void Txt_Money_KeyUp(object sender, KeyEventArgs e)
        {
            double zAmount = 0;
            double.TryParse(txt_Money.Text.Trim(), out zAmount);
            txt_Money.Text = zAmount.Ton0String();
            txt_Money.Focus();
            txt_Money.SelectionStart = txt_Money.Text.Length;
        }
        private void txt_Money_KeyPress(object sender, KeyPressEventArgs e)
        {

            if ((e.KeyChar >= '0' && e.KeyChar <= '9') || (Keys)e.KeyChar == Keys.Back || e.KeyChar == 46 || e.KeyChar == 44)
            {

                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }

        }
        private void LoadData()
        {
            Time_Rice_Info zinfo = new Time_Rice_Info(_Key);

            txt_ID.Text = zinfo.RiceID;
            txt_RiceName.Text = zinfo.RiceName;
            if (zinfo.Type == 0)
            {
                chk_Plus.Checked = true;  // 0: cơm cộng , 1: cơm trừ
                chk_Minus.Checked = false;
            }
            else
            {
                chk_Plus.Checked = false;
                chk_Minus.Checked = true;
            }
            txt_Money.Text = zinfo.Money.Toe0String();
            txt_Rank.Text = zinfo.Rank.ToString();
            txt_Description.Text = zinfo.Description;

            if (zinfo.Key == 0)
            {
                int zTemp = Time_Rice_Data.AutoKeyEnd(); //Lấy số AutoKey cuối cùng
                if (zTemp == 0)
                {
                    txt_ID_Rice.Text = "PC01";
                    txt_SL.Text = "SL01";
                    txt_ST.Text = "TT01";
                }
                else
                {
                    zTemp++;
                    txt_ID_Rice.Text = "PC" + zTemp.ToString().PadLeft(2, '0');
                    txt_SL.Text = "SL" + zTemp.ToString().PadLeft(2, '0');
                    txt_ST.Text = "TT" + zTemp.ToString().PadLeft(2, '0');
                }

            }
            else
            {
                txt_ID_Rice.Text = zinfo.ID_Rice;
                txt_SL.Text = zinfo.ID_Number;
                txt_ST.Text = zinfo.ID_Money;
            }
            lbl_Created.Text = "Tạo bởi:[" + zinfo.CreatedName + "][" + zinfo.CreatedOn + "]";
            lbl_Modified.Text = "Chỉnh sửa:[" + zinfo.ModifiedName + "][" + zinfo.ModifiedOn + "]";
        }

        #endregion

        #region[Event]
        private void Btn_Save_Click(object sender, EventArgs e)
        {
            string zMessage = "";
            if (txt_ID.Text.Trim().Length == 0)
            {
                Utils.TNMessageBoxOK("Chưa nhập mã cơm!", 1);
                return;
            }
            if (txt_RiceName.Text.Trim().Length == 0)
            {
                Utils.TNMessageBoxOK("Chưa nhập tên cột cơm!", 1);
                return;
            }
            int zRank;
            if (!int.TryParse(txt_Rank.Text, out zRank))
            {
                Utils.TNMessageBoxOK("Vui lòng nhập đúng định dạng số!", 1);
                return;
            }
            if (zRank == 0)
            {
                Utils.TNMessageBoxOK("Số sắp xếp phải lớn hơn 0!", 1);
                return;
            }

            else
            {
                Time_Rice_Info zinfo = new Time_Rice_Info(_Key);

                zinfo.RiceID = txt_ID.Text.ToUpper();
                zinfo.RiceName = txt_RiceName.Text.Trim();
                if (chk_Plus.Checked == false && chk_Minus.Checked == true)
                {
                    zinfo.Type = 1;  // 0: cơm cộng , 1: cơm trừ
                    zinfo.TypeName = "Trừ tiền";
                }
                else
                {
                    zinfo.Type = 0;
                    zinfo.TypeName = "Cộng tiền";
                }
                zinfo.Rank = int.Parse(txt_Rank.Text.Trim());
                zinfo.Description = txt_Description.Text.Trim();
                zinfo.Money = txt_Money.Text.ToFloat();

                zinfo.ID_Rice = txt_ID_Rice.Text;
                zinfo.ID_Number = txt_SL.Text;
                zinfo.ID_Money = txt_ST.Text;

                zinfo.CreatedBy = SessionUser.UserLogin.Key;
                zinfo.CreatedName = SessionUser.UserLogin.EmployeeName;
                zinfo.ModifiedBy = SessionUser.UserLogin.Key;
                zinfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
                if (_Key == 0)
                {
                    zinfo.Create();
                    zMessage = TN_Message.Show(zinfo.Message);
                    if (zMessage == "")
                    {

                        Utils.TNMessageBoxOK("Cập nhật thành công!", 3);
                        DisplayData();
                        _Key = 0;
                        LoadData();
                        LoadComboBoxRice();
                    }
                    else
                    {
                        Utils.TNMessageBoxOK(zMessage, 4);
                    }

                }
                else
                {
                    zinfo.Update();
                    zMessage = TN_Message.Show(zinfo.Message);
                    if (zMessage == "")
                    {
                        Utils.TNMessageBoxOK("Cập nhật thành công!", 3);
                        DisplayData();
                        _Key = zinfo.CategoryKey;
                        LoadData();
                        LoadComboBoxRice();
                    }
                    else
                    {
                        Utils.TNMessageBoxOK(zMessage, 4);
                    }
                }

            }
        }
        private void Btn_Del_Click(object sender, EventArgs e)
        {
            string zMessage = "";
            var zResult = dte_Month.Value.FirstEndMonth();
            if (_Key == 0)
            {
                Utils.TNMessageBoxOK("Chưa chọn thông tin!", 1);
            }
            if (TempRice_Data.CountRice(_Key) > 0)
            {
                Utils.TNMessageBoxOK("Mã đang sử dụng không được xóa!", 1);
            }
            else
            {
                if (Utils.TNMessageBox("Bạn có chắc xóa thông tin này ?.", 2) == "Y")
                {
                    Time_Rice_Info zinfo = new Time_Rice_Info(_Key);

                    zinfo.ModifiedBy = SessionUser.UserLogin.Key;
                    zinfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
                    zinfo.Delete();
                    zMessage = TN_Message.Show(zinfo.Message);
                    if (zMessage == "")
                    {
                        Utils.TNMessageBoxOK("Xóa thành công!", 3);
                        DisplayData();
                        _Key = 0;
                        LoadData();
                        LoadComboBoxRice();
                    }
                    else
                    {
                        Utils.TNMessageBoxOK(zMessage, 4);
                    }

                }
            }

        }
        private void Btn_New_Click(object sender, EventArgs e)
        {
            _Key = 0;
            LoadData();
            lbl_Created.Text = "Tạo bởi:";
            lbl_Modified.Text = "Chỉnh sửa:";
            txt_ID.Focus();
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {

            DisplayData();
        }
        #endregion

        #region[ListView]
        private void DisplayData()
        {

            DataTable ztb = Time_Rice_Data.Search(txt_Search.Text.Trim());
            InitGV_Layout(ztb);
        }
        private void GVData_List_Click(object sender, EventArgs e)
        {
            if (GVData.Rows.Count > 0)
            {
                string zKey = GVData.Rows[GVData.RowSel][7].ToString();
                if (zKey != null || zKey != "")
                {
                    _Key = int.Parse(zKey);
                    LoadData();
                }
                else
                {
                    _Key = 0;
                    LoadData();
                }
            }
        }
        private void Btn_Export_Click(object sender, EventArgs e)
        {
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Danh sách cột hiển thị cơm.xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }
                    GVData.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }

        }
        void InitGV_Layout(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            int TotalRow = TableView.Rows.Count + 1;
            int ToTalCol = 8;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(TotalRow);

            //Row Header
            GVData.Rows[0][0] = "Stt";
            GVData.Rows[0][1] = "Mã cơm";
            GVData.Rows[0][2] = "Tên cơm";
            GVData.Rows[0][3] = "Loại cơm";
            GVData.Rows[0][4] = "Số tiền";
            GVData.Rows[0][5] = "Sắp xếp";
            GVData.Rows[0][6] = "Ghi chú";
            GVData.Rows[0][7] = ""; // ẩn cột này

            //Style         
            //GVData.AllowFreezing = AllowFreezingEnum.Both;
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.Custom;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.LeftCenter;
            GVData.Styles.Normal.WordWrap = true;

            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                DataRow rData = TableView.Rows[rIndex];

                GVData.Rows[rIndex + 1][0] = rIndex + 1;
                GVData.Rows[rIndex + 1][1] = rData[1];
                GVData.Rows[rIndex + 1][2] = rData[2];
                GVData.Rows[rIndex + 1][3] = rData[3];
                GVData.Rows[rIndex + 1][4] = rData[4].Toe0String();
                GVData.Rows[rIndex + 1][5] = rData[5];
                GVData.Rows[rIndex + 1][6] = rData[6];
                GVData.Rows[rIndex + 1][7] = rData[0];

            }

            //Freeze Row and Column                              
            GVData.Rows.Fixed = 1;
            GVData.Rows[0].TextAlign = TextAlignEnum.CenterCenter;
            GVData.Rows[0].Height = 50;

            GVData.Cols[0].Width = 30;
            GVData.Cols[1].Width = 80;
            GVData.Cols[2].Width = 200;
            GVData.Cols[3].Width = 70;
            GVData.Cols[4].Width = 70;
            GVData.Cols[5].TextAlign = TextAlignEnum.RightCenter;
            GVData.Cols[5].Width = 80;
            GVData.Cols[6].Width = 80;
            GVData.Cols[7].Visible = false;
        }
        #endregion

        #endregion

        #region[Tab-2]
        private void DisplayData_Temp()
        {
            var zResult = dte_Month.Value.FirstEndMonth();
            _FromDate = zResult.Item1;
            _ToDate = zResult.Item2;
            DataTable ztb = TempRice_Data.List(_FromDate, _ToDate);
            InitGV_Layout_Temp(ztb);
            lbl_Title.Text = "Danh sách tháng " + dte_Month.Value.ToString("MM/yyyy");
            if (GV_Temp.Rows.Count > 1)
            {
                btn_Copy.Enabled = true;
                btn_Done.Enabled = true;
            }
            else
            {
                btn_Copy.Enabled = false;
                btn_Done.Enabled = false;
            }
        }
        private void GV_Temp_List_Click(object sender, EventArgs e)
        {
            if (GV_Temp.Rows.Count > 1)
            {
                string zKey = GV_Temp.Rows[GV_Temp.RowSel][6].ToString();
                string zRank = GV_Temp.Rows[GV_Temp.RowSel][5].ToString();
                if (zKey != null || zKey != "")
                {
                    _KeyTemp = int.Parse(zKey);
                }
                //else
                //{
                //    _KeyTemp = 0;
                //    LoadData_Temp(zRank.ToInt());
                //}
            }
        }
        void InitGV_Layout_Temp(DataTable TableView)
        {
            GV_Temp.Rows.Count = 0;
            GV_Temp.Cols.Count = 0;
            GV_Temp.Clear();
            int TotalRow = TableView.Rows.Count + 1;
            int ToTalCol = 7;

            GV_Temp.Cols.Add(ToTalCol);
            GV_Temp.Rows.Add(TotalRow);

            //Row Header
            GV_Temp.Rows[0][0] = "Stt";
            GV_Temp.Rows[0][1] = "Bảng";
            GV_Temp.Rows[0][2] = "Loại cơm";
            GV_Temp.Rows[0][3] = "Tên cột";
            GV_Temp.Rows[0][4] = "Phụ đề";
            GV_Temp.Rows[0][5] = "Thứ tự hiển thị";
            GV_Temp.Rows[0][6] = ""; // ẩn cột này

            //Style         
            //GVData.AllowFreezing = AllowFreezingEnum.Both;
            GV_Temp.AllowResizing = AllowResizingEnum.Both;
            GV_Temp.AllowMerging = AllowMergingEnum.FixedOnly;
            GV_Temp.SelectionMode = SelectionModeEnum.Row;
            GV_Temp.VisualStyle = VisualStyle.Office2010Blue;
            GV_Temp.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GV_Temp.Styles.Normal.TextAlign = TextAlignEnum.LeftCenter;
            GV_Temp.Styles.Normal.WordWrap = true;

            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                DataRow rData = TableView.Rows[rIndex];

                GV_Temp.Rows[rIndex + 1][0] = rIndex + 1;
                GV_Temp.Rows[rIndex + 1][1] = rData[1];
                GV_Temp.Rows[rIndex + 1][2] = rData[2];
                GV_Temp.Rows[rIndex + 1][3] = rData[3];
                GV_Temp.Rows[rIndex + 1][4] = rData[4];
                GV_Temp.Rows[rIndex + 1][5] = rData[5];
                GV_Temp.Rows[rIndex + 1][6] = rData[0];
                if (rData[6].ToInt() == 0)
                {
                    GV_Temp.Rows[rIndex + 1].StyleNew.BackColor = Color.LightGreen;
                }
                else
                {

                }
            }

            //Freeze Row and Column                              
            GV_Temp.Rows.Fixed = 1;
            GV_Temp.Rows[0].TextAlign = TextAlignEnum.CenterCenter;
            GV_Temp.Rows[0].Height = 50;

            GV_Temp.Cols[0].Width = 30;
            GV_Temp.Cols[1].Width = 200;
            GV_Temp.Cols[2].Width = 200;
            GV_Temp.Cols[3].Width = 200;
            GV_Temp.Cols[4].Width = 200;

            GV_Temp.Cols[5].TextAlign = TextAlignEnum.RightCenter;
            GV_Temp.Cols[6].Visible = false;

            GV_Temp.Cols[1].AllowMerging = true;
            GV_Temp.Cols[2].AllowMerging = true;
            GV_Temp.Cols.Fixed = 2;
        }
        private void Btn_DelTemp_Click(object sender, EventArgs e)
        {

            //int zKey = cbo_RiceYes.SelectedValue.ToInt();
            //if (zKey == 0)
            //{
            //    Utils.TNMessageBoxOK("Chưa chọn mục cơm!", 1);
            //    return;
            //}
            //else
            //{
            //    if (Utils.TNMessageBox("Bạn có chắc xóa thông tin này ?.", 2) == "Y")
            //    {
            //        string zMessage = DeleteTemp(zKey);
            //        if (zMessage == "")
            //        {
            //            Utils.TNMessageBoxOK("Xóa thành công!", 3);
            //            DisplayData_Temp();
            //            LoadComboBoxRice();
            //        }
            //        else
            //        {
            //            Utils.TNMessageBoxOK(zMessage, 4);
            //        }

            //    }
            //}

        }
        private string DeleteTemp(int RiceKey)
        {
            string zMessage = "";
            DataTable zTb = TempRice_Data.List_FromRiceKey(RiceKey, _FromDate, _ToDate);
            if (zTb.Rows.Count > 0)
            {

                for (int i = 0; i < zTb.Rows.Count; i++)
                {
                    zMessage += TempRice_Data.DeleteFromRiceKey(zTb.Rows[i]["AutoKey"].ToInt(), zTb.Rows[i]["Report"].ToInt(), _FromDate, _ToDate);
                }
            }
            return zMessage;
        }
        private void Btn_SaveTemp_Click(object sender, EventArgs e)
        {
            if (SessionUser.Date_Lock >= dte_Date.Value)
            {
                Utils.TNMessageBoxOK("Thời gian này đã khóa sổ.!", 2);
                return;
            }
            if (TempRice_Data.KiemTraDuLieuThangTruoc(dte_Date.Value) == 0)
            {
                Utils.TNMessageBoxOK("Dữ liệu các tháng không liên tục.Vui lòng kiểm tra lại!", 2);
                return;
            }
            var zResult = dte_Date.Value.FirstEndMonth();
            string zMessage = "";
            zMessage += TempRice_Data.DeleteAll(zResult.Item1, zResult.Item2); //  xóa bảng hiển thị cơm
            var zOld = dte_Date.Value.AddMonths(-1).FirstEndMonth();
            var zNew = dte_Date.Value.FirstEndMonth(); // nếu chưa có bảng mã, thì copy tháng trước sang
            zMessage += TempRice_Data.CoppyCodeReport(zOld.Item1, zOld.Item2, zNew.Item1, zNew.Item2);
            zMessage += TempRice_Data.CoppyCodeReportOffice(zOld.Item1, zOld.Item2, zNew.Item1, zNew.Item2);
            zMessage += TempRice_Data.CoppyCodeReportSupport(zOld.Item1, zOld.Item2, zNew.Item1, zNew.Item2);
            zMessage += TempRice_Data.CoppyCodeReportWorker(zOld.Item1, zOld.Item2, zNew.Item1, zNew.Item2);
            if (_Chuoi.Length > 0)
            {
                string[] s = _Chuoi.Split(',');
                for (int i = 0; i < s.Length; i++)
                {
                    Time_Rice_Info zinfo = new Time_Rice_Info(s[i].ToInt());
                    if (zinfo.Type == 0)
                        Time_Rice_Data.Add_RicePlus(zinfo.Key, zinfo.RiceID, zinfo.RiceName, zinfo.ID_Rice, zinfo.ID_Number, zinfo.ID_Money, zResult.Item1, zResult.Item2);
                    else
                        Time_Rice_Data.Add_RiceMinus(zinfo.Key, zinfo.RiceID, zinfo.RiceName, zinfo.ID_Rice, zinfo.ID_Number, zinfo.ID_Money, zResult.Item1, zResult.Item2);
                }
                if (zMessage == "")
                {
                    Utils.TNMessageBoxOK("Cập nhật thành công!", 3);
                    dte_Month.Value = zResult.Item1;
                    DisplayData_Temp();
                    Btn_Refresh_Click(null, null);
                }
                else
                {
                    Utils.TNMessageBoxOK(zMessage, 4);
                }
            }
            else
            {
                Utils.TNMessageBoxOK("Vui lòng chọn danh sách mã cơm!", 2);
                return;
            }

        }
        private void Btn_Done_Click(object sender, EventArgs e)
        {
            if (SessionUser.Date_Lock >= dte_Month.Value)
            {
                Utils.TNMessageBoxOK("Thời gian này đã khóa sổ.!", 2);
                return;
            }
            string zMessage = "";
            zMessage += MaIDCom();
            zMessage += SoPhanCom();
            zMessage += ComCong_Worker();
            zMessage += ComTru_Worker();
            zMessage += ComCong_Support();
            zMessage += ComTru_Support();
            zMessage += ComCong_Office();
            zMessage += ComTru_Office();
            if (zMessage == "")
            {
                Utils.TNMessageBoxOK("Áp dụng thành công!", 3);
            }
            else
            {
                Utils.TNMessageBoxOK(zMessage, 4);
            }
        }
        private void Btn_SearchTemp_Click(object sender, EventArgs e)
        {
            DisplayData_Temp();
            LoadComboBoxRice();
        }
        private void Btn_Copy_Click(object sender, EventArgs e)
        {
            Panel_Done.Visible = true;
        }
        private void BtnClose_Panel_Message_Click(object sender, EventArgs e)
        {
            Panel_Done.Visible = false;
        }
        private void Btn_OK_Click(object sender, EventArgs e)
        {
            if (dte_Month.Value == dte_Coppy.Value)
            {
                Utils.TNMessageBoxOK("Thời gian trùng nhau", 1);
                return;
            }

            if (SessionUser.Date_Lock >= dte_Coppy.Value)
            {
                Utils.TNMessageBoxOK("Thời gian này đã khóa sổ.!", 2);
                return;
            }
            if(TempRice_Data.KiemTraDuLieuThangTruoc(dte_Coppy.Value)==0)
            {
                Utils.TNMessageBoxOK("Dữ liệu các tháng không liên tục.Vui lòng kiểm tra lại!", 2);
                return;
            }    
            if (Utils.TNMessageBox("Bạn có chắc sao chép dữ liệu sang tháng mới ?.", 2) == "Y")
            {
                var zOld = dte_Month.Value.FirstEndMonth();
                var zNew = dte_Coppy.Value.FirstEndMonth();
                string zMessage = "";
                zMessage += TempRice_Data.CoppyTempRice(zOld.Item1, zOld.Item2, zNew.Item1, zNew.Item2);
                zMessage += TempRice_Data.CoppyCodeReport(zOld.Item1, zOld.Item2, zNew.Item1, zNew.Item2);
                zMessage += TempRice_Data.CoppyCodeReportOffice(zOld.Item1, zOld.Item2, zNew.Item1, zNew.Item2);
                zMessage += TempRice_Data.CoppyCodeReportSupport(zOld.Item1, zOld.Item2, zNew.Item1, zNew.Item2);
                zMessage += TempRice_Data.CoppyCodeReportWorker(zOld.Item1, zOld.Item2, zNew.Item1, zNew.Item2);

                if (zMessage == "")
                {
                    Utils.TNMessageBoxOK("Sao chép thành công!", 3);
                    dte_Month.Value = dte_Coppy.Value; //load lại dữ liệu vừa tạo
                    DisplayData_Temp();
                    _KeyTemp = 0;
                    Panel_Done.Visible = false;
                }
                else
                {
                    Utils.TNMessageBoxOK(zMessage, 4);
                }

            }
        }

        private int _Rank = 0;
        string _Chuoi = "";
        private void Btn_Ad_Click(object sender, EventArgs e)
        {
            string[] s = _Chuoi.Split(',');

            bool KT = true;
            for (int i = 0; i < s.Length; i++)
            {
                if (cbo_Rice.SelectedValue.ToString() == s[i].ToString())
                {
                    KT = false;
                    break;
                }
            }
            if (KT == true)
            {
                _Rank++;
                LB_List.Items.Add(" STT:" + _Rank.ToString() + " Mã cơm: " + cbo_Rice.Text.ToString());
                if (_Chuoi == "")
                    _Chuoi += cbo_Rice.SelectedValue.ToString();
                else
                    _Chuoi += "," + cbo_Rice.SelectedValue.ToString();
            }
        }
        private void Btn_Refresh_Click(object sender, EventArgs e)
        {
            LB_List.Items.Clear();
            _Chuoi = "";
            _Rank = 0;
        }

        private string MaIDCom()
        {
            int zBĐ = TempRice_Data.ViTriDauMaIDCom(_FromDate, _ToDate);
            string zMessage = "";
            DataTable zBangXoa = TempRice_Data.DanhSachMaIDCom_Cu(_FromDate, _ToDate);
            if (zBangXoa.Rows.Count > 0)
            {
                for (int i = 0; i < zBangXoa.Rows.Count; i++)
                {
                    CodeReport_Info zinfo = new CodeReport_Info(zBangXoa.Rows[i]["AutoKey"].ToInt());
                    zinfo.Delete();
                    if (zinfo.Message != "")
                    {
                        zMessage += zinfo.Message;
                    }
                }
            }
            DataTable zBangMoi = TempRice_Data.DanhSachMaIDCom_Moi(_FromDate, _ToDate);
            if (zBangMoi.Rows.Count > 0)
            {
                for (int i = 0; i < zBangMoi.Rows.Count; i++)
                {
                    zBĐ++;
                    DataRow r = zBangMoi.Rows[i];
                    CodeReport_Info zinfo = new CodeReport_Info();
                    zinfo.DateWrite = _FromDate;
                    zinfo.Type = 2;
                    zinfo.TypeName = "Mã cơm";
                    zinfo.ID = r["ID"].ToString();
                    zinfo.Name = r["Name"].ToString();
                    zinfo.CategoryKey = r["CategoryKey"].ToInt();
                    zinfo.CategoryName = r["CategoryName"].ToString();
                    zinfo.Rank = zBĐ;
                    zinfo.Create();
                    if (zinfo.Message != "")
                    {
                        zMessage += zinfo.Message;
                    }
                }
            }
            return zMessage;
        }
        private string SoPhanCom()
        {
            int zBĐ = TempRice_Data.ViTriDauSoPhanCom(_FromDate, _ToDate);
            string zMessage = "";
            DataTable zBangXoa = TempRice_Data.DanhSachSoPhanCom_Cu(_FromDate, _ToDate);
            if (zBangXoa.Rows.Count > 0)
            {
                for (int i = 0; i < zBangXoa.Rows.Count; i++)
                {
                    CodeReport_Info zinfo = new CodeReport_Info(zBangXoa.Rows[i]["AutoKey"].ToInt());
                    zinfo.Delete();
                    if (zinfo.Message != "")
                    {
                        zMessage += zinfo.Message;
                    }
                }
            }
            DataTable zBangMoi = TempRice_Data.DanhSachSoPhanCom_Moi(_FromDate, _ToDate);
            if (zBangMoi.Rows.Count > 0)
            {
                for (int i = 0; i < zBangMoi.Rows.Count; i++)
                {
                    zBĐ++;
                    DataRow r = zBangMoi.Rows[i];
                    CodeReport_Info zinfo = new CodeReport_Info();
                    zinfo.DateWrite = _FromDate;
                    zinfo.ID = r["ID"].ToString();
                    zinfo.Name = r["Name"].ToString();
                    zinfo.Type = 3;//
                    zinfo.TypeName = "Số phần cơm";
                    zinfo.CategoryKey = r["CategoryKey"].ToInt();
                    zinfo.CategoryName = r["CategoryName"].ToString();
                    zinfo.Rank = zBĐ;
                    zinfo.Create();
                    if (zinfo.Message != "")
                    {
                        zMessage += zinfo.Message;
                    }
                }
            }
            return zMessage;
        }
        private string ComCong_Worker()
        {
            int zBĐ = TempRice_Data.ViTriDauComCongWorker(_FromDate, _ToDate);
            string zMessage = "";
            DataTable zBangXoa = TempRice_Data.DanhSach_ComCongWorker_Cu(_FromDate, _ToDate);
            if (zBangXoa.Rows.Count > 0)
            {
                for (int i = 0; i < zBangXoa.Rows.Count; i++)
                {
                    CodeReportWorker_Info zinfo = new CodeReportWorker_Info(zBangXoa.Rows[i]["AutoKey"].ToInt());
                    zinfo.Delete();
                    if (zinfo.Message != "")
                    {
                        zMessage += zinfo.Message;
                    }
                }
            }
            DataTable zBangMoi = TempRice_Data.DanhSach_ComCongWorker_Moi(_FromDate, _ToDate);
            if (zBangMoi.Rows.Count > 0)
            {
                for (int i = 0; i < zBangMoi.Rows.Count; i++)
                {
                    zBĐ++;
                    DataRow r = zBangMoi.Rows[i];
                    CodeReportWorker_Info zinfo = new CodeReportWorker_Info();
                    zinfo.DateWrite = _FromDate;
                    zinfo.ID = r["ID"].ToString();
                    zinfo.Name = r["Name"].ToString().ToUpper();
                    zinfo.Type = 1;
                    zinfo.TypeName = r["SubName"].ToString().ToUpper();
                    zinfo.CategoryKey = r["CategoryKey"].ToInt();
                    zinfo.CategoryName = r["CategoryName"].ToString();
                    zinfo.Publish = 1;
                    zinfo.Publish_Close = 1;
                    zinfo.Rank = zBĐ;
                    zinfo.Create();
                    if (zinfo.Message != "")
                    {
                        zMessage += zinfo.Message;
                    }
                }
            }
            return zMessage;
        }
        private string ComTru_Worker()
        {
            int zBĐ = TempRice_Data.ViTriDauComTruWorker(_FromDate, _ToDate);
            string zMessage = "";
            DataTable zBangXoa = TempRice_Data.DanhSach_ComTruWorker_Cu(_FromDate, _ToDate);
            if (zBangXoa.Rows.Count > 0)
            {
                for (int i = 0; i < zBangXoa.Rows.Count; i++)
                {
                    CodeReportWorker_Info zinfo = new CodeReportWorker_Info(zBangXoa.Rows[i]["AutoKey"].ToInt());
                    zinfo.Delete();
                    if (zinfo.Message != "")
                    {
                        zMessage += zinfo.Message;
                    }
                }
            }
            DataTable zBangMoi = TempRice_Data.DanhSach_ComTruWorker_Moi(_FromDate, _ToDate);
            if (zBangMoi.Rows.Count > 0)
            {
                for (int i = 0; i < zBangMoi.Rows.Count; i++)
                {
                    zBĐ++;
                    DataRow r = zBangMoi.Rows[i];
                    CodeReportWorker_Info zinfo = new CodeReportWorker_Info();
                    zinfo.DateWrite = _FromDate;
                    zinfo.ID = r["ID"].ToString();
                    zinfo.Name = r["Name"].ToString().ToUpper();
                    zinfo.Type = 1;
                    zinfo.TypeName = r["SubName"].ToString().ToUpper();
                    zinfo.CategoryKey = r["CategoryKey"].ToInt();
                    zinfo.CategoryName = r["CategoryName"].ToString();
                    zinfo.Publish = 1;
                    zinfo.Publish_Close = 1;
                    zinfo.Rank = zBĐ;
                    zinfo.Create();
                    if (zinfo.Message != "")
                    {
                        zMessage += zinfo.Message;
                    }
                }
            }
            return zMessage;
        }
        private string ComCong_Support()
        {
            int zBĐ = TempRice_Data.ViTriDauComCongSupport(_FromDate, _ToDate);
            string zMessage = "";
            DataTable zBangXoa = TempRice_Data.DanhSach_ComCongSupport_Cu(_FromDate, _ToDate);
            if (zBangXoa.Rows.Count > 0)
            {
                for (int i = 0; i < zBangXoa.Rows.Count; i++)
                {
                    CodeReportSupport_Info zinfo = new CodeReportSupport_Info(zBangXoa.Rows[i]["AutoKey"].ToInt());
                    zinfo.Delete();
                    if (zinfo.Message != "")
                    {
                        zMessage += zinfo.Message;
                    }
                }
            }
            DataTable zBangMoi = TempRice_Data.DanhSach_ComCongSupport_Moi(_FromDate, _ToDate);
            if (zBangMoi.Rows.Count > 0)
            {
                for (int i = 0; i < zBangMoi.Rows.Count; i++)
                {
                    zBĐ++;
                    DataRow r = zBangMoi.Rows[i];
                    CodeReportSupport_Info zinfo = new CodeReportSupport_Info();
                    zinfo.DateWrite = _FromDate;
                    zinfo.ID = r["ID"].ToString();
                    zinfo.Name = r["Name"].ToString().ToUpper();
                    zinfo.Type = 1;
                    zinfo.TypeName = r["SubName"].ToString().ToUpper();
                    zinfo.CategoryKey = r["CategoryKey"].ToInt();
                    zinfo.CategoryName = r["CategoryName"].ToString();
                    zinfo.Publish = 1;
                    zinfo.Publish_Close = 1;
                    zinfo.Rank = zBĐ;
                    zinfo.Create();
                    if (zinfo.Message != "")
                    {
                        zMessage += zinfo.Message;
                    }
                }
            }
            return zMessage;
        }
        private string ComTru_Support()
        {
            int zBĐ = TempRice_Data.ViTriDauComTruSupport(_FromDate, _ToDate);
            string zMessage = "";
            DataTable zBangXoa = TempRice_Data.DanhSach_ComTruSupport_Cu(_FromDate, _ToDate);
            if (zBangXoa.Rows.Count > 0)
            {
                for (int i = 0; i < zBangXoa.Rows.Count; i++)
                {
                    CodeReportSupport_Info zinfo = new CodeReportSupport_Info(zBangXoa.Rows[i]["AutoKey"].ToInt());
                    zinfo.Delete();
                    if (zinfo.Message != "")
                    {
                        zMessage += zinfo.Message;
                    }
                }
            }
            DataTable zBangMoi = TempRice_Data.DanhSach_ComTruSupport_Moi(_FromDate, _ToDate);
            if (zBangMoi.Rows.Count > 0)
            {
                for (int i = 0; i < zBangMoi.Rows.Count; i++)
                {
                    zBĐ++;
                    DataRow r = zBangMoi.Rows[i];
                    CodeReportSupport_Info zinfo = new CodeReportSupport_Info();
                    zinfo.DateWrite = _FromDate;
                    zinfo.ID = r["ID"].ToString();
                    zinfo.Name = r["Name"].ToString().ToUpper();
                    zinfo.Type = 1;
                    zinfo.TypeName = r["SubName"].ToString().ToUpper();
                    zinfo.CategoryKey = r["CategoryKey"].ToInt();
                    zinfo.CategoryName = r["CategoryName"].ToString();
                    zinfo.Publish = 1;
                    zinfo.Publish_Close = 1;
                    zinfo.Rank = zBĐ;
                    zinfo.Create();
                    if (zinfo.Message != "")
                    {
                        zMessage += zinfo.Message;
                    }
                }
            }
            return zMessage;
        }
        private string ComCong_Office()
        {
            int zBĐ = TempRice_Data.ViTriDauComCongOffice(_FromDate, _ToDate);
            string zMessage = "";
            DataTable zBangXoa = TempRice_Data.DanhSach_ComCongOffice_Cu(_FromDate, _ToDate);
            if (zBangXoa.Rows.Count > 0)
            {
                for (int i = 0; i < zBangXoa.Rows.Count; i++)
                {
                    CodeReportOffice_Info zinfo = new CodeReportOffice_Info(zBangXoa.Rows[i]["AutoKey"].ToInt());
                    zinfo.Delete();
                    if (zinfo.Message != "")
                    {
                        zMessage += zinfo.Message;
                    }
                }
            }
            DataTable zBangMoi = TempRice_Data.DanhSach_ComCongOffice_Moi(_FromDate, _ToDate);
            if (zBangMoi.Rows.Count > 0)
            {
                for (int i = 0; i < zBangMoi.Rows.Count; i++)
                {
                    zBĐ++;
                    DataRow r = zBangMoi.Rows[i];
                    CodeReportOffice_Info zinfo = new CodeReportOffice_Info();
                    zinfo.DateWrite = _FromDate;
                    zinfo.ID = r["ID"].ToString();
                    zinfo.Name = r["Name"].ToString().ToUpper();
                    zinfo.Type = 1;
                    zinfo.TypeName = r["SubName"].ToString().ToUpper();
                    zinfo.CategoryKey = r["CategoryKey"].ToInt();
                    zinfo.CategoryName = r["CategoryName"].ToString();
                    zinfo.Publish = 1;
                    zinfo.Publish_Close = 1;
                    zinfo.Rank = zBĐ;
                    zinfo.Create();
                    if (zinfo.Message != "")
                    {
                        zMessage += zinfo.Message;
                    }
                }
            }
            return zMessage;
        }
        private string ComTru_Office()
        {
            int zBĐ = TempRice_Data.ViTriDauComTruOffice(_FromDate, _ToDate);
            string zMessage = "";
            DataTable zBangXoa = TempRice_Data.DanhSach_ComTruOffice_Cu(_FromDate, _ToDate);
            if (zBangXoa.Rows.Count > 0)
            {
                for (int i = 0; i < zBangXoa.Rows.Count; i++)
                {
                    CodeReportOffice_Info zinfo = new CodeReportOffice_Info(zBangXoa.Rows[i]["AutoKey"].ToInt());
                    zinfo.Delete();
                    if (zinfo.Message != "")
                    {
                        zMessage += zinfo.Message;
                    }
                }
            }
            DataTable zBangMoi = TempRice_Data.DanhSach_ComTruOffice_Moi(_FromDate, _ToDate);
            if (zBangMoi.Rows.Count > 0)
            {
                for (int i = 0; i < zBangMoi.Rows.Count; i++)
                {
                    zBĐ++;
                    DataRow r = zBangMoi.Rows[i];
                    CodeReportOffice_Info zinfo = new CodeReportOffice_Info();
                    zinfo.DateWrite = _FromDate;
                    zinfo.ID = r["ID"].ToString();
                    zinfo.Name = r["Name"].ToString().ToUpper();
                    zinfo.Type = 1;
                    zinfo.TypeName = r["SubName"].ToString().ToUpper();
                    zinfo.CategoryKey = r["CategoryKey"].ToInt();
                    zinfo.CategoryName = r["CategoryName"].ToString();
                    zinfo.Publish = 1;
                    zinfo.Publish_Close = 1;
                    zinfo.Rank = zBĐ;
                    zinfo.Create();
                    if (zinfo.Message != "")
                    {
                        zMessage += zinfo.Message;
                    }
                }
            }
            return zMessage;
        }
        #endregion

       


        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                //this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
                btn_Export.Enabled = false;
            }
            if (Role.RoleAdd == 0 && Role.RoleEdit == 0)
            {
                // btn_New.Enabled = false;
                btn_Save.Enabled = false;
            }
            if (Role.RoleAdd == 0 && Role.RoleEdit == 1)
            {
                //btn_New.Enabled = false;
                btn_Save.Enabled = true;
            }
            if (Role.RoleDel == 0)
            {
                btn_Del.Enabled = false;
            }
        }
        #endregion
    }
}
