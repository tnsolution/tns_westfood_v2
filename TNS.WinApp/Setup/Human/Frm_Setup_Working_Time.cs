﻿using C1.Win.C1FlexGrid;
using System;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Setup_Working_Time : Form
    {
        #region[Private]
        private int _CodeKey = 0;
        #endregion
        public Frm_Setup_Working_Time()
        {
            InitializeComponent();

            btnClose.Click += btnClose_Click;
            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            txt_Value.KeyPress += txt_Value_KeyPress;
            txt_Value.Leave += Txt_Value_Leave;

            btn_Save.Click += Btn_Save_Click;
            btn_New.Click += Btn_New_Click;
            btn_Del.Click += Btn_Del_Click;
            btn_Search.Click += Btn_Search_Click;
            GVData.Click += GVData_List_Click;
            btn_Export.Click += Btn_Export_Click;
        }

       

        
       
        DataTable ztbCategory = new DataTable();
        DataTable ztbSlug = new DataTable();
        private void Frm_Time_Defines_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            Check_RoleForm();

            LoadTableCategory();
            LoadTableSlug();
            LoadDataToToolbox.ComboBoxData(cbo_Category.ComboBox, ztbCategory, false, 0, 1);
            LoadDataToToolbox.ComboBoxData(cbo_Slug.ComboBox, ztbSlug, false, 0, 1);
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            LoadData();
        }

        #region[Progess]

        void LoadTableCategory()
        {
            ztbCategory = new DataTable();
            ztbCategory.Columns.Add("Value");
            ztbCategory.Columns.Add("Name");
            ztbCategory.Rows.Add("0", "--Chọn--");
            ztbCategory.Rows.Add("1", "Đi làm");
            ztbCategory.Rows.Add("2", "Được nghỉ");

        }
        void LoadTableSlug()
        {
            ztbSlug = new DataTable();
            ztbSlug.Columns.Add("Value");
            ztbSlug.Columns.Add("Name");
            ztbSlug.Rows.Add("0", "--Chọn--");
            ztbSlug.Rows.Add("1", "Được tính chuyên cần");
            ztbSlug.Rows.Add("2", "Không được tính chuyên cần");

        }
        private void LoadData()
        {

            Time_Defines_Info zinfo = new Time_Defines_Info(_CodeKey);
            txt_CodeID.Text = zinfo.CodeID;
            txt_CodeName.Text = zinfo.CodeName;
            txt_Value.Text = zinfo.Value.Ton1String();
            txt_Rank.Text = zinfo.Rank.ToString();
            txt_Description.Text = zinfo.Description;
            cbo_Category.SelectedValue = zinfo.CategoryKey.ToString();
            cbo_Slug.SelectedValue = zinfo.Slug.ToString();
            lbl_Created.Text = "Tạo bởi:[" + zinfo.CreatedName + "][" + zinfo.CreatedOn + "]";
            lbl_Modified.Text = "Chỉnh sửa:[" + zinfo.ModifiedName + "][" + zinfo.ModifiedOn + "]";
        }

        #endregion

        #region[Event]
        private void Btn_Save_Click(object sender, EventArgs e)
        {
            if (txt_CodeID.Text.Trim().Length == 0)
            {
               Utils.TNMessageBoxOK("Chưa nhập mã!", 1);
                return;
            }
            if (txt_CodeName.Text.Trim().Length == 0)
            {
                Utils.TNMessageBoxOK("Chưa nhập tên!", 1);
                return;
            }
            int zRank;
            if (!int.TryParse(txt_Rank.Text, out zRank))
            {
                Utils.TNMessageBoxOK("Vui lòng nhập sắp xếp đúng định dạng số!",1);
                return;
            }
            float zValue;
            if (!float.TryParse(txt_Value.Text, out zValue))
            {
                Utils.TNMessageBoxOK("Vui lòng nhập số công đúng định dạng số!", 1);
                return;
            }
            if (Time_Defines_Data.CountID(txt_CodeID.Text.Trim().ToUpper()) > 0 && _CodeKey == 0)
            {
                Utils.TNMessageBoxOK("Mã chấm công đã tồn tại !.", 1);
                return;
            }
            if (cbo_Category.SelectedIndex == 0)
            {
                Utils.TNMessageBoxOK("Chưa chọn loại áp dụng!",1);
                return;
            }
             if(cbo_Slug.SelectedIndex == 0)
            {
                Utils.TNMessageBoxOK("Chưa chọn loại!", 1);
                return;
            }
            else
            {
                string zMessage = "";
                Time_Defines_Info zinfo = new Time_Defines_Info(_CodeKey);
                zinfo.CodeID = txt_CodeID.Text.Trim().ToUpper();
                zinfo.CodeName = txt_CodeName.Text.Trim();
                zValue = 0;
                if(float.TryParse(txt_Value.Text.Trim(),out zValue))
                {

                }
                zinfo.Value = zValue;
                zinfo.Rank = int.Parse(txt_Rank.Text.Trim());
                zinfo.CategoryKey = int.Parse(cbo_Category.SelectedValue.ToString());
                zinfo.Slug = int.Parse(cbo_Slug.SelectedValue.ToString());
                zinfo.Description = txt_Description.Text.Trim();

                zinfo.CreatedBy = SessionUser.UserLogin.Key;
                zinfo.CreatedName = SessionUser.UserLogin.EmployeeName;
                zinfo.ModifiedBy = SessionUser.UserLogin.Key;
                zinfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
                zinfo.Save();
                zMessage = TN_Message.Show(zinfo.Message);
                if (zMessage =="")
                {
                    Utils.TNMessageBoxOK("Cập nhật thành công!",3);
                    DisplayData();
                    _CodeKey = zinfo.Key;
                    LoadData();
                }
                else
                {
                    Utils.TNMessageBoxOK(zMessage,4);
                }
            }


        }
        private void Btn_Del_Click(object sender, EventArgs e)
        {
            string zMessage = "";
            if (_CodeKey == 0)
            {
                Utils.TNMessageBoxOK("Chưa chọn thông tin!", 1);
            }
            else
            {
                if  (Utils.TNMessageBox("Bạn có chắc xóa thông tin này ?.",2) == "Y")
                {
                    Time_Defines_Info zinfo = new Time_Defines_Info(_CodeKey);

                    zinfo.ModifiedBy = SessionUser.UserLogin.Key;
                    zinfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
                    zinfo.Delete();
                    zMessage = TN_Message.Show(zinfo.Message);
                    if (zMessage.Length == 0)
                    {
                        Utils.TNMessageBoxOK("Đã xóa thành công!",3);
                        DisplayData();
                        _CodeKey = 0;
                        LoadData();
                    }
                    else
                    {
                        Utils.TNMessageBoxOK(zMessage, 4);
                    }

                }
            }

        }
        private void Btn_New_Click(object sender, EventArgs e)
        {
            _CodeKey = 0;
            LoadData();
            lbl_Created.Text = "Tạo bởi:";
            lbl_Modified.Text = "Chỉnh sửa:";
            txt_CodeID.Focus();
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {

            DisplayData();
        }
        #endregion

        #region[ListView]
        private void DisplayData()
        {

            DataTable ztb = Time_Defines_Data.Search(txt_Search.Text.Trim());
            InitGV_Layout(ztb);
        }
        private void GVData_List_Click(object sender, EventArgs e)
        {
            if (GVData.Rows.Count > 0)
            {
                string zKey = GVData.Rows[GVData.RowSel][7].ToString();
                if (zKey != null || zKey != "")
                {
                    _CodeKey = int.Parse(zKey);
                    LoadData();
                }
                else
                {
                    _CodeKey = 0;
                    LoadData();
                }
            }
        }
        private void Btn_Export_Click(object sender, EventArgs e)
        {
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Danh sách mã cơm.xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }
                    GVData.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }

        }
        void InitGV_Layout(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            int TotalRow = TableView.Rows.Count + 1;
            int ToTalCol = 8;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(TotalRow);

            //Row Header
            GVData.Rows[0][0] = "Stt";
            GVData.Rows[0][1] = "Mã";
            GVData.Rows[0][2] = "Tên";
            GVData.Rows[0][3] = "Giá trị";
            GVData.Rows[0][4] = "Loại";
            GVData.Rows[0][5] = "Tình trạng";
            GVData.Rows[0][6] = "Ghi chú";
            GVData.Rows[0][7] = ""; // ẩn cột này

            //Style         
            //GVData.AllowFreezing = AllowFreezingEnum.Both;
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.Custom;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.LeftCenter;
            GVData.Styles.Normal.WordWrap = true;

            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                DataRow rData = TableView.Rows[rIndex];

                GVData.Rows[rIndex + 1][0] = rIndex + 1;
                GVData.Rows[rIndex + 1][1] = rData[1];
                GVData.Rows[rIndex + 1][2] = rData[2];
                GVData.Rows[rIndex + 1][3] = rData[3];
                if(rData[4].ToString()=="1")
                {
                    GVData.Rows[rIndex + 1][4] = "Đi làm";
                }
                else
                {
                    GVData.Rows[rIndex + 1][4] = "Được nghỉ";
                }
                if (rData[5].ToString() == "1")
                {
                    GVData.Rows[rIndex + 1][5] = "Được tính chuyển cần";
                }
                else
                {
                    GVData.Rows[rIndex + 1][5] = "Không tính chuyên cần";
                }

                GVData.Rows[rIndex + 1][6] = rData[6];
                GVData.Rows[rIndex + 1][7] = rData[0];

            }



            //Freeze Row and Column                              
            GVData.Rows.Fixed = 1;
            GVData.Rows[0].TextAlign = TextAlignEnum.CenterCenter;
            GVData.Rows[0].Height = 50;

            GVData.Cols[0].Width = 30;
            GVData.Cols[1].Width = 80;
            GVData.Cols[2].Width = 200;
            GVData.Cols[3].Width = 70;
            GVData.Cols[4].Width = 90;
            GVData.Cols[5].Width = 150;
            GVData.Cols[6].Width = 100;
            GVData.Cols[7].Visible = false;

            GVData.Cols[3].TextAlign = TextAlignEnum.RightCenter;
        }
        #endregion

        private void txt_Rank_KeyPress(object sender, KeyPressEventArgs e)
        {
           
                if ((e.KeyChar >= '0' && e.KeyChar <= '9') || (Keys)e.KeyChar == Keys.Back || e.KeyChar == 46||e.KeyChar==44)
                {

                    e.Handled = false;

                }
                else
                {
                    e.Handled = true;
                }
           
        }
        private void Txt_Value_Leave(object sender, EventArgs e)
        {
            double zAmount = 0;
            if (double.TryParse(txt_Value.Text.Trim(), out zAmount))
            {
                txt_Value.Text = zAmount.Toe1String();
            }
            else
            {
                MessageBox.Show("Lỗi!.Định dạng số.");
            }
        }
        private void txt_Value_KeyPress(object sender, KeyPressEventArgs e)
        {

            if ((e.KeyChar >= '0' && e.KeyChar <= '9') || (Keys)e.KeyChar == Keys.Back || e.KeyChar == 46||e.KeyChar==44)
            {

                e.Handled = false;

            }
            else
            {
                e.Handled = true;
            }

        }
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
               // this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion             
        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
                btn_Export.Enabled = false;
            }
            if (Role.RoleAdd == 0 && Role.RoleEdit == 0)
            {
                btn_New.Enabled = false;
                btn_Save.Enabled = false;
            }
            if (Role.RoleAdd == 0 && Role.RoleEdit == 1)
            {
                btn_New.Enabled = false;
                btn_Save.Enabled = true;
            }
            if (Role.RoleDel == 0)
            {
                btn_Del.Enabled = false;
            }
        }
        #endregion
    }
}
