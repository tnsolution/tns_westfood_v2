﻿
namespace TNS.WinApp
{
    partial class Frm_Setup_Rice_03
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Setup_Rice_03));
            this.HeaderControl = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btnMini = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnMax = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnClose = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.Panel_Left = new System.Windows.Forms.Panel();
            this.GVData = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.kryptonHeader3 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.Panel_Right = new System.Windows.Forms.Panel();
            this.btn_Save = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Del = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_New = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.kryptonHeader6 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.label13 = new System.Windows.Forms.Label();
            this.txt_Money = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.chk_Minus = new System.Windows.Forms.CheckBox();
            this.chk_Plus = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbl_Modified = new System.Windows.Forms.Label();
            this.lbl_Created = new System.Windows.Forms.Label();
            this.kryptonHeader5 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.txt_Description = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txt_ID = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txt_RiceName = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txt_ST = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txt_ID_Rice = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txt_SL = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txt_Rank = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.btn_Export = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Search = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.txt_Search = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btn_SaveTemp = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Refresh = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Ad = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.dte_Date = new ComponentFactory.Krypton.Toolkit.KryptonDateTimePicker();
            this.kryptonHeader2 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.kryptonHeader4 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.LB_List = new System.Windows.Forms.ListBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.cbo_Rice = new ComponentFactory.Krypton.Toolkit.KryptonComboBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.Panel_Done = new System.Windows.Forms.Panel();
            this.dte_Coppy = new ComponentFactory.Krypton.Toolkit.KryptonDateTimePicker();
            this.lbl_SaoChep = new System.Windows.Forms.Label();
            this.kryptonHeader1 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btnClose_Panel_Message = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btn_OK = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.GV_Temp = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.lbl_Title = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.panel5 = new System.Windows.Forms.Panel();
            this.dte_Month = new ComponentFactory.Krypton.Toolkit.KryptonDateTimePicker();
            this.label12 = new System.Windows.Forms.Label();
            this.btn_SearchTemp = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Done = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Copy = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.label6 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.Panel_Left.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GVData)).BeginInit();
            this.Panel_Right.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbo_Rice)).BeginInit();
            this.panel2.SuspendLayout();
            this.Panel_Done.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GV_Temp)).BeginInit();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // HeaderControl
            // 
            this.HeaderControl.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnMini,
            this.btnMax,
            this.btnClose});
            this.HeaderControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.HeaderControl.Location = new System.Drawing.Point(0, 0);
            this.HeaderControl.Name = "HeaderControl";
            this.HeaderControl.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.HeaderControl.Size = new System.Drawing.Size(1366, 42);
            this.HeaderControl.TabIndex = 220;
            this.HeaderControl.Values.Description = "";
            this.HeaderControl.Values.Heading = "Cài đặt mã cơm";
            this.HeaderControl.Values.Image = ((System.Drawing.Image)(resources.GetObject("HeaderControl.Values.Image")));
            // 
            // btnMini
            // 
            this.btnMini.Image = ((System.Drawing.Image)(resources.GetObject("btnMini.Image")));
            this.btnMini.UniqueName = "F5F06E8241504E72ABB5BEA3F6A9B753";
            // 
            // btnMax
            // 
            this.btnMax.Image = ((System.Drawing.Image)(resources.GetObject("btnMax.Image")));
            this.btnMax.UniqueName = "035D1A4881E44F58A084C31DE7352A94";
            // 
            // btnClose
            // 
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.UniqueName = "11B07C6F4E1C4F9D8B91BD924CB0EBE6";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(0, 42);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1366, 558);
            this.tabControl1.TabIndex = 306;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(178)))), ((int)(((byte)(229)))));
            this.tabPage1.Controls.Add(this.panel3);
            this.tabPage1.Location = new System.Drawing.Point(4, 23);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(1358, 531);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Cài đặt mã cơm";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.Panel_Left);
            this.panel3.Controls.Add(this.Panel_Right);
            this.panel3.Controls.Add(this.panel1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1358, 531);
            this.panel3.TabIndex = 204;
            // 
            // Panel_Left
            // 
            this.Panel_Left.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Left.Controls.Add(this.GVData);
            this.Panel_Left.Controls.Add(this.kryptonHeader3);
            this.Panel_Left.Dock = System.Windows.Forms.DockStyle.Left;
            this.Panel_Left.Location = new System.Drawing.Point(0, 80);
            this.Panel_Left.Name = "Panel_Left";
            this.Panel_Left.Size = new System.Drawing.Size(993, 449);
            this.Panel_Left.TabIndex = 307;
            // 
            // GVData
            // 
            this.GVData.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None;
            this.GVData.AllowEditing = false;
            this.GVData.AllowResizing = C1.Win.C1FlexGrid.AllowResizingEnum.None;
            this.GVData.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None;
            this.GVData.AutoResize = true;
            this.GVData.ColumnInfo = "10,1,0,0,0,95,Columns:";
            this.GVData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GVData.ExtendLastCol = true;
            this.GVData.Location = new System.Drawing.Point(0, 30);
            this.GVData.Name = "GVData";
            this.GVData.Rows.DefaultSize = 19;
            this.GVData.Size = new System.Drawing.Size(993, 419);
            this.GVData.TabIndex = 283;
            // 
            // kryptonHeader3
            // 
            this.kryptonHeader3.AutoSize = false;
            this.kryptonHeader3.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader3.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader3.Name = "kryptonHeader3";
            this.kryptonHeader3.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader3.Size = new System.Drawing.Size(993, 30);
            this.kryptonHeader3.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader3.TabIndex = 224;
            this.kryptonHeader3.Values.Description = "";
            this.kryptonHeader3.Values.Heading = "Danh sách";
            // 
            // Panel_Right
            // 
            this.Panel_Right.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Right.Controls.Add(this.btn_Save);
            this.Panel_Right.Controls.Add(this.btn_Del);
            this.Panel_Right.Controls.Add(this.btn_New);
            this.Panel_Right.Controls.Add(this.kryptonHeader6);
            this.Panel_Right.Controls.Add(this.label13);
            this.Panel_Right.Controls.Add(this.txt_Money);
            this.Panel_Right.Controls.Add(this.chk_Minus);
            this.Panel_Right.Controls.Add(this.chk_Plus);
            this.Panel_Right.Controls.Add(this.label1);
            this.Panel_Right.Controls.Add(this.label5);
            this.Panel_Right.Controls.Add(this.label4);
            this.Panel_Right.Controls.Add(this.label8);
            this.Panel_Right.Controls.Add(this.label7);
            this.Panel_Right.Controls.Add(this.label3);
            this.Panel_Right.Controls.Add(this.label2);
            this.Panel_Right.Controls.Add(this.lbl_Modified);
            this.Panel_Right.Controls.Add(this.lbl_Created);
            this.Panel_Right.Controls.Add(this.kryptonHeader5);
            this.Panel_Right.Controls.Add(this.txt_Description);
            this.Panel_Right.Controls.Add(this.txt_ID);
            this.Panel_Right.Controls.Add(this.txt_RiceName);
            this.Panel_Right.Controls.Add(this.txt_ST);
            this.Panel_Right.Controls.Add(this.txt_ID_Rice);
            this.Panel_Right.Controls.Add(this.txt_SL);
            this.Panel_Right.Controls.Add(this.txt_Rank);
            this.Panel_Right.Dock = System.Windows.Forms.DockStyle.Right;
            this.Panel_Right.Location = new System.Drawing.Point(1019, 80);
            this.Panel_Right.Name = "Panel_Right";
            this.Panel_Right.Size = new System.Drawing.Size(337, 449);
            this.Panel_Right.TabIndex = 228;
            // 
            // btn_Save
            // 
            this.btn_Save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Save.Location = new System.Drawing.Point(120, 402);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Save.Size = new System.Drawing.Size(98, 40);
            this.btn_Save.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Save.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Save.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Save.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Save.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Save.TabIndex = 356;
            this.btn_Save.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Save.Values.Image")));
            this.btn_Save.Values.Text = "Cập nhật";
            // 
            // btn_Del
            // 
            this.btn_Del.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Del.Location = new System.Drawing.Point(232, 402);
            this.btn_Del.Name = "btn_Del";
            this.btn_Del.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Del.Size = new System.Drawing.Size(93, 40);
            this.btn_Del.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Del.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Del.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Del.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Del.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Del.TabIndex = 357;
            this.btn_Del.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Del.Values.Image")));
            this.btn_Del.Values.Text = "Xóa";
            // 
            // btn_New
            // 
            this.btn_New.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_New.Location = new System.Drawing.Point(11, 402);
            this.btn_New.Name = "btn_New";
            this.btn_New.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_New.Size = new System.Drawing.Size(94, 40);
            this.btn_New.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_New.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_New.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_New.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_New.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_New.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_New.TabIndex = 355;
            this.btn_New.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_New.Values.Image")));
            this.btn_New.Values.Text = "Làm mới";
            // 
            // kryptonHeader6
            // 
            this.kryptonHeader6.AutoSize = false;
            this.kryptonHeader6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.kryptonHeader6.Location = new System.Drawing.Point(0, 389);
            this.kryptonHeader6.Name = "kryptonHeader6";
            this.kryptonHeader6.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader6.Size = new System.Drawing.Size(337, 60);
            this.kryptonHeader6.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader6.TabIndex = 364;
            this.kryptonHeader6.Values.Description = "";
            this.kryptonHeader6.Values.Heading = "";
            this.kryptonHeader6.Values.Image = null;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label13.Location = new System.Drawing.Point(6, 284);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(46, 15);
            this.label13.TabIndex = 363;
            this.label13.Text = "Số tiền";
            // 
            // txt_Money
            // 
            this.txt_Money.Location = new System.Drawing.Point(70, 282);
            this.txt_Money.Multiline = true;
            this.txt_Money.Name = "txt_Money";
            this.txt_Money.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_Money.Size = new System.Drawing.Size(122, 26);
            this.txt_Money.StateCommon.Border.ColorAngle = 1F;
            this.txt_Money.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_Money.StateCommon.Border.Rounding = 4;
            this.txt_Money.StateCommon.Border.Width = 1;
            this.txt_Money.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Money.TabIndex = 362;
            this.txt_Money.Text = "0";
            this.txt_Money.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // chk_Minus
            // 
            this.chk_Minus.AutoSize = true;
            this.chk_Minus.Location = new System.Drawing.Point(76, 166);
            this.chk_Minus.Name = "chk_Minus";
            this.chk_Minus.Size = new System.Drawing.Size(102, 18);
            this.chk_Minus.TabIndex = 359;
            this.chk_Minus.Text = "Trừ tiền cơm ";
            this.chk_Minus.UseVisualStyleBackColor = true;
            // 
            // chk_Plus
            // 
            this.chk_Plus.AutoSize = true;
            this.chk_Plus.Checked = true;
            this.chk_Plus.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chk_Plus.Location = new System.Drawing.Point(76, 142);
            this.chk_Plus.Name = "chk_Plus";
            this.chk_Plus.Size = new System.Drawing.Size(110, 18);
            this.chk_Plus.TabIndex = 360;
            this.chk_Plus.Text = "Cộng tiền cơm ";
            this.chk_Plus.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label1.Location = new System.Drawing.Point(14, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 15);
            this.label1.TabIndex = 358;
            this.label1.Text = "Mã cơm";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label5.Location = new System.Drawing.Point(24, 111);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 15);
            this.label5.TabIndex = 358;
            this.label5.Text = "Mã ID";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label4.Location = new System.Drawing.Point(9, 142);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 15);
            this.label4.TabIndex = 358;
            this.label4.Text = "Loại cơm";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label8.Location = new System.Drawing.Point(8, 324);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 15);
            this.label8.TabIndex = 353;
            this.label8.Text = "Sắp xếp";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label7.Location = new System.Drawing.Point(11, 184);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 15);
            this.label7.TabIndex = 353;
            this.label7.Text = "Ghi chú";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(103, 76);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 15);
            this.label3.TabIndex = 353;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label2.Location = new System.Drawing.Point(7, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 15);
            this.label2.TabIndex = 353;
            this.label2.Text = "Tên cơm";
            // 
            // lbl_Modified
            // 
            this.lbl_Modified.AutoSize = true;
            this.lbl_Modified.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lbl_Modified.Location = new System.Drawing.Point(9, 372);
            this.lbl_Modified.Name = "lbl_Modified";
            this.lbl_Modified.Size = new System.Drawing.Size(68, 14);
            this.lbl_Modified.TabIndex = 285;
            this.lbl_Modified.Text = "Chỉnh sửa: ";
            // 
            // lbl_Created
            // 
            this.lbl_Created.AutoSize = true;
            this.lbl_Created.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lbl_Created.Location = new System.Drawing.Point(14, 346);
            this.lbl_Created.Name = "lbl_Created";
            this.lbl_Created.Size = new System.Drawing.Size(56, 14);
            this.lbl_Created.TabIndex = 286;
            this.lbl_Created.Text = "Tạo bởi: ";
            // 
            // kryptonHeader5
            // 
            this.kryptonHeader5.AutoSize = false;
            this.kryptonHeader5.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader5.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader5.Name = "kryptonHeader5";
            this.kryptonHeader5.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader5.Size = new System.Drawing.Size(337, 30);
            this.kryptonHeader5.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader5.TabIndex = 223;
            this.kryptonHeader5.Values.Description = "";
            this.kryptonHeader5.Values.Heading = "Thông tin chi tiết";
            // 
            // txt_Description
            // 
            this.txt_Description.Location = new System.Drawing.Point(74, 191);
            this.txt_Description.Multiline = true;
            this.txt_Description.Name = "txt_Description";
            this.txt_Description.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_Description.Size = new System.Drawing.Size(209, 85);
            this.txt_Description.StateCommon.Border.ColorAngle = 1F;
            this.txt_Description.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_Description.StateCommon.Border.Rounding = 4;
            this.txt_Description.StateCommon.Border.Width = 1;
            this.txt_Description.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Description.TabIndex = 110;
            // 
            // txt_ID
            // 
            this.txt_ID.Location = new System.Drawing.Point(74, 44);
            this.txt_ID.Multiline = true;
            this.txt_ID.Name = "txt_ID";
            this.txt_ID.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_ID.Size = new System.Drawing.Size(75, 23);
            this.txt_ID.StateCommon.Border.ColorAngle = 1F;
            this.txt_ID.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_ID.StateCommon.Border.Rounding = 4;
            this.txt_ID.StateCommon.Border.Width = 1;
            this.txt_ID.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_ID.TabIndex = 110;
            // 
            // txt_RiceName
            // 
            this.txt_RiceName.Location = new System.Drawing.Point(74, 77);
            this.txt_RiceName.Multiline = true;
            this.txt_RiceName.Name = "txt_RiceName";
            this.txt_RiceName.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_RiceName.Size = new System.Drawing.Size(207, 23);
            this.txt_RiceName.StateCommon.Border.ColorAngle = 1F;
            this.txt_RiceName.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_RiceName.StateCommon.Border.Rounding = 4;
            this.txt_RiceName.StateCommon.Border.Width = 1;
            this.txt_RiceName.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_RiceName.TabIndex = 110;
            // 
            // txt_ST
            // 
            this.txt_ST.Enabled = false;
            this.txt_ST.Location = new System.Drawing.Point(219, 110);
            this.txt_ST.Name = "txt_ST";
            this.txt_ST.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_ST.Size = new System.Drawing.Size(62, 26);
            this.txt_ST.StateCommon.Border.ColorAngle = 1F;
            this.txt_ST.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_ST.StateCommon.Border.Rounding = 4;
            this.txt_ST.StateCommon.Border.Width = 1;
            this.txt_ST.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_ST.TabIndex = 110;
            this.txt_ST.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt_ID_Rice
            // 
            this.txt_ID_Rice.Enabled = false;
            this.txt_ID_Rice.Location = new System.Drawing.Point(74, 110);
            this.txt_ID_Rice.Name = "txt_ID_Rice";
            this.txt_ID_Rice.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_ID_Rice.Size = new System.Drawing.Size(62, 26);
            this.txt_ID_Rice.StateCommon.Border.ColorAngle = 1F;
            this.txt_ID_Rice.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_ID_Rice.StateCommon.Border.Rounding = 4;
            this.txt_ID_Rice.StateCommon.Border.Width = 1;
            this.txt_ID_Rice.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_ID_Rice.TabIndex = 110;
            this.txt_ID_Rice.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt_SL
            // 
            this.txt_SL.Enabled = false;
            this.txt_SL.Location = new System.Drawing.Point(146, 110);
            this.txt_SL.Name = "txt_SL";
            this.txt_SL.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_SL.Size = new System.Drawing.Size(62, 26);
            this.txt_SL.StateCommon.Border.ColorAngle = 1F;
            this.txt_SL.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_SL.StateCommon.Border.Rounding = 4;
            this.txt_SL.StateCommon.Border.Width = 1;
            this.txt_SL.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_SL.TabIndex = 110;
            this.txt_SL.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt_Rank
            // 
            this.txt_Rank.Location = new System.Drawing.Point(71, 320);
            this.txt_Rank.Name = "txt_Rank";
            this.txt_Rank.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_Rank.Size = new System.Drawing.Size(121, 26);
            this.txt_Rank.StateCommon.Border.ColorAngle = 1F;
            this.txt_Rank.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_Rank.StateCommon.Border.Rounding = 4;
            this.txt_Rank.StateCommon.Border.Width = 1;
            this.txt_Rank.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Rank.TabIndex = 110;
            this.txt_Rank.Text = "0";
            this.txt_Rank.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.btn_Export);
            this.panel1.Controls.Add(this.btn_Search);
            this.panel1.Controls.Add(this.txt_Search);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1356, 80);
            this.panel1.TabIndex = 306;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label15.Location = new System.Drawing.Point(16, 27);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(46, 15);
            this.label15.TabIndex = 267;
            this.label15.Text = "Mã/tên";
            // 
            // btn_Export
            // 
            this.btn_Export.Location = new System.Drawing.Point(372, 13);
            this.btn_Export.Name = "btn_Export";
            this.btn_Export.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Export.Size = new System.Drawing.Size(113, 40);
            this.btn_Export.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Export.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Export.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Export.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Export.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Export.TabIndex = 238;
            this.btn_Export.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Export.Values.Image")));
            this.btn_Export.Values.Text = "Xuất Excel";
            // 
            // btn_Search
            // 
            this.btn_Search.Location = new System.Drawing.Point(261, 13);
            this.btn_Search.Name = "btn_Search";
            this.btn_Search.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Search.Size = new System.Drawing.Size(105, 40);
            this.btn_Search.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Search.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Search.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Search.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Search.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Search.TabIndex = 206;
            this.btn_Search.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Search.Values.Image")));
            this.btn_Search.Values.Text = "Tìm kiếm";
            // 
            // txt_Search
            // 
            this.txt_Search.Location = new System.Drawing.Point(79, 21);
            this.txt_Search.Name = "txt_Search";
            this.txt_Search.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_Search.Size = new System.Drawing.Size(176, 26);
            this.txt_Search.StateCommon.Border.ColorAngle = 1F;
            this.txt_Search.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_Search.StateCommon.Border.Rounding = 4;
            this.txt_Search.StateCommon.Border.Width = 1;
            this.txt_Search.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Search.TabIndex = 110;
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.tabPage3.Controls.Add(this.panel4);
            this.tabPage3.Controls.Add(this.panel2);
            this.tabPage3.Controls.Add(this.panel5);
            this.tabPage3.Location = new System.Drawing.Point(4, 23);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1358, 531);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Cài đặt hiển thị";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panel4.Controls.Add(this.btn_SaveTemp);
            this.panel4.Controls.Add(this.btn_Refresh);
            this.panel4.Controls.Add(this.btn_Ad);
            this.panel4.Controls.Add(this.dte_Date);
            this.panel4.Controls.Add(this.kryptonHeader2);
            this.panel4.Controls.Add(this.kryptonHeader4);
            this.panel4.Controls.Add(this.LB_List);
            this.panel4.Controls.Add(this.label6);
            this.panel4.Controls.Add(this.label22);
            this.panel4.Controls.Add(this.label17);
            this.panel4.Controls.Add(this.cbo_Rice);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(1026, 83);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(329, 445);
            this.panel4.TabIndex = 309;
            // 
            // btn_SaveTemp
            // 
            this.btn_SaveTemp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_SaveTemp.Location = new System.Drawing.Point(184, 400);
            this.btn_SaveTemp.Name = "btn_SaveTemp";
            this.btn_SaveTemp.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_SaveTemp.Size = new System.Drawing.Size(116, 40);
            this.btn_SaveTemp.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_SaveTemp.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_SaveTemp.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_SaveTemp.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_SaveTemp.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_SaveTemp.TabIndex = 356;
            this.btn_SaveTemp.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_SaveTemp.Values.Image")));
            this.btn_SaveTemp.Values.Text = "Cập nhật";
            // 
            // btn_Refresh
            // 
            this.btn_Refresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_Refresh.Location = new System.Drawing.Point(28, 400);
            this.btn_Refresh.Name = "btn_Refresh";
            this.btn_Refresh.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Refresh.Size = new System.Drawing.Size(116, 40);
            this.btn_Refresh.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Refresh.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Refresh.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Refresh.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Refresh.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Refresh.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Refresh.TabIndex = 362;
            this.btn_Refresh.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Refresh.Values.Image")));
            this.btn_Refresh.Values.Text = "Làm mới";
            // 
            // btn_Ad
            // 
            this.btn_Ad.Location = new System.Drawing.Point(265, 233);
            this.btn_Ad.Name = "btn_Ad";
            this.btn_Ad.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Ad.Size = new System.Drawing.Size(35, 23);
            this.btn_Ad.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Ad.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Ad.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Ad.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Ad.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Ad.TabIndex = 363;
            this.btn_Ad.Values.Image = global::TNS.WinApp.Properties.Resources.greenplus_16x16;
            this.btn_Ad.Values.Text = "";
            // 
            // dte_Date
            // 
            this.dte_Date.CalendarTodayDate = new System.DateTime(2021, 10, 20, 0, 0, 0, 0);
            this.dte_Date.CustomFormat = "MM/yyyy";
            this.dte_Date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dte_Date.Location = new System.Drawing.Point(92, 52);
            this.dte_Date.Name = "dte_Date";
            this.dte_Date.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.dte_Date.ShowUpDown = true;
            this.dte_Date.Size = new System.Drawing.Size(86, 23);
            this.dte_Date.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.dte_Date.StateCommon.Border.Rounding = 4;
            this.dte_Date.StateCommon.Border.Width = 1;
            this.dte_Date.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dte_Date.TabIndex = 307;
            // 
            // kryptonHeader2
            // 
            this.kryptonHeader2.AutoSize = false;
            this.kryptonHeader2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.kryptonHeader2.Location = new System.Drawing.Point(0, 390);
            this.kryptonHeader2.Name = "kryptonHeader2";
            this.kryptonHeader2.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader2.Size = new System.Drawing.Size(329, 55);
            this.kryptonHeader2.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader2.TabIndex = 354;
            this.kryptonHeader2.Values.Description = "";
            this.kryptonHeader2.Values.Heading = "";
            this.kryptonHeader2.Values.Image = null;
            // 
            // kryptonHeader4
            // 
            this.kryptonHeader4.AutoSize = false;
            this.kryptonHeader4.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader4.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader4.Name = "kryptonHeader4";
            this.kryptonHeader4.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader4.Size = new System.Drawing.Size(329, 30);
            this.kryptonHeader4.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader4.TabIndex = 223;
            this.kryptonHeader4.Values.Description = "";
            this.kryptonHeader4.Values.Heading = "Thông tin tạo mới";
            // 
            // LB_List
            // 
            this.LB_List.FormattingEnabled = true;
            this.LB_List.ItemHeight = 14;
            this.LB_List.Location = new System.Drawing.Point(36, 111);
            this.LB_List.Name = "LB_List";
            this.LB_List.Size = new System.Drawing.Size(264, 102);
            this.LB_List.TabIndex = 361;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label22.Location = new System.Drawing.Point(33, 57);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(42, 15);
            this.label22.TabIndex = 269;
            this.label22.Text = "Tháng";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label17.Location = new System.Drawing.Point(37, 236);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(54, 15);
            this.label17.TabIndex = 353;
            this.label17.Text = "Mã cơm";
            // 
            // cbo_Rice
            // 
            this.cbo_Rice.DropDownWidth = 119;
            this.cbo_Rice.Location = new System.Drawing.Point(94, 233);
            this.cbo_Rice.Name = "cbo_Rice";
            this.cbo_Rice.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.cbo_Rice.Size = new System.Drawing.Size(165, 22);
            this.cbo_Rice.StateCommon.ComboBox.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.cbo_Rice.StateCommon.ComboBox.Border.Rounding = 4;
            this.cbo_Rice.StateCommon.ComboBox.Border.Width = 1;
            this.cbo_Rice.StateCommon.ComboBox.Content.Font = new System.Drawing.Font("Tahoma", 9F);
            this.cbo_Rice.StateCommon.Item.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.cbo_Rice.StateCommon.Item.Border.Rounding = 4;
            this.cbo_Rice.StateCommon.Item.Border.Width = 1;
            this.cbo_Rice.StateCommon.Item.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cbo_Rice.TabIndex = 359;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panel2.Controls.Add(this.Panel_Done);
            this.panel2.Controls.Add(this.GV_Temp);
            this.panel2.Controls.Add(this.lbl_Title);
            this.panel2.Location = new System.Drawing.Point(3, 83);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1023, 445);
            this.panel2.TabIndex = 308;
            // 
            // Panel_Done
            // 
            this.Panel_Done.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.Panel_Done.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Done.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel_Done.Controls.Add(this.dte_Coppy);
            this.Panel_Done.Controls.Add(this.lbl_SaoChep);
            this.Panel_Done.Controls.Add(this.kryptonHeader1);
            this.Panel_Done.Controls.Add(this.btn_OK);
            this.Panel_Done.Location = new System.Drawing.Point(487, 110);
            this.Panel_Done.Name = "Panel_Done";
            this.Panel_Done.Size = new System.Drawing.Size(321, 154);
            this.Panel_Done.TabIndex = 292;
            // 
            // dte_Coppy
            // 
            this.dte_Coppy.CalendarTodayDate = new System.DateTime(2021, 10, 20, 0, 0, 0, 0);
            this.dte_Coppy.CustomFormat = "MM/yyyy";
            this.dte_Coppy.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dte_Coppy.Location = new System.Drawing.Point(168, 48);
            this.dte_Coppy.Name = "dte_Coppy";
            this.dte_Coppy.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.dte_Coppy.ShowUpDown = true;
            this.dte_Coppy.Size = new System.Drawing.Size(86, 23);
            this.dte_Coppy.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.dte_Coppy.StateCommon.Border.Rounding = 4;
            this.dte_Coppy.StateCommon.Border.Width = 1;
            this.dte_Coppy.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dte_Coppy.TabIndex = 307;
            // 
            // lbl_SaoChep
            // 
            this.lbl_SaoChep.AutoSize = true;
            this.lbl_SaoChep.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_SaoChep.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lbl_SaoChep.Location = new System.Drawing.Point(44, 37);
            this.lbl_SaoChep.Name = "lbl_SaoChep";
            this.lbl_SaoChep.Size = new System.Drawing.Size(108, 45);
            this.lbl_SaoChep.TabIndex = 269;
            this.lbl_SaoChep.Text = "Bạn có muốn sao \r\nchép tháng đang \r\nxem sang tháng";
            // 
            // kryptonHeader1
            // 
            this.kryptonHeader1.AutoSize = false;
            this.kryptonHeader1.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnClose_Panel_Message});
            this.kryptonHeader1.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader1.HeaderStyle = ComponentFactory.Krypton.Toolkit.HeaderStyle.Secondary;
            this.kryptonHeader1.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader1.Name = "kryptonHeader1";
            this.kryptonHeader1.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader1.Size = new System.Drawing.Size(319, 30);
            this.kryptonHeader1.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.kryptonHeader1.TabIndex = 206;
            this.kryptonHeader1.Values.Description = "";
            this.kryptonHeader1.Values.Heading = "Thông tin sao chép";
            this.kryptonHeader1.Values.Image = null;
            // 
            // btnClose_Panel_Message
            // 
            this.btnClose_Panel_Message.Checked = ComponentFactory.Krypton.Toolkit.ButtonCheckState.Checked;
            this.btnClose_Panel_Message.Enabled = ComponentFactory.Krypton.Toolkit.ButtonEnabled.True;
            this.btnClose_Panel_Message.Type = ComponentFactory.Krypton.Toolkit.PaletteButtonSpecStyle.Close;
            this.btnClose_Panel_Message.UniqueName = "BEA2AE54CDCE44F263AB208C74E6907A";
            // 
            // btn_OK
            // 
            this.btn_OK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_OK.Location = new System.Drawing.Point(97, 98);
            this.btn_OK.Name = "btn_OK";
            this.btn_OK.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_OK.Size = new System.Drawing.Size(119, 40);
            this.btn_OK.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_OK.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_OK.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_OK.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_OK.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_OK.TabIndex = 12;
            this.btn_OK.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_OK.Values.Image")));
            this.btn_OK.Values.Text = "OK";
            // 
            // GV_Temp
            // 
            this.GV_Temp.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None;
            this.GV_Temp.AllowEditing = false;
            this.GV_Temp.AllowResizing = C1.Win.C1FlexGrid.AllowResizingEnum.None;
            this.GV_Temp.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None;
            this.GV_Temp.AutoResize = true;
            this.GV_Temp.ColumnInfo = "10,1,0,0,0,95,Columns:";
            this.GV_Temp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GV_Temp.ExtendLastCol = true;
            this.GV_Temp.Location = new System.Drawing.Point(0, 30);
            this.GV_Temp.Name = "GV_Temp";
            this.GV_Temp.Rows.DefaultSize = 19;
            this.GV_Temp.Size = new System.Drawing.Size(1023, 415);
            this.GV_Temp.TabIndex = 283;
            // 
            // lbl_Title
            // 
            this.lbl_Title.AutoSize = false;
            this.lbl_Title.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_Title.Location = new System.Drawing.Point(0, 0);
            this.lbl_Title.Name = "lbl_Title";
            this.lbl_Title.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.lbl_Title.Size = new System.Drawing.Size(1023, 30);
            this.lbl_Title.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.lbl_Title.TabIndex = 224;
            this.lbl_Title.Values.Description = "";
            this.lbl_Title.Values.Heading = "Danh sách";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panel5.Controls.Add(this.dte_Month);
            this.panel5.Controls.Add(this.label12);
            this.panel5.Controls.Add(this.btn_SearchTemp);
            this.panel5.Controls.Add(this.btn_Done);
            this.panel5.Controls.Add(this.btn_Copy);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(3, 3);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1352, 80);
            this.panel5.TabIndex = 310;
            // 
            // dte_Month
            // 
            this.dte_Month.CalendarTodayDate = new System.DateTime(2021, 10, 20, 0, 0, 0, 0);
            this.dte_Month.CustomFormat = "MM/yyyy";
            this.dte_Month.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dte_Month.Location = new System.Drawing.Point(64, 27);
            this.dte_Month.Name = "dte_Month";
            this.dte_Month.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.dte_Month.ShowUpDown = true;
            this.dte_Month.Size = new System.Drawing.Size(86, 23);
            this.dte_Month.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.dte_Month.StateCommon.Border.Rounding = 4;
            this.dte_Month.StateCommon.Border.Width = 1;
            this.dte_Month.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dte_Month.TabIndex = 307;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label12.Location = new System.Drawing.Point(16, 30);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(42, 15);
            this.label12.TabIndex = 267;
            this.label12.Text = "Tháng";
            // 
            // btn_SearchTemp
            // 
            this.btn_SearchTemp.Location = new System.Drawing.Point(156, 17);
            this.btn_SearchTemp.Name = "btn_SearchTemp";
            this.btn_SearchTemp.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_SearchTemp.Size = new System.Drawing.Size(105, 40);
            this.btn_SearchTemp.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_SearchTemp.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_SearchTemp.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_SearchTemp.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_SearchTemp.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_SearchTemp.TabIndex = 206;
            this.btn_SearchTemp.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_SearchTemp.Values.Image")));
            this.btn_SearchTemp.Values.Text = "Tìm kiếm";
            // 
            // btn_Done
            // 
            this.btn_Done.Location = new System.Drawing.Point(267, 17);
            this.btn_Done.Name = "btn_Done";
            this.btn_Done.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Done.Size = new System.Drawing.Size(116, 40);
            this.btn_Done.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Done.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Done.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Done.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Done.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Done.TabIndex = 358;
            this.btn_Done.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Done.Values.Image")));
            this.btn_Done.Values.Text = "Áp dụng";
            // 
            // btn_Copy
            // 
            this.btn_Copy.Location = new System.Drawing.Point(389, 17);
            this.btn_Copy.Name = "btn_Copy";
            this.btn_Copy.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Copy.Size = new System.Drawing.Size(116, 40);
            this.btn_Copy.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Copy.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Copy.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Copy.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Copy.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Copy.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Copy.TabIndex = 361;
            this.btn_Copy.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Copy.Values.Image")));
            this.btn_Copy.Values.Text = "Sao chép";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label6.Location = new System.Drawing.Point(33, 90);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(151, 15);
            this.label6.TabIndex = 269;
            this.label6.Text = "Dánh sách thứ tự hiển thị";
            // 
            // Frm_Setup_Rice_03
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1366, 600);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.HeaderControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_Setup_Rice_03";
            this.Text = "Frm_Setup_Rice_03";
            this.Load += new System.EventHandler(this.Frm_Setup_Rice_03_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.Panel_Left.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GVData)).EndInit();
            this.Panel_Right.ResumeLayout(false);
            this.Panel_Right.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbo_Rice)).EndInit();
            this.panel2.ResumeLayout(false);
            this.Panel_Done.ResumeLayout(false);
            this.Panel_Done.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GV_Temp)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ComponentFactory.Krypton.Toolkit.KryptonHeader HeaderControl;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMini;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMax;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnClose;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Panel Panel_Right;
        private System.Windows.Forms.CheckBox chk_Minus;
        private System.Windows.Forms.CheckBox chk_Plus;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Save;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Del;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_New;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbl_Modified;
        private System.Windows.Forms.Label lbl_Created;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader5;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_Description;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_RiceName;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_ST;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_SL;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_Rank;
        private System.Windows.Forms.Label label13;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_Money;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label15;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Export;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Search;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_Search;
        private System.Windows.Forms.Panel Panel_Left;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader3;
        private C1.Win.C1FlexGrid.C1FlexGrid GVData;
        private System.Windows.Forms.Panel panel2;
        private C1.Win.C1FlexGrid.C1FlexGrid GV_Temp;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader lbl_Title;
        private System.Windows.Forms.Panel panel4;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader2;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader4;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Done;
        private System.Windows.Forms.Label label1;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_ID;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_ID_Rice;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader6;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Copy;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label12;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_SearchTemp;
        private ComponentFactory.Krypton.Toolkit.KryptonDateTimePicker dte_Month;
        private ComponentFactory.Krypton.Toolkit.KryptonDateTimePicker dte_Coppy;
        private System.Windows.Forms.ListBox LB_List;
        private System.Windows.Forms.Label label22;
        private ComponentFactory.Krypton.Toolkit.KryptonComboBox cbo_Rice;
        private System.Windows.Forms.Label label17;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Refresh;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_SaveTemp;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Ad;
        private ComponentFactory.Krypton.Toolkit.KryptonDateTimePicker dte_Date;
        private System.Windows.Forms.Panel Panel_Done;
        private System.Windows.Forms.Label lbl_SaoChep;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader1;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnClose_Panel_Message;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_OK;
        private System.Windows.Forms.Label label6;
    }
}