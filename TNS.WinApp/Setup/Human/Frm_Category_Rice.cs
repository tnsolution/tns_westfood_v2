﻿using C1.Win.C1FlexGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TN.Library.System;
using TNS.HRM;
using TNS.LOG;
using TNS.Misc;
using TNS.SLR;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Category_Rice : Form
    {
        private int _Key = 0;
        public Frm_Category_Rice()
        {
            InitializeComponent();
            btnClose.Click += btnClose_Click;
            btnMini.Click += btnMini_Click;

            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            btn_Save.Click += Btn_Save_Click;
            btn_New.Click += Btn_New_Click;
            btn_Del.Click += Btn_Del_Click;
            btn_Search.Click += Btn_Search_Click;
            GVData.Click += GVData_List_Click;
            btn_Export.Click += Btn_Export_Click;

            chk_Minus.Click += Chk_Minus_Click;
            chk_Plus.Click += Chk_Plus_Click;
        }

        private void Frm_Category_Rice_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            //Check_RoleForm();
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            LoadData();
        }
        private void Chk_Plus_Click(object sender, EventArgs e)
        {

            if (chk_Plus.Checked == true)
            {
                chk_Plus.Checked = true;
                chk_Minus.Checked = false;
            }
            else
            {
                chk_Plus.Checked = false;
                chk_Minus.Checked = true;
            }
        }
        private void Chk_Minus_Click(object sender, EventArgs e)
        {
            if (chk_Minus.Checked == true)
            {
                chk_Plus.Checked = false;
                chk_Minus.Checked = true;
            }
            else
            {
                chk_Plus.Checked = true;
                chk_Minus.Checked = false;
            }
        }

        #region[Progess]


        private void LoadData()
        {
            Category_Rice_Info zinfo = new Category_Rice_Info(_Key);
            txt_Name.Text = zinfo.CategoryName;
            if (zinfo.Type == 0)
            {
                chk_Plus.Checked = true;  // 0: cơm cộng , 1: cơm trừ
                chk_Minus.Checked = false;
            }
            else
            {
                chk_Plus.Checked = false;
                chk_Minus.Checked = true;
            }
            txt_Rank.Text = zinfo.Rank.ToString();
            txt_Description.Text = zinfo.Description;
            if (zinfo.Slug == 0)
            {
                chk_Slug.Checked = false;
            }
            else
            {
                chk_Slug.Checked = true;
            }
            if (zinfo.CategoryKey == 0)
            {
                int zTemp = Category_Rice_Data.AutoKeyEnd(); //Lấy số AutoKey cuối cùng
                if (zTemp == 0)
                {
                    txt_SL.Text = "SL01";
                    txt_ST.Text = "TT01";
                }
                else
                {
                    zTemp++;
                    txt_SL.Text = "SL" + zTemp.ToString().PadLeft(2, '0');
                    txt_ST.Text = "TT" + zTemp.ToString().PadLeft(2, '0');
                }

            }
            else
            {
                txt_SL.Text = zinfo.ID_Number;
                txt_ST.Text = zinfo.ID_Money;
            }
            lbl_Created.Text = "Tạo bởi:[" + zinfo.CreatedName + "][" + zinfo.CreatedOn + "]";
            lbl_Modified.Text = "Chỉnh sửa:[" + zinfo.ModifiedName + "][" + zinfo.ModifiedOn + "]";
        }

        #endregion
        #region[Event]
        private void Btn_Save_Click(object sender, EventArgs e)
        {
            string zMessage = "";
            if (txt_Name.Text.Trim().Length == 0)
            {
                Utils.TNMessageBoxOK("Chưa nhập tên cột cơm!", 1);
                return;
            }
            int zRank;
            if (!int.TryParse(txt_Rank.Text, out zRank))
            {
                Utils.TNMessageBoxOK("Vui lòng nhập đúng định dạng số!", 1);
                return;
            }
            if (zRank == 0)
            {
                Utils.TNMessageBoxOK("Số sắp xếp phải lớn hơn 0!", 1);
                return;
            }

            else
            {
                Category_Rice_Info zinfo = new Category_Rice_Info(_Key);

                zinfo.CategoryName = txt_Name.Text.Trim();
                if (chk_Plus.Checked == false && chk_Minus.Checked == true)
                {
                    zinfo.Type = 1;  // 0: cơm cộng , 1: cơm trừ
                    zinfo.TypeName = "Trừ tiền";
                }
                else
                {
                    zinfo.Type = 0;
                    zinfo.TypeName = "Cộng tiền";
                }
                zinfo.Rank = int.Parse(txt_Rank.Text.Trim());
                zinfo.Description = txt_Description.Text.Trim();
                if (chk_Slug.Checked == false)
                {
                    zinfo.Slug = 0;  // 0: ẩn , 1 : hiện
                }
                else
                {
                    zinfo.Slug = 1;
                }
                zinfo.ID_Number = txt_SL.Text;
                zinfo.ID_Money = txt_ST.Text;

                zinfo.CreatedBy = SessionUser.UserLogin.Key;
                zinfo.CreatedName = SessionUser.UserLogin.EmployeeName;
                zinfo.ModifiedBy = SessionUser.UserLogin.Key;
                zinfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
                if (_Key == 0)
                {
                    zinfo.Create_ServerKey();
                    zMessage = TN_Message.Show(zinfo.Message);
                    if (zMessage == "")
                    {
                        string zKq =SaveLogBangDinhNghia();
                        bool zHRM = false;
                        bool zOffice = false;
                        bool zSupport = false;
                        bool zWorker = false;

                        zHRM = InsertRiceCategory_HRM_CodeReport(zinfo.ID_Number, zinfo.Type, zinfo.CategoryName);
                        zOffice = InsertRiceCategory_SAL_CodeReportOffice(zinfo.ID_Number, zinfo.ID_Money, zinfo.Type, zinfo.CategoryName);
                        zSupport = InsertRiceCategory_SAL_CodeReportSupport(zinfo.ID_Number, zinfo.ID_Money, zinfo.Type, zinfo.CategoryName);
                        zWorker = InsertRiceCategory_SAL_CodeReportWorker(zinfo.ID_Number, zinfo.ID_Money, zinfo.Type, zinfo.CategoryName);
                        if (zKq == "" && zHRM == true && zOffice == true && zSupport == true && zWorker == true)
                        {
                            Utils.TNMessageBoxOK("Cập nhật thành công!", 3);
                            DisplayData();
                            _Key = 0;
                            LoadData();
                        }
                        else
                        {
                            Utils.TNMessageBoxOK("Lỗi.Vui lòng liên hệ IT", 4);
                        }
                    }
                    else
                    {
                        Utils.TNMessageBoxOK(zMessage, 4);
                    }

                }
                else
                {
                    zinfo.Update();
                    zMessage = TN_Message.Show(zinfo.Message);
                    if (zMessage == "")
                    {
                        Utils.TNMessageBoxOK("Cập nhật thành công!", 3);
                        DisplayData();
                        _Key = zinfo.CategoryKey;
                        LoadData();
                    }
                    else
                    {
                        Utils.TNMessageBoxOK(zMessage, 4);
                    }
                }

            }
        }
        private void Btn_Del_Click(object sender, EventArgs e)
        {
            string zMessage = "";
            if (_Key == 0)
            {
                Utils.TNMessageBoxOK("Chưa chọn thông tin!", 1);
            }
            else
            {
                if (Utils.TNMessageBox("Bạn có chắc xóa thông tin này ?.", 2) == "Y")
                {
                    Category_Rice_Info zinfo = new Category_Rice_Info(_Key);

                    zinfo.ModifiedBy = SessionUser.UserLogin.Key;
                    zinfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
                    zinfo.Delete();
                    zMessage = TN_Message.Show(zinfo.Message);
                    if (zMessage == "")
                    {
                        string zKq = SaveLogBangDinhNghia(); ; //Lưu log lại trước 

                        bool zHRM = false;
                        bool zOffice = false;
                        bool zSupport = false;
                        bool zWorker = false;

                        zHRM = DeleteRiceCategory_HRM_CodeReport(zinfo.ID_Number);
                        zOffice = DeleteRiceCategory_CodeOffice(zinfo.ID_Number, zinfo.ID_Money);
                        zSupport = DeleteRiceCategory_CodeReport(zinfo.ID_Number, zinfo.ID_Money);
                        zWorker = DeleteRiceCategory_CodeWorker(zinfo.ID_Number, zinfo.ID_Money);
                        if (zKq == "" && zHRM == true && zOffice == true && zSupport == true && zWorker == true)
                        {
                            Utils.TNMessageBoxOK("Xóa thành công!", 3);
                            DisplayData();
                            _Key = 0;
                            LoadData();
                        }
                        else
                        {
                            Utils.TNMessageBoxOK("Lỗi xóa chấm công.Vui lòng liên hệ IT !", 4);
                        }
                    }
                    else
                    {
                        Utils.TNMessageBoxOK(zMessage, 4);
                    }

                }
            }

        }
        private void Btn_New_Click(object sender, EventArgs e)
        {
            _Key = 0;
            LoadData();
            lbl_Created.Text = "Tạo bởi:";
            lbl_Modified.Text = "Chỉnh sửa:";
            txt_Name.Focus();
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {

            DisplayData();
        }
        #endregion
        #region[ListView]
        private void DisplayData()
        {

            DataTable ztb = Category_Rice_Data.Search(txt_Search.Text.Trim());
            InitGV_Layout(ztb);
        }
        private void GVData_List_Click(object sender, EventArgs e)
        {
            if (GVData.Rows.Count > 0)
            {
                string zKey = GVData.Rows[GVData.RowSel][6].ToString();
                if (zKey != null || zKey != "")
                {
                    _Key = int.Parse(zKey);
                    LoadData();
                }
                else
                {
                    _Key = 0;
                    LoadData();
                }
            }
        }
        private void Btn_Export_Click(object sender, EventArgs e)
        {
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Danh sách cột hiển thị cơm.xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }
                    GVData.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }

        }
        void InitGV_Layout(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            int TotalRow = TableView.Rows.Count + 1;
            int ToTalCol = 7;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(TotalRow);

            //Row Header
            GVData.Rows[0][0] = "Stt";
            GVData.Rows[0][1] = "Tên cột hiển thị";
            GVData.Rows[0][2] = "Loại cơm";
            GVData.Rows[0][3] = "Hiển thị";
            GVData.Rows[0][4] = "Sắp xếp";
            GVData.Rows[0][5] = "Ghi chú";
            GVData.Rows[0][6] = ""; // ẩn cột này

            //Style         
            //GVData.AllowFreezing = AllowFreezingEnum.Both;
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.Custom;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.LeftCenter;
            GVData.Styles.Normal.WordWrap = true;

            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                DataRow rData = TableView.Rows[rIndex];

                GVData.Rows[rIndex + 1][0] = rIndex + 1;
                GVData.Rows[rIndex + 1][1] = rData[1];
                GVData.Rows[rIndex + 1][2] = rData[2];
                if (rData[3].ToString() == "0")
                {
                    GVData.Rows[rIndex + 1][3] = "Tắt";
                }
                else
                {
                    GVData.Rows[rIndex + 1][3] = "Bật";
                }
                GVData.Rows[rIndex + 1][4] = rData[4];
                GVData.Rows[rIndex + 1][5] = rData[5];
                GVData.Rows[rIndex + 1][6] = rData[0];

            }

            //Freeze Row and Column                              
            GVData.Rows.Fixed = 1;
            GVData.Rows[0].TextAlign = TextAlignEnum.CenterCenter;
            GVData.Rows[0].Height = 50;

            GVData.Cols[0].Width = 30;
            GVData.Cols[1].Width = 200;
            GVData.Cols[2].Width = 80;
            GVData.Cols[3].Width = 70;
            GVData.Cols[4].Width = 70;
            GVData.Cols[4].TextAlign = TextAlignEnum.RightCenter;
            GVData.Cols[5].Width = 100;
            GVData.Cols[6].Visible = false;
        }
        #endregion

        #region[Xử lý trong Thêm cột]
        private bool InsertRiceCategory_HRM_CodeReport(string ID_Number, int Type, string CategoryName)
        {

            bool zResult = false;

            if (Type == 0) // cơm cộng
            {
                zResult = Category_Rice_Data.Add_RicePlus(ID_Number, CategoryName);
            }
            else
            {
                zResult = Category_Rice_Data.Add_RiceMinus(ID_Number, CategoryName);
            }
            return zResult;
        }
        private bool InsertRiceCategory_SAL_CodeReportOffice(string ID_Number, string ID_Money, int Type, string CategoryName)
        {

            bool zResult = false;

            if (Type == 0) // cơm cộng
            {
                zResult = Category_Rice_Data.Add_RiceOffice(ID_Number, ID_Money, CategoryName);
            }
            else
            {
                zResult = Category_Rice_Data.Minus_RiceOffice(ID_Number, ID_Money, CategoryName);
            }
            return zResult;
        }
        private bool InsertRiceCategory_SAL_CodeReportSupport(string ID_Number, string ID_Money, int Type, string CategoryName)
        {

            bool zResult = false;

            if (Type == 0) // cơm cộng
            {
                zResult = Category_Rice_Data.Add_RiceSupport(ID_Number, ID_Money, CategoryName);
            }
            else
            {
                zResult = Category_Rice_Data.Minus_RiceSupport(ID_Number, ID_Money, CategoryName);
            }
            return zResult;
        }
        private bool InsertRiceCategory_SAL_CodeReportWorker(string ID_Number, string ID_Money, int Type, string CategoryName)
        {

            bool zResult = false;

            if (Type == 0) // cơm cộng
            {
                zResult = Category_Rice_Data.Add_RiceWorker(ID_Number, ID_Money, CategoryName);
            }
            else
            {
                zResult = Category_Rice_Data.Minus_RiceWorker(ID_Number, ID_Money, CategoryName);
            }
            return zResult;
        }

        private bool DeleteRiceCategory_HRM_CodeReport(string ID_Number)
        {
            bool zResult = false;
            zResult = Category_Rice_Data.Delete_Rice_HRM(ID_Number);
            return zResult;
        }
        private bool DeleteRiceCategory_CodeOffice(string ID_Number, string ID_Money)
        {
            bool zResult = false;
            zResult = Category_Rice_Data.Delete_Rice_Office(ID_Number, ID_Money);
            return zResult;
        }
        private bool DeleteRiceCategory_CodeReport(string ID_Number, string ID_Money)
        {
            bool zResult = false;
            zResult = Category_Rice_Data.Delete_Rice_Support(ID_Number, ID_Money);
            return zResult;
        }
        private bool DeleteRiceCategory_CodeWorker(string ID_Number, string ID_Money)
        {
            bool zResult = false;
            zResult = Category_Rice_Data.Delete_Rice_Worker(ID_Number, ID_Money);
            return zResult;
        }
        #endregion

        private string SaveLogBangDinhNghia()
        {
            string zKq = "";
            zKq += CodeReport_Data.SaveLog_CodeReport(); //Lưu log lại trước 
            zKq += CodeReportOffice_Data.SaveLog_CodeReportOffice(); //Lưu log lại trước 
            zKq += CodeReportSupport_Data.SaveLog_CodeReportSupport(); //Lưu log lại trước 
            zKq += CodeReportWorker_Data.SaveLog_CodeReportWorker(); //Lưu log lại trước 
            return zKq;
        }
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;



        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
                btn_Export.Enabled = false;
            }
            if (Role.RoleAdd == 0 && Role.RoleEdit == 0)
            {
                btn_New.Enabled = false;
                btn_Save.Enabled = false;
            }
            if (Role.RoleAdd == 0 && Role.RoleEdit == 1)
            {
                btn_New.Enabled = false;
                btn_Save.Enabled = true;
            }
            if (Role.RoleDel == 0)
            {
                btn_Del.Enabled = false;
            }
        }
        #endregion
    }
}
