﻿using C1.Win.C1FlexGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TN.Library.System;
using TNS.HRM;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Setup_Rice_02 : Form
    {
        private int _Key = 0;
        public Frm_Setup_Rice_02()
        {
            InitializeComponent();
            btnClose.Click += btnClose_Click;
            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;

            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            btn_Save.Click += Btn_Save_Click;
            btn_New.Click += Btn_New_Click;
            btn_Del.Click += Btn_Del_Click;
            btn_Search.Click += Btn_Search_Click;
            txt_Money.KeyPress += txt_Money_KeyPress;
            txt_Money.KeyUp += Txt_Money_KeyUp;
            txt_Rank.KeyPress += txt_Rank_KeyPress;
            GVData.Click += GVData_List_Click;
            btn_Export.Click += Btn_Export_Click;
            btn_AddCategory.Click += Btn_AddCategory_Click;
        }
        private void Frm_Setup_Rice_02_Load(object sender, EventArgs e)
        {
            LoadDataToToolbox.KryptonComboBox(cbo_Category, "SELECT CategoryKey,CategoryName FROM HRM_Category_Rice WHERE  RecordStatus < 99","--Chọn--");
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            Check_RoleForm();
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            LoadData();
        }

        private void Txt_Money_KeyUp(object sender, KeyEventArgs e)
        {
            double zAmount = 0;
            double.TryParse(txt_Money.Text.Trim(), out zAmount);
            txt_Money.Text = zAmount.Ton0String();
            txt_Money.Focus();
            txt_Money.SelectionStart = txt_Money.Text.Length;
        }

        #region[Progess]
        private void LoadData()
        {

            Time_Rice_Info zinfo = new Time_Rice_Info(_Key);
            txt_RiceID.Text = zinfo.RiceID;
            txt_RiceName.Text = zinfo.RiceName;
            txt_Rank.Text = zinfo.Rank.ToString();
            txt_Description.Text = zinfo.Description;
            dte_FromDate.Value = zinfo.FromDate;
            dte_ToDate.Value = zinfo.ToDate;
            cbo_Category.SelectedValue = zinfo.CategoryKey;
            
            if (zinfo.Money != 0)
                txt_Money.Text = zinfo.Money.ToString("###,###,###");
            else
                txt_Money.Text = "0";
            lbl_Created.Text = "Tạo bởi:[" + zinfo.CreatedName + "][" + zinfo.CreatedOn + "]";
            lbl_Modified.Text = "Chỉnh sửa:[" + zinfo.ModifiedName + "][" + zinfo.ModifiedOn + "]";
        }
        #endregion

        #region[Event]
        private void Btn_Save_Click(object sender, EventArgs e)
        {
            string zMessage = "";
            if (txt_RiceID.Text.Trim().Length == 0)
            {
                Utils.TNMessageBoxOK("Chưa nhập mã!", 1);
                return;
            }
            float zMoney;
            if (!float.TryParse(txt_Money.Text, out zMoney))
            {
                Utils.TNMessageBoxOK("Vui lòng nhập đúng định dạng số!", 1);
                return;
            }
            int zRank;
            if (!int.TryParse(txt_Rank.Text, out zRank))
            {
                Utils.TNMessageBoxOK("Vui lòng nhập đúng định dạng số!", 1);
                return;
            }
            if(cbo_Category.SelectedValue==null|| cbo_Category.SelectedValue.ToInt()==0)
            {
                Utils.TNMessageBoxOK("Vui lòng chọn cột hiển thị cơm!", 1);
                return;
            }    
            else
            {
                Time_Rice_Info zinfo = new Time_Rice_Info(_Key);

                zinfo.RiceID = txt_RiceID.Text.Trim().ToUpper();
                zinfo.RiceName = txt_RiceName.Text.Trim();
                zinfo.Money = float.Parse(txt_Money.Text);
                zinfo.Rank = int.Parse(txt_Rank.Text.Trim());
                zinfo.Description = txt_Description.Text.Trim();
                zinfo.FromDate = dte_FromDate.Value;
                zinfo.ToDate = dte_ToDate.Value;
                zinfo.CategoryKey = cbo_Category.SelectedValue.ToInt();
                Category_Rice_Info zCate = new Category_Rice_Info(zinfo.CategoryKey);
                zinfo.Slug = zCate.Type; //lấy loại cơm cộng hoặc trừ

                zinfo.CreatedBy = SessionUser.UserLogin.Key;
                zinfo.CreatedName = SessionUser.UserLogin.EmployeeName;
                zinfo.ModifiedBy = SessionUser.UserLogin.Key;
                zinfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
                if (zinfo.Key == 0)
                    zinfo.Create();
                else
                    zinfo.Update();
                zMessage = TN_Message.Show(zinfo.Message);
                if (zMessage == "")
                {
                    // vừa tạo 1 mã --> mã mới
                    if (Time_Rice_Data.CountID(zinfo.RiceID) == 1)
                    {

                        bool zResult = false;
                        zResult = InsertRice_HRM_CodeReport(zinfo.RiceID, zinfo.Slug); //Slug: loại cơm cộng hay trử

                        if (zResult == true )
                        {
                            Utils.TNMessageBoxOK("Cập nhật thành công!", 3);
                            DisplayData();
                            _Key = 0;
                            LoadData();
                        }
                        else
                        {
                            Utils.TNMessageBoxOK("Lỗi tạo cột tự động.Vui lòng liên hệ IT!", 4);
                        }    
                    }
                  
                }
                else
                {
                    Utils.TNMessageBoxOK(zMessage, 4);
                }
            }


        }
        private void Btn_Del_Click(object sender, EventArgs e)
        {
            string zMessage = "";
            if (_Key == 0)
            {
                Utils.TNMessageBoxOK("Chưa chọn thông tin!", 1);
            }
            else
            {
                if (Utils.TNMessageBox("Bạn có chắc xóa thông tin này ?.", 2) == "Y")
                {
                    Time_Rice_Info zinfo = new Time_Rice_Info(_Key);

                    zinfo.ModifiedBy = SessionUser.UserLogin.Key;
                    zinfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
                    zinfo.Delete();
                    zMessage = TN_Message.Show(zinfo.Message);
                    if (zMessage == "")
                    {
                        if (Time_Rice_Data.CountID(zinfo.RiceID) == 0)
                        {
                            bool zResult = false;
                            zResult=  DeleteRice_HRM_CodeReport(zinfo.RiceID); // bảng cơm lun
                            if (zResult == true)
                            {
                                Utils.TNMessageBoxOK("Xóa thành công!", 3);
                                DisplayData();
                                _Key = 0;
                                LoadData();
                            }
                            else
                            {
                                Utils.TNMessageBoxOK("Lỗi xóa cột tự động.Vui lòng liên hệ IT!", 4);
                            }    
                        }
                        else
                        {
                            Utils.TNMessageBoxOK("Xóa thành công!", 3);
                            DisplayData();
                            _Key = 0;
                            LoadData();
                        }
                    }
                    else
                    {
                        Utils.TNMessageBoxOK(zMessage, 4);
                    }

                }
            }

        }
        private void Btn_New_Click(object sender, EventArgs e)
        {
            _Key = 0;
            LoadData();
            lbl_Created.Text = "Tạo bởi:";
            lbl_Modified.Text = "Chỉnh sửa:";
            txt_RiceID.Focus();
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {

            DisplayData();
        }
        private void Btn_AddCategory_Click(object sender, EventArgs e)
        {
            LoadDataToToolbox.KryptonComboBox(cbo_Category, "SELECT CategoryKey,CategoryName FROM HRM_Category_Rice WHERE  RecordStatus < 99", "");
            LoadData();
        }
        #endregion

        #region[ListView]
        private void DisplayData()
        {

            DataTable ztb = Time_Rice_Data.Search(txt_Search.Text.Trim());
            InitGV_Layout(ztb);
        }
        private void GVData_List_Click(object sender, EventArgs e)
        {
            if (GVData.Rows.Count > 0)
            {
                string zKey = GVData.Rows[GVData.RowSel][7].ToString();
                if (zKey != null || zKey != "")
                {
                    _Key = int.Parse(zKey);
                    LoadData();
                }
                else
                {
                    _Key = 0;
                    LoadData();
                }
            }
        }
        private void Btn_Export_Click(object sender, EventArgs e)
        {
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Danh sách mã cơm.xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }
                    GVData.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }

        }
        void InitGV_Layout(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            int TotalRow = TableView.Rows.Count + 1;
            int ToTalCol = 8;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(TotalRow);

            //Row Header
            GVData.Rows[0][0] = "Stt";
            GVData.Rows[0][1] = "Mã";
            GVData.Rows[0][2] = "Tên";
            GVData.Rows[0][3] = "Ngày áp dụng";
            GVData.Rows[0][4] = "Ngày hết hạn";
            GVData.Rows[0][5] = "Số tiền";
            GVData.Rows[0][6] = "Ghi chú";
            GVData.Rows[0][7] = ""; // ẩn cột này

            //Style         
            //GVData.AllowFreezing = AllowFreezingEnum.Both;
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.Custom;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.LeftCenter;
            GVData.Styles.Normal.WordWrap = true;

            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                DataRow rData = TableView.Rows[rIndex];

                GVData.Rows[rIndex + 1][0] = rIndex + 1;
                GVData.Rows[rIndex + 1][1] = rData[1];
                GVData.Rows[rIndex + 1][2] = rData[2];
                GVData.Rows[rIndex + 1][3] = rData[3];
                GVData.Rows[rIndex + 1][4] = rData[4];
                GVData.Rows[rIndex + 1][5] = rData[5].Toe0String();
                GVData.Rows[rIndex + 1][6] = rData[6];
                GVData.Rows[rIndex + 1][7] = rData[0];
                if(rData[4].ToString()!="")
                {
                    GVData.Rows[rIndex + 1].StyleNew.BackColor = Color.DarkGray;
                }    

            }



            //Freeze Row and Column                              
            GVData.Rows.Fixed = 1;
            GVData.Rows[0].TextAlign = TextAlignEnum.CenterCenter;
            GVData.Rows[0].Height = 50;

            GVData.Cols[0].Width = 30;
            GVData.Cols[1].Width = 80;
            GVData.Cols[2].Width = 200;
            GVData.Cols[3].Width = 100;
            GVData.Cols[4].Width = 100;
            GVData.Cols[5].Width = 100;
            GVData.Cols[6].Width = 100;
            GVData.Cols[7].Visible = false;

            GVData.Cols[5].TextAlign = TextAlignEnum.RightCenter;
        }
        #endregion

        private bool InsertRice_HRM_CodeReport(string RiceID,int Type)
        {

            bool zResult = false;

            if (Type == 0) // cơm cộng
            {

                //zResult = Time_Rice_Data.Add_RicePlus(RiceID);
            }
            else
            {
                
                //zResult = Time_Rice_Data.Add_RicePlus(RiceID);
            }
            return zResult;
        }
        private bool DeleteRice_HRM_CodeReport(string RiceID)
        {

            bool zResult = false;

            //zResult = Time_Rice_Data.Delete_Rice_HRM(RiceID);
            return zResult;
        }

        private void txt_Rank_KeyPress(object sender, KeyPressEventArgs e)
        {

            if ((e.KeyChar >= '0' && e.KeyChar <= '9') || (Keys)e.KeyChar == Keys.Back || e.KeyChar == 46 || e.KeyChar == 44)
            {

                e.Handled = false;

            }
            else
            {
                e.Handled = true;
            }

        }
        private void txt_Money_KeyPress(object sender, KeyPressEventArgs e)
        {

            if ((e.KeyChar >= '0' && e.KeyChar <= '9') || (Keys)e.KeyChar == Keys.Back || e.KeyChar == 46 || e.KeyChar == 44)
            {

                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }

        }
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                //this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
                btn_Export.Enabled = false;
            }
            if (Role.RoleAdd == 0 && Role.RoleEdit == 0)
            {
                btn_New.Enabled = false;
                btn_Save.Enabled = false;
            }
            if (Role.RoleAdd == 0 && Role.RoleEdit == 1)
            {
                btn_New.Enabled = false;
                btn_Save.Enabled = true;
            }
            if (Role.RoleDel == 0)
            {
                btn_Del.Enabled = false;
            }
        }
        #endregion
    }
}
