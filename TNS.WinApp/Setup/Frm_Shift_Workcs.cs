﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TN.Library.Miscellaneous;
using TN.Library.SystemManagement;

namespace TN_WinApp
{
    public partial class Frm_Shift_Workcs : Form
    {
        #region[Private]
        private int _Key = 0;
        DataTable ztb_Active = new DataTable();
        #endregion
        public Frm_Shift_Workcs()
        {
            InitializeComponent();
            btn_Save.Click += Btn_Save_Click;
            btn_New.Click += Btn_New_Click;
            btn_Del.Click += Btn_Del_Click;
            btn_Search.Click += Btn_Search_Click;
            LV_List.Click += LV_List_Click;
        }
       
        private void Frm_Shift_Workcs_Load(object sender, EventArgs e)
        {

            LoadTableActive();
            LoadDataToToolbox.ComboBoxData(cbo_Active, ztb_Active, false, 0, 1);
            LV_Layout(LV_List);
            LV_LoadData();
            SetDefault();
        }
        #region[Progess]
        void LoadTableActive()
        {
            ztb_Active = new DataTable();
            ztb_Active.Columns.Add("Value");
            ztb_Active.Columns.Add("Name");
            ztb_Active.Rows.Add("1", "Kích hoạt");
            ztb_Active.Rows.Add("2", "Không kích hoạt");
        }
        private void LoadData()
        {

            Shift_Work_Info zinfo = new Shift_Work_Info(_Key);
            txt_ShiftID.Text = zinfo.ShiftID;
            txt_ShiftName.Text = zinfo.ShiftName;
            dte_BeginTime.Text = zinfo.BeginTime.ToString();
            dte_EndTime.Text = zinfo.EndTime.ToString();
            cbo_Active.SelectedValue = zinfo.Active.ToString();
            txt_Rank.Text = zinfo.Rank.ToString();
            txt_Description.Text = zinfo.Description;
        }

        private void SetDefault()
        {
            _Key = 0;
            txt_ShiftID.Text = "";
            txt_ShiftName.Text = "";
            cbo_Active.SelectedIndex = 0;
            dte_BeginTime.Text = "00:00:00";
            dte_EndTime.Text = "00:00:00";
            txt_Rank.Text = "0";
            txt_Description.Text = "";
            txt_ShiftID.Focus();
        }

        #endregion

        #region[Event]
        private void Btn_Save_Click(object sender, EventArgs e)
        {
            if (txt_ShiftID.Text.Trim().Length == 0)
            {
                MessageBox.Show("Chưa nhập mã!");
                return;
            }
            if (txt_ShiftName.Text.Trim().Length == 0)
            {
                MessageBox.Show("Chưa nhập tên ca làm việc!");
                return;
            }
            int zRank;
            if (!int.TryParse(txt_Rank.Text, out zRank))
            {
                MessageBox.Show("Vui lòng nhập đúng định dạng số!");
                return;
            }
            else
            {
                Shift_Work_Info zinfo = new Shift_Work_Info(_Key);
                zinfo.ShiftID = txt_ShiftID.Text.Trim().ToUpper();
                zinfo.ShiftName = txt_ShiftName.Text.Trim();
                zinfo.Active = cbo_Active.SelectedValue.ToInt();
                zinfo.BeginTime = TimeSpan.Parse(dte_BeginTime.Text);
                zinfo.EndTime = TimeSpan.Parse(dte_EndTime.Text);
                zinfo.Rank = int.Parse(txt_Rank.Text.Trim());
                zinfo.Description = txt_Description.Text.Trim();
                zinfo.Save();
                if (zinfo.Message ==string.Empty)
                {
                    MessageBox.Show("Cập nhật thành công!");
                    LV_LoadData();
                    SetDefault();
                }
                else
                {
                    MessageBox.Show("Cập nhật không thành công!Vui lòng kiểm tra lại!");
                }
            }


        }

        private void Btn_Del_Click(object sender, EventArgs e)
        {
            if (_Key == 0)
            {
                MessageBox.Show("Chưa chọn thông tin!");
            }
            else
            {
                if (MessageBox.Show("Bạn có chắc xóa thông tin này ?.", "Thông báo", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    Shift_Work_Info zinfo = new Shift_Work_Info(_Key);
                    zinfo.Delete();
                    if (zinfo.Message == string.Empty)
                    {
                        MessageBox.Show("Đã xóa !");
                        LV_LoadData();
                        SetDefault();
                    }
                    else
                    {
                        MessageBox.Show("Xóa không thành công!Vui lòng kiểm tra lại!");
                    }

                }
            }

        }


        private void Btn_New_Click(object sender, EventArgs e)
        {
            SetDefault();
        }

        private void Btn_Search_Click(object sender, EventArgs e)
        {

            LV_LoadData();
        }
        #endregion


        #region[ListView]
        private void LV_Layout(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "No";
            colHead.Width = 30;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên ca";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            //colHead = new ColumnHeader();
            //colHead.Text = "Giờ bắt đầu";
            //colHead.Width = 100;
            //colHead.TextAlign = HorizontalAlignment.Right;
            //LV.Columns.Add(colHead);

            //colHead = new ColumnHeader();
            //colHead.Text = "Giờ kết thúc";
            //colHead.Width = 100;
            //colHead.TextAlign = HorizontalAlignment.Right;
            //LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tình trạng";
            colHead.Width = 95;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            //colHead = new ColumnHeader();
            //colHead.Text = "Diễn giải";
            //colHead.Width = 170;
            //colHead.TextAlign = HorizontalAlignment.Left;
            //LV.Columns.Add(colHead);

            //colHead = new ColumnHeader();
            //colHead.Text = "Sắp xếp ";
            //colHead.Width = 100;
            //colHead.TextAlign = HorizontalAlignment.Right;
            //LV.Columns.Add(colHead);
        }
        private void LV_LoadData()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LV_List;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            LV.Items.Clear();
            DataTable ztb = Shift_Work_Data.Search(txt_Search.Text.Trim());
            for (int i = 0; i < ztb.Rows.Count; i++)
            {
                DataRow nRow = ztb.Rows[i];

                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.Tag = nRow["ShiftKey"];
                lvi.ForeColor = Color.Navy;

                lvi.BackColor = Color.White;
                lvi.ImageIndex = 0;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["ShiftID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["ShiftName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                //lvsi = new ListViewItem.ListViewSubItem();
                //lvsi.Text = nRow["BeginTime"].ToString().Trim();
                //lvi.SubItems.Add(lvsi);

                //lvsi = new ListViewItem.ListViewSubItem();
                //lvsi.Text = nRow["EndTime"].ToString().Trim();
                //lvi.SubItems.Add(lvsi);

                int zActive = int.Parse(nRow["Active"].ToString().Trim());
                if (zActive == 1)
                {
                    lvsi = new ListViewItem.ListViewSubItem();
                    lvsi.Text = "Kích hoạt";
                    lvi.SubItems.Add(lvsi);
                }
                if (zActive == 2)
                {
                    lvsi = new ListViewItem.ListViewSubItem();
                    lvsi.Text = "Chưa kích hoạt";
                    lvi.SubItems.Add(lvsi);
                }

                //lvsi = new ListViewItem.ListViewSubItem();
                //lvsi.Text = nRow["Description"].ToString().Trim();
                //lvi.SubItems.Add(lvsi);

                //lvsi = new ListViewItem.ListViewSubItem();
                //lvsi.Text = nRow["Rank"].ToString().Trim();
                //lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }

            this.Cursor = Cursors.Default;

        }
        private void LV_List_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < LV_List.Items.Count; i++)
            {
                if (LV_List.Items[i].Selected == true)
                {
                    LV_List.Items[i].BackColor = Color.LightBlue; // highlighted item
                }
                else
                {
                    LV_List.Items[i].BackColor = SystemColors.Window; // normal item
                }
            }
            _Key = int.Parse(LV_List.SelectedItems[0].Tag.ToString());
            LoadData();
        }
        #endregion

    }
}
