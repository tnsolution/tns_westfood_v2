﻿using OfficeOpenXml;
using System;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using TNS.IVT;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Setup_Work : Form
    {
        public int TeamKey;
        public bool SearchStyle;
        public Product_Stages_Info Product_Stages;
        private DataTable zTable;
        private int _AutoKey = 0;
        private int _GroupKey = 0;

        public Frm_Setup_Work()
        {
            InitializeComponent();

            Utils.DoubleBuffered(GVData, true);
            Utils.DrawGVStyle(ref GVData);

            InitLayout_GV();

            GVData.KeyDown += GVData_KeyDown;
            GVData.CellContentDoubleClick += GVData_CellContentDoubleClick;
            GVData.CellEndEdit += GVData_CellEndEdit;

            btn_Save.Click += Btn_Save_Click;
            btn_Import.Click += Btn_Import_Click;
            btn_Search.Click += Btn_Search_Click;
            btn_Select.Click += Btn_Select_Click;
            btn_Export.Click += Btn_Export_Click;
        }


        private void GVData_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow zRowEdit = GVData.Rows[e.RowIndex];
            bool zIsChangeValued = false;
            switch (e.ColumnIndex)
            {
                case 1:
                    if (zRowEdit.Cells[e.ColumnIndex].Value == null)
                    {
                        Utils.TNMessageBoxOK("Mã công việc không được rỗng !.",1);
                    }
                    else
                    {
                        string ID = zRowEdit.Cells[e.ColumnIndex].Value.ToString();
                        Product_Stages = new Product_Stages_Info();
                        Product_Stages.GetStagesID_Info(ID);
                        if (Product_Stages.Key != 0)
                        {
                            Utils.TNMessageBoxOK("Mã công việc này đã có rồi !.", 1);
                        }
                    }
                    zIsChangeValued = true;
                    break;
                case 2:
                    if (zRowEdit.Cells[e.ColumnIndex].Value == null)
                    {
                        Utils.TNMessageBoxOK("Tên công việc không được rỗng !.", 1);
                    }
                    zIsChangeValued = true;
                    break;
                case 3:
                    if (zRowEdit.Cells[e.ColumnIndex].Value == null)
                    {
                        Utils.TNMessageBoxOK("Đơn giá công việc không được rỗng !.", 1);
                    }
                    else
                    {
                        double zAmount = 0;
                        double.TryParse(zRowEdit.Cells[e.ColumnIndex].Value.ToString().Trim(), out zAmount);
                        zRowEdit.Cells[e.ColumnIndex].Value = zAmount.Ton1String();
                    }
                    zIsChangeValued = true;
                    break;
                case 4:
                    if (zRowEdit.Cells[e.ColumnIndex].Value == null)
                    {
                        Utils.TNMessageBoxOK("Đơn vị tính công việc không được rỗng !.",1);
                    }
                    zIsChangeValued = true;
                    break;
                case 5:
                    zIsChangeValued = true;
                    break;
                case 6:
                    zIsChangeValued = true;
                    break;

                case 7:
                    zIsChangeValued = true;
                    break;
                case 8:
                    if (zRowEdit.Cells[e.ColumnIndex].Value == null|| zRowEdit.Cells[e.ColumnIndex].Value.ToString() == "0")
                    {
                        Utils.TNMessageBoxOK("Nhóm sản xuất không được trống !.", 1);
                    }
                    zIsChangeValued = true;
                    break;
                case 9:
                    {
                        if(zRowEdit.Cells[e.ColumnIndex].Value == null)
                        {
                            zRowEdit.Cells[e.ColumnIndex].Value = "";
                        }
                        zIsChangeValued = true;

                    }
                    break;
            }
            if (zIsChangeValued)
            {
                zRowEdit.Cells[0].Tag = 0;
            }
        }

        private void Frm_Stages_Work_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);

            string zSQL = @"SELECT TeamKey,TeamID +'-'+ TeamName AS TeamName  FROM SYS_Team 
                            WHERE RecordStatus <> 99 AND BranchKey=4 AND  DepartmentKey != 26
                            ORDER BY Rank";
            LoadDataToToolbox.KryptonComboBox(cbo_Team, zSQL, "---Tất cả---");
            LoadDataToToolbox.KryptonComboBox(cbo_GroupStage, "SELECT GroupKey,GroupName FROM IVT_Product_Stages_Group WHERE RecordStatus <> 99", "---Tất cả---");
            if (SearchStyle)
            {
                this.Width = 1000;
                this.Height = 600;
                this.Location = new Point(121, 167);

                GVData.ReadOnly = true;
                btnMini.Visible = false;
                btnMax.Visible = false;
                btn_Select.Visible = true;
                btn_Save.Visible = false;
                btn_Import.Visible = false;
                cbo_Team.SelectedValue = TeamKey;
                ReadData();
                //InitData_GV(TeamKey, string.Empty);

                GVData.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            }
            else
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                Check_RoleForm();
            }
        }

        #region [Process Data]


        private void SaveGV()
        {
            string zMessage = "";
            int n = GVData.Rows.Count;

            Product_Stages = new Product_Stages_Info();
            for (int i = 0; i < n - 1; i++)
            {
                if (GVData.Rows[i].Cells["No"].Tag != null &&
                    GVData.Rows[i].Cells["StagesID"].Value != null)
                {
                    if (GVData.Rows[i].Tag == null)
                    {
                        _AutoKey = 0;
                    }
                    else
                    {
                        _AutoKey = int.Parse(GVData.Rows[i].Tag.ToString());
                    }
                    Product_Stages.Key = _AutoKey;
                    Product_Stages.StageName = GVData.Rows[i].Cells["StageName"].Value.ToString();
                    Product_Stages.StagesID = GVData.Rows[i].Cells["StagesID"].Value.ToString();
                    Product_Stages.UnitName = GVData.Rows[i].Cells["Unit"].Value.ToString();
                    Product_Unit_Info zUnit = new Product_Unit_Info(Product_Stages.UnitName);
                    Product_Stages.BasicUnit = zUnit.Key;
                    Product_Stages.Price = float.Parse(GVData.Rows[i].Cells["Price"].Value.ToString());
                    Product_Stages.Description= GVData.Rows[i].Cells["Description"].Value.ToString().Trim();

                    Product_Stages.CreatedName = SessionUser.UserLogin.EmployeeName;
                    Product_Stages.CreatedBy = SessionUser.UserLogin.Key;
                    Product_Stages.ModifiedName = SessionUser.UserLogin.EmployeeName;
                    Product_Stages.ModifiedBy = SessionUser.UserLogin.Key;

                    if (GVData.Rows[i].Cells["Slug"].Value != null)
                    {
                        if (Convert.ToBoolean(GVData.Rows[i].Cells["Slug"].Value) == true)
                        {
                            Product_Stages.Slug = 1;
                        }
                        else
                        {
                            Product_Stages.Slug = 0;
                        }
                    }

                    if (GVData.Rows[i].Cells["IsWorkTime"].Value != null)
                    {
                        if (Convert.ToBoolean(GVData.Rows[i].Cells["IsWorkTime"].Value) == true)
                        {
                            Product_Stages.IsWorkTime = 1;
                        }
                        else
                        {
                            Product_Stages.IsWorkTime = 0;
                        }
                    }

                    if (GVData.Rows[i].Cells["cboCategory"].Value != null)
                    {
                        Product_Stages.GroupKey = GVData.Rows[i].Cells["cboCategory"].Value.ToInt();
                    }
                    if (GVData.Rows[i].Cells["cboTeamDetail"].Value.ToString() != null)
                    {
                        Product_Stages.TeamKey = GVData.Rows[i].Cells["cboTeamDetail"].Value.ToInt();
                    }

                    Product_Stages.Save();
                    if (Product_Stages.Message != "11" &&
                        Product_Stages.Message != "20")
                    {
                        zMessage += Product_Stages.Message;
                    }
                }
            }
            if (zMessage.Length > 0)
            {
                Utils.TNMessageBoxOK(zMessage, 4);
            }
            else
            {
               Utils.TNMessageBoxOK("Cập nhật thành công! Nhấn Tìm kiếm để tải lại danh sách",3);
                //InitData_GV(_Catergory, txt_Search.Text.Trim());
                //ReadData();
            }
        }
        #endregion

        #region [Design Layout]
        private void InitLayout_GV()
        {
            DataGridViewCheckBoxColumn zColumn;
            // Setup Column 
            GVData.Columns.Add("No", "STT");
            GVData.Columns.Add("StagesID", "Mã");
            GVData.Columns.Add("StageName", "Tên Công Việc");
            GVData.Columns.Add("Price", "Đơn Giá");
            GVData.Columns.Add("Unit", "Đơn vị tính");

            zColumn = new DataGridViewCheckBoxColumn();
            zColumn.Name = "Slug";
            zColumn.HeaderText = "TG cho Nhóm";
            GVData.Columns.Add(zColumn);

            zColumn = new DataGridViewCheckBoxColumn();
            zColumn.Name = "IsWorkTime";
            zColumn.HeaderText = "Tính chuyên cần";
            GVData.Columns.Add(zColumn);

            DataGridViewComboBoxColumn cboCategory = new DataGridViewComboBoxColumn();
            cboCategory.HeaderText = "Nhóm công việc";
            cboCategory.Name = "cboCategory";
            LoadDataToToolbox.ComboBoxData(cboCategory, " SELECT GroupKey, GroupID FROM IVT_Product_Stages_Group WHERE RecordStatus <> 99", true);
            cboCategory.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
            GVData.Columns.Add(cboCategory);

            DataGridViewComboBoxColumn cboTeamDetail = new DataGridViewComboBoxColumn();
            cboTeamDetail.HeaderText = "Nhóm sản xuất";
            cboTeamDetail.Name = "cboTeamDetail";
            LoadDataToToolbox.ComboBoxData(cboTeamDetail, " SELECT TeamKey, TeamID FROM SYS_Team WHERE RecordStatus <> 99 AND BranchKey=4 ", true);
            cboTeamDetail.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
            GVData.Columns.Add(cboTeamDetail);

            GVData.Columns.Add("Description", "Ghi chú");

            GVData.Columns["No"].Width = 40;
            GVData.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GVData.Columns["StageName"].Width = 350;
            GVData.Columns["StageName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GVData.Columns["StageName"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GVData.Columns["StagesID"].Width = 100;
            GVData.Columns["StagesID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GVData.Columns["Unit"].Width = 100;
            GVData.Columns["Unit"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GVData.Columns["Price"].Width = 100;
            GVData.Columns["Price"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GVData.Columns["Slug"].Width = 100;
            GVData.Columns["Slug"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GVData.Columns["IsWorkTime"].Width = 100;
            GVData.Columns["IsWorkTime"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GVData.Columns["Description"].Width = 200;
            GVData.Columns["Description"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GVData.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.DisplayedCells;
            GVData.ColumnHeadersHeight = 45;
        }
        private void InitData_GV(DataTable zTable)
        {
            GVData.Rows.Clear();

            int i = 0;
            foreach (DataRow nRow in zTable.Rows)
            {
                GVData.Rows.Add();
                DataGridViewRow nRowView = GVData.Rows[i];
                nRowView.Tag = nRow["StageKey"].ToString();
                nRowView.Cells["No"].Value = (i + 1).ToString();
                nRowView.Cells["StageName"].Value = nRow["StageName"].ToString().Trim();
                nRowView.Cells["StagesID"].Value = nRow["StagesID"].ToString().Trim();
                nRowView.Cells["Unit"].Value = nRow["UnitName"].ToString().Trim();
                float zPrice = float.Parse(nRow["Price"].ToString());
                nRowView.Cells["Price"].Value = zPrice.ToString("n1");

                if (nRow["Slug"].ToString() == "1")
                    nRowView.Cells["Slug"].Value = true;

                if (nRow["GroupKey"] != null)
                    nRowView.Cells["cboCategory"].Value = nRow["GroupKey"].ToInt();

                if (nRow["IsWorkTime"].ToString() == "1")
                    nRowView.Cells["IsWorkTime"].Value = true;

                if (nRow["TeamKey"] != null)
                    nRowView.Cells["cboTeamDetail"].Value = nRow["TeamKey"].ToInt();
                nRowView.Cells["Description"].Value = nRow["Description"].ToString().Trim();
                i++;
            }
        }
        #endregion

        private void Btn_Import_Click(object sender, EventArgs e)
        {
           
                //TeamKey = int.Parse(cbo_Team.SelectedValue.ToString());
                Frm_Import_CongDoan frm = new Frm_Import_CongDoan();
                frm.ShowDialog();
                //ReadData();//InitData_GV(TeamKey, txt_Search.Text.Trim());
            
        }

        //private void LVData_KeyDown(object sender, KeyEventArgs e)
        //{
        //    if (e.KeyData == Keys.Delete)
        //    {
        //        Product_Stages_Info Product_Stages = new Product_Stages_Info(_Catergory);
        //        Product_Stages.ModifiedBy = SessionUser.UserLogin.Key;
        //        Product_Stages.ModifiedName = SessionUser.UserLogin.EmployeeName;
        //        Product_Stages.Delete();
        //        if (Product_Stages.Message.Length > 0)
        //        {
        //            MessageBox.Show("Vui lòng kiểm tra lại", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        }
        //        else
        //        {
        //            MessageBox.Show("Cập nhật thành công", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
        //            InitData_LV();
        //        }
        //    }
        //}
        //private void btn_Add_Click(object sender, EventArgs e)
        //{
        //}
        private void Btn_Save_Click(object sender, EventArgs e)
        {
            if (GVData.Rows.Count > 1 )
            {
                string zMessage = CheckBeforSave();
                if (zMessage == "")
                    SaveGV();
                else
                    Utils.TNMessageBoxOK(zMessage,4);
            }
            else
            {
               Utils.TNMessageBoxOK("Không tìm thấy dữ liệu !",1);
            }
        }
        private string CheckBeforSave()
        {
            string zMesage = "";
            for (int i = 0; i < GVData.Rows.Count - 1; i++)
            {
                if (GVData.Rows[i].Cells["No"].Tag != null && GVData.Rows[i].Cells["StagesID"].Value != null)
                {
                    if (GVData.Rows[i].Tag == null)
                    {
                        string ID = GVData.Rows[i].Cells["StagesID"].Value.ToString();
                        Product_Stages = new Product_Stages_Info();
                        Product_Stages.GetStagesID_Info(ID);
                        if (Product_Stages.Key != 0)
                        {
                            zMesage += "Mã công việc [" + ID + "] đã tồn tại.\n";
                        }
                        if (GVData.Rows[i].Cells["StageName"].Value == null)
                        {
                            zMesage += "Chưa nhập tên công việc. \n";
                        }
                        if (GVData.Rows[i].Cells["Unit"].Value == null)
                        {
                            zMesage += "Chưa nhập đơn vị. \n";
                        }
                        else
                        {
                            Product_Unit_Info zUnit = new Product_Unit_Info(GVData.Rows[i].Cells["Unit"].Value.ToString());
                            if (zUnit.Key == 0)
                            {
                                zMesage += "Đơn vị tính không đúng. \n";
                            }
                        }
                        if (GVData.Rows[i].Cells["Price"].Value == null)
                        {
                            zMesage += "Chưa nhập đơn giá. \n";
                        }
                        if (GVData.Rows[i].Cells["cboTeamDetail"].Value.ToString() == "0")
                        {
                            zMesage += "Chưa chọn nhóm sản xuất. \n";
                        }
                        if (GVData.Rows[i].Cells["Description"].Value == null)
                        {
                            GVData.Rows[i].Cells["Description"].Value = "";
                        }
                    }
                }
            }
            return zMesage;
        }
        private void GVData_KeyDown(object sender, KeyEventArgs e)
        {
            if (Role.RoleDel == 1)
            {
                if (e.KeyData == Keys.Delete)
                {
                    if (Utils.TNMessageBox("Bạn có xóa thông tin này ?",2)=="Y")
                    {
                        foreach (DataGridViewRow nRow in GVData.SelectedRows)
                        {
                            if (GVData.CurrentRow.Tag != null)
                            {
                                int CategoryKey = int.Parse(GVData.CurrentRow.Tag.ToString());
                                Product_Stages = new Product_Stages_Info();
                                Product_Stages.Key = CategoryKey;
                                Product_Stages.ModifiedBy = SessionUser.UserLogin.Key;
                                Product_Stages.ModifiedName = SessionUser.UserLogin.EmployeeName;
                                Product_Stages.Delete();
                                //InitData_GV(TeamKey, txt_Search.Text.Trim());
                                ReadData();
                            }
                            else
                            {
                                GVData.Rows.RemoveAt(GVData.CurrentRow.Index);
                                //InitData_GV(TeamKey, txt_Search.Text.Trim());
                                ReadData();
                            }
                        }
                    }
                }

                if (e.KeyCode == Keys.Delete && e.Control)
                {
                    if (Utils.TNMessageBox("Bạn có xóa thông tin này ?", 2) == "Y")
                    {
                        foreach (DataGridViewRow nRow in GVData.SelectedRows)
                        {
                            if (GVData.CurrentRow.Tag != null)
                            {
                                int TeamKey = int.Parse(GVData.CurrentRow.Tag.ToString());
                                Product_Stages = new Product_Stages_Info();
                                Product_Stages.ModifiedBy = SessionUser.UserLogin.Key;
                                Product_Stages.ModifiedName = SessionUser.UserLogin.EmployeeName;
                                Product_Stages.Delete(TeamKey);
                                //InitData_GV(TeamKey, txt_Search.Text.Trim());
                                ReadData();
                            }
                            else
                            {
                                GVData.Rows.RemoveAt(GVData.CurrentRow.Index);
                                //InitData_GV(T, txt_Search.Text.Trim());
                                ReadData();
                            }
                        }
                    }
                }
            }
            else
            {
                Utils.TNMessageBoxOK("Bạn chưa được phân quyền xóa!", 1);
            }
        }
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                //this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        private void Btn_Search_Click(object sender, EventArgs e)
        {
            GVData.Rows.Clear();
            TeamKey = int.Parse(cbo_Team.SelectedValue.ToString());

            string Status = "Tìm DL form " + HeaderControl.Text + " > Nhóm:" + cbo_Team.Text + " > Từ khóa:" + txt_Search.Text;
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);
            _GroupKey = cbo_GroupStage.SelectedValue.ToInt();
            ReadData();


        }
        private void ReadData()
        {
            lbl_Name.Text = "CÁC CÔNG VIỆC CỦA CÔNG ĐOẠN " + ": " + cbo_Team.Text;
            try
            {
                using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }
            }
            catch (Exception ex)
            {
                Utils.TNMessageBoxOK( ex.ToString(), 4);
            }
        }
        private void DisplayData()
        {
            DataTable _Intable = Product_Stages_Data.List(txt_Search.Text, TeamKey,_GroupKey);
            if (_Intable.Rows.Count > 0)
            {
                this.Invoke(new MethodInvoker(delegate ()
                {
                    InitData_GV(_Intable);
                }));
            }
        }
        private void Btn_Select_Click(object sender, EventArgs e)
        {
            int Key = GVData.CurrentRow.Tag.ToInt();
            Product_Stages = new Product_Stages_Info(Key);
            if (Product_Stages != null)
                this.Close();
            else
                Utils.TNMessageBoxOK("Bạn phải chọn 1 công việc !.",1);
        }
        private void GVData_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int Key = GVData.CurrentRow.Tag.ToInt();
            Product_Stages = new Product_Stages_Info(Key);
            if (Product_Stages == null)
                Utils.TNMessageBoxOK("Bạn phải chọn 1 công việc !.",1);
            else
                this.Close();
        }
        #region [ Get Auth-Action-Chg_Lang ]
        Access_Role_Info Role;
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

             Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
                btn_Export.Enabled = false;
            }
            if (Role.RoleAdd == 0 && Role.RoleEdit == 0)
            {
                //bt.Enabled = false;
                btn_Save.Enabled = false;
            }
            if (Role.RoleAdd == 0 && Role.RoleEdit == 1)
            {
                //btn_New.Enabled = false;
                btn_Save.Enabled = true;
            }
            if (Role.RoleDel == 0)
            {
                //btn_Del.Enabled = false;
            }
        }
        #endregion

        private void Btn_Export_Click(object sender, EventArgs e)
        {
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Danh sách Công đoạn.xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                Path = FDialog.FileName;
                DataTable dt = new DataTable();
                foreach (DataGridViewColumn col in GVData.Columns)
                {
                    dt.Columns.Add(col.HeaderText);
                }

                for (int i=0; i<GVData.RowCount-1;i++)
                {
                    DataGridViewRow row = GVData.Rows[i];
                    DataRow dRow = dt.NewRow();

                    int k = 0;
                    foreach (DataGridViewCell cell in row.Cells)
                    {
                        if (k == 5)
                        {
                            if (cell.Value == null)
                                dRow[cell.ColumnIndex] = 0;
                            else if ((bool)cell.Value == true)
                                dRow[cell.ColumnIndex] = 1;
                            else
                                dRow[cell.ColumnIndex] = 0;
                        }
                        else if (k==6)
                        {
                            if (cell.Value == null)
                                dRow[cell.ColumnIndex] = 0;
                            else if((bool)cell.Value==true)
                                dRow[cell.ColumnIndex] = 1;
                            else
                                dRow[cell.ColumnIndex] = 0;
                        }
                        else if (k == 7)
                        {
                            int Key = cell.Value.ToInt();
                            Product_Stages_Group_Info zgroup = new Product_Stages_Group_Info(Key);
                            dRow[cell.ColumnIndex] = zgroup.GroupID;
                        }
                        else if (k == 8)
                        {
                            int Key = cell.Value.ToInt();
                            Team_Info zTeam = new Team_Info(Key);
                            dRow[cell.ColumnIndex] = zTeam.TeamID;
                        }
                        else
                            dRow[cell.ColumnIndex] = cell.Value;
                        k++;
                    }
                    dt.Rows.Add(dRow);
                }
                string Message = ExportTableToExcel(dt, Path);
                if (Message != "OK")
                    Utils.TNMessageBoxOK(Message,4);
                else
                {
                    Message = "Đã tạo tập tin thành công !." + Environment.NewLine + "Bạn có muốn mở thư mục chứa ?.";
                    if (Utils.TNMessageBox(Message,3) == "Y")
                    {
                        Process.Start(Path);
                    }
                }
            }
        }
       
        protected virtual bool IsFileLocked(FileInfo file)
        {
            try
            {
                using (FileStream stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    stream.Close();
                }
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }

            //file is not locked
            return false;
        }
        private string ExportTableToExcel(DataTable zTable, string Folder)
        {
            try
            {
                var newFile = new FileInfo(Folder);
                if (newFile.Exists)
                {
                    if (!IsFileLocked(newFile))
                    {
                        newFile.Delete();
                    }
                    else
                    {
                        return "Tập tin đang sử dụng !.";
                    }
                }

                using (var package = new ExcelPackage(newFile))
                {
                    //Tạo Sheet 1 mới với tên Sheet là DonHang
                    ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("PhanBo");
                    worksheet.Cells["A1"].LoadFromDataTable(zTable, true);
                    worksheet.Cells.Style.Font.Name = "Times New Roman";
                    worksheet.Cells.Style.Font.Size = 12;
                    worksheet.Cells.AutoFilter = true;
                    worksheet.Cells.AutoFitColumns();
                    worksheet.View.FreezePanes(2, 4);

                    //Rowheader và row tổng
                    worksheet.Row(1).Height = 60;
                    worksheet.Row(1).Style.WrapText = true;
                    worksheet.Row(1).Style.Font.Bold = true;
                    worksheet.Row(1).Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                    worksheet.Row(1).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    //worksheet.Row(zTable.Rows.Count + 1).Style.Font.Bold = true;//dòng tổng
                    //Chỉnh fix độ rộng cột
                    for (int j = 4; j <= zTable.Columns.Count; j++)
                    {
                        worksheet.Column(j).Width = 20;
                    }
                    worksheet.Column(1).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Column(1).Width = 5;
                    worksheet.Column(2).Width = 20;
                    worksheet.Column(3).Width = 40;
                    worksheet.Column(8).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    worksheet.Column(9).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    //worksheet.Column(zTable.Columns.Count).Style.Font.Bold = true;//côt tổng
                    //Canh phải số
                    for (int i = 2; i <=zTable.Rows.Count+1; i++)
                    {
                        for (int j = 4; j < zTable.Columns.Count; j++)
                        {
                            worksheet.Cells[i, j].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                        }
                    }
                    worksheet.Column(8).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    worksheet.Column(9).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    //Lưu file Excel
                    package.SaveAs(newFile);
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
            return "OK";
        }
    }
}
