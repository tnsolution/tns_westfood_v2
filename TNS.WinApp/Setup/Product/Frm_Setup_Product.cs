﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;
using TNS.IVT;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Setup_Product : Form
    {
        string _KeyParent = "";
        Product_Object _ProductObj;
        List<Product_Info> _List;

        public Frm_Setup_Product()
        {
            InitializeComponent();
            btnClose.Click += btnClose_Click;
            btnMax.Click += btnMax_Click;
            btnMini.Click += btnMini_Click;
            GVData.KeyDown += GVData_KeyDown;
            GVData.CellEndEdit += GVData_CellEndEdit;
            GVData.DataError += GVData_DataError;
            btn_Save.Click += Btn_Save_Click;

            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;
        }

        private void GVData_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            // Don't throw an exception when we're done.
            e.ThrowException = false;

            // Display an error message.
            string txt = "Error with " +
                GVData.Columns[e.ColumnIndex].HeaderText +
                "\n\n" + e.Exception;
            MessageBox.Show(txt, "Error",
                MessageBoxButtons.OK, MessageBoxIcon.Error);

            // If this is true, then the user is trapped in this cell.
            e.Cancel = false;
        }

        private void GVData_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            switch (GVData.CurrentCell.ColumnIndex)
            {
                case 1: // autocomplete for product
                    if (GVData.CurrentCell.Value != null && GVData.CurrentCell.Value.ToString().Trim() != string.Empty)
                    {
                        string ID = GVData.CurrentCell.Value.ToString().Trim();
                        Product_Info zInfo = new Product_Info();
                        zInfo.Get_Product_Info_ID(ID);
                        if (zInfo.ProductKey.Length > 0)
                        {
                            MessageBox.Show("Mã sản phẩm này đã có rồi !.");
                        }
                    }
                    break;
            }
        }

        private void Frm_Setup_Product_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            Check_RoleForm();
            this.DoubleBuffered = true;
            Utils.DrawGVStyle(ref GVData);
            Utils.DrawLVStyle(ref LVData);

            InitLayout_LV(LVData);
            InitLayout_GV(GVData);

            LoadDataToToolbox.ComboBoxData(cboUnit, "SELECT UnitKey, UnitName FROM IVT_Product_Unit WHERE RecordStatus < 99 ORDER BY UnitName", true);

            //InitData_LV(LVData, txt_Search_Parent.Text.Trim());
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
        }

        private void InitLayout_LV(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "#";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Nguyên liệu";
            colHead.Width = 250;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            LV.SelectedIndexChanged += LV_Click;
        }

        private void InitLayout_GV(DataGridView GV)
        {
            // Setup Column 
            GV.Columns.Add("No", "STT");
            GV.Columns.Add("ProductID", "Mã");
            GV.Columns.Add("ProductName", "Tên");

            DataGridViewComboBoxColumn cboUnit = new DataGridViewComboBoxColumn();
            cboUnit.HeaderText = "Đơn vị";
            cboUnit.Name = "cboUnit";
            LoadDataToToolbox.ComboBoxData(cboUnit, "SELECT UnitKey, UnitName FROM IVT_Product_Unit WHERE RecordStatus < 99 ORDER BY UnitName", true);
            cboUnit.Width = 150;
            cboUnit.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
            GVData.Columns.Add(cboUnit);

            DataGridViewComboBoxColumn cboCategory = new DataGridViewComboBoxColumn();
            cboCategory.HeaderText = "Loại";
            cboCategory.Name = "cboCategory";
            LoadDataToToolbox.ComboBoxData(cboCategory, " SELECT CategoryKey, CategoryName FROM IVT_Product_Category WHERE CategoryKey <> 1 AND RecordStatus< 99 ORDER BY [RANK]", true);
            cboCategory.Width = 150;
            cboCategory.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
            GVData.Columns.Add(cboCategory);

            GV.Columns.Add("Rank", "Sắp xếp");
            GV.Columns.Add("Description", "Ghi chú");

            GV.Columns["No"].Width = 40;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV.Columns["No"].ReadOnly = true;

            GV.Columns["ProductID"].Width = 100;
            GV.Columns["ProductID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["ProductName"].Width = 250;
            GV.Columns["ProductName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["Description"].Width = 200;
            GV.Columns["Description"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        }

        private void LV_Click(object sender, EventArgs e)
        {
            if (LVData.SelectedItems.Count > 0)
            {

                _KeyParent = LVData.SelectedItems[0].Tag.ToString();
                string Name = LVData.SelectedItems[0].SubItems[2].Text;
                txtSubTitle.Text = "Chi tiết sản phẩm [" + Name + "]";
                LoadProduct(_KeyParent);
            }
            else
            {
                txtSubTitle.Text = "";
                GVData.Rows.Clear();
                _ProductObj = new Product_Object();
            }
        }

        private void LoadProduct(string Key)
        {
            this.Cursor = Cursors.WaitCursor;
            _ProductObj = new Product_Object(Key);
            cboUnit.SelectedValue = _ProductObj.BasicUnit;
            txt_ProductName.Text = _ProductObj.ProductName.ToString();
            txt_ProductID.Text = _ProductObj.ProductID.ToString();
            txt_Description.Text = _ProductObj.Description;
            txt_Rank.Text = _ProductObj.Rank.ToString();
            InitData_GV(GVData, _ProductObj.ChildProduct);

            _List = _ProductObj.ChildProduct;
            this.Cursor = Cursors.Default;
        }

        private void SaveProduct(string Key)
        {
            this.Cursor = Cursors.WaitCursor;
            if (cboUnit.SelectedValue == null)
            {
                cboUnit.BackColor = Color.FromArgb(230, 100, 100);
                this.Cursor = Cursors.Default;
                return;
            }
            if (txt_ProductName.Text.Trim().Length <= 0)
            {
                txt_ProductName.BackColor = Color.FromArgb(230, 100, 100);
                this.Cursor = Cursors.Default;
                return;
            }

            cboUnit.BackColor = Color.FromArgb(255, 255, 225);
            txt_ProductName.BackColor = Color.FromArgb(255, 255, 225);

            if (Key != "")
                _ProductObj = new Product_Object(Key);
            else
                _ProductObj = new Product_Object();

            _ProductObj.ProductID = txt_ProductID.Text.ToUpper();
            _ProductObj.ProductName = txt_ProductName.Text;
            if (cboUnit.SelectedValue != null)
                _ProductObj.BasicUnit = int.Parse(cboUnit.SelectedValue.ToString());
            _ProductObj.CategoryKey = 1;
            if (txt_Rank.Text.Trim().Length > 0)
                _ProductObj.Rank = int.Parse(txt_Rank.Text.Trim());
            _ProductObj.Parent = "0";
            _ProductObj.Description = txt_Description.Text.Trim();

            if (GVData.Rows.Count > 0)
            {
                List<Product_Info> zList = new List<Product_Info>();
                for (int i = 0; i < GVData.Rows.Count - 1; i++)
                {
                    DataGridViewRow GvRow = GVData.Rows[i];
                    Product_Info zNewChild = (Product_Info)GvRow.Tag;
                    if (zNewChild != null)
                    {
                        if (GvRow.Cells["ProductID"].Tag.ToInt() == 99)
                        {
                            zNewChild.RecordStatus = 99;
                        }
                        else
                        {
                            if (GvRow.Cells["ProductID"].Tag != null)
                                zNewChild.ProductKey = GvRow.Cells["ProductID"].Tag.ToString();
                            if (GvRow.Cells["ProductID"].Value != null)
                                zNewChild.ProductID = GvRow.Cells["ProductID"].Value.ToString();
                            if (GvRow.Cells["ProductName"].Value != null)
                                zNewChild.ProductName = GvRow.Cells["ProductName"].Value.ToString();
                            if (GvRow.Cells["cboUnit"].Value != null)
                                zNewChild.BasicUnit = GvRow.Cells["cboUnit"].Value.ToInt();
                            if (GvRow.Cells["cboCategory"].Value != null)
                                zNewChild.CategoryKey = GvRow.Cells["cboCategory"].Value.ToInt();
                            if (GvRow.Cells["Description"].Value != null)
                                zNewChild.Description = GvRow.Cells["Description"].Value.ToString();
                            if (GvRow.Cells["Rank"].Value != null)
                                zNewChild.Rank = GvRow.Cells["Rank"].Value.ToInt();

                            Product_Info zOldChild = _ProductObj.ChildProduct.Find(x => x.ProductKey == zNewChild.ProductKey);
                            if (zOldChild != null)
                            {
                                //kiem tra 2 đối tượng có thay đổi giá trị hay không có => update 
                                bool Exist = DeepCompare(zOldChild, zNewChild);
                                if (!Exist)
                                {
                                    zNewChild.RecordStatus = 2;
                                }
                            }
                        }

                        zList.Add(zNewChild);
                    }
                    else
                    {
                        zNewChild = new Product_Info();
                        zNewChild.RecordStatus = 1;
                        zNewChild.ProductKey = "";
                        if (GvRow.Cells["ProductID"].Value != null)
                            zNewChild.ProductID = GvRow.Cells["ProductID"].Value.ToString();
                        if (GvRow.Cells["ProductName"].Value != null)
                            zNewChild.ProductName = GvRow.Cells["ProductName"].Value.ToString();
                        if (GvRow.Cells["cboUnit"].Value != null)
                            zNewChild.BasicUnit = GvRow.Cells["cboUnit"].Value.ToInt();
                        if (GvRow.Cells["cboCategory"].Value != null)
                            zNewChild.CategoryKey = GvRow.Cells["cboCategory"].Value.ToInt();
                        if (GvRow.Cells["Description"].Value != null)
                            zNewChild.Description = GvRow.Cells["Description"].Value.ToString();
                        if (GvRow.Cells["Rank"].Value != null)
                            zNewChild.Rank = GvRow.Cells["Rank"].Value.ToInt();
                        zList.Add(zNewChild);
                    }
                }
                _ProductObj.ChildProduct = zList;
            }
            _ProductObj.CreatedBy = SessionUser.UserLogin.Key;
            _ProductObj.CreatedName = SessionUser.UserLogin.EmployeeName;
            _ProductObj.ModifiedBy = SessionUser.UserLogin.Key;
            _ProductObj.ModifiedName = SessionUser.UserLogin.EmployeeName;

            _ProductObj.Save_Object();

            if (_ProductObj.Message.Substring(0, 2) == "20" ||
                _ProductObj.Message.Substring(0, 2) == "11")
            {
                InitData_LV(LVData, string.Empty);
                LoadProduct(_KeyParent);
                this.Cursor = Cursors.Default;
                MessageBox.Show("Cập nhật thành công!");
            }
            else
            {
                this.Cursor = Cursors.Default;
                MessageBox.Show("Cập nhật không thành công !.\r\nVui lòng kiểm tra lại !.");
            }
        }
        private void InitData_GV(DataGridView GV, List<Product_Info> zList)
        {
            GV.Rows.Clear();
            if (zList.Count > 0)
            {
                for (int i = 0; i < zList.Count; i++)
                {
                    Product_Info zProduct = zList[i];

                    GV.Rows.Add();
                    GV.Rows[i].Tag = zProduct;
                    GV.Rows[i].Cells["No"].Value = (i + 1).ToString();
                    GV.Rows[i].Cells["ProductID"].Tag = zProduct.ProductKey;
                    GV.Rows[i].Cells["ProductID"].Value = zProduct.ProductID;
                    GV.Rows[i].Cells["ProductName"].Value = zProduct.ProductName;
                    GV.Rows[i].Cells["cboUnit"].Value = zProduct.BasicUnit;
                    GV.Rows[i].Cells["cboCategory"].Value = zProduct.CategoryKey;
                    GV.Rows[i].Cells["Description"].Value = zProduct.Description;
                    GV.Rows[i].Cells["Rank"].Value = zProduct.Rank;
                }
            }
        }

        private void InitData_LV(ListView LV, string txtSearch)
        {
            this.Cursor = Cursors.WaitCursor;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            LV.Items.Clear();
            DataTable ztb = Product_Data.List_Material_Search(txtSearch);
            for (int i = 0; i < ztb.Rows.Count; i++)
            {
                DataRow nRow = ztb.Rows[i];

                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.Tag = nRow["ProductKey"];
                lvi.ForeColor = Color.Navy;

                lvi.BackColor = Color.White;
                lvi.ImageIndex = 0;


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["ProductID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["ProductName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }

            this.Cursor = Cursors.Default;

        }



        private void GVData_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                DialogResult dlr = MessageBox.Show("Bạn có xóa thông tin này ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlr == DialogResult.Yes)
                {
                    for (int i = 0; i < GVData.Rows.Count; i++)
                    {
                        if (GVData.Rows[i].Selected)
                        {
                            if (GVData.Rows[i].Tag != null)
                            {
                                string Key = GVData.Rows[i].Cells["ProductID"].Tag.ToString();
                                Product_Info zInfo = new Product_Info(Key);
                                zInfo.ModifiedBy = SessionUser.UserLogin.Key;
                                zInfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
                                zInfo.Delete();

                                GVData.Rows.Remove(GVData.Rows[i]);
                            }
                            else
                            {
                                GVData.Rows.RemoveAt(i);
                            }

                            GVData.CommitEdit(DataGridViewDataErrorContexts.Commit);
                        }
                    }
                }
            }

        }

        private void btn_Search_Parent_Click(object sender, EventArgs e)
        {
            InitData_LV(LVData, txt_Search_Parent.Text.Trim());
        }

        private void btn_Del_Click(object sender, EventArgs e)
        {
            if (LVData.SelectedItems.Count <= 0)
            {
                MessageBox.Show("Chưa chọn thông tin !.");
            }
            else
            {
                if (MessageBox.Show("Bạn có chắc xóa thông tin này ?.", "Thông báo", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    string Key = LVData.SelectedItems[0].Tag.ToString();
                    _ProductObj = new Product_Object(Key);
                    _ProductObj.ModifiedBy = SessionUser.UserLogin.Key;
                    _ProductObj.ModifiedName = SessionUser.UserLogin.EmployeeName;
                    _ProductObj.DeleteObject();
                    if (_ProductObj.Message.Substring(0, 2) != "30")
                    {
                        MessageBox.Show("Xóa không thành công !.\r\nVui lòng kiểm tra lại !.");
                    }
                    else
                    {
                        InitData_LV(LVData, txt_Search_Parent.Text.Trim());
                        ClearForm();
                    }
                }
            }
        }

        private void ClearForm()
        {
            _KeyParent = "";
            cboUnit.SelectedIndex = 0;
            txt_ProductName.Text = "";
            txt_ProductID.Text = "";
            txt_Description.Text = "";
            txt_Rank.Text = "0";
            txt_ProductID.Focus();
            GVData.Rows.Clear();
            if (LVData.SelectedIndices.Count > 0)
            {
                for (int i = 0; i < LVData.SelectedIndices.Count; i++)
                {
                    LVData.Items[LVData.SelectedIndices[i]].Selected = false;
                }
            }
        }

        private void Btn_Save_Click(object sender, EventArgs e)
        {
            SaveProduct(_KeyParent);
        }

        private void btn_New_Click(object sender, EventArgs e)
        {

            ClearForm();
        }

        public static bool Compare<T>(T e1, T e2)
        {
            bool flag = true;
            bool match = false;
            int countFirst, countSecond;
            foreach (PropertyInfo propObj1 in e1.GetType().GetProperties())
            {
                var propObj2 = e2.GetType().GetProperty(propObj1.Name);
                if (propObj1.PropertyType.Name.Equals("List`1"))
                {
                    dynamic objList1 = propObj1.GetValue(e1, null);
                    dynamic objList2 = propObj2.GetValue(e2, null);
                    countFirst = objList1.Count;
                    countSecond = objList2.Count;
                    if (countFirst == countSecond)
                    {
                        countFirst = objList1.Count - 1;
                        while (countFirst > -1)
                        {
                            match = false;
                            countSecond = objList2.Count - 1;
                            while (countSecond > -1)
                            {
                                match = Compare(objList1[countFirst], objList2[countSecond]);
                                if (match)
                                {
                                    objList2.Remove(objList2[countSecond]);
                                    countSecond = -1;
                                    match = true;
                                }
                                if (match == false && countSecond == 0)
                                {
                                    return false;
                                }
                                countSecond--;
                            }
                            countFirst--;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                else if (!(propObj1.GetValue(e1, null).Equals(propObj2.GetValue(e2, null))))
                {
                    flag = false;
                    return flag;
                }
            }
            return flag;
        }

        public static bool DeepCompare(object obj, object another)
        {
            if (ReferenceEquals(obj, another)) return true;
            if ((obj == null) || (another == null)) return false;
            //Compare two object's class, return false if they are difference
            if (obj.GetType() != another.GetType()) return false;

            var result = true;
            //Get all properties of obj
            //And compare each other
            foreach (var property in obj.GetType().GetProperties())
            {
                var objValue = property.GetValue(obj, null);
                var anotherValue = property.GetValue(another, null);
                if (!objValue.Equals(anotherValue))
                {
                    result = false;
                    break;
                }
            }

            return result;
        }

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        private void btn_Search_Child_Click(object sender, EventArgs e)
        {
            if (LVData.SelectedItems.Count <= 0)
            {
                MessageBox.Show("Bạn phải chọn 1 sản phẩm !.");
                return;
            }
            if (txt_Search_Child.Text.Trim().Length > 0)
            {
                string Key = LVData.SelectedItems[0].Tag.ToString();
                List<Product_Info> ListResult = _List.FindAll(x => x.ProductID == txt_Search_Child.Text.Trim());
                InitData_GV(GVData, ListResult);
            }
            else
            {
                InitData_GV(GVData, _List);
            }
        }
        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search_Parent.Enabled = false;
                btn_Search_Child.Enabled = false;
            }
            if (Role.RoleAdd == 0 && Role.RoleEdit == 0)
            {
                btn_New.Enabled = false;
                btn_Save.Enabled = false;
            }
            if (Role.RoleAdd == 0 && Role.RoleEdit == 1)
            {
                btn_New.Enabled = false;
                btn_Save.Enabled = true;
            }
            if (Role.RoleDel == 0)
            {
                btn_Del.Enabled = false;
            }
        }
        #endregion
    }
}