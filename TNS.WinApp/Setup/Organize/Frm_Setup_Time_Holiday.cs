﻿using C1.Win.C1FlexGrid;
using System;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Setup_Time_Holiday : Form
    {
        public Frm_Setup_Time_Holiday()
        {
            InitializeComponent();
            btnClose.Click += btnClose_Click;
            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            btn_Save.Click += Btn_Save_Click;
            btn_New.Click += Btn_New_Click;
            btn_Del.Click += Btn_Del_Click;
            btn_Search.Click += Btn_Search_Click;
            txt_Factor.Leave += Txt_Factor_Leave;
            GVData.Click += GVData_List_Click;
            btn_Export.Click += Btn_Export_Click;
        }

        #region[Private]
        private int _HolidayKey = 0;
        private DateTime _Date;
        DateTime FromDate;
        DateTime ToDate;
        #endregion

        private void Frm_CategoryHoliday_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            Check_RoleForm();
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            LoadData();
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            _Date = dte_Search.Value;
            dte_Holiday.Value = SessionUser.Date_Work;
        }

        #region[Progess]
        private void LoadData()
        {
            Time_Holiday_Info zinfo = new Time_Holiday_Info(_HolidayKey);
            dte_Holiday.Value = zinfo.Holiday;
            txt_HolidayName.Text = zinfo.HolidayName;
            txt_Rank.Text = zinfo.Rank.ToString("n0");
            txt_Description.Text = zinfo.Description;
            txt_Factor.Text = zinfo.Factor.ToString("n1");
            if (zinfo.Support == 1)
                cbo_Support.SelectedIndex = 0;
            else
                cbo_Support.SelectedIndex = 1;
            lbl_Created.Text = "Tạo bởi:[" + zinfo.CreatedName + "][" + zinfo.CreatedOn + "]";
            lbl_Modified.Text = "Chỉnh sửa:[" + zinfo.ModifiedName + "][" + zinfo.ModifiedOn + "]";
        }

        private void SetDefault()
        {
            _HolidayKey = 0;
            dte_Holiday.Value = DateTime.Now;
            txt_HolidayName.Text = "";
            txt_Rank.Text = "0";
            txt_Description.Text = "";
            txt_Factor.Text = "1";
            cbo_Support.SelectedIndex = 0;

            lbl_Created.Text = "Tạo bởi:";
            lbl_Modified.Text = "Chỉnh sửa:";
        }

        #endregion

        #region[Event]
        private void Btn_Save_Click(object sender, EventArgs e)
        {
            if (SessionUser.Date_Lock >= dte_Holiday.Value)
            {
                Utils.TNMessageBoxOK("Thời gian này đã khóa sổ.!", 2);
                return;
            }

            if (txt_HolidayName.Text.Trim().Length == 0)
            {
                Utils.TNMessageBoxOK("Vui lòng nhập tên ngày nghỉ/lễ!", 1);
                return;
            }
            float zFactor;
            if (!float.TryParse(txt_Factor.Text, out zFactor))
            {
                Utils.TNMessageBoxOK("Vui lòng nhập đúng định dạng số!", 1);
                return;
            }
            int zRank;
            if (!int.TryParse(txt_Rank.Text, out zRank))
            {
                Utils.TNMessageBoxOK("Vui lòng nhập đúng định dạng số!", 1);
                return;
            }
            else
            {
                txt_HolidayName.BackColor = Color.White;
                Time_Holiday_Info zinfo = new Time_Holiday_Info(_HolidayKey);
                zinfo.Holiday = dte_Holiday.Value;
                zinfo.HolidayName = txt_HolidayName.Text.Trim();
                if (txt_Rank.Text.Length == 0)
                {
                    zinfo.Rank = 0;
                }
                else
                    zinfo.Rank = int.Parse(txt_Rank.Text);
                zinfo.Description = txt_Description.Text.Trim();
                zinfo.Factor = float.Parse(txt_Factor.Text.Trim());
                if (cbo_Support.SelectedIndex == 0)
                    zinfo.Support = 1;
                else
                    zinfo.Support = 0;
                zinfo.CreatedBy = SessionUser.UserLogin.Key;
                zinfo.CreatedName = SessionUser.UserLogin.EmployeeName;
                zinfo.ModifiedBy = SessionUser.UserLogin.Key;
                zinfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
                zinfo.Save();
                if (zinfo.Message == string.Empty)
                {
                    Utils.TNMessageBoxOK("Cập nhật thành công!", 3);
                    DisplayData();
                    SetDefault();
                }
                else
                {
                    Utils.TNMessageBoxOK(zinfo.Message, 4);
                }
            }


        }
        private void Btn_Del_Click(object sender, EventArgs e)
        {
            if (_HolidayKey == 0)
            {
                Utils.TNMessageBoxOK("Chưa chọn thông tin!", 1);
            }
            else
            {
                Time_Holiday_Info zinfo = new Time_Holiday_Info(_HolidayKey);
                if (SessionUser.Date_Lock >= zinfo.Holiday)
                {
                    Utils.TNMessageBoxOK("Thời gian này đã khóa sổ.!", 2);
                    return;
                }
                if (Utils.TNMessageBox("Bạn có chắc xóa thông tin này ?.", 2) == "Y")
                {
                   
                    zinfo.ModifiedBy = SessionUser.UserLogin.Key;
                    zinfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
                    zinfo.Delete();
                    if (zinfo.Message == string.Empty)
                    {
                        Utils.TNMessageBoxOK("Đã xóa thành công !", 3);
                        DisplayData();
                        SetDefault();
                    }
                    else
                    {
                        Utils.TNMessageBoxOK(zinfo.Message, 4);
                    }

                }
            }

        }
        private void Btn_New_Click(object sender, EventArgs e)
        {
            SetDefault();
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            DisplayData();
        }
        private void Txt_Factor_Leave(object sender, EventArgs e)
        {
            double zAmount = 0;
            if (double.TryParse(txt_Factor.Text.Trim(), out zAmount))
            {
                txt_Factor.Text = zAmount.Toe1String();
                txt_Factor.SelectionStart = txt_Factor.Text.Length;
            }
            else
            {
                Utils.TNMessageBoxOK("Lỗi!.Định dạng số.", 1);
            }
        }

        private void txt_Factor_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar > '0' && e.KeyChar < '9') || (Keys)e.KeyChar == Keys.Back || e.KeyChar == 46 | e.KeyChar == 44)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
        private void txt_Rank_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar > '0' && e.KeyChar < '9') || (Keys)e.KeyChar == Keys.Back || e.KeyChar == 46 | e.KeyChar == 44)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
        #endregion

        #region[ListView]
        private void DisplayData()
        {

            DataTable zTable = Time_Holiday_Data.Search(dte_Search.Value, txt_Search.Text);
            InitGV_Layout(zTable);
        }

        private void GVData_List_Click(object sender, EventArgs e)
        {
            if (GVData.Rows.Count > 0)
            {
                string zKey = GVData.Rows[GVData.RowSel][7].ToString();
                if (zKey != null || zKey != "")
                {
                    _HolidayKey = int.Parse(zKey);
                    LoadData();
                }
                else
                {
                    _HolidayKey = 0;
                    LoadData();
                }
            }
        }
        private void Btn_Export_Click(object sender, EventArgs e)
        {
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Danh sách ngày nghỉ.xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }
                    GVData.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }

        }
        void InitGV_Layout(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            int TotalRow = TableView.Rows.Count + 1;
            int ToTalCol = 8;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(TotalRow);

            //Row Header
            GVData.Rows[0][0] = "Stt";
            GVData.Rows[0][1] = "Ngày";
            GVData.Rows[0][2] = "Nội dung";
            GVData.Rows[0][3] = "Hế số làm lễ tết";
            GVData.Rows[0][4] = "Nghỉ lễ tết được tính hỗ trợ";
            GVData.Rows[0][5] = "Sắp xếp";
            GVData.Rows[0][6] = "Ghi chú";
            GVData.Rows[0][7] = ""; // ẩn cột này

            //Style         
            //GVData.AllowFreezing = AllowFreezingEnum.Both;
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.Custom;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.LeftCenter;
            GVData.Styles.Normal.WordWrap = true;

            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                DataRow rData = TableView.Rows[rIndex];

                GVData.Rows[rIndex + 1][0] = rIndex + 1;
                GVData.Rows[rIndex + 1][1] = rData[1];
                GVData.Rows[rIndex + 1][2] = rData[2];
                GVData.Rows[rIndex + 1][3] = rData[3].Toe1String();
                if (rData[4].ToInt() == 1)
                    GVData.Rows[rIndex + 1][4] = "Có";
                else
                    GVData.Rows[rIndex + 1][4] = "Không";
                GVData.Rows[rIndex + 1][5] = rData[5];
                GVData.Rows[rIndex + 1][6] = rData[6];
                GVData.Rows[rIndex + 1][7] = rData[0];

            }



            //Freeze Row and Column                              
            GVData.Rows.Fixed = 1;
            GVData.Rows[0].TextAlign = TextAlignEnum.CenterCenter;
            GVData.Rows[0].Height = 50;

            GVData.Cols[0].Width = 30;
            GVData.Cols[1].Width = 100;
            GVData.Cols[2].Width = 250;
            GVData.Cols[3].Width = 80;
            GVData.Cols[4].Width = 80;
            GVData.Cols[5].Width = 80;
            GVData.Cols[7].Visible = false;

            GVData.Cols[3].TextAlign = TextAlignEnum.RightCenter;
            GVData.Cols[4].TextAlign = TextAlignEnum.RightCenter;
        }
        #endregion

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                // this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion               

        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
                btn_Export.Enabled = false;
            }
            if (Role.RoleAdd == 0 && Role.RoleEdit == 0)
            {
                btn_New.Enabled = false;
                btn_Save.Enabled = false;
            }
            if (Role.RoleAdd == 0 && Role.RoleEdit == 1)
            {
                btn_New.Enabled = false;
                btn_Save.Enabled = true;
            }
            if (Role.RoleDel == 0)
            {
                btn_Del.Enabled = false;
            }
        }
        #endregion
    }
}
