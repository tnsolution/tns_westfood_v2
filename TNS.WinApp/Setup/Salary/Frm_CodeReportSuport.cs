﻿using C1.Win.C1FlexGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.LOG;
using TNS.Misc;
using TNS.SLR;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_CodeReportSuport : Form
    {
        private int _Key = 0;
        private DateTime _FromDate;
        private DateTime _ToDate;
        public Frm_CodeReportSuport()
        {
            InitializeComponent();
            btn_Save.Click += Btn_Save_Click;
            btn_New.Click += Btn_New_Click;
            btn_Del.Click += Btn_Del_Click;

            btnMini.Click += btnMini_Click;
            btnClose.Click += btnClose_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            btn_Search.Click += Btn_Search_Click;
            GVData.Click += GV_Click;
            btn_Export.Click += Btn_Export_Click;


        }

        

        private void Frm_CodeReportSuport_Load(object sender, EventArgs e)
        {
            SetDefault();
            DisplayData();
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
        }
        private void SetDefault()
        {
            _Key = 0;
            txt_Rank.Text = "0";
            txt_ID.Text = "";
            txt_Name.Text = "";
            txt_TypeName.Text = "";
            chk_Publish.Checked=false;
            chk_PublishClose.Checked = false;
            txt_Description.Text = "";
        }
        private void LoadData()
        {
            CodeReportSupport_Info zinfo = new CodeReportSupport_Info(_Key);
            txt_ID.Text = zinfo.ID;
            txt_Name.Text = zinfo.Name;
            txt_Rank.Text = zinfo.Rank.ToString();
            if (zinfo.Publish == 1)
                chk_Publish.Checked = true;
            else
                chk_Publish.Checked = false;
            if (zinfo.Publish_Close == 1)
                chk_PublishClose.Checked = true;
            else
                chk_PublishClose.Checked = false;
            txt_TypeName.Text = zinfo.TypeName;
            txt_Description.Text = zinfo.Description;
        }
        private void Btn_Save_Click(object sender, EventArgs e)
        {
            if (txt_ID.Text.Trim().Length == 0)
            {
                MessageBox.Show("Chưa nhập mã!");
                return;
            }
            int zNumber = 0;
            if (!int.TryParse(txt_Rank.Text.Trim(), out zNumber))
            {
                MessageBox.Show("Số thứ tự sai định dạng");
                return;
            }
            if (zNumber == 0)
            {
                MessageBox.Show("Số sắp xếp phải lớn hơn 0");
                return;
            }
            else
            {
                CodeReportSupport_Info zinfo = new CodeReportSupport_Info(_Key);
                zinfo.DateWrite = _FromDate;
                zinfo.ID = txt_ID.Text.Trim().ToUpper();
                zinfo.Name = txt_Name.Text.Trim().ToUpper();
                zinfo.TypeName = txt_TypeName.Text.Trim().ToString();
                if(zinfo.TypeName.Length>0)
                {
                    zinfo.Type = 1;
                }
                else
                {
                    zinfo.Type = 0;
                }
                zinfo.Rank = int.Parse(txt_Rank.Text.Trim());
                if (chk_Publish.Checked)
                    zinfo.Publish = 1;
                else
                    zinfo.Publish = 0;
                if (chk_PublishClose.Checked)
                    zinfo.Publish_Close = 1;
                else
                    zinfo.Publish_Close = 0;
                zinfo.Description = txt_Description.Text.Trim();

                zinfo.CreatedBy = SessionUser.UserLogin.Key;
                zinfo.CreatedName = SessionUser.UserLogin.EmployeeName;
                zinfo.ModifiedBy = SessionUser.UserLogin.Key;
                zinfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
                zinfo.Save();
                if (zinfo.Message == "")
                {
                    MessageBox.Show("Cập nhật thành công!");
                    SetDefault();
                    DisplayData();
                }
                else
                {
                    MessageBox.Show(zinfo.Message);
                }
            }


        }
        private void Btn_Del_Click(object sender, EventArgs e)
        {
            if (_Key == 0)
            {
                MessageBox.Show("Chưa chọn thông tin!");
            }
            else
            {
                if (MessageBox.Show("Bạn có chắc xóa thông tin này ?.", "Thông báo", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    CodeReportSupport_Info zinfo = new CodeReportSupport_Info(_Key);
                    zinfo.ModifiedBy = SessionUser.UserLogin.Key;
                    zinfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
                    zinfo.Delete();
                    if (zinfo.Message == "")
                    {
                        MessageBox.Show("Đã xóa !");
                        SetDefault();
                        DisplayData();
                    }
                    else
                    {
                        MessageBox.Show(zinfo.Message);
                    }

                }
            }

        }
        private void Btn_New_Click(object sender, EventArgs e)
        {
            SetDefault();
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {

            DisplayData();

        }
        private void DisplayData()
        {
            var zResult = dte_Month.Value.FirstEndMonth();
            _FromDate = zResult.Item1;
            _ToDate = zResult.Item2;

            DataTable In_Table = new DataTable();
            In_Table = CodeReportSupport_Data.List(_FromDate, _ToDate);
            InitGV_Layout(In_Table);
        }
        private void GV_Click(object sender, EventArgs e)
        {

            if (GVData.Rows[GVData.RowSel][8].ToString() != "")
            {
                _Key = GVData.Rows[GVData.RowSel][8].ToInt();
                LoadData();


            }
        }
        void InitGV_Layout(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            int TotalRow = TableView.Rows.Count + 1;
            int ToTalCol = 9;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(TotalRow);

            //Row Header
            GVData.Rows[0][0] = "STT";
            GVData.Rows[0][1] = "MÃ";
            GVData.Rows[0][2] = "TÊN LƯƠNG";
            GVData.Rows[0][3] = "LOẠI";
            GVData.Rows[0][4] = "SẮP XẾP";
            GVData.Rows[0][5] = "BẢNG LƯƠNG";
            GVData.Rows[0][6] = "BẢNG LƯƠNG CHỐT";
            GVData.Rows[0][7] = "GHI CHÚ";

            GVData.Rows[0][8] = ""; // ẩn cột này

            //Style         
            //GVData.AllowFreezing = AllowFreezingEnum.Both;
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.Custom;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.LeftCenter;
            GVData.Styles.Normal.WordWrap = true;

            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                DataRow rData = TableView.Rows[rIndex];

                GVData.Rows[rIndex + 1][0] = rIndex + 1;
                GVData.Rows[rIndex + 1][1] = rData[1];
                GVData.Rows[rIndex + 1][2] = rData[2];
                GVData.Rows[rIndex + 1][3] = rData[3];
                GVData.Rows[rIndex + 1][4] = rData[4];
                if (rData[5].ToInt() == 1)
                    GVData.Rows[rIndex + 1][5] = "Hiện";
                else
                    GVData.Rows[rIndex + 1][5] = "Ẩn";
                if (rData[6].ToInt() == 1)
                    GVData.Rows[rIndex + 1][6] = "Hiện";
                else
                    GVData.Rows[rIndex + 1][6] = "Ẩn";
                GVData.Rows[rIndex + 1][7] = rData[7];
                GVData.Rows[rIndex + 1][8] = rData[0];
            }



            //Freeze Row and Column                              
            GVData.Rows.Fixed = 1;
            GVData.Rows[0].TextAlign = TextAlignEnum.CenterCenter;
            GVData.Rows[0].Height = 40;

            GVData.Cols[0].Width = 30;
            GVData.Cols[1].Width = 70;
            GVData.Cols[2].Width = 200;
            GVData.Cols[3].Width = 200;
            GVData.Cols[4].Width = 60;
            GVData.Cols[5].Width = 70;
            GVData.Cols[6].Width = 100;
            GVData.Cols[7].Width = 60;
            GVData.Cols[8].Visible = false;

            GVData.Cols[4].TextAlign = TextAlignEnum.RightCenter;
            GVData.Cols[5].TextAlign = TextAlignEnum.RightCenter;
        }
        private void Btn_Export_Click(object sender, EventArgs e)
        {
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Định nghĩa lương hỗ trợ.xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        MessageBox.Show("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }

                    string Status = "Xuất Excel form " + HeaderControl.Text + " > đường dẫn và tên file xuất:" + FDialog.FileName;
                    //Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);

                    GVData.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }

        }


        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;



        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}
