﻿using System;
using System.Threading;
using System.Windows.Forms;
using TNS.WinApp;

namespace TNS.WinApp
{
    static class Program
    {
        private static Mutex mutex = null;
        [STAThread]
        static void Main()
        {
            //const string appName = "WestFood";
            //bool createdNew;

            //mutex = new Mutex(true, appName, out createdNew);

            //if (!createdNew)
            //{
            //    //app is already running! Exiting the application  
            //    return;
            //}

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Frm_Main());
        }
    }
}
