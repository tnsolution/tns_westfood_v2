﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Connect;

namespace TNS.LOC
{
    public class District_Info
    {
        #region [ Field Name ]
        private int _DistrictKey = 0;
        private string _DistrictID = "";
        private string _DistrictName = "";
        private string _Latlong = "";
        private string _Ext = "";
        private int _Rank = 0;
        private int _ProvinceKey = 0;
        private int _CountryKey = 0;
        private int _RecordStatus = 0;
        private DateTime _CreatedOn;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _RoleID = "";
        private string _Message = "";
        #endregion
        #region [ Properties ]
        public bool IsCommandOK
        {
            get
            {
                int zNumber = 0;
                int.TryParse(_Message.Substring(0, 1), out zNumber);
                if (zNumber <= 3) return true; else return false;
            }
        }
        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        public int Key
        {
            get { return _DistrictKey; }
            set { _DistrictKey = value; }
        }
        public string DistrictID
        {
            get { return _DistrictID; }
            set { _DistrictID = value; }
        }
        public string DistrictName
        {
            get { return _DistrictName; }
            set { _DistrictName = value; }
        }
        public string Latlong
        {
            get { return _Latlong; }
            set { _Latlong = value; }
        }
        public string Ext
        {
            get { return _Ext; }
            set { _Ext = value; }
        }
        public int Rank
        {
            get { return _Rank; }
            set { _Rank = value; }
        }
        public int ProvinceKey
        {
            get { return _ProvinceKey; }
            set { _ProvinceKey = value; }
        }
        public int CountryKey
        {
            get { return _CountryKey; }
            set { _CountryKey = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion
        #region [ Constructor Get Information ]
        public District_Info()
        {
        }
        public District_Info(int DistrictKey)
        {
            string zSQL = "SELECT * FROM LOC_District WHERE DistrictKey = @DistrictKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@DistrictKey", SqlDbType.Int).Value = DistrictKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["DistrictKey"] != DBNull.Value)
                        _DistrictKey = int.Parse(zReader["DistrictKey"].ToString());
                    _DistrictID = zReader["DistrictID"].ToString().Trim();
                    _DistrictName = zReader["DistrictName"].ToString().Trim();
                    _Latlong = zReader["Latlong"].ToString().Trim();
                    _Ext = zReader["Ext"].ToString().Trim();
                    if (zReader["Rank"] != DBNull.Value)
                        _Rank = int.Parse(zReader["Rank"].ToString());
                    if (zReader["ProvinceKey"] != DBNull.Value)
                        _ProvinceKey = int.Parse(zReader["ProvinceKey"].ToString());
                    if (zReader["CountryKey"] != DBNull.Value)
                        _CountryKey = int.Parse(zReader["CountryKey"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }

        public void Get_District_Info(string DistrictName,int ProvinceKey)
        {
            string zSQL = "SELECT * FROM LOC_District WHERE DistrictName = @DistrictName AND ProvinceKey = @ProvinceKey AND RecordStatus <> 99 ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@DistrictName", SqlDbType.NVarChar).Value = DistrictName;
                zCommand.Parameters.Add("@ProvinceKey", SqlDbType.Int).Value = ProvinceKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["DistrictKey"] != DBNull.Value)
                        _DistrictKey = int.Parse(zReader["DistrictKey"].ToString());
                    _DistrictID = zReader["DistrictID"].ToString().Trim();
                    _DistrictName = zReader["DistrictName"].ToString().Trim();
                    _Latlong = zReader["Latlong"].ToString().Trim();
                    _Ext = zReader["Ext"].ToString().Trim();
                    if (zReader["Rank"] != DBNull.Value)
                        _Rank = int.Parse(zReader["Rank"].ToString());
                    if (zReader["ProvinceKey"] != DBNull.Value)
                        _ProvinceKey = int.Parse(zReader["ProvinceKey"].ToString());
                    if (zReader["CountryKey"] != DBNull.Value)
                        _CountryKey = int.Parse(zReader["CountryKey"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO LOC_District ("
        + " DistrictID ,DistrictName ,Latlong ,Ext ,Rank ,ProvinceKey ,CountryKey ,CreatedOn ,CreatedBy ,CreatedName ,ModifiedOn ,ModifiedBy ,ModifiedName ) "
         + " VALUES ( "
         + "@DistrictID ,@DistrictName ,@Latlong ,@Ext ,@Rank ,@ProvinceKey ,@CountryKey ,GETDATE(),@CreatedBy ,@CreatedName ,GETDATE(),@ModifiedBy ,@ModifiedName ) ";
            zSQL += " SELECT DistrictKey FROM LOC_District WHERE DistrictKey = SCOPE_IDENTITY() ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@DistrictID", SqlDbType.NVarChar).Value = _DistrictID.Trim();
                zCommand.Parameters.Add("@DistrictName", SqlDbType.NVarChar).Value = _DistrictName.Trim();
                zCommand.Parameters.Add("@Latlong", SqlDbType.NVarChar).Value = _Latlong.Trim();
                zCommand.Parameters.Add("@Ext", SqlDbType.NVarChar).Value = _Ext.Trim();
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zCommand.Parameters.Add("@ProvinceKey", SqlDbType.Int).Value = _ProvinceKey;
                zCommand.Parameters.Add("@CountryKey", SqlDbType.Int).Value = _CountryKey;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE LOC_District SET "
                        + " DistrictID = @DistrictID,"
                        + " DistrictName = @DistrictName,"
                        + " Latlong = @Latlong,"
                        + " Ext = @Ext,"
                        + " Rank = @Rank,"
                        + " ProvinceKey = @ProvinceKey,"
                        + " CountryKey = @CountryKey,"
                        + " CreatedOn = @CreatedOn,"
                        + " CreatedBy = @CreatedBy,"
                        + " CreatedName = @CreatedName,"
                        + " ModifiedOn = @ModifiedOn,"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                       + " WHERE DistrictKey = @DistrictKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@DistrictKey", SqlDbType.Int).Value = _DistrictKey;
                zCommand.Parameters.Add("@DistrictID", SqlDbType.NVarChar).Value = _DistrictID.Trim();
                zCommand.Parameters.Add("@DistrictName", SqlDbType.NVarChar).Value = _DistrictName.Trim();
                zCommand.Parameters.Add("@Latlong", SqlDbType.NVarChar).Value = _Latlong.Trim();
                zCommand.Parameters.Add("@Ext", SqlDbType.NVarChar).Value = _Ext.Trim();
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zCommand.Parameters.Add("@ProvinceKey", SqlDbType.Int).Value = _ProvinceKey;
                zCommand.Parameters.Add("@CountryKey", SqlDbType.Int).Value = _CountryKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE LOC_District SET [RecordStatus] = 99, [ModifiedOn] = GETDATE(), ModifiedBy = @ModifiedBy, ModifiedName = @ModifiedName WHERE DistrictKey = @DistrictKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@DistrictKey", SqlDbType.Int).Value = _DistrictKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_DistrictKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        #endregion
    }
}
